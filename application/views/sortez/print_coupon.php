<html>
<head>
<meta charset="UTF-8">
<link rel="icon" href="images/favicon.ico">
<?php if (!isset($data)) $data['data_init'] = true; ?>
<title>Untitled Document</title>

<!-- Bootstrap core CSS -->
<link href="<?php echo GetCssPath("bootstrap/") ; ?>/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo GetCssPath("bootstrap/") ; ?>/bootstrap-theme.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/jquery-1.11.3.min.js"></script>


<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="<?php echo base_url()."application/resources/fancybox"; ?>/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<script type="text/javascript">
$(document).ready(function () {
window.print();
});
</script>


<style type="text/css">
<!--
.print_container {
	margin: auto;
    padding: 15px;
    text-align: center;
    width: 600px;
	
	background-color: transparent;
    color: #000000;
    font-family: "Futura Md",sans-serif;
    font-size: 26.7px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    /*line-height: 35px;*/
    text-decoration: none;
    vertical-align: 0;
}
.pink_trait {border: 8px dashed rgb(220, 26, 149); padding:15px 0;}
.bg_red {
	background-color: rgb(220, 26, 149); color: rgb(255, 255, 255);
    font-family: "Futura Md",sans-serif;
    font-size: 32px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    /*line-height: 42px;*/
    text-decoration: none;
    vertical-align: 0;
	display:table;
	width:100%;
	padding: 15px;
}
.margintop5 { margin-top:5px;}
.fontnormal { font-size:14px;}
-->
</style>
</head>

<body>
<div class="print_container">
  <div class="row pink_trait">
    <div class="col-xs-12"><img src="<?php echo GetImagePath("privicarte/");?>/img_print_coupont.png" width="100%"/></div>
    <div class="col-xs-12 fontnormal"><strong>Offre valable jusqu'au <?php echo convert_Sqldate_to_Frenchdate($oDetailbonplan->bonplan_date_fin); ?></strong></div>
    <div class="col-xs-12" style="margin-top: 30px">Coupon conc&eacute;d&eacute; par :</div>
    <div class="col-xs-12" style="margin-bottom: 30px">
    <?php echo $oInfoCommercant->NomSociete; ?>
	<?php echo $oInfoCommercant->Adresse1; ?>
    </div>
    <div class="col-xs-12">
    	<div class="col-xs-12 bg_red"><?php echo $oDetailbonplan->bonplan_titre; ?></div>
    </div>
  	<div class="col-xs-12 margintop5">
		<div class="col-xs-12 bg_red">
            <div class="col-xs-12 fontnormal"><strong>Conditions d'utilisation :</strong></div>
            <div class="col-xs-12 fontnormal"><?php echo $oDetailbonplan->bonplan_texte; ?></div>
        </div>
    </div>
  </div>
</div>
</body>
</html>