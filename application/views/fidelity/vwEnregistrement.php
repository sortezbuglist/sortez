<?php $data["zTitle"] = 'Accueil' ?>
<?php 
if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
	$this->load->view("fidelity/includes/header_mini_2_mobile", $data);
} else { 
	$this->load->view("fidelity/includes/header_mini_2", $data);
} 
?>

<div style="text-align:center;">

<?php $this->load->view("fidelity/button_css"); ?>


<script type="text/javascript">
function check_num_card(){
	
	var zErreur = "" ;
	var num_card_verification = $("#num_card_verification").val();
	
	$('#span_check_num_card_1').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
	$('#span_check_num_card_2').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
	
	$.post(
		'<?php echo site_url("front/fidelity/check_num_card");?>',
		{ 
		num_card_verification: num_card_verification
		},
		function (zReponse2)
		{
			if (zReponse2 == "1") {
				$('#span_check_num_card_1').html('<span style="color:#090">Le numéro de votre carte est reconnu,<br/>vérifier sa disponibilié</span>');
				$('#span_check_num_card_2').html('<span style="color:#090">Votre carte est valide,<br/>vous pouvez continuer votre enregistrement !</span>');
			} else if (zReponse2 == "2") {
				$('#span_check_num_card_1').html('');
				$('#span_check_num_card_2').html('<span style="color:#FF0000">Votre carte est répertorié, mais n\'est pas activé !</span>');
			} else {
				$('#span_check_num_card_1').html('');
				$('#span_check_num_card_2').html('<span style="color:#FF0000">Ce numéro de carte est invalide !</span>');
			}
	   });
	
}

function fidelity_reconnexion(){
	var zErreur = "" ;
	var identity = $("#identity").val();
	var password = $("#password").val();
	
	if (identity=="" || password=="") {
		$('#span_customer_login').html('<span style="color:#FF0000">Indiquer Identification / Mot de passe !</span>');
	} else {
		$.post(
			'<?php echo site_url("auth/logout");?>',
			function (zReponse2)
			{
				
				$.post(
					'<?php echo site_url("auth/login");?>',
					{ 
					identity: identity,
					password: password
					},
					function (zReponse2)
					{
						document.location = '<?php echo site_url("auth/login");?>';
				   });
				
		   });
	}
	
}

</script>


<p><img src="<?php echo GetImagePath("front/"); ?>/fidelity/img_la_carte.png" width="312" height="97" alt="lacarte" /></p>

<table width="280" border="0" cellpadding="5" cellspacing="10" style="text-align:center" align="center">
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/fidelity/card_enregistrement.png" width="278" height="68" alt="card" /></td>
  </tr>
  <tr>
    <td class="tdjaune"><a href="<?php echo site_url('front/fidelity');?>" class="link_btn_jaune">Retour menu</a></td>
  </tr>
  
  <tr>
  	<td>Nous vous remercions d'avoir choisi la carte Privicarte</td>
  </tr>
  <tr>
  	<td>LA CARTE FIDELITE UNIQUE ET DEPARTEMENTALE SUR LES ALPES-MARITIMES &amp; MONACO</td>
  </tr>
  <tr>
  	<td><span id="span_check_num_card_1"></span></td>
  </tr>
  <tr>
  	<td>
    <form action="" method="post" name="frmverificationcard" id="frmverificationcard">
    <input type="text" name="num_card_verification" id="num_card_verification" />
  	  <input type="button" name="submit_verification" id="submit_verification" value="Envoyer"  onclick="javascript:check_num_card();"/>
      </form></td>
  </tr>
  <tr>
  	<td><span id="span_check_num_card_2"></span></td>
  </tr>
  <tr>
  	<td>Si vous avez déjà un compte en qualité de particulier sur le site du magazine Proximité, nous vous demandons uniquement de vous identifier</td>
  </tr>
   <form action="<?php echo site_url('auth/login');?>" id="frm_fidelity_customer_login" name="frm_fidelity_customer_login" method="post" accept-charset="utf-8">
  <tr>
  	<td>Mail d'identification
  	  <input type="text" name="identity" value="" id="identity"  /> </td>
  </tr>
  <tr>
  	<td>Mot de passe 
  	  <input type="password" name="password" value="" id="password"  /></td>
  </tr>
  <tr>
  	<td><span id="span_customer_login"></span></td>
  </tr>
  <tr>
    <td class="tdjaune"><a href="javascript:void(0);" onclick="javascript:fidelity_reconnexion();" class="link_btn_jaune">Validation</a></td>
  </tr>
  </form>
  <tr>
  	<td>Si vous n'avez pas encore de compte, Nous vous invitons à vous inscrire immédiatement ...</td>
  </tr>
  
  
  
  
  <tr>
    <td class="tdjaune"><a href="<?php echo site_url('front/particuliers/inscription');?>" class="link_btn_jaune">Accès au formulaire d'inscription</a></td>
  </tr>
</table>


</div>


<?php 
if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
	$this->load->view("fidelity/includes/footer_mini_2_mobile");
} else { 
	$this->load->view("fidelity/includes/footer_mini_2");
} 
?>