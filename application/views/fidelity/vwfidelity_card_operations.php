<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("frontAout2013/includes/header_mini_2", $data); ?>

<div style="text-align:center;">

<?php $this->load->view("fidelity/button_css"); ?>

<script type="text/javascript">
$(function(){
	
	$("#link_btn_tampon_submit").click(function(){
		var id_card = $("#id_card").val();
		var id_bonplan = $("#id_bonplan").val();
		var id_commercant = $("#id_commercant").val();
		var id_user = $("#id_user").val();
		
		$('#span_card_tampon').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
		
		$.post(
			'<?php echo site_url("front/fidelity/card_tampon_check_client");?>',
			{ 
			id_card: id_card,
			id_bonplan: id_bonplan,
			id_commercant: id_commercant,
			id_user: id_user
			},
			function (zReponse_tampon)
			{
				$('#span_card_tampon').html(zReponse_tampon);
		   });
	});
	
	
	$("#link_btn_capital_submit").click(function(){
		var id_card = $("#id_card").val();
		var id_bonplan = $("#id_bonplan").val();
		var id_commercant = $("#id_commercant").val();
		var id_user = $("#id_user").val();
		
		$('#span_card_capital').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
		
		$.post(
			'<?php echo site_url("front/fidelity/card_capital_check_client");?>',
			{ 
			id_card: id_card,
			id_bonplan: id_bonplan,
			id_commercant: id_commercant,
			id_user: id_user
			},
			function (zReponse_capital)
			{
				$('#span_card_capital').html(zReponse_capital);
		   });
	});
	
});

function btn_card_tampon_oui(){
	var id_card = $("#id_card").val();
	var id_bonplan = $("#id_bonplan").val();
	var id_commercant = $("#id_commercant").val();
	var id_user = $("#id_user").val();
	
	$('#span_card_tampon').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
	
	$.post(
		'<?php echo site_url("front/fidelity/card_tampon_client_add");?>',
		{ 
		id_card: id_card,
		id_bonplan: id_bonplan,
		id_commercant: id_commercant,
		id_user: id_user
		},
		function (zReponse_tampon)
		{
			document.location = '<?php echo site_url("front/fidelity/card_tampon_client_validate");?>'+"/"+zReponse_tampon;
			//alert(zReponse_tampon);
	   });
}

function btn_card_tampon_non(){
		$('#span_card_tampon').html('');
}


function btn_card_capital_oui(){
	var id_card = $("#id_card").val();
	var id_bonplan = $("#id_bonplan").val();
	var id_commercant = $("#id_commercant").val();
	var id_user = $("#id_user").val();
	var card_capital_value = $("#card_capital_value").val();
	
	$('#span_card_capital').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
	
	if (card_capital_value != "" && !isNaN(card_capital_value)) {
		
		$.post(
			'<?php echo site_url("front/fidelity/card_capital_client_add");?>',
			{ 
			id_card: id_card,
			id_bonplan: id_bonplan,
			id_commercant: id_commercant,
			id_user: id_user,
			card_capital_value: card_capital_value
			},
			function (zReponse_capital)
			{
				document.location = '<?php echo site_url("front/fidelity/card_capital_client_validate");?>'+"/"+zReponse_capital;
				//alert(zReponse_capital);
		   });
		   
	} else {
		
		$('#span_card_capital').html('<strong style="color:#FF0000;">Veuillez ajouter un montant valide !</strong>');
		
	}
	
}

function btn_card_capital_non(){
		$('#span_card_capital').html('');
}

</script>



<p><img src="<?php echo GetImagePath("front/"); ?>/fidelity/img_la_carte.png" width="312" height="97" alt="lacarte" /></p>

<table width="280" border="0" cellpadding="5" cellspacing="10" style="text-align:center" align="center">
  <tr>
    <td><strong>Client : <?php if (isset($oUser) && is_object($oUser)) echo $oUser->Nom.' '.$oUser->Prenom ?></strong></td>
  </tr>
  <tr>
    <td class="tdjaune"><a href="javascript:void(0);" class="link_btn_jaune">Bon Plan</a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="tdjaune"><a href="javascript:void(0);" class="link_btn_jaune" id="link_btn_tampon_submit">Programme "Coups de tampons"</a></td>
  </tr>
  <tr>
    <td class="tdjaune">
    <form method="post" action="" name="" id="">
    <input name="id_card" id="id_card" type="hidden" value="<?php if (isset($oCard) && is_object($oCard)) echo $oCard->id; ?>" />
    <input name="id_bonplan" id="id_bonplan" type="hidden" value="<?php if (isset($oBonplan) && is_object($oBonplan)) echo $oBonplan->bonplan_id; ?>" />
    <input name="id_commercant" id="id_commercant" type="hidden" value="<?php if (isset($id_commercant)) echo $id_commercant; ?>" />
    <input name="id_user" id="id_user" type="hidden" value="<?php if (isset($oUser) && is_object($oUser)) echo $oUser->IdUser; ?>" class="input280" disabled="disabled"/>
    <input name="card_tampon_value" id="card_tampon_value" type="text" value="<?php if (isset($oTampon) && is_object($oTampon)) echo $oTampon->titre; ?>" class="input280" disabled="disabled"/>
    </form>
    </td>
  </tr>
  <tr>
    <td><span id="span_card_tampon"></span></td>
  </tr>
  <tr>
    <td class="tdjaune"><a href="javascript:void(0);" class="link_btn_jaune" id="link_btn_capital_submit">Programme "Capitalisation"</a></td>
  </tr>
  <tr>
    <td class="tdjaune">
    <form method="post" action="" name="" id="">
    <span>Montant d'achat à capitaliser (en €)</span><br/>
    <input name="card_capital_value" id="card_capital_value" type="text"  class="input280"/>
    </form>
    </td>
  </tr>
  <tr>
    <td><span id="span_card_capital"></span></td>
  </tr>
  <tr>
    <td class="tdjaune"><a href="javascript:void(0);" class="link_btn_jaune">Enregistrement d'une "Carte cadeau"</a></td>
  </tr>
  <tr>
    <td class="tdjaune"><span>Numéro de la carte</span><br/><input name="card_cadeau_number" id="card_cadeau_number" type="text"  class="input280"/></td>
  </tr>
  <tr>
    <td class="tdjaune"><span>Montant de la carte</span><br/><input name="card_cadeau_montant" id="card_cadeau_montant" type="text"  class="input280"/></td>
  </tr>
  <tr>
    <td class="tdjaune"><span>Carte valable jusqu'au</span><br/><input name="card_cadeau_expir" id="card_cadeau_expir" type="text"  class="input280"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="tdjaune"><a href="javascript:void(0);" class="link_btn_jaune">Détails fiche client</a></td>
  </tr>
  <tr>
    <td class="tdjaune"><a href="<?php echo site_url('front/fidelity'); ?>" class="link_btn_jaune">Retour menu</a></td>
  </tr>
</table>


</div>


<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>