<?php $data["zTitle"] = 'Accueil' ?>
<?php 
if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
	$this->load->view("fidelity/includes/header_mini_2_mobile", $data);
} else { 
	$this->load->view("fidelity/includes/header_mini_2", $data);
} 
?>

<script type="text/javascript">

jQuery(document).ready(function() {
		
		
		jQuery("#capitalsubmit").click(function(){
			
            var txtError = "";
			
            var date_debut = jQuery("#date_debut").val();
            if(date_debut=="" || date_debut==null) {
                txtError += "- Veuillez indiquer date_debut.<br/>";
            }
			
			var date_fin = jQuery("#date_fin").val();
            if(date_fin=="" || date_fin==null) {
                txtError += "- Veuillez indiquer date_fin.<br/>";
            }
			
			var description = jQuery("#description").val();
            if(description=="" || description==null) {
                txtError += "- Veuillez indiquer description.<br/>";
            }
			
			var remise_value = jQuery("#remise_value").val();
            if(remise_value=="" || remise_value==null) {
                txtError += "- Veuillez indiquer remise_value.<br/>";
            }
			
			var montant = jQuery("#montant").val();
            if(montant=="" || montant==null) {
                txtError += "- Veuillez indiquer montant.<br/>";
            }
			
			if(txtError == "") {
                jQuery("#frmcapital").submit();
            } else {
				jQuery("#div_error_frmcapital").html("Tous les champs sont obligatoires !");
			}
			
			
		});
		
});



$(function() {
	$("#date_debut").datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:2020'
	});
	$("#date_fin").datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:2020'
	});
});
		
</script>


<div style="text-align:center;">


<p><img src="<?php echo GetImagePath("front/"); ?>/fidelity/img_la_carte.png" width="312" height="97" alt="lacarte" /></p>

<p style="font-family:Arial, Helvetica, sans-serif;
	font-size:16px;
	font-weight:bold;
	text-decoration:none;
	color:#000;">Programme capitalisation</p>


<div style="margin-top:20px; text-align:center;">
<form action="<?php echo site_url('front/fidelity/editcapital'); ?>" method="post" name="frmcapital" id="frmcapital">
<input name="capital[id]" id="id" type="hidden" value="<?php if (isset($oCapital) && is_object($oCapital)) echo $oCapital->id; else echo "0";?>" />
<table width="280" border="0" cellpadding="5" style="text-align:left;" align="center">
  <tr>
    <td><strong>Date de début</strong></td>
  </tr>
  <tr>
    <td>
      <input type="text" name="capital[date_debut]" id="date_debut" value="<?php if (isset($oCapital) && is_object($oCapital)) echo convert_Sqldate_to_Frenchdate($oCapital->date_debut);?>" /></td>
  </tr>
  <tr>
    <td><strong>Date de fin</strong></td>
  </tr>
  <tr>
    <td><input type="text" name="capital[date_fin]" id="date_fin" value="<?php if (isset($oCapital) && is_object($oCapital)) echo convert_Sqldate_to_Frenchdate($oCapital->date_fin);?>" /></td>
  </tr>
  <tr>
    <td><strong>Détail de l'offre de fidélisation</strong></td>
  </tr>
  <tr>
    <td><textarea name="capital[description]" id="description" cols="30" rows="5"><?php if (isset($oCapital) && is_object($oCapital)) echo $oCapital->description;?></textarea></td>
  </tr>
  <tr>
    <td><strong>Montant de la remise en euros</strong></td>
  </tr>
  <tr>
    <td><input type="text" name="capital[remise_value]" id="remise_value" value="<?php if (isset($oCapital) && is_object($oCapital)) echo $oCapital->remise_value;?>"/></td>
  </tr>
  <tr>
    <td><strong>Montant total des achats en euros</strong></td>
  </tr>
  <tr>
    <td><input type="text" name="capital[montant]" id="montant"  value="<?php if (isset($oCapital) && is_object($oCapital)) echo $oCapital->montant;?>"/></td>
  </tr>
  <tr>
  	<td><div id="div_error_frmcapital" style="color:#FF0000;"></div></td>
  </tr>
  <tr>
    <td style="text-align:center;"><a href="javascript:void(0);" name="capitalsubmit" id="capitalsubmit"><img src="<?php echo GetImagePath("front/"); ?>/fidelity/valider.png" width="260" height="43" alt="valider" /></a></td>
  </tr>
  <tr>
    <td style="text-align:center;"><a href="javascript:void(0);" onclick="javascript:document.location='<?php echo site_url('front/fidelity/my_parameters');?>'"><img src="<?php echo GetImagePath("front/"); ?>/fidelity/annuler.png" width="260" height="43" alt="annuler" /></a></td>
  </tr>
  
</table>
</form>
</div>



</div>


<?php 
if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
	$this->load->view("fidelity/includes/footer_mini_2_mobile");
} else { 
	$this->load->view("fidelity/includes/footer_mini_2");
} 
?>