<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Basic QR-code reader example</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="<?php echo GetJsPath("front/") ; ?>/js_qrcode/css/reset.css">
<link rel="stylesheet" href="<?php echo GetJsPath("front/") ; ?>/js_qrcode/css/styles.css">
<script src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/jquery.min.js"></script>
<script src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/photobooth.js"></script>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/grid.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/version.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/detector.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/formatinf.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/errorlevel.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/bitmat.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/datablock.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/bmparser.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/datamask.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/rsdecoder.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/gf256poly.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/gf256.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/decoder.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/qrcode.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/findpat.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/alignpat.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/qr/databr.js"></script>

<script src="<?php echo GetJsPath("front/") ; ?>/js_qrcode/js/effects.js"></script>

<style type="text/css">
body  {
	margin:0px;
	padding:0px;
	background-color:#003366;
	font-family: "Arial",sans-serif;
    font-size: 12px;
    line-height: 1.25em;
	margin:0px;
	padding:0px;
	background-repeat: repeat;
	background-position:center top;
	/*background-image: url(<?php //echo GetImagePath("front/"); ?>/wpimages2013/wp30e2106b_06.png);*/
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/wp9e1de8e1_05_06.jpg);
}
img {
	border:none;
}
.contener2013 {
	height: auto;
	width: 650px;
	margin-right: auto;
	margin-left: auto;
	position: relative;
	margin-top: 0px;
}
.header2013 {
	/*<?php // if (CURRENT_SITE == "agenda"){ ?>
		<?php // if (isset($current_page) && $current_page == "espaceconsommateurs"){ ?>
		background-image: url(<?php // echo GetImagePath("front/"); ?>/agenda/head_mini_agenda_espcons_bg.png);
		<?php // } else { ?>
		background-image: url(<?php // echo GetImagePath("front/"); ?>/agenda/head_mini_agenda_bg.png);
		<?php // } ?>
	<?php // } else { ?>
	background-image: url(<?php // echo GetImagePath("front/"); ?>/wpimages2013/head_mini_2_bg.png);
	<?php // } ?>
	background-repeat: no-repeat;
	background-position: center top;*/
	float: left;
	height: 185px;
	width: 650px;
	position: relative;
}
.maincontener2013 {
	background-color:#FFFFFF;
	float: left;
	height: auto;
	position: relative;
	width: 650px;
}
#table_form_inscriptionpartculier .td_part_form {
	text-align:left;
	font-weight:bold;
}
#table_form_inscriptionpartculier {
	text-align:center;
}
.table_club{
	
}

</style>

<script type="text/javascript">
	//variables globales
	gCONFIG = new Array();
	gCONFIG["BASE_URL"] = "<?php echo base_url(); ?>";
	gCONFIG["SITE_URL"] = "<?php echo site_url(); ?>";
</script>
</head>

<body>
<div class="contener2013">
	<div class="header2013">
    <?php $data["zTitle"] = 'Accueil' ?>
	<?php $this->load->view("frontAout2013/includes/header_content_mini_2", $data); ?>
    </div>
    <div class="maincontener2013" style="padding-bottom:1500px;">

<div style="text-align:center; width:650fpx;">

<?php $this->load->view("fidelity/button_css"); ?>

<script type="text/javascript">
function check_scan_card(qrcode_value=""){
	
	var zErreur = "" ;
	var num_card_scan = "";
	
	if (qrcode_value!="") {
		num_card_scan = qrcode_value;
	} else {
		num_card_scan = $("#num_card_scan").val();
	}
	
	$('#span_card_scan').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
	
	$.post(
		'<?php echo site_url("front/fidelity/check_scan_card");?>',
		{ 
		num_card_scan: num_card_scan
		},
		function (zReponse)
		{
			if (zReponse == "1") {
				$('#span_card_scan').html('<span style="color:#090"><p><strong>Votre carte est Valide</strong></p><p><img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/></p></span>');
				
				
				$.post(
						'<?php echo site_url("front/fidelity/getuser_card_scan");?>',
						{ 
						num_card_scan: num_card_scan
						},
						function (zReponse2)
						{
							document.location = '<?php echo site_url("front/fidelity/fidelity_card_operations/");?>'+"/"+zReponse2;
					    });
				
				
				
			} else if (zReponse == "2") {
				$('#span_card_scan').html('<span style="color:#FF0000"><strong>Votre carte est répertorié, mais n\'est pas activé !</strong></span>');
			} else if (zReponse == "3") {
				$('#span_card_scan').html('<span style="color:#FF0000"><strong>Votre carte n\'est pas encore liée à un compte Client !</strong></span>');
			} else {
				$('#span_card_scan').html('<span style="color:#FF0000"><strong>Cette carte est invalide !</strong></span>');
			}
	   });
	
}


</script>

<table width="650" border="0" cellpadding="" cellspacing="0" align="center" style="margin-top:20px;text-align:center;">
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/fidelity/img_la_carte.png" width="312" height="97" alt="lacarte" /></td>
  </tr>
</table>

<table width="650" border="0" cellpadding="5" cellspacing="10" align="center" style="margin-top:20px;text-align:center;">
  <tr>
    <td><a href="<?php echo site_url('front/fidelity'); ?>" class="link_btn_jaune"><img src="<?php echo GetImagePath("front/"); ?>/fidelity/retour.png" width="260" height="43" alt="retour"></a></td>
  </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_club" style="text-align:center; margin-top:0px; padding-top:0px;" align="center">




  <tr>
    <td>
    <div class="pageWrapper">
    
        <div class="boxWrapper">
            <div id="example"></div>
        </div>
    
        <div class="button">
            <a id="button">Scan QR code</a>
        </div>
    
        <div class="boxWrapper auto">
            <div id="hiddenImg"></div>
            <div id="qrContent"><p>Voir ici le contenu.</p></div>
        </div>
    
    </div>
    </td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  
  
  
  <tr>
    <td align="center" style="text-align:center;">
    <table width="650" border="0" cellpadding="5" cellspacing="10" style="text-align:center" align="center">
  <tr>
    <td><span id="span_card_scan"><?php if (isset($mssg) && $mssg!="") echo $mssg; ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Saisir N° Carte</td>
  </tr>
  <tr>
    <td>
    <form action="<?php echo site_url('front/fidelity/card_scan'); ?>" method="post" name="frmscancard" id="frmscancard">
    <input type="text" id="num_card_scan" name="num_card_scan" value="" style="background-color:#000; border: 1px;border-color:#000; width:200px; color:#FFF;"/>
    </form>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td style="margin-top:20px;"><a href="javascript:void(0);" onclick="javascript:check_scan_card();" class="link_btn_jaune"><img src="<?php echo GetImagePath("front/"); ?>/fidelity/valider.png" width="260" height="43" alt="valider"></a></td>
  </tr>
</table>
    </td>
  </tr>
  
</table>







</div>


   </div>
</div>
</body>
</html>