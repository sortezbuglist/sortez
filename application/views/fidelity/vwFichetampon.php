<?php $data["zTitle"] = 'Accueil' ?>
<?php 
if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
	$this->load->view("fidelity/includes/header_mini_2_mobile", $data);
} else { 
	$this->load->view("fidelity/includes/header_mini_2", $data);
} 
?>

<script type="text/javascript">

jQuery(document).ready(function() {
		
		
		jQuery("#tamponsubmit").click(function(){
			
            var txtError = "";
			
            var date_debut = jQuery("#date_debut").val();
            if(date_debut=="" || date_debut==null) {
                txtError += "- Veuillez indiquer date_debut.<br/>";
            }
			
			var date_fin = jQuery("#date_fin").val();
            if(date_fin=="" || date_fin==null) {
                txtError += "- Veuillez indiquer date_fin.<br/>";
            }
			
			var titre = jQuery("#titre").val();
            if(titre=="" || titre==null) {
                txtError += "- Veuillez indiquer titre.<br/>";
            }
			
			var description = jQuery("#description").val();
            if(description=="" || description==null) {
                txtError += "- Veuillez indiquer description.<br/>";
            }
			
			var tampon_value = jQuery("#tampon_value").val();
            if(titre=="" || titre==null) {
                txtError += "- Veuillez indiquer tampon_value.<br/>";
            }
			
			if(txtError == "") {
                jQuery("#frmtampon").submit();
            } else {
				jQuery("#div_error_frmtampon").html("Tous les champs sont obligatoires !");
			}
			
			
		});
		
});



$(function() {
	$("#date_debut").datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:2040'
	});
	$("#date_fin").datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:2040'
	});
});
		
</script>


<div style="text-align:center;">

<p><img src="<?php echo GetImagePath("front/"); ?>/fidelity/img_la_carte.png" width="312" height="97" alt="lacarte" /></p>

<p style="font-family:Arial, Helvetica, sans-serif;
	font-size:16px;
	font-weight:bold;
	text-decoration:none;
	color:#000;">Programme coups de tampon</p>


<div style="margin-top:20px; text-align:center;">
<form action="<?php echo site_url('front/fidelity/edittampon'); ?>" method="post" name="frmtampon" id="frmtampon">
<input name="tampon[id]" id="id" type="hidden" value="<?php if (isset($oTampon) && is_object($oTampon)) echo $oTampon->id; else echo "0";?>" />
<table width="280" border="0" cellpadding="5" style="text-align:left;" align="center">
  <tr>
    <td><strong>Date de début</strong></td>
  </tr>
  <tr>
    <td>
      <input type="text" name="tampon[date_debut]" id="date_debut" value="<?php if (isset($oTampon) && is_object($oTampon)) echo convert_Sqldate_to_Frenchdate($oTampon->date_debut);?>" /></td>
  </tr>
  <tr>
    <td><strong>Date de fin</strong></td>
  </tr>
  <tr>
    <td><input type="text" name="tampon[date_fin]" id="date_fin" value="<?php if (isset($oTampon) && is_object($oTampon)) echo convert_Sqldate_to_Frenchdate($oTampon->date_fin);?>" /></td>
  </tr>
  <tr>
    <td><strong>Créer le titre</strong></td>
  </tr>
  <tr>
    <td><input type="text" name="tampon[titre]" id="titre"  value="<?php if (isset($oTampon) && is_object($oTampon)) echo $oTampon->titre;?>"/></td>
  </tr>
  <tr>
    <td><strong>Détail de l'offre de fidélisation</strong></td>
  </tr>
  <tr>
    <td><textarea name="tampon[description]" id="description" cols="30" rows="5"><?php if (isset($oTampon) && is_object($oTampon)) echo $oTampon->description;?></textarea></td>
  </tr>
  <tr>
    <td><strong>Objectif à atteindre</strong></td>
  </tr>
  <tr>
    <td><input type="text" name="tampon[tampon_value]" id="tampon_value" value="<?php if (isset($oTampon) && is_object($oTampon)) echo $oTampon->tampon_value;?>"/></td>
  </tr>
  
  <tr>
  	<td><div id="div_error_frmtampon" style="color:#FF0000;"></div></td>
  </tr>
  <tr>
    <td style="text-align: center;"><a href="javascript:void(0);" name="tamponsubmit" id="tamponsubmit"><img src="<?php echo GetImagePath("front/"); ?>/fidelity/valider.png" width="260" height="43" alt="valider" /></a></td>
  </tr>
  <tr>
    <td style="text-align: center;"><a href="javascript:void(0);" onclick="javascript:document.location='<?php echo site_url('front/fidelity/my_parameters');?>'"><img src="<?php echo GetImagePath("front/"); ?>/fidelity/annuler.png" width="260" height="43" alt="annuler" /></a></td>
  </tr>
</table>
</form>
</div>



</div>


<?php 
if (($is_mobile_ipad == false && $is_mobile == true) || $is_robot == true) { 
	$this->load->view("fidelity/includes/footer_mini_2_mobile");
} else { 
	$this->load->view("fidelity/includes/footer_mini_2");
} 
?>