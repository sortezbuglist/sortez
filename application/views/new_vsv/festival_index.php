<?php $data["zTitle"] = 'Festivals'; ?>
<?php $data["slide"] = 'wp41514793_05_06.jpg'; ?>

<?php
$thisss =& get_instance();
$thisss->load->library('session');
$main_width_device = $thisss->session->userdata('main_width_device');
$main_mobile_screen = false;
if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device) <= 991) {
    $main_mobile_screen = true;
} else {
    $main_mobile_screen = false;
}
?>

<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>
    <?php $this->load->view("new_vsv/includes/main_header", $data); ?>
    <?php $this->load->view("new_vsv/includes/festival_ready", $data); ?>

    <?php if ($main_mobile_screen == true) $this->load->view("new_vsv/includes/festival_banner", $data); ?>

    <div class="container" style="display: block;margin: auto;">
        <?php $this->load->view("new_vsv/includes/header_top", $data); ?>
    </div>
    <div class="container">
        <?php $this->load->view("new_vsv/includes/header_right", $data); ?>
    </div>
    <!--<div class="d-none d-lg-block d-xl-block">
        <?php// $this->load->view("new_vsv/includes/main_banner", $data); ?>
    </div> !-->
    <?php $this->load->view("new_vsv/includes/main_rubrique_menu", $data); ?>




    <?php if ($main_mobile_screen == false) $this->load->view("new_vsv/includes/festival_banner", $data); ?>
    <?php //$this->load->view("new_vsv/includes/main_banner_pub_link", $data); ?>
    <?php $this->load->view("new_vsv/includes/main_banner_vos_pub", $data); ?>

    <div class="container">
        <div class="row main_body_title justify-content-center pt-3">
            <!--<h1>JE DÉCOUVRE MA VILLE</h1>
            <h2>SORTIES, LOISIRS, CULTURE, ARTISANAT, COMMERCES, PATRIMOINE...</h2>-->
        </div>
    </div>

<?php } ?>


<?php $this->load->view("new_vsv/festival_list", $data); ?>

<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list']=='1') {} else { ?>
    <?php $this->load->view('new_vsv/includes/footer_front'); ?>
    <?php $this->load->view("new_vsv/includes/main_footer_link", $data); ?>
    <?php $this->load->view("new_vsv/includes/main_footer_copyright", $data); ?>
    <?php $this->load->view("new_vsv/includes/main_footer", $data); ?>
<?php } ?>