
<div class="container container_select_ville">
    <div id="bgLayers_comp-kldhbtwo">
		<img src="https://static.wixstatic.com/media/5fa146_cf1797a97a4a42c88c259684b202f61b~mv2.jpg/v1/fill/w_981,h_326,al_c,q_80,usm_0.66_1.00_0.01/Fotolia_60439715_L.webp" alt="main.jpg" style="width: 930px; height: 326px; object-fit: cover; object-position: 50% 50%;">
	</div>
	</br>    
</div>

<div class="container container_select_ville">
    <div class="row" >
	    <div id="comp-kldglvln">
            <h2>Le fonctionnement des acteurs professionnels...</h2>
            <h3>Collectivit&eacutes, associations, commer&ccedilants, artisans, Pme & PMI</h3>
            <ul class="font_8">
                <li><p class="font_8"><span>
                   <p> Chaque site local "vivresaville.fr" ouvre automatiquement et gratuitement le r&eacutef&eacuterencement des acteurs professionnels du territoire concern&eacute (commune, intercommunalit&eacute, communaut&eacute de communes) mais aussi venant de nos sites f&eacuted&eacuterateurs.</p></span></li>
	        </ul>
	    </div> 

	    <div class="col-md-2 "></div>

        <div class="col-md-3 ">
		    <div id="ordi3">
       			<img src="https://static.wixstatic.com/media/5fa146_dfb3342edf4a4be2abce9cdcaa01184f~mv2.png/v1/fill/w_335,h_257,al_c,q_85,usm_0.66_1.00_0.01/soutenons-ecran-sortez.webp" alt="soutenons-ecran-sortez.png" style="width:335px;height:257px;object-fit:cover;object-position:50% 50%">
       		</div>
    	</div>  

   	    <div class="col-md-1 ">
	     	<div id="acces3">
			    <a data-testid="linkElement" href="https://www.magazine-sortez.org" target="_blank" rel="noopener" class="btn-change" style="text-decoration:none"><span class="_3fUtn">Acc&egrave;s</span></a>
		    </div>
   	    </div>

   	    <div class="col-md-1 ">
    	    <div id="acces4">
       		    <a data-testid="linkElement" href="https://www.soutenonslecommercelocal.fr" target="_blank" rel="noopener" class="btn-change" style="text-decoration:none"><span class="_3fUtn">Acc&egrave;s</span></a>
            </div>
        </div>

        <div class="col-md-3 ">
            <div id="ordi4">
             	<img src="https://static.wixstatic.com/media/5fa146_0688aeed266d486088199fabbca428cd~mv2.png/v1/fill/w_335,h_257,al_c,q_85,usm_0.66_1.00_0.01/mmmmmmmmmmmmmm.webp" alt="mmmmmmmmmmmmmm.png" style="width:335px;height:257px;object-fit:cover;object-position:50% 50%">
       		</div>
   	    </div>

   	    <div class="col-md-2 "></div>

	    <div id="comp-kldglvln">
	        <ul class="font_8">
		        <li><p class="font_8">
                    <span>
		                <p> Les professionnels acc&egravedent &agrave la cr&eacuteation ou &agrave la modification de leur landing-page personnelle organis&eacutee comme un v&eacuteritable site web vitrine ; </p>
		            </span>
                </li>
		        <li><p class="font_8">
                    <span>
		                <p> Puis, sur un compte personnel et s&eacutecuris&eacute, ils enrichissent leur abonnement de base en souscrivant &agrave; des modules marketings compl&eacute;mentaires pour booster leur activit&eacute; sur le web : </p>
		            </span>
                </li>
            </ul>

	    <ol class="font_8">
		    <li><p class="font_8"><span>
			    Le module actualit&eacute (articles, &eacute;v&eacute;nements) ; </p></span>
		    </li>
		    <li><p class="font_8"><span>
			    <p> la boutique en ligne avec le module PAYPAL de paiement en ligne ; </p></span>
		    </li>
		    <li><p class="font_8"><span>
			    <p> la gestion des bons plans et des conditions de fid&eacute;lisation ; </p></span>
		    </li>
		    <li><p class="font_8"><span>
			    <p>une carte de fid&eacute;lit&eacute; d&eacute;mat&eacute;rialis&eacute;e sur les mobiles avec son application Android ;</p></span>
		    </li>
		    <li><p class="font_8"><span>
			    <p>la gestion des plats du jour et la gestion des r&eacute;servations pour les restaurants ;</p></span>
		    </li>
		    <li><p class="font_8"><span>
			    <p>le module de r&eacute;servation pour les G&icirctes et chambres d'h&ocirctes ;</p></span>
		    </li>
		    <li><p class="font_8"><span>
			    <p>un site web professionnel vitrine complet avec le pack de r&eacute;f&eacute;rencement Google</p></span>
		    </li>
		    <li><p class="font_8"><span>
			    <p>sur devis, nous r&eacute;alisons une vid&eacute;o commerciale, nous g&eacute;rons votre pr&eacute;sence sur les r&eacute;seaux sociaux ;</p></span>
		    </li>
		    <li><p class="font_8"><span>
			    <p>sur devis, nous aidons &agrave la cr&eacute;ation des pages.</p></span>
		    </li>
	    </ol>
	  </div>
    </div>
</div>

<style type="text/css">
	#ordi3{
	    margin-left:-150px;
	        }
	  #ordi4{
	    margin-left:70px;
	        }

	#acces3{
    		background-color:transparent;
    		position: absolute;
    		top: -10px;
    		width: 133px;
    		left: -50px;
   
    }

 	#acces4{
    		background-color:transparent;
    		position: absolute;
    		top: 120px;
    		right: -70px;
    		width: 140px;
	}

	#li{
		text-align:center;
	}

	h2.comp-kldglvln {
		font-size: 2rem;
		font-style: italic;
		color: #E80EAE;
		font-size: 40px;
		line-height: 0.9em;
		text-align: center;
	}

	h3.comp-kldglvln {
		font-size: 2rem;
		font-style: italic;
		color: #E80EAE;
		font-size: 25px;
		line-height: 0.9em;
		text-align: center;
	}

	
</style>