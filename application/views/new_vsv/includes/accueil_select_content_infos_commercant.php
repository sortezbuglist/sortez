<div class="container accueil_select_content_2 text-center pt-3">
    <div class="row">
        <div class="col-12">
            <p>
                <a href="<?php echo site_url('vivresaville/ListVille'); ?>"
                   class="vsv_accueil_btn vsv_select_link plus_infos_link">RETOUR PAGE D'ACCUEIL...</a>
            </p>
            <p class="title_1 pt-5 pl-5 pr-5">
                VOTRE VILLE OU TERRITOIRE SONT DÉJÀ PRÉSENTS !…<br/>
                ASSOCIATION, COMMERÇANT, ARTISAN, ADMINISTRATION<br/>
                RÉFÉRENCEZ IMMÉDIATEMENT<br/>
                ET GRATUITEMENT VOTRE STRUCTURE !…
            </p>
            <p class="pt-5">
                <img
                    src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wpa89873e4_06.png"
                    class="mt-0 border-0 img-fluid"/>
            </p>
            <p class="title_3 pb-3">
                Les sites s’adaptent à tous les écrans
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="col p-0 func_discov_case text-center p-5">
                <p class="txt_info_bold pb-5">
                    Découvrez le fonctionnement<br/>
                    de vivresaville.fr
                </p>
                <p>
                    <a href="https://spark.adobe.com/page/9elybiCOnQrOC/" target="_blank"
                      class="vsv_accueil_btn vsv_select_link plus_infos_link w-25">VISUALISEZ</a>
                </p>
            </div>
            <div class="pt-5 yexy-left">
                <p>
                    Nous avons créé un site web multilingue, unique et personnalisé au nom de votre ville ou votre territoire,  il rassemble les informations institutionnelles et privées (annuaire complet, articles, agenda, bons plans, conditions de fidélisation, boutiques en ligne)…
                </p>
                <p>
                    Les données sont déposées par les acteurs administratifs, associatifs et économiques de votre cité.
                </p>
                <p>
                    Chaque acteur  (association, commerçant, artisan, PMe)  bénéficie d’un kit de démarrage clés en mains et gratuit…
                </p>
            </div>
        </div>
    </div>
    <div class="row pt-5 txt_infos_pro_offers text-center m-0">
        <div class="col-12">
            <p class="title_1 txt_rose_color title_partenaire p-5">
                DÉCOUVREZ NOTRE PACK DE DÉMARRAGE GRATUIT
            </p>
        </div>
        <div class="row m-0 pb-5">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-12 info_pro_img">
                        <img
                                src="<?php echo base_url(); ?>wpimages_vsv/wp41145ba2_06.png"
                                class="mt-0 border-0 img-fluid"/>
                    </div>
                    <div class="col-12 info_pro_title">
                        un abonnement gratuit
                    </div>
                    <div class="col-12 info_pro_txt">
                        L'inscription des professionnels<br/>
                        est gratuite avec l’ouverture<br/>
                        d’un compte personnel et sécurisé …
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-12 info_pro_img">
                        <img
                                src="<?php echo base_url(); ?>wpimages_vsv/wp7f61e81b_06.png"
                                class="mt-0 border-0 img-fluid"/>
                    </div>
                    <div class="col-12 info_pro_title">
                        des champs préconfigurés
                    </div>
                    <div class="col-12 info_pro_txt">
                        La présence d'une interface intuitive avec l’intégration de vos données sans connaissance techniques…
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-12 info_pro_img">
                        <img
                                src="<?php echo base_url(); ?>wpimages_vsv/wp87f88cf8_06.png"
                                class="mt-0 border-0 img-fluid"/>
                    </div>
                    <div class="col-12 info_pro_title">
                        une landing page
                    </div>
                    <div class="col-12 info_pro_txt">
                        Après validation, elles rejoignent<br/>
                        une page de présentation  organisée comme un site web. (Détail plus bas)
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-0 pb-5">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-12 info_pro_img">
                        <img
                                src="<?php echo base_url(); ?>wpimages_vsv/wp969019f1_06.png"
                                class="mt-0 border-0 img-fluid"/>
                    </div>
                    <div class="col-12 info_pro_title">
                        le dépôt de vos évenements
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-12 info_pro_img">
                        <img
                                src="<?php echo base_url(); ?>wpimages_vsv/wpcc3f3fd5_06.png"
                                class="mt-0 border-0 img-fluid"/>
                    </div>
                    <div class="col-12 info_pro_title">
                        le dépôt de vos articles
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-12 info_pro_img">
                        <img
                                src="<?php echo base_url(); ?>wpimages_vsv/wpb2b6f44d_06.png"
                                class="mt-0 border-0 img-fluid"/>
                    </div>
                    <div class="col-12 info_pro_title">
                        le dépôt de vos annonces
                    </div>
                </div>
            </div>
        </div>
        <div class="row m-0 pb-5">
            <div class="col text-center">
                Avec le pack de démarrage, vous disposez gratuitement d'un quota maximum de 5 événement, 5 articles et 5 annonces…
                Après validatiion, vos données rejoignent  votre page de présentation et nos annuaires correspondants.
                Par la suite vous augmentez votre quota de 25 ou de 100 annonces en souscrivant un abonnement payant…
                Vous gérez, modifiez, supprimez vos annonces autant de fois que vous le désirez tout en respectant le quota initial.
            </div>
        </div>
        <div class="row m-0 pb-5">
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-12 info_pro_img">
                        <img
                                src="<?php echo base_url(); ?>wpimages_vsv/wp26a4eb91_06.png"
                                class="mt-0 border-0 img-fluid"/>
                    </div>
                    <div class="col-12 info_pro_title">
                        un site web personnalisée
                    </div>
                    <div class="col-12 info_pro_txt">
                        Pour disposer d’un site web personnel,<br/>
                        il vous suffit de créer une adresse URL<br/>
                        d’un simple clic !
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-12 info_pro_img">
                        <img
                                src="<?php echo base_url(); ?>wpimages_vsv/wpdbcc481e_06.png"
                                class="mt-0 border-0 img-fluid"/>
                    </div>
                    <div class="col-12 info_pro_title">
                        L'accès à la mobilité
                    </div>
                    <div class="col-12 info_pro_txt">
                        Vos pages et votre site web s’adaptent<br/>
                        à la mobilité. Vous pouvez créer sur votre Admin un QRCODE personnalisé.
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="col-12 info_pro_img">
                        <img
                                src="<?php echo base_url(); ?>wpimages_vsv/wpab9257db_06.png"
                                class="mt-0 border-0 img-fluid"/>
                    </div>
                    <div class="col-12 info_pro_title">
                        le référencement local
                    </div>
                    <div class="col-12 info_pro_txt">
                        Vos pages et données sont référencées<br/>
                        sur les annuaires correspondants<br/>
                        du site de votre ville)
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <p class="title_1 title_partenaire p-5">
                DÉCOUVREZ LE DÉTAIL DES FONCTIONS À INTÉGRER<br/>
                SUR VOTRE LANDING PAGE  !…
            </p>
        </div>
        <div class="row m-0 pb-5">
            <ul class="text-left">
                <li>La présentation de votre activité.</li>
                <li>La présence de vos horaires.</li>
                <li>La présence d’un plan de localisation</li>
                <li>La présence d’une vidéo</li>
                <li>Le téléchargement d’un document PDF</li>
                <li>La présence d’un formulaire de contact</li>
                <li>La présence d’une galerie photos</li>
                <li>La présence de vos moyens de paiement…</li>
                <li>La présence de vos liens divers :  site web, email, sociaux (Facebook, Twitter, Google+)</li>
                <li>La gestion des favoris et  lien de recommandation</li>
                <li>La présence d’un module de traduction Google</li>
                <li>La fourniture d’un QRCODE personnalisé afin d’accéder à vos pages mobiles</li>
                <li>La création immédiate d’un URL personnalisé en sous-domaine (ex. mon.etablissement.sortez.org)</li>
                <li>La présence d’un module de création de mots clés pour un meilleur réferencement de votre page</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pt-5">
            <p class="pt-5 pb-5 text-center">
                <a href="<?php echo site_url('front/professionnels/inscription');?>" class="vsv_accueil_btn vsv_select_link" style="width: 200px; display: inline; text-transform: uppercase;">Inscription gratuite...</a>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p class="title_1 txt_rose_color title_partenaire pl-5 pr-5 pt-5">
                PLUS D’INFORMATIONS, VISUALISEZ NOS PRÉSENTATIONS !...
            </p>
        </div>
    </div>
    <div class="row accueil_visu_presentation pb-5">
        <div class="col-sm-4 p-3">
            <div class="m-3 p-5 visu_communalite_1">
                Découvrez
                le fonctionnement
                de vivresaville.fr
            </div>
            <div class="text-center mt-5">
                <a href="https://spark.adobe.com/page/9elybiCOnQrOC/" target="_blank" class="vsv_accueil_btn vsv_select_link " style="width: 200px; display: inline;">VISUALISEZ...</a>
            </div>
        </div>
        <div class="col-sm-4 p-3">
            <div class="m-3 p-5 visu_communalite_2">
                Créez et développez
                vos actions
                promotionnelles !
            </div>
            <div class="text-center mt-5">
                <a href="https://spark.adobe.com/page/NmHOPOkea1oJW/" target="_blank" class="vsv_accueil_btn vsv_select_link " style="width: 200px; display: inline;">VISUALISEZ...</a>
            </div>
        </div>
        <div class="col-sm-4 p-3">
            <div class="m-3 p-5 visu_communalite_3">
                Créez un site web
                gratuit en moins
                de quinze minutes !
            </div>
            <div class="text-center mt-5">
                <a href="https://spark.adobe.com/page/Sp9JDPs9b0hSi/" target="_blank" class="vsv_accueil_btn vsv_select_link " style="width: 200px; display: inline;">VISUALISEZ...</a>
            </div>
        </div>
    </div>
</div>