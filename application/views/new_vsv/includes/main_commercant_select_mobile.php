<?php
//LOCALDATA FILTRE
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
$localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
$inputIdCommercant_x = $this_session_localdata->session->userdata('inputIdCommercant_x');
?>
<div class="container main_nav_bar_rubrique_menu main_rubrique_menu d-lg-none d-xl-none">
    <div class="row">
        <nav class="container navbar navbar-toggleable-md navbar-inverse bg-inverse mb-0 w-100 p-0">
            <button class="navbar-toggler navbar-toggler-left collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarsCommercantDefault" aria-controls="navbarsCommercantDefault" aria-expanded="false"
                    id="navbar_button_commercant_link"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><img alt="" src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcon.png" class="img-fluid"></span>
            </button>
            <a class="navbar-brand text-right" id="navbar_brand_communes_link" href="javascript:void(0);" onclick="javascript:click_commercant_menu_btn();">
                <?php
                if(isset($inputIdCommercant_x) && $inputIdCommercant_x != "" && $inputIdCommercant_x != 0 && $inputIdCommercant_x != "0")
                {
                    foreach($tocommercant as $oVille){
                        if ($oVille->IdCommercant == $inputIdCommercant_x){
                            echo $oVille->NomSociete." (".$oVille->nbcommercant.")".'</a>';
                        }
                    }
                } else echo 'Tous les partenaires';
                ?>
            </a>
            <div class="navbar-collapse collapse" id="navbarsCommercantDefault" aria-expanded="false" style="max-height: 400px; overflow-y: scroll;">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active pl-3 pr-3">
                        <a href="javascript:void(0);" onclick="javascript:pvc_select_commercant(0);" class="col nav-link">Tous les partenaires</a>
                    </li>
                    <?php
                    if(isset($tocommercant) && $tocommercant != "" && $tocommercant != 0 && $tocommercant != "0")
                    {
                        foreach($tocommercant as $oVille){ ?>
                            <li class="nav-item  pl-3 pr-3">
                                <a href="javascript:void(0);" onclick="javascript:pvc_select_commercant(<?php echo $oVille->IdCommercant ; ?>);" class="col nav-link ">
                                    <?php echo $oVille->NomSociete." (".$oVille->nbcommercant.")" ; ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php }
                    ?>
                </ul>
            </div>
        </nav>
    </div>
</div>
<script type="text/javascript">
    function pvc_select_commercant(id_commercant){
        $("#inputIdCommercant").val(id_commercant);
        //$("#frmRechercheBonplan").submit();
        $("#frmRecherchePartenaire").submit();
    }
    function click_commercant_menu_btn() {
        $("#navbar_button_communes_link").click();
    }
</script>