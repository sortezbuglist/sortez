<style>
    .content_menu_soutenons a span{
        color: black;
        text-decoration: none;
        font-family: LibreBaskerville-Bold;
        font-style: italic;
        font-size: 1em!important;
    }
    .content_menu_soutenons a{
        color: black;
        text-decoration: none;
        font-family: LibreBaskerville-Bold;
        font-style: italic;
        font-size: 1em!important;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .col-lgss-2{
        -ms-flex: 0 0 13%;
        flex: 0 0 13%;
        max-width: 13%;
    }
    .col-lgss-2_article{
        -ms-flex: 0 0 20%;
        flex: 0 0 20%;
        max-width: 20%;
    }

    /* The container <div> - needed to position the dropdown content */
    .dropdown_menu {
        position: relative;
        display: inline-block;
        padding-bottom: 10px;
        padding-top: 10px;
    }

    /* Dropdown Content (Hidden by Default) */
    .dropdown-content {
        display: none;
        position: absolute;
        left: -55px;
        background-color: white;
        min-width: 255px;
        width: 255px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1000;
    }

    /* Links inside the dropdown */
    .dropdown-content a {
        color: black;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-left: 5px;
        padding-right: 5px;
        text-decoration: none;
        display: block;
        border-top: 1px solid black;
    }

    /* Show the dropdown menu on hover */
    .dropdown_menu:hover .dropdown-content {display: block;}

    /* Change the background color of the dropdown button when the dropdown content is shown */
    .dropdown_menu:hover{display:block;}

    .border_none{
        border-top: unset!important;
    }
    .span_menu{
        color: black;
        text-decoration: none;
        font-family: LibreBaskerville-Bold;
        font-style: italic;
        font-size: 1em!important;
        cursor: pointer;
    }
    .span_menu:hover{
        color: black!important;
    }
</style>
<div class="content_menu_soutenons d-none d-lg-flex d-xl-flex">
<!--    <div class="col-lgss-2" style="border-right: 1px solid #000000">-->
<!--        <a href="--><?php //echo base_url('vivresaville/Accueil')?><!--" class="--><?php //if ($zcontentactive =="accueil"){ echo"active" ;} ?><!--">-->
<!--            Page accueil-->
<!--        </a>-->
<!--    </div>-->
    <div class="col-lgss-2_article" style="padding-top: 10px;padding-bottom: 10px;border-right: 1px solid #000000;" >
        <a href="<?php echo site_url('article')?>" class="<?php if ($zcontentactive =="article"){ echo"active" ;} ?>">
            L'actualité & la revue de presse
        </a>
    </div>
    <div class="col-lgss-2" style="padding-top: 10px;padding-bottom: 10px;border-right: 1px solid #000000;">
        <a href="<?php echo site_url('agenda')?>" class="<?php if ($zcontentactive =="agenda"){ echo"active" ;} ?>">
            L'Agenda
        </a>
    </div>
    <div class="col-lgss-2" style="padding-top: 10px;padding-bottom: 10px;border-right: 1px solid #000000;">
        <a href="<?php echo site_url('annuaire')?>" class="<?php if ($zcontentactive =="annuaire"){ echo"active" ;} ?>">
            Les bonnes adresses
        </a>
    </div>
    <div class="col-lgss-2" style="padding-top: 10px;padding-bottom: 10px;border-right: 1px solid #000000;">
        <a href="<?php echo site_url('Boutique')?>" class="<?php if ($zcontentactive =="boutique"){ echo"active" ;} ?>">
           Boutiques
        </a>
    </div>
    <div class="col-lgss-2" style="padding-top: 10px;padding-bottom: 10px;border-right: 1px solid #000000;">
        <a href="<?php echo site_url('DealAndFidelite')?>" class="<?php if ($zcontentactive =="dealandfidelite"){ echo"active" ;} ?>">
            Deals & fidélité
        </a>
    </div>
    <div class="col-lgss-2" style="border-right: 1px solid #000000;">
        <div class="dropdown_menu">
            <a href="#" class="span_menu">Compte particulier</a>
            <div class="dropdown-content">
                <a class="border_none" href="https://www.soutenonslecommercelocal.fr/vivresaville-administres" target="_blank">
                    Les avantages
                </a>
                <a href="<?php echo site_url('auth/login')?>" target="_blank">
                    Accés à Mon Compte
                </a>
                <a href="<?php echo site_url('auth/login')?>" target="_blank">
                    Accés à ma carte
                </a>

                <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                    Conditions générales
                </a>
            </div>
        </div>
    </div>
    <div class="col-lgss-2" id="article_menu">
        <div class="dropdown_menu">
            <a href="#" class="span_menu">Compte pro</a>
            <div class="dropdown-content">
<!--                <a class="border_none" href="--><?php //echo site_url('front/professionnels/inscription')?><!--" target="_blank">-->
<!--                    Souscription-->
<!--                </a>-->
                <a class="border_none" href="https://www.soutenonslecommercelocal.fr/abonnement-premium" target="_blank">
                    Abonnement Premium
                </a>
                <a href="https://www.soutenonslecommercelocal.fr/options-vivresaville" target="_blank">
                    Options vivresaville.fr
                </a>
                 <a href="https://www.soutenonslecommercelocal.fr/pack-infos" target="_blank">
                    Le Pack Infos
                </a>
                <a href="https://www.soutenonslecommercelocal.fr/pack-boutiques" target="_blank">
                    Le Pack Boutiques en ligne
                </a>
                <a href="https://www.soutenonslecommercelocal.fr/formulaire-vente" target="_blank">
                    Formulaire de vente en ligne
                </a>
                <a href="https://www.paypal.com/fr/signin" target="_blank">
                    Site paypal
                </a>
<!--                <a href="--><?php //echo site_url('auth/login')?><!--" target="_blank">-->
<!--                    Accés à Mon Compte-->
<!--                </a>-->
               
                <a href="https://www.soutenonslecommercelocal.fr/pack-promo" target="_blank">
                    Le Pack Promo
                </a>
                 <a href="https://www.soutenonslecommercelocal.fr/pack-restaurant-vsv" target="_blank">
                    Le Pack Restaurant
                </a>
                 <a href="https://www.soutenonslecommercelocal.fr/pack-gites-vsv" target="_blank">
                    Le Pack Gîtes
                </a>
                
                <a href="https://www.soutenonslecommercelocal.fr/abonnement-platinium" target="_blank">
                    Module Platinium
                </a>
                 <a href="https://www.soutenonslecommercelocal.fr/gestion-pro-de-la-carte-vsv" target="_blank">
                    La carte pro vivresaville.fr
                </a>
                 <a href="https://www.soutenonslecommercelocal.fr/modules-exports-csv" target="_blank">
                    Les module exports
                </a>
               
               
                <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                    Les conditions Générales
                </a>
                
               
            </div>
        </div>
    </div>

</div>
<?php if(($zcontentactive =="agenda") || ($zcontentactive =="article") || ($zcontentactive =="annuaire")){ ?>
    <style>
        #plus_display{
            display: none;
        }
        .col-lg-20{
            width: 25%;
        }
        #article_menu{
            border-right: none!important;
        }
    </style>
<?php } ?>


