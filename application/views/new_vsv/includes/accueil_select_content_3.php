<div class="container container_select_ville">
    
    <div class="row">

            <div class="col-md-2 "></div>

            <div class="col-md-3 ">
		        <div id="ordi1">
	                <img src="https://static.wixstatic.com/media/5fa146_dfb3342edf4a4be2abce9cdcaa01184f~mv2.png/v1/fill/w_335,h_257,al_c,q_85,usm_0.66_1.00_0.01/soutenons-ecran-sortez.webp" alt="soutenons-ecran-sortez.png" style="width:335px;height:257px;object-fit:cover;object-position:50% 50%">
	            </div>
	        </div>  

	        <div class="col-md-1 ">
		        <div id="acces1">
	                <a data-testid="linkElement" href="https://www.magazine-sortez.org" target="_blank" rel="noopener" class="btn-change" style="text-decoration:none"><span class="_3fUtn">Acc&egrave;s</span></a>
		        </div>
	        </div>

	        <div class="col-md-1 ">
	    	    <div id="acces2">
	                <a data-testid="linkElement" href="https://www.soutenonslecommercelocal.fr" target="_blank" rel="noopener" class="btn-change" style="text-decoration:none"><span class="_3fUtn">Acc&egrave;s</span></a>
	            </div>
	        </div>

	        <div class="col-md-3 ">
		        <div id="ordi2">
	                <img src="https://static.wixstatic.com/media/5fa146_0688aeed266d486088199fabbca428cd~mv2.png/v1/fill/w_335,h_257,al_c,q_85,usm_0.66_1.00_0.01/mmmmmmmmmmmmmm.webp" alt="mmmmmmmmmmmmmm.png" style="width:335px;height:257px;object-fit:cover;object-position:50% 50%">
	            </div>
	        </div>

	    <div class="col-md-2 "></div><br>

	    <div class="col-md-2 "></div>

	    <div class="col-md-1 ">
		    <div id="logo1">
	            <img src="https://www.vivresaville.fr/assets/vivresaville/sortez/24heure.png" alt="24-hours-logo-png-4.png" style="width:213px;height:213px;object-fit:cover;object-position:50% 50%">
	        </div>
	    </div>

	    <div class="col-md-6 ">
		    <div id="text1">
		        <h3>D&egrave;s la signature du partenariat,</h3>
		        <h3>le site est mis en ligne</h3>
		        <h3>sous un d&eacute;lai de 24 heures !...</h3>
		    </div>
	    </div>

	    <div class="col-md-1 ">
		    <div id="logo2">
	            <img src="https://www.vivresaville.fr/assets/vivresaville/sortez/24heure.png" alt="24-hours-logo-png-4.png" style="width:213px;height:213px;object-fit:cover;object-position:50% 50%">
	        </div>
	    </div>

        <div class="col-md-2 "></div>
	    <div class="col-md-4 "></div>

	    <div class="col-md-4 ">
		    <div id="det">
		        <a data-testid="linkElement" href="https://www.soutenonslecommercelocal.fr/vivresaville-collectivites" target="_blank" rel="noopener" class="_1_kc0"><span class="_3fUtn">D&eacute;tails</span></a>
		    </div>
	    </div>

	   <div class="col-md-4 "></div> 
       
	</div>
</div>

    <style type="text/css">
	  #ordi1{
	    margin-left:-120px;
	        }
	  #ordi2{
	    margin-left:60px;
	        }

	#logo1{
	    margin-left:-160px;
		margin-top: 70px;
	}

	#logo2{
	    margin-left: 60px;
		margin-top: 70px;
	}

	#text1 h3{
		text-align: center;
		color: #E80EAE;
		font-family: libre baskerville,serif;
		font-size: 35px;
		font-style: italic;
	}

	#acces1{
    		background-color:transparent;
    		position: absolute;
    		top: 20px;
    		width: 133px;
    		left: -25px;
   
    }

 	#acces2{
		background-color: transparent; 
		position: relative; 
		top: 81px; 
		right: 10px; 
		width: 140px;
	}

	#acces1 span{
		position: relative;
		left: 30px;
		top: 5px;
		font-size: 18px;
	}

	#acces2 span{
		position: relative;
		left: 30px;
		top: 5px;
		font-size: 18px;
	}

	#acces3 span{
		position: relative;
		left: 30px;
		top: 5px;
		font-size: 18px;
	}

	#acces4 span{
		position: relative;
		left: 30px;
		top: 5px;
		font-size: 18px;
	}

	#det{
			background-color:#ff00ff;
	    	text-align:center;
	    	top: 102px;
	    	width: 311px;
			height: 50px;
			margin-bottom:60px;
			margin-top: 40px;
			margin-left: 40px;
			color:white;
	}

	#det:hover {
		background-color:mediumseagreen;
	}

	#det2 span{
		font-size: 20px;
		position: relative;
		left: 5px;
		top: 8px;
	}

	#det span{
		font-size: 20px;
		position: relative;
		left: 5px;
		top: 8px;
	}

	#det a{
		color:white;
		text-decoration:none;
	}

	#text1{
		width : 500px;
		margin-top:110px;
		margin-left: 10px;
	}

	.btn-change{
		height: 40px;
		width: 110px;
		background: #ff00ff;
		margin: 20px;
		float: left;
		box-shadow: 0 0 1px #ccc;
		-webkit-transition: all 0.5s ease-in-out;
		border: 0px;
		color: white;
	}

	.btn-change:hover{
		-webkit-transform: scale(1.1);
		background-color: mediumseagreen;
		color: white;
	}

	</style>