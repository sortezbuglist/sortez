<?php
$thisss =& get_instance();
$thisss->load->library('session');
$main_width_device = $thisss->session->userdata('main_width_device');
$main_mobile_screen = false;
if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device)<=1199){
    $main_mobile_screen = true;
} else {
    $main_mobile_screen = false;
}
?>

<?php if ($main_mobile_screen==false) { ?>
<div class="vs_main_menu padding0 d-none d-lg-block d-xl-block">
    <div class="vs_ul_main_menu vsv_modal_container row textaligncenter d-none d-lg-block d-xl-block">
        <div class="mm_1">
            <a href="javascript:history.go(-1);">
                <img src="<?php echo base_url(); ?>assets/ville-test/wpimages/wp968a194e_06.png" alt="" width="47"
                     height="47">
            </a>
        </div>
        <div class="mm_2">
            <a href="javascript:void(0);" class="vsv_modal_link"
               id="vsv_main_link_infos" attachmentid="<?php echo site_url('site-vivresaville/informations-conso-pro.html'); ?>">
                Informations
                et souscription
                <!--<span><img alt="" src="<?php echo base_url(); ?>assets/ville-test/wpimages/wp8d51f9c9_06.png"></span>-->
            </a>
        </div>
        <div class="mm_3">
            <a href="<?php echo site_url('auth'); ?>">
                Mon compte
                <!--<span><img alt="" src="<?php echo base_url(); ?>assets/ville-test/wpimages/wp3739c610_06.png"></span>-->
            </a>
        </div>
        <div class="mm_4">
            <a href="<?php echo site_url('front/utilisateur/liste_favoris'); ?>">
                Mes favoris
                <!--<span><img alt="" src="<?php echo base_url(); ?>assets/ville-test/wpimages/wpddd50288_06.png"></span>-->
            </a>
        </div>
        <div class="mm_5">
            <a href="javascript:void(0);" class="vsv_modal_link"
               id="vsv_main_link_contact" attachmentid="<?php echo base_url();?>/contact/">
                Contact
                <!--<span><img alt="" src="<?php echo base_url(); ?>assets/ville-test/wpimages/wp43b494f1_06.png"></span>-->
            </a>
        </div>
        <!--<div class="mm_6">
            <a href="javascript:void(0);" class="vsv_modal_link"
               id="vsv_main_link_newsletter" attachmentid="<?php echo site_url('publightbox'); ?>">
                Newsletter
                <span><img alt="" src="<?php echo base_url(); ?>assets/ville-test/wpimages/wpc300430f_06.png"></span>
            </a>
        </div>-->
        <div class="mm_7">
            <a href="<?php echo site_url('vivresaville/accueil/other_ville'); ?>">
                Choisissez un autre secteur
            </a>
        </div>
        <div class="mm_6">
            <!--Suivez-nous-->
            <span>
                <a href="javascript:void(0);"
                   id="vsv_main_link_fb" class="vsv_modal_link"
                   attachmentid="<?php echo site_url('front/professionnels/FacebookPrivicarteForm'); ?>">
                    <img alt="" src="<?php echo base_url(); ?>assets/ville-test/wpimages/img_social_menu_fb.png">
                </a>
            </span>
            <span>
                <a href="javascript:void(0);"
                   id="vsv_main_link_twt" class="vsv_modal_link"
                   attachmentid="<?php echo site_url('front/professionnels/TwitterPrivicarteForm'); ?>">
                    <img alt="" src="<?php echo base_url(); ?>assets/ville-test/wpimages/img_social_menu_twt.png">
                </a>
            </span>
        </div>
        <div class="mm_8 pl-4">
            <div id="google_translate_element"></div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({
                        pageLanguage: 'fr',
                        includedLanguages: 'de,en,es,fr,it,ja,nl,no,pl,pt,ru,sv,zh-CN',
                        layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
                        autoDisplay: false
                    }, 'google_translate_element');
                }
            </script>
            <script type="text/javascript"
                    src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
            <!--<span><img alt="" src="<?php echo base_url(); ?>assets/ville-test/wpimages/wp43f61dd7_06.png"></span>-->
        </div>
    </div>
</div>
<?php } ?>

<?php $data['empty'] = null; ?>
<?php $this->load->view("vivresaville/includes/main_menu_mobile_index", $data); ?>

<div class="modal fade" id="vsv_modal_main_menu" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe id="vsv_modal_main_iframe" class="embed-responsive-item" frameborder="0"></iframe>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>


<script type="application/javascript">
    $(document).ready(function () {
        /*setTimeout(function(){
         $('#google_translate_element .skiptranslate .goog-te-gadget-simple a.goog-te-menu-value span:first-child').html("LANGUE");
         }, 2500);*/

        $(".vsv_modal_container a.vsv_modal_link").each(function () {
            var id = $(this).attr("id");
            $(".vsv_modal_container").on('click', "#" + id, function () {
                if (id == "vsv_main_link_fb" || id == "vsv_main_link_twt") {
                    $('#vsv_modal_main_menu .modal-body .embed-responsive').css("height", "700px");
                } else {
                    $('#vsv_modal_main_menu .modal-body .embed-responsive').css("height", "auto");
                    $('#vsv_modal_main_menu .modal-body .embed-responsive').css("min-height", "768px");
                }
                var src = $(this).attr('attachmentid');
                $("#vsv_modal_main_iframe").attr('src', src);
                $('#vsv_modal_main_menu').modal('show');
                return false;
            });
        });
    });
</script>
