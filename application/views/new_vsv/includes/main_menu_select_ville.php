<style>
    .content_menu_soutenons a span{
        color: black;
        text-decoration: none;
        font-family: LibreBaskerville-Bold;
        font-style: italic;
        font-size: 1em!important;
    }
    .content_menu_soutenons a{
        color: black;
        text-decoration: none;
        font-family: LibreBaskerville-Bold;
        font-style: italic;
        font-size: 1em!important;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .col-lgi-2{
        -ms-flex: 0 0 20%;
        flex: 0 0 20%;
        max-width: 20%;
    }
    .col-lgi-2_article{
        -ms-flex: 0 0 20%;
        flex: 0 0 20%;
        max-width: 20%;
    }

    /* The container <div> - needed to position the dropdown content */
    .dropdown_menu {
        position: relative;
        display: inline-block;
        padding-bottom: 10px;
        padding-top: 10px;
    }

    /* Dropdown Content (Hidden by Default) */
    .dropdown-content {
        display: none;
        position: absolute;
        top: 40px;
        left: 0;
        background-color: white;
        min-width: 100%;
        width: 100%;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1000;
    }

    /* Links inside the dropdown */
    .dropdown-content a {
        color: black;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-left: 5px;
        padding-right: 5px;
        text-decoration: none;
        display: block;
        border-top: 1px solid black;
    }
    .dropdown-content a :hover{
        color: black !important;
        padding-top: 5px;
        padding-bottom: 5px;
        padding-left: 5px;
        padding-right: 5px;
        text-decoration: none;
        display: block;
        border-top: 1px solid black;
    }

    /* Show the dropdown menu on hover */
    .dropdown_menu:hover .dropdown-content {display: block;}

    /* Change the background color of the dropdown button when the dropdown content is shown */
    .dropdown_menu:hover{display:block;}

    .border_none{
        border-top: unset!important;
    }
    .span_menu{
        color: black;
        text-decoration: none;
        font-family: LibreBaskerville-Bold;
        font-style: italic;
        font-size: 1em!important;
        cursor: pointer;
    }
    .span_menu:hover{
        color: black!important;
    }
</style>
<div class="content_menu_soutenons d-none d-lg-flex d-xl-flex " style="justify-content: space-around;">
    <!--    <div class="col-lgi-2" style="border-right: 1px solid #000000">-->
    <!--        <a href="--><?php //echo base_url('vivresaville/Accueil')?><!--" class="--><?php //if ($zcontentactive =="accueil"){ echo"active" ;} ?><!--">-->
    <!--            Page accueil-->
    <!--        </a>-->
    <!--    </div>-->
    <div class="col-lgi-2" style="border-right: 1px solid #000000;">
        <div class="dropdown_menu">
            <a href="<?php echo site_url('vivresaville/ListVille'); ?>" class="finance_menu <?php if(isset($type_page) && $type_page == ''){ echo 'active'; } ?>">Page d'accueil</a>
            <div class="dropdown-content">
                <a class="border_none ni_class" href="<?php echo site_url('vivresaville/ListVille/index/contact'); ?>">
                    Nous contacter
                </a>
                <a href="<?php echo base_url('vivresaville/ListVille/index/newsletter') ?>">
                    Newsletter
                </a>
            </div>
        </div>
    </div>
    <div class="col-lgi-2" style= "border-right: 1px solid #000000;">
        <div class="dropdown_menu">
            <a href="#" class="span_menu">Villes & territoires référencés</a>
            <div class="dropdown-content">
               <?php $count=0; if (isset($villes) && count($villes)>0) { ?>
                    <?php foreach ($villes as $ville_item) { ?>
                        <a <?php if($count == '0') echo'class="border_none"'; ?> href="javascript:void(0)" onclick="set_value_id_ville(<?php echo $ville_item->id;?>)">
                            <?php echo $ville_item->name_ville;?>
                        </a>
                    <?php $count+=1; } ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-lgi-2" style="border-right: 1px solid #000000;">
        <div class="dropdown_menu">
            <a href="#" class="span_menu">Les avantages des particuliers</a>
            <div class="dropdown-content">
                <!-- <a class="border_none" href="https://www.soutenonslecommercelocal.fr/vivresaville-administres" target="_blank">
                    Les avantages
                </a> -->
                <a  class="border_none" href="<?php echo site_url('auth/login')?>" target="_blank">
                    Accés à Mon Compte
                </a>
                <a href="<?php echo site_url('auth/login')?>" target="_blank">
                    Accés à ma carte
                </a>

                <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                    Conditions générales
                </a>
            </div>
        </div>
    </div>
<!--     <div class="col-lgi-2" style="border-right: 1px solid #000000;">
        <div class="dropdown_menu">
            <a href="#" class="span_menu">
                    Les avantages des collectivités
                </a>
                <div class="dropdown-content">
                <a class="border_none" href="https://www.soutenonslecommercelocal.fr/vivresaville-collectivites" target="_blank">
                    Les avantages
                </a>
                <a href="<?php echo site_url('vivresaville/ListVille/index/financement') ?>">
                    Les Conditions financières
                </a>
                <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                    Conditions générales
                </a>
                
                    

                </div>
        </div>
    </div> -->
    <div class="col-lgi-2" id="article_menu">
        <div class="dropdown_menu">
            <a href="#" class="span_menu">
                    Les avantages des professionnels
                </a>
            <div class="dropdown-content">
                <a class="border_none" href="https://www.soutenonslecommercelocal.fr/abonnement-vivresaville" target="_blank">
                    L'abonnement premium vivresaville
                </a>                        
                <a data-testid="linkElement" href="https://www.soutenonslecommercelocal.fr/options-vivresaville" target="_blank" rel="noreferrer noopener" class="" aria-haspopup="false" tabindex="-1">Options abonnement vivresaville</a>
<!--                 <a href="https://www.soutenonslecommercelocal.fr/abonnement-special-covid" target="_blank">
                    Abonnement "Spécial Covid"
                </a>
                <a href="https://www.soutenonslecommercelocal.fr/options-special-covid" href="" target="_blank">
                    Options "Spécial Covid"
                </a> -->
                <a href="<?php echo site_url('auth/login')?>" target="_blank">
                    Accés à Mon Compte
                </a>                
<!--                 <a href="https://www.soutenonslecommercelocal.fr/pack-infos" target="_blank">
                    Le Pack Infos
                </a> -->
                <a href="https://www.soutenonslecommercelocal.fr/pack-infos-vsv" target="_blank">
                    Le Pack Infos
                </a>                

<!--                 <a href="https://www.soutenonslecommercelocal.fr/pack-boutiques" target="_blank">
                    Le Pack Boutiques
                </a> -->
                <a href="https://www.soutenonslecommercelocal.fr/pack-boutiques-vsv" target="_blank">
                    Le Pack Boutiques
                </a>                

                

                <a data-testid="linkElement" href="https://www.soutenonslecommercelocal.fr/formulaire-vente-vsv" target="_blank" rel="noreferrer noopener" class="" aria-haspopup="false" tabindex="-1">Formulaire de vente en ligne</a>


                <a href="https://www.soutenonslecommercelocal.fr/pack-promo-vsv" target="_blank">
                    Le Pack Promo
                </a>

                <a data-testid="linkElement" href="https://www.soutenonslecommercelocal.fr/gestion-pro-de-la-carte-vsv" target="_blank" rel="noreferrer noopener" class="" aria-haspopup="false" tabindex="-1">La carte pro</a>                

                <a href="https://www.soutenonslecommercelocal.fr/pack-restauration-vsv" target="_blank">
                    Le pack restaurant
                </a>

                <a href="https://www.soutenonslecommercelocal.fr/pack-gites-vsv" target="_blank">
                    Le Pack Gîtes
                </a>

                <a href="https://www.soutenonslecommercelocal.fr/abonnement-platinium-vsv" target="_blank">
                    L'abonnement Platinium
                </a>
                <a data-testid="linkElement" href="https://www.soutenonslecommercelocal.fr/modules-exports-csv" target="_blank" rel="noreferrer noopener" class="" aria-haspopup="false" tabindex="-1">Les modules exports</a>

                <a data-testid="linkElement" href="https://www.soutenonslecommercelocal.fr/carte-pro" target="_blank" rel="noreferrer noopener" class="" aria-haspopup="false" tabindex="-1">Application carte-pro</a>                

                <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                    Les conditions Générales de Vente
                </a>
                <a href="<?php echo site_url('vivresaville/ListVille/createAbonnementInscription')?>" target="_blank">
                    S'inscrire à un compte Vivresaville.fr
                </a>
            </div>
        </div>
    </div>
    <!-- <div class="col-lgi-2" id="article_menu">
        <div class="dropdown_menu">
            <a href="<?php //echo site_url('vivresaville/ListVille/index/financement') ?>" class="finance_menu <?php// if(isset($type_page) && $type_page == "financement"){ echo 'active'; } ?>">Les conditions financières</a>
        </div>
    </div> -->
</div>



<style>
    .finance_menu:hover{
        color:#E80EAE;
    }
</style>
<?php if(($zcontentactive =="agenda") || ($zcontentactive =="article") || ($zcontentactive =="annuaire")){ ?>
    <style>
        #plus_display{
            display: none;
        }
        .col-lg-20{
            width: 25%;
        }
        #article_menu{
            border-right: none!important;
        }
    </style>
<?php } ?>



