<?php
    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }
    if(isMobile()){
        $thiss =& get_instance();
        $thiss->load->library('session');
        $thiss->load->model('vsv_ville_other');
        $localdata_IdVille = $thiss->session->userdata('localdata_IdVille');
        $localdata_IdVille_parent = $thiss->session->userdata('localdata_IdVille_parent');
        $localdata_IdVille_all = $thiss->session->userdata('localdata_IdVille_all');
        $thiss->load->model("vivresaville_villes");
        $logo_to_show_vsv = base_url() . "assets/ville-test/wpimages/logo_ville_test.png";
        if (isset($localdata_IdVille_parent) && $localdata_IdVille_parent != "" && $localdata_IdVille_parent != false) {
            $vsv_id_vivresaville = $thiss->vivresaville_villes->getByIdVille($localdata_IdVille_parent);
            if (isset($vsv_id_vivresaville) && isset($vsv_id_vivresaville->id)) {
                $vsv_object = $thiss->vivresaville_villes->getById($vsv_id_vivresaville->id);
                $vsv_other_ville = $thiss->vsv_ville_other->getByIdVsv($vsv_id_vivresaville->id);
            }
            if (isset($vsv_object) && isset($vsv_object->logo) && $vsv_object->logo != '' && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo)) {
                $logo_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/" . $vsv_object->logo;
            }else{
                $logo_to_show_vsv = base_url('assets/soutenons/logo_vivresaville.png');
            }
            if (isset($vsv_object) && isset($vsv_object->logo_droite) && $vsv_object->logo_droite!= '' && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo_droite)) {
                $logo_droite_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/".$vsv_object->logo_droite;
            }else{
                $logo_droite_to_show_vsv = base_url('assets/soutenons/logo_vivresaville.png');
            }
        }
?>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <div class="menu-mobile">
        <?php if($zcontentactive != 'ville_select'){?>
            <style>
                .menu-mobile{
                    margin-top: 0px!important;
                }
            </style>
            <div class="d-block">
                <div class="container text-center">
                    <div class="row pt-4 d-lg-flex">

                        <div class="col-lg-12 col-sm-12 p-0">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lgs-2 col-sm-12 p-0 ml-2 mr-2" >
                                        <a class="menus_2 btn btn-lg" href="<?php echo site_url('vivresaville/accueil/index') ?>" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">Page accueil</a>
                                    </div>
                                    <div class="col-lgs-2 col-sm-12 p-0 ml-2 mr-2">
                                        <a class="menus_2 btn btn-lg" data-fancybox="" data-animation-duration="500" data-src="#divContactPartnerForm" href="javascript:;" id="IdCsontactPartnerForm" title="Contactez-nous!" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">Contact</a>
                                    </div>
                                    <div class="col-lgs-2 col-sm-12 p-0 ml-2 mr-2" >
                                        <a class="menus_2 btn btn-lg" href="<?php echo site_url("/auth/login");?>" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">Mon compte</a>
                                    </div>

                                    <div class="col-lgs-2 col-sm-12 p-0 ml-2 mr-2" >
                                        <a class="menus_2 btn btn-lg" href="<?php echo site_url(); ?>admin" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">Mes favoris</a>
                                    </div>

                                    <div class="col-lgs-2 col-sm-12 p-0 ml-2" >
                                        <a class="menus_2 btn btn-lg" href="#divContactRecommandationForm" id="IdRecommandationPartnerForm_main_menu" title="Recommandation" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">
                                            Recommandez
                                        </a>
                                    </div>
                                    <div class="col-lgs-2 col-sm-12 p-0 ml-2" >
                                        <a class="menus_2 btn btn-lg" id="google_translate_element" onclick="translatede()" href="javascript:void(0)" title="language" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px!important;font-style: italic;font-weight: normal!important;">
                                            Language
                                        </a>
                                    </div>
                                    <div class="col-lgsi-2 col-sm-12 p-0 ml-2" >
                                        <a class="menus_2 btn btn-lg" href="<?php echo site_url('vivresaville/accueil/other_ville') ?>" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">
                                            Autres secteurs
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php }else{ ?>
            <div class="d-block d-lg-none-lg d-md-none">
                <div class="container text-center">
                    <div class="row">
                        <div class="col-4 pr-0">
                            <a class="menus_2 btn" id="google_translate_element" onclick="translatede()" href="javascript:void(0)" title="language" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px!important;font-style: italic;font-weight: normal!important;">
                                Language
                            </a>
                        </div>
                        <div class="col-4 pl-1 pr-1">
                            <a class="menus_2 btn" data-fancybox="" data-animation-duration="500" data-src="#divContactPartnerForm" href="javascript:;" id="IdCsontactPartnerForm" title="Contactez-nous!" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">
                                Contact
                            </a>
                        </div>
                        <div class="col-4 pl-0">
                            <a class="menus_2 btn" href="<?php echo site_url("/auth/login");?>" style="font-family:Libre-Baskerville-Italic,serif!important;font-size:14px;font-style: italic;font-weight: normal!important;">Mon compte</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <style>
        .navbar-toggler{
            background-color: rgba(0,0,0,0.7);
            margin-top: 15px;
            margin-left: 40%;
            margin-right: 40%;
        }
        .item-nav{
            border-bottom: 1px solid #000000;
        }

        @media only screen and (max-width: 600px) {
            [class*="col-"] {
                width: 100%;
            }

        }

         .item-menu {
            display: inline-block;
            top: 40px;
            background-color: transparent;
            width: 100%;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        }
         .item-menu a {
            color: white;
            padding-top: 5px;
            padding-bottom: 5px;
            padding-right: 5px;
             padding-left: 20px;
            text-decoration: none;
            display: block;
            border-top: 1px solid black;
        }
        .navbar-nav.mr-auto{
            margin-top: 2px;
            transition: transform 0s -webkit-transform 0s;
            background: rgba(159, 159, 158, 0.9);
        }
        .item-nav a{
            transition: transform 0s -webkit-transform 0s;
            color: white;
        }
        .item-nav .w_85{
            width: 86%;
            text-align: left;
            border: unset!important;
            text-overflow: ellipsis;
        }
        .item-nav .w_15{
            width: 14%;
            text-align: center;
            border-left: 2px solid #ffffff;
            border-bottom: unset!important;
            border-top: unset!important;
            border-right: unset!important;
            border-radius: unset;
        }
        div.collapse{
            width: 100%;
        }
        .list-menu{
            padding-left: 0;
            margin-left: 0;
        }
    </style>

       <nav class="d-flex d-lg-none d-xl-none navbar navbar-expand-lg navbar-dark bg-dark" style="background: #ffff  !important;">

        <button onclick="change_class()" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" style="background: rgba(160, 160, 159, 1);">
<!--            <span id="content_navbar" class="navbar-toggler-icon" style="color: white;margin-top: 5px;margin-bottom: 5px;"></span>-->
            <span id="content_navbar" class="material-icons" style="font-size: 35px;color: white;margin-top: 7px;margin-bottom: 7px;">
                dehaze
            </span>
        </button>
       <?php if($zcontentactive != 'ville_select'){?>
           <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto">
                   <li class="item-nav">
                       <button type="button" class="btn">
                           <a href="<?php echo site_url('article'); ?>" class="span_menu">
                               L'actualité & la revue de presse
                           </a>
                       </button>
                   </li>
                   <li class="item-nav">
                       <button type="button" class="btn">
                           <a href="<?php echo site_url('agenda'); ?>" class="span_menu">
                               L'agenda
                           </a>
                       </button>
                   </li>
                   <li class="item-nav">
                       <button type="button" class="btn">
                           <a href="<?php echo site_url('annuaire'); ?>" class="span_menu">
                               Les Bonnes Adresses
                           </a>
                       </button>
                   </li>
                   <li class="item-nav">
                       <button type="button" class="btn">
                           <a href="<?php echo site_url('Boutique'); ?>" class="span_menu">
                               Boutiques
                           </a>
                       </button>
                   </li>
                   <li class="item-nav">
                       <button type="button" class="btn">
                           <a href="<?php echo site_url('DealAndFidelite'); ?>" class="span_menu">
                               Deal & fidelité
                           </a>
                       </button>
                   </li>
                   <li class="item-nav">
                       <div class="container">
                           <div class="row w-100">
                               <button type="button" class="btn w_85" data-toggle="collapse" data-target="#avParticulier"><a href="#" class="span_menu">Les Compte particuliers</a><span class="glyphicon glyphicon-chevron-down"></span></button>
                               <button type="button" class="btn w_15" data-toggle="collapse" data-target="#avParticulier"><a href="javascript:void(0)" class="span_menu" style="margin-left: 15px;font-size: 25px!important;">V</a></button>
                           </div>
                       </div>
                       <div id="avParticulier" class="collapse">
                           <ul class="list-menu">
                               <li class="item-menu">
                                   <a class="border_none" href="https://www.soutenonslecommercelocal.fr/vivresaville-administres" target="_blank">
                                       Les avantages
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="<?php echo site_url('auth/login')?>" target="_blank">
                                       Accés à Mon Compte
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="<?php echo site_url('auth/login')?>" target="_blank">
                                       Accés à ma carte
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                                       Conditions générales
                                   </a>
                               </li>
                           </ul>
                       </div>
                   </li>
                   <li class="item-nav">
                       <div class="container">
                           <div class="row w-100">
                               <button type="button" class="btn w_85" data-toggle="collapse" data-target="#plus"><a href="#" class="span_menu">Les avantages des professionnels</a><span class="glyphicon glyphicon-chevron-down"></span></button>
                               <button type="button" class="btn w_15" data-toggle="collapse" data-target="#plus"><a href="javascript:void(0)" class="span_menu" style="margin-left: 15px;font-size: 25px!important;">V</a></button>
                           </div>
                       </div>
                       <div id="plus" class="collapse">
                           <ul class="list-menu">
                               <li class="item-menu">
                                   <a class="border_none" href="https://www.soutenonslecommercelocal.fr/abonnement-premium" target="_blank">
                                       Abonnement Premium
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/fonctions-complementaires" target="_blank">
                                       Fonctions Complémentaires
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/formulaire-vente" target="_blank">
                                       Formulaire de vente en ligne
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/pack-infos" target="_blank">
                                       Le Pack Infos
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/pack-promo" target="_blank">
                                       Le Pack Promo
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/pack-boutiques" target="_blank">
                                       Le Pack Boutiques
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/abonnement-platinium" target="_blank">
                                       L'abonnement Platinium
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/pack-restauration" target="_blank">
                                       Le Pack Restaurant
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                                       Les conditions Générales
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.paypal.com/fr/signin" target="_blank">
                                       Site paypal
                                   </a>
                               </li>
                           </ul>
                       </div>
                   </li>
               </ul>
           </div>
       <?php }else{ ?>
           <div class="collapse navbar-collapse" id="navbarSupportedContent">
               <ul class="navbar-nav mr-auto">
                   <li class="item-nav">
                       <div class="container">
                           <div class="row w-100">
                               <button type="button" class="btn w_85" data-toggle="collapse" data-target="#pageAccueil"><a href="<?php echo site_url('vivresaville/ListVille'); ?>" class="span_menu">Page d'accueil</a></button>
                               <button type="button" class="btn w_15" data-toggle="collapse" data-target="#pageAccueil"><a href="javascript:void(0)" class="span_menu" style="margin-left: 15px;font-size: 25px!important;">V</a></button>
                           </div>
                       </div>
                       <div id="pageAccueil" class="collapse">
                           <ul class="list-menu">
                               <li class="item-menu">
                                   <a class="border_none ni_class" href="<?php echo site_url('vivresaville/ListVille/index/contact'); ?>">
                                       Nous contacter
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="<?php echo base_url('vivresaville/ListVille/index/newsletter') ?>">
                                       Newsletter
                                   </a>
                               </li>
                           </ul>
                       </div>
                   </li>
                   <li class="item-nav">
                       <div class="container">
                           <div class="row w-100">
                               <button type="button" class="btn w_85" data-toggle="collapse" data-target="#villes"><a href="#" class="span_menu">Villes & territoires référencés</a><span class="glyphicon glyphicon-chevron-down"></span></button>
                               <button type="button" class="btn w_15" data-toggle="collapse" data-target="#villes"><a href="javascript:void(0)" class="span_menu" style="margin-left: 15px;font-size: 25px!important;">V</a></button>
                           </div>
                       </div>
                       <div id="villes" class="collapse">
                           <ul class="list-menu">
                               <li class="item-menu">
                                   <?php if (isset($villes) && count($villes)>0) { ?>
                                       <?php foreach ($villes as $ville_item) { ?>
                                           <a href="javascript:void(0)" onclick="set_value_id_ville(<?php echo $ville_item->id;?>)">
                                               <?php echo $ville_item->name_ville;?>
                                           </a>
                                       <?php } ?>
                                   <?php } ?>
                               </li>
                           </ul>
                       </div>
                   </li>
                   <li class="item-nav">
                       <div class="container">
                           <div class="row w-100">
                               <button type="button" class="btn w_85" data-toggle="collapse" data-target="#avPart1"><a href="#" class="span_menu">Les avantages des particuliers</a><span class="glyphicon glyphicon-chevron-down"></span></button>
                               <button type="button" class="btn w_15" data-toggle="collapse" data-target="#avPart1"><a href="javascript:void(0)" class="span_menu" style="margin-left: 15px;font-size: 25px!important;">V</a></button>
                           </div>
                       </div>
                       <div id="avPart1" class="collapse">
                           <ul class="list-menu">
                               <li class="item-menu">
                                   <a class="border_none" href="https://www.soutenonslecommercelocal.fr/vivresaville-administres" target="_blank">
                                       Les avantages
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="<?php echo site_url('auth/login')?>" target="_blank">
                                       Accés à Mon Compte
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                                       Conditions générales
                                   </a>
                               </li>
                           </ul>
                       </div>
                   </li>
                   <li class="item-nav">
                       <div class="container">
                           <div class="row w-100">
                               <button type="button" class="btn w_85" data-toggle="collapse" data-target="#avPart"><a href="#" class="span_menu">Les avantages des collectivités</a><span class="glyphicon glyphicon-chevron-down"></span></button>
                               <button type="button" class="btn w_15" data-toggle="collapse" data-target="#avPart"><a href="javascript:void(0)" class="span_menu" style="margin-left: 15px;font-size: 25px!important;">V</a></button>
                           </div>
                       </div>
                       <div id="avPart" class="collapse">
                           <ul class="list-menu">
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/vivresaville-collectivites" target="_blank">
                                       Les avantages
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="<?php echo site_url('vivresaville/ListVille/index/financement') ?>">
                                       Les Conditions financières
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                                       Conditions générales
                                   </a>
                               </li>
                           </ul>
                       </div>
                   </li>
                   <li class="item-nav">
                       <div class="container">
                           <div class="row w-100">
                               <button type="button" class="btn w_85" data-toggle="collapse" data-target="#plus"><a href="#" class="span_menu">Les avantages des professionnels</a><span class="glyphicon glyphicon-chevron-down"></span></button>
                               <button type="button" class="btn w_15" data-toggle="collapse" data-target="#plus"><a href="javascript:void(0)" class="span_menu" style="margin-left: 15px;font-size: 25px!important;">V</a></button>
                           </div>
                       </div>
                       <div id="plus" class="collapse">
                           <ul class="list-menu">
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/abonnement-special-covid" target="_blank">
                                       Abonnement "Spécial Covid"
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/options-special-covid" href="" target="_blank">
                                       Options "Spécial Covid"
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="<?php echo site_url('auth/login')?>" target="_blank">
                                       Accés à Mon Compte
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/pack-infos" target="_blank">
                                       Le Pack Infos
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/pack-promo" target="_blank">
                                       Le Pack Promo
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/pack-boutiques" target="_blank">
                                       Le Pack Boutiques
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/pack-restauration" target="_blank">
                                       Le Pack Restauration
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/pack-gites" target="_blank">
                                       Le Pack Gîtes
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/abonnement-platinium" target="_blank">
                                       L'abonnement Platinium
                                   </a>
                               </li>
                               <li class="item-menu">
                                   <a href="https://www.soutenonslecommercelocal.fr/conditions-generales" target="_blank">
                                       Les conditions Générales de Vente
                                   </a>
                               </li>
                           </ul>
                       </div>
                   </li>
               </ul>
           </div>
       <?php } ?>
    </nav>
    <div class="d-block d-lg-none-lg d-md-none">
        <div class="row" style="margin-left: 0; margin-right: 0;">
                <div class="col-12 text-center " style="margin-left: 0; margin-right: 0;">
                <table>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-12 vsv_main_ville_name_txt logo_vivresaville">
                                    <?php if($zcontentactive != 'ville_select'){?>
                                    <div style="width: 150px;margin: auto;height: auto;">
                                        <img src="<?php echo $logo_to_show_vsv; ?>" class="img-fluid">
                                    </div>
                                    <img src="<?php echo base_url('assets/vivresaville/sortez/select_ville2.png') ?>" class="img-fluid">
                                    <?php } ?>
                                    <?php if (isset($vsv_object->name_ville)){ ?>
                                    <div class="container mb-3">
                                        <p class="ville_name_class">
                                            <?php echo $vsv_object->name_ville; ?>
                                        </p>
                                    </div>
                                    <?php
                                        }else{
                                            echo '<img src="'.base_url("assets/vivresaville/sortez/select_ville2.png").'" class="img_fluid">';
                                        }
                                    ?>
                                </div>
                                <?php if($zcontentactive != 'ville_select'){?>
                                <div class="col-12 p-0 text-center">
                                    <?php if (isset($vsv_other_ville) && count($vsv_other_ville) > 0) { ?>
                                        <form class="vsv_other_ville_commune_form" id="vsv_other_ville_commune_form_mobile" name="vsv_other_ville_commune_form" method="post" action="<?php  echo site_url("vivresaville/Accueil");?>">
                                            <select id="vsv_other_ville_select_mobile" name="vsv_other_ville_select"
                                                    class="mb-3 p-0 form-control text-center vsv_other_ville_select ">
                                                <option value="0" <?php if (isset($localdata_IdVille_all) && $localdata_IdVille_all!="") echo "selected";?>>Toutes les communes</option>
                                                <?php foreach ($vsv_other_ville as $vsv_item) { ?>
                                                    <option value="<?php echo $vsv_item->id_ville; ?>" <?php if (isset($localdata_IdVille) && $localdata_IdVille==$vsv_item->id_ville && (!isset($localdata_IdVille_all) || $localdata_IdVille_all==false)) echo "selected";?>><?php echo $vsv_item->name; ?></option>
                                                <?php } ?>
                                            </select>
                                            <input type="hidden" id="vsv_other_ville_currenturl" name="vsv_other_ville_currenturl" value="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"/>
                                        </form>
                                        <script>$(document).ready(function(){$('#vsv_other_ville_select_mobile').change(function(){ vsv_other_ville_commune_form_mobile.submit();});});</script>
                                    <?php } ?>
                                </div>
                                <div class="col-12 p-0 text-center vsv_referencez_site_ville">
                                    <?php
                                    $ville_reference_vsv = false;
                                    $thisss =& get_instance();
                                    $thisss->load->model("vsv_ville_reference");
                                    $thisss->load->library('session');
                                    $localdata_IdVille = $thisss->session->userdata('localdata_IdVille');
                                    $ip_adress = $thisss->input->ip_address();
                                    $objVilleReference = $thisss->vsv_ville_reference->getWhere(' ip="'.$ip_adress.'" and id_ville='.$localdata_IdVille);
                                    if (isset($objVilleReference) && count($objVilleReference)>0) {
                                        $ville_reference_vsv = true;
                                    }
                                    ?>
                                    <form id="form_id_ville_reference_vsv_mobile" class="form_id_ville_reference_vsv w-100" method="post" name="form_id_ville_reference_vsv" action="<?php echo site_url("vivresaville/Accueil");?>">
    <!--                                    <input type="checkbox" id="id_ville_reference_vsv" name="id_ville_reference_vsv" class="id_ville_reference_vsv mt-1" --><?php //if($ville_reference_vsv) echo 'checked';?><!-- onclick="fn_id_ville_reference_vsv();"/>-->
                                        <input type="hidden" name="check_id_ville_reference_vsv" id="check_id_ville_reference_vsv_mobile" value="<?php if($ville_reference_vsv) echo '1'; else echo '2';?>">
    <!--                                    <label class="reference_site">Référencez ce site</label>-->
                                        <label class="content_checkmark reference_site">
                                            Référencez ce site
                                            <input type="checkbox" id="id_ville_reference_vsv_mobile" name="id_ville_reference_vsv_mobile" class="id_ville_reference_vsv mt-1" <?php if($ville_reference_vsv) echo 'checked';?> onclick="fn_id_ville_reference_vsv_mobile();"/>
                                            <span class="checkmark"></span>
                                        </label>
                                    </form>
                                    <script type="application/javascript">
                                        function fn_id_ville_reference_vsv_mobile() {
                                            if($('input[name="id_ville_reference_vsv_mobile"]').is(':checked')) {
                                                $("#check_id_ville_reference_vsv_mobile").val(1);
                                            } else {
                                                $("#check_id_ville_reference_vsv_mobile").val(2);
                                            }
                                            $("#form_id_ville_reference_vsv_mobile").submit();
                                        }
                                    </script>
                                </div>
                                <div class="col-12 p-0 text-center">
                                    <div style="width: 150px;margin: auto;height: auto;">
                                        <img src="<?php echo $logo_droite_to_show_vsv; ?>"
                                             alt="logo" id="vsv_img_main_logo" class="img-fluid"/>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                    <div class="col-12 p-0 text-center">
                                        <img src="<?php echo base_url('assets/soutenons/logo_vivresaville.png'); ?>"
                                             alt="logo" id="vsv_img_main_logo" class="img-fluid"/>
                                    </div>
                                <?php } ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
    </div>
    </div>

<script>
    function change_class(){
        var content_get = $('#content_navbar').html();

        if(content_get != 'clear'){
            $('#content_navbar').html('clear');
        }else{
            $('#content_navbar').html('dehaze');
        }
    }
</script>


    <style>
        @media only screen and (min-width: 992px) {
            .menu-mobile{
                display: none;
            }
        }
        @media only screen and (max-width: 600px) {
            .menu-mobile{
                display: block;
            }
        }
        .menu-mobile{
            margin-top: 45px;
        }
        .navbar-collapse.collapse,
        .navbar-collapse.collapsing{
            position: absolute;
            width: 90%;
            left: 15px;
            top: 85px;
            z-index: 100;
        }
        .span_menu:hover{
            color: white!important;
        }
        .span_menu{
            color: white!important;
            text-decoration: none!important;
            font-family: 'Futura-LT-Book';
            font-style: normal!important;
        }
        .menus_2{
            padding: 0!important;
            padding-top: 10px!important;
            padding-bottom: 10px!important;
        }
        .navbar{
            margin-bottom: 0px!important;s
        }
        .ville_name_class{
            background-color: #e80eae;
            color: #ffffff;
            font-size: 17px;
            border-radius: unset;
            width: 95%;
            margin: auto;
            padding-top: 5px;
            padding-bottom: 5px;
        }
        .content_checkmark{
            left: -55px;
            padding-left: 0px;
        }
        .checkmark{
            left: 93px;
        }
        @media (max-width: 600px){
            .col-lgs-2,
            .col-lgsi-2{
                -webkit-box-flex: 0;
                -webkit-flex: 0 0 50%!important;
                -ms-flex: 0 0 50%!important;
                flex: 0 0 50%!important;
                max-width: 50%!important;
                padding-right: 15px!important;
                padding-left: 15px!important;
                padding-bottom: 15px!important;
                margin: auto!important;
            }
            #footer_section_copyright{
                padding-left: 15px;
            }
        }
        .content_image_overflow img{
            width: 100%;
        }

    </style>
<?php } ?>