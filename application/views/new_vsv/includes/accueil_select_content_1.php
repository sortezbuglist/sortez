<style>
    .img-fluid.img-24{
    width: 212px;
    height: 212px;
    object-fit: cover;
    /*margin-top: 80px; */
    }
    .img-fluid.img-242{
    width: 212px;
    height: 212px;
    object-fit: cover;
    /*margin-top: 80px; */
    }

    @media screen and (max-width: 600px){
        span .titre{
                line-height: 1em;
                text-align: center;
                font-size: 20px;
        }
        .img-fluid.img-242{
            display: none;
        }
        .img-fluid.img-24{
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
            margin-top: 20px;
        }
        .floating-btn{
            right: 20px!important;
            bottom: 40px!important;
        }
        .dropup-content{
            right: 15px!important;
            bottom: 85px!important;
        }
        .dropup:hover .dropup-content {
            display: block!important;
        }
        .content_blocker{
            height: auto;
            padding-bottom: 10px;
        }
        .h24{
            font-size: 30px;
        }
    }
</style>
<?php if(isset($type_page) && $type_page == ""){ ?>
<div class="container container_select_ville">
    <div class="row mt-5">
        <div id="ni_title">
        <h2 class="font_2" style="font-size:30px; line-height:1em; text-align:center">
            <span style="color:#E80EAE">
               <span style="font-size:30px">
                   <span style="font-style:italic">
                       <span style="font-family:'Libre-Baskerville-Italic'">
                           <span  class="titre" style="letter-spacing:1px">Rassembler et dynamiser l'attractivité commerciale, associative et institutionnelle de votre ville ou d'un territoire sur un principe de "tout-en-un" !...</span>
                       </span>
                   </span>
               </span> 
            </span>
        </h2>  
        </div>


        <ul class="list_text">
            <li>
                Vivresaville.fr est un concept de communication mutualisé, fédérateur, innovant et complet ;
            </li>
            <li>
                Il est basé sur la création de sites web locaux, personnalisés élaborés en collaboration avec les collectivités locales ou territoriales et les associations de la ville ;
            </li>
            <li>
                Chaque site Vivresaville.fr ouvre gratuitement le référencement de l'ensemble des acteurs locaux (collectivités, associations, professionnels) du secteur référencé (commune ou intercommunalité, communauté de communes), il suffit de préciser le ou les codes postaux s'y rapportant ;
            </li>
            <li>
                Il est possible d'adapter le service et la tarification en fonction de la volonté, de la politique et de la participation de chaque collectivité ;
            </li>
            <li>
                En fonction de l'engagement des collectivités, les acteurs peuvent bénéficier d'une gratuité totale ou partielle pour certains abonnements ;
            </li>
            <li>
                Sur un compte personnel et sécurisé, les acteurs locaux déposent et diffusent leurs données en temps réel et d’un simple clic ;
            </li>
            <!--<li>
                Vivresaville.fr favorise leur accès à des outils marketing, clés en mains, gratuits ou optionnels, personnels ou groupés dont certains sont habituellement utilisés par les grandes enseignes ;
            </li>
            <li>
                Vivresaville.fr leur ouvre gratuitement la porte du web et de la mobilité (site web, QRCODE, e.boutique…) ;
            </li>-->
            <li>
                La base de données, outils et les services qui en découlent sont ceux présentés sur le site du magazine sortez ;
            </li>
            <li>
                Les informations de l'ensemble des acteurs sont centralisés et référencés en temps réel sur le site de votre ville, mais aussi sur celui du magazine et sur notre market place "soutenonslecommercelocal.fr".
            </li>
        </ul>
        <div class="col-md-3 col-sm-6 mb-4">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/covid.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Relancer une initiative économique malgré la crise pour votre ville
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 mb-4">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/drapeaufrance.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Mettre en avant l'actualité institutionnelle
                        de la collectivité.
                        L'ensemble de l'actualité de la ville est transféré automatiquement sur ce site avec la technique du webscrapping
                    </p>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 mb-4">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/puzzle.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Sur un compte personnel et sécurisé, les acteurs locaux déposent et diffusent leurs données en temps réel et d’un simple clic sur des champs préconfigurés sans faire appel
                        à un webmaster
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 mb-4">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/jaime.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        ​Ouvrir gratuitement les portes du web à tous les acteurs de votre ville avec la gestion de leurs données
                        en temps réel
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 mb-4">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/fuse.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        ​Apporter aux commerces locaux des outils marketings utilisés habituellement
                        par les grandes enseignes
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 mb-4">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/commerce.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Booster la vie économique des
                        professionnels
                        e-boutique - réservation livraison à domicile
                        click & collect - deals...
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 mb-4">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/aider.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Dynamiser la vie associative
                        Sport - loisir - culture actualité - agenda
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 mb-4">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/shooping.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Stimuler le sentiment d'appartenance
                        de vos administrés !
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="row" style="height: 320px;">
                <div class="col-sm-3">
                    <img class="img-fluid img-24" src="<?php echo base_url('assets/vivresaville/sortez/24heure.png') ?>">
                </div>
                <div class="col-sm-6" style="    margin: auto;">
                    <p class="font_8" style="font-size:35px; line-height:1.2em; text-align:center">
                      <span style="font-style:italic">
                          <span style="font-family:Libre-Baskerville,serif">
                              <span style="font-size:35px">
                                  <span class="h24" style="color:#E80EAE">D&egrave;s la signature du partenariat,</br>

                                    le site est mis en ligne</span>
                              </span>
                          </span>
                      </span>  
                    </p>
                    <p class="font_8" style="font-size:35px; line-height:1.2em; text-align:center">
                        <span style="font-size:45px">
                            <span style="font-style:italic">
                                <span  style="font-family:Libre-Baskerville,serif">
                                    <span class="h24" style="color:#E80EAE">sous 24 heures !...</span>
                                </span>
                            </span>
                        </span>
                    </p>
                </div>
                <div class="col-sm-3">
                    <img class="img-fluid img-242" src="<?php echo base_url('assets/vivresaville/sortez/24heure.png') ?>">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container container_select_ville">
<!--        <h1 class="title_select_ville_categ">-->
<!--            Découvrez les avantages<br/>-->
<!--            pour les catégorie d'acteurs ...-->
<!--        </h1>-->
    <!--<div class="row">
        <div class="col-md-4 col-sm-6 mb-3">
            <div class="box_shadow_ville_select">
                <div class="container">
                    <div class="row">
                        <img class="img-fluid w-100" src="<?php //echo base_url('assets/vivresaville/admin.png') ?>">
                    </div>
                </div>
                <div class="container content_white">
                    <p class="title_block_categ">Les administrés</p>
                    <a href="https://www.soutenonslecommercelocal.fr/vivresaville-administres" class="btn btn_block_categ1">
                        Détails
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 mb-3">
            <div class="box_shadow_ville_select">
                <div class="container">
                    <div class="row">
                        <img class="img-fluid w-100" src="<?php //echo base_url('assets/vivresaville/associations.png') ?>">
                    </div>
                </div>
                <div class="container content_white">
                    <p class="title_block_categ">Les associations et les professionnels</p>
                    <a href="https://www.soutenonslecommercelocal.fr/abonnement-premium" class="btn btn_block_categ1">
                        Détails
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6 mb-3">
            <div class="box_shadow_ville_select">
                <div class="container">
                    <div class="row">
                        <img class="img-fluid w-100" src="<?php //echo base_url('assets/vivresaville/local.png') ?>">
                    </div>
                </div>
                <div class="container content_white">
                    <p class="title_block_categ">Les collectivités locales & territoriales</p>
                    <a href="https://www.soutenonslecommercelocal.fr/vivresaville-collectivites" class="btn btn_block_categ1">
                        Détails
                    </a>
                </div>
            </div>
        </div>
    </div>-->
</div>
<?php } ?>
<div class="content_block">
    <div class="container container_select_ville mt-5 back_categ">
        <h1 class="title_select_ville_categ">
            Les données déposées par les différents acteurs<br/>
            se retrouvent sur des annuaires fédérateurs
        </h1>
        <div class="row">
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="container pb-3 content_white_mobile">
                    <h3 class="title_bloc_rubriques">Les bonnes adresses</h3>
                    <p class="text_blockrubriques">Accédez aux pages de présentation<br/> des collectivités, commerce,  services, loisirs<br/> dans votre ville!</p>
                    <img class="img-fluid w-100" src="<?php echo base_url('assets/vivresaville/sortez/adresse.png') ?>">
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="container pb-3 content_white_mobile">
                    <h3 class="title_bloc_rubriques">L'actualité</h3>
                    <p class="text_blockrubriques">Accédez aux articles récents<br/> déposés en ligne <br/>par les partenaires de votre ville</p>
                    <img class="img-fluid w-100" src="<?php echo base_url('assets/vivresaville/sortez/actualite.png') ?>">
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="container pb-3 content_white_mobile">
                    <h3 class="title_bloc_rubriques">L'agenda événementiel</h3>
                    <p class="text_blockrubriques">Accédez aux  événements de votre ville<br/> (salons, concerts, spectacles,<br/>théâtres, sports ...)</p>
                    <img class="img-fluid w-100" src="<?php echo base_url('assets/vivresaville/sortez/agenda.png') ?>">
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="container pb-3 content_white_mobile">
                    <h3 class="title_bloc_rubriques">Les boutiques en ligne</h3>
                    <p class="text_blockrubriques">Accédez aux annonces<br/> commerciales déposées<br/> par les partenaires de votre ville

                    </p>
                    <img class="img-fluid w-100" src="<?php echo base_url('assets/vivresaville/sortez/boutique.png') ?>">
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="container pb-3 content_white_mobile">
                    <h3 class="title_bloc_rubriques">Les deals et la fidélité</h3>
                    <p class="text_blockrubriques">Les partenaires de votre ville<br/> vous proposent des actions<br/> promotionnelles</p>
                    <img class="img-fluid w-100" src="<?php echo base_url('assets/vivresaville/sortez/deal.png') ?>">
                </div>
            </div>
            <div class="col-md-4 col-sm-6 mb-3">
                <div class="container pb-3 content_white_mobile">
                    <h3 class="title_bloc_rubriques">La mobilité intégrée</h3>
                    <p class="text_blockrubriques">30% des achats en ligne<br/> sont réalisés via un mobile<br/> une carte de fidélité est intégrée</p>
                    <img class="img-fluid w-100" src="<?php echo base_url('assets/vivresaville/sortez/mobile.png') ?>">
                </div>
            </div>
        </div>
    </div>
</div>
