<style>
  .height_fixe{
    height: 100%;
  } 
  .col-md-4.col-sm-6{
    margin-bottom: 35px;
  }

  #comp-kldglvln{
	padding-bottom: 38px;
	line-height: 22px;
   	padding-bottom: 38px;
    text-align: justify;
    font-size: 17px;
    margin-top: 1px;
	margin-bottom: 0px;
	padding-top: 22px;

    }
		
    #bgLayers_comp-kldhbtwo{
		text-align: center;
		margin-top: 40px;
	}
	
    .tittre_h3{
		text-align: center;
		color:#E80EAE;
		font-family:libre baskerville,serif;
		font-size: 40px;
		font-style: italic;
	}
	
    #comp-kldglv5i{
		margin-top: 40px;
	}

		.accueil_select_content_1, .accueil_select_content_2{
			width: 288px;
			height: 95px;
		}

		dl, ol, ul {
			margin-top: 0;
			margin-bottom: 1rem;
			font-family: Futura-LT-Book, Sans-Serif;
			font-size: 17px;
			margin-bottom: 25px;
		}

		h3 {
			text-align: center;
			color: #E80EAE;
			font-family: libre baskerville,serif;
			font-size: 35px;
			font-style: italic;
		}

		h2 {
			font-size: 2rem;
			font-style: italic;
			color: #E80EAE;
			font-size: 40px;
			line-height: 0.9em;
			text-align: center;
		}
	
	@media screen and (max-width: 767px ){

		#bgLayers_comp-kldhbtwo img{
			width: 100%!important;
			height: 100%!important;
			
		}

		#ordi1 {
			margin-bottom: 17px;
			margin-left : 60px!important;
		}

		#acces1 {
			background-color: transparent;
			margin-top: 0%;
			margin-left: 30%;
			position: relative!important;
    		top: 0px!important;
		}

		#ordi2 {
			display: block;
			margin-left: 0px !important;
		}

		#acces2 {
			background-color: transparent;
			position: relative;
			top: 350px !important;
			margin-top: 0px;
			left: 25%;
			bottom: 98px;
		}

		#logo1 {
			margin-left: 50px !important;
		}

		#text1 {
			display: block;
			width: 280px !important;
			margin-top: 50px !important;
			margin-left: 25px !important;
		}

		#logo2 {
			margin-left: 60px;
			margin-top: 70px;
		}

		#logo2 img{
			display: none;
		}

		h2 {
			font-size:30px !important;
		}

		h3 {
			font-size: 20px !important;
		}

		#ordi3 {
			display: block;
			margin-bottom: 20px;
			margin-left: 0px !important;
		}

		#acces3 {
			background-color: transparent;
			position: relative !important;
			top: -10px;
			left: 100px !important;
		}

		#acces4 {
			position: relative !important;
			top: 348px !important;
			left: 25%;
		}

		#ordi4 {
			display: block;
			margin-bottom: 20%;
			margin-left: 0px !important;
		}

		#det {
			margin-left: 90px !important;
    		width: 150px !important;
		}

		#det2 {
			margin-left: 90px !important;
    		width: 150px !important;
		}
	}


</style>
</br> 
<div class="container container_select_ville">
    <div id="bgLayers_comp-kldhbtwo">
		<img src="https://static.wixstatic.com/media/5fa146_5a5ababda084456b8cfceb23641b26b0~mv2.jpg/v1/fill/w_614,h_204,al_c,lg_1,q_80/5fa146_5a5ababda084456b8cfceb23641b26b0~mv2.webp" alt="main.jpg" style="width: 900px; height: 326px; object-fit: cover; object-position: 50% 50%;">
	</div>
    <div class="container container_select_ville">
        <div id="comp-kldglv5i">
			<h2>Le principe d'un partenariat avec les collectivit&eacute;s</h2>
			<h2>ou les associations de commer&ccedil;ants...</h2>
		</div> 
        <div id="comp-kldglvln">
			<ul class="font_8">
				<li><p class="font_8"><span> "Vivresaville.fr" est un concept de communication mutualis&eacute;, f&eacute;d&eacute;rateur, innovant et complet ;</span></p></li>
				<li><p class="font_8"><span>Il est bas&eacute; sur la cr&eacute;ation de sites web locaux, personnalis&eacute;s et &eacute;labor&eacute;s en partenariat avec les collectivit&eacute;s locales ou territoriales et/ou les associations ou f&eacute;d&eacute;rations de commer&ccedil;ants des territoires concern&eacute;s ;
				</span></p></li>
				<li><p class="font_8"><span>Les bases de donn&eacute;es sont li&eacute;es &agrave un market-place innovant avec des outils marketings sur le web qui en d&eacute;coulent ;</span></p></li>
				<li><p class="font_8"><span>Ce m&ecirc;me dispositif est d&eacute;j&agrave; utilis&eacute; sur le site d&eacute;partemental du magazine Sortez "www.magazine-sortez.org" et le site national "www.soutenonslecommercelocal.fr" ;
				</span></p></li>
				<li><p class="font_8"><span>Seules les conditions financi&egrave;res des abonnements seront diff&eacute;rentes et personnalis&eacute;es en fonction des engagements de chaque partenaire ;
				</span></p></li>
				<li><p class="font_8"><span>La gestion et les mises &agrave;jour sont administr&eacute;es par "vivresaville.fr".
				</span></p></li>
			</ul>
				
		</div>
    </div>
    
</div>

