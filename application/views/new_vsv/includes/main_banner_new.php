<?php
$thiss =& get_instance();
$thiss->load->library('session');
$thiss->load->model('vsv_ville_other');
$localdata_IdVille = $thiss->session->userdata('localdata_IdVille');
$localdata_IdVille_parent = $thiss->session->userdata('localdata_IdVille_parent');
$localdata_IdVille_all = $thiss->session->userdata('localdata_IdVille_all');
$thiss->load->model("vivresaville_villes");
$logo_to_show_vsv = base_url() . "assets/ville-test/wpimages/logo_ville_test.png";
if (isset($localdata_IdVille_parent) && $localdata_IdVille_parent != "" && $localdata_IdVille_parent != false) {
    $vsv_id_vivresaville = $thiss->vivresaville_villes->getByIdVille($localdata_IdVille_parent);
    if (isset($vsv_id_vivresaville) && isset($vsv_id_vivresaville->id)) {
        $vsv_object = $thiss->vivresaville_villes->getById($vsv_id_vivresaville->id);
        $vsv_other_ville = $thiss->vsv_ville_other->getByIdVsv($vsv_id_vivresaville->id);
    }
    if (isset($vsv_object) && isset($vsv_object->logo) && $vsv_object->logo != '' && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo)) {
        $logo_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/" . $vsv_object->logo;
    }else{
        $logo_to_show_vsv = base_url('assets/soutenons/logo_vivresaville.png');
    }
    if (isset($vsv_object) && isset($vsv_object->logo_droite) && $vsv_object->logo_droite!= '' && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo_droite)) {
        $logo_droite_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/".$vsv_object->logo_droite;
    }else{
        $logo_droite_to_show_vsv = base_url('assets/soutenons/logo_vivresaville.png');
    }
}
?>

<div class="container pt-4 pb-4 main_banner_container align-items-center d-none d-sm-block d-md-block d-lg-block d-xl-block">
    <div class="row">
        <div class="col-md-3 logo_ville" style="/*background-image: url('<?php echo base_url();?>/site2/wpimages/wpb5bf54e2_06.png');*/ background-repeat: no-repeat; background-position: left center; background-size: 150% auto;">
            <table>
                <tr>
                    <td>
                        <?php if($zcontentactive != 'ville_select'){?>
                        <img src="<?php echo $logo_to_show_vsv; ?>"
                             alt="logo" id="vsv_img_main_logo" class="img-fluid"/>
                        <?php }else{ ?>
                            <img src="<?php echo base_url('assets/soutenons/logo_vivresaville.png'); ?>"
                                 alt="logo" id="vsv_img_main_logo" class="img-fluid"/>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-6 text-center ">
            <table>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-12 vsv_main_ville_name_txt logo_vivresaville">
                                <?php if (isset($vsv_object->name_ville)){echo $vsv_object->name_ville;}else{ echo '<img src="'.base_url("assets/vivresaville/sortez/select_ville2.png").'" class="img_fluid">'; }  ?>
                            </div>
                            <?php if($zcontentactive != 'ville_select'){?>
                            <div class="col-12 p-0 text-center">
                                <?php if (isset($vsv_other_ville) && count($vsv_other_ville) > 0) { ?>
                                    <form class="vsv_other_ville_commune_form" id="vsv_other_ville_commune_form" name="vsv_other_ville_commune_form" method="post" action="<?php  echo site_url("vivresaville/Accueil");?>">
                                        <select id="vsv_other_ville_select" name="vsv_other_ville_select"
                                                class="mb-3 p-0 form-control text-center vsv_other_ville_select ">
                                            <option value="0" <?php if (isset($localdata_IdVille_all) && $localdata_IdVille_all!="") echo "selected";?>>Toutes les communes</option>
                                            <?php foreach ($vsv_other_ville as $vsv_item) { ?>
                                                <option value="<?php echo $vsv_item->id_ville; ?>" <?php if (isset($localdata_IdVille) && $localdata_IdVille==$vsv_item->id_ville && (!isset($localdata_IdVille_all) || $localdata_IdVille_all==false)) echo "selected";?>><?php echo $vsv_item->name; ?></option>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" id="vsv_other_ville_currenturl" name="vsv_other_ville_currenturl" value="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"/>
                                    </form>
                                    <script>$(document).ready(function(){$('#vsv_other_ville_select').change(function(){ vsv_other_ville_commune_form.submit();});});</script>
                                <?php } ?>
                            </div>
                            <div class="col-12 p-0 text-center vsv_referencez_site_ville">
                                <?php
                                $ville_reference_vsv = false;
                                $thisss =& get_instance();
                                $thisss->load->model("vsv_ville_reference");
                                $thisss->load->library('session');
                                $localdata_IdVille = $thisss->session->userdata('localdata_IdVille');
                                $ip_adress = $thisss->input->ip_address();
                                $objVilleReference = $thisss->vsv_ville_reference->getWhere(' ip="'.$ip_adress.'" and id_ville='.$localdata_IdVille);
                                if (isset($objVilleReference) && count($objVilleReference)>0) {
                                    $ville_reference_vsv = true;
                                }
                                ?>
                                <form id="form_id_ville_reference_vsv" class="form_id_ville_reference_vsv m-auto" method="post" name="form_id_ville_reference_vsv" action="<?php echo site_url("vivresaville/Accueil");?>">
<!--                                    <input type="checkbox" id="id_ville_reference_vsv" name="id_ville_reference_vsv" class="id_ville_reference_vsv mt-1" --><?php //if($ville_reference_vsv) echo 'checked';?><!-- onclick="fn_id_ville_reference_vsv();"/>-->
                                    <input type="hidden" name="check_id_ville_reference_vsv" id="check_id_ville_reference_vsv" value="<?php if($ville_reference_vsv) echo '1'; else echo '2';?>">
<!--                                    <label class="reference_site">Référencez ce site</label>-->
                                    <label class="content_checkmark reference_site">
                                        Référencez ce site
                                        <input type="checkbox" id="id_ville_reference_vsv" name="id_ville_reference_vsv" class="id_ville_reference_vsv mt-1" <?php if($ville_reference_vsv) echo 'checked';?> onclick="fn_id_ville_reference_vsv();"/>
                                        <span class="checkmark"></span>
                                    </label>
                                </form>
                                <script type="application/javascript">
                                    function fn_id_ville_reference_vsv() {
                                        if($('input[name="id_ville_reference_vsv"]').is(':checked')) {
                                            $("#check_id_ville_reference_vsv").val(1);
                                        } else {
                                            $("#check_id_ville_reference_vsv").val(2);
                                        }
                                        $("#form_id_ville_reference_vsv").submit();
                                    }
                                </script>
                            </div>
                            <?php }else{ ?>
                                <div class="col-12 logo_vivresaville mb-4 mt-4">
                                    <div class="row w-100">
                                        <form class="w-100 text-center" method="post" action="" name="myform">
                                            <div class="container">
                                                <input type="hidden" value="0" id="mySelect" name="id_ville_selected">
                                                <div class="dropdown_menu drop_me_check">
                                                    <div class="content_check">
                                                        <a href="#" class="span_check_ville">Choisissez votre ville ou territoire</a>
                                                    </div>
                                                    <div class="dropdown-content drop_check_list">
                                                        <?php if (isset($villes) && count($villes)>0) { ?>
                                                            <?php foreach ($villes as $ville_item) { ?>
                                                                <a class="border_none" href="javascript:void(0)" onclick="set_value_id_ville(<?php echo $ville_item->id;?>)">
                                                                    <?php echo $ville_item->name_ville;?>
                                                                </a>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <script>
                                            function set_value_id_ville(id){
                                                $('#mySelect').val(id);
                                                myform.submit();
                                            }
                                        </script>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-3 text-center logo_ville d-sm-block d-md-none d-xl-none">
            <img src="<?php echo $logo_to_show_vsv; ?>"
                 alt="logo" id="vsv_img_main_logo" class="img-fluid"/>
        </div>
        <div class="col-md-3 text-center my-auto logo_carte">
            <table>
                <tr>
                    <td>
                        <?php if($zcontentactive != 'ville_select'){?>
                            <img src="<?php echo $logo_droite_to_show_vsv; ?>"
                                 alt="logo" id="vsv_img_main_logo_droite" class="img-fluid"/>
                        <?php }else{ ?>
                            <img src="<?php echo base_url('assets/soutenons/logo_vivresaville.png'); ?>"
                                 alt="logo" id="vsv_img_main_logo_droite" class="img-fluid"/>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php if($zcontentactive == 'ville_select'){?>
<style>
    .main_banner_container .vsv_main_ville_name_txt{
        background-image: unset;
    }
    .main_banner_container .vsv_main_ville_name_txt{
        padding: 35px 25px 0 25px;
    }
    @media only screen and (max-width: 600px) {
        #logo_carte{
            display: inline-block;
        }
    }
</style>
<?php } ?>
<style>
    .main_banner_container .vsv_main_ville_name_txt{
        min-height: 155px;
    }
    .reference_site{
        font-size: 15px;
        font-family: Libre-Baskerville-Italic, Serif;
        color: #000000!important;
        text-decoration: unset!important;
    }
    .drop_check_list{
        top: 69px!important;
        left: 23px!important;
        border: 3px solid #E80EAE!important;
        border-bottom: unset!important;
        min-width: 85% !important;
        width: 90% !important;
        z-index: 1001!important;
    }
    .drop_check_list a{
        border-bottom: 3px solid #E80EAE!important;
        text-align: center;
        padding-top: 10px!important;
        padding-bottom: 10px!important;
    }
    .drop_me_check{
        width: 81%;
        margin-left: 55px;
        margin-right: 30px;
    }
    .drop_me_check:hover{
        width: 81%;
        margin-left: 60px;
        margin-right: 35px;
    }
    .drop_me_check a:hover{
        text-decoration: none!important;
    }
    .drop_me_check a{
        color: #E80EAE;
        font-size: 20px;
        font-family: Futura-LT-Book, Sans-Serif;
    }
    .content_check{
        border-radius: 30px;
        border: 3px solid #E80EAE;
        width: 100%;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .vsv_other_ville_select{
        background-color: rgba(230, 14, 136, 1);
        border-radius: unset;
        border-color: rgba(230, 14, 136, 1);
        color: white;
        max-width: 255px;
    }
    .vsv_other_ville_select:hover{
        background-color: white;
        color: black;
    }
    .vsv_other_ville_select  input{
        background-color: white!important;
        color: black!important;
    }
    #mySelect{
        border: 2px solid #E80EAE;
        border-radius: 25px;
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: center;
        font-size: 20px;
        color: #E80EAE;
        margin-left: 40px;
    }
    #mySelect:hover{
        border: 2px solid #000000;
    }
    /* The container */
    .content_checkmark {
        display: block;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .content_checkmark input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
        height: 0;
        width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 10px;
        height: 17px!important;
        width: 17px!important;
        border: 2px solid #E80EAE;
        border-radius: unset!important;
        background-color: #ffffff!important;
    }

    /* On mouse-over, add a grey background color */
    .content_checkmark:hover input ~ .checkmark {
        background-color: #ffffff;
    }

    /* When the checkbox is checked, add a blue background */
    .content_checkmark input:checked ~ .checkmark {
        background-color: #ffffff;
        border: 2px solid #E80EAE;
        border-radius: unset;
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .content_checkmark input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .content_checkmark .checkmark:after {
        left: 3px;
        top: 0px;
        width: 7px;
        height: 10px;
        border: solid #E80EAE;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
</style>