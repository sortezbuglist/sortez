<div class="" id="footer_section_copyright" style="padding-right:15px;height: auto!important">
    <div style="background-color:#fff;">
        <div class="row text-center">
<!--            <div class="col-lg-12">-->
<!--                <img src="--><?php //echo base_url('/assets/soutenons/icon_phone.png');?><!--" style="margin-top: 20px;" ><br>-->
<!--                <img src="--><?php //echo base_url('/assets/soutenons/num_phone.png');?><!--" style="margin-bottom: 14px;">-->
<!--            </div>-->
<!--            <div class="col-lg-6 offset-lg-3 pt-4">-->
<!--                <div class="row">-->
<!--                    <div class="col-lg-3 offset-lg-2 pt-2">-->
<!--                        <div class="fs1shareButton" style="cursor: pointer">-->
<!--                            <span class="fs1icon"></span>-->
<!--                            <span class="fs1label">Partager</span>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-lg-2">-->
<!--                        <img src="--><?php //echo base_url('/assets/soutenons/facebook.webp');?><!--" style="cursor: pointer" />-->
<!--                    </div>-->
<!--                    <div class="col-lg-5 pt-2">-->
<!--                        <div class="row">-->
<!--                            <div class="col-lg-4">-->
<!--                                <img src="--><?php //echo base_url('/assets/soutenons/jaime.png') ?><!--" style="cursor: pointer" />-->
<!--                            </div>-->
<!--                            <div class="col-lg-8 p-0">-->
<!--                                <p style="text-align:justify;cursor:default">223 personnes aiment ça. Soyez le premier parmi vos amis.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <div class="col-lg-12 pt-4 mb-5" style="margin-bottom: 100px!important;">
                <p class="Corps P-3 mb-0">
                    <img src="<?php echo base_url("assets/vivresaville/sortez/select_ville2.png")?>" class="img_fluid">
                </p>
                <!-- <p class="Corps P-3 mb-4"><span class="futura_sous_title">Une réalisation du Magazine Sortez</span></p> -->
                <p class="Corps P-3 mb-4"><span class="futura_title">magazine-sortez.org - vivresaville.fr</span></p>
                <p class="Corps P-4 mb-0"><span class="futura_sous_title_2">Priviconcept SAS, 427, chemin de Vosgelade, le Mas Raoum, 06140 Vence</span></p>
                <p class="Corps P-6 mb-0"><span class="futura_sous_title_3">Siret : 820 043 693 00010 - Code NAF : 6201Z</span></p>
                <p class="Corps P-6 mb-0">
                    <a href="#" class="futura_sous_title_3" style="text-decoration: underline; color: #E80EAE;font-size:15px;">Conditions générales - Mentions légales</a>
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Return to Top -->
<a href="javascript:void(0);" id="return-to-top"><i class="icon-chevron-up"></i></a>
<!-- ICON NEEDS FONT AWESOME FOR CHEVRON UP ICON -->
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">


<div class="container p-0 vsv_top_page_container_main d-sm-none d-md-none d-xl-none">
    <div class="vsv_top_page_btn_main container p-0 w-100">
        <div class="container p-0">
            <div class="row m-0">
                <div class="col top_link_btn"><a href="javascript:void(0);" id="vsv_top_page_btn_main_btn"><img alt="" src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcontop.png" class="img-fluid" width="30"></a></div>
                <div class="col top_link_txt"><a href="javascript:void(0);" id="vsv_top_page_btn_main_txt">Haut de page</a></div>
            </div>
        </div>
    </div>
</div>
<!-- ----------------------------------------------------------dropup------------------------ -->
<!-- <div class="dropup">
  <img src="<?php //echo base_url('assets/vivresaville/sortez/circles.gif') ?>" class=" dropbtn material-icons floating-btn">
  <div class="dropup-content">

    <a class="resp-sharing-button__link" href="https://twitter.com/intent/tweet/?text=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking.&amp;url=http%3A%2F%2Fsharingbuttons.io" target="_blank" rel="noopener" aria-label="">
    <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm5.26 9.38v.34c0 3.48-2.64 7.5-7.48 7.5-1.48 0-2.87-.44-4.03-1.2 1.37.17 2.77-.2 3.9-1.08-1.16-.02-2.13-.78-2.46-1.83.38.1.8.07 1.17-.03-1.2-.24-2.1-1.3-2.1-2.58v-.05c.35.2.75.32 1.18.33-.7-.47-1.17-1.28-1.17-2.2 0-.47.13-.92.36-1.3C7.94 8.85 9.88 9.9 12.06 10c-.04-.2-.06-.4-.06-.6 0-1.46 1.18-2.63 2.63-2.63.76 0 1.44.3 1.92.82.6-.12 1.95-.27 1.95-.27-.35.53-.72 1.66-1.24 2.04z"/></svg>
    </div>
  </div>
</a> -->
<!-- <a class="resp-sharing-button__link" href="https://facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsharingbuttons.io" target="_blank" rel="noopener" aria-label="">
  <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm3.6 11.5h-2.1v7h-3v-7h-2v-2h2V8.34c0-1.1.35-2.82 2.65-2.82h2.35v2.3h-1.4c-.25 0-.6.13-.6.66V9.5h2.34l-.24 2z"/></svg>
    </div>
  </div>
</a> -->
<!-- Sharingbutton Tumblr -->


<!-- Sharingbutton E-Mail -->
<!-- <a class="resp-sharing-button__link" href="mailto:?subject=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking.&amp;body=http%3A%2F%2Fsharingbuttons.io" target="_self" rel="noopener" aria-label="">
  <div class="resp-sharing-button resp-sharing-button--email resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm8 16c0 1.1-.9 2-2 2H6c-1.1 0-2-.9-2-2V8c0-1.1.9-2 2-2h12c1.1 0 2 .9 2 2v8z"/><path d="M17.9 8.18c-.2-.2-.5-.24-.72-.07L12 12.38 6.82 8.1c-.22-.16-.53-.13-.7.08s-.15.53.06.7l3.62 2.97-3.57 2.23c-.23.14-.3.45-.15.7.1.14.25.22.42.22.1 0 .18-.02.27-.08l3.85-2.4 1.06.87c.1.04.2.1.32.1s.23-.06.32-.1l1.06-.9 3.86 2.4c.08.06.17.1.26.1.17 0 .33-.1.42-.25.15-.24.08-.55-.15-.7l-3.57-2.22 3.62-2.96c.2-.2.24-.5.07-.72z"/></svg>
    </div>
  </div>
</a> -->

<!-- Sharingbutton Pinterest -->
<!-- <a class="resp-sharing-button__link" href="https://pinterest.com/pin/create/button/?url=http%3A%2F%2Fsharingbuttons.io&amp;media=http%3A%2F%2Fsharingbuttons.io&amp;description=Super%20fast%20and%20easy%20Social%20Media%20Sharing%20Buttons.%20No%20JavaScript.%20No%20tracking." target="_blank" rel="noopener" aria-label="">
  <div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solidcircle">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 0C5.38 0 0 5.38 0 12s5.38 12 12 12 12-5.38 12-12S18.62 0 12 0zm1.4 15.56c-1 0-1.94-.53-2.25-1.14l-.65 2.52c-.4 1.45-1.57 2.9-1.66 3-.06.1-.2.07-.22-.04-.02-.2-.32-2 .03-3.5l1.18-5s-.3-.6-.3-1.46c0-1.36.8-2.37 1.78-2.37.85 0 1.25.62 1.25 1.37 0 .85-.53 2.1-.8 3.27-.24.98.48 1.78 1.44 1.78 1.73 0 2.9-2.24 2.9-4.9 0-2-1.35-3.5-3.82-3.5-2.8 0-4.53 2.07-4.53 4.4 0 .5.1.9.25 1.23l-1.5.82c-.36-.64-.54-1.43-.54-2.28 0-2.6 2.2-5.74 6.57-5.74 3.5 0 5.82 2.54 5.82 5.27 0 3.6-2 6.3-4.96 6.3z"/></svg>
    </div>
  </div>
</a> -->

<!-- Sharingbutton Twitter -->


 <!--  </div>
</div> -->
<style>
    .fs1label{
        line-height: 21px;
        margin-left: 22px;
        padding: 3px 6px;
        color: #fff;
        font-size: 12px;
        text-shadow: 1px 1px 1px #304871;
        border-left: 1px solid #6176a3;
    }
    .fs1icon {
        width: 21px;
        height: 21px;
        background: url(https://static.parastorage.com/services/skins/2.1229.80/images/wysiwyg/core/themes/base/facebooklogo.png) 5px 3px no-repeat;
        border-right: 1px solid #425e85;
        position: absolute;
    }
    .fs1shareButton {
        position: relative!important;
        left: 48%;
        width: 80px;
        height: 22px;
        border-radius: 2px;
        border-width: 1px;
        border-style: solid;
        border-color: #9aabc6 #6f83ad #6176a3;
        background: url(https://static.parastorage.com/services/skins/2.1229.80/images/wysiwyg/core/themes/base/bg_fbshare.png) 0 0 repeat-x;
    }
    .futura_title{
        font-size: 20px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        color: #000000;
        letter-spacing: 0.03em;
        text-align: center;
    }
    .futura_sous_title{
        font-size: 21px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        letter-spacing: 0.03em;
        text-align: center;
        color:#E80EAE;
    }
    .futura_sous_title_2{
        font-size: 18px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        color: #000000;
        letter-spacing: 0.03em;
        text-align: center;
    }
    .futura_sous_title_3{
        font-size: 15px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        color: #000000;
        letter-spacing: 0.03em;
        text-align: center;
    }
    .futura_title_vivre{
        font-size: 40px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        letter-spacing: 0.03em;
        text-align: center;
        color: #E80EAE;
    }
    /*---------------------------dropup style---------------------------------------*/
    .dropup-content {
  display: none;
  position: fixed;
  z-index: 1;
  right: 75px;
  bottom: 70px;
}

.dropup-content a {
  text-decoration: none;
  display: block;
}

.dropup:hover .dropup-content {
  display: inline-flex;
  right: 135px;
  bottom: 15px;
}

.floating-btn{
  width: 50px;
  height: 50px;
  background-color: grey;
  opacity: 0.5;
  display: flex;
  align-items: center;
  justify-content: center;
  text-decoration: none;
  border-radius: 50%;
  color: white;
  font-size: 35px;
  box-shadow: 2px, 2px, 5px, rgba(0,0,0,0.5);
  position: fixed;
  right: 80px;
  bottom: 20px;
  transition: background 0.25s;
 outline: blue;
 border: : none;
 cursor: pointer;


}
.floating-btn:active{
  background-color: #007d63;
}
/*-------------------------------------------------------------------------------*/
   .resp-sharing-button__link,
.resp-sharing-button__icon {
  display: inline-block
}

.resp-sharing-button__link {
  text-decoration: none;
  color: #fff;
  margin: 0.5em
}

.resp-sharing-button {
  border-radius: 50px;
  transition: 25ms ease-out;
  padding: 0.5em 0.75em;
  font-family: Helvetica Neue,Helvetica,Arial,sans-serif;
  font-size: 20px;
}

.resp-sharing-button__icon svg {
  width: 1em;
  height: 1em;
  margin-right: 0.4em;
  vertical-align: top
}

.resp-sharing-button--small svg {
  margin: 0;
  vertical-align: middle
}

/* Non solid icons get a stroke */
.resp-sharing-button__icon {
  stroke: #fff;
  fill: none
}

/* Solid icons get a fill */
.resp-sharing-button__icon--solid,
.resp-sharing-button__icon--solidcircle {
  fill: #fff;
  stroke: none
}

.resp-sharing-button--twitter {
  background-color: #55acee
}

.resp-sharing-button--twitter:hover {
  background-color: #2795e9
}

.resp-sharing-button--pinterest {
  background-color: #bd081c
}

.resp-sharing-button--pinterest:hover {
  background-color: #8c0615
}

.resp-sharing-button--facebook {
  background-color: #3b5998
}

.resp-sharing-button--facebook:hover {
  background-color: #2d4373
}



.resp-sharing-button--reddit {
  background-color: #5f99cf
}

.resp-sharing-button--reddit:hover {
  background-color: #3a80c1
}

.resp-sharing-button--google {
  background-color: #dd4b39
}

.resp-sharing-button--google:hover {
  background-color: #c23321
}

.resp-sharing-button--linkedin {
  background-color: #0077b5
}

.resp-sharing-button--linkedin:hover {
  background-color: #046293
}

.resp-sharing-button--email {
  background-color: #777
}

.resp-sharing-button--email:hover {
  background-color: #5e5e5e
}

.resp-sharing-button--xing {
  background-color: #1a7576
}

.resp-sharing-button--xing:hover {
  background-color: #114c4c
}

.resp-sharing-button--whatsapp {
  background-color: #25D366
}

.resp-sharing-button--whatsapp:hover {
  background-color: #1da851
}

.resp-sharing-button--hackernews {
background-color: #FF6600
}
.resp-sharing-button--hackernews:hover, .resp-sharing-button--hackernews:focus {   background-color: #FB6200 }

.resp-sharing-button--vk {
  background-color: #507299
}

.resp-sharing-button--vk:hover {
  background-color: #43648c
}

.resp-sharing-button--facebook {
  background-color: #3b5998;
  border-color: #3b5998;
}

.resp-sharing-button--facebook:hover,
.resp-sharing-button--facebook:active {
  background-color: #2d4373;
  border-color: #2d4373;
}

.resp-sharing-button--twitter {
  background-color: #55acee;
  border-color: #55acee;
}

.resp-sharing-button--twitter:hover,
.resp-sharing-button--twitter:active {
  background-color: #2795e9;
  border-color: #2795e9;
}


.resp-sharing-button--email {
  background-color: #777777;
  border-color: #777777;
}

.resp-sharing-button--email:hover,
.resp-sharing-button--email:active {
  background-color: #5e5e5e;
  border-color: #5e5e5e;
}

.resp-sharing-button--pinterest {
  background-color: #bd081c;
  border-color: #bd081c;
}

.resp-sharing-button--pinterest:hover,
.resp-sharing-button--pinterest:active {
  background-color: #8c0615;
  border-color: #8c0615;
}

</style>

