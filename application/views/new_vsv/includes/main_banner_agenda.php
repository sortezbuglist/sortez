<?php

$thiss =& get_instance();

$thiss->load->library('session');

$thiss->load->model('vsv_ville_other');

$main_width_device = $thiss->session->userdata('main_width_device');

$main_mobile_screen = false;

if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device) <= 991) {

    $main_mobile_screen = true;

} else {

    $main_mobile_screen = false;

}

$thiss->load->model("vivresaville_villes");

$logo_to_show_vsv = base_url() . "assets/ville-test/wpimages/logo_ville_test.png";

if (isset($localdata_IdVille_parent) && $localdata_IdVille_parent != "" && $localdata_IdVille_parent != false) {

    $vsv_id_vivresaville = $thiss->vivresaville_villes->getByIdVille($localdata_IdVille_parent);

    if (isset($vsv_id_vivresaville) && isset($vsv_id_vivresaville->id)) {

        $vsv_object = $thiss->vivresaville_villes->getById($vsv_id_vivresaville->id);

        $vsv_other_ville = $thiss->vsv_ville_other->getByIdVsv($vsv_id_vivresaville->id);

    }

    if (isset($vsv_object) && isset($vsv_object->logo) && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo)) {

        $logo_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/" . $vsv_object->logo;

    }

}

?>



<div class="mt-lg-5 mb-lg-5 main_banner_container_soutenons">

    <div class=""

         style="background-color: white!important;padding-top: 10px;padding-bottom: 25px">

        <div class="col-lg-12">

            <p class="gros_titre">

                L'agenda événementiel

            </p>

        </div>

        <div class="col-lg-12 text-center" >

                <div class="container_select_ville m-auto">

                    <div class="row">

                        <div class="container">

                            <div class="row" >

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <div class="dropdown">

                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons"

                                             type="text"

                                             id="Recherches_par_partenaire"

                                             data-toggle="dropdown"

                                             aria-haspopup="true"

                                             aria-expanded="false">

                                            <span class="d-none d-sm-inline-block">Recherche par partenaire</span>

                                            <span class="d-contents d-sm-none d-md-none d-ld-none d-xl-none">Partenaire</span>

                                        </div>

                                        <div style="font-size:12px;"

                                             class="dropdown-menu main_communes_select"

                                             id="div_Recherches_par_partenaire"

                                             aria-labelledby="Recherches_par_partenaire">

                                            <?php

                                            if (isset($tocommercant) && $tocommercant != "" && $tocommercant != 0 && $tocommercant != "0") {

                                                foreach ($tocommercant as $oCommercant) {

                                                    ?>

                                                    <a class="dropdown-item <?= $oCommercant->nbcommercant ?>"

                                                       id="partenaire_select_id_<?= $oCommercant->IdCommercant ?>"

                                                       href="javascript:void(0);"

                                                       onclick="javascript:change_commercant_select(<?php echo $oCommercant->IdCommercant; ?>);"><?php echo $oCommercant->NomSociete; // $oCommercant->nbCommercant ?></a>

                                                    <?php

                                                }

                                            }

                                            ?>

                                        </div>

                                    </div>

                                </div>

                                <div class="col-sm-12 col-lg-4 text-left pt-2">

                                    <input class="border_content_soutenons"

                                           id="zMotcle_value"

                                           type="text"

                                           placeholder="Recherches par mot clé"

                                           value="<?php if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; ?>">

                                </div>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <div class="dropdown">

                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons"

                                             type="text"

                                             id="dropdownMenuButton_com_categ"

                                             data-toggle="dropdown"

                                             aria-haspopup="true"

                                             aria-expanded="false">

                                            <span class="d-none d-sm-inline-block">Recherche par catégorie</span>

                                            <span class="d-contents d-sm-none d-md-none d-ld-none d-xl-none">Catégorie</span>

                                        </div>

                                        <div style="font-size:12px;"

                                             class="dropdown-menu main_communes_select"

                                             id="span_leftcontener2013_form_article"

                                             aria-labelledby="dropdownMenuButton_com">

                                            <?php if (isset($toCategorie_principale) && $toCategorie_principale != '' && $toCategorie_principale != null) { ?>

                                                <?php foreach ($toCategorie_principale as $toctages) { ?>

                                                    <a class="dropdown-item <?= $toctages->nb_article ?>"

                                                       id="liste_categorie_agenda_<?php echo $toctages->agenda_categid; ?>"

                                                       href="javascript:void(0);"

                                                       onclick="javascript:change_categ_filters(<?php echo $toctages->agenda_categid; ?>);"><?php echo $toctages->category;//$toctages->nb_article ?></a>

                                                <?php } ?>

                                            <?php } ?>

                                        </div>

                                    </div>

                                </div>

                        <div class="container">
                            <div class="row" id="contect_ceckbox">

                                <div class="" id="checkbox1">

                                    <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(101);">

                                        <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"

                                            id="optionsRadios_0" value="0"

                                            <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '101') { ?>checked<?php } ?>>

                                        Aujourd'hui

                                    </label>



                                </div>

                                <div class="" id="checkbox2" style="margin-left:50px;">



                                    <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(303);">

                                        <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"

                                            id="optionsRadios_1" value="1"

                                            <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '303') { ?>checked<?php } ?>>

                                        Cette semaine

                                    </label>



                                </div>

                                <div class="" id="checkbox3" style="margin-left:45px">



                                    <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(202);">

                                        <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"

                                            id="optionsRadios_2" value="2"

                                            <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '202') { ?>checked<?php } ?>>

                                        Ce Week-end

                                    </label>



                                </div>

                                <div class="" id="checkbox4" style="margin-left:30px;">



                                    <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(404);">

                                        <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"

                                            id="optionsRadios_3" value="3"

                                            <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '404') { ?>checked<?php } ?>>

                                        Semaine prochaine

                                    </label>



                                </div>

                            </div> 
                        </div>
                                <div class="offset-lg-2 col-sm-12 col-lg-4 text-left pt-2">

                                    <input class="btn btn-secondary border_content_soutenons"

                                           id="date_debut"

                                           type="text"

                                           placeholder="Date de début"

                                           onfocus="(this.type='date')"

                                           value="">

                                </div>

                                <div class="col-sm-12 col-lg-4 text-left pt-2">

                                    <input class="btn btn-secondary border_content_soutenons"

                                           id="date_fin"

                                           type="text"

                                           placeholder="Date de fin"

                                           onfocus="(this.type='date')"

                                           value="">

                                </div>

                                <input type="hidden" name="inputStringQuandHidden" id="inputStringQuandHidden">

                            </div>

                            <div class="row">

                                <div class="col-sm-12 col-lg-4 offset-lg-2 pt-2">

                                    <div class="btn btn-secondary reinit_soutenons_down w-100"

                                         onclick="javascript:btn_re_init_agenda_list(); btn_re_init_annuaire_list();">

                                        Réinitialisez

                                    </div>

                                </div>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <div class="btn btn-secondary applic_soutenons_down w-100"

                                         onclick="javascript:form_submit_search_agenda_soutenons();"

                                    >

                                        Appliquez

                                        votre

                                        choix

                                    </div>

                                    <script type="text/javascript">

    

    function annuaire_filter_avantage(filter_value=0){

        jQuery("#inputStringQuandHidden").val(filter_value);

        // jQuery("#frmRecherchePartenaire").submit();

    }

</script>

                                    <script type="text/javascript">

                                        function form_submit_search_agenda_soutenons(keyword_value) {

                                            var base_url_visurba = '<?php echo site_url();?>';

                                            jQuery("#id_mainbody_main").html('<div class="text-center w-100"><img src="' + base_url_visurba + 'application/resources/front/images/wait.gif" alt="loading...."/></div>');

                                            $("#frmRecherchePartenaire #zMotCle").val($("#zMotcle_value").val());



                                            var inputStringDepartementHidden_partenaires = document.getElementById("inputStringDepartementHidden_partenaires").value;

                                            var inputStringVilleHidden_partenaires = document.getElementById("inputStringVilleHidden_partenaires").value;

                                            var inputIdCommercant = document.getElementById("inputIdCommercant").value;

                                            var inputagenda_article_type_id = document.getElementById("inputagenda_article_type_id").value;

                                            var inputagenda_categorie = document.getElementById("inputagenda_categorie").value;

                                            // var inputStringHidden = document.getElementById("inputStringHidden").value;

                                            // var inputStringHiddenSubCateg = document.getElementById("inputStringHiddenSubCateg").value;

                                            // var inputStringOrderByHidden = document.getElementById("inputStringOrderByHidden").value;

                                            var zMotCle = document.getElementById("zMotcle_value").value;



                                            var date_debut = document.getElementById("date_debut").value;



                                            var date_fin = document.getElementById("date_fin").value;

                                            var inputStringQuandHidden = document.getElementById("inputStringQuandHidden").value;

                                            // var inputAvantagePartenaire = document.getElementById("inputAvantagePartenaire").value;

                                            // var input_is_actif_coronna = document.getElementById("input_is_actif_coronna").value;

                                            // var input_is_actif_command = document.getElementById("input_is_actif_command").value;



                                            //alert(inputStringDepartementHidden_partenaires);



                                            jQuery.ajax({

                                                method: "POST",

                                                url: '<?php echo site_url("agenda/liste/?content_only_list=1");?>',

                                                data: {

                                                    inputStringDepartementHidden: inputStringDepartementHidden_partenaires,

                                                    inputStringVilleHidden: inputStringVilleHidden_partenaires,

                                                    inputIdCommercant: inputIdCommercant,

                                                    inputagenda_article_type_id: inputagenda_article_type_id,

                                                    inputStringHidden: inputagenda_categorie,

                                                    zMotCle: zMotCle,

                                                    inputStringDatedebutHidden: date_debut,

                                                    inputStringDatefinHidden: date_fin,

                                                     inputStringQuandHidden: inputStringQuandHidden,

                                                    // input_is_actif_command: input_is_actif_command

                                                },

                                                dataType: 'html',

                                                success: function (html) {

                                                    jQuery('#id_mainbody_main').html(html);

                                                }

                                            });



                                            //$("#frmRecherchePartenaire").submit();

                                        }

                                        function btn_re_init_agenda_list(){

                                            document.getElementById("inputagenda_categorie").value = "";

                                            document.getElementById("inputStringDepartementHidden_partenaires").value = "";

                                            document.getElementById("zMotcle_value").value = "";

                                            document.getElementById("inputIdCommercant").value = "";

                                            document.getElementById("date_debut").value = "";

                                            document.getElementById("date_fin").value = "";

                                            document.getElementById("inputStringVilleHidden_partenaires").value = "";

                                            $("#dropdownMenuButton_com_categ").html("Recherche par catégories");

                                            $("#div_Recherches_par_medias").html("Recherche par médias");

                                            $("#Recherches_par_departement").html("Recherches par departements");

                                            $("#Recherches_par_partenaire").html("Recherches par partenaires");

                                            $("#dropdownMenuButton_com").html("Recherche par communes");

                                            form_submit_search_agenda_soutenons();

                                        }

                                    </script>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>



        </div>

    </div>

</div>

<style type="text/css">

    [type="radio"] {

    margin-left: -20px !important;

    margin-top: 6px;

}

#contect_ceckbox{
    justify-content: center !important;
    margin-top: 6px;
}

.form-check-label{



    font-size: 16px;

    color: #E80EAE;

}

</style>





<script type="text/javascript">

    function change_categ_filters(id_categ) {

        document.getElementById("inputagenda_categorie").value = id_categ;

        $("#dropdownMenuButton_com_categ").html($("#liste_categorie_agenda_" + id_categ).html());

    }

    function change_media_select(id_media) {

        document.getElementById("inputagenda_article_type_id").value = id_media;

        $("#div_Recherches_par_medias").html($("#articleType_select_id_" + id_media).html());

    }

    function change_department_select(iddepart){

        document.getElementById("inputStringDepartementHidden_partenaires").value = iddepart;

        $("#Recherches_par_departement").html($("#department_select_id_" + iddepart).html());

    }

    function change_commercant_select(idcom){

        document.getElementById("inputIdCommercant").value = idcom;

        $("#Recherches_par_partenaire").html($("#partenaire_select_id_" + idcom).html());

    }

    function form_submit_search_annuaire_soutenons(keyword_value) {

        var base_url_visurba = '<?php echo site_url();?>';

        jQuery("#Id_main_bodylistannuaire_main").html('<div class="text-center w-100"><img src="' + base_url_visurba + 'application/resources/front/images/wait.gif" alt="loading...."/></div>');

        $("#frmRecherchePartenaire #zMotCle").val($("#zMotcle_value").val());



        var inputStringDepartementHidden_partenaires = document.getElementById("inputStringDepartementHidden_partenaires").value;

        var inputStringVilleHidden_partenaires = document.getElementById("inputStringVilleHidden_partenaires").value;

        var inputIdCommercant = document.getElementById("inputIdCommercant").value;

        var inputagenda_article_type_id = document.getElementById("inputagenda_article_type_id").value;

        var inputagenda_categorie = document.getElementById("inputagenda_categorie").value;

        // var inputStringHidden = document.getElementById("inputStringHidden").value;

        // var inputStringHiddenSubCateg = document.getElementById("inputStringHiddenSubCateg").value;

        // var inputStringOrderByHidden = document.getElementById("inputStringOrderByHidden").value;

        var zMotCle = document.getElementById("zMotcle_value").value;

        // var inputAvantagePartenaire = document.getElementById("inputAvantagePartenaire").value;

        // var input_is_actif_coronna = document.getElementById("input_is_actif_coronna").value;

        // var input_is_actif_command = document.getElementById("input_is_actif_command").value;





        jQuery.ajax({

            method: "POST",

            url: '<?php echo site_url("article/liste/?content_only_list=1");?>',

            data: {

                inputStringDepartementHidden_partenaires: inputStringDepartementHidden_partenaires,

                inputStringVilleHidden_partenaires: inputStringVilleHidden_partenaires,

                inputIdCommercant: inputIdCommercant,

                inputagenda_article_type_id: inputagenda_article_type_id,

                inputagenda_categorie: inputagenda_categorie,

                zMotCle: zMotCle,

                // input_is_actif_command: input_is_actif_command

            },

            dataType: 'html',

            success: function (html) {

                jQuery('#id_mainbody_main').html(html);

            }

        });



        //$("#frmRecherchePartenaire").submit();

    }

</script>

<script type="text/javascript">

    function show_current_subcategcontent_soutenons(id_sousrubrique) {

        $("#inputStringHiddenSubCateg").val(id_sousrubrique);

        //$("#frmRecherchePartenaire").submit();

        //$("#div_subcateg_annuaire_contents").css("display","none");

        $("#dropdownMenuButton_com_sucateg").html($("#vsv_sub_categmain_" + id_sousrubrique).html());

    }

</script>