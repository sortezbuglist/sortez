<?php $data["zTitle"] = 'Vivresaville';?>

<?php $this->load->view("new_vsv/includes/main_header", $data); ?>
<?php $this->load->view("new_vsv/includes/main_menu", $data); ?>
<?php $this->load->view("new_vsv/includes/main_banner", $data); ?>
<?php $this->load->view("new_vsv/includes/main_rubrique_menu", $data); ?>

<?php
if (isset($link) && $link != ""){
    $link_iframe = 'http://www.vivresaville.fr/site/'.$link;
?>
<div class="container" style="background-color: #ffffff;">
    <div class="row">
        <div class="col-12 p-0 pt-3">
            <div class="embed-responsive embed-responsive-1by1" style="height: 7180px;">
                <iframe src="<?php echo $link_iframe; ?>" class="w-100 border-0" style="min-height: 7200px;"></iframe>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php $this->load->view("new_vsv/includes/main_footer_link", $data); ?>
<?php $this->load->view("new_vsv/includes/main_footer_copyright", $data); ?>
<?php $this->load->view("new_vsv/includes/main_footer", $data); ?>
