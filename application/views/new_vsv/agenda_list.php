<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>
<div class="">
    <style>
        .col-lgs-2{
            -webkit-box-flex: 0;
            -webkit-flex: 0 0 18%;
            -ms-flex: 0 0 18%;
            flex: 0 0 18%;
            float: left;
        }
        @media screen and (max-width: 575px) {
            .col-lgs-2 {max-width: 100%;}
        }
        @media screen and (min-width: 576px) and (max-width:599px) {
            .col-lgs-2 {max-width: 50%;}
        }
        @media screen and (min-width: 600px) and (max-width:767px) {
            .col-lgs-2 {max-width: 46%;}
        }
        @media screen and (min-width: 768px) and (max-width:991px) {
            .col-lgs-2 {max-width: 31%;}
        }
        @media screen and (min-width: 992px) and (max-width:1199px) {
            .col-lgs-2 {max-width: 23%;}
        }
        @media screen and (min-width:1200px) {
            .col-lgs-2 {max-width: 18.4%;}
        }
    </style>
    <div id="mob_grey2" class="pb-3 col" style="/*background-color: #ffffff;*/">
        <div id="id_mainbody_main" class="main_body_list w-100">
            <?php } ?>

            <script type="text/javascript">
                var iCategorieId_array = [];
                <?php
                $toAgenda['iCategorieId'] = (array)$toAgenda['iCategorieId'];
                for ($i = 0; $i <  count($toAgenda['iCategorieId']); $i++) {
                ?>
                iCategorieId_array.push("<?php echo $toAgenda['iCategorieId'][$i];?>");
                <?php
                }
                ?>
                jQuery.ajax({
                    type: "POST",
                    url: "<?php echo site_url("agenda/agendaGetFilter"); ?>",
                    data: {
                        iCategorieId: iCategorieId_array,
                        iVilleId: "<?php echo $toAgenda['iVilleId'];?>",
                        iDepartementId: "<?php echo $toAgenda['iDepartementId'];?>",
                        zMotCle: "<?php echo $toAgenda['zMotCle'];?>",
                        iSousCategorieId: "<?php echo $toAgenda['iSousCategorieId'];?>",
                        page_pagination: "<?php echo $toAgenda['page_pagination'];?>",
                        per_page: "<?php echo $toAgenda['per_page'];?>",
                        iOrderBy: "<?php echo $toAgenda['iOrderBy'];?>",
                        inputStringQuandHidden: "<?php echo $toAgenda['inputStringQuandHidden'];?>",
                        inputStringDatedebutHidden: "<?php echo $toAgenda['inputStringDatedebutHidden'];?>",
                        inputStringDatefinHidden: "<?php echo $toAgenda['inputStringDatefinHidden'];?>",
                        inputIdCommercant: "<?php echo $toAgenda['inputIdCommercant'];?>"
                    },
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        //alert(data[0].id);
                        //alert(data.length);
                        if(data.length>0){
                            for (i = 0; i < data.length; i++) {
                                jQuery.post(
                                    "<?php echo site_url("agenda/agendaGetListItem/");?>",
                                    {
                                        datetime_id: data[i].datetime_id,
                                        agenda_id: data[i].id
                                    },
                                    function (response) {
                                        $("#id_mainbody_main").append(response);
                                    });
                            }
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            </script>

            <!--- ALL AGENDA ITEM LIST HERE --->

            <?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
            } else { ?>
        </div>
    </div>
</div>
<?php } ?>


<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>

    <div class="container pb-3" style="display:table;">
        <script type="text/javascript">
            jQuery(document).ready(function() {
                var win = jQuery(window);
                //jQuery('#loading_agenda_adresses').hide();
                var PerPage_init = jQuery('#PerPage_agenda_adresses').val();
                var filter_array = [];
                var iiii = 0;

                // Each time the user scrolls

                // End of the document reached?
                $("#plus").click(function(){


                    //if (jQuery(document).scrollTop() + jQuery(window).height() == getDocHeight()) {
                    //jQuery('#loading_agenda_adresses').show();
                    var PerPage = jQuery('#PerPage_agenda_adresses').val();
                    var test_filtering = $.inArray(PerPage, filter_array); //alert(String(test_filtering)+' / '+String(iiii));

                    if (test_filtering=='-1') {
                        filter_array[iiii] = PerPage;
                        jQuery.ajax({
                            url: '<?php echo site_url("agenda/liste/");?>'+String(PerPage)+'?content_only_list=1',
                            dataType: 'html',
                            success: function(html) {
                                jQuery('#id_mainbody_main').append(html);
                                //jQuery('#loading_agenda_adresses').hide();
                                jQuery('#PerPage_agenda_adresses').val(parseInt(PerPage)+parseInt(PerPage_init));
                            }
                        });
                    } else {
                        //jQuery('#loading_agenda_adresses').hide();
                    }
                    iiii = iiii + 1;
                });

            });
            function getDocHeight() {
                var D = document;
                return Math.max(
                    D.body.scrollHeight, D.documentElement.scrollHeight,
                    D.body.offsetHeight, D.documentElement.offsetHeight,
                    D.body.clientHeight, D.documentElement.clientHeight
                );
            }
        </script>

        <div id="loading_agenda_adresses" class="" style="text-align:center;">
            <button id="plus" class="btn btn-success" style="color:white;font-familly: Futura Md;margin-top: 50px">voir plus</button>
        </div>
        <input id="PerPage_agenda_adresses" type="hidden" value="<?php if (isset($PerPage)) echo $PerPage; else echo "0"; ?>"/>
    </div>

<?php } ?>
