<div class="container mt-5 pt-3 mb-5 pb-5">
    <section class="mx-auto mt-2 mb-5" style="width:45%">
        <p class="text-center mb-5" style="font-family:Libre-Baskerville, Serif;font-style: italic;font-size: 2.5vw;">Nous contacter par mail</p>
        <form action="" method="post">
            <div class="form-group mt-5">
                <input type="text" class="form-control" placeholder="Nom de l'établissement"></br>
                <input type="text" class="form-control" placeholder="Nom,Prénom"></br>
                <input type="text" class="form-control"  placeholder="Email"></br>
                <input type="text" class="form-control" placeholder="Téléphone direct*">
            </div>
            <h6>Votre statut:</h6>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" id="ch1">
                <label class="form-check-label" for="ch1">Particulier</label></br>
                <input class="form-check-input" type="checkbox" id="ch2">
                <label class="form-check-label" for="ch2">Commerçants - artisans - PME</label></br>
                <input class="form-check-input" type="checkbox" id="ch3">
                <label class="form-check-label" for="ch3">Association</label></br>
                <input class="form-check-input"  type="checkbox" id="ch4">
                <label class="form-check-label" for="ch4">Collectivité</label></br>
                <input class="form-check-input" type="checkbox" id="ch5">
                <label class="form-check-label" for="ch5">Organisateur d'événements</label></br>
            </div>
            </br>
            <h6>Vos centres d'intérêt:</h6>
            <div class="form-check">
                <input class="form-check-input"  type="checkbox" id="ch6">
                <label class="form-check-label" for="ch6">"La publicité sur le magazine Sortez"</label></br>
                <input class="form-check-input" type="checkbox" id="ch7">
                <label class="form-check-label"for="ch7">"Le site web du magazine Sortez"</label></br>
                <input class="form-check-input" type="checkbox" id="ch8">
                <label class="form-check-label" for="ch8">"soutenonslecommercelocal;fr"</label></br>
                <input class="form-check-input" type="checkbox" id="ch9">
                <label class="form-check-label" for="ch9">"vivresaville.fr"</label></br>
                <input class="form-check-input" type="checkbox" id="ch10">
                <label class="form-check-label" for="ch10">"La Création d'un site web"</label></br></br>
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="Commentaires"></br>
            </div>
            <div>
                <input type="submit" class="btn  btn-block"value="Validation" style="background-color:#FF1493; font-weight: bold;color: white; ">
        </form>
    </section>
</div>