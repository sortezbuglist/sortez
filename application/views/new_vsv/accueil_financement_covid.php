<div class="row m-0">
    <img src="<?php echo base_url('assets/vivresaville/sortez/top_covid.png') ?>" class="img-fluid w-100">
</div>
<div class="container container_select_ville">
    <div class="row mt-5">
        <h1 class="fat_title w-100 mb-5">
            Les conditions financières <br/>
            du partenarait "Spécial COVID"
        </h1>

        <ul class="list_text">
            <li>
                Les collectivités ou les associations ou fédérations de commerçants valident un partenariat de 6 mois renouvelable ;
            </li>
            <li>
                Pendant cette période, la structure partenaire reversent à vivresaville une redevance mensuelle qui varie en fonction du nombre d'habitants du territoire référencé ;
            </li>
            <li>
                Les professionnels (associations, commerçants, artisans, PME) disposent d'un abonnement Premium entièrement gratuit avec l'intégration d'outils choisis offerts dont vous avez le détail ci-dessous.
            </li>
        </ul>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="height_fixe box_shadow_ville_select">
                    <div class="container">
                        <div class="row content_image">
                            <img src="<?php echo base_url('assets/vivresaville/sortez/premium.png') ?>" class="img-fluid w-100">
                        </div>
                    </div>
                    <div class="container text-center">
                        <p class="description_abonnement_new">Le site vitrine Premium

                            avec sa page de présentation</p>
                        <a href="https://www.soutenonslecommercelocal.fr/abonnement-premium" class="btn btn_block_categ1">
                            Détails
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="height_fixe box_shadow_ville_select">
                    <div class="container">
                        <div class="row content_image">
                            <img src="<?php echo base_url('assets/vivresaville/sortez/prem_module.png') ?>" class="img-fluid w-100">
                        </div>
                    </div>
                    <div class="container text-center">
                        <p class="description_abonnement_new">Le site vitrine Premium

                            avec les modules actualité

                            et agenda</p>
                        <a href="https://www.soutenonslecommercelocal.fr/pack-infos" class="btn btn_block_categ1">
                            Détails
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="height_fixe box_shadow_ville_select">
                    <div class="container">
                        <div class="row content_image">
                            <img src="<?php echo base_url('assets/vivresaville/sortez/prem_modl_bout.png') ?>" class="img-fluid w-100">
                        </div>
                    </div>
                    <div class="container text-center">
                        <p class="description_abonnement_new">Le site vitrine Premium

                            avec le module e-boutique</p>
                        <a href="https://www.soutenonslecommercelocal.fr/pack-boutiques" class="btn btn_block_categ1">
                            Détails
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="height_fixe box_shadow_ville_select">
                    <div class="container">
                        <div class="row content_image">
                            <img src="<?php echo base_url('assets/vivresaville/sortez/prem_vente.png') ?>" class="img-fluid w-100">
                        </div>
                    </div>
                    <div class="container text-center">
                        <p class="description_abonnement_new">Le site vitrine Premium

                            avec le module de vente en ligne</p>
                        <a href="https://www.soutenonslecommercelocal.fr/formulaire-vente" class="btn btn_block_categ1">
                            Détails
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="height_fixe box_shadow_ville_select">
                    <div class="container">
                        <div class="row content_image">
                            <img src="<?php echo base_url('assets/vivresaville/sortez/prem_modl_deal.png') ?>" class="img-fluid w-100">
                        </div>
                    </div>
                    <div class="container text-center">
                        <p class="description_abonnement_new">Le site vitrine Premium

                            avec les modules

                            Deals & fidélité</p>
                        <a href="https://www.soutenonslecommercelocal.fr/pack-promo" class="btn btn_block_categ1">
                            Détails
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="height_fixe box_shadow_ville_select">
                    <div class="container">
                        <div class="row content_image">
                            <img src="<?php echo base_url('assets/vivresaville/sortez/prem_gites.png') ?>" class="img-fluid w-100">
                        </div>
                    </div>
                    <div class="container text-center">
                        <p class="description_abonnement_new">Le site vitrine Premium

                            avec le pack Gîtes</p>
                        <a href="https://www.soutenonslecommercelocal.fr/pack-gites" class="btn btn_block_categ1">
                            Détails
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h1 class="fat_title w-100 mt-5 mb-5">
                Le barème mensuel
            </h1>
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="content_blocker">
                    <div class="col-sm-12 text-center pt-4">
                        <img src="<?php echo base_url('assets/vivresaville/sortez/accord.png') ?>" class="img-fluid image_border_set">
                    </div>
                    <div class="col-sm-12 pt-4">
                        <p class="descri_blocker">
                            Territoire de moins

                            de 10 000 habitants
                        </p>
                        <p class="price_bloc">1 000€ ht</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="content_blocker">
                    <div class="col-sm-12 text-center pt-4">
                        <img src="<?php echo base_url('assets/vivresaville/sortez/accord.png') ?>" class="img-fluid image_border_set">
                    </div>
                    <div class="col-sm-12 pt-4">
                        <p class="descri_blocker">
                            Territoire de moins

                            de 50 000 habitants
                        </p>
                        <p class="price_bloc">1 500€ ht</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="content_blocker">
                    <div class="col-sm-12 text-center pt-4">
                        <img src="<?php echo base_url('assets/vivresaville/sortez/accord.png') ?>" class="img-fluid image_border_set">
                    </div>
                    <div class="col-sm-12 pt-4">
                        <p class="descri_blocker">
                            Territoire de moins

                            de 100 000 habitants
                        </p>
                        <p class="price_bloc">2 000€ ht</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 mb-4">
                <div class="content_blocker">
                    <div class="col-sm-12 text-center pt-4">
                        <img src="<?php echo base_url('assets/vivresaville/sortez/accord.png') ?>" class="img-fluid image_border_set">
                    </div>
                    <div class="col-sm-12 pt-4">
                        <p class="descri_blocker">
                            Territoire de plus

                            de 100 000 habitants
                        </p>
                        <p class="price_bloc">3 000€ ht</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h1 class="fat_title w-100 mt-5 mb-5">
                L'engagement de vivresaville
            </h1>
            <ul class="list_text">
                <li>
                    Vivresaville crée et personnalise un site web fédérateur personnalisé (images et logo) affilié à notre concept, ce site est accessible depuis un ordinateur, une tablette et un mobile ;
                </li>
                <li>
                    Nous fournissons une adresse web personnalisé : monterritoire.vivresaville.fr
                </li>
                <li>
                    Sur le lien suivant vous pouvez vous rendre compte de la qualité graphique d'un site test : <a href="https://www.soutenonslecommercelocal.fr/fonctions-complementaires" class="acces" style="text-decoration: underline"> accés</a>
                </li>
                <li>
                    Le référencement sur ce site est gratuit pour tous les acteurs commerciaux et associatifs dont le siège social appartient à ce territoire. Il leur suffira sur un compte personnel et sécurisé de déposer leurs données (images, textes, liens...) dans des champs préconfigurés sans faire appel à un webmaster.
                </li>
                <li>
                    L'accès aux outils marketings précisés ci-dessus sont offerts, il suffira à chaque professionnel de préciser lors de la souscription les modules qu'il désire utiliser.
                </li>
                <li>
                    Par la suite chaque professionnel peut enrichir son abonnement de base en ajoutant des fonctions complémentaires optionnelles. Une remise de 20% sera appliquée sur le tarif de référence : <a href="https://www.soutenonslecommercelocal.fr/fonctions-complementaires" class="acces" style="text-decoration: underline"> accés</a>
                </li>
                <li>
                    Sur chaque secteur, nous nous engageons à trouver parmi les commerçants un correspondant local, il sera notre animateur, notre conseiller. Il pourra avec un forfait étudié aider et même remplacer le commerçant dans son référencement.
                </li>
                <li>
                    Ce conseiller pourra gratuitement organiser avec l'aide de la collectivité des réunions de formation.
                </li>
            </ul>
        </div>
    </div>
</div>
<?php $this->load->view("new_vsv/includes/accueil_select_content_1", $data);
$this->load->view("new_vsv/includes/accueil_select_content_3", $data); ?>
<style>
    .price_bloc{
        color:#E80EAE;
        font-size:25px;
        font-family: Libre-Baskerville, serif;
        text-align: center;
    }
    .content_blocker{
        height: 350px!important;
    }
    .descri_blocker{
        font-size: 17px!important;
    }
    .box_shadow_ville_select{
        box-shadow: 3px 3px 5px 2px #ccc;
        padding-bottom: 15px;
    }
    .height_fixe{
        height: 380px;
        background: #ffffff;
        margin-bottom: 15px;
    }
    .content_image{
        height: 205px;
        position: relative;
    }
    .description_abonnement_new{
        padding-top: 15px;
        overflow: hidden;
        height: 85px;
        margin-bottom: 30px;
        font-size: 19px;
        font-family: Libre-Baskerville, Serif;
        text-align: center;
        line-height: 1.1em;
        color:#E60E88;
    }
    .list_text li{
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 17px;
        margin-bottom: 25px;
    }
    .container_select_ville{
        width: 925px;
    }
</style>