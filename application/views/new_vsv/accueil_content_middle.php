
<div class="container accueil_content_middle container_select_ville pl-0 pr-0">
    <div class="rowmt-5">
        <h1 class="fat_title w-100 mb-4">
            Les avantages de vivresaville.fr
        </h1>
        <div class="petit">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/covid.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Relancer une initiative économique malgré la crise pour votre ville
                    </p>
                </div>
            </div>
        </div>
        <div class="petit">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/drapeaufrance.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Mettre en avant l'actualité institutionnelle
                        de la collectivité.
                        L'ensemble de l'actualité de la ville est transféré automatiquement sur ce site avec la technique du webscrapping
                    </p>
                </div>
            </div>
        </div>
        <div class="petit">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/puzzle.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Sur un compte personnel et sécurisé, les acteurs locaux déposent et diffusent leurs données en temps réel et d’un simple clic sur des champs préconfigurés sans faire appel
                        à un webmaster
                </div>
            </div>
        </div>
        <div class="petit">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/jaime.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        ​Ouvrir gratuitement les portes du web à tous les acteurs de votre ville avec la gestion de leurs données
                        en temps réel
                </div>
            </div>
        </div>
        <div class="petit">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/fuse.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        ​Apporter aux commerces locaux des outils marketings utilisés habituellement
                        par les grandes enseignes
                </div>
            </div>
        </div>
        <div class="petit">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/commerce.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Booster la vie économique des
                        professionnels
                        e-boutique - réservation livraison à domicile
                        click & collect - deals...
                </div>
            </div>
        </div>
        <div class="petit">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/aider.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Dynamiser la vie associative
                        Sport - loisir - culture actualité - agenda
                </div>
            </div>
        </div>
        <div class="petit">
            <div class="content_blocker">
                <div class="col-sm-12 text-center pt-4">
                    <img src="<?php echo base_url('assets/vivresaville/sortez/shooping.png') ?>" class="img-fluid image_border_set">
                </div>
                <div class="col-sm-12 pt-4">
                    <p class="descri_blocker">
                        Stimuler le sentiment d'appartenance
                        de vos administrés !
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="footer1">
    <div class="col-sm-12 text-center" style="margin-bottom: -15%; padding-top: 4%;  width: 98%;">
        <h1 class="medium_title w-100 pt-4 pb-4">
            Professionnels & associations,<br/>
            bénéficiez de l'offre négociée avec votre commune<br/>
            pour référencer votre établissement !
        </h1>
        <!-- <div class="col-sm-12 text-center mt-5"> -->
        <a href="https://www.soutenonslecommercelocal.fr/abonnement-vivresaville" class="btn btn_block_categ_other">
            Détails
        </a>
    <!-- </div> -->
     </div>
        <div class="" style=" width: 500px;      margin-left: 57%;">
                        <img src="https://www.vivresaville.fr/application/views/front_soutenons/ordi-woman1.png" class="img-fluid">
                    </div>
   



    
    </div>

<style>
.petit {
    display: inline-flex;
    width: 23%;
        margin-right: 12px;
    margin-bottom: 12px;
}
.rowmt-5 {
    text-align: center;
}
.footer1{
    background-color: #3d9be9;
    margin-top: 5rem;
}
.medium_title{
    color: white!important;
}
@media screen and (max-width: 1199px) and (min-width:  769px){

    .petit {
    display: inline-flex;
    width: 20%;
     margin-right: 12px;
    margin-bottom: 12px;
}
}
</style>