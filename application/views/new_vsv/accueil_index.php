<?php $data["zTitle"] = 'Accueil - Vivresaville';?>

<?php
$thisss =& get_instance();
$thisss->load->library('session');
$main_width_device = $thisss->session->userdata('main_width_device');
$main_mobile_screen = false;
if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device)<=991){
    $main_mobile_screen = true;
} else {
    $main_mobile_screen = false;
}
$data["zcontentactive"] = 'accueil';
?>

    <?php $this->load->view("new_vsv/includes/main_header", $data); ?>
    <?php $this->load->view("new_vsv/includes/main_menu", $data); ?>
    <?php $this->load->view("new_vsv/includes/main_menu", $data); ?>
    <?php $this->load->view("new_vsv/includes/header_right", $data); ?>
    <?php $this->load->view("new_vsv/includes/main_banner_new", $data); ?>
    <?php $this->load->view("new_vsv/includes/header_top", $data);?>
    <?php $this->load->view("new_vsv/includes/mobile_menu", $data);?>



    <style type="text/css">
        .containercontainer {
            width: 100%;
            padding-right: 15px;
            padding-left: 15px;
            margin-right: auto;
            margin-left: auto;
        }

        @media (min-width: 576px) {
            .containercontainer {
                max-width: 540px;
            }
        }

        @media (min-width: 768px) {
            .containercontainer {
                max-width: 720px;
            }
        }

        @media (min-width: 992px) {
            .containercontainer {
                max-width: 960px;
            }
        }

        @media (min-width: 1200px) {
            .containercontainer {
                max-width: 1140px;
            }
        }
        .menus a:hover{
            text-decoration: none!important;
        }
        @media screen and (min-width: 800px) {
            .dropdown, .dropup {
                text-align: left;
            }
        }
    </style>
<!--<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">-->
<!--    <ol class="carousel-indicators">-->
<!--        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>-->
<!--        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>-->
<!--        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>-->
<!--    </ol>-->
<!--    <div class="carousel-inner">-->
<!--        <div class="carousel-item active">-->
<!--            <img class="d-block w-100" src="--><?php //echo base_url('wpimages_vsv/wp1c4febbe_06.png') ?><!--" alt="First slide">-->
<!--        </div>-->
<!--        <div class="carousel-item">-->
<!--            <img class="d-block w-100" src="--><?php //echo base_url('wpimages_vsv/wp1c4febbe_06.png') ?><!--" alt="Second slide">-->
<!--        </div>-->
<!--        <div class="carousel-item">-->
<!--            <img class="d-block w-100" src="--><?php //echo base_url('wpimages_vsv/wp1c4febbe_06.png') ?><!--" alt="Third slide">-->
<!--        </div>-->
<!--    </div>-->
<!--    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">-->
<!--        <span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
<!--        <span class="sr-only">Previous</span>-->
<!--    </a>-->
<!--    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">-->
<!--        <span class="carousel-control-next-icon" aria-hidden="true"></span>-->
<!--        <span class="sr-only">Next</span>-->
<!--    </a>-->
<!--</div>-->
<?php $this->load->view("new_vsv/accueil_content_top", $data); ?>
<?php $this->load->view("new_vsv/accueil_content_middle", $data); ?>
<?php $this->load->view("new_vsv/accueil_content_img_btn", $data); ?>
<?php //$this->load->view("new_vsv/accueil_content_pub", $data); ?>
<?php //$this->load->view("new_vsv/accueil_content_bottom", $data); ?>

<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list']=='1') {} else { ?>
    <?php $this->load->view("new_vsv/includes/main_footer_copyright", $data); ?>
<?php } ?>
