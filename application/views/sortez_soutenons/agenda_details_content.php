<?php $data["empty"] = NULL; ?>

<div class="container" style="display: table;width: 100%; background-color:#FFFFFF!important;">

    <div class="col-lg-12" style="display:table; width:100%; background-color:#FFFFFF; padding-top:20px;">

        <div class="col-lg-12 padding0 container_slide_article" style="text-align:center; padding-bottom:20px;">
            <?php $this->load->view("agenda/includes/slide_details_agenda", $data); ?>
        </div>
        <div class="col-xs-12 padding0" style="text-align:center; min-height:40px;"><a
                    href="<?php echo site_url("agenda"); ?>" class="btn_link_rose">Retour &agrave; la liste des
                événements</a></div>

        <script type="text/javascript">
            $(document).ready(function () {

                $("#contact_partner_reset").click(function () {
                    $("#contact_partner_nom").val();
                    $("#contact_partner_tel").val();
                    $("#contact_partner_mail").val();
                    $("#contact_partner_msg").val();
                    $("#spanContactPartnerForm").html('* champs obligatoires');
                });

                $("#contact_partner_send").click(function () {

                    var error = 0;
                    var contact_partner_nom = $("#contact_partner_nom").val();
                    if (contact_partner_nom == '') error = 1;
                    var contact_partner_tel = $("#contact_partner_tel").val();
                    if (contact_partner_tel == '') error = 1;
                    var contact_partner_mail = $("#contact_partner_mail").val();
                    if (contact_partner_mail == '') error = 1;

                    if (!verifier(contact_partner_mail)) error = 2;
                    var contact_partner_msg = $("#contact_partner_msg").val();
                    if (contact_partner_msg == '') error = 1;
                    $("#spanContactPartnerForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

                    if (error == 1) {
                        $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
                    } else if (error == 2) {
                        $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
                        $("#contact_partner_mail").css('border-color', '#ff0000');
                    } else {
                        $.post(
                            "<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>",
                            {
                                contact_partner_nom: contact_partner_nom,
                                contact_partner_tel: contact_partner_tel,
                                contact_partner_mail: contact_partner_mail,
                                contact_partner_msg: contact_partner_msg,
                                contact_partner_mailto: "<?php if ($oDetailAgenda->email){echo $oDetailAgenda->email;}  ?>"
                            },
                            function (data) {
                                $("#spanContactPartnerForm").html(data);
                            });
                    }
                });

            });
            function verifier(contact_partner_mail) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(contact_partner_mail);
            }


        </script>

        <!--Video content-->
        <div class="modal fade" id="divVideoPartnerAgenda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Vid&eacute;o</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div style="background-color:#000000;">
                            <table width="100%" id="tableContactPartnerForm" border="0" cellspacing="0" cellpadding="0"
                                   style="text-align:center; width:540px; height:472px; background-color:#000000;">
                                <tr>
                                    <td>
                                        <?php
                                        $link_video_club_agenda = preg_split('[v=]', $oDetailAgenda->video);
                                        if (isset($link_video_club_agenda[1])) {
                                            ?>
                                            <object width="540" height="472">
                                                <param
                                                        value="http://www.youtube.com/v/<?php echo $link_video_club_agenda[1]; ?>&autoplay=1&loop=1&showinfo=0&rel=0&fs=1&hd=1"
                                                        name="movie">
                                                <param value="true" name="allowFullScreen">
                                                <param value="always" name="allowscriptaccess">
                                                <param value="transparent" name="wmode">
                                                <embed width="540" height="472" allowfullscreen="true" allowscriptaccess="always"
                                                       wmode="transparent" type="application/x-shockwave-flash"
                                                       style="width:540px;height:472px;"
                                                       src="http://www.youtube.com/v/<?php echo $link_video_club_agenda[1]; ?>&autoplay=0&loop=1&showinfo=0&rel=0&fs=1&hd=1">
                                            </object>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>



        <!--Contact form contet-->
        <div class="modal fade" id="divContactPartnerForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nous Contacter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div style="background-color:#FFFFFF;">
                            <form name="formContactPartnerForm" id="formContactPartnerForm" action="#">
                                <div class="form-group"><input type="text" class="form-control" name="contact_partner_nom" id="contact_partner_nom" placeholder="Votre nom *"/>
                                </div>
                                <div class="form-group"><input type="text" class="form-control" name="contact_partner_tel" id="contact_partner_tel"
                                                               placeholder="Votre numéro de téléphone *"/></div>
                                <div class="form-group"><input type="email" name="contact_partner_mail" id="contact_partner_mail" class="form-control" aria-describedby="emailHelp"
                                                               placeholder="Votre courriel *"/></div>
                                <div class="form-group"><textarea class="form-control" name="contact_partner_msg" id="contact_partner_msg" placeholder="Votre message *"></textarea>
                                </div>
                                <div><span id="spanContactPartnerForm" class="text-danger">* champs obligatoires</span></div>
                                <?php if ($oDetailAgenda->email){?>
                                <div>
                                    <div class="text-right"><input type="button" class="btn btn-success" name="contact_partner_send"
                                                                   id="contact_partner_send" value="Envoyer"/></div>
                                </div>
                                <?php  } ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php
        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        //var_dump($oDetailAgenda);
        ?>


        <div class="col-lg-12 padding0">
            <div class="detail_agenda_title_container">
        <span class="oDetailAgenda_subcateg"><?php if ($oDetailAgenda->subcateg !="" AND $oDetailAgenda->subcateg!=0){echo $oDetailAgenda->subcateg;}  ?>
        </span><br/>
                <span
                        class="oDetailAgenda_nom_manifestation"><?php echo $oDetailAgenda->nom_manifestation; ?></span><br/>

                <span class="oDetailAgenda_date_debut">
        <?php if (isset($toArticle_datetime) && count($toArticle_datetime) > 0) { ?>
            <?php foreach ($toArticle_datetime as $objArticle_datetime) { ?>
                <?php
                if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00" && ($objArticle_datetime->date_debut == $objArticle_datetime->date_fin)) {
                    echo "<br/>Le " . translate_date_to_fr($objArticle_datetime->date_debut);
                    if (isset($objArticle_datetime->heure_debut) && $objArticle_datetime->heure_debut != "0:00" && $objArticle_datetime->heure_debut != "") echo " à " . str_replace(":", "h", $objArticle_datetime->heure_debut);
                } else {
                    if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") echo "<br/>Du " . translate_date_to_fr($objArticle_datetime->date_debut);
                    if (isset($objArticle_datetime->date_fin) && $objArticle_datetime->date_fin != "0000-00-00") {
                        if (isset($objArticle_datetime->date_debut) && $objArticle_datetime->date_debut != "0000-00-00") echo " au " . translate_date_to_fr($objArticle_datetime->date_fin);
                        else echo " Jusqu'au " . translate_date_to_fr($objArticle_datetime->date_fin);
                    }
                    if (isset($objArticle_datetime->heure_debut) && $objArticle_datetime->heure_debut != "0:00" && $objArticle_datetime->heure_debut != "") echo " à " . str_replace(":", "h", $objArticle_datetime->heure_debut);
                }
                ?>
            <?php } ?>
        <?php } ?>

        </span><br/>

                <span class="oDetailAgenda_nom_localisation">
    <?php
    if (isset($oDetailAgenda->location_id) && $oDetailAgenda->location_id != "0") {
        $obj_location_article_details = $this->mdl_localisation->getById($oDetailAgenda->location_id);
        if (isset($obj_location_article_details) && is_object($obj_location_article_details)) {
            if ($obj_location_article_details->location != $obj_location_article_details->location_address){
                if ($oDetailAgenda->IdCommercant != "301444"){
                    echo $obj_location_article_details->location . "<br/>" . $obj_location_article_details->location_address . " - ";
                }else{
                    echo str_replace($oDetailAgenda->codepostal_localisation,"",$obj_location_article_details->location) . "<br/>" . str_replace($oDetailAgenda->codepostal_localisation,"",$obj_location_article_details->location_address . " - ");
                }
                $obj_ville_location_details = $this->mdlville->getVilleById($obj_location_article_details->location_villeid);
                if (isset($obj_ville_location_details->Nom) && is_object($obj_ville_location_details)) {
                    if ($oDetailAgenda->IdCommercant != "301444"){
                        echo $obj_ville_location_details->CodePostal . " - ";
                    }else{
                        echo "";
                    }
                    $thisss = get_instance();
                    $thisss->load->model('mdlville');
                    $ville_nom_all = $thisss->mdlville->GetVilleByCodePostal_localisation($obj_ville_location_details->CodePostal);
                    foreach ($ville_nom_all as $ville_nom){
                        if (preg_match("/".$ville_nom->Nom."/",$obj_location_article_details->location_address)){
                            $shown =1;
                            echo $ville_nom->Nom;
                        }
                    }
                    if ($shown != 1){
                        echo $obj_ville_location_details->Nom;
                    }
                }
            }else{
                if ($oDetailAgenda->IdCommercant != "301444"){
                    echo  $obj_location_article_details->location_address."-".$oDetailAgenda->codepostal_localisation ;
                }else{
                    echo  str_replace($oDetailAgenda->codepostal_localisation,"",$obj_location_article_details->location_address) ;
                }

//                $obj_ville_location_details = $this->mdlville->getVilleById($obj_location_article_details->location_villeid);
//                if (isset($obj_ville_location_details->Nom) && is_object($obj_ville_location_details)) {
//                    echo $obj_ville_location_details->CodePostal . " - " . $thisss = get_instance();
//                    $thisss->load->model('mdlville');
//                    $ville_nom_all = $thisss->mdlville->GetVilleByCodePostal_localisation($obj_ville_location_details->CodePostal);
//                    foreach ($ville_nom_all as $ville_nom){
//                        if (preg_match("/".$ville_nom->Nom."/",$obj_location_article_details->location_address)){
//                            $shown =1;
//                            echo $ville_nom->Nom;
//                        }
//                    }
//                    if ($shown != 1){
//                        echo $obj_ville_location_details->Nom;
//                    }
//                }
            }
        }
    }elseif (isset($oDetailAgenda->adresse_localisation) && $oDetailAgenda->adresse_localisation != "0"){echo $oDetailAgenda->adresse_localisation;}
    ?>
    </span><br>
                <span style="font-size: large;"><?php if ($oDetailAgenda->IdCommercant==301299){if (isset($oDetailAgenda->IdVille) AND $oDetailAgenda->IdVille !=0){
                        $ville= $this->mdl_localisation->getlieuville($oDetailAgenda->IdVille);
                        echo  $ville->Nom ;'".<br>."';
                    }} ?></span><br>
                <?php
                $this->load->model("mdlcommercant");
                $oCommercant = $this->mdlcommercant->infoCommercant($oDetailAgenda->IdCommercant);
                ?>

                <span class="oDetailAgenda_telephone">
    <?php if ($oDetailAgenda->mobile != "") echo "<br/>Mobile. " . $oDetailAgenda->mobile; ?>
    <?php if ($oDetailAgenda->fax != "") echo "<br/>Fax. " . $oDetailAgenda->fax; ?>
    <?php if ($oDetailAgenda->telephone != "") echo "<br/>Tél. " . $oDetailAgenda->telephone; ?>
    </span>
            </div>
        </div>

    </div>

    <!--<div class="col-lg-12 title_categ_black">Description</div>-->

    <div class="col-lg-12 article_all_details_content_container" style="background-color: white!important;">
        <div class="col-lg-12 padding0 article_all_details_description_container" style="text-align:justify;"><?php echo $oDetailAgenda->description; ?></div>
        <div class="col-lg-12 text-center pt-5 pb-5">
            <a href="<?php echo site_url('agenda/partner_web_page/'.$oDetailAgenda->id); ?>" target="_blank"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/reservation_online2.png" width="150px" height="150ox" class="img-fluid" alt="reservation"/>
            </a>

        </div>
        <div class="col-lg-12" style="text-align:center; padding:15px 0 0;">
            <div class="addthis_inline_share_toolbox"></div>
        </div>

        <div class="col-lg-12" style="text-align: center; padding:15px;">Article d&eacute;pos&eacute;
            le <?php echo translate_date_to_fr($oDetailAgenda->date_depot); ?>
            par <?php echo $oInfoCommercant->NomSociete; ?></div>

        <div class="col-lg-12 padding0" style="text-align:center; margin:15px 0 40px 0;">
            <?php
            if ((isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0" && $localdata_IdVille != NULL) || (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0" && $localdata_IdDepartement != NULL)) {
                ?>
                <a href="javascript:void(0);"
                   onclick='javascript:window.open("<?php echo site_url($oCommercant->nom_url . "/article"); ?>", "Agenda", "width=1045, height=800, scrollbars=yes");'
                   title="Pages Articles" class="btn_link_rose">Acc&egrave;s direct aux articles de ce diffuseur</a>
            <?php } else { ?>
                <a href="<?php echo site_url($oCommercant->nom_url . "/agenda"); ?>" class="btn_link_rose">Acc&egrave;s
                    direct aux articles de ce diffuseur</a>
            <?php } ?>
        </div>
    </div>


    <?php if (
        (isset($oDetailAgenda->description_tarif) && $oDetailAgenda->description_tarif != "") ||
        (isset($oDetailAgenda->conditions_promo) && $oDetailAgenda->conditions_promo != "") ||
        (isset($oDetailAgenda->reservation_enligne) && $oDetailAgenda->reservation_enligne != "")
    ) { ?>

        <div class="col-lg-12 title_categ_black" style="margin-bottom:0;">TARIF ET LIEN DE R&Eacute;SERVATION EN LIGNE
        </div>

        <div class="col-lg-12" style="padding:15px 0; background-color:#FFFFFF;">
            <div>
                <div style='background-color: transparent;
        color: #000000;
        font-family: "Arial",sans-serif;
        font-size: 18.7px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 23px;
        text-decoration: none; text-align:justify;
        vertical-align: 0;'><?php echo $oDetailAgenda->description_tarif; ?></div>
                <div><?php echo $oDetailAgenda->conditions_promo; ?></div>
            </div>
            <div style="text-align:center;">
                <?php
                if ($oDetailAgenda->reservation_enligne != "") {
                    $reservation_enligne_agenda = $oDetailAgenda->reservation_enligne;
                    ?><a href="<?php echo $reservation_enligne_agenda; ?>" target="_blank"><img
                        src="<?php echo GetImagePath("privicarte/"); ?>/reservation_online.png" class="img-fluid" alt="reservation"/>
                    </a><?php
                }
                ?>
            </div>
        </div>

    <?php } ?>

    <div class="col-lg-12 title_categ_black" style="margin-top:0;">CONTACT & INFORMATIONS</div>

    <?php
    if (isset($oDetailAgenda->organiser_id) && $oDetailAgenda->organiser_id != "0") {
        $obj_organiser_article_details = $this->mdl_article_organiser->getById($oDetailAgenda->organiser_id);
        if (isset($obj_organiser_article_details) && is_object($obj_organiser_article_details)) {
            ?>
            <div class="article_case_contact_infos" style="border: 1px solid; margin: 0 auto; text-align: center; width: 500px; display: table;">Ev&eacute;nement organis&eacute; par
                <?php
                echo $obj_organiser_article_details->name . "<br/>";
                if (!preg_match("/".$obj_organiser_article_details->postal_code."/",$obj_organiser_article_details->address1)){
                    if ($oDetailAgenda->IdCommercant != "301444"){
                        echo $obj_organiser_article_details->postal_code . " - ";
                    }else{
                        echo "";
                    }

                }else{
                    echo "";
                }

                if (isset($obj_organiser_article_details->address1) && $obj_organiser_article_details->address1 != "") {
                    if ($oDetailAgenda->IdCommercant != "301444"){
                        echo $obj_organiser_article_details->address1 . " - ";
                    }else{
                        echo str_replace($oDetailAgenda->codepostal_localisation,"",$obj_organiser_article_details->address1 . " - ");
                    }

                }
                if (isset($obj_organiser_article_details->address2) && $obj_organiser_article_details->address2 != "") echo $obj_organiser_article_details->address2 . " - ";
                if (isset($obj_organiser_article_details->ville_id) && $obj_organiser_article_details->ville_id != "0"){
                    $thisss = get_instance();
                    $thisss->load->model('mdlville');
                    $ville_nom_all = $thisss->mdlville->GetVilleByCodePostal_localisation($obj_ville_location_details->CodePostal);
                    foreach ($ville_nom_all as $ville_nom){
                        if (preg_match("/".$ville_nom->Nom."/",$obj_location_article_details->location_address)){
                            $shown =1;
                            //echo $ville_nom->Nom;
                        }
                    }
                    if ($shown != 1){
                    echo $this->mdlville->getVilleById($obj_organiser_article_details->ville_id)->Nom;
                    }
                }
                if (isset($obj_organiser_article_details->tel) && $obj_organiser_article_details->tel != "") echo "<br/>Tel : " . $obj_organiser_article_details->tel;
                if (isset($obj_organiser_article_details->mobile) && $obj_organiser_article_details->mobile != "") echo "<br/>Mobile : " . $obj_organiser_article_details->mobile;
                if (isset($obj_organiser_article_details->website) && $obj_organiser_article_details->website != "") $organiser_website_page = $obj_organiser_article_details->website; else $organiser_website_page = "";//echo "<br/>Site Web : " . $obj_organiser_article_details->website;
                if (isset($obj_organiser_article_details->facebook) && $obj_organiser_article_details->facebook != "") $organiser_facebook_page = $obj_organiser_article_details->facebook; else $organiser_facebook_page = "";//echo "<br/>Facebook : " . $obj_organiser_article_details->facebook;
                if (isset($obj_organiser_article_details->twitter) && $obj_organiser_article_details->twitter != "") $organiser_twitter_page = $obj_organiser_article_details->twitter; else $organiser_twitter_page = "";//echo "<br/>Twitter : " . $obj_organiser_article_details->twitter;
                if (isset($obj_organiser_article_details->googleplus) && $obj_organiser_article_details->googleplus != "") $organiser_googleplus_page = $obj_organiser_article_details->googleplus; else $organiser_googleplus_page = "";//echo "<br/>Google+ : " . $obj_organiser_article_details->googleplus;
                ?>
            </div>
            <?php
        }
    }
    ?>

    <div class="col-lg-12 padding0">



        <div class="col-lg-12 padding0" style="text-align:center; padding-top:20px !important;">

            <ul class="contact_info_article_list">

                <?php if ($oDetailAgenda->video != "") { ?>
                    <li>
                            <a href="javascript:void(0);" id="IdVideoPartnerAgenda" title="Vidéo" data-toggle="modal" data-target="#divVideoPartnerAgenda"><img
                                    src="<?php echo GetImagePath("privicarte/"); ?>/ico_youtube.png" alt="video"/></a>
                    </li>
                <?php } ?>



                <?php if (isset($oDetailAgenda->pdf) && $oDetailAgenda->pdf != "") { ?>
                    <li>
                        <a href="<?php echo base_url() . "/application/resources/front/images/agenda/pdf/" . $oDetailAgenda->pdf; ?>"
                           target="_blank" title="<?php echo $oDetailAgenda->titre_pdf; ?>"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/ico_pdf.png" alt="pdf"/></a>
                    </li>
                <?php } ?>


                <li>
                    <a href="javascript:void(0);" id="IdContactPartnerForm" title="Contact" data-toggle="modal" data-target="#divContactPartnerForm"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/ico_msg.png"/></a>
                </li>


                <?php
                if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                } else {
                    ?>
                    <?php
                    if (isset($organiser_facebook_page) AND $organiser_facebook_page == "" && isset($oDetailAgenda->facebook) && $oDetailAgenda->facebook != "") $organiser_facebook_page = $oDetailAgenda->facebook;
                    if (isset($organiser_facebook_page) && $organiser_facebook_page != "" && $organiser_facebook_page != NULL) { ?>
                        <?php
                        if (preg_match('#https://#', $organiser_facebook_page) || preg_match('#http://#', $organiser_facebook_page)) {
                            $link_fb_to_show = $organiser_facebook_page;
                        } else {
                            $link_fb_to_show = "https://www.facebook.com/" . $organiser_facebook_page;
                        }
                        ?>
                        <li>
                            <a href="javascript:void(0);"
                               onclick='javascript:window.open("<?php echo $link_fb_to_show; ?>", "Facebook", "width=850, height=800");'
                               title="Page Facebook"><img src="<?php echo GetImagePath("privicarte/"); ?>/ico_fb.png"/></a>
                        </li>
                    <?php } ?>
                <?php } ?>



                <?php
                if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                } else {
                    if (isset($organiser_googleplus_page) && $organiser_googleplus_page != "" && $organiser_googleplus_page != NULL) { ?>
                        <?php
                        if (preg_match('#https://#', $organiser_googleplus_page) || preg_match('#http://#', $organiser_googleplus_page)) {
                            $link_gp_to_show = $organiser_googleplus_page;
                        } else {
                            $link_gp_to_show = "https://plus.google.com/" . $organiser_googleplus_page;
                        }
                        ?>
                        <li>
                            <a href="javascript:void(0);"
                               onclick='javascript:window.open("<?php echo $link_gp_to_show; ?>", "GooglePlus", "width=850, height=800");'
                               title="Page GooglePlus"><img width="95px" height="110px" src="<?php echo GetImagePath("privicarte/"); ?>/footer_link_gplus.png"/></a>
                        </li>
                    <?php } ?>
                <?php } ?>
                <?php
                if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                } else {
                    if (isset($organiser_twitter_page) AND $organiser_twitter_page == "" &&isset($oInfoCommercant) && $oInfoCommercant->google_plus != "" && $oInfoCommercant->google_plus != NULL) $organiser_twitter_page = $oInfoCommercant->google_plus;
                    if (isset($organiser_twitter_page) AND $organiser_twitter_page != "" && $organiser_twitter_page != NULL) {
                        if (preg_match('#https://#', $organiser_twitter_page) || preg_match('#http://#', $organiser_twitter_page)) {
                            $link_google_plus_to_show = $organiser_twitter_page;
                        } else {
                            $link_google_plus_to_show = "http://www.twitter.com/" . $organiser_twitter_page;
                        }
                        ?>
                        <?php if ($oDetailAgenda->twitter){ ?>
                        <li>
                            <a href="javascript:void(0);"
                               onclick='javascript:window.open("<?php echo $link_google_plus_to_show; ?>", "Facebook", "width=850, height=800");'
                               title="Page Twitter"><img src="<?php echo GetImagePath("privicarte/"); ?>/ico_twt.png"/></a>
                        </li>
                            <?php } ?>
                    <?php } ?>
                <?php } ?>

                <?php
                if (isset($organiser_website_page) AND $organiser_website_page !== "" && isset($oDetailAgenda->siteweb) && $oDetailAgenda->siteweb != "") $organiser_website_page = $oDetailAgenda->siteweb;
                else $organiser_website_page = "javascript:void(0);"; ?>
                <?php if (isset($organiser_website_page) && $organiser_website_page != '' && $organiser_website_page != null && $organiser_website_page != 'http://www.') { ?>
                    <?php if ($oDetailAgenda->siteweb){ ?>
                    <li>
                        <a href="<?php echo $organiser_website_page; ?>" title="Site web" target="_blank"><img
                                    src="<?php echo GetImagePath("privicarte/"); ?>/ico_web.png"/></a>
                    </li>
                        <?php } ?>
                <?php } ?>




                <?php

                $this->load->model("user");
                $thisss =& get_instance();
                $thisss->load->library('ion_auth');
                $this->load->model("ion_auth_used_by_club");
                $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oDetailAgenda->IdCommercant);
                if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
                if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;


                if ($thisss->ion_auth->logged_in()) {
                    $user_ion_auth = $thisss->ion_auth->user()->row();
                    $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                    if ($iduser == null || $iduser == 0 || $iduser == "") {
                        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                    }
                } else $iduser = 0;
                if (isset($iduser) && $iduser != 0 && $iduser != NULL && $iduser != "") {
                    $oCommercantFavoris = $this->user->verify_favoris($iduser, $oDetailAgenda->IdCommercant);
                }

                ?>




                <li>
                    <?php if (isset($oCommercantFavoris) && $oCommercantFavoris != NULL && $oCommercantFavoris->Favoris == "1") { ?>
                        <a href="<?php echo site_url('front/utilisateur/delete_favoris/' . $oDetailAgenda->IdCommercant); ?>" title="Supprimer de mes Favoris">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/ico_favoris.png" />
                        </a>
                    <?php } else { ?>
                        <a href="<?php echo site_url('front/utilisateur/ajout_favoris/' . $oDetailAgenda->IdCommercant); ?>" title="Ajouter à mes Favoris">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/ico_favoris.png" />
                        </a>
                    <?php } ?>
                </li>




            </ul>

        </div>

    </div>


    <?php
    $bonPlanParCommercant = $this->mdlbonplan->bonPlanParCommercant($oCommercant->IdCommercant);
    //var_dump($bonPlanParCommercant);
    if (isset($bonPlanParCommercant) && count($bonPlanParCommercant) > 0) {
        ?>

        <script type="text/javascript">
            $(document).ready(function () {
                $(".fancybox_<?php echo $oCommercant->IdCommercant; ?>").fancybox();
                $("#id_bonplan_<?php echo $oCommercant->IdCommercant; ?>").fancybox({
                    autoScale: false,
                    overlayOpacity: 0.8, // Set opacity to 0.8
                    overlayColor: "#000000", // Set color to Black
                    padding: 5,
                    width: 1055,
                    height: 800,
                    transitionIn: 'elastic',
                    transitionOut: 'elastic',
                    type: 'iframe'
                });
            });
        </script>


    <?php } ?>


    <div class="col-lg-12 title_categ_black">Adresse & Plan d'acc&egrave;s</div>


    <div class="col-lg-12 padding0">

        <div class="col-lg-12" style='background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 14px;
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: 16px;
    text-align: center;
    vertical-align: 0;'><?php //echo $oDetailAgenda->nom_localisation; ?>
            <?php
            if (isset($oDetailAgenda->location_id) && $oDetailAgenda->location_id!="0") {
                $obj_location_article_details = $this->mdl_localisation->getById($oDetailAgenda->location_id);
                if (isset($obj_location_article_details) && is_object($obj_location_article_details)) {
                    if ($obj_location_article_details->location != $obj_location_article_details->location_address ){
                        if ($oDetailAgenda->IdCommercant != "301444"){
                            echo $obj_location_article_details->location . " - " . $obj_location_article_details->location_address . " - ";
                        }else{
                            echo str_replace($oDetailAgenda->codepostal_localisation,"",$obj_location_article_details->location . " - " . $obj_location_article_details->location_address . " - ");
                        }
                        $obj_ville_location_details = $this->mdlville->getVilleById($obj_location_article_details->location_villeid);
                        if (isset($obj_ville_location_details->Nom) && is_object($obj_ville_location_details)) {
                            if ($oDetailAgenda->IdCommercant != "301444"){
                                echo $obj_ville_location_details->CodePostal . " - ";
                            }else{
                                echo  "";
                            }

                            $thisss = get_instance();
                            $thisss->load->model('mdlville');
                            $ville_nom_all = $thisss->mdlville->GetVilleByCodePostal_localisation($obj_ville_location_details->CodePostal);
                            foreach ($ville_nom_all as $ville_nom){
                                if (preg_match("/".$ville_nom->Nom."/",$obj_location_article_details->location_address)){
                                    $shown =1;
                                    echo $ville_nom->Nom;
                                }
                            }
                            if ($shown != 1){
                                echo  $obj_ville_location_details->Nom;;
                            }

                        }
                    }else{
                        if ($oDetailAgenda->IdCommercant != "301444"){
                            echo $obj_location_article_details->location."-".$oDetailAgenda->codepostal_localisation;
                        }else{
                            echo str_replace($oDetailAgenda->codepostal_localisation,"",$obj_location_article_details->location);
                        }

                    }

                }
            }
            ?></div>
        <div class="col-lg-12" style="text-align:center; padding:15px 0;">
            <?php //if (isset($oDetailAgenda->ville)) $ville_map = $oDetailAgenda->ville; else $ville_map = ''; ?>
            <?php $ville_map = ''; ?>
            <?php if (isset($oDetailAgenda->adresse_localisation)) { ?>
                <div class="col-lg-12">
                    <iframe
                        src="https://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php if (isset($obj_location_article_details->location)) echo $obj_location_article_details->location . " "; ?>
<?php
     if ($oDetailAgenda->IdCommercant != "301444"){
         echo $oDetailAgenda->adresse_localisation . ", " . $oDetailAgenda->codepostal_localisation . " &nbsp;" . $ville_map;
     }else{
         echo $oDetailAgenda->adresse_localisation . ", &nbsp;" . $ville_map;
     }
?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php
                        if ($oDetailAgenda->IdCommercant != "301444"){
                        echo $oDetailAgenda->adresse_localisation . ", " . $oDetailAgenda->codepostal_localisation . " &nbsp;" . $ville_map;
                        }else{
                            echo $oDetailAgenda->adresse_localisation . ", &nbsp;" . $ville_map;
                        }
                        ?>&amp;t=m&amp;vpsrc=0&amp;output=embed"
                        width="100%" height="350"></iframe>
                </div>
            <?php } ?>
        </div>
    </div>


    <?php
    $thisss =& get_instance();
    $thisss->load->model("mdlfestival");
    if(isset($oDetailAgenda->IdFestival)) $objFestival = $thisss->mdlfestival->getById($oDetailAgenda->IdFestival);


    if (isset($objFestival->id) && is_object($objFestival)) {
        ?>
        <div class="col-lg-12 title_categ_black">Festival associé</div>
        <div class="col-lg-12 padding0">
            <div class="col-sm-8">
                <h3><a style="color: black;" href="<?php echo site_url("front/festivals/details_festivals/" . $objFestival->id); ?>"><?php echo $objFestival->nom_manifestation;?></a></h3>

                </a>

                <a style="color: black;" href="<?php echo site_url("front/festivals/details_festivals/" . $objFestival->id); ?>"><?php
                    if (isset($objFestival->location_id) && $objFestival->location_id != "0") {
                        $objFestival = $this->mdl_localisation->getById($objFestival->location_id);
                        if (isset($objFestival) && is_object($objFestival)) {
                            echo $objFestival->location . "<br/>" . $objFestival->location_address . " - ";
                            $obj_ville_location_details = $this->mdlville->getVilleById($objFestival->location_villeid);
                            if (isset($obj_ville_location_details->Nom) && is_object($obj_ville_location_details)) {
                                if ($oDetailAgenda->IdCommercant != "301444" ){
                                    echo $obj_ville_location_details->CodePostal . " - " . $obj_ville_location_details->Nom;
                                }else{
                                    echo  $obj_ville_location_details->Nom;
                                }

                            }
                        }
                    }
                    ?></a>
            </div>
        </div>
    <?php } ?>

    <?php


    $thisss=& get_instance();
    $thisss->load->model("mdl_agenda");
    if(isset($oDetailAgenda->IdFestival)) $ooDetailAgenda = $thisss->mdl_agenda->GetById($oDetailAgenda->IdFestival);
    //var_dump($ooDetailAgenda);
    if ((isset($ooDetailAgenda->id)) && is_object($ooDetailAgenda)) { ?>
        <h4><div class="col-lg-12">Evénements:</div></h4>
            <h5><div class="col-sm-8"><?php echo $ooDetailAgenda->nom_manifestation;?></div></h5>
   <?php } ?>

    <?php if (isset($oDetailAgenda->activ_fb_comment) && $oDetailAgenda->activ_fb_comment == '1') { ?>
        <div class="col-lg-12 padding0" style="background-color: #fff;">
            <div class="fb-comments" data-href="<?php echo site_url("/article/details/" . $oDetailAgenda->id); ?>"
                 data-width="800" data-numposts="5"></div>
        </div>
    <?php } ?>


    <div style="margin:20px; display:none">
        <!--<strong>Organisateur :</strong><br/> <?php // echo $oDetailAgenda->organisateur ; ?> de <?php // echo $oDetailAgenda->ville ; ?><br/>
<?php // echo $oDetailAgenda->adresse_localisation ; ?> <?php // echo $oDetailAgenda->codepostal_localisation ; ?>
<?php // if ($oDetailAgenda->telephone!="") echo "<br/>Tél. ".$oDetailAgenda->telephone ; ?>
<?php // if ($oDetailAgenda->mobile!="") echo "<br/>Mobile. ".$oDetailAgenda->mobile ; ?>
<?php // if ($oDetailAgenda->fax!="") echo "<br/>Fax. ".$oDetailAgenda->fax ; ?>-->

        <?php echo "<br/><strong>Partenaire</strong> : " . $oCommercant->NomSociete; ?>
        <?php
        $this->load->model("mdlville");
        $oVilleCommercant = $this->mdlville->getVilleById($oCommercant->IdVille);
        if (isset($oVilleCommercant)) echo "<br/>" . $oVilleCommercant->Nom;
        echo " " . $oVilleCommercant->CodePostal . "<br/>";
        if (isset($oCommercant->TelFixe)) echo " Tel." . $oCommercant->TelFixe;
        if (isset($oCommercant->TelMobile)) echo " Mobile." . $oCommercant->TelMobile;
        ?>
        <?php if ($oDetailAgenda->date_depot != "") echo "<br/>Fiche déposée le " . translate_date_to_fr($oDetailAgenda->date_depot); ?>
        <?php if ($oDetailAgenda->last_update != "") echo "<br/>Fiche modifiée le " . translate_date_to_fr($oDetailAgenda->last_update); ?>

    </div>

    <div style='font-family: "Arial",sans-serif; display:none;
    font-size: 12px; margin-bottom:10px; text-align:center;
    font-weight: 700; margin-top:30px;
    line-height: 1.25em;'>Vous avez une question particulière à nous poser, adressez nous un mail express
    </div>

    <div style="margin-bottom:10px; margin-top:10px; display:none;">
        <center>
            <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
            <script type="application/javascript">
                $(document).ready(function () {
                    $("#btn_submit_form_module_detailbonnplan").click(function () {
                        //alert('test form submit');
                        txtErrorform = "";

                        var txtError_text_mail_form_module_detailbonnplan = "";
                        var text_mail_form_module_detailbonnplan = $("#text_mail_form_module_detailbonnplan").val();
                        if (text_mail_form_module_detailbonnplan == "") {
                            //$("#divErrorform_module_detailbonnplan").html('<font color="#FF0000">Veuillez saisir votre demande</font>');
                            txtErrorform += "1";
                            $("#text_mail_form_module_detailbonnplan").css('border-color', 'red');
                            $("#text_mail_form_module_detailbonnplan").focus();
                        } else {
                            $("#text_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
                        }

                        var nom_mail_form_module_detailbonnplan = $("#nom_mail_form_module_detailbonnplan").val();
                        if (nom_mail_form_module_detailbonnplan == "") {
                            txtErrorform += "- Veuillez indiquer Votre nom_mail_form_module_detailbonnplan <br/>";
                            $("#nom_mail_form_module_detailbonnplan").css('border-color', 'red');
                            $("#nom_mail_form_module_detailbonnplan").focus();
                        } else {
                            $("#nom_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
                        }

                        var email_mail_form_module_detailbonnplan = $("#email_mail_form_module_detailbonnplan").val();
                        if (email_mail_form_module_detailbonnplan == "" || !isEmail(email_mail_form_module_detailbonnplan)) {
                            txtErrorform += "- Veuillez indiquer Votre email_mail_form_module_detailbonnplan <br/>";
                            //alert("Veuillez indiquer Votre nom");
                            $("#email_mail_form_module_detailbonnplan").css('border-color', 'red');
                            $("#email_mail_form_module_detailbonnplan").focus();
                        } else {
                            $("#email_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
                        }


                        if (txtErrorform == "") {
                            $("#form_module_detailbonnplan").submit();
                        }
                    });


                });
            </script>
            <?php if (isset($user_ion_auth)) { ?>
                <style type="text/css">
                    .inputhidder {
                        visibility: hidden;
                    }
                    .article_all_details_content_container .container{
                        width: 100%!important;
                    }
                </style>
            <?php } ?>

            <table border="0" align="center" style="text-align:center; width:100%;">
                <tr>
                    <td style="text-align:left;"><img
                            src="<?php echo GetImagePath("front/"); ?>/btn_new/info_annonce_img.png" alt="img"
                            width="250"></td>
                    <td>
                        <form method="post" name="form_module_detailbonnplan" id="form_module_detailbonnplan" action=""
                              enctype="multipart/form-data">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr class="inputhidder">
                                    <td>Votre Nom</td>
                                    <td><input id="nom_mail_form_module_detailbonnplan"
                                               name="nom_mail_form_module_detailbonnplan" type="text"
                                               value="<?php if (isset($user_ion_auth) && $user_ion_auth->first_name != "") echo $user_ion_auth->first_name; ?>">
                                    </td>
                                </tr>
                                <tr class="inputhidder">
                                    <td>Votre Téléphone</td>
                                    <td><input id="tel_mail_form_module_detailbonnplan"
                                               name="tel_mail_form_module_detailbonnplan" type="text"
                                               value="<?php if (isset($user_ion_auth) && $user_ion_auth->phone != "") echo $user_ion_auth->phone; ?>">
                                    </td>
                                </tr>
                                <tr class="inputhidder">
                                    <td>Votre Email</td>
                                    <td><input id="email_mail_form_module_detailbonnplan"
                                               name="email_mail_form_module_detailbonnplan" type="text"
                                               value="<?php if (isset($user_ion_auth) && $user_ion_auth->email != "") echo $user_ion_auth->email; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <textarea id="text_mail_form_module_detailbonnplan" cols="28" rows="7"
                                                  name="text_mail_form_module_detailbonnplan"
                                                  style="width:246px; height:130px;font-family:Arial, Helvetica, sans-serif; font-size:10px;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input id="btn_reset_form_module_detailbonnplan" type="reset" value="Effacer"
                                               style="width:82px; height:22px;">
                                        <input id="btn_submit_form_module_detailbonnplan" type="button"
                                               name="btn_submit_form_module_detailbonnplan" value="Envoyer"
                                               style="width:90px; height:22px;">
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <div
                            id="divErrorform_module_detailbonnplan"><?php if (isset($mssg_envoi_module_detail_bonplan)) echo $mssg_envoi_module_detail_bonplan; ?></div>
                    </td>
                </tr>
            </table>
            <br/>

        </center>
    </div>


</div>