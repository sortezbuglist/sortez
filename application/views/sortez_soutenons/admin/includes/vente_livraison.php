<div class="row glissiere_tab livraison_row">
    <div class="col-4">
        <div class="text-left titre_gli_up">LIVRAISON A DOMICILE</div>
    </div>
    <div class="col-4" id="activ_glissiere_livraison">
        <div class="cercle">
            <div class="text-center up d-none" id="activ_glissiere_livraison_up"><p>fermer</p></div>
            <div class="text-center down" id="activ_glissiere_livraison_down"><p>ouvrir</p></div>
        </div>
    </div>
    <div class="col-4 d-flex">
        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
        <div class="text-right w-25 titre_gli_up">
            <!--                    Activé:-->
            <!--                    <select id="is_activ_livr" name="data[is_activ_livr]">-->
            <!--                        <option value="0">Non</option>-->
            <!--                        <option --><?php //if (isset($data_commande->is_activ_livr) AND $data_commande->is_activ_livr == '1' ) echo "selected='selected'"; ?><!-- value="1">Oui</option>-->
            <!--                    </select>-->
            <label class="switch">
                <input id="is_activ_livr_a_domicile" type="checkbox" <?php if (isset($data_commande->is_activ_livr_a_domicile) AND $data_commande->is_activ_livr_a_domicile == '1') echo 'checked'; ?> >
                <span class="slider round"></span>
            </label>
            <input name="data[is_activ_livr_a_domicile]" id="is_activ_livr_a_domicile_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_livr_a_domicile)) echo $data_commande->is_activ_livr_a_domicile; ?>">
        </div>
    </div>
</div>
<div class="row d-none pb-4 shadowed activ_glissiere_livraison_child1" id="activ_glissiere_livraison_child">
    <div class="row p-2">
        <div class="col-12">
            <label for="horaire_livr_ouvert" class="text-left w-100 text_label text-center">Horaires et jours d’ouverture</label>
            <textarea class="form-control textarea_style" id="horaire_livr_ouvert" name="data[horaire_livr_ouvert]"><?php if (isset($data_commande->horaire_livr_ouvert)) echo $data_commande->horaire_livr_ouvert; ?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pl-4 mt-2 pr-4">
            <div class="text-center text_label">
                Horaires et jours de livraison à domicile
            </div>
            <textarea name="data[jour_heur_livr]" class="form-control textarea_style"><?php if (isset($data_commande->jour_heur_livr)) echo $data_commande->jour_heur_livr; ?></textarea>
        </div>
    </div>
    <div class="row pt-3">
        <div class="col-12 pl-4 pr-4">
            <label for="commune_livr_des" class="mb-0 text-center w-100 text_label">Communes desservies</label>
            <textarea class="form-control textarea_style" id="commune_livr_desserv" name="data[commune_livr_desserv]"><?php if (isset($data_commande->commune_livr_desserv)) echo $data_commande->commune_livr_desserv; ?></textarea>
        </div>
    </div>
    <div class="row">
        <div class="col-6 mt-3">
            <div class="w-100 text-center text_label pl-2">
                Livraison gratuite pour toute commande supérieure à
            </div>
            <div class="w-50 pl-2 m-auto mt-0 text-center">
                <label class="switch_out absoluted">
                    <input id="is_activ_prix_livr_grat" type="checkbox" <?php if (isset($data_commande->is_activ_prix_livr_grat) AND $data_commande->is_activ_prix_livr_grat == '1') echo 'checked'; ?> >
                    <span class="slider_out round_out"></span>
                </label>
                <input name="data[is_activ_prix_livr_grat]" id="is_activ_prix_livr_grat_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_prix_livr_grat)) echo $data_commande->is_activ_prix_livr_grat; ?>" />
                <input value="<?php if (isset($data_commande->livraison_grtuit_fee)) echo $data_commande->livraison_grtuit_fee; ?>" type="number" placeholder="40 €" class="inpt price_l_fee textarea_style pl-4" name="data[livraison_grtuit_fee]">
            </div>
        </div>
        <div class="col-6">
            <label class="switch_out absoluted2">
                <input id="is_activ_del_livr" type="checkbox" <?php if (isset($data_commande->is_activ_del_livr) AND $data_commande->is_activ_del_livr == '1') echo 'checked'; ?> >
                <span class="slider_out round_out"></span>
            </label>
            <input name="data[is_activ_del_livr]" id="is_activ_del_livr_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_del_livr)) echo $data_commande->is_activ_del_livr; ?>" />
            <div class="w-100 pt-3 text_label text-center">Délai de livraison</div>
            <div class="w-100 m-auto text_label text-center">
                <input type="text" id="delai_livr_debut" value="<?php if (isset($data_commande->delai_livr_debut)) echo $data_commande->delai_livr_debut; ?>" placeholder="De 45 @ 60 minutes" class="inpt price_l_fee textarea_style pl-3 w-50" name="data[delai_livr_debut]" />
            </div>
        </div>
    </div>
    <div class="col-12 m-2 pt-4 text-center">
        <input type="submit" class="btn btn-secondary  w-50" />
    </div>
</div>

<div class="row  shadowed pb-5 pt-4 mt-4 d-none activ_glissiere_livraison_child">
    <div class="col-8 text-center">
        <h5>1. VENTE EN LIGNE :  LIVRAISON ET PAIEMENT A DOMICILE</h5>
    </div>
    <div class="col-4 d-flex">
        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
        <div class="text-right w-25 titre_gli_up">
            <label class="switch">
                <input id="is_activ_livr" type="checkbox" <?php if (isset($data_commande->is_activ_livr) AND $data_commande->is_activ_livr == '1') echo 'checked'; ?> >
                <span class="slider round"></span>
            </label>
            <input name="data[is_activ_livr]" id="is_activ_livr_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_livr)) echo $data_commande->is_activ_livr; ?>">
        </div>
    </div>
    <div class="col-12 pl-4 mt-2 pr-4">
        <div class="text-center row text_label">
            <div class="col-6 text-right">Commentaires par défaut*</div>
            <div class="col-6 text-left">
                <label class="switch_out">
                    <input id="is_activ_comment_default_livr" type="checkbox" <?php if (isset($data_commande->is_activ_comment_default_livr) AND $data_commande->is_activ_comment_default_livr == '1') echo 'checked'; ?> >
                    <span class="slider_out round_out"></span>
                </label>
                <input id="is_activ_comment_default_livr_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_comment_default_livr)) echo $data_commande->is_activ_comment_default_livr; ?>" name="data[is_activ_comment_default_livr]">
            </div>
        </div>
        <textarea id="livraison_comment_textarea" name="data[livraison_comment]" class="form-control textarea_style"><?php if (isset($data_commande->livraison_comment)) echo $data_commande->livraison_comment; ?></textarea>
    </div>
    <div class="row w-100 mt-4">
        <div class="col-2">
            <div class="row">
                <div class="col-12 pr-0 text-right">
                    <label class="switch_out absoluted">
                        <input id="is_activ_type_livr" type="checkbox" <?php if (isset($data_commande->is_activ_type_livr) AND $data_commande->is_activ_type_livr == '1') echo 'checked'; ?> >
                        <span class="slider_out round_out"></span>
                    </label>
                    <input name="data[is_activ_type_livr]" id="is_activ_type_livr_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_type_livr)) echo $data_commande->is_activ_type_livr; ?>" />
                </div>
            </div>
        </div>
        <div class="col-4 text-left">
            <div class="row">
                <div class="col-12 pr-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_cheque_livr" type="checkbox" <?php if (isset($data_commande->is_activ_cheque_livr) AND $data_commande->is_activ_cheque_livr == '1') echo 'checked'; ?> >
                        <input id="is_activ_cheque_livr_hidden" type="hidden" name="data[is_activ_cheque_livr]" value="<?php if (isset($data_commande->is_activ_cheque_livr)) echo $data_commande->is_activ_cheque_livr; ?>">
                        <div class="pl-2">Chèque dûment rempli et signé</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="row">
                <div class="col-12 p-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_termin_bank_livr" type="checkbox" <?php if (isset($data_commande->is_activ_termin_bank_livr) AND $data_commande->is_activ_termin_bank_livr == '1') echo 'checked'; ?> >
                        <input id="is_activ_termin_bank_livr_hidden" type="hidden" name="data[is_activ_termin_bank_livr]" value="<?php if (isset($data_commande->is_activ_termin_bank_livr)) echo $data_commande->is_activ_termin_bank_livr; ?>">
                        <div class="pl-2">Carte bancaire avec un terminal de paiement</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row w-100">
        <div class="col-12 pt-5 text-center">
            <input type="submit" class="btn btn-secondary  w-50" />
        </div>
    </div>
</div>



<div class="row activ_glissiere_livraison_child d-none shadowed pb-5 pt-4 mt-4">
    <div class="col-8 text-center">
        <h5>2. VENTE ET PAIEMENT A LA COMMANDE EN LIGNE AVEC PAYPAL</h5>
    </div>
    <div class="col-4 d-flex">
        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
        <div class="text-right w-25 titre_gli_up">
            <label class="switch">
                <input id="is_activ_livr_paypal" type="checkbox" <?php if (isset($data_commande->is_activ_livr_paypal) AND $data_commande->is_activ_livr_paypal == '1') echo 'checked'; ?> >
                <span class="slider round"></span>
            </label>
            <input name="data[is_activ_livr_paypal]" id="is_activ_livr_paypal_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_livr_paypal)) echo $data_commande->is_activ_livr_paypal; ?>">
        </div>
    </div>
    <div class="col-12 pl-4 mt-2 pr-4">
        <div class="text-center row text_label">
            <div class="col-6 text-right">Commentaires par défaut*</div>
            <div class="col-6 text-left">
                <label class="switch_out">
                    <input id="is_activ_paypal_comment" type="checkbox" <?php if (isset($data_commande->is_activ_paypal_comment) AND $data_commande->is_activ_paypal_comment == '1') echo 'checked'; ?> >
                    <span class="slider_out round_out"></span>
                </label>
                <input id="is_activ_paypal_comment_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_paypal_comment)) echo $data_commande->is_activ_paypal_comment; ?>" name="data[is_activ_paypal_comment]">
            </div>
        </div>
        <textarea id="new_paypal_comment" name="data[new_paypal_comment]" class="form-control textarea_style"><?php if (isset($data_commande->new_paypal_comment)) echo $data_commande->new_paypal_comment; ?></textarea>
    </div>
    <div class="row p-4 w-100">
        <p class="col-12 pt-3">
            Sur les modules "vente à emporter" et "livraison à domicile", vous avez préalablement référencé vos conditions de règlement en ligne avec le module Paypal. Nous allons vous préciser comment intégrer un bouton de paiement à votre formulaire de commande.
        </p>
        <div class="col-4 pt-4 text-center">
            <img src="<?php echo base_url()?>assets/images/paypal-paiement-securise.webp">
        </div>
        <div class="col-8 text-center pt-3 txt_title_pay">
            <ol class="font_7" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:15px;">
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Connectez vous sur le site :&nbsp; </span></span><span style="color:#3D9BE9;"><span style="font-family:futura-lt-w01-book,sans-serif;"><span style="text-decoration:underline;"><a href="https://www.paypal.com/fr/home" target="_blank" data-content="https://www.paypal.com/fr/home" data-type="external" rel="noopener">https://www.paypal.com/fr/home</a></span></span></span><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;"> ;</span></span></span></p>
                </li>
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Après avoir ouvert un compte personnel, vous accédez à votre page administrative&nbsp; ;</span></span></span></p>
                </li>
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Cliquez sur le bouton du menu "Outils" puis sur "Plus d'outils" ;</span></span></span></p>
                </li>
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Sur le bloc "Boutons de paiement", cliquez sur "Ouvrir" ;</span></span></span></p>
                </li>
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Sur la nouvelle page, cliquez sur le bouton "Acheter" ;</span></span></span></p>
                </li>
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Sur le formulaire, remplissez tout simplement le champ "Nom de l'objet" avec le nom de l'établissement ;</span></span></span></p>
                </li>
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">N'indiquez pas de montant, ce sera votre client qui précisera le montant ttc à payer ;</span></span></span></p>
                </li>
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Cliquez sur&nbsp;le bouton "Enregistrez les modifications"</span></span></span></p>
                </li>
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Sur la nouvelle page , copiez le code du bouton et collez le sur le champ ci-dessous&nbsp;</span></span></span><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">;</span></span></span></p>
                </li>
                <li>
                    <p class="font_7" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Le bouton apparaîtra sur votre page de formulaire</span></span></span></p>
                </li>
            </ol>
        </div>
        <div id="form_paypal" class="m-0 w-100 row pt-4 d-flex">
            <div class="text-center col-6 pl-3 pt-5">
                <img class="w-50 img-fluid" src="<?php echo base_url()?>assets/images/bouton-paypal.webp"/>
            </div>
            <div class="col-6 pt-3">
                <div class="code_coll pb-3">Coller ici le code</div>
                <textarea name="data[paypal_content]" class="form-control textarea_style_paypal" id="paypal_content"><?php if (isset($data_commande->paypal_content)) echo $data_commande->paypal_content; ?></textarea>
            </div>
        </div>
    </div>
    <div class="row w-100 pt-5">
        <div class="col-12 pt-4 text-center">
            <input type="submit" class="btn btn-secondary  w-50" />
        </div>
    </div>
</div>