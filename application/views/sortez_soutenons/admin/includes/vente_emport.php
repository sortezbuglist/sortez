<div class="row glissiere_tab">
    <div class="col-4">
        <div class="text-left titre_gli_up">VENTE A EMPORTER</div>
    </div>
    <div class="col-4" id="activ_glissiere_emporter">
        <div class="cercle">
            <div class="text-center up" id="activ_glissiere_emporter_up"><p>fermer</p></div>
            <div class="text-center down d-none" id="activ_glissiere_emporter_down"><p>ouvrir</p></div>
        </div>
    </div>
    <div class="col-4 pb-2 d-flex">
        <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
        <div class="text-right w-25 titre_gli_up">
            <label class="switch">
                <input id="is_activ_emp" type="checkbox" <?php if (isset($data_commande->is_activ_emporter) AND $data_commande->is_activ_emporter == '1') echo 'checked'; ?> >
                <span class="slider round"></span>
            </label>
            <input name="data[is_activ_emporter]" id="is_activ_emp_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_emporter)) echo $data_commande->is_activ_emporter; ?>">
        </div>
    </div>
    <div class=""></div>
</div>

<div class="row tab_child p-2 pb-4 d-block" id="is_activ_emp_child">
    <label for="heure_emp_ouvert" class="text_label w-100 text-center">Horaires et jours d’ouvertures</label>
    <textarea id="heure_emp_ouvert" class="form-control textarea_style" name="data[horaire_emporter_ouvert]"><?php if (isset($data_commande->horaire_emporter_ouvert)) echo $data_commande->horaire_emporter_ouvert; ?></textarea>
    <label class="pt-2 text_label w-100 text-center" for="heure_emp_enlev">Horaires d’enlèvement</label>
    <textarea id="heure_emp_enlev" class="form-control textarea_style" name="data[horaire_emporter_enlev]"><?php if (isset($data_commande->horaire_emporter_ouvert)) echo $data_commande->horaire_emporter_enlev; ?></textarea>
    <div class="w-100 h5 pt-4">
        VENTE EN LIGNE A EMPORTER / RETRAIT ET PAIEMENT EN MAGASIN
    </div>
    <label for="comment_emporter_txt" class="pt-2 text_label w-100 text-center">
        <div style="display: inline-block"><div class="label_switc">Commentaires par défaut</div></div>
        <label class="switch_out">
            <input id="is_activ_comment_default_enlev" type="checkbox" <?php if (isset($data_commande->is_activ_comment_default_enlev) AND $data_commande->is_activ_comment_default_enlev == '1') echo 'checked'; ?> >
            <span class="slider_out round_out"></span>
        </label>
    </label>
    <input id="is_activ_comment_default_enlev_hidden" type="hidden" name="data[is_activ_comment_default_enlev]" value="<?php if (isset($data_commande->is_activ_comment_default_enlev)) echo $data_commande->is_activ_comment_default_enlev; ?>" />
    <textarea id="comment_emporter_txt" class="form-control textarea_style w-100 text-center" name="data[comment_emporter_txt]" placeholder=""><?php if (isset($data_commande->comment_emporter_txt)) echo $data_commande->comment_emporter_txt; ?></textarea>
    <div class="row mt-4">
        <div class="col-2">
            <div class="row">
                <div class="col-12 pr-0 text-right">
                    <label class="switch_out absoluted">
                        <input id="is_activ_type_livr_grat" type="checkbox" <?php if (isset($data_commande->is_activ_type_livr_grat) AND $data_commande->is_activ_type_livr_grat == '1') echo 'checked'; ?> >
                        <span class="slider_out round_out"></span>
                    </label>
                    <input name="data[is_activ_type_livr_grat]" id="is_activ_type_livr_grat_hidden" type="hidden" value="<?php if (isset($data_commande->is_activ_type_livr_grat)) echo $data_commande->is_activ_type_livr_grat; ?>" />
                </div>
            </div>
        </div>
        <div class="col-2 text-left">
            <div class="row">
                <div class="col-12 pr-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_cheque_enlev" type="checkbox" <?php if (isset($data_commande->is_activ_cheque_enlev) AND $data_commande->is_activ_cheque_enlev == '1') echo 'checked'; ?> >
                        <input id="is_activ_cheque_enlev_hidden" type="hidden" name="data[is_activ_cheque_enlev]" value="<?php if (isset($data_commande->is_activ_cheque_enlev)) echo $data_commande->is_activ_cheque_enlev; ?>">
                        <div class="pl-2">Chèque</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="row">
                <div class="col-12 p-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_termin_banc_enlev" type="checkbox" <?php if (isset($data_commande->is_activ_termin_banc_enlev) AND $data_commande->is_activ_termin_banc_enlev == '1') echo 'checked'; ?> >
                        <input id="is_activ_termin_banc_enlev_hidden" type="hidden" name="data[is_activ_termin_banc_enlev]" value="<?php if (isset($data_commande->is_activ_termin_banc_enlev)) echo $data_commande->is_activ_termin_banc_enlev; ?>">
                        <div class="pl-2">Carte bancaire</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="row">
                <div class="col-12 pr-0 text-left">
                    <div class="w-100 d-flex switch_term_mob">
                        <input class="check_new" id="is_activ_espece_enlev" type="checkbox" <?php if (isset($data_commande->is_activ_espece_enlev) AND $data_commande->is_activ_espece_enlev == '1') echo 'checked'; ?> >
                        <input id="is_activ_espece_enlev_hidden" type="hidden" name="data[is_activ_espece_enlev]" value="<?php if (isset($data_commande->is_activ_espece_enlev)) echo $data_commande->is_activ_espece_enlev; ?>">
                        <div class="pl-2">Espèces</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pt-5 text-center">
            <input type="submit" class="btn btn-secondary  w-50" />
        </div>
    </div>

</div>