
     <div class="container cont_all">
       
            <div id="txt_title_gli1" class="txt_title_gli1_ppl text-center col-8">
            <div class=""style="background-color: #3453A2, color: white;">PERSONNALISE VOTRE BOUTON DE MENU</div>
       
    </div>
     <?php $this->load->view('admin/menu/js/script') ?>

     <script type="text/javascript">
    function delete_click_menu(id_com){
        var data = "idcom="+id_com;
        $.ajax({
            type: "POST",
            url: "https://www.sortez.org/soutenons/admin/Gestion_compte/delete_click_menu/"+id_com,
            data: data ,
            success: function() {
                if (data != "") {
                    var to_open = "'https://www.sortez.org/media/index/"+id_com+"-click_menu-photo_menu_gen"+""+"' , '' ,'width=1045 , height=675, scrollbars=yes'"
                    var to_ch = '<div id="Menuphoto_menu_gen_container" onclick="javascript:window.open('+to_open+');", href="javascript:void(0);" class="w-100" ><img  src="https://www.sortez.org/assets/image/download-icon-png-5.jpg" style="height: 160px"></div>';
                    $("#img_menu_gen_contents").html(to_ch);
               }
            },
            error: function() {
                alert(data);
            }
        });
    }
</script>

     <form method="post" action="<?php echo site_url();?>soutenons/admin/gestion_compte/save_click_menu/<?echo  $infocom->IdCommercant; ?>" class="row pb-5 shadowed p-2 gli_content d-flex">
    <div class="col-4 text-center">
       <div class="w-100 text-center top_menu_gen">
           Je désire que ce bouton s’intègre sur le menu principal de mon site<br>
           <input <?php if (isset($infocom->is_actif_click) AND $infocom->is_actif_click == '1' ){echo 'checked';} ?> type="checkbox" class="checkbox_active_default_menu" id="is_activ_default_btn_btn" >
           <input id="is_activ_default_btn" value="<?php if (isset($infocom->is_actif_click) AND $infocom->is_actif_click == '1' ){echo '1';}else{echo '0';} ?>"  type="hidden" name="data[is_actif_click]">           
       </div>
       <!--  <div class="w-100 text-center">
            <img src="<?php //echo base_url()?>/assets/image/btn_menu.png">
        </div> -->
        choisissez la couleur du bouton en cliquant sur le panel 
        <input type="color" name="data[click_color]" style="width:150px; height: 150px"  value="<?php if (isset($infocom->click_color)) echo $infocom->click_color; ?>">
    </div>

    <div class="col-4">
        <div class="w-100 text-center top_menu_gen">
            Télécharger l'image de personnalisation, Cliquez sur l'icone !...
        </div>
        <div class="w-100 text-center" id="img_menu_gen_contents">
            <?php if (isset($infocom->click_icon) AND $infocom->click_icon !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$infocom->user_ionauth_id."/click_menu/".$infocom->click_icon)){ ?>
                <div class="w-100">
                    <img style="max-width: 200px" src="<?php echo base_url()?>application/resources/front/photoCommercant/imagesbank/<?php echo $infocom->user_ionauth_id; ?>/click_menu/<?php echo $infocom->click_icon ?>">
                    <a onclick="delete_click_menu(<?php echo $infocom->IdCommercant; ?>)" class="btn btn-danger mt-3">Supprimer image</a>
                </div>
            <?php }else{ ?>
                <div class="w-100" id="Menuphoto_menu_gen_container">
                    <img onclick='javascript:window.open("<?php echo site_url("media/index/".$infocom->IdCommercant."-click_menu-photo_menu_gen"); ?>", "", "width=1045, height=675, scrollbars=yes");' src="<?php echo base_url()?>assets/image/download-icon-png-5.jpg">
                </div>
                <div id="Articleimg_menuphoto_menu_gen_container"></div>
            <?php } ?>
        </div>
    </div>

    <div class="col-4">
        <div class="w-100 top_menu_gen">
            Précisez le texte
            du bouton
        </div>
        <div class="w-100 text-center menu_txt_container">
            <input maxlength="22" value="<?php if (isset($infocom->click_menu)) echo $infocom->click_menu; ?>" type="text" class="form-control input_menu_txt" name="data[click_menu]">
            <?php if($error_message!=NULL):?>
           <div class="alert alert-danger mt-2" role="alert">
                <strong>Erreur ! </strong><?php print_r($error_message);?>
            </div>
            <?php endif;?>
        </div>
    </div>
    <div class="w-100 text-center pt-4 pb-4">
        <input type="submit" class="btn btn-secondary  w-50" />
    </div>
</form>
</div>