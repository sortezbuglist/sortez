<div class="container">
    <div class="content_page_container">
        <div class="row">
            <div class="title_align_center mt-5 mb-5">
                <h1 class="title_large">Les avantages des membres</h1>
                <p class="sous_title">Souscrivez à notre carte privilège et accédez aux avantages concédés par nos partenaires !…</p>
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-5 ml-5">
                        <div class="row row_content_souscr">
                            <div>
                                <p class="title_bloc">Les Deals</p>
                                <p class="content_bloc">Des offres uniques,exceptionnelles motivantes pour le consommateur.</p>
                            </div>
                            <img class="d-block w-100" src="<?php echo base_url('/assets/soutenons/Deals.png')?>" alt="#">
                            <div class="mt-4 col-lg-6 offset-lg-3 details">Détails</div>
                        </div>
                    </div>
                    <div class="col-lg-5 ml-4">
                        <div class="row row_content_souscr">
                            <div lass="title_align_center">
                                <p class="title_bloc">La fidélité</p>
                                <p class="content_bloc">Remises Cash,Capitalisation,Coup de tampon.</p>
                            </div>
                            <img class="d-block w-100" src="<?php echo base_url('/assets/soutenons/fidelite.png')?>" alt="#">
                            <div class="mt-4 col-lg-6 offset-lg-3 details">Détails</div>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                </div>
                <div class="row mt-5">
                    <img class="d-block w-100" src="<?php echo base_url('/assets/soutenons/carte4.webp')?>" alt="#">
                    <div class="title_align_center mt-5 mb-5">
                        <p class="title_content">Souscrivez à notre carte privilège et accédez aux avantages concédés par nos partenaires !…</p>
                        <ul>
                            <li class="li_content">Certains bons plans doivent être réservés (offre unique et/ou quantitative).</li>
                            <li class="li_content">En les validant,elles sont enregistrées sur votre compte et celui du professionnel.</li>
                            <li class="li_content">Lors de votre déplacement, vous présentez votre carte et bénéficiez immédiatement des conditions du professionnel.</li>
                            <li class="li_content">Si vous ne possédez pas de smartphone, il vous suffira sur votre ordinateur familial d’imprimer votre QRCODE et les mails de confirmation pour les présenter lors de vos déplacements chez nos partenaires.</li>
                        </ul>
                    </div>
                    <img class="d-block w-100" src="<?php echo base_url('/assets/soutenons/carte6.webp')?>" alt="#">
                    <div class="title_align_center mt-5 mb-5">
                        <p class="title_content">La gestion des transactions</p>
                        <ul>
                            <li class="li_content">A chaque transaction validée, vos consommations et vos soldes sont actualisés en temps réel(liste et détails par partenaires).</li>
                        </ul>
                    </div>
                    <div class="title_align_center mb-5">
                        <p class="title_content">Avec la carte privilège vivresaville, vous bénéficiez d’autres avantages !…</p>
                    </div>
                    <img class="d-block w-100" src="<?php echo base_url('/assets/soutenons/carte2.webp')?>" alt="#">
                    <div class="title_align_center mt-5 mb-5">
                        <ul>
                            <li class="li_content"> LA GESTION DES FAVORIS : à tout moment, vous pouvez référencer le partenaire de votre choix dans vos favoris. </li>
                            <li class="li_content"> NEWSLETTER : Recevez toutes les semaines notre newsletter</li>
                            <li class="li_content"> LES TIRAGES AU SORT :  gagnez des entrées ou des places de spectacles…</li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 offset-lg-6">
                            <a href="<?php echo site_url('front/particuliers/inscription'); ?>" target="_blank">
                                <img src="<?php echo base_url('/assets/soutenons/inscription.png');?>">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .content_page_container{
        margin-left: 230px;
        margin-right: 230px;
        margin-bottom: 150px;
    }
    .row_content_souscr{
        background-color: white;
        box-shadow: 2px 2px 2px 2px #e1e1e1;
        padding: 40px;
        height: 540px;
    }
    .details{
        background-color: rgba(232, 14, 174, 1);
        border: solid rgba(32, 206, 136, 1) 0px;
        cursor: pointer !important;
        color: #FFFFFF;
        padding: 10px;
        text-align: center;
        font-size: 14px;
        font-family: Futura-LT-Book,Sans-Serif!important;
    }
    .details:hover{
        background-color: #1db578;
    }
    .title_large{
        font-family: Libre-Baskerville, Serif!important;
        font-size: 35px;
        text-align: center;
        color: #292929;
    }
    .sous_title{
        font-family: Libre-Baskerville, Serif!important;
        font-size: 18px;
        text-align: center;
        color: #E80EAE;
        letter-spacing: 0.05em;
    }
    .title_bloc{
        letter-spacing: 0.08em;
        color: #E80EAE;
        font-family: Libre-Baskerville, Serif!important;
        font-size: 25px;
        text-align: center;
    }
    .content_bloc{
        color: #000000;
        font-size: 15px;
        font-family: Futura-LT-Book,Sans-Serif!important;
        text-align: center;
        letter-spacing: 0.08em;
    }
    .title_content{
        letter-spacing: 0.05em;
        color: #E80EAE;
        font-family: Libre-Baskerville,Serif!important;
        font-size: 20px;
        text-align: center;
    }
    .li_content{
        margin-left: -15px;
        color: #000000;
        letter-spacing: 0.05em;
        font-size: 15px;
        font-family:Futura-LT-Book,Sans-Serif!important;
    }
    .title_align_center{
        display: block;
        width: 100%;
    }
</style>