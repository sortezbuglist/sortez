<?php $data["zTitle"] = 'Accueil - Vivresaville';?>

<?php $this->load->view("sortez_vsv/includes/main_header", $data); ?>

<?php if (!isset($info_type)||$info_type=="") $this->load->view("sortez_vsv/includes/accueil_select_content_1", $data); ?>
<?php
if (isset($info_type) && $info_type != "" && $info_type == "communes")
    $this->load->view("sortez_vsv/includes/accueil_select_content_infos_communes", $data);
else if (isset($info_type) && $info_type != "" && $info_type == "communaute")
    $this->load->view("sortez_vsv/includes/accueil_select_content_infos_communaute", $data);
else if (isset($info_type) && $info_type != "" && $info_type == "commercant")
    $this->load->view("sortez_vsv/includes/accueil_select_content_infos_commercant", $data);
else if (isset($info_type) && $info_type != "" && $info_type == "particulier")
    $this->load->view("sortez_vsv/includes/accueil_select_content_infos_particulier", $data);
else $this->load->view("sortez_vsv/includes/accueil_select_content_2", $data);
?>
<?php $this->load->view("sortez_vsv/includes/accueil_select_content_3", $data); ?>

<!-- Return to Top -->
<a href="javascript:void(0);" id="return-to-top"><i class="icon-chevron-up"></i></a>
<!-- ICON NEEDS FONT AWESOME FOR CHEVRON UP ICON -->
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

<?php $this->load->view("sortez_vsv/includes/main_footer", $data); ?>
