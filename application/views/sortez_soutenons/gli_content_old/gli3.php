<div class="row glissiere_tab">
    <form class="m-0 w-100 row d-flex">
        <div class="col-4">
            <div id="txt_title_gli3" class="text-left title_gli titre_gli_up"><?php if (isset($title_gli3->titre_glissiere)) {echo $title_gli3->titre_glissiere;}else{echo 'AJOUTER UNE GLISSIERE ';} ?></div>
        </div>
        <div class="col-4" id="activ_glissiere_activ3">
            <div class="cercle">
                <div class="text-center up d-none" id="activ_glissiere_activ3_up"><p>fermer</p></div>
                <div class="text-center down" id="activ_glissiere_activ3_down"><p>ouvrir</p></div>
            </div>
        </div>
        <div class="col-4 d-flex">
            <div class="text-right w-75 act_desact_text">Activer/désactiver</div>
            <div class="text-right w-25 titre_gli_up">
<!--                Activé:-->
<!--                <select id="title_gli3_is_activ" name="data[is_activ_gli3]">-->
<!--                    <option value="0">Non</option>-->
<!--                    <option --><?php //if (isset($title_gli3->is_activ_glissiere) AND $title_gli3->is_activ_glissiere == '1' ) echo "selected='selected'";  ?><!-- value="1">Oui</option>-->
<!--                </select>-->
                <label class="switch">
                    <input id="title_gli3_is_check" type="checkbox" <?php if (isset($title_gli3->is_activ_glissiere) AND $title_gli3->is_activ_glissiere == '1') echo 'checked'; ?> >
                    <span class="slider round"></span>
                </label>
                <input name="data[is_activ_gli3]" id="title_gli3_is_activ" type="hidden" value="<?php if (isset($title_gli3->is_activ_glissiere)) echo $title_gli3->is_activ_glissiere; ?>">
            </div>
        </div>
    </form>
</div>
<div class="row pb-5 shadowed p-2 gli_content d-none" id="activ_glissiere_activ3_content">
    <div class="row">
        <div class="col-2"></div>
        <div class="col-8 pl-2">
            <h5 class="text_label_spec pt-2">Ajouter un titre à cette glissière</h5>
            <span class="text_label_spec pr-4 float-right"><span id="char_true_title_3">20 </span>Caractères</span>
        </div>
    </div>
    <div class="row pb-2 ml-0 mr-1 bordered_bottom">
        <div class="col-2 title_gli_title text-center">Titre</div>
        <div class="col-8 pl-0 pr-4 padded_title">
            <input id="title_gli3" value="<?php if (isset($title_gli3->titre_glissiere)) echo $title_gli3->titre_glissiere ?>" onchange="chenge_title_hiddden(this)" class="form-control pr-0 textarea_style padded_title_input" type="text">
            <input id="hidden_title_gli3" value="" type="hidden">
        </div>
        <div class="col-2 pl-0 pr-2">
            <a target="_blank" href="https://fotoflexer.com/editor/"><img src="<?php echo base_url()?>assets/images/IMAGE45.webp" /></a>
            <button id="title_gli3" onclick="chenge_title(this)" class="w-100 btn btn-secondary noned_3 d-none" style="border-radius: unset!important">Valider</button>
        </div>
    </div>
    <div class="w-100">
        <div class="w-100" id="glis_content3">
            <?php if (!isset($data_gli3) OR (isset($data_gli3) AND empty($data_gli3)) ){ ?>
                <div class="pt-3 lined3" id="gliss31">
                    <div class="w-100 row d-flex">
                        <div class="col-2 pr-0" id="img_contentss31"><div class='text_label2 pt-5'>Article non enregistré</div></div>
                        <div class="col-8">
                            <div class="w-100 des_label">Titre produit ou service<span class="float-right"><span id="char_title_31">70 </span>Caractères</span></div>
                            <input type="text"  class="form-control true_title" id="true_title_art31">
                            <div class="w-100 des_label">Désignation<span class="float-right"><span id="char_31">180 </span>Caractères</span></div>
                            <textarea id="titre_art31" type="text" class="form-control textarea_style_designation" ></textarea>
                        </div>
                        <div class="col-2 price_cont_art">
                            <div class="col-12 title_txts">
                                Prix unitaire
                            </div>
                            <input id="prix_art31" type="number" class="form-control textarea_style" />
                        </div>
                        <input type="hidden" id="id_art31" name="id_art31" value="0">
                        <div class="d-none" id="save_ind31" onclick="save_art3(1)"></div>
                    </div>
                </div>
            <?php }else{$i3=1; ?>
                <?php foreach ($data_gli3 as $gli3) {  ?>
                    <div class="pt-3 lined3" id="gliss3<?php echo $i3; ?>">
                        <div class="w-100 row d-flex">
                            <div class="col-2 pr-0" id="img_contentss3<?php echo $i3; ?>">
                                <?php if ($gli3->image !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$user_ion_auth."/soutgli1/".$gli3->image)){ ?>
                                    <img class="img-fluid" src="<?php echo base_url()."application/resources/front/photoCommercant/imagesbank/".$user_ion_auth."/soutgli1/".$gli3->image ?>">
                                    <div class="w-100 pl-1"><div onclick="delete_image(<?php echo $gli3->id; ?>,3<?php echo $i3; ?>)" class="btn_delete_img">x</div></div>
                                <?php }else{ ?>
                                    <div id="Articlephoto3<?php echo $i3; ?>_container" onclick='javascript:window.open("<?php echo site_url("media/index/".$gli3->id."-soutgli1-photo3".$i3); ?>", "", "width=1045, height=675, scrollbars=yes");' href='javascript:void(0);' class="w-100 img_add text-center" style="border-radius: unset!important"><img class="w-100 h-100" src="<?php echo base_url()?>assets/images/download-icon-png.webp"></div>
                                <?php } ?>
                            </div>
                            <div class="col-8">
                                <div class="w-100 des_label">Titre produit ou service<span class="float-right"><span id="char_title_3<?php echo $i3; ?>">70 </span>Caractères</span></div>
                                <input type="text" id="true_title_art3<?php echo $i3; ?>" class="form-control true_title" value="<?php echo $gli3->true_title ?? '';?>">
                                <div class="w-100 des_label">Désignation<span class="float-right"><span id="char_3<?php echo $i3; ?>">180 </span>Caractères</span></div>
                                <textarea id="titre_art3<?php echo $i3; ?>" type="text" class="form-control textarea_style_designation" ><?php echo $gli3->titre;?></textarea>
                            </div>
                            <div class="col-2 price_cont_art">
                                <?php if (isset($gli3->id)){ ?>
                                    <div class="w-100" id="delete_art1<?php echo $i3; ?>">
                                        <div onclick="delete_art(<?php echo $gli3->id; ?>)" class="delete_img_div">x</div>
                                    </div>
                                <?php } ?>
                                <div class="w-100 title_txts">
                                    Prix unitaire
                                </div>
                                <input id="prix_art3<?php echo $i3; ?>" value="<?php echo $gli3->prix;?>" type="number" class="form-control textarea_style" />
                            </div>
                            <div class="d-none" id="save_ind3<?php echo $i3; ?>" onclick="save_art3(<?php echo $i3; ?>)"></div>
                            <input type="hidden" id="id_art3<?php echo $i3; ?>" name="id_art<?php echo $i3; ?>" value="<?php echo $gli3->id;?>">
                        </div>
                    </div>
                    <?php $i3++; } ?>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-12 pt-3">
                <div class="row">
                    <div class="col-1 pl-3 pr-0">
                        <div id="add_line3" class="btn_add_line">+</div>
                    </div>
                    <div class="col-31 pl-0 pt-2 text-left">
                        <div class="add_txts">Ajouter une ligne</div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center p-2">
                <button id="save_all3" class="btn btn-secondary w-50">Validation</button>
            </div>
        </div>
        <div class="row">
            <div class="col-12 pt-3 warn_img">
                Avant d'intégrer les images, vous devez préciser les textes et les prix unitaires et valider.
            </div>
        </div>
    </div>
</div>