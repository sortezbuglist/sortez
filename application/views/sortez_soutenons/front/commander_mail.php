
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="padding: 10px 0 30px 0;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="75%"
                           style="border: 1px solid #cccccc; border-collapse: collapse;">
                        
                        <?php if ($com == '0') { ?>
                            <tr>
                                <td style="text-align: center; padding: 40px 15px 10px 15px; color: #007bff; font-family: Arial, sans-serif;">
                                    <h2>MAGAZINE SORTEZ<br> COURRIER DE VALIDATION DE COMMANDE « LIVRÉ OU A EMPORTER »</h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"
                                    style="text-align: justify; padding: 0px 10px 0px 10px; color: #000000; font-size: 14px; font-family: Arial, sans-serif;">
                                    <p>Bonjour, <?php 
                                        $civilite = $infoclient->Civilite; if ($civilite == 0) {$civilite = 'Mr';}
                                        if ($civilite == 1) {$civilite = 'Mme';}
                                        if ($civilite == 2) {$civilite = 'Mlle';} ?><?php echo $civilite . " " . $infoclient->Prenom . " " . $infoclient->Nom; ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"
                                    style="text-align: justify; padding: 20px 15px 20px 15px; color: #007bff; font-size: 14px; font-family: Arial, sans-serif;">
                                    <b>Nom de l’établissement :</b> "<?php echo $oInfoCommercant->NomSociete;?>"
                                </td>
                            </tr>
                            <tr>
                                <td align="center"
                                        style="padding-left: 15px;padding-bottom:5px;text-align: justify; color: #007bff; font-size: 14px; font-family: Arial, sans-serif;">
                                        <b>Nom : </b><span style="font-size: 14px;"><?php echo $infoclient->Prenom . " " . $infoclient->Nom; ?></span>
                                    </td>
                            </tr>
                            <?php if(isset($infoclient->Adresse) && $infoclient->Adresse != null && $infoclient->Adresse != ""){ ?>
                                <tr>
                                    <td align="center"
                                        style="padding-left: 15px;padding-bottom:5px;text-align: justify; color: #007bff; font-size: 14px; font-family: Arial, sans-serif;">
                                        <b>Adresse : </b><span style="font-size: 14px;"><?php echo $infoclient->Adresse; ?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if(isset($infoclient->ville_nom) && $infoclient->ville_nom != null && $infoclient->ville_nom != ""){ ?>
                                <tr>
                                    <td align="center"
                                        style="padding-left: 15px;padding-bottom:5px;text-align: justify; color: #007bff; font-size: 14px; font-family: Arial, sans-serif;">
                                        <b>Ville : </b><span style="font-size: 14px;"><?php echo $infoclient->ville_nom; ?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if(isset($infoclient->Login) && $infoclient->Login != null && $infoclient->Login != ""){ ?>
                                <tr>
                                    <td align="center"
                                        style="padding-left: 15px;padding-bottom:5px; text-align: justify; color: #007bff; font-size: 14px; font-family: Arial, sans-serif;">
                                        <b>Courriel : </b><span
                                                style="font-size: 14px; color: red"><?php echo $infoclient->Login; ?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if(isset($infoclient->Telephone) && $infoclient->Telephone != null && $infoclient->Telephone != ""){ ?>
                                <tr>
                                    <td align="center"
                                        style="padding-left: 15px;padding-bottom:5px; text-align: justify; color: #007bff; font-size: 14px; font-family: Arial, sans-serif;">
                                        <b>Téléphone : </b><span
                                                style="font-size: 14px;"><?php echo $infoclient->Telephone; ?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php if(isset($infoclient->Portable) && $infoclient->Portable != null && $infoclient->Portable != ""){ ?>
                                <tr>
                                    <td align="center"
                                        style="padding-left: 15px;padding-bottom:5px; text-align: justify; color: #007bff; font-size: 14px; font-family: Arial, sans-serif;">
                                        <b>Tél. Mobile : </b><span
                                                style="font-size: 14px;"><?php echo $infoclient->Portable; ?></span>
                                    </td>
                                </tr>
                            <?php } ?>
                        
                        <?php } elseif ($com == '1') { ?>
                            <tr>
                                <td style="text-align: center; padding: 40px 15px 10px 15px; color: #007bff; font-family: Arial, sans-serif;">
                                    <h2>MAGAZINE SORTEZ<br> COURRIER DE RECEPTION DE COMMANDE « LIVRÉ OU A EMPORTER »</h2>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"
                                    style="text-align: justify; padding: 0px 10px 0px 10px; color: #007bff; font-size: 14px; font-family: Arial, sans-serif;">
                                    <p>Bonjour <?php 
                                        $civilite = $infoclient->Civilite; if ($civilite == 0) {$civilite = 'Mr';}
                                        if ($civilite == 1) {$civilite = 'Mme';}
                                        if ($civilite == 2) {$civilite = 'Mlle';} ?><?php echo $civilite . " " . $infoclient->Prenom . " " . $infoclient->Nom; ?>,</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px; color: #000; font-family: Arial, sans-serif; color: #007bff; font-size: 14px; line-height: 20px;">
                                    L’établissement : "<?php echo $oInfoCommercant->NomSociete;?>" vous remercie de votre commande.
<br>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 10px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; font-weight: bold;">
                                    Ceci est un mail de validation de votre commande.<br>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td style="padding: 10px; color: #007bff; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">
                                Le numéro de la carte vivresaville du client est : <?php echo $card->num_id_card_virtual ?? ''; ?><br>
                                <?php $real_path=base_url('application/resources/front/images/cards/'); ?>
                                <img src="<?php echo $real_path."qrcode_".$card->num_id_card_virtual.'.png';?>" alt="" width="100"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 0 10px; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">        
                                Vous trouverez ci-dessous le détail de
                                    <?php if ($com == '0') { ?>
                                        <span style="color: #007bff;">la</span> 
                                    <?php } elseif ($com == '1') { ?>
                                        <span style="color: #007bff;">votre</span> 
                                    <?php } ?>
                                Commande :    
                            </td>
                        </tr>
                        <?php if (isset($type_commande) and $type_commande == 'spec' and $com == '0') { ?>
                            <tr>
                                <td style="padding: 10px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">
                                    Le commande est du type "TELECHARGEMENT D'UNE COMMANDE OU DOCUMENT ET ENVOI".<br>
                                    La pièce jointe est téléchargeable via ce <a
                                            href="<?php echo base_url() ?>application/resources/front/command_clients/<?php echo $infoclient->IdUser ?>/<?php echo $img_file; ?>">lien</a><br>
                                    <?php if(isset($comment_clinet) && $comment_clinet != null){ ?>
                                        <b style="font-size: 14px">Le client a laissé un message:</b>
                                        <p><?php echo $comment_clinet; ?></p>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php }
                        if (isset($type_commande) and $type_commande == 'spec' and $com == '1') { ?>
                            <tr>
                                <td style="padding: 10px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">
                                    Votre commande est du type "TELECHARGEMENT D'UNE COMMANDE OU DOCUMENT ET ENVOI".<br>
                                    <?php if(isset($comment_clinet) && $comment_clinet != null && $comment_clinet != ""){ ?>
                                        <b style="font-size: 14px">Vous avez laissé un message:</b>
                                        <p><?php echo $comment_clinet; ?></p>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if (isset($categs) and !empty($categs)) { ?>
                            <?php foreach ($categs as $cat) { ?>
                                <tr>
                                    <td style="text-transform:uppercase; padding: 20px 20px 0 20px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">
                                        <b><?php echo $cat; ?></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table style="margin:0 20px 0 20px; width: 100%;">
                                            <tr>
                                                <td style="text-align: left; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; width: 60%;">
                                                    <b>
                                                        Produit
                                                    </b>
                                                </td>
                                                <td style="text-align: center;padding: 0px 20px 0px 5px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; width: 20%;">
                                                    <b>
                                                        Prix
                                                    </b>
                                                </td>
                                                <td style="text-align: center;padding: 0px 15px 0px 5px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; width: 20%;">
                                                    <b>
                                                        Nombre
                                                    </b>
                                                </td>
                                            </tr>
                                            <?php if (isset($to_view) and !empty($to_view)) { ?>
                                                <?php foreach ($to_view as $prod) { ?>
                                                    <?php if ($prod['categ_name'] == $cat) { ?>
                                                        <tr>
                                                            <td style="text-align:left; padding: 0px 10px 0 0; color: #0000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; width: 60%;"><?php echo $prod['product_name'] ?? ''; ?></td>
                                                            <td style="text-align:center; padding: 0px 10px 0 0; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; width: 20%;"><?php echo $prod['product_price'] ?? ''; ?><span style="color: #007bff">€</span></td>
                                                            <td style="text-align:center; padding: 0px 10px 0 0; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; width: 20%;"><?php echo $prod['nbre'] ?? ''; ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        <tr>
                            <td style="padding: 20px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: center;">
                                <b style="color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">Type
                                    de Livraison
                                    : </b><span style="color: #007bff"> <?php echo $type_livraison ?? "Le client n'a pas precisé"; ?> </span><br>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: center;">
                                <b>Jour souhaité :<span style="color: #007bff"> <?php echo $jour_enlev ?? "Le client n'a pas precisé"; ?> </span></b>
                            </td>  
                        </tr>
                        <tr>
                            <td style="color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: center;">
                               <b>Heure souhaité : <span> <?php echo $heure_enlev ?? "Le client n'a pas precisé"; ?> </span></b> 
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 10px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: center;">      
                                <?php if (isset($total_prix) && $total_prix != null && $total_prix != ""){ ?> 
                                    <b style="color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">Montant
                                        total ttc : <?php echo $total_prix; ?> € </b> <?php if (isset($remise)){ echo "dont <b>".$remise."</b> de remise dûe à <b>l'OFFRE PROMOTIONNELLE</b>";} ?><br>
                                <?php } ?>
                            </td>    
                        </tr>

                        <?php if (isset($type_payement) && $type_payement != null && $type_payement != ""){ ?>
                            <tr>
                                <td style="padding: 20px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: center;">
                                    <b style="color: #007bff;">
                                        Type de payement : </b><?php echo $type_payement; ?>  <br>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if (isset($type_facture_differe) AND $type_facture_differe !=""){ ?>
                            <tr>
                                <td style="padding: 20px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: center;">
                                    <b>Type
                                        de Facture
                                        : </b><span> <?php echo $type_facture_differe ?? "Le client n'a pas precisé"; ?> </span><br>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if (isset($comment) && $comment != null && $comment != ""){ ?>
                            <tr>
                                <td style="padding: 10px; color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px; text-align: center;">
                                    <b style="color: #000; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">
                                        Question/Commentaires du Client :<br> </b><?php echo $comment; ?>  <br>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php if ($com == '1') { ?>
                            <tr>
                                <td style="padding: 20px; color: #153643; font-family: Arial, sans-serif; line-height: 20px; text-align: center;">
                                        <span style="font-size: 14px;"> Cette commande a été conçue avec le Market Place du Magazine Sortez - www.magazine-sortez.org<br>
                                        Priviconcept SAS au capital de 1000€,  427, Chemin de Vosgelade, Le Mas Raoum,  06140 Vence</span><br>
                                        <span style="font-size: 12px;">Siret : 820 043 693 00010 - Code NAF : 6201</span>
                                </td>
                            </tr>
                        <?php } ?>
                    
                    </table>
                </td>
            </tr>
        </table>