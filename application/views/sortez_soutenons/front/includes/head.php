<div class="row mt-4">
    <div class="col-12 text-center">
        <h3 class="titre_header">COMMANDER</h3>
    </div>
</div>
<div class="row">
    <div class="col-12 mb-3">
        <p class="text_head1">
            Notre établissement est partenaire de la Carte Vivresaville.
        </p>
        <p class="text_head1">
            Vous devez obligatoirement vous identifier en précisant le numéro de votre carte,
        </p>
        <p class="text_head1">
            Dès validation, vos coordonnées apparaîtront directement sur cette commande...
        </p>
    </div>
</div>
<div class="row head_commande pb-3">
    <div class="col-2" style="margin-left: 0px">
        <img src="<?php echo base_url() ?>/assets/images/image_left.png" style="margin-top: -12px;margin-left: -8px;">
    </div>
    <div class="col-8">
        <div class="row">
            <div class="col-6 offset-3 d-flex mb-3">
                <input id="number_card" class="form-control w-75 textarea_style_cherche" type="text"/>
                <button onclick="get_users_info()" class="btn btn-success w-25 btn_cherche">Ok</button>
            </div>
            <div class="col-11 offset-1">
                <p class="text_head1">Accéder directement à votre carte dématérialisée et à son numéro</p>
            </div>
            <div class="col-6 offset-3 mt-3">
                <button onclick="get_users_info()" class="btn btn-success w-25 btn_carte">Votre carte</button>
            </div>
        </div>
    </div>
    <div class="col-2" id="qr_content" style="margin-left: -20px;">
        <img src="<?php echo base_url() ?>/assets/images/qr_right.png">
    </div>
</div>

<div class="row head_commande">
    <div class="col-10 offset-1 pb-4">
        <div class="row">
            <div class="col-6 offset-4">
                <img src="<?php echo base_url('/assets/soutenons/logo_vivresaville.png'); ?>">
            </div>
            <div class="col-12">
                <p class="text_head1">Par contre, si vous ne la possédez pas encore, vous pouvez la demander gratuitement</p>
                <p class="text_head1">en validant le formulaire ci-dessous</p>
            </div>
            <div class="col-6">
                <div class="w-100 pt-2">
                    <label class="label_input">Nom</label>
                    <input id="nom_client" placeholder="Nom" class="form-control textarea_style" type="text"/>
                </div>
            </div>
            <div class="col-6">
                <div class="w-100 pt-2">
                    <label class="label_input">Prénom</label>
                    <input id="prenom_client" placeholder="Prénom" class="form-control textarea_style" type="text"/>
                </div>
            </div>
            <div class="col-12 mt-2">
                <label class="label_input">Adresse</label>
                <input id="adresse_client" placeholder="Adresse" class="form-control textarea_style" type="text"/>
            </div>
            <div class="col-6">
                <div class="w-100 pt-2">
                    <label class="label_input">Code postal</label>
                    <input id="code_postal_client" placeholder="Code postal" class="form-control textarea_style" type="text"/>
                </div>
                <div class="w-100 pt-2">
                    <label class="label_input">Téléphone mobile</label>
                    <input id="telephone_client" placeholder="Téléphone mobile" class="form-control textarea_style" type="text"/>
                </div>
                <div class="w-100 pt-2">
                    <label class="label_input">Courriel</label>
                    <input id="mail_client" placeholder="Mail" class="form-control textarea_style" type="mail"/>
                </div>
            </div>
            <div class="col-6">
                <div class="w-100 pt-2">
                    <label class="label_input">Ville</label>
                    <input id="ville_client" placeholder="Ville" class="form-control textarea_style" style="height: 43.2px" type="text"/>
                </div>
                <div class="w-100 pt-2">
                    <label class="label_input">Date de naissance</label>
                    <input id="date_naissance_client" placeholder="Date de naissance" class="form-control textarea_style" type="text"/>
                </div>
                <div class="w-100 pt-2">
                    <label class="label_input">Mot de passe</label>
                    <input id="Mot_de_passe" placeholder="Mot de passe" class="form-control textarea_style" type="password"/>
                </div>
            </div>
        </div>
        <div id="txt_error_add"></div>
        <div class="row pt-4">
            <div class="col-6">
                <button onclick="subscribtion()" class="btn btn_after_input text-center"  id="demande_carte">
                    Je demande une carte vivresaville
                </button>
            </div>
            <div class="col-6">
                <button onclick="" class="btn btn_after_input text-center">
                    Les autres avantages
                </button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-6 offset-6">
        <div class="row">
            <div class="col-6 offset-2 pr-0">
                <p class="text_head1">Activation de la vente à emporter ou la livraison à domicile.</p>
            </div>
            <div class="col-4 text-right pl-0">
                <img src="<?php echo base_url('/assets/images/fleche.png') ?>">
            </div>
        </div>
    </div>
</div>

            <input type="hidden" id="activ_glissiere_emporter_value" value="0">
            <input type="hidden" id="activ_glissiere_livraison_value" value="0">
            <input type="hidden" id="activ_glissiere_activ1_value" value="0">
            <input type="hidden" id="activ_glissiere_activ2_value" value="0">
            <input type="hidden" id="activ_glissiere_activ3_value" value="0">
            <input type="hidden" id="activ_glissiere_activ4_value" value="0">
            <input type="hidden" id="activ_glissiere_activ7_value" value="0">
            <input type="hidden" id="activ_glissiere_activ6_value" value="0">
            <input type="hidden" id="activ_glissiere_activ7_value" value="0">
            <input type="hidden" id="value_emp_activ" value="0">
            <input type="hidden" id="value_livr_activ" value="0">
            <input type="hidden" id="all_added_product1">
            <input type="hidden" id="all_added_product2">
            <input type="hidden" id="all_added_product3">
            <input type="hidden" id="all_added_product4">
            <input type="hidden" id="all_added_product6">
            <input type="hidden" id="all_added_product7">
            <input type="hidden" id="id_client" value="0">