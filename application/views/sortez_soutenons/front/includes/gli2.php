<?php if (isset($data_gli2) and !empty($data_gli2) and isset($title_gli2->is_activ_glissiere) and ($title_gli2->is_activ_glissiere != 0)) { ?>
    <!--<input type="hidden" id="totalgli2" value="0">-->
    <div class="row glissiere_tab marge_gli_1">
        <form class="m-0 w-100 row d-flex">
            <div class="col-4 d-flex">
                <div id="txt_title_gli2"
                     class="text-left title_gli d-flex titre_gli_up">
                    <div class="round_num">2</div>
                    <div class="title_categs">
                    <?php if (isset($title_gli2->titre_glissiere) and $title_gli2->titre_glissiere != null) {
                        echo $title_gli2->titre_glissiere;
                    } else {
                        echo "TITRE";
                    } ?>
                    </div>
                </div>
            </div>
            <div class="col-4 drop_up_down" id="activ_glissiere_activ2">
                <div class="cercle">
                    <div class="text-center up_1 d-none" id="activ_glissiere_activ2_up"></div>
                    <div class="text-center down_1" id="activ_glissiere_activ2_down"></div>
                </div>
            </div>
            <div class="col-4">
                <div class="text-right text_head" style="margin-left: -95px;">
                    Montant total TTC de cette catégorie<input id="gli2total" disabled="disabled" class="p-2 ml-1 input_glissiere" type="text" value="0€"/>
                </div>
            </div>
        </form>
    </div>

    <div class="row pr-3 pb-5 pt-4 d-none head_commande" id="activ_glissiere_activ2_content">
        <div class="row">
            <?php $i2 = 0;
            foreach ($data_gli2 as $gli2) { ?>
                <div class="col-12">

                </div>
                <div class="col-12 p-0 pl-3">
                    <div class="row">
                        <div class="col-3 d-flex w-100 img-glisss">
                            <?php if (isset($gli2->image) and is_file("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli2->image)) { ?>
                                <img class="img-fluid" style="align-self: center"
                                     src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli2->image); ?>">
                            <?php } else { ?>
                                <p class="text_head pl-4" style="align-self: center">pas d'images</p>
                            <?php } ?>
                        </div>
                        <div class="col-7">
                            <?php if (isset($gli2->true_title) AND $gli2->true_title !=null ){ ?>
                                <div class="w-100 text-left true_title">
                                    <?php echo $gli2->true_title ?>
                                </div>
                            <?php } ?>
                            <div class="w-100 text-center pt-2">
                                <?php if (isset($gli2->titre) && $gli2->titre != null) { ?>
                                    <input class="id_gliss_2" value="<?php echo $gli2->id; ?>" type="hidden" id="product_gli2_<?php  echo $i; ?>" />
                                    <p class="description_categorie">
                                        <?php echo $gli2->titre; ?>
                                    </p>
                                <?php } else { ?>
                                    <p class="titre_categorie pt-3 mb-0">TITRE</p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-2 pt-2">
                            <div class="row text-center">

                                <?php if (isset($gli2->prix) and $gli2->prix != null) { ?>
                                    <input id="prix2<?php echo $i2; ?>" type="hidden" value="<?php echo $gli2->prix; ?>">
                                    <label class="col-12 text_label">Prix unitaire</label>
                                    <div class="col-12">
                                        <input class="nbre_gliss2_pr textarea_style_input text_label w-100 p-2 " value="<?php echo $gli2->prix . " €"; ?>" disabled>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <div class="col-12 text_label">Quantité</div>
                                <div class="col-12 pb-2">
                                    <input value="0" onchange="change_total_price2('prix2<?php echo $i2 . '\''; ?>,'nbre2<?php echo $i2 . '\''; ?>,'product_gli4_<?php  echo $i.'\''; ?>)" id="nbre2<?php echo $i2; ?>" type="number" class="w-100 textarea_style_input nbre_gliss2_qt text_label p-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ml-2 mr-1" style="border-bottom: solid 1px rgba(145, 145, 145, 1)"></div>
                </div>
                <?php $i2++;
            } ?>
        </div>
    </div>
<?php } ?>