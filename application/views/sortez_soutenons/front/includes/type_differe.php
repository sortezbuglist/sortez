<?php if (isset($datacommande->is_activ_vente_differe) and $datacommande->is_activ_vente_differe == '1') { ?>
    <div class="row glissiere_tab marge_gli_1">
        <input type="hidden" id="activ_glissiere_differe_value" value="0">
        <form class="m-0 w-100 row d-flex">
            <div class="col-5 d-flex">
                <div id="txt_title_gli1" class="text-left title_gli titre_gli_up pt-1" style="margin-right: -70px;">VENTE EN LIGNE - PAIEMENT DIFFERE</div>
            </div>
            <div class="col-2 drop_up_down" id="activ_glissiere_differe">
                <div class="cercle">
                    <div class="text-center up_1 d-none" id="activ_glissiere_differe_up"></div>
                    <div class="text-center down_1" id="activ_glissiere_differe_down"></div>
                </div>
            </div>
            <div class="col-5 text-right">
                <label class="switch" id="switch_check_differe">
                    <input type="checkbox" onchange="switch_differe()" id="value_differe_activ_value">
                    <span class="slider round"></span>
                </label>
                <input type="hidden" id="value_differe_activ_value_hidden" value="0">
            </div>
        </form>
    </div>
    <div class="row p-2 pb-5 pt-3 d-none head_commande activ_glissiere_differe_child" id="differe_content">
        <div class="row pt-2">

            <div class="col-lg-12">
                <div class="title_differe">
                    COMMANDE EN LIGNE :  ENVOIS DE DOCUMENTS (devis, factures...) POUR LES COLLECTIVITES ET ENTREPRISES AVEC UN PAIEMENT EN RETOUR (virements, chèques)
                </div>
            </div>

        </div>

        <div class="col-12">
            <div class="text_label text-center text-justify">Jours et heures de contact</div>
            <div class="col-12 pl-0 pr-0">
                <textarea disabled="disabled" id="comment_differe" class="textarea_style_input form-control text_head p-3"><?php if (isset($datacommande->horaire_contact) AND $datacommande->horaire_contact !=null){ echo $datacommande->horaire_contact; } ?></textarea>
            </div>
        </div>
    </div>
<?php } ?>