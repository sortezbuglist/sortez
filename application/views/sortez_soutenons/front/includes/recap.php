<div class="row row glissiere_tab marge_gli_1">
    <div class="text-left titre_gli_up title_gli pl-2">RECAPITULATIF DES VALEURS</div>
</div>
<div class="row pt-2">
    <div class="col-12">
        <div class="row">
            <div class="col-10">
                <script type="text/javascript">
                    $(document).ready(function() {
                        // if($('#value_emp_activ').val() == 1 && $('#value_livr_activ').val() == 0){
                        //     $('#type_commande').html('Vous avez choisi la commande à emporter');
                        // }else if($('#value_emp_activ').val() == 0 && $('#value_livr_activ').val() == 1){
                        //     $('#type_commande').html('Vous avez choisi la livraison à domicile');
                        // }else{
                        //     $('#type_commande').html("Veuillez choisir commande à emporté ou livraison à domicile");
                        // }
                    });
                </script>
                    <p class="text_head" id="type_commande"></p>
            </div>
            <div class="col-2">

            </div>
            <div class="col-10 mt-4">
                <div class="row">
                    <input type="hidden" id="totalgli1" value="0">
                    <input value="0" id="totalgli2" type="hidden" />
                    <input value="0" id="totalgli3" type="hidden" />
                    <input value="0" id="totalgli4" type="hidden" />
                    <input value="0" id="totalgli6" type="hidden" />
                    <input value="0" id="totalgli7" type="hidden" />
                    <?php if (isset($title_gli1) AND !empty($title_gli1)){ ?>
                        <div class="col-2 text-center">
                            <div class="heigth_tt text_label">
                                <div class="m-auto round_num">1</div>
                            </div><br>
                            <input value="0€" disabled="disabled" id="subs_bottomgli1" class="w-100 textarea_style_input text_label2 text-center p-2" type="text" />
                            <?php if ((isset($title_gli2) AND !empty($title_gli2)) || (isset($title_gli3) AND !empty($title_gli3)) || (isset($title_gli4) AND !empty($title_gli4)) || (isset($title_gli6) AND !empty($title_gli6)) || (isset($title_gli7) AND !empty($title_gli7))){ ?>
                                <span class="plus_somme">
                                    <img src="<?php echo base_url('/assets/images/plus.png')?>">
                                </span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($title_gli2) AND !empty($title_gli2)){ ?>
                        <div class="col-2 text-center">
                            <div class="heigth_tt text_label">
<!--                                --><?php //echo $title_gli2->titre_glissiere ?>
                                <div class="m-auto round_num">2</div>
                            </div><br>
                            <input value="0€" disabled="disabled" id="subs_bottomgli2" class="w-100 textarea_style_input text_label2 text-center p-2" type="text" />
                            <?php if ((isset($title_gli3) AND !empty($title_gli3)) || (isset($title_gli4) AND !empty($title_gli4)) || (isset($title_gli6) AND !empty($title_gli6)) || (isset($title_gli7) AND !empty($title_gli7))){ ?>
                                <span class="plus_somme">
                                    <img src="<?php echo base_url('/assets/images/plus.png')?>">
                                </span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($title_gli3) AND !empty($title_gli3)){ ?>
                        <div class="col-2 text-center">
                            <div class="heigth_tt text_label">
<!--                                --><?php //echo $title_gli3->titre_glissiere ?>
                                <div class="m-auto round_num">3</div>
                            </div><br>
                            <input value="0€" disabled="disabled" id="subs_bottomgli3" class="w-100 textarea_style_input text_label2 text-center p-2" type="text" />
                            <?php if ((isset($title_gli4) AND !empty($title_gli4)) || (isset($title_gli6) AND !empty($title_gli6)) || (isset($title_gli7) AND !empty($title_gli7))){ ?>
                                <span class="plus_somme">
                                    <img src="<?php echo base_url('/assets/images/plus.png')?>">
                                </span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($title_gli4) AND !empty($title_gli4)){ ?>
                        <div class="col-2 text-center">
                            <div class="heigth_tt text_label">
<!--                                --><?php //echo $title_gli4->titre_glissiere ?>
                                <div class="m-auto round_num">4</div>
                            </div><br>
                            <input value="0€" disabled="disabled" id="subs_bottomgli4" class="w-100 textarea_style_input text_label2 text-center p-2" type="text" />
                            <?php if ((isset($title_gli6) AND !empty($title_gli6)) || (isset($title_gli7) AND !empty($title_gli7))){ ?>
                                <span class="plus_somme">
                                    <img src="<?php echo base_url('/assets/images/plus.png')?>">
                                </span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($title_gli6) AND !empty($title_gli6)){ ?>
                        <div class="col-2 text-center">
                            <div class="heigth_tt text_label">
<!--                                --><?php //echo $title_gli6->titre_glissiere ?>
                                <div class="m-auto round_num">5</div>
                            </div><br>
                            <input value="0€" disabled="disabled" id="subs_bottomgli6" class="w-100 textarea_style_input text_label2 text-center p-2" type="text" />
                            <?php if (isset($title_gli7) AND !empty($title_gli7)){ ?>
                                <span class="plus_somme">
                                        <img src="<?php echo base_url('/assets/images/plus.png')?>">
                                    </span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($title_gli7) AND !empty($title_gli7)){ ?>
                        <div class="col-2 text-center">
                            <div class="heigth_tt text_label">
<!--                                --><?php //echo $title_gli7->titre_glissiere ?>
                                <div class="m-auto round_num">6</div>
                            </div><br>
                            <input value="0€" disabled="disabled" id="subs_bottomgli7" class="w-100 textarea_style_input text_label2 text-center p-2" type="text" />
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-2 mt-4">
                <div class="row">
                    <div class="col-12 p-0 pl-4">
                        <p class="text_label">Montant total ttc à régler</p>
                    </div>
                    <div class="col-2 pl-0 pr-0">
                            <span class="egale">
                                <img src="<?php echo base_url('/assets/images/egale.png')?>">
                            </span>
                    </div>
                    <div class="col-10 pl-2 pr-0">
                        <input disabled="disabled" id="all_totalised" class="w-100 text_label2 textarea_style_input p-2" type="text" value="0€">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (isset($datacommande->price_to_reach) AND $datacommande->price_to_reach !=null && isset($datacommande->price_to_win) AND $datacommande->price_to_win !=null ){ ?>
<div class="row pt-4 mt-3 pb-4 prom_row">
    <div class="col-3 text-center">
        <img src="<?php echo base_url()?>assets/images/cible.webp">
    </div>
    <div class="col-9">
        <div class="title_prom text-center">PROFITEZ DE NOTRE OFFRE PROMOTIONNELLE</div>
            <div class="row">
                <div class="col-6">
                    <div class="row">
                        <div class="col-8 pl-0 text-prom">
                            Si vous dépassez un
                            achat d'un montant de
                        </div>
                        <div class="col-4 pl-0 pt-2">
                            <input value="<?php echo $datacommande->price_to_reach; ?>" class="w-100 mr-2 input_prom" type="number" disabled>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="row">
                        <div class="col-8 text-prom">
                            Vous bénéficiez d'une
                            remise immédiate de
                        </div>
                        <div class="col-4 pl-0 pt-2">
                            <input value="<?php echo $datacommande->price_to_win; ?>" class="w-100 input_prom" type="number" disabled>
                        </div>
                    </div>
                </div>
        </div>
        <input type="hidden" id="is_graced" value="0">
        <div class="row pt-4 pb-4">
            <div id="text_warn" style="line-height:35px;font-size: 25px!important" class="col-lg-12 text-center title_prom">
                Désolez ! votre montant d'achat est inférieur,
                vous ne pouvez pas en bénéficiez !...
            </div>
        </div>
        <div class="row d-none" id="is_bravo">
            <div class="col-6">
                <div class="row">
                    <div class="col-8 pl-0 pt-2 text-prom">
                        Remise TTC à déduire
                    </div>
                    <div class="col-4 pl-0 pt-2">
                        <input value="<?php echo $datacommande->price_to_win; ?>" class="w-100 mr-2 input_prom" type="number" disabled>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-8 text-prom">
                        Nouveau montant
                        total TTC à régler
                    </div>
                    <div class="col-4 pl-0 pt-2">
                        <input id="rest_to_give" value="0" class="w-100 input_prom" type="number" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="row pb-2 mt-2">-->
<!--    <div class="col-12">-->
<!--        <div class="w-100">-->
<!--            <label class="text_label text-left">Commentaires</label>-->
<!--            <textarea id="comment_comm" class="form-control textarea_style_input"></textarea>-->
<!--        </div>-->
<!--        <div id="commande_error" class="w-100 text-center">-->
<!---->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
    <?php } ?>
<?php if (isset($datacommande->is_activ_type_livr_grat) AND $datacommande->is_activ_type_livr_grat == '1'){ ?>
    <div class="row emp_contentss">
        <div class="col-lg-12 pt-4">
            <div class="w-100 middle_title">
                VENTE EN LIGNE A EMPORTER / RETRAIT ET PAIEMENT EN MAGASIN
            </div>
            <div class="w-100 pt-4">
                <p><?php echo $datacommande->comment_emporter_txt ?? ""; ?></p>
            </div>
        </div>
    </div>
<?php } ?>
<?php //if (isset($datacommande->is_activ_paypal) AND $datacommande->is_activ_paypal == '1' ){ ?>
<div class="row emp_contentss pl-3 shadowed pb-4">
<!--    --><?php //if ($datacommande->cgv_file != null && is_file("application/resources/front/photoCommercant/cgv/".$infocom->user_ionauth_id."/".$datacommande->cgv_file)){ ?>
<!--    <div class="col-12 cond pt-4">-->
<!--        <div class="row">-->
<!--            <div class="col-1">-->
<!--                <label class="switch_carree" >-->
<!--                    <input type="checkbox" id="cond_accepted">-->
<!--                    <span class="slider_carree round_carree"></span>-->
<!--                </label>-->
<!--            </div>-->
<!--            <div class="col-11 pt-2">-->
<!--                <span class="txt_cond" >J'accepte les  conditions générales du fournisseur <a target="_blank" href="--><?php //echo base_url()?><!--application/resources/front/photoCommercant/cgv/--><?php //echo $infocom->user_ionauth_id."/".$datacommande->cgv_file;?><!--"><span class="link_blue">(voir les conditions).</span></span></a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--        --><?php //} ?>
<!--    <div class="col-12 cond pt-2">-->
<!--        <div class="row">-->
<!--            <div class="col-1">-->
<!--                <label class="switch_carree" id="">-->
<!--                    <input type="checkbox" id="paypal_accepted">-->
<!--                    <span class="slider_carree round_carree"></span>-->
<!--                </label>-->
<!--            </div>-->
<!--            <div class="col-11 pt-2">-->
<!--                <span class="txt_cond" >J'accepte le règlement de cette commande avec le module Paypal en précisant le montant total ttc net a régler, valider le bouton PAYPAL qui se trouve sur paragraphe suivant.</span>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="col-12 cond pt-2">-->
<!--        <div class="row">-->
<!--            <div class="col-1">-->
<!--                <label class="switch_carree" >-->
<!--                    <input type="checkbox" id="accept_type_livraison">-->
<!--                    <span class="slider_carree round_carree"></span>-->
<!--                </label>-->
<!--            </div>-->
<!--            <div class="col-11 pt-1">-->
<!--                <span class="txt_cond" >J'accepte de régler cette commande lors de la livraison à domicile ou à l'enlèvement.</span>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <input type="hidden" id="type_paiment_cli" value="0">
    <div class="col-12 cond pt-2">
        <div class="row">
            <div class="col-3 p-0">Conditions de règlement</div>
            <?php if ($datacommande->is_activ_cheque_enlev =="1" ||  $datacommande->is_activ_cheque_enlev =="1"){ ?>
                    <div class="col-2">
                        <input id="type_norm_cheque" class="check_cond" type="checkbox">
                        <span class="txt_cond" >Chèque</span>
                    </div>
            <?php } ?>
            <?php if ($datacommande->is_activ_termin_banc_enlev =="1"  || $datacommande->is_activ_termin_banc_enlev =="1"){ ?>
                <div class="col-1">
                    <input id="type_norm_bank" class="check_cond" type="checkbox">
                </div>
                <div class="col-2 pl-0">
                    <span class="txt_cond">Carte bancaire</span>
                </div>
            <?php } ?>
            <?php if ($datacommande->is_activ_espece_enlev =="1"  || $datacommande->is_activ_espece_enlev =="1"){ ?>
                <div class="col-1">
                    <input id="type_norm_espace" class="check_cond" type="checkbox">
                </div>
                <div class="col-2 pl-0">
                    <span class="txt_cond">Espèces</span>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
    <div class="row mt-4 livr_contentss">
        <div class="col-lg-12 pt-4">
            <div class="w-100 middle_title">
                VENTE EN LIGNE :  LIVRAISON ET PAIEMENT A DOMICILE
            </div>
            <div class="w-100 pt-4">
                <p><?php echo $datacommande->commune_livr_desserv ?? "";?></p>
            </div>
        </div>
    </div>
<?php if (isset($datacommande->is_activ_type_livr) AND $datacommande->is_activ_type_livr == '1'){ ?>
<div class="row pl-3 shadowed pb-4 livr_contentss">
    <div class="col-12 cond pt-2">
        <div class="row">
            <div class="col-3 p-0">Conditions de règlement</div>
            <?php if ($datacommande->is_activ_cheque_livr =="1" ||  $datacommande->is_activ_cheque_livr =="1"){ ?>
                <div class="col-4 pr-0 pl-0">
                    <input id="type_norm_cheque_livr" class="check_cond" type="checkbox">
                    <span class="txt_cond" >Chèque dûment rempli et signé</span>
                </div>
            <?php } ?>
            <?php if ($datacommande->is_activ_termin_bank_livr =="1"  || $datacommande->is_activ_termin_bank_livr =="1"){ ?>
                <div class="col-1">
                    <input id="type_norm_bank_livr" class="check_cond" type="checkbox">
                </div>
                <div class="col-4 pl-0">
                    <span class="txt_cond">Carte bancaire avec un terminal de paiement</span>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
    <?php } ?>

<div class="row diff_contentss">
    <div class="col-lg-12 pt-4">
        <div class="w-100 text-center middle_title">
            COMMANDE EN LIGNE :  ENVOIS DE DOCUMENTS (devis, factures...) ET PAIEMENT EN RETOUR
        </div>
        <div class="w-100 pt-4">
            <p>
                <?php echo $datacommande->comment_differe_txt ?? ""; ?>
            </p>
        </div>
    </div>
</div>
<?php if (isset($datacommande->is_activ_fact_type_differe) AND $datacommande->is_activ_fact_type_differe =="1"){ ?>
<div class="row diff_contentss">
    <?php if (isset($datacommande->is_activ_devis_differe) AND $datacommande->is_activ_devis_differe == '1'){ ?>
    <div class="col-2">
        <input id="is_activ_devis_differe" class="check_cond" type="checkbox"> Devis
    </div>
    <?php } ?>
    <?php if (isset($datacommande->is_activ_pro_forma_differe) AND $datacommande->is_activ_pro_forma_differe == '1'){ ?>
    <div class="col-3">
        <input id="is_activ_pro_forma_differe" class="check_cond" type="checkbox"> Facture pro forma
    </div>
    <?php } ?>
    <?php if (isset($datacommande->is_activ_facture_differe) AND $datacommande->is_activ_facture_differe == '1'){ ?>
    <div class="col-3">
        <input id="is_activ_facture_differe" class="check_cond" type="checkbox"> Facture
    </div>
    <?php } ?>
</div>
    <?php } ?>
<?php if (isset($datacommande->is_activ_banc_type_differe) AND $datacommande->is_activ_banc_type_differe == "1"){ ?>
<div class="row pt-2 pb-4 shadowed diff_contentss">
    <?php if (isset($datacommande->is_activ_Cheque_differe) AND $datacommande->is_activ_Cheque_differe == '1'){ ?>
    <div class="col-2">
        <input id="is_activ_Cheque_differe" class="check_cond" type="checkbox"> Chèque
    </div>
    <?php } ?>
    <?php if (isset($datacommande->is_activ_Virement_differe) AND $datacommande->is_activ_Virement_differe == '1'){ ?>
    <div class="col-3">
        <input id="is_activ_Virement_differe" class="check_cond" type="checkbox"> Virement
    </div>
    <?php } ?>
    <?php if (isset($datacommande->is_activ_carte_differe) AND $datacommande->is_activ_carte_differe == '1'){ ?>
    <div class="col-3">
        <input id="is_activ_carte_differe" class="check_cond" type="checkbox"> Carte bancaire
    </div>
    <?php } ?>
</div>
    <?php } ?>

<?php if (isset($datacommande) AND $datacommande->is_activ_livr_paypal == '1'){ ?>
<div class="row livr_contentss">
    <div class="col-lg-12 pt-4">
        <div class="w-100 text-center middle_title">
            VENTE ET PAIEMENT A LA COMMANDE EN LIGNE AVEC PAYPAL
        </div>
    </div>
</div>
<div class="row pt-3 pb-4 shadowed livr_contentss">
    <div class="col-1 p-0 pt-2 text-center">
        <input id="accept_paypal" class="check_cond w-100" type="checkbox">
    </div>
    <div class="col-11 pl-0">
        <?php echo $datacommande->new_paypal_comment ?? ""; ?>
    </div>
</div>
    <?php } ?>
<?php if (isset($datacommande->cgv_file) AND is_file("application/resources/front/photoCommercant/cgv/".$infocom->user_ionauth_id."/".$datacommande->cgv_file)){ ?>
<div class="row pt-5 pb-4 shadowed">
    <div class="col-1 p-0 pt-2 text-center">
        <input id="accept_cond_gen" class="check_cond w-100" type="checkbox">
    </div>
    <div class="col-5 pl-0 pt-2">
        J'accepte les  conditions générales de vente
    </div>
    <div class="col-3">
        <input disabled class="form-control" type="text" value="<?php if (isset($datacommande->cgv_file) AND is_file("application/resources/front/photoCommercant/cgv/".$infocom->user_ionauth_id."/".$datacommande->cgv_file)){echo base_url()."application/resources/front/photoCommercant/cgv/".$infocom->user_ionauth_id."/".$datacommande->cgv_file; } ?>">
    </div>
    <div class="col-3">
        <a href="<?php echo base_url()."application/resources/front/photoCommercant/cgv/".$infocom->user_ionauth_id."/".$datacommande->cgv_file; ?>" target="_blank" ><div class="w-100 h-100 btn btn_after_input_blue">+Téléchargement</div></a>
    </div>
</div>
<?php } ?>

<div class="row pb-2 mt-4">
    <div class="col-12">
        <div class="w-100">
            <label class="text_label w-100 text-center">Questions ou Commentaires</label>
            <textarea id="comment_comm" class="form-control textarea_style_input"></textarea>
        </div>
        <div id="commande_error" class="w-100 text-center">

        </div>
    </div>
</div>

<div class="row shadowed pb-4">
    <div class="col-4 pl-1 pr-1 pt-0 mt-5 text-center">
        <button class="w-100 btn btn_after_input" id="validate_command" onclick="send_command()">Validation de la commande</button>
    </div>
<!--    <div class="col-6 pt-3 text-center">-->
<!--        <div class="row">-->
<!--            <div class="col-8 offset-2 pt-4">-->
<!--                --><?php //if (isset($datacommande->paypal_content) AND $datacommande->paypal_content !='' AND $datacommande->paypal_content != null) { print  html_entity_decode($datacommande->paypal_content) ;}else{?>
<!--                <img src="--><?php //echo base_url('/assets/images/paypal.png') ?><!--" style="width: 100%">-->
<!--                 --><?php //} ?>
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
    <div class="col-4 pl-1 pr-1 pt-5 text-center">
        <button class="w-100 btn btn_after_input_blue" id="validate_command" onclick="">Impression du justificatif de réception</button>
    </div>
    <div class="col-4 pl-1 pr-1 pt-5 text-center">
        <button class="w-100 btn btn_after_input_blue" id="validate_command" onclick="">Export de la commande en PDF</button>
    </div>
</div>
<?php //} ?>