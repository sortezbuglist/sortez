<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<?php if((isset($datacommande->new_paypal_comment) && $datacommande->new_paypal_comment != null AND isset($datacommande->is_activ_paypal) AND $datacommande->is_activ_paypal =='1' )AND ($datacommande->is_activ_paypal_enlev == '1' || $datacommande->is_activ_paypal_livr == "1" ) ){ ?>
<div class="row row glissiere_tab">
    <div class="text-center w-100 titre_gli_up title_gli pl-2">Règlement par Paypal</div>
</div>
<div class="row">
    <div class="col-3" style="padding-top: 10%">
        <?php if (isset($datacommande->paypal_content) AND $datacommande->paypal_content !='' AND $datacommande->paypal_content != null) { print  html_entity_decode($datacommande->paypal_content) ;}else{?>
            <img class="img-fluid" src="<?php echo base_url('/assets/images/paypal.png') ?>" style="width: 100%">
        <?php } ?>
    </div>
    <div class="col-9 pt-3">
        <ol class="font_7" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:15px;">
            <li>
                <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Après avoir confirmé votre commande vous effectuer le règlement par carte bancaire. ;</span></span></span></p>
            </li>
            <li>
                <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Il vous suffira de noter sur le module PAYPAL, le montant total TTC&nbsp;indiqué ci-dessus ;</span></span></span></p>
            </li>
            <li>
                <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Imprimez et signez le justificatif de bonne réception ;</span></span></span></p>
            </li>
            <li>
                <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Dès réception, nous vous contacterons pour vous remercier et vous préciser les modalités de votre livraison ;</span></span></span></p>
            </li>
            <li>
                <p class="font_7 m-0 pb-2" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Remettez le justificatif de bonne réception à notre livreur ou dans notre établissement dans le cadre d'une vente à emporter.</span></span></span></p>
            </li>
        </ol>
    </div>
</div>


<?php } ?>

<?php if (isset($datacommande->is_activ_com_spec) AND $datacommande->is_activ_com_spec =="1" ){ ?>
<div class="row">
    <img class="w-100 pt-4" src="<?php echo base_url('assets/images/image50.webp')?>" />
</div>

<div class="row row glissiere_tab">
    <div class="text-left titre_gli_up title_gli pl-2">TELECHARGEMENT D'UNE COMMANDE OU DOCUMENT ET ENVOI</div>
</div>
    <form method="post" enctype="multipart/form-data" id="upload_file_custom" />
<div class="row">
    <div class="col-3 text-center p-3">
        <div class="w-100">
            <img src="<?php echo base_url()?>assets/images/icone-telecharger-doc (2).webp">
        </div>
            <div class="w-100 pt-3">
                <div onclick="import_command_file()" class="btn-down-specific-command">
                    Téléchargement <span class="plus_ico">+</span>
                </div>
                <input class="d-none" type="file" name="command_file" id="command_file">
                <input type="hidden" name="id_client" id="id_client2" value="0">
            </div>
        <div class="w-100 pt-3">
            <div class="weight-file">
                Maximum 15MB
            </div>
        </div>
    </div>
    <div class="col-9">
        <div class="w-100 pt-4">
            Avec ce module, vous pouvez nous envoyer vos demandes (notes, commandes, ordonnances, documents word ou PDF...) mais aussi vos questions ou commentaires.
        </div>
        <div class="w-100 pt-4">
            <ol class="font_7" style="color:rgb(0, 0, 0); font-family:futura-lt-w01-book,sans-serif; font-size:15px; text-align:left;">
                <li>
                    <p class="font_7 m-0" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Téléchargez votre document ;</span></span></span></p>
                </li>
                <li>
                    <p class="font_7 m-0" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Précisez votre choix : enlèvement ou livraison, date et heure</span></span></span></p>
                </li>
                <li>
                    <p class="font_7 m-0" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Et/ou remplissez le champ des commentaires</span></span></span></p>
                </li>
                <li>
                    <p class="font_7 m-0" style="font-size:15px;"><span style="font-size:15px;"><span style="color:#000000;"><span style="font-family:futura-lt-w01-book,sans-serif;">Envoyez</span></span></span></p>
                </li>
            </ol>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 text-center">
        Questions ou Commentaires
    </div>
    <div class="w-100 pt-2">
        <textarea name="comment_livr_enlev" class="form-control textarea_style_paypal h-100" id="comment_livr_enlev"><?php if (isset($data_commande->comment_livr_enlev)) echo $data_commande->comment_livr_enlev; ?></textarea>
    </div>
</div>
<input type="hidden" name="id_com" value="<?php echo $infocom->IdCommercant; ?>">
<div class="row pt-4">
    <div class="col-4 pl-0 pr-3 pt-4">
        <div class="w-100">
            <select name="type_livraison" class="form-control" id="type_livraison">
                <option selected>Choisir</option>
                <?php if (isset($datacommande->is_activ_comm_enlev) AND $datacommande->is_activ_comm_enlev =="1"){ ?>
                <option value="Vente à emporter">Vente à emporter</option>
                <?php } ?>
                <?php if (isset($datacommande->is_activ_comm_livr_dom) AND $datacommande->is_activ_comm_livr_dom =="1"){ ?>
                <option value="Livraison à domicile">Livraison à domicile</option>
                <?php } ?>
            </select>
        </div>
    </div>
    <div class="col-4 pt-4 d-flex">
        <div class="w-25">Jour souhaité</div>
        <div class="w-75">
           <input id="jour_souhaite" name="jour_souhaite" class="form-control" type="date" />
        </div>
    </div>
    <div class="col-4 d-flex pt-4">
        <div class="w-25">Heure souhaitée</div>
        <div class="w-75">
            <select id="heure_souhaite" name="heure_souhaite" class="form-control"><option value="00:00">00:00</option><option value="00:30">00:30</option><option value="01:00">01:00</option><option value="01:30">01:30</option><option value="02:00">02:00</option><option value="02:30">02:30</option><option value="03:00">03:00</option><option value="03:30">03:30</option><option value="04:00">04:00</option><option value="04:30">04:30</option><option value="05:00">05:00</option><option value="05:30">05:30</option><option value="06:00">06:00</option><option value="06:30">06:30</option><option value="07:00">07:00</option><option value="07:30">07:30</option><option value="08:00">08:00</option><option value="08:30">08:30</option><option value="09:00">09:00</option><option value="09:30">09:30</option><option value="10:00">10:00</option><option value="10:30">10:30</option><option value="11:00">11:00</option><option value="11:30">11:30</option><option value="12:00">12:00</option><option value="12:30">12:30</option><option value="13:00">13:00</option><option value="13:30">13:30</option><option value="14:00">14:00</option><option selected="" value="14:30">14:30</option><option value="15:00">15:00</option><option value="15:30">15:30</option><option value="16:00">16:00</option><option value="16:30">16:30</option><option value="17:00">17:00</option><option value="17:30">17:30</option><option value="18:00">18:00</option><option value="18:30">18:30</option><option value="19:00">19:00</option><option value="19:30">19:30</option><option value="20:00">20:00</option><option value="20:30">20:30</option><option value="21:00">21:00</option><option value="21:30">21:30</option><option value="22:00">22:00</option><option value="22:30">22:30</option><option value="23:00">23:00</option><option value="23:30">23:30</option></select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-3 p-0">
        <img class="w-100 pt-3" src="<?php echo base_url('assets/images/zette2.webp')?>" />
    </div>
    <div class="col-9">
        <div class="w-100 pt-4">
            Dès réception de votre commande, notre équipe vous contactera et vous précisera les conditions de livraison ou de mise à disposition et son montant.
        </div>
        <div class="w-100 pt-3 title_blues">
            CHOIX DU REGLEMENT
        </div>
        <?php if (isset($datacommande->is_activ_card_bank_bottom) && $datacommande->is_activ_card_bank_bottom == "1"){ ?>
        <div class="w-100 d-flex pt-2">
            <div class="col-1 pl-0">
                <input id="check_bank_type" class="check_cond" type="checkbox">
            </div>
            <div class="col-11 pl-0">Par carte bancaire avec un terminal de paiement mobile</div>
        </div>
        <?php } ?>
        <?php if (isset($datacommande->is_activ_cheque_bottom) && $datacommande->is_activ_cheque_bottom == "1"){ ?>
        <div class="w-100 d-flex pt-2">
            <div class="col-1 pl-0">
                <input id="check_cheque_type" class="check_cond" type="checkbox">
            </div>
            <div class="col-11 pl-0">Par chèque bancaire préalablement rempli et signé à l'ordre de notre établissement</div>
        </div>
        <?php } ?>
        <div class="w-100 text-center pt-4">
            <div onclick="submit_custom_form()" class="w-50 mt-2 btn btn_after_input text-center">Envoyez</div>
        </div>
    </div>
</div>
<input type="hidden" value="0" id="type_paiement_custom" name="type_paiement" />
<div class="row pt-4 shadowed pb-5">
    <div class="col-3">
        <img class="w-100 pt-3" src="<?php echo base_url('assets/images/logo-carte-vivresaville_fr.webp')?>" />
    </div>
    <div class="col-6">
        <div class="w-100 pt-3 text-center title_pink">
            Attention !
        </div>
        <div class="col-12 pl-0">
            Pour pouvoir adresser vos documents, vous devez obligatoirement posséder la carte vivresaville.
        </div>
        <div class="col-12  pl-0 pt-3">
            Pour enregistrer votre carte ou la demander, cliquez sur le bouton ci-contre pour accéder au formulaire.        </div>
        </div>
    <div class="col-3 pt-3 text-center">
        <div id="scroll_uped" onclick="to_top()" class="big_round m-auto">
            <i class="icon-chevron-up"></i>
        </div>
    </div>
</div>
    </form>
<?php } ?>