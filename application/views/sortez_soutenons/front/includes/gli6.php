<?php if (isset($data_gli7) and !empty($data_gli7) and isset($title_gli7->is_activ_glissiere) and ($title_gli7->is_activ_glissiere != 0)) { ?>
    <!--<input type="hidden" id="totalgli7" value="0">-->
    <div class="row glissiere_tab marge_gli_1">
        <form class="m-0 w-100 row d-flex">
            <div class="col-4 d-flex">
                <div id="txt_title_gli7"
                     class="text-left d-flex title_gli titre_gli_up">
                    <div class="round_num">6</div>
                    <div class="title_categs">
                    <?php if (isset($title_gli7->titre_glissiere) and $title_gli7->titre_glissiere != null) {
                        echo $title_gli7->titre_glissiere;
                    } else {
                        echo "TITRE";
                    } ?>
                    </div>
                </div>
            </div>
            <div class="col-4 drop_up_down" id="activ_glissiere_activ7">
                <div class="cercle">
                    <div class="text-center up_1 d-none" id="activ_glissiere_activ7_up"></div>
                    <div class="text-center down_1" id="activ_glissiere_activ7_down"></div>
                </div>
            </div>
            <div class="col-4">
                <div class="text-right text_head" style="margin-left: -95px;">
                    Montant total TTC de cette catégorie<input id="gli7total" disabled="disabled" class="p-2 ml-1 input_glissiere" type="text" value="0€"/>
                </div>
            </div>
        </form>
    </div>

    <div class="row pr-3 pb-5 pt-4 d-none head_commande" id="activ_glissiere_activ7_content">
        <div class="row">
            <?php $i7 = 0;
            foreach ($data_gli7 as $gli7) { ?>
                <div class="col-12">
                    <div class="row">
                        <div class="col-3 w-100 img-glisss d-flex">
                            <?php if (isset($gli7->image) and is_file("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli7->image)) { ?>
                                <img class="img-fluid" style="align-self: center"
                                     src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli7->image); ?>">
                            <?php } else { ?>
                                <p class="text_head pl-4" style="align-self: center">pas d'images</p>
                            <?php } ?>
                        </div>
                        <div class=" col-7 w-100 text-center pt-2">
                            <?php if (isset($gli7->true_title) AND $gli7->true_title !=null ){ ?>
                                <div class="w-100 text-left true_title">
                                    <?php echo $gli7->true_title ?>
                                </div>
                            <?php } ?>
                            <?php if (isset($gli7->titre) and $gli7->titre != null) { ?>
                                <p class="description_categorie">
                                    <?php  echo $gli7->titre; ?>
                                </p>
                            <?php } else { ?>
                                <p class="titre_categorie pt-3 mb-0">TITRE</p>
                            <?php } ?>
                            <input class="id_gliss_7" type="hidden" value="<?php echo $gli7->id; ?>" id="product_gli7_<?php  echo $i; ?>" />
                        </div>
                        <div class="col-2 w-100 text-center pt-2">
                            <div class="row">
                                <?php if (isset($gli7->prix) and $gli7->prix != null) { ?>
                                    <input id="prix7<?php echo $i7; ?>" type="hidden" value="<?php echo $gli7->prix; ?>">
                                    <label class="col-12 text_label">Prix unitaire</label>
                                    <div class="col-12">
                                        <input class="nbre_gliss7_pr textarea_style_input p-2 text_label w-100" value="<?php echo $gli7->prix . " €"; ?>" disabled>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <div class="col-12 text_label">Quantité</div>
                                <div class="col-12 pb-2">
                                    <input value="0" onchange="change_total_price7('prix7<?php echo $i7 . '\''; ?>,'nbre7<?php echo $i7 . '\''; ?>,'product_gli7_<?php  echo $i.'\''; ?>)" id="nbre7<?php echo $i7; ?>" type="number" class="w-100 nbre_gliss7_qt textarea_style_input p-2 text_label">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ml-2 mr-1" style="border-bottom: solid 1px rgba(145, 145, 145, 1)"></div>
                </div>
                <?php $i7++;
            } ?>
        </div>
    </div>
<?php } ?>