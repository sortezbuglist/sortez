<?php if (isset($datacommande->is_activ_emporter) and $datacommande->is_activ_emporter == '1') { ?>
    <div class="row glissiere_tab marge_gli_2">
        <form class="m-0 w-100 row d-flex">
            <div class="col-4 d-flex">
                <div id="txt_title_gli1" class="text-left title_gli titre_gli_up pt-1" style="margin-right: -20px;">Vente à emporter</div>
            </div>
            <div class="col-4 drop_up_down" id="activ_glissiere_emporter">
                <div class="cercle">
                    <div class="text-center up_1 d-none" id="activ_glissiere_emporter_up"></div>
                    <div class="text-center down_1" id="activ_glissiere_emporter_down"></div>
                </div>
            </div>
            <div class="col-4 text-right">
                <label class="switch" id="value_emp_activ_value_btn">
                    <input type="checkbox" onchange="switch_emporter()" id="value_emp_activ_value">
                    <span class="slider round"></span>
                </label>
            </div>
        </form>
    </div>
    <div class="row p-2 pr-4 pt-4 pb-4 d-none" id="emporter_content">
        <div class="row mb-2">
            <div class="col-4 d-flex">
                <div id="txt_title_gli1" class="text-left pt-2 text_label">Jours et heures d’ouvertures </div>
            </div>
            <div class="col-8 pr-0">
                <div id="txt_title_gli1"
                     class="text-left <?php if (isset($datacommande->horaire_emporter_ouvert) && $datacommande->horaire_emporter_ouvert != null){ echo "p-2";}else{ echo  "p-4";}?> title_gli_val textarea_style_input text_head"><?php if (isset($datacommande->horaire_emporter_ouvert)) echo $datacommande->horaire_emporter_ouvert ?></div>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-4 d-flex">
                <div id="txt_title_gli1" class="text-left pt-2 text_label">Jours et heures d’enlèvement </div>
            </div>
            <div class="col-8 pr-0">
                <div id="txt_title_gli1"
                     class="text-left <?php if (isset($datacommande->horaire_emporter_enlev) && $datacommande->horaire_emporter_enlev != null){ echo "p-2";}else{ echo  "p-4";}?> title_gli_val textarea_style textarea_style_input text_head"><?php if (isset($datacommande->horaire_emporter_enlev)) echo $datacommande->horaire_emporter_enlev ?></div>
            </div>
        </div>
        <div class="col-12">
            <div class="row mt-4">
                <div class="col-7 pr-0">
                    <div class="row">
                        <div class="col-6 pl-0 pr-0 d-flex">
                            <div id="txt_title_gli1" class="text_label">Jour souhaité de l’enlèvement </div>
                        </div>
                        <div class="col-6 pl-4 pr-0" style="padding-left: 34px!important;">
                            <input id="jour_emporter" class="form-control textarea_style text_head" type="date">
                        </div>
                    </div>
                </div>
                <div class="col-5 pr-0">
                    <div class="row">
                        <div class="col-8 pr-0 d-flex">
                            <div class="text_label"> Heure d’enlèvement souhaitée </div>
                        </div>
                        <div class="col-4 pr-0 pl-0">
                            <select id="heure_enlevement" class="form-control textarea_style text_head">
                                <option value="09:00">9:00</option>
                                <option value="10:00">10:00</option>
                                <option value="11:00">11:00</option>
                                <option value="12:00">12:00</option>
                                <option value="13:00">13:00</option>
                                <option value="18:00">18:00</option>
                                <option value="19:00">19:00</option>
                                <option value="20:00">20:00</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        <div class="row mb-2">-->
<!--            <div class="col-4 d-flex">-->
<!--                <div id="txt_title_gli1" class="text-left pt-2 text_label">Jour souhaité de l’enlèvement </div>-->
<!--            </div>-->
<!--            <div class="col-8 pr-0">-->
<!--                <div id="txt_title_gli1" class="text-left pt-2 title_gli_val text_head">-->
<!--                    <div class="row">-->
<!--                        <div class="col-lg-4 mr-0 pr-0">-->
<!--                            <input id="jour_emporter" class="form-control textarea_style_input pr-0 text_head" type="date">-->
<!--                        </div>-->
<!--                        <div class="col-lg-5 d-flex">-->
<!--                            <div class="w-100 text_label">-->
<!--                                Heure d’enlèvement souhaitée-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-3 pl-0">-->
<!--                            <div class="w-100 text_head">-->
<!--                                <select id="heure_enlevement" class="form-control textarea_style_input">-->
<!--                                    <option value="09:00">9:00</option>-->
<!--                                    <option value="10:00">10:00</option>-->
<!--                                    <option value="11:00">11:00</option>-->
<!--                                    <option value="12:00">12:00</option>-->
<!--                                    <option value="13:00">13:00</option>-->
<!--                                    <option value="18:00">18:00</option>-->
<!--                                    <option value="19:00">19:00</option>-->
<!--                                    <option value="20:00">20:00</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="row pl-1 pt-3 pb-2">
            <div class="p-2 text_head text-justify">
                <?php if (isset($datacommande->comment_emporter_txt)) echo $datacommande->comment_emporter_txt; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="emporter_error" class="col-12 text-center">

        </div>
    </div>
<?php } ?>

<?php if (isset($datacommande->is_activ_livr) and $datacommande->is_activ_livr == '1') { ?>
    <div class="row glissiere_tab marge_gli_1">
        <form class="m-0 w-100 row d-flex">
            <div class="col-4 d-flex">
                <div id="txt_title_gli1" class="text-left title_gli titre_gli_up pt-1" style="margin-right: -70px;">Livraison à domicile</div>
            </div>
            <div class="col-4 drop_up_down" id="activ_glissiere_livraison">
                <div class="cercle">
                    <div class="text-center up_1 d-none" id="activ_glissiere_livraison_up"></div>
                    <div class="text-center down_1" id="activ_glissiere_livraison_down"></div>
                </div>
            </div>
            <div class="col-4 text-right">
                <label class="switch" id="value_livr_activ_value_btn">
                    <input type="checkbox" onchange="switch_livraison()" id="value_livr_activ_value">
                    <span class="slider round"></span>
                </label>
            </div>
        </form>
    </div>
    <div class="row p-2 pb-5 pt-4 d-none head_commande" id="livraison_content">
        <div class="row pt-2">
            <?php if (isset($datacommande->is_activ_prix_livr_grat) AND $datacommande->is_activ_prix_livr_grat == '1' ){ ?>
            <div class="col-6">
                <div class="row">
                    <div class="col-8 d-flex"><div class="text_label text-left">Livraison gratuite pour toute commande supérieure à </div></div>
                    <div class="col-4 pl-1 pr-0"><div class="value_title_sub textarea_style_input p-2 text_head"><?php if (isset($datacommande->livraison_grtuit_fee)) echo $datacommande->livraison_grtuit_fee; ?>€</div></div>
                </div>
            </div>
            <?php } ?>
            <div class="col-6">
                <div class="row">
                    <div class="col-6 d-flex <?php if (isset($datacommande->is_activ_prix_livr_grat) AND $datacommande->is_activ_prix_livr_grat == '1' ){ ?> pl-5<?php } ?>">
                        <div class="text_label">
                            délai de livraison
                        </div>
                    </div>
                    <div class="col-6 pl-0">
                        <div class="value_title_sub textarea_style_input p-2 text_head">
                            <?php if (isset($datacommande->delai_livr_debut)) echo $datacommande->delai_livr_debut; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row mt-3">
                <div class="col-4 pl-0 d-flex">
                    <div class="text_label text-justify">Communes desservies </div>
                </div>
                <div class="col-8 pl-0 pr-0">
                    <div class="textarea_style_input text_head p-3"> <?php if (isset($datacommande->commune_livr_desserv)) echo $datacommande->commune_livr_desserv; ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row mt-4">
                <div class="col-4 pl-0 d-flex">
                    <div class="text_label">Jours et heures d'ouverture </div>
                </div>
                <div class="col-8 pl-0 pr-0">
                    <div class="textarea_style_input text_head p-2"><?php if (isset($datacommande->jour_heur_livr)) echo $datacommande->jour_heur_livr; ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row mt-4">
                <div class="col-4 pl-0 d-flex">
                    <div class="text_label">Jours et heures de livraison </div>
                </div>
                <div class="col-8 pl-0 pr-0">
                    <div class="textarea_style_input text_head <?php if (isset($datacommande->horaire_livr_ouvert) && $datacommande->horaire_livr_ouvert != null){ echo "p-2" ; }else{ echo "p-3"; } ?>"><?php if (isset($datacommande->horaire_livr_ouvert)) echo $datacommande->horaire_livr_ouvert; ?></div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="row mt-4">
                <div class="col-7 pr-0">
                    <div class="row">
                        <div class="col-6 pl-0 pr-0 d-flex">
                            <div class="text_label">Date souhaitée de la livraison </div>
                        </div>
                        <div class="col-6 pl-4 pr-0">
                            <input id="jour_livraison" class="form-control textarea_style text_head" type="date">
                        </div>
                    </div>
                </div>
                <div class="col-5">
                    <div class="row">
                        <div class="col-8 pr-0 d-flex">
                            <div class="text_label">Heure de livraison souhaitée  </div>
                        </div>
                        <div class="col-4 pr-0 pl-0">
                            <select id="heure_livraison" class="form-control textarea_style text_head">
                                <option value="09:00">9:00</option>
                                <option value="10:00">10:00</option>
                                <option value="11:00">11:00</option>
                                <option value="12:00">12:00</option>
                                <option value="13:00">13:00</option>
                                <option value="18:00">18:00</option>
                                <option value="19:00">19:00</option>
                                <option value="20:00">20:00</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pl-1 pr-3 pb-2 pt-3">
            <div class="p-2 text_head text-justify">
                <?php if (isset($datacommande->livraison_comment)) echo $datacommande->livraison_comment; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center" id="error_livraison_a_dom"></div>
        </div>
    </div>
<?php } ?>
<div class="row">
    <div class="col-12 text-center" id="choose_livr_error">

    </div>
</div>
