<?php if (isset($data_gli1) && !empty($data_gli1) && isset($title_gli1->is_activ_glissiere) && $title_gli1->is_activ_glissiere != 0) { ?>
    <div class="row glissiere_tab marge_gli_1">
        <form class="m-0 w-100 row d-flex">
            <div class="col-4 d-flex">
                <div id="txt_title_gli1"
                     class="text-left title_gli d-flex titre_gli_up">
                    <div class="round_num">1</div>
                    <div class="title_categs">
                        <?php if (isset($title_gli1->titre_glissiere) and $title_gli1->titre_glissiere != null) {
                            echo $title_gli1->titre_glissiere;
                        } else {
                            echo "TITRE";
                        } ?>
                    </div>
                    </div>
            </div>
            <div class="col-4 drop_up_down" id="activ_glissiere_activ1">
                <div class="cercle">
                    <div class="text-center up_1 d-none" id="activ_glissiere_activ1_up"></div>
                    <div class="text-center down_1" id="activ_glissiere_activ1_down"></div>
                </div>
            </div>
            <div class="col-4">
                <div class="text-right text_head" style="margin-left: -95px;">
                    Montant total TTC de cette catégorie<input id="gli1total" disabled="disabled"
                                                               class="p-2 ml-1 input_glissiere" type="text" value="0€"/>
                </div>
            </div>
        </form>
    </div>

    <div class="row pr-3 pb-5 pt-4 d-none head_commande" id="activ_glissiere_activ1_content">
        <div class="row">
            <?php $i = 0; foreach ($data_gli1 as $gli1) { ?>
                <div class="col-12 p-0 pl-3">
                    <div class="row">
                        <div class="col-3 w-100 img-glisss d-flex">
                            <?php if (isset($gli1->image) and is_file("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli1->image)) { ?>
                                <img class="img-fluid" style="align-self: center"
                                     src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli1->image); ?>">
                            <?php } else { ?>
                                <p class="text_head pl-4" style="align-self: center"><?php echo "pas d'images"; ?></p>
                            <?php } ?>
                        </div>
                        <div class="col-7 w-100 text-center pt-2">
                            <?php if (isset($gli1->true_title) AND $gli1->true_title !=null ){ ?>
                                <div class="w-100 text-left true_title">
                                    <?php echo $gli1->true_title ?>
                                </div>
                            <?php } ?>
                            <?php if (isset($gli1->titre) and $gli1->titre != null) { ?>
                                <input class="id_gliss_1" value="<?php echo $gli1->id; ?>" type="hidden"
                                       id="product_gli1_<?php echo $i; ?>"/>
                                <p class="description_categorie">
                                    <?php echo $gli1->titre; ?>
                                </p>

                            <?php } else { ?>
                                <p class="titre_categorie pt-3 mb-0">TITRE</p>
                            <?php } ?>
                        </div>
                        <div class="col-2 w-100 text-center pt-2">
                            <div class="row">
                                <?php if (isset($gli1->prix) and $gli1->prix != null) { ?>
                                    <input id="prix1<?php echo $i; ?>" type="hidden" value="<?php echo $gli1->prix; ?>">
                                    <label class="col-12 text_label">Prix unitaire</label>
                                    <div class="col-12">
                                        <input value="<?php echo $gli1->prix . " €"; ?>"
                                               class="w-100 nbre_gliss1_pr textarea_style_input p-2 text_label" disabled>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <div class="col-12 text_label">Quantité</div>
                                <div class="col-12 pb-2">
                                    <input value="0"
                                           onchange="change_total_price1('prix1<?php echo $i . '\''; ?>,'nbre1<?php echo $i . '\''; ?>,'product_gli1_<?php echo $i . '\''; ?>)"
                                           id="nbre1<?php echo $i; ?>" type="number"
                                           class="w-100 nbre_gliss1_qt textarea_style_input p-2 text_label">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ml-2 mr-1" style="border-bottom: solid 1px rgba(145, 145, 145, 1)"></div>

                </div>
                <?php $i++;
            } ?>
        </div>
    </div>
<?php } ?>