<?php if (isset($data_gli4) and !empty($data_gli4) and isset($title_gli4->is_activ_glissiere) and ($title_gli4->is_activ_glissiere != 0)) { ?>
    <!--<input type="hidden" id="totalgli4" value="0">-->
    <div class="row glissiere_tab marge_gli_1">
        <form class="m-0 w-100 row d-flex">
            <div class="col-4 d-flex">
                <div id="txt_title_gli4"
                     class="text-left d-flex title_gli titre_gli_up">
                    <div class="round_num">4</div>
                    <div class="title_categs">
                    <?php if (isset($title_gli4->titre_glissiere) and $title_gli4->titre_glissiere != null) {
                        echo $title_gli4->titre_glissiere;
                    } else {
                        echo "TITRE";
                    } ?>
                    </div>
                </div>
            </div>
            <div class="col-4 drop_up_down" id="activ_glissiere_activ4">
                <div class="cercle">
                    <div class="text-center up_1 d-none" id="activ_glissiere_activ4_up"></div>
                    <div class="text-center down_1" id="activ_glissiere_activ4_down"></div>
                </div>
            </div>
            <div class="col-4">
                <div class="text-right text_head" style="margin-left: -95px;">
                    Montant total TTC de cette catégorie<input id="gli4total" disabled="disabled" class="p-2 ml-1 input_glissiere" type="text" value="0€"/>
                </div>
            </div>
        </form>
    </div>

    <div class="row pr-3 pb-5 pt-4 d-none head_commande" id="activ_glissiere_activ4_content">
        <div class="row">
            <?php $i4 = 0;
            foreach ($data_gli4 as $gli4) { ?>
                <div class="col-12">
                    <div class="row">
                        <div class=" col-3 d-flex w-100 img-glisss d-flex">
                            <?php if (isset($gli4->image) and is_file("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli4->image)) { ?>
                                <img class="img-fluid" style="align-self: center"
                                     src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/soutgli1/" . $gli4->image); ?>">
                            <?php } else { ?>
                                <p class="text_head pl-4" style="align-self: center">pas d'images</p>
                            <?php } ?>
                        </div>
                        <div class="col-7 w-100 text-center pt-2">
                            <?php if (isset($gli4->true_title) AND $gli4->true_title !=null ){ ?>
                                <div class="w-100 text-left true_title">
                                    <?php echo $gli4->true_title ?>
                                </div>
                            <?php } ?>
                            <?php if (isset($gli4->titre) and $gli4->titre != null) { ?>
                                <p class="description_categorie">
                                    <?php   echo $gli4->titre; ?>
                                </p>
                            <?php }else { ?>
                                <p class="titre_categorie pt-3 mb-0">TITRE</p>
                            <?php } ?>
                            <input class="id_gliss_4" type="hidden" value="<?php echo $gli4->id; ?>" id="product_gli4_<?php  echo $i4; ?>" />
                        </div>
                        <div class="col-2 w-100 text-center pt-2">
                            <div class="row">
                                <?php if (isset($gli4->prix) and $gli4->prix != null) { ?>
                                    <input id="prix4<?php echo $i4; ?>" type="hidden" value="<?php echo $gli4->prix; ?>">
                                    <label class="col-12 text_label">Prix unitaire</label>
                                    <div class="col-12">
                                        <input class="nbre_gliss4_pr w-100 textarea_style_input text_label p-2" value="<?php echo $gli4->prix . " €"; ?>" disabled>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="row">
                                <div class="col-12 text_label">Quantité</div>
                                <div class="col-12 pb-2">
                                    <input value="0" onchange="change_total_price4('prix4<?php echo $i4 . '\''; ?>,'nbre4<?php echo $i4 . '\''; ?>,'product_gli4_<?php  echo $i4.'\''; ?>)" id="nbre4<?php echo $i4; ?>" type="number" class="w-100 nbre_gliss4_qt textarea_style_input text_label p-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ml-2 mr-1" style="border-bottom: solid 1px rgba(145, 145, 145, 1)"></div>
                </div>
                <?php $i4++;
            } ?>
        </div>
    </div>
<?php } ?>