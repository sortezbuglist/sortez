<script type="text/javascript">
    $(document).ready(function(){
        $( "#upload_file_custom" ).submit(function(event) {
            event.preventDefault();
            $.ajax({
                url:"<?php echo site_url('soutenons/Commander/save_command_specific_file')?>",
                method:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(data)
                {
                    console.log(data)
                    if(data =="ok"){
                        alert("Votre commande a été enregistré avec success");
                    }else{
                        alert("Une erreur s'est produite !");
                    }
                }
            })
        });
        $( "#check_cheque_type" ).change(function() {
            var is_check = document.getElementById("check_cheque_type").checked;
            if(is_check == true){
                $("#type_paiement_custom").val("cheque");
                document.getElementById("check_bank_type").checked = false;
            }
        });
        $( "#check_bank_type" ).change(function() {
            var is_check = document.getElementById("check_bank_type").checked;
            if(is_check == true){
                $("#type_paiement_custom").val("Bancaire");
                document.getElementById("check_cheque_type").checked = false;
            }
        });
    })
    function switch_differe(){

        if (document.getElementById('value_differe_activ_value').checked == true){
            $("#value_differe_activ_value_hidden").val(1);

            if($('#value_emp_activ').val() == 1){
                $('#value_emp_activ').val('0');
                $('#value_emp_activ_value_btn').click();
            }
            if($('#value_livr_activ').val() == 1){
                $('#value_livr_activ').val('0');
                $('#value_livr_activ_value_btn').click();
            }
            $(".emp_contentss").removeClass("d-block");
            $(".emp_contentss").addClass("d-none");
            $(".livr_contentss").removeClass("d-block");
            $(".livr_contentss").addClass("d-none");
            $(".diff_contentss").removeClass("d-none");
            $(".diff_contentss").addClass("d-flex");
        }else{
            $("#value_differe_activ_value_hidden").val(0);
            $(".diff_contentss").removeClass("d-flex");
            $(".diff_contentss").addClass("d-none");
        }

    }
    function switch_livraison() {

        if (document.getElementById('value_livr_activ_value').checked == true){
            $('#value_livr_activ').val(1)

            if($('#value_emp_activ').val() == 1){

                $('#value_emp_activ').val('0');
                $('#value_emp_activ_value_btn').click();
            }

            if ($("#value_differe_activ_value_hidden").val() == 1 ){
                $("#switch_check_differe").click();
                $("#value_differe_activ_value_hidden").val(0);
            }
            $(".emp_contentss").removeClass("d-block");
            $(".emp_contentss").addClass("d-none");

            $(".diff_contentss").removeClass("d-flex");
            $(".diff_contentss").addClass("d-none");

            $(".livr_contentss").removeClass("d-none");
            $(".livr_contentss").addClass("d-block");
        }else{
            $('#value_livr_activ').val(0);
            $(".livr_contentss").removeClass("d-block");
            $(".livr_contentss").addClass("d-none");
        }
        if($('#value_emp_activ').val() == 1 && $('#value_livr_activ').val() == 0){
            $('#type_commande').html('Vous avez choisi la commande à emporter');
        }else if($('#value_emp_activ').val() == 0 && $('#value_livr_activ').val() == 1){
            $('#type_commande').html('Vous avez choisi la livraison à domicile');
        }else{
            $('#type_commande').html("Veuillez choisir commande à emporté ou livraison à domicile");
        }
    }
    function switch_emporter() {
        if (document.getElementById('value_emp_activ_value').checked == true){

            $('#value_emp_activ').val('1');

            if($('#value_livr_activ').val() == 1){
                $('#value_livr_activ').val('0');
                $('#value_livr_activ_value_btn').click();
            }
            if ($("#value_differe_activ_value_hidden").val() == 1 ){
                $("#value_differe_activ_value_hidden").val(0);
                $("#switch_check_differe").click();
            }
            $(".emp_contentss").removeClass("d-none");
            $(".emp_contentss").addClass("d-block");
            $(".emp_contentss").removeClass("d-none");

            $(".livr_contentss").removeClass("d-block");
            $(".livr_contentss").addClass("d-none");
            $(".diff_contentss").removeClass("d-flex");
            $(".diff_contentss").addClass("d-none");

        }else{
            $('#value_emp_activ_value').click();
            $('#value_emp_activ').val('0');
            $(".emp_contentss").removeClass("d-block");
            $(".emp_contentss").addClass("d-none");
        }
        if($('#value_emp_activ').val() == 1 && $('#value_livr_activ').val() == 0){
            $('#type_commande').html('Vous avez choisi la commande à emporter');
        }else if($('#value_emp_activ').val() == 0 && $('#value_livr_activ').val() == 1){
            $('#type_commande').html('Vous avez choisi la livraison à domicile');
        }else{
            $('#type_commande').html("Veuillez choisir commande à emporté ou livraison à domicile");
        }
    }
 function submit_custom_form() {
        if ($("#type_paiement_custom").val() == "0" || $("#type_livraison").val() == "choisir" || $("#jour_souhaite").val() == "" || $("#command_file").val() == "" ){
            alert("champ obligatoire non rempli !");
        }else{
            var type_bank = document.getElementById('check_bank_type');
            var type_cheque = document.getElementById('check_cheque_type');
            if (type_bank.cheked == false && type_cheque.checked == false){
                alert('Vous devez choisir un type de paiement');
                throw new Error("my error message");
            }
            $("#upload_file_custom").submit();
        }

 }
    function get_users_info() {
        var num = $("#number_card").val();
        var datas = "nom_card="+num;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('soutenons/Commander/check_user'); ?>",
            data: datas ,
            success: function(data_saved) {
                if (data_saved != 'ko'){
                    var parsed = JSON.parse(data_saved);
                    console.log(parsed)
                    $("#id_client").val(parsed.IdUser);
                    $("#id_client2").val(parsed.IdUser);
                    $("#nom_client").val(parsed.Nom);
                    $("#prenom_client").val(parsed.Prenom);
                    $("#code_postal_client").val(parsed.CodePostal);
                    $("#adresse_client").val(parsed.Adresse);
                    $("#ville_client").val(parsed.IdVille);
                    $("#telephone_client").val(parsed.Telephone);
                    $("#date_naissance_client").val(parsed.DateNaissance);
                    $("#mail_client").val(parsed.Email)
                    $("#qr_content").html("<img src='<?php echo base_url()?>application/resources/front/images/cards/qrcode_"+parsed.virtual_card_img+"' class='img-fluid'/>")
                    $("#demande_carte").hide();
                }else{
                    alert("Votre carte n'est pas encore valide!");
                }
            },
            error: function() {
                // alert('Erreur');
            }
        });
    }
    function to_top(){
        window.scrollTo(0, 1000);
    }
    function subscribtion() {
        var nom_client = $("#nom_client").val();
        var prenom_client = $("#prenom_client").val();
        var code_postal_client = $("#code_postal_client").val();
        var adresse_client = $("#adresse_client").val();
        var ville_client = $("#ville_client").val();
        var telephone_client = $("#telephone_client").val();
        var date_naissance_client = $("#date_naissance_client").val();
        var mail_client = $("#mail_client").val()
        var password = $("#Mot_de_passe").val()

        if ($("#nom_client").val() !=="" && $("#prenom_client").val() !=="" && $("#code_postal_client").val() !=="" && $("#adresse_client").val() !=="" && $("#ville_client").val() !=="" && $("#telephone_client").val() !== "" && $("#mail_client").val() !=""){

            var datasz = "nom_client="+nom_client+"&prenom_client="+prenom_client+"&code_postal_client="+code_postal_client+"&adresse_client="+adresse_client+"&ville_client="+ville_client+"&telephone_client="+telephone_client+"&date_naissance_client="+date_naissance_client+"&mail_client="+mail_client+"&password="+password;
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('soutenons/Commander/ajouter_compte_part'); ?>",
                data: datasz ,
                success: function(data_saved) {
                    if (data_saved == 'already'){
                        alert('Vous êtes déjà Abbonné');
                    }else if(data_saved !== ''){
                        alert('Votre compte est validé');
                    }
                    $("#id_client").val(data_saved);
                    $("#id_client2").val(data_saved);
                },
                error: function() {
                    // alert('Erreur');
                }
            });
        }else{
            $("#txt_error_add").html("Les champs sont obligatoires");
            $("#txt_error_add").css("color","red");
        }
    }

    function import_command_file(){
        var id_cli = $("#id_client").val();
        if (id_cli != "0"){
            $("#command_file").click();
        }else{
            alert("vous devez preciser votre numero de carte ou en obtenir un !");
            location.href="#number_card";
        }
    }

    function upload_file_command_custom(){

    }

    function send_command(){
        
        <?php if (isset($datacommande->is_activ_livr_paypal) AND $datacommande->is_activ_livr_paypal == "1"){ ?>
        var is_accept_paypal = document.getElementById("accept_paypal");
        if($("#value_livr_activ").val() == '1' && ( is_accept_paypal.checked == false && document.getElementById("type_norm_cheque_livr").checked == false && document.getElementById("type_norm_bank_livr").checked == false)){
            alert("Vous devez choisir une type de payement !");
            throw new Error("my error message");
        }
        <?php } ?>
        <?php if (isset($datacommande->cgv_file) AND is_file("application/resources/front/photoCommercant/cgv/".$infocom->user_ionauth_id."/".$datacommande->cgv_file)){ ?>

        var cong_gen = document.getElementById('accept_cond_gen');
        if(cong_gen != null){
            if (cong_gen.checked == false){
                alert("Vous devez accepter les conditions mentionnées ci-dessus !");
                throw new Error("my error message");
            }
        }

        <?php } ?>
        
        <?php if (isset($datacommande->is_activ_emporter) AND $datacommande->is_activ_emporter == '1' AND $datacommande->is_activ_fact_type_differe == '1' AND ($datacommande->is_activ_banc_type_differe == '0' || $datacommande->is_activ_banc_type_differe == null )){ ?>

        if($('#value_emp_activ').val() == '1' && ( document.getElementById('type_norm_cheque').checked == false && document.getElementById("type_norm_bank").checked == false && document.getElementById("type_norm_espace").checked == false)){
            alert("Vous devez choisir une type de payement !");
            throw new Error("my error message");
        }

        <?php } ?>
        
        <?php if (isset($datacommande->is_activ_emporter) AND $datacommande->is_activ_emporter == '1' AND $datacommande->is_activ_banc_type_differe == '1' AND ($datacommande->is_activ_fact_type_differe == '0' || $datacommande->is_activ_fact_type_differe == null )){ ?>

        if($('#value_emp_activ').val() == '1' && ( document.getElementById('is_activ_Cheque_differe').checked == false && document.getElementById("is_activ_Virement_differe").checked == false && document.getElementById("is_activ_carte_differe").checked == false)){
            alert("Vous devez choisir une type de payement !");
            throw new Error("my error message");
        }

        <?php } ?>
        
        <?php if (isset($datacommande->is_activ_emporter) AND $datacommande->is_activ_emporter == '1' AND $datacommande->is_activ_banc_type_differe == '1' AND $datacommande->is_activ_fact_type_differe == '1'){ ?>
        if($('#value_emp_activ').val() == '1' && ( document.getElementById('is_activ_Cheque_differe').checked == false && document.getElementById("is_activ_Virement_differe").checked == false && document.getElementById("is_activ_carte_differe").checked == false && document.getElementById('type_norm_cheque').checked == false && document.getElementById("type_norm_bank").checked == false && document.getElementById("type_norm_espace").checked == false )){
            alert("Vous devez choisir une type de payement !");
            throw new Error("my error message");
        }
        <?php } ?>
        
        <?php if (isset($datacommande->is_activ_vente_differe) AND $datacommande->is_activ_vente_differe == '1' ){ ?>
        if($('#value_differe_activ_value_hidden').val() == '1' && ( document.getElementById('is_activ_devis_differe').checked == false && document.getElementById("is_activ_pro_forma_differe").checked == false && document.getElementById("is_activ_facture_differe").checked == false)){
            alert("Vous devez choisir une type de payement !");
            throw new Error("my error message");
        }
        <?php } ?>
        
        if($('#value_livr_activ').val() == '0' && $('#value_emp_activ').val() == '0' && $("#value_differe_activ_value_hidden").val() == '0' ){
            alert("Vous devez Choisir une type de commande !");
            throw new Error("my error message");
        }
        if ($("#id_client").val() !== "0"){

            var data_saved = $("#id_client").val();
            /// gli1
            var alll = [];
            var nbre_gli1 = document.getElementsByClassName('nbre_gliss1_qt');
            var id_gli1 = document.getElementsByClassName('id_gliss_1');

            var nbre_gli2 = document.getElementsByClassName('nbre_gliss2_qt');
            var id_gli2 = document.getElementsByClassName('id_gliss_2');

            var nbre_gli3 = document.getElementsByClassName('nbre_gliss3_qt');
            var id_gli3 = document.getElementsByClassName('id_gliss_3');

            var nbre_gli4 = document.getElementsByClassName('nbre_gliss4_qt');
            var id_gli4 = document.getElementsByClassName('id_gliss_4');

            var nbre_gli6 = document.getElementsByClassName('nbre_gliss6_qt');
            var id_gli6 = document.getElementsByClassName('id_gliss_6');

            var nbre_gli7 = document.getElementsByClassName('nbre_gliss7_qt');
            var id_gli7 = document.getElementsByClassName('id_gliss_7');


            if (nbre_gli1.length !== 0){
                for (i=0;i < nbre_gli1.length;i++){
                    if (nbre_gli1[i].value > 0){
                        var to_append  = {id_produit:id_gli1[i].value, nbre:nbre_gli1[i].value,id_glissiere:1};
                        alll.push(to_append);
                    }
                }
            }

            /// gli2
            if (nbre_gli2.length !== 0){
                for (i=0;i < nbre_gli2.length;i++){
                    if (nbre_gli2[i].value > 0){
                        var to_append2  = {id_produit:id_gli2[i].value, nbre:nbre_gli2[i].value,id_glissiere:2};
                        alll.push(to_append2);
                    }
                }
                console.log(alll);
            }

            /// gli3
            if (nbre_gli3.length !== 0){
                for (i=0;i < nbre_gli3.length;i++){
                    if (nbre_gli3[i].value > 0){
                        var to_append3  = {id_produit:id_gli3[i].value, nbre:nbre_gli3[i].value,id_glissiere:3};
                        alll.push(to_append3);
                    }
                }
                console.log(alll);
            }

            /// gli4
            if (nbre_gli4.length !== 0){
                for (i=0;i < nbre_gli4.length;i++){
                    console.log('------------------')
                    console.log(nbre_gli4.length)
                    console.log(nbre_gli4[i])
                    console.log('------------------')
                    if (nbre_gli4[i].value > 0){
                        var to_append4  = {id_produit:id_gli4[i].value, nbre:nbre_gli4[i].value,id_glissiere:4};
                        alll.push(to_append4);
                    }
                }
                console.log(alll);
            }

            /// gli6
            if (nbre_gli6.length !== 0){
                for (i=0;i < nbre_gli6.length;i++){
                    if (nbre_gli6[i].value > 0){
                        var to_append6  = {id_produit:id_gli6[i].value, nbre:nbre_gli6[i].value,id_glissiere:6};
                        alll.push(to_append6);
                    }
                }
                console.log(alll);
            }

            /// gli7
            if (nbre_gli7.length !== 0){
                for (i=0;i < nbre_gli7.length;i++){
                    if (nbre_gli7[i].value > 0){
                        var to_append7  = {id_produit:id_gli7[i].value, nbre:nbre_gli7[i].value,id_glissiere:7};
                        alll.push(to_append7);
                    }
                }
                console.log(alll);
            }

            if($("#value_livr_activ").val() ==="1" ){

                var type = "Livraison à Domicile";
                var jour_enlev = $("#jour_livraison").val();
                var heure_emporter = $("#heure_livraison").val();

                if($('#jour_livraison').val() == ""){
                    $("#error_livraison_a_dom").html("Veuillez preciser la date de livraison souhaité");
                    alert("Veuillez preciser la date de livraison souhaité");
                    $("#error_livraison_a_dom").css("color","red");
                    location.href = "#error_livraison_a_dom";
                    throw new Error("my error message");
                }

            }else if($("#value_emp_activ").val() ==="1"){
                var type = "Enlevement Chez le vendeur";
                var jour_enlev = $("#jour_emporter").val();
                var heure_emporter = $("#heure_enlevement").val();

                if($('#jour_emporter').val() == ""){
                    $("#emporter_error").html("Veuillez preciser la date de visite");
                    alert("Veuillez preciser la date de visite");
                    $("#emporter_error").css("color","red");
                    location.href = "#emporter_error";
                    throw new Error("my error message");
                }

            }else if($("#value_differe_activ_value_hidden").val() == '1'){
                var type = "VENTE EN LIGNE - PAIEMENT DIFFERE";
                var jour_enlev = "VENTE EN LIGNE - PAIEMENT DIFFERE";
                var heure_emporter = "VENTE EN LIGNE - PAIEMENT DIFFERE";
            }

            var type_payement = $("#type_paiment_cli").val();
            if (!type_payement){
                alert("Vous devez Choisir une type de paiement")
                throw new Error("my error message");
            }

            var to_prevent = "&type="+type+"&jour_enlev="+jour_enlev+"&heure_emporter="+heure_emporter+"&type_payement="+type_payement;
            if(alll.length !== 0){
                let datas = "allprod="+JSON.stringify(alll)+"&idCom="+"<?php echo $infocom->IdCommercant; ?>"+"&idclient="+data_saved+"&comment="+$("#comment_comm").val()+to_prevent;
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('soutenons/Commander/valid_command'); ?>",
                    data: datas,
                    success: function(data) {
                        console.log(data);
                        if (data == "ok"){
                            $("#commande_error").html("");
                            $("#error_livraison_a_dom").html("");
                            $("#emporter_error").html("");
                            $("#choose_livr_error").html("");
                            $("#txt_error_add").html("");
                            alert('Votre commande a été envoyé avec succèss !!!');
                        }else if(data == "sokok"){
                            // alert('Votre mots de passe par défaut à été envoyé par mail,veuillez confirmer votre compte');
                            alert('Votre commande a été envoué avec succèss  !!!');
                        }
                    },
                    error: function() {
                        // alert('Erreur');
                    }
                });
            }else{
                $("#commande_error").html("Aucune Produit sélectionné");
                alert("Aucune Produit sélectionné");
                $("#commande_error").css("color","red");
            }
        }else{
            $("#txt_error_add").html("Veuillez vous souscrire d'abord, C'est gratuit !!");
            alert("Veuillez vous souscrire d'abord, C'est gratuit !!");
            $("#txt_error_add").css("color","red");
            location.href = "#txt_error_add";
        }

    }

    function change_total_price1(idprix, idnbre) {
        // var get = $("#" + get_other).val();
        // alert(get);
        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss1_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss1_qt')[i].value;
            get = get + (parseInt(class_name_prix[i].value.replace(' €' , '')) * qtt);
        }
        // alert(get);
        // var prix = $("#" + idprix).val();
        // var nbre = $("#" + idnbre).val();
        // //var totalold = $("#totalgli1").val();
        // var subtotal = parseInt(prix) * parseInt(nbre);
        // var total = parseInt(subtotal);
        $("#gli1total").val(get+"€");
        $("#totalgli1").val(get);
        $("#subs_bottomgli1").val(parseInt(get)+"€");
        // alert(parseInt($("#subs_bottomgli5").val()));
        $('#all_totalised').val(parseInt($("#totalgli1").val())+parseInt($("#totalgli2").val())+parseInt($("#totalgli3").val())+parseInt($("#totalgli4").val())+parseInt($("#totalgli6").val())+parseInt($("#totalgli7").val())+"€");

        if( parseInt($("#all_totalised").val()) <= parseInt(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolez ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficiez !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficiez !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseInt($("#all_totalised").val()) - parseInt(<?php echo $datacommande->price_to_win; ?>));        }
    }

    function change_total_price2(idprix, idnbre) {
        // var prix = $("#" + idprix).val();
        // var nbre = $("#" + idnbre).val();
        // // var totalold = $("#totalgli2").val();
        // var subtotal = parseInt(prix) * parseInt(nbre);
        // var total =  parseInt(subtotal);
        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss2_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss2_qt')[i].value;
            get = get + (parseInt(class_name_prix[i].value.replace(' €' , '')) * qtt);
        }
        $("#gli2total").val(get+"€");
        $("#totalgli2").val(get);
        $("#subs_bottomgli2").val(parseInt(get)+"€");
        $('#all_totalised').val(parseInt($("#totalgli1").val())+parseInt($("#totalgli2").val())+parseInt($("#totalgli3").val())+parseInt($("#totalgli4").val())+parseInt($("#totalgli6").val())+parseInt($("#totalgli7").val())+"€")
        if( parseInt($("#all_totalised").val()) <= parseInt(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolez ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficiez !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficiez !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseInt($("#all_totalised").val()) - parseInt(<?php echo $datacommande->price_to_win; ?>));        }
    }

    function change_total_price3(idprix, idnbre) {
        // var prix = $("#" + idprix).val();
        // var nbre = $("#" + idnbre).val();
        // // var totalold = $("#totalgli3").val();
        // var subtotal = parseInt(prix) * parseInt(nbre);
        // var total = parseInt(subtotal);
        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss3_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss3_qt')[i].value;
            get = get + (parseInt(class_name_prix[i].value.replace(' €' , '')) * qtt);
        }
        $("#gli3total").val(get+"€");
        $("#totalgli3").val(get);
        $("#subs_bottomgli3").val(parseInt(get)+"€");
        $('#all_totalised').val(parseInt($("#totalgli1").val())+parseInt($("#totalgli2").val())+parseInt($("#totalgli3").val())+parseInt($("#totalgli4").val())+parseInt($("#totalgli6").val())+parseInt($("#totalgli7").val())+"€")
        if( parseInt($("#all_totalised").val()) <= parseInt(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolez ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficiez !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficiez !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseInt($("#all_totalised").val()) - parseInt(<?php echo $datacommande->price_to_win; ?>));        }
    }

    function change_total_price4(idprix, idnbre) {
        // var prix = $("#" + idprix).val();
        // var nbre = $("#" + idnbre).val();
        // // var totalold = $("#totalgli4").val();
        // var subtotal = parseInt(prix) * parseInt(nbre);
        // var total = parseInt(subtotal);
        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss4_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss4_qt')[i].value;
            get = get + (parseInt(class_name_prix[i].value.replace(' €' , '')) * qtt);
        }
        $("#gli4total").val(get+"€");
        $("#totalgli4").val(get);
        $("#subs_bottomgli4").val(parseInt(get)+"€");
        $('#all_totalised').val(parseInt($("#totalgli1").val())+parseInt($("#totalgli2").val())+parseInt($("#totalgli3").val())+parseInt($("#totalgli4").val())+parseInt($("#totalgli6").val())+parseInt($("#totalgli7").val())+"€")
        if( parseInt($("#all_totalised").val()) <= parseInt(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolez ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficiez !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficiez !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseInt($("#all_totalised").val()) - parseInt(<?php echo $datacommande->price_to_win; ?>));        }
    }

    function change_total_price6(idprix, idnbre) {
        // var prix = $("#" + idprix).val();
        // var nbre = $("#" + idnbre).val();
        // // var totalold = $("#totalgli6").val();
        // var subtotal = parseInt(prix) * parseInt(nbre);
        // var total = parseInt(subtotal);
        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss6_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss6_qt')[i].value;
            get = get + (parseInt(class_name_prix[i].value.replace(' €' , '')) * qtt);
        }
        $("#gli6total").val(get+"€");
        $("#totalgli6").val(get);
        $("#subs_bottomgli6").val(parseInt(get)+"€");
        $('#all_totalised').val(parseInt($("#totalgli1").val())+parseInt($("#totalgli2").val())+parseInt($("#totalgli3").val())+parseInt($("#totalgli4").val())+parseInt($("#totalgli6").val())+parseInt($("#totalgli7").val())+"€")
        if( parseInt($("#all_totalised").val()) <= parseInt(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolez ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficiez !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficiez !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseInt($("#all_totalised").val()) - parseInt(<?php echo $datacommande->price_to_win; ?>));        }
    }

    function change_total_price7(idprix, idnbre) {
        // var prix = $("#" + idprix).val();
        // var nbre = $("#" + idnbre).val();
        // var totalold = $("#totalgli7").val();
        // var subtotal = parseInt(prix) * parseInt(nbre);
        // var total = parseInt(subtotal);
        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss7_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss7_qt')[i].value;
            get = get + (parseInt(class_name_prix[i].value.replace(' €' , '')) * qtt);
        }
        $("#gli7total").val(get+"€");
        $("#totalgli7").val(get);
        $("#subs_bottomgli7").val(parseInt(get)+"€");
        $('#all_totalised').val(parseInt($("#totalgli1").val())+parseInt($("#totalgli2").val())+parseInt($("#totalgli3").val())+parseInt($("#totalgli4").val())+parseInt($("#totalgli6").val())+parseInt($("#totalgli7").val())+"€")
        if( parseInt($("#all_totalised").val()) <= parseInt(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolez ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficiez !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficiez !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseInt($("#all_totalised").val()) - parseInt(<?php echo $datacommande->price_to_win; ?>));
        }
    }
</script>
<script type="text/javascript">
    $("#activ_glissiere_emporter").click(function (){
        if($("#activ_glissiere_emporter_value").val() == 0){
            $("#emporter_content").removeClass('d-none');
            $("#emporter_content").addClass('d-block');
            $("#activ_glissiere_emporter_up").removeClass('d-none');
            $("#activ_glissiere_emporter_up").addClass('d-block');
            $("#activ_glissiere_emporter_down").removeClass('d-block');
            $("#activ_glissiere_emporter_down").addClass('d-none');
            $("#activ_glissiere_emporter_value").val('1');
        }
        else{
            $("#emporter_content").removeClass('d-block');
            $("#emporter_content").addClass('d-none');
            $("#activ_glissiere_emporter_down").removeClass('d-none');
            $("#activ_glissiere_emporter_down").addClass('d-block');
            $("#activ_glissiere_emporter_up").removeClass('d-block');
            $("#activ_glissiere_emporter_up").addClass('d-none');
            $("#activ_glissiere_emporter_value").val('0');
        }
    })
    $("#type_norm_cheque").click(function (){
        document.getElementById('type_norm_bank').checked = false;
        document.getElementById('type_norm_espace').checked = false;
        $("#type_paiment_cli").val("Chèque");
    })
    $("#type_norm_bank").click(function (){
        document.getElementById('type_norm_cheque').checked = false;
        document.getElementById('type_norm_espace').checked = false;
        $("#type_paiment_cli").val("Carte bancaire");
    })
    $("#type_norm_espace").click(function (){
        document.getElementById('type_norm_cheque').checked = false;
        document.getElementById('type_norm_bank').checked = false;
        $("#type_paiment_cli").val("Espèces");
    })
    $("#type_norm_cheque_livr").click(function (){
        document.getElementById('type_norm_bank_livr').checked = false;
        $("#type_paiment_cli").val("Chèque dûment rempli et signé");
    })
    $("#type_norm_bank_livr").click(function (){
        document.getElementById('type_norm_cheque_livr').checked = false;
        $("#type_paiment_cli").val("Carte bancaire avec un terminal de paiement");
    })


    $("#is_activ_devis_differe").click(function (){
        document.getElementById('is_activ_pro_forma_differe').checked = false;
        document.getElementById('is_activ_facture_differe').checked = false;
        document.getElementById('is_activ_Cheque_differe').checked = false;
        document.getElementById('is_activ_Virement_differe').checked = false;
        document.getElementById('is_activ_carte_differe').checked = false;

        $("#type_paiment_cli").val("Devis");
    })

    $("#is_activ_pro_forma_differe").click(function (){
        document.getElementById('is_activ_devis_differe').checked = false;
        document.getElementById('is_activ_facture_differe').checked = false;
        document.getElementById('is_activ_Cheque_differe').checked = false;
        document.getElementById('is_activ_Virement_differe').checked = false;
        document.getElementById('is_activ_carte_differe').checked = false;

        $("#type_paiment_cli").val("Facture pro forma");
    })

    $("#is_activ_facture_differe").click(function (){
        document.getElementById('is_activ_devis_differe').checked = false;
        document.getElementById('is_activ_pro_forma_differe').checked = false;
        document.getElementById('is_activ_Cheque_differe').checked = false;
        document.getElementById('is_activ_Virement_differe').checked = false;
        document.getElementById('is_activ_carte_differe').checked = false;

        $("#type_paiment_cli").val("Facture");
    })

    $("#is_activ_Cheque_differe").click(function (){
        document.getElementById('is_activ_devis_differe').checked = false;
        document.getElementById('is_activ_pro_forma_differe').checked = false;
        document.getElementById('is_activ_facture_differe').checked = false;
        document.getElementById('is_activ_Virement_differe').checked = false;
        document.getElementById('is_activ_carte_differe').checked = false;

        $("#type_paiment_cli").val("Chèque");
    })

    $("#is_activ_Virement_differe").click(function (){
        document.getElementById('is_activ_devis_differe').checked = false;
        document.getElementById('is_activ_pro_forma_differe').checked = false;
        document.getElementById('is_activ_facture_differe').checked = false;
        document.getElementById('is_activ_Cheque_differe').checked = false;
        document.getElementById('is_activ_carte_differe').checked = false;

        $("#type_paiment_cli").val("Virement");
    })

    $("#is_activ_carte_differe").click(function (){
        document.getElementById('is_activ_devis_differe').checked = false;
        document.getElementById('is_activ_pro_forma_differe').checked = false;
        document.getElementById('is_activ_facture_differe').checked = false;
        document.getElementById('is_activ_Cheque_differe').checked = false;
        document.getElementById('is_activ_Virement_differe').checked = false;

        $("#type_paiment_cli").val("Carte bancaire");
    })

    $("#activ_glissiere_differe").click(function (){
        if($("#activ_glissiere_differe_value").val() == 0){
            $(".activ_glissiere_differe_child").removeClass('d-none');
            $(".activ_glissiere_differe_child").addClass('d-flex');
            $("#activ_glissiere_differe_up").removeClass('d-none');
            $("#activ_glissiere_differe_up").addClass('d-block');
            $("#activ_glissiere_differe_down").removeClass('d-block');
            $("#activ_glissiere_differe_down").addClass('d-none');
            $("#activ_glissiere_differe_value").val('1');
        }
        else{
            $(".activ_glissiere_differe_child").removeClass('d-flex');
            $(".activ_glissiere_differe_child").addClass('d-none');
            $("#activ_glissiere_differe_down").removeClass('d-none');
            $("#activ_glissiere_differe_down").addClass('d-block');
            $("#activ_glissiere_differe_up").removeClass('d-block');
            $("#activ_glissiere_differe_up").addClass('d-none');
            $("#activ_glissiere_differe_value").val('0');
        }
    })
    $("#activ_glissiere_livraison").click(function (){
        if($("#activ_glissiere_livraison_value").val() == 0){
            $("#livraison_content").removeClass('d-none');
            $("#livraison_content").addClass('d-block');
            $("#activ_glissiere_livraison_up").removeClass('d-none');
            $("#activ_glissiere_livraison_up").addClass('d-block');
            $("#activ_glissiere_livraison_down").removeClass('d-block');
            $("#activ_glissiere_livraison_down").addClass('d-none');
            $("#activ_glissiere_livraison_value").val('1');
        }
        else{
            $("#livraison_content").removeClass('d-block');
            $("#livraison_content").addClass('d-none');
            $("#activ_glissiere_livraison_down").removeClass('d-none');
            $("#activ_glissiere_livraison_down").addClass('d-block');
            $("#activ_glissiere_livraison_up").removeClass('d-block');
            $("#activ_glissiere_livraison_up").addClass('d-none');
            $("#activ_glissiere_livraison_value").val('0');
        }
    })
    $("#activ_glissiere_activ1").click(function (){
        if($("#activ_glissiere_activ1_value").val() == 0){
            $("#activ_glissiere_activ1_content").removeClass('d-none');
            $("#activ_glissiere_activ1_content").addClass('d-block');
            $("#activ_glissiere_activ1_up").removeClass('d-none');
            $("#activ_glissiere_activ1_up").addClass('d-block');
            $("#activ_glissiere_activ1_down").removeClass('d-block');
            $("#activ_glissiere_activ1_down").addClass('d-none');
            $("#activ_glissiere_activ1_value").val('1');
        }
        else{
            $("#activ_glissiere_activ1_content").removeClass('d-block');
            $("#activ_glissiere_activ1_content").addClass('d-none');
            $("#activ_glissiere_activ1_down").removeClass('d-none');
            $("#activ_glissiere_activ1_down").addClass('d-block');
            $("#activ_glissiere_activ1_up").removeClass('d-block');
            $("#activ_glissiere_activ1_up").addClass('d-none');
            $("#activ_glissiere_activ1_value").val('0');
        }
    })
    $("#activ_glissiere_activ2").click(function (){
        if($("#activ_glissiere_activ2_value").val() == 0){
            $("#activ_glissiere_activ2_content").removeClass('d-none');
            $("#activ_glissiere_activ2_content").addClass('d-block');
            $("#activ_glissiere_activ2_up").removeClass('d-none');
            $("#activ_glissiere_activ2_up").addClass('d-block');
            $("#activ_glissiere_activ2_down").removeClass('d-block');
            $("#activ_glissiere_activ2_down").addClass('d-none');
            $("#activ_glissiere_activ2_value").val('1');
        }
        else{
            $("#activ_glissiere_activ2_content").removeClass('d-block');
            $("#activ_glissiere_activ2_content").addClass('d-none');
            $("#activ_glissiere_activ2_down").removeClass('d-none');
            $("#activ_glissiere_activ2_down").addClass('d-block');
            $("#activ_glissiere_activ2_up").removeClass('d-block');
            $("#activ_glissiere_activ2_up").addClass('d-none');
            $("#activ_glissiere_activ2_value").val('0');
        }
    })

    $("#activ_glissiere_activ3").click(function (){
        if($("#activ_glissiere_activ3_value").val() == 0){
            $("#activ_glissiere_activ3_content").removeClass('d-none');
            $("#activ_glissiere_activ3_content").addClass('d-block');
            $("#activ_glissiere_activ3_up").removeClass('d-none');
            $("#activ_glissiere_activ3_up").addClass('d-block');
            $("#activ_glissiere_activ3_down").removeClass('d-block');
            $("#activ_glissiere_activ3_down").addClass('d-none');
            $("#activ_glissiere_activ3_value").val('1');
        }
        else{
            $("#activ_glissiere_activ3_content").removeClass('d-block');
            $("#activ_glissiere_activ3_content").addClass('d-none');
            $("#activ_glissiere_activ3_down").removeClass('d-none');
            $("#activ_glissiere_activ3_down").addClass('d-block');
            $("#activ_glissiere_activ3_up").removeClass('d-block');
            $("#activ_glissiere_activ3_up").addClass('d-none');
            $("#activ_glissiere_activ3_value").val('0');
        }
    })

    $("#activ_glissiere_activ4").click(function (){
        if($("#activ_glissiere_activ4_value").val() == 0){
            $("#activ_glissiere_activ4_content").removeClass('d-none');
            $("#activ_glissiere_activ4_content").addClass('d-block');
            $("#activ_glissiere_activ4_up").removeClass('d-none');
            $("#activ_glissiere_activ4_up").addClass('d-block');
            $("#activ_glissiere_activ4_down").removeClass('d-block');
            $("#activ_glissiere_activ4_down").addClass('d-none');
            $("#activ_glissiere_activ4_value").val('1');
        }
        else{
            $("#activ_glissiere_activ4_content").removeClass('d-block');
            $("#activ_glissiere_activ4_content").addClass('d-none');
            $("#activ_glissiere_activ4_down").removeClass('d-none');
            $("#activ_glissiere_activ4_down").addClass('d-block');
            $("#activ_glissiere_activ4_up").removeClass('d-block');
            $("#activ_glissiere_activ4_up").addClass('d-none');
            $("#activ_glissiere_activ4_value").val('0');
        }
    })

    $("#activ_glissiere_activ7").click(function (){
        if($("#activ_glissiere_activ7_value").val() == 0){
            $("#activ_glissiere_activ7_content").removeClass('d-none');
            $("#activ_glissiere_activ7_content").addClass('d-block');
            $("#activ_glissiere_activ7_up").removeClass('d-none');
            $("#activ_glissiere_activ7_up").addClass('d-block');
            $("#activ_glissiere_activ7_down").removeClass('d-block');
            $("#activ_glissiere_activ7_down").addClass('d-none');
            $("#activ_glissiere_activ7_value").val('1');
        }
        else{
            $("#activ_glissiere_activ7_content").removeClass('d-block');
            $("#activ_glissiere_activ7_content").addClass('d-none');
            $("#activ_glissiere_activ7_down").removeClass('d-none');
            $("#activ_glissiere_activ7_down").addClass('d-block');
            $("#activ_glissiere_activ7_up").removeClass('d-block');
            $("#activ_glissiere_activ7_up").addClass('d-none');
            $("#activ_glissiere_activ7_value").val('0');
        }
    })
    $("#activ_glissiere_activ6").click(function (){
        if($("#activ_glissiere_activ6_value").val() == 0){
            $("#activ_glissiere_activ6_content").removeClass('d-none');
            $("#activ_glissiere_activ6_content").addClass('d-block');
            $("#activ_glissiere_activ6_up").removeClass('d-none');
            $("#activ_glissiere_activ6_up").addClass('d-block');
            $("#activ_glissiere_activ6_down").removeClass('d-block');
            $("#activ_glissiere_activ6_down").addClass('d-none');
            $("#activ_glissiere_activ6_value").val('1');
        }
        else{
            $("#activ_glissiere_activ6_content").removeClass('d-block');
            $("#activ_glissiere_activ6_content").addClass('d-none');
            $("#activ_glissiere_activ6_down").removeClass('d-none');
            $("#activ_glissiere_activ6_down").addClass('d-block');
            $("#activ_glissiere_activ6_up").removeClass('d-block');
            $("#activ_glissiere_activ6_up").addClass('d-none');
            $("#activ_glissiere_activ6_value").val('0');
        }
    })
    function check_show_categ_partner(){

    }
</script>