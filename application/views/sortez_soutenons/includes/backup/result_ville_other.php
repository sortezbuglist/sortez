<style type="text/css">
    body {
        background: none !important;
    }
</style>

<?php $data["zTitle"] = 'Vivresaville'; ?>
<?php $this->load->view("sortez_vsv/includes/main_header", $data); ?>
<div class="row m-0">
    <div class="alert alert-success" role="alert">
        <strong>Succès !</strong> Vous avez bien enregistré l'élément <a href="javascript:void(0);" class="alert-link"><?php if (isset($ville_other['id_vsv'])) echo $ville_other['name']; ?></a>.
    </div>
</div>
<div class="row m-0">
    <a href="<?php echo base_url();?>front/page/edit_autre_ville/<?php if (isset($ville_other['id_vsv'])) echo $ville_other['id_vsv']; ?>/0" id="vsv_add_territoire_new" class="btn btn-primary">Ajouter un térritoire additionnel</a>
</div>
<?php $this->load->view("sortez_vsv/includes/main_footer", $data); ?>