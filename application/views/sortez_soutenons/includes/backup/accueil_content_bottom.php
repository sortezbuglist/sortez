<div class="container accueil_content_bottom pt-3" style="background-color: #ffffff;">
    <div class="row">
        <div class="col-12">
            <?php
            if (isset($oville->home_photo_annuaire) && $oville->home_photo_annuaire != "0" && $oville->home_photo_annuaire != "" && file_exists("application/resources/front/images/vivresaville/" . $oville->home_photo_annuaire)) { ?>
                <img
                        src="<?php echo base_url(); ?>application/resources/front/images/vivresaville/<?php echo $oville->home_photo_annuaire; ?>"
                        style="width: 100%" class="mt-0 border-0"/>
            <?php } ?>
        </div>
    </div>
    <div class="row justify-content-center pt-5 pb-5">
        <div class="col-12 text-center">
            <h2 style="font-family: Futura_regular;">PROFESSIONNELS & ASSOCIATIONS<br/>RÉFÉRENCEZ GRATUITEMENT VOTRE ÉTABLISSEMENT !...</h2>
        </div>
        <div class="col text-justify vsv_accueil_text_bottom p-5">
            <?php if (isset($oville->home_description) && $oville->home_description != "") echo $oville->home_description; ?>
        </div>
        <div class="col-12 pb-3 pt-3">
            <div class="col-12 pr-0 text-center">
                <a href="<?php echo site_url('vivresaville/page/index/infos-consommateurs.html'); ?>" class="vsv_accueil_btn_black  ml-auto mr-auto">Plus d'infos...</a>
            </div>
        </div>
    </div>
</div>