<?php
$this_session = &get_instance();
$this_session->load->library('session');
$session_zMotCle_verification = $this_session->session->userdata('zMotCle_x');
?>

<div class="row justify-content-center">
    <div class="col-10">
        <div class="input-group">
            <input type="text" name="inputString_zMotCle" id="inputString_zMotCle"
                   value="<?php if (isset($session_zMotCle_verification) && $session_zMotCle_verification != " ") echo $session_zMotCle_verification; ?>"
                   class="form-control" placeholder="Mot clé à rechercher" aria-describedby="basic-addon2"/>
            <button name="zMotCle_to_check_btn" id="inputString_zMotCle_submit" class="input-group-addon">Rechercher
            </button>
        </div>
    </div>
</div>

