<?php
$thisss =& get_instance();
$thisss->load->library('session');
$localdata_IdVille = $thisss->session->userdata('localdata_IdVille');
$localdata_IdDepartement = $thisss->session->userdata('iDepartementId_x');
$iVilleId_x = $thisss->session->userdata('iVilleId_x');
?>

<div class="container main_nav_bar_menu d-lg-none d-xl-none">
    <div class="row">
        <nav class="container navbar navbar-toggleable-md navbar-inverse bg-inverse mb-0 w-100 p-0">
            <button class="navbar-toggler navbar-toggler-left collapsed" type="button" data-toggle="collapse"
                    data-target="#navbarsDepartMenuDefault" aria-controls="navbarsDepartMenuDefault" aria-expanded="false"
                    id="navbar_button_depart_link"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"><img alt="" src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/burgerIcon.png" class="img-fluid"></span>
            </button>
            <a class="navbar-brand text-right" id="navbar_brand_depart_link" href="javascript:void(0);"  onclick="javascript:click_depart_menu_btn();">
                <?php
                if(isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != 0 && $localdata_IdDepartement != "0") {
                    if(isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0")
                    {
                        foreach($toDepartement as $oDepartement){
                            if ($oDepartement->departement_code == $localdata_IdDepartement)
                                echo $oDepartement->departement_nom." (".$oDepartement->nbCommercant.")".'</a>';
                        }
                    }
                }
                else echo 'Alpes-Maritimes & Monaco';
                ?>
            </a>
            <div class="navbar-collapse collapse" id="navbarsDepartMenuDefault" aria-expanded="false" style="">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="javascript:void(0);" onclick="javascript:pvc_select_departement_mobile(0);">
                            Tous les départements
                        </a>
                    </li>
                    <?php
                    if(isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0")
                    {
                        foreach($toDepartement as $oDepartement){ ?>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0);" onclick="javascript:pvc_select_departement_mobile(<?php echo $oDepartement->departement_id ; ?>);">
                                    <?php echo $oDepartement->departement_nom." (".$oDepartement->nbCommercant.")" ; ?>
                                </a>
                            </li>
                        <?php } ?>
                    <?php }
                    ?>
                </ul>
            </div>
        </nav>
    </div>
</div>
<script type="text/javascript">
    function pvc_select_departement_mobile(departement_id) {
        $("#inputStringDepartementHidden_bonplans").val(departement_id);
        $("#inputStringDepartementHidden_partenaires").val(departement_id);
        $("#inputStringDepartementHidden_annnonces").val(departement_id);

        $("#inputStringVilleHidden_bonplans").val(0);
        $("#inputStringVilleHidden_partenaires").val(0);
        $("#inputStringVilleHidden_annonces").val(0);

        $("#frmRechercheBonplan").submit();
        $("#frmRecherchePartenaire").submit();
        $("#frmRechercheAnnonce").submit();
    }
    function click_depart_menu_btn() {
        $("#navbar_button_depart_link").click();
    }
</script>
