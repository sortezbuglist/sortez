<?php

$thiss = &get_instance();

$thiss->load->library('session');

$thiss->load->model('vsv_ville_other');

$localdata_IdVille = $thiss->session->userdata('localdata_IdVille');

$localdata_IdVille_parent = $thiss->session->userdata('localdata_IdVille_parent');

$localdata_IdVille_all = $thiss->session->userdata('localdata_IdVille_all');

$main_width_device = $thiss->session->userdata('main_width_device');

$main_mobile_screen = false;

$localdata_IdDepartement = $thiss->session->userdata('iDepartementId_x');

$localdata_IdCategory = $thiss->session->userdata('iCategorieId_x');

$localdata_iSubCategorieId = $thiss->session->userdata('iSubCategorieId_x');

$localdata_zMotCle = $thiss->session->userdata('zMotCle_x');



if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device) <= 991) {

    $main_mobile_screen = true;

} else {

    $main_mobile_screen = false;

}

$thiss->load->model("vivresaville_villes");

$logo_to_show_vsv = base_url() . "assets/ville-test/wpimages/logo_ville_test.png";

if (isset($localdata_IdVille_parent) && $localdata_IdVille_parent != "" && $localdata_IdVille_parent != false) {

    $vsv_id_vivresaville = $thiss->vivresaville_villes->getByIdVille($localdata_IdVille_parent);

    if (isset($vsv_id_vivresaville) && isset($vsv_id_vivresaville->id)) {

        $vsv_object = $thiss->vivresaville_villes->getById($vsv_id_vivresaville->id);

        $vsv_other_ville = $thiss->vsv_ville_other->getByIdVsv($vsv_id_vivresaville->id);

    }

    if (isset($vsv_object) && isset($vsv_object->logo) && file_exists("application/resources/front/images/vivresaville/" . $vsv_object->logo)) {

        $logo_to_show_vsv = base_url() . "application/resources/front/images/vivresaville/" . $vsv_object->logo;

    }

}

?>



<div class="mt-lg-5 mb-lg-5 main_banner_container_soutenons">

    <div class="" style="background-color: white!important;padding-top: 10px;padding-bottom: 25px">

        <div class="col-lg-12">

            <p class="gros_titre">

                <?php

                if ($pagecategory == "annuaire") {

                    echo "Recherches par communes & catégories";

                } else if ($pagecategory == "annonce") {

                    echo "Recherches par annonces";

                } else {

                    echo "Recherches par Deals & fidélité";

                }

                ?>

            </p>

        </div>

        <div class="col-lg-12 text-center">



            <div class="">

                <div class="row">

                    <div class="container content_soutenons_1">

                        <div class="row">

                            <div class="col-lg-4"></div>

                            <div class="col-lg-4">

                                <!-- <div class="dropdown"> -->

                                <!-- <div class="btn btn-secondary dropdown-toggle border_content_soutenons_dep" type="button" id="Recherches_par_departement" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->

                                <?php

                                // if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != 0 && $localdata_IdDepartement != "0" && intval($localdata_IdDepartement) != 9999) {

                                //     if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") {

                                //         foreach ($toDepartement as $oDepartement) {

                                //             if ($oDepartement->departement_id == $localdata_IdDepartement)

                                //                 echo $oDepartement->departement_nom; //$oDepartement->nbCommercant

                                //         }

                                //     }

                                // } else echo 'Recherches par département';

                                ?>

                                <!-- </div> -->

                                <!-- <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_Recherches_par_departement" aria-labelledby="dropdownMenuButton_com"> -->

                                <?php

                                // if ($pagecategory == "annuaire" || $pagecategory == "annonce") {

                                //     if (isset($toDepartement) && $toDepartement != "" && $toDepartement != 0 && $toDepartement != "0") {

                                //         foreach ($toDepartement as $oDepartement) {

                                ?>

                                <!-- <a class="dropdown-item" id="department_select_id_<?php // echo $oDepartement->departement_id 

                                                                                        ?>" href="javascript:void(0);" onclick="

                                                                   //javascript:pvc_select_departement(<?php //echo $oDepartement->departement_id; 

                                                                                                        ?>);

                                                                   //javascript:change_commune_by_department_soutenons(<?php //echo $oDepartement->departement_id; 

                                                                                                                        ?>);

                                                                   //javascript:change_categories_by_department_soutenons(<?php //echo $oDepartement->departement_id; 

                                                                                                                            ?>);

                                                                   //javascript:change_subcategories_by_department_soutenons(<?php //echo $oDepartement->departement_id; 

                                                                                                                                ?>);

                                                                   "><?php //echo $oDepartement->departement_nom . " (" . $oDepartement->nbCommercant . ") "; // $oDepartement->nbCommercant 

                                                                        ?></a> -->

                                <?php

                                //    }

                                //    }

                                //} else {

                                //foreach ($toDepartement as $toDepartements) {

                                ?>

                                <!-- <a class="dropdown-item" id="department_select_id_<?php //echo $toDepartements['departement_id'] 

                                                                                        ?>" href="javascript:void(0);" onclick="//javascript:pvc_select_departement_deal(<?php //echo $toDepartements['departement_id']; 

                                                                                                                                                                            ?>);"><?php //echo $toDepartements['departement_nom']; // $toDepartements['nbCommercant'] 

                                                                                                                                                                                        ?></a> -->

                                <?php

                                //    }

                                //}

                                ?>

                                <!-- </div> -->

                                <!-- </div> -->

                            </div>

                            <div class="col-lg-4"></div>

                        </div>

                        <script type="text/javascript">

                            function pvc_select_departement(departement_id) {

                                $("#inputStringDepartementHidden_partenaires").val(departement_id);

                                $("#Recherches_par_departement").html($("#department_select_id_" + departement_id).html());

                                $("#dropdownMenuButton_com").html("Recherche par communes");

                                $("#dropdownMenuButton_com_categ").html("Recherche par catégories");

                                $("#dropdownMenuButton_com_sucateg").html("Recherche par sous catégories");

                                $("#inputStringVilleHidden_partenaires").val("");

                                $("#inputStringHidden").val("");

                                $("#inputStringHiddenSubCateg").val("");

                            }

                        </script>



                        <div class="row">

                            <div class="col-sm-12 col-lg-4 pt-2">

                                <?php $data['empty'] = null;

                                $this->load->view("sortez_soutenons/includes/main_communes_select", $data); ?>

                            </div>

                            <div class="col-sm-12 col-lg-4 pt-2">

                                <div class="dropdown">

                                    <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="button" id="dropdownMenuButton_com_categ" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                        Recherche

                                        par

                                        catégories

                                    </div>

                                    <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="<?php if ($pagecategory == "annuaire") {

                                                                                                                    echo "span_leftcontener2013_form_partenaires";

                                                                                                                } else if ($pagecategory == "annonce") {

                                                                                                                    echo "span_leftcontener2013_form_annonces";

                                                                                                                } else {

                                                                                                                    echo "span_leftcontener2013_form_deal";

                                                                                                                } ?>" aria-labelledby="dropdownMenuButton_com">

                                        <?php if ($toallcateg != '' && $toallcateg != null) { ?>

                                            <?php if ($pagecategory == "annuaire" || $pagecategory == "annonce") { ?>

                                                <?php foreach ($toallcateg as $toctages) { ?>

                                                    <a class="dropdown-item" href="javascript:void(0);" onclick="javascript:pvc_select_departement(<?php echo $toctages->IdRubrique; ?>);"><?php echo htmlspecialchars_decode($toctages->rubrique) . " (" . $toctages->nbCommercant . ")";; ?></a>

                                                <?php } ?>

                                            <?php } else { ?>

                                                <?php foreach ($toallcateg as $toctages) { ?>

                                                    <a class="dropdown-item" href="javascript:void(0);" id="content_deal_<?php echo $toctages['IdRubrique']; ?>" onclick="javascript:pvc_select_categ_deal(<?php echo $toctages['IdRubrique']; ?>);"><?php echo $toctages['rubrique']; ?></a>

                                                <?php } ?>

                                            <?php } ?>

                                        <?php } ?>

                                    </div>

                                </div>

                            </div>

                            <?php if ($pagecategory == "annuaire") { ?>

                                <script type="text/javascript">

                                    function show_current_categcontent_soutenons() {



                                    }



                                    function show_current_subcategcontent_soutenons(id_sousrubrique) {

                                        $("#inputStringHiddenSubCateg").val(id_sousrubrique);

                                        //$("#frmRecherchePartenaire").submit();

                                        //$("#div_subcateg_annuaire_contents").css("display","none");

                                        $('#div_subcateg_annuaire_contents').css('display', 'none');

                                        $("#dropdownMenuButton_com_sucateg").html($("#vsv_sub_categmain_" + id_sousrubrique).html());

                                    }

                                </script>

                            <?php } ?>

                            <?php if ($pagecategory == "annonce") { ?>

                                <script type="text/javascript">

                                    function show_current_categcontent_soutenons() {



                                    }



                                    function show_current_subcategcontent_soutenons(id_sousrubrique) {

                                        $("#inputStringHiddenSubCateg").val(id_sousrubrique);

                                        //$("#frmRecherchePartenaire").submit();

                                        //$("#div_subcateg_annuaire_contents").css("display","none");

                                        $('#div_subcateg_annuaire_contents').css('display', 'none');

                                        $("#dropdownMenuButton_com_sucateg").html($("#vsv_sub_categmain_" + id_sousrubrique).html());

                                    }



                                    function show_current_categ_subcateg(id_sousrubrique) {

                                        $("#inputStringHidden").val(id_sousrubrique);

                                        //$("#frmRecherchePartenaire").submit();

                                        //$("#div_subcateg_annuaire_contents").css("display","none");

                                        $("#dropdownMenuButton_com_categ").html($('#vsv_categmain_' + id_sousrubrique).html());

                                        show_current_categ_subcateg_1(id_sousrubrique);

                                    }

                                </script>

                            <?php } ?>

                            <div class="col-sm-12 col-lg-4 pt-2">

                                <?php

                                if ($pagecategory == "annuaire" || $pagecategory == "annonce") { ?>

                                    <div class="dropdown">

                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="button" id="dropdownMenuButton_com_sucateg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            <?php

                                            if (isset($localdata_iSubCategorieId) && $localdata_iSubCategorieId != "" && $localdata_iSubCategorieId != 0 && $localdata_iSubCategorieId != "0") {

                                                if (isset($toSubCategorie) && $toSubCategorie != "" && $toSubCategorie != 0 && $toSubCategorie != "0") {

                                                    foreach ($toSubCategorie as $oSubCategorie) {

                                                        if ($oSubCategorie->IdSousRubrique == $localdata_iSubCategorieId)

                                                            echo $oSubCategorie->Nom;

                                                    }

                                                }

                                            } else echo 'Recherche

                                                par

                                                sous

                                                catégories';

                                            ?>

                                        </div>

                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" id="div_subcateg_annuaire_contents" aria-labelledby="dropdownMenuButton_com">

                                        </div>

                                    </div>

                                <?php } else { ?>

                                    <div class="dropdown">

                                        <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="button" data-toggle="dropdown" aria-haspopup="true" id="check_type_vue_deal" aria-expanded="false">

                                            Deals

                                            ou

                                            fidélité

                                        </div>

                                        <div style="font-size:12px;" class="dropdown-menu main_communes_select" aria-labelledby="dropdownMenuButton_com">

                                            <a class="dropdown-item" id="value_get_deal_0" onclick="javascript:getvaluetypedeal(0)">

                                                Toutes

                                                les

                                                offres

                                            </a>

                                            <a class="dropdown-item" id="value_get_deal_1" onclick="javascript:getvaluetypedeal(1)">

                                                Les

                                                deals

                                            </a>

                                            <a class="dropdown-item" id="value_get_deal_2" onclick="javascript:getvaluetypedeal(2)">

                                                La

                                                fidélité

                                            </a>

                                        </div>

                                    </div>

                                <?php } ?>

                            </div>

                            <?php

                            if ($pagecategory == "annuaire") { ?>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <input class=" border_content_soutenons" id="zMotcle_value" type="text" placeholder="Recherches par mot clé" value="<?php if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; ?>">

                                </div>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                    <div class="btn btn-secondary border_content_soutenons"><input id="filter_coronna_ligne" class="check_filter" type="checkbox">

                                        <!-- <div class="text_input">Coronavirus : établissements ouverts</div> -->

                                        <div class="text_input">Restaurants : réservations & plats du jour</div>

                                    </div>

                                </div>

                                <div class="col-sm-12 col-lg-4 text-left pt-2">

                                <div class="btn btn-secondary border_content_soutenons">

                                <input id="filter_com_en_ligne" class="check_filter" type="checkbox">

                                <div class="text_input">Formulaires de commande en ligne</div>

                                </div>

                                </div>

                                <?php } else if ($pagecategory == "annonce") { ?>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                <div class="dropdown">

                                <div class="btn btn-secondary dropdown-toggle border_content_soutenons" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="filter_id_annonce">

                                    Filtrer

                                </div>

                                <div style="font-size:12px;" class="dropdown-menu main_communes_select" aria-labelledby="dropdownMenuButton_com">

                                    <a class="dropdown-item" id="getvalueetat_1" onclick="javascript:getvalueetat(1)">Neuf</a>

                                    <a class="dropdown-item" id="getvalueetat_0" onclick="javascript:getvalueetat(0)">Revente</a>

                                    <a class="dropdown-item" id="getvalueetat_2" onclick="javascript:getvalueetat(2)">Service</a>

                                </div>

                                </div>

                                </div>

                                <?php } else { ?>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                <input class="border_content_soutenons" id="zMotcle_value" type="text" placeholder="Recherches par mot clé" value="<?php if (isset($localdata_zMotCle) && $localdata_zMotCle != '') echo $localdata_zMotCle; ?>">

                                </div>

                                <?php } ?>

                                <div class="col-sm-12 col-lg-4 <?php if ($pagecategory == "annuaire") {

                                                        echo 'offset-lg-2';

                                                    } ?> pt-2">

                                <div class="btn btn-secondary reinit_soutenons" onclick="<?php if ($pagecategory == "annuaire") {

                                                                                        echo "btn_re_init_annuaire_list();";

                                                                                    } else if ($pagecategory == "annonce") {

                                                                                        echo "btn_re_init_annonce_list();";

                                                                                    } else {

                                                                                        echo "btn_re_init_deal_list();";

                                                                                    } ?>">

                                Réinitialisez

                                </div>

                                </div>

                                <div class="col-sm-12 col-lg-4 pt-2">

                                <div class="btn btn-secondary applic_soutenons" onclick="javascript:<?php if ($pagecategory == "annuaire") {

                                                                                                echo "form_submit_search_annuaire_soutenons();";

                                                                                            } else if ($pagecategory == "annonce") {

                                                                                                echo "form_submit_search_annonce_soutenons();";

                                                                                            } else {

                                                                                                echo "form_submit_search_deal_soutenons();";

                                                                                            } ?>">

                                Appliquez

                                votre

                                choix

                                </div>

    <script type="text/javascript">

    function getvalueetat(valeur_etat) {

    $('#iEtat').val(valeur_etat);

    $('#filter_id_annonce').html($('#getvalueetat_' + valeur_etat).html());

    }



    function btn_re_init_annonce_list() {

    $('#iEtat').val("");

    $('#inputStringDepartementHidden_partenaires').val("");

    $('#inputStringVilleHidden_partenaires').val("");

    $('#inputStringHiddenSubCateg').val("");

    $("#inputStringHidden").val("");

    $('#filter_id_annonce').html("Filtrer");

    $('#Recherches_par_departement').html("Recherches par departement");

    $('#dropdownMenuButton_com').html("Recherches par communes");

    $('#dropdownMenuButton_com_sucateg').html("Recherches par sous catégories");

    $("#dropdownMenuButton_com_categ").html("Recherche par catégories");

    form_submit_search_annonce_soutenons();

    }



    function form_submit_search_annonce_soutenons() {

    var base_url_visurba = '<?php echo site_url(); ?>';

    jQuery("#id_mainbody_main").html('<div class="text-center w-100"><img src="' + base_url_visurba + 'application/resources/front/images/wait.gif" alt="loading...."/></div>');



    var inputStringDepartementHidden_partenaires = document.getElementById("inputStringDepartementHidden_partenaires").value;

    var inputStringVilleHidden_partenaires = document.getElementById("inputStringVilleHidden_partenaires").value;

    var inputStringHidden = document.getElementById("inputStringHidden").value;

    var inputStringHiddenSubCateg = document.getElementById("inputStringHiddenSubCateg").value;

    var inputStringOrderByHidden = document.getElementById("inputStringOrderByHidden").value;

    var iEtat = document.getElementById("iEtat").value;

    var inputAvantagePartenaire = document.getElementById("inputAvantagePartenaire").value;

    var inputStringCommercantHidden = document.getElementById("inputStringCommercantHidden").value;



    //alert(inputStringDepartementHidden_partenaires);



    jQuery.ajax({

        method: "POST",

        url: '<?php echo site_url("soutenons/Annonce/index/?content_only_list=1"); ?>',

        data: {

            inputStringDepartementHidden: inputStringDepartementHidden_partenaires,

            inputStringVilleHidden_annonces: inputStringVilleHidden_partenaires,

            inputStringHidden: inputStringHiddenSubCateg,

            inputStringHiddenSubCateg: inputStringHidden,

            inputStringOrderByHidden: inputStringOrderByHidden,

            iEtat: iEtat,

            inputAvantagePartenaire: inputAvantagePartenaire,

            inputStringCommercantHidden: inputStringCommercantHidden

        },

        dataType: 'html',

        success: function(html) {

            jQuery('#id_mainbody_main').html(html);

        }

    });

    }



    function btn_re_init_deal_list() {

    $('#zMotcle_value').val("");

    $('#type_vue_deal').val("0");

    $('#id_categ_deal').val("0");

    $('#id_depart_deal').val("0");

    $('#id_communes_deal').val("0");

    $('#Recherches_par_departement').html("Recherches par departement");

    $('#check_type_vue_deal').html("Deals ou fidélité");

    $('#dropdownMenuButton_com').html("Recherches par communes");

    $("#dropdownMenuButton_com_categ").html("Recherche par catégories");

    form_submit_search_deal_soutenons();

    }



    function form_submit_search_deal_soutenons() {

    var base_url_visurba = '<?php echo site_url(); ?>';

    jQuery("#id_mainbody_main").html('<div class="text-center w-100"><img src="' + base_url_visurba + 'application/resources/front/images/wait.gif" alt="loading...."/></div>');



    var zMotcle_value = document.getElementById("zMotcle_value").value;

    var type_vue_deal = document.getElementById("type_vue_deal").value;

    var id_categ_deal = document.getElementById("id_categ_deal").value;

    var id_depart_deal = document.getElementById("id_depart_deal").value;

    var id_communes_deal = document.getElementById("id_communes_deal").value;



    //alert(inputStringDepartementHidden_partenaires);



    jQuery.ajax({

        method: "POST",

        url: '<?php echo site_url("DealAndFidelite/index/?content_only_list=1"); ?>',

        data: {

            id_communes_deal: id_communes_deal,

            id_depart_deal: id_depart_deal,

            id_categ_deal: id_categ_deal,

            type_vue_deal: type_vue_deal,

            zMotcle_value: zMotcle_value

        },

        dataType: 'html',

        success: function(html) {

            jQuery('#id_mainbody_main').html(html);

        }

    });

    }



    function form_submit_search_annuaire_soutenons(keyword_value) {

    var base_url_visurba = '<?php echo site_url(); ?>';

    jQuery("#Id_main_bodylistannuaire_main").html('<div class="text-center w-100"><img src="' + base_url_visurba + 'application/resources/front/images/wait.gif" alt="loading...."/></div>');

    $("#frmRecherchePartenaire #zMotCle").val($("#zMotcle_value").val());



    var inputStringDepartementHidden_partenaires = document.getElementById("inputStringDepartementHidden_partenaires").value;

    var inputStringVilleHidden_partenaires = document.getElementById("inputStringVilleHidden_partenaires").value;

    var inputStringHidden = document.getElementById("inputStringHidden").value;

    var inputStringHiddenSubCateg = document.getElementById("inputStringHiddenSubCateg").value;

    var inputStringOrderByHidden = document.getElementById("inputStringOrderByHidden").value;

    var input_is_actif_coronna = document.getElementById("input_is_actif_coronna").value;

    var input_is_actif_command = document.getElementById("input_is_actif_command").value;

    var zMotCle = document.getElementById("zMotCle").value;

    var inputAvantagePartenaire = document.getElementById("inputAvantagePartenaire").value;



    //alert(inputStringDepartementHidden_partenaires);



    jQuery.ajax({

        method: "POST",

        url: '<?php echo site_url("soutenons/Annuaire_Soutenons/index/?content_only_list=1"); ?>',

        data: {

            inputStringDepartementHidden: inputStringDepartementHidden_partenaires,

            inputStringVilleHidden: inputStringVilleHidden_partenaires,

            inputStringHidden: inputStringHidden,

            inputStringHiddenSubCateg: inputStringHiddenSubCateg,

            inputStringOrderByHidden: inputStringOrderByHidden,

            input_is_actif_coronna: input_is_actif_coronna,

            input_is_actif_command: input_is_actif_command,

            zMotCle: zMotCle,

            inputAvantagePartenaire: inputAvantagePartenaire

        },

        dataType: 'html',

        success: function(html) {

            jQuery('#Id_main_bodylistannuaire_main').html(html);

        }

    });



    //$("#frmRecherchePartenaire").submit();

    }



    function getvaluetypedeal(get_val) {

    $('#type_vue_deal').val(get_val);

    $('#check_type_vue_deal').html($('#value_get_deal_' + get_val).html());

    }



    function pvc_select_categ_deal(id_categ) {

    $('#id_categ_deal').val(id_categ);

    $('#dropdownMenuButton_com_categ').html($('#content_deal_' + id_categ).html());

    }



    function pvc_select_departement_deal(id_depart) {

    $('#id_depart_deal').val(id_depart);

    $('#Recherches_par_departement').html($('#department_select_id_' + id_depart).html());

    }



    function pvc_select_communes(id_communes) {

    $('#id_communes_deal').val(id_communes);

    $('#dropdownMenuButton_com').html($('#commune_select_id_' + id_communes).html())

    }

    </script>

    <?php if ($pagecategory == "dealandfidelity") { ?>

    <input type="hidden" id="type_vue_deal" value="0">

    <input type="hidden" id="id_categ_deal" value="0">

    <input type="hidden" id="id_depart_deal" value="0">

    <input type="hidden" id="id_communes_deal" value="0">

    <?php } ?>



    </div>

    </div>

    </div>

    </div>

    </div>



    </div>

    </div>

    </div>

    <style>

    .check_filter {

    width: 26px;

    height: 26px;

    }

    </style>

    <input type="hidden" value="0" id="input_is_actif_coronna">

    <input type="hidden" value="0" id="input_is_actif_command">

    <script>

    $("#filter_coronna_ligne").change(function() {

    if (document.getElementById('filter_coronna_ligne').checked == true) {



    $("#input_is_actif_coronna").val(1);



    } else {

    $("#input_is_actif_coronna").val(0);

    }

    })



    $("#filter_com_en_ligne").change(function() {

    if (document.getElementById('filter_com_en_ligne').checked == true) {



    $("#input_is_actif_command").val(1);



    } else {

    $("#input_is_actif_command").val(0);

    }

    })

    </script>

    <style>

    .reinit_soutenons {

    padding-bottom: 10px;

    width: 100%;

    color: #ffffff !important;

    background-color: rgba(230, 14, 136, 1);

    border: solid rgba(32, 206, 136, 1) 0px;

    border-radius: 0px !important;

    cursor: pointer !important;

    }



    .reinit_soutenons:hover {

    padding-bottom: 10px;

    width: 100%;

    color: #ffffff !important;

    background-color: rgba(230, 14, 136, 1);

    border: solid rgba(32, 206, 136, 1) 0px;

    border-radius: 0px !important;

    cursor: pointer !important;

    }

    </style>