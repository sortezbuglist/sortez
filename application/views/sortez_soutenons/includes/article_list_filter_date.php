<div class="row">
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(101);">
            <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                   id="optionsRadios_0" value="0"
                   <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '101') { ?>checked<?php } ?>>
            Aujourd'hui
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(303);">
            <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                   id="optionsRadios_1" value="1"
                   <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '303') { ?>checked<?php } ?>>
            Cette semaine
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(202);">
            <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                   id="optionsRadios_2" value="2"
                   <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '202') { ?>checked<?php } ?>>
            Ce Week-end
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(404);">
            <input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                   id="optionsRadios_3" value="3"
                   <?php if (isset($inputStringOrderByHidden) && $inputStringOrderByHidden == '404') { ?>checked<?php } ?>>
            Semaine prochaine
        </label>

    </div>
</div>
<div class="row">
    <div class="col p-2">
        <label class="form-check-label btn subcateb_filter_banner w-100"><!-- onclick="javascript:article_filter_date_choice();"-->
            <!--<input type="radio" class="form-check-input" name="inputStringQuandHidden_check"
                   id="input_check_filter_datechoice" value="4"
                   <?php /*if ((isset($inputStringDatedebutHidden) && $inputStringDatedebutHidden != '' && $inputStringDatedebutHidden != "0000-00-00") ||
                   (isset($inputStringDatefinHidden) && $inputStringDatefinHidden != '' && $inputStringDatefinHidden != "0000-00-00")) { */?>checked<?php //} ?>>-->
            Filtrer par date
        </label>
    </div>
    <div class="col p-2">
        <input type="text" id="inputStringDatedebutHidden_check" name="inputStringDatedebutHidden_check" class="form-control tab_filter_date_input" placeholder="Date début (jj/mm/aaaa)" value="<?php if (isset($inputStringDatedebutHidden) && $inputStringDatedebutHidden != '' && $inputStringDatedebutHidden != "0000-00-00") echo convertDateWithSlashes($inputStringDatedebutHidden); ?>"/>
    </div>
    <div class="col p-2">
        <input type="text" id="inputStringDatefinHidden_check" name="inputStringDatefinHidden_check" class="form-control tab_filter_date_input" placeholder="Date fin (jj/mm/aaaa)" value="<?php if (isset($inputStringDatefinHidden) && $inputStringDatefinHidden != '' && $inputStringDatefinHidden != "0000-00-00") echo convertDateWithSlashes($inputStringDatefinHidden); ?>"/>
    </div>
    <div class="col p-2">
        <a href="javascript:void(0);" id="input_submit_filter_date_choice" type="button" class="btn btn-secondary w-100 tab_filter_date_input" onclick="javascript:article_filter_date_submit();">Valider</a>
    </div>
</div>
