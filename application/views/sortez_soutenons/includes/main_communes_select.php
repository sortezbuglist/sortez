<?php
//LOCALDATA FILTRE
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
$localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
$iDepartementId_x_soutenons = $this_session_localdata->session->userdata('iDepartementId_x');
$iVilleId_x = $this_session_localdata->session->userdata('iVilleId_x');
?>
<div class="dropdown">
    <div class="btn btn-secondary dropdown-toggle border_content_soutenons"
         type="button"
         id="dropdownMenuButton_com"
         data-toggle="dropdown"
         aria-haspopup="true"
         aria-expanded="false">
        <?php
        if (isset($iVilleId_x) && $iVilleId_x != "" && $iVilleId_x != 0 && $iVilleId_x != "0") {
            foreach ($toVille as $oVille) {
                if ($oVille->IdVille == $iVilleId_x)
                    echo $oVille->ville_nom; //$oVille->nbCommercant
            }
        } else echo 'Recherche par communes';
        ?>
    </div>
    <div style="font-size:12px;"
         class="dropdown-menu main_communes_select"
         id="container_dropdownMenuButton_com"
         aria-labelledby="dropdownMenuButton_com">
        <?php if ($pagecategory == "annuaire" || $pagecategory == "annonce") { ?>
            <?php
            if (isset($toVille)) {
                foreach ($toVille as $oVille) { ?>
                    <a class="dropdown-item"
                       id="commune_select_id_<?= $oVille->IdVille ?>"
                       href="javascript:void(0);"
                       onclick="javascript:pvc_select_commune(<?php echo $oVille->IdVille; ?>);"><?php echo $oVille->ville_nom ; //$oVille->nbCommercant ?></a>
                <?php }
            }
        } else {
            foreach ($toVille as $oVille) { ?>
                <a class="dropdown-item"
                   id="commune_select_id_<?= $oVille['IdVille'] ?>"
                   href="javascript:void(0);"
                   onclick="javascript:pvc_select_communes(<?php echo $oVille['IdVille']; ?>);"><?php echo $oVille['ville_nom']; //$oVille['nbCommercant'] ?></a>
                <?php
            }
        }
        ?>
    </div>
</div>
<script type="text/javascript">
    function pvc_select_commune(id_commune) {
        $("#inputStringVilleHidden_partenaires").val(id_commune);
        $("#dropdownMenuButton_com").html($("#commune_select_id_" + id_commune).html());
        //$("#frmRecherchePartenaire").submit();
    }
</script>