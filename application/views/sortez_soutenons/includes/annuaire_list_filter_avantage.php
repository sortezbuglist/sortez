<div class="row">
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(0);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_partenaires"
                   id="optionsRadios_0" value="0" checked>
            Toutes les Rubriques
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(1);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_partenaires"
                   id="optionsRadios_1" value="1"
                   <?php if (isset($inputAvantagePartenaire) && $inputAvantagePartenaire == '1') { ?>checked<?php } ?>>
            Les bons plans
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(2);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_partenaires"
                   id="optionsRadios_2" value="2"
                   <?php if (isset($inputAvantagePartenaire) && $inputAvantagePartenaire == '2') { ?>checked<?php } ?>>
            La fidélité
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(3);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_partenaires"
                   id="optionsRadios_3" value="3"
                   <?php if (isset($inputAvantagePartenaire) && $inputAvantagePartenaire == '3') { ?>checked<?php } ?>>
            Les boutiques en ligne
        </label>

    </div>
    <div class="col p-2">

        <label class="form-check-label btn subcateb_filter_banner w-100" onclick="javascript:annuaire_filter_avantage(4);">
            <input type="radio" class="form-check-input" name="inputStringAvantageHidden_partenaires"
                   id="optionsRadios_4" value="4"
                   <?php if (isset($inputAvantagePartenaire) && $inputAvantagePartenaire == '4') { ?>checked<?php } ?>>
            L'agenda
        </label>

    </div>
</div>