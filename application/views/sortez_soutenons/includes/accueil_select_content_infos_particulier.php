<div class="container accueil_select_content_2 text-center pt-3">
    <div class="row">
        <div class="col-12">
            <p>
                <a href="<?php echo site_url('vivresaville/ListVille'); ?>"
                   class="vsv_accueil_btn vsv_select_link plus_infos_link">RETOUR PAGE D'ACCUEIL...</a>
            </p>
            <p class="title_1 pt-5 pl-5 pr-5">
                LES ADMINISTRÉS ACCÈDENT<br/>
                À UNE MULTITUDE D’INFORMATIONS<br/>
                QUI DYNAMISENT LA VIE LOCALE ET COMMERCIALE<br/>
                DE LEUR COMMUNE OU DE LEUR TERRITOIRE !...
            </p>
            <p class="title_1 txt_rose_color title_partenaire p-3">
                Un annuaire complet - L’actualité -  Un agenda événementiel - Des bonnes affaires<br/>
                Une carte de fidélité - Des boutiques en ligne
            </p>
            <p class="pt-5">
                <img
                    src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wpa89873e4_06.png"
                    class="mt-0 border-0 img-fluid"/>
            </p>
            <p class="title_3 pb-3">
                Les sites s’adaptent à tous les écrans
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <p class="title_1 title_partenaire pl-5 pr-5 pt-3">
                DEMANDEZ GRATUITEMENT LA CARTE PRIVILÈGE VIVRESAVILLE<br/>
                ET BÉNÉFICIEZ DE MULTIPLES AVANTAGES…
            </p>
            <p>
            <ul class="accueil_list_ul">
                <li>Afin de bénéficier des bons plans et des conditions de fidélisation, les consommateurs souscrivent gratuitement à notre carte privilège</li>
                <li>Avec cette carte, ils disposent sur leur compte personnel, du suivi des transactions (réservations des bons plans, objectifs et soldes des conditions de fidélisation).</li>
                <li>Avec cette carte, vous recevez automatiquement notre newsletter et accéder à la gestion de vos favoris. </li>
                <li>La carte est valable sur l’ensemble des sites vivresaville.fr et sur notre site du magazine Sortez</li>
            </ul>
            </p>
            <p class="pt-5">
                <img
                    src="<?php echo base_url(); ?>application/resources/front/images/vivresaville_custom/wp5f352936_06.png"
                    class="mt-0 border-0 img-fluid"/>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 pt-5">
            <p class="pt-5 pb-5 text-center">
                <a href="https://spark.adobe.com/page/8WadzNJv1u3cF/" target="_blank" class="vsv_accueil_btn vsv_select_link" style="width: 200px; display: inline; text-transform: uppercase;">Plus dinformations...</a>
            </p>
        </div>
        <div class="col-sm-6 pt-5">
            <p class="pt-5 pb-5 text-center">
                <a href="<?php echo site_url('front/particuliers/inscription');?>" class="vsv_accueil_btn vsv_select_link" style="width: 200px; display: inline; text-transform: uppercase;">Inscription gratuite...</a>
            </p>
        </div>
    </div>
</div>