<nav class="d-flex d-lg-none d-xl-none navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="https://www.soutenonslecommercelocal.fr/">Soutenons le commerce local</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="https://www.soutenonslecommercelocal.fr/">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>soutenons">Les Bonnes adresses</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>soutenons/Annonce">Les boutiques</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>soutenons/DealAndFidelite">Deals & fidélité</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.magazine-sortez.org/newsletter">Newsletter</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>auth/login">Mon compte</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= site_url();?>">Magazine-sortez.org</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.magazine-sortez.org/nous-contacter">Contact</a>
            </li>
        </ul>
    </div>
</nav>

<style type="text/css">
    .navbar-collapse {
        font-size: 16px;
    }
    .navbar-collapse {
        font-size: 16px;
    }
    .navbar li.nav-item {
        border-bottom: groove;
    }
</style>