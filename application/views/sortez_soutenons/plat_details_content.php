<?php $data["empty"] = NULL; ?>
<?php //var_dump($oDetailPlat);die(); ?>
<div class="container" style="display: table;width: 100%; background-color:#FFFFFF!important;">

    <div class="col-lg-12" style="display:table; width:100%; background-color:#FFFFFF; padding-top:20px;">

        <div class="col-xs-12 padding0" style="text-align:center; min-height:40px; padding-top:30px;"><a
                href="<?php echo site_url("plat_du_jour/liste_plat/"); ?>" class="btn_link_rose">Retour &agrave; la liste des
                plat du jour</a></div>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#contact_partner_reset").click(function () {
                    $("#contact_partner_nom").val();
                    $("#contact_partner_tel").val();
                    $("#contact_partner_mail").val();
                    $("#contact_partner_msg").val();
                    $("#spanContactPartnerForm").html('* champs obligatoires');
                });

                $("#contact_partner_send").click(function () {
                    var error = 0;
                    var contact_partner_nom = $("#contact_partner_nom").val();
                    if (contact_partner_nom == '') error = 1;
                    var contact_partner_tel = $("#contact_partner_tel").val();
                    if (contact_partner_tel == '') error = 1;
                    var contact_partner_mail = $("#contact_partner_mail").val();
                    if (contact_partner_mail == '') error = 1;
                    if (!verifier(contact_partner_mail)) error = 2;
                    var contact_partner_msg = $("#contact_partner_msg").val();
                    if (contact_partner_msg == '') error = 1;
                    $("#spanContactPartnerForm").html('<img src="<?php echo GetImagePath("front/");?>/loading.gif" />');

                    if (error == 1) {
                        $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Les champs sont obligatoires</span>');
                    } else if (error == 2) {
                        $("#spanContactPartnerForm").html('<span style="color:#FF0000;">* Adresse email invalide</span>');
                        $("#contact_partner_mail").css('border-color', '#ff0000');
                    } else {
                        $.post(
                            "<?php echo site_url("front/professionnels/contact_partner_sendmail/");?>",
                            {
                                contact_partner_nom: contact_partner_nom,
                                contact_partner_tel: contact_partner_tel,
                                contact_partner_mail: contact_partner_mail,
                                contact_partner_msg: contact_partner_msg,
                                contact_partner_mailto: "<?php echo $oDetailPlat->email; ?>"
                            },
                            function (data) {
                                $("#spanContactPartnerForm").html(data);
                            });
                    }
                });

            });
            function verifier(contact_partner_mail) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(contact_partner_mail);
            }
        </script>

        <!--Video content-->
        <div class="modal fade" id="divVideoPartnerAgenda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Vid&eacute;o</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>



        <!--Contact form contet-->
        <div class="modal fade" id="divContactPartnerForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nous Contacter</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div style="background-color:#FFFFFF;">
                            <form name="formContactPartnerForm" id="formContactPartnerForm" action="#">
                                <div class="form-group"><input type="text" class="form-control" name="contact_partner_nom" id="contact_partner_nom" placeholder="Votre nom *"/>
                                </div>
                                <div class="form-group"><input type="text" class="form-control" name="contact_partner_tel" id="contact_partner_tel"
                                                               placeholder="Votre numéro de téléphone *"/></div>
                                <div class="form-group"><input type="email" name="contact_partner_mail" id="contact_partner_mail" class="form-control" aria-describedby="emailHelp"
                                                               placeholder="Votre courriel *"/></div>
                                <div class="form-group"><textarea class="form-control" name="contact_partner_msg" id="contact_partner_msg" placeholder="Votre message *"></textarea>
                                </div>
                                <div><span id="spanContactPartnerForm" class="text-danger">* champs obligatoires</span></div>
                                <div>
                                    <div class="text-right"><input type="button" class="btn btn-success" name="contact_partner_send"
                                                                   id="contact_partner_send" value="Envoyer"/></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $oDetailPlat->IdUsers_ionauth . "/";
        $photoCommercant_path2= "application/resources/front/photoCommercant/imagesbank/scrapped_image/";
        $photoCommercant_path_old = "application/resources/front/photoCommercant/imagesbank/rss_image/";
        ?>

        <?php
        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        //var_dump($oDetailPlat);
        ?>


        <div class="col-lg-12 padding0">
            <div class="col-lg-12 p-0" style="background-color:white; margin-top:15px; display:table;">
                <?php
                $image_home_vignette = base_url()."/application/resources/front/photoCommercant/imagesbank/". $oDetailPlat->IdUsers_ionauth."/";
                if (isset($oDetailPlat->photo) && $oDetailPlat->photo != "" && is_file($photoCommercant_path . $oDetailPlat->photo) == true) { ?>
                    <img style="width:100%;height:auto;border-radius: 10px;" src=" <?php echo $image_home_vignette.$oDetailPlat->photo; ?>">
                <?php } ?>
            </div>
            <div class="col-lg-12" style="text-align: center;">
                <span
                    class="oDetailAgenda_nom_manifestation"><?php echo $oDetailPlat->description_plat; ?></span><br/>
                <span
                    class="oDetailAgenda_telephone"><?php echo $oDetailPlat->nom; ?></span><br/>
                <span
                    class="oDetailAgenda_telephone"><?php echo $oDetailPlat->ville; ?></span><br/>
                <span
                        style="
   	                                background-color: transparent;
                                    color: #008000;
                                    font-family: 'Arial',sans-serif;
                                    font-size: 26.7px;
                                    font-style: normal;
                                    font-variant: normal;
                                    font-weight: normal;
                                    /*line-height: 33px;*/
                                    text-decoration: none;
                                    vertical-align: 0;
                                    text-align:center;
                        "><?php echo $oDetailPlat->prix_plat; ?>€</span><br/>
                <span
                        style="background-color: transparent;
                                   color: #000000;
                                   font-family: 'Arial',sans-serif;
                                    font-size: 16px;
                                    font-style: normal;
                                    font-variant: normal;
                                    font-weight: normal;
                                    /*line-height: 20px;*/
                                    text-decoration: none;
                                    text-align:center; height:85px; overflow:hidden;
                                    vertical-align: 0;"><?php echo $oDetailPlat->nbre_plat_propose; ?> plat proposé</span><br/>

                <span style="background-color: transparent;
                                   color: #000000;
                                   font-family: 'Arial',sans-serif;
                                    font-size: 16px;
                                    font-style: normal;
                                    font-variant: normal;
                                    font-weight: normal;
                                    /*line-height: 20px;*/
                                    text-decoration: none;
                                    text-align:center; height:85px; overflow:hidden;
                                    vertical-align: 0;">
                    
                <?php
                if (isset($oDetailPlat->date_debut_plat) && $oDetailPlat->date_debut_plat != "0000-00-00" && ($oDetailPlat->date_debut_plat == $oDetailPlat->date_fin_plat)) {
                    echo "<br/>Le " . translate_date_to_fr($oDetailPlat->date_debut_plat);
                    if (isset($oDetailPlat->heure_debut_plat) && $oDetailPlat->heure_debut_reservation != "0:00"  && $oDetailPlat->heure_debut_reservation != "" && $oDetailPlat->heure_debut_reservation != null) echo " à " . str_replace(":", "h", $oDetailPlat->heure_debut_reservation);
                } else {
                    if (isset($oDetailPlat->date_debut_plat) && $oDetailPlat->date_debut_plat != "0000-00-00") echo "<br/>Du " . translate_date_to_fr($oDetailPlat->date_debut_plat);
                    if (isset($oDetailPlat->date_fin_plat) && $oDetailPlat->date_fin_plat != "0000-00-00") {
                        if (isset($oDetailPlat->date_debut_plat) && $oDetailPlat->date_debut_plat != "0000-00-00") echo " au " . translate_date_to_fr($oDetailPlat->date_fin_plat);
                        else echo " Jusqu'au " . translate_date_to_fr($oDetailPlat->date_fin_plat);
                    }
                    if (isset($oDetailPlat->heure_debut_reservation) && $oDetailPlat->heure_debut_reservation != "0:00"   && $oDetailPlat->heure_debut_reservation != "" && $oDetailPlat->heure_debut_reservation != null) echo " à " . str_replace(":", "h", $oDetailPlat->heure_debut_reservation);
                }
                ?>
        </span><br/>
                <div class="col-lg-12" style="font-size:14px;font-family: 'Times New Roman', serif; padding-top: 10px;">
                    <?php
                    $this->load->model("mdlcommercant");
                    $oCommercant = $this->mdlcommercant->infoCommercant($oDetailPlat->IdCommercant);
                    //var_dump($oCommercant);die();
                    ?>
                    <span style="background-color: transparent;
                                   color: #000000;
                                   font-family: 'Arial',sans-serif;
                                    font-size: 16px;
                                    font-style: normal;
                                    font-variant: normal;
                                    font-weight: normal;
                                    /*line-height: 20px;*/
                                    text-decoration: none;
                                    text-align:center; height:85px; overflow:hidden;
                                    vertical-align: 0;">
                        <span class="oDetailAgenda_telephone">Les contacts du commercant</span>
                        <?php if ($oCommercant->TelFixe != "") echo "<br/>Téléphone fixe: " . $oCommercant->TelFixe; ?>
                        <?php if ($oCommercant->TelMobile != "") echo "<br/>Téléphone Mobile: " . $oCommercant->TelMobile; ?>
                        <?php if ($oCommercant->fax != "") echo "<br/>Fax: " . $oCommercant->fax; ?>
                    </span>
                </div>
                <a href="<?php echo site_url($oDetailPlat->url.'/reservation/')?>" target="_blank"><input class="btn_link_rose" style="text-transform: uppercase; cursor: pointer;" type="button" value="Je réserve"></a>
            </div>
        </div>

    </div>

    <!--<div class="col-lg-12 title_categ_black">Description</div>-->

    <div class="col-lg-12 article_all_details_content_container">
        <div class="col-lg-12" style="text-align:center; padding:15px 0 0;">
            <div class="addthis_inline_share_toolbox"></div>
        </div>
    </div>


<!--    --><?php //if (
//        (isset($oDetailPlat->description_tarif) && $oDetailPlat->description_tarif != "") ||
//        (isset($oDetailPlat->conditions_promo) && $oDetailPlat->conditions_promo != "") ||
//        (isset($oDetailPlat->reservation_enligne) && $oDetailPlat->reservation_enligne != "")
//    ) { ?>
<!---->
<!--        <div class="col-lg-12 title_categ_black" style="margin-bottom:0;">TARIF ET LIEN DE R&Eacute;SERVATION EN LIGNE-->
<!--        </div>-->
<!---->
<!--        <div class="col-lg-12" style="padding:15px 0; background-color:#FFFFFF;">-->
<!--            <div>-->
<!--                <div style='background-color: transparent;-->
<!--        color: #000000;-->
<!--        font-family: "Arial",sans-serif;-->
<!--        font-size: 18.7px;-->
<!--        font-style: normal;-->
<!--        font-variant: normal;-->
<!--        font-weight: 700;-->
<!--        line-height: 23px;-->
<!--        text-decoration: none; text-align:justify;-->
<!--        vertical-align: 0;'>--><?php //echo $oDetailPlat->description_tarif; ?><!--</div>-->
<!--                <div>--><?php //echo $oDetailPlat->conditions_promo; ?><!--</div>-->
<!--            </div>-->
<!--            <div style="text-align:center;">-->
<!--                --><?php
//                if ($oDetailPlat->reservation_enligne != "") {
//                    $reservation_enligne_agenda = $oDetailPlat->reservation_enligne;
//                    ?><!--<a href="--><?php //echo $reservation_enligne_agenda; ?><!--" target="_blank"><img-->
<!--                        src="--><?php //echo GetImagePath("privicarte/"); ?><!--/reservation_online.png" alt="reservation"/>-->
<!--                    </a>--><?php
//                }
//                ?>
<!--            </div>-->
<!--        </div>-->
<!---->
<!--    --><?php //} ?>


<!--    <div class="col-lg-12 title_categ_black" style="margin-top:0;">CONTACT & INFORMATIONS</div>-->
<!---->
<!--    --><?php
//    if (isset($oDetailPlat->organiser_id) && $oDetailPlat->organiser_id != "0") {
//        $obj_organiser_article_details = $this->mdl_article_organiser->getById($oDetailPlat->organiser_id);
//        if (isset($obj_organiser_article_details) && is_object($obj_organiser_article_details)) {
//            ?>
<!--            <div class="article_case_contact_infos" style="border: 1px solid; margin: 0 auto; text-align: center; width: 500px; display: table;">Ev&eacute;nement organis&eacute; par-->
<!--                --><?php
//                echo $obj_organiser_article_details->name . "<br/>";
//                echo $obj_organiser_article_details->postal_code . " - ";
//                if (isset($obj_organiser_article_details->address1) && $obj_organiser_article_details->address1 != "") echo $obj_organiser_article_details->address1 . " - ";
//                if (isset($obj_organiser_article_details->address2) && $obj_organiser_article_details->address2 != "") echo $obj_organiser_article_details->address2 . " - ";
//                if (isset($obj_organiser_article_details->ville_id) && $obj_organiser_article_details->ville_id != "0") echo $this->mdlville->getVilleById($obj_organiser_article_details->ville_id)->Nom;
//                if (isset($obj_organiser_article_details->tel) && $obj_organiser_article_details->tel != "") echo "<br/>Tel : " . $obj_organiser_article_details->tel;
//                if (isset($obj_organiser_article_details->mobile) && $obj_organiser_article_details->mobile != "") echo "<br/>Mobile : " . $obj_organiser_article_details->mobile;
//                if (isset($obj_organiser_article_details->website) && $obj_organiser_article_details->website != "") $organiser_website_page = $obj_organiser_article_details->website; else $organiser_website_page = "";//echo "<br/>Site Web : " . $obj_organiser_article_details->website;
//                if (isset($obj_organiser_article_details->facebook) && $obj_organiser_article_details->facebook != "") $organiser_facebook_page = $obj_organiser_article_details->facebook; else $organiser_facebook_page = "";//echo "<br/>Facebook : " . $obj_organiser_article_details->facebook;
//                if (isset($obj_organiser_article_details->twitter) && $obj_organiser_article_details->twitter != "") $organiser_twitter_page = $obj_organiser_article_details->twitter; else $organiser_twitter_page = "";//echo "<br/>Twitter : " . $obj_organiser_article_details->twitter;
//                if (isset($obj_organiser_article_details->googleplus) && $obj_organiser_article_details->googleplus != "") $organiser_googleplus_page = $obj_organiser_article_details->googleplus; else $organiser_googleplus_page = "";//echo "<br/>Google+ : " . $obj_organiser_article_details->googleplus;
//                ?>
<!--            </div>-->
<!--            --><?php
//        }
//    }
//    ?>

    <div class="col-lg-12 padding0">

        <div class="col-lg-12 padding0" style="text-align:center; padding-top:20px !important;">

            <ul class="contact_info_article_list">


                <?php if (isset($oDetailPlat->pdf) && $oDetailPlat->pdf != "") { ?>
                    <li>
                        <a href="<?php echo base_url() . "/application/resources/front/images/plat/pdf/" . $oDetailPlat->pdf; ?>"
                           target="_blank" title="<?php echo $oDetailPlat->titre_pdf; ?>"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/ico_pdf.png" alt="pdf"/></a>
                    </li>
                <?php } ?>


                <li>
                    <a href="javascript:void(0);" id="IdContactPartnerForm" title="Contact" data-toggle="modal" data-target="#divContactPartnerForm"><img
                            src="<?php echo GetImagePath("privicarte/"); ?>/ico_msg.png"/></a>
                </li>


                <?php
                if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                } else {
                    ?>
                    <?php
                    if (isset($organiser_facebook_page) AND $organiser_facebook_page == "" && isset($oDetailPlat->facebook) && $oDetailPlat->facebook != "") $organiser_facebook_page = $oDetailPlat->facebook;
                    if (isset($organiser_facebook_page) && $organiser_facebook_page != "" && $organiser_facebook_page != NULL) { ?>
                        <?php
                        if (preg_match('#https://#', $organiser_facebook_page) || preg_match('#http://#', $organiser_facebook_page)) {
                            $link_fb_to_show = $organiser_facebook_page;
                        } else {
                            $link_fb_to_show = "https://www.facebook.com/" . $organiser_facebook_page;
                        }
                        ?>
                        <li>
                            <a href="javascript:void(0);"
                               onclick='javascript:window.open("<?php echo $link_fb_to_show; ?>", "Facebook", "width=850, height=800");'
                               title="Page Facebook"><img src="<?php echo GetImagePath("privicarte/"); ?>/ico_fb.png"/></a>
                        </li>
                    <?php } ?>
                <?php } ?>



                <?php
                if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                } else {
                    if (isset($organiser_googleplus_page) && $organiser_googleplus_page != "" && $organiser_googleplus_page != NULL) { ?>
                        <?php
                        if (preg_match('#https://#', $organiser_googleplus_page) || preg_match('#http://#', $organiser_googleplus_page)) {
                            $link_gp_to_show = $organiser_googleplus_page;
                        } else {
                            $link_gp_to_show = "https://plus.google.com/" . $organiser_googleplus_page;
                        }
                        ?>
                        <li>
                            <a href="javascript:void(0);"
                               onclick='javascript:window.open("<?php echo $link_gp_to_show; ?>", "GooglePlus", "width=850, height=800");'
                               title="Page GooglePlus"><img width="95px" height="110px" src="<?php echo GetImagePath("privicarte/"); ?>/footer_link_gplus.png"/></a>
                        </li>
                    <?php } ?>
                <?php } ?>




                <?php
                if (isset($iframe_session_navigation) && $iframe_session_navigation == "1") {
                } else {
                    if (isset($organiser_twitter_page) AND $organiser_twitter_page == "" && isset($oInfoCommercant) && $oInfoCommercant->google_plus != "" && $oInfoCommercant->google_plus != NULL) $organiser_twitter_page = $oInfoCommercant->google_plus;
                    if (isset($organiser_twitter_page) AND $organiser_twitter_page != "" && $organiser_twitter_page != NULL) {
                        if (preg_match('#https://#', $organiser_twitter_page) || preg_match('#http://#', $organiser_twitter_page)) {
                            $link_google_plus_to_show = $organiser_twitter_page;
                        } else {
                            $link_google_plus_to_show = "http://www.twitter.com/" . $organiser_twitter_page;
                        }
                        ?>
                        <li>
                            <a href="javascript:void(0);"
                               onclick='javascript:window.open("<?php echo $link_google_plus_to_show; ?>", "Facebook", "width=850, height=800");'
                               title="Page Twitter"><img src="<?php echo GetImagePath("privicarte/"); ?>/ico_twt.png"/></a>
                        </li>
                    <?php } ?>
                <?php } ?>


                <?php
                if (isset($organiser_website_page) AND $organiser_website_page == "" && isset($oDetailPlat->siteweb) && $oDetailPlat->siteweb != "") $organiser_website_page = $oDetailPlat->siteweb;
                else $organiser_website_page = "javascript:void(0);"; ?>
                <?php if (isset($organiser_website_page) && $organiser_website_page != '' && $organiser_website_page != null && $organiser_website_page != 'http://www.') { ?>
                    <li>
                        <a href="<?php echo $organiser_website_page; ?>" title="Site web" target="_blank"><img
                                src="<?php echo GetImagePath("privicarte/"); ?>/ico_web.png"/></a>
                    </li>
                <?php } ?>




                <?php
                $this->load->model("user");
                $thisss =& get_instance();
                $thisss->load->library('ion_auth');
                $this->load->model("ion_auth_used_by_club");
                $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oDetailPlat->IdCommercant);
                if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
                if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;


                if ($thisss->ion_auth->logged_in()) {
                    $user_ion_auth = $thisss->ion_auth->user()->row();
                    $iduser = $this->ion_auth_used_by_club->get_user_id_from_ion_id($user_ion_auth->id);
                    if ($iduser == null || $iduser == 0 || $iduser == "") {
                        $iduser = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
                    }
                } else $iduser = 0;
                if (isset($iduser) && $iduser != 0 && $iduser != NULL && $iduser != "") {
                    $oCommercantFavoris = $this->user->verify_favoris($iduser, $oDetailPlat->IdCommercant);
                }

                ?>




                <li>
                    <?php if (isset($oCommercantFavoris) && $oCommercantFavoris != NULL && $oCommercantFavoris->Favoris == "1") { ?>
                        <a href="<?php echo site_url('front/utilisateur/delete_favoris/' . $oDetailPlat->IdCommercant); ?>" title="Supprimer de mes Favoris">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/ico_favoris.png" />
                        </a>
                    <?php } else { ?>
                        <a href="<?php echo site_url('front/utilisateur/ajout_favoris/' . $oDetailPlat->IdCommercant); ?>" title="Ajouter à mes Favoris">
                            <img src="<?php echo GetImagePath("privicarte/"); ?>/ico_favoris.png" />
                        </a>
                    <?php } ?>
                </li>




            </ul>

        </div>

    </div>

<!--    --><?php
//    $oVilleCommercant = $this->mdlville->getVilleById($oCommercant->IdVille);
//    var_dump($oVilleCommercant);die();
//    ?>
<!--    --><?php //if (isset($oDetailPlat->activ_fb_comment) && $oDetailPlat->activ_fb_comment == '1') { ?>
<!--        <div class="col-lg-12 padding0" style="background-color: #fff;">-->
<!--            <div class="fb-comments" data-href="--><?php //echo site_url("/plat_du_jour/details/" . $oDetailPlat->id); ?><!--"-->
<!--                 data-width="800" data-numposts="5"></div>-->
<!--        </div>-->
<!--    --><?php //} ?>


    <div style="margin:20px; display:none">
        <!--<strong>Organisateur :</strong><br/> <?php // echo $oDetailPlat->organisateur ; ?> de <?php // echo $oDetailPlat->ville ; ?><br/>
<?php // echo $oDetailPlat->adresse_localisation ; ?> <?php // echo $oDetailPlat->codepostal_localisation ; ?>
<?php // if ($oDetailPlat->telephone!="") echo "<br/>Tél. ".$oDetailPlat->telephone ; ?>
<?php // if ($oDetailPlat->mobile!="") echo "<br/>Mobile. ".$oDetailPlat->mobile ; ?>
<?php // if ($oDetailPlat->fax!="") echo "<br/>Fax. ".$oDetailPlat->fax ; ?>-->

        <?php echo "<br/><strong>Partenaire</strong> : " . $oCommercant->NomSociete; ?>
        <?php
        $this->load->model("mdlville");
        $oVilleCommercant = $this->mdlville->getVilleById($oCommercant->IdVille);
        //var_dump($oVilleCommercant);die();
        if (isset($oVilleCommercant)) echo "<br/>" . $oVilleCommercant->Nom;
        echo " " . $oVilleCommercant->CodePostal . "<br/>";
        if (isset($oCommercant->TelFixe)) echo " Tel." . $oCommercant->TelFixe;
        if (isset($oCommercant->TelMobile)) echo " Mobile." . $oCommercant->TelMobile;
        ?>
    </div>

    <div style='font-family: "Arial",sans-serif; display:none;
    font-size: 12px; margin-bottom:10px; text-align:center;
    font-weight: 700; margin-top:30px;
    line-height: 1.25em;'>Vous avez une question particulière à nous poser, adressez nous un mail express
    </div>

    <div style="margin-bottom:10px; margin-top:10px; display:none;">
            <script type="text/javascript" src="<?php echo GetJsPath("front/"); ?>/fields.check.js"></script>
            <script type="application/javascript">
                $(document).ready(function () {
                    $("#btn_submit_form_module_detailbonnplan").click(function () {
                        //alert('test form submit');
                        txtErrorform = "";

                        var txtError_text_mail_form_module_detailbonnplan = "";
                        var text_mail_form_module_detailbonnplan = $("#text_mail_form_module_detailbonnplan").val();
                        if (text_mail_form_module_detailbonnplan == "") {
                            //$("#divErrorform_module_detailbonnplan").html('<font color="#FF0000">Veuillez saisir votre demande</font>');
                            txtErrorform += "1";
                            $("#text_mail_form_module_detailbonnplan").css('border-color', 'red');
                            $("#text_mail_form_module_detailbonnplan").focus();
                        } else {
                            $("#text_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
                        }

                        var nom_mail_form_module_detailbonnplan = $("#nom_mail_form_module_detailbonnplan").val();
                        if (nom_mail_form_module_detailbonnplan == "") {
                            txtErrorform += "- Veuillez indiquer Votre nom_mail_form_module_detailbonnplan <br/>";
                            $("#nom_mail_form_module_detailbonnplan").css('border-color', 'red');
                            $("#nom_mail_form_module_detailbonnplan").focus();
                        } else {
                            $("#nom_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
                        }

                        var email_mail_form_module_detailbonnplan = $("#email_mail_form_module_detailbonnplan").val();
                        if (email_mail_form_module_detailbonnplan == "" || !isEmail(email_mail_form_module_detailbonnplan)) {
                            txtErrorform += "- Veuillez indiquer Votre email_mail_form_module_detailbonnplan <br/>";
                            //alert("Veuillez indiquer Votre nom");
                            $("#email_mail_form_module_detailbonnplan").css('border-color', 'red');
                            $("#email_mail_form_module_detailbonnplan").focus();
                        } else {
                            $("#email_mail_form_module_detailbonnplan").css('border-color', '#E3E1E2');
                        }


                        if (txtErrorform == "") {
                            $("#form_module_detailbonnplan").submit();
                        }
                    });


                });
            </script>
            <?php if (isset($user_ion_auth)) { ?>
                <style type="text/css">
                    .inputhidder {
                        visibility: hidden;
                    }
                </style>
            <?php } ?>

            <table border="0" align="center" style="text-align:center; width:100%;">
                <tr>
                    <td style="text-align:left;"><img
                            src="<?php echo GetImagePath("front/"); ?>/btn_new/info_annonce_img.png" alt="img"
                            width="250"></td>
                    <td>
                        <form method="post" name="form_module_detailbonnplan" id="form_module_detailbonnplan" action=""
                              enctype="multipart/form-data">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr class="inputhidder">
                                    <td>Votre Nom</td>
                                    <td><input id="nom_mail_form_module_detailbonnplan"
                                               name="nom_mail_form_module_detailbonnplan" type="text"
                                               value="<?php if (isset($user_ion_auth) && $user_ion_auth->first_name != "") echo $user_ion_auth->first_name; ?>">
                                    </td>
                                </tr>
                                <tr class="inputhidder">
                                    <td>Votre Téléphone</td>
                                    <td><input id="tel_mail_form_module_detailbonnplan"
                                               name="tel_mail_form_module_detailbonnplan" type="text"
                                               value="<?php if (isset($user_ion_auth) && $user_ion_auth->phone != "") echo $user_ion_auth->phone; ?>">
                                    </td>
                                </tr>
                                <tr class="inputhidder">
                                    <td>Votre Email</td>
                                    <td><input id="email_mail_form_module_detailbonnplan"
                                               name="email_mail_form_module_detailbonnplan" type="text"
                                               value="<?php if (isset($user_ion_auth) && $user_ion_auth->email != "") echo $user_ion_auth->email; ?>">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <textarea id="text_mail_form_module_detailbonnplan" cols="28" rows="7"
                                                  name="text_mail_form_module_detailbonnplan"
                                                  style="width:246px; height:130px;font-family:Arial, Helvetica, sans-serif; font-size:10px;"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input id="btn_reset_form_module_detailbonnplan" type="reset" value="Effacer"
                                               style="width:82px; height:22px;">
                                        <input id="btn_submit_form_module_detailbonnplan" type="button"
                                               name="btn_submit_form_module_detailbonnplan" value="Envoyer"
                                               style="width:90px; height:22px;">
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
            <br/>
    </div>


</div>