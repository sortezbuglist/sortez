<?php if (!isset($data)) $data['data_init'] = true; ?>

<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>

<div class="paddingright0 pb-3 w-100" style="display:table;">
    <div id="id_mainbody_main" class="col-lg-12">

        <?php } ?>

        <?php $ij = 0; ?>
        <?php
        $thisss =& get_instance();
        $thisss->load->library('session');
        $thisss->load->library('generate_thumb');
        $main_width_device = $thisss->session->userdata('main_width_device');
        $main_tablet_screen = false;
        if (isset($main_width_device) && $main_width_device != "" && $main_width_device != "0" && is_numeric($main_width_device) && floatval($main_width_device)>=991 && floatval($main_width_device)<=1199){
            $main_tablet_screen = true;
        } else {
            $main_tablet_screen = false;
        }
        ?>

        <div class="row w-100 d-inline-block m-0">
            <?php foreach ($alldata as $oListdealsfidelity){ ?>
            
            <?php
            $thisss =& get_instance();
            $thisss->load->model("ion_auth_used_by_club");
            $ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oListdealsfidelity['idcom']);

            $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/" . $ionauth_id . "/";
            $photoCommercant_path_thumb = "application/resources/front/photoCommercant/imagesbank/" . $ionauth_id . "/";
            $photoCommercant_path_old = "application/resources/front/images/";
            if(isset($oListdealsfidelity['type']) && $oListdealsfidelity['type']=='remise') {
                $photoCommercant_path = $photoCommercant_path."fidelity_photo/";
            } else if(isset($oListdealsfidelity['type']) && $oListdealsfidelity['type']=='tampon') {
                $photoCommercant_path = $photoCommercant_path."fidelity_photo/";
            } else if(isset($oListdealsfidelity['type']) && $oListdealsfidelity['type']=='capital') {
                $photoCommercant_path = $photoCommercant_path."fidelity_photo/";
            }

            $deals_main_details_url = "";
            if(isset($oListdealsfidelity['type']) && $oListdealsfidelity['type']=='annonce') {
                $deals_main_details_url = site_url("soutenons/Annonce/detailAnnonce/" . $oListdealsfidelity['id']);
            } else if(isset($oListdealsfidelity['type']) && $oListdealsfidelity['type']=='remise') {
                $deals_main_details_url = site_url($oListdealsfidelity['partenaire_url']."/notre_fidelisation_commercants/remise/" . $oListdealsfidelity['id']);
            } else if(isset($oListdealsfidelity['type']) && $oListdealsfidelity['type']=='tampon') {
                $deals_main_details_url = site_url($oListdealsfidelity['partenaire_url']."/notre_fidelisation_commercants/tampon/" . $oListdealsfidelity['id']);
            } else if(isset($oListdealsfidelity['type']) && $oListdealsfidelity['type']=='capital') {
                $deals_main_details_url = site_url($oListdealsfidelity['partenaire_url']."/notre_fidelisation_commercants/capital/" . $oListdealsfidelity['id']);
            } else if(isset($oListdealsfidelity['type']) && ($oListdealsfidelity['type']=='1' || $oListdealsfidelity['type']=='2' || $oListdealsfidelity['type']=='3')) {
                $deals_main_details_url = site_url($oListdealsfidelity['partenaire_url']."/notre_bonplan_commercants/" . $oListdealsfidelity['id']);
            }

            ?>

            <div class="col-card-sortez p-2 line_number_top_<?= $ij ?> <?= $main_tablet_screen;?> <?= $main_width_device;?>">

                <div id="item_line_<?php echo $oListdealsfidelity['id']; ?>" class="annonce_item_line  col-12 p-0 pb-3" style="height: auto!important;">

                    <div class="col-sm-12 p-0 annonce_list_img" id="item_list_img_<?php echo $oListdealsfidelity['id']; ?>" style="min-height: 175px!important;height: 204px!important;overflow: hidden!important">

                        <?php
                        //echo base_url().$photoCommercant_path.$image_home_vignette;
                        $image_home_vignette = "";
                        if (isset($oListdealsfidelity['image1']) && $oListdealsfidelity['image1'] != "" && is_file($photoCommercant_path . $oListdealsfidelity['image1']) == true) {
                            $image_home_vignette = $oListdealsfidelity['image1'];
                        }  else if ($image_home_vignette == "" && isset($oListdealsfidelity['image1']) && $oListdealsfidelity['image1'] != "" && is_file($photoCommercant_path_old . $oListdealsfidelity['image1']) == true) {
                            $image_home_vignette = $oListdealsfidelity['image1'];
                        }
                        //var_dump($image_home_vignette);
                        ?>


                        <a href='<?php echo $deals_main_details_url; ?>' target="_blank" title="<?php echo $oListdealsfidelity['titre']; ?>">
                            <?php
                            if ($image_home_vignette != "") {
                                if(isset($image_home_vignette)&& $image_home_vignette != null && is_file($photoCommercant_path_thumb.'thumbs/thumbnail_'.$image_home_vignette) == true){
                                    echo '<img src="' .base_url(). $photoCommercant_path_thumb.'thumbs/thumbnail_'.$image_home_vignette . '" width="100%"/>';
                                }else{
                                    if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path . $image_home_vignette) == true){
                                        $updir = $photoCommercant_path;
                                    }else{
                                        $updir = $photoCommercant_path_old;
                                    }
                                    $img = $thisss->generate_thumb->makeThumbnails($updir,$image_home_vignette,$photoCommercant_path_thumb);
                                    echo '<img src="' .base_url().$img . '" width="100%"/>';
                                }
                            } else {
                                $image_home_vignette_to_show = GetImagePath("front/") . "/no_image_boutique.png";
                                echo '<img src="' . $image_home_vignette_to_show . '" width="100%" height="246" border="0" id="pic_965" name="pic_965" title="" alt=""/>';
                            }
                            ?>
                        </a>
                        <div
                            style="font-family: Futura-LT-Book,Sans-serif;font-size:16px;position:absolute; bottom:0; width:100%; height:40px; background:#292b2c; color:#FFFFFF; font-weight: normal; text-transform:uppercase; text-align:center; line-height:40px;"><?php if ($oListdealsfidelity['type'] != "1"&& $oListdealsfidelity['type'] != "2" && $oListdealsfidelity['type'] != "3") {echo "Offre ".$oListdealsfidelity['type'];}elseif($oListdealsfidelity['type'] == "1"){echo "Offre bonplan Simple";}elseif($oListdealsfidelity['type'] == "2"){echo "Offre bonplan unique";}elseif($oListdealsfidelity['type'] == "3"){echo "Offre bonplan multiple";} ?></div>
                    </div>
                    <div class="col-12 p-0 pt-2" style='
                                                    color:#E80EAE;
                                                    font-family: Libre-Baskerville,serif;
                                                    font-size: 17px;
                                                    font-style: italic;
                                                    font-variant: normal;
                                                    font-weight: normal;
                                                    line-height: 25px;
                                                    text-decoration: none;
                                                    text-align:center; height:30px; overflow:hidden;
                                                    vertical-align: 0;
                    '><?php if (isset($oListdealsfidelity['partenaire'])) echo $oListdealsfidelity['partenaire']; ?></div>
                    <div class="p_annonce_titre_text col-12 p-0" style='
                                                                        font-variant-ligatures: normal;
                                                                        font-variant-caps: normal;
                                                                        font-variant-numeric: normal;
                                                                        font-variant-east-asian: normal;
                                                                        font-stretch: normal;
                                                                        height: 20px;
                                                                        font-family:Futura-LT-Book,sans-serif;
                                                                        overflow: hidden;
                                                                        text-align: center;
                                                                        font-size: 15px;
                                                                        color: black;
                    '>
                        <?php
                            $apercu_description = $oListdealsfidelity['ville_nom'];
                            echo $apercu_description;
                        ?></div>
                    <div class="p_annonce_titre_text col-12 p-0" style='
                                                                        font-variant-ligatures: normal;
                                                                        font-variant-caps: normal;
                                                                        font-variant-numeric: normal;
                                                                        font-variant-east-asian: normal;
                                                                        font-stretch: normal;
                                                                        height: 20px;
                                                                        font-family:Futura-LT-Book,sans-serif;
                                                                        overflow: hidden;
                                                                        text-align: center;
                                                                        font-size: 15px;
                                                                        color: black;
                    '>
                        <?php
                        if (isset($oListdealsfidelity['date_fin']) && $oListdealsfidelity['date_fin']!=""){
                            $apercu_description = $oListdealsfidelity['date_fin'];
                            echo "Expire le ".$apercu_description;
                        }
                        ?></div>
                    <div class="col-sm-12 text-center">
                        <?php if (isset($oListdealsfidelity['type']) AND $oListdealsfidelity['type'] == 'tampon' ){ ?>
                            <img class="img-fluid pt-3" src="<?php echo base_url(); ?>assets/images/coupdetampon.png">
                        <?php }elseif (isset($oListdealsfidelity['type']) AND $oListdealsfidelity['type'] == 'remise'){ ?>
                            <img class="img-fluid pt-3" src="<?php echo base_url(); ?>assets/images/20%.png">
                        <?php }elseif (isset($oListdealsfidelity['type']) AND $oListdealsfidelity['type'] == 'capital'){ ?>
                            <img class="img-fluid pt-3" src="<?php echo base_url(); ?>assets/images/capitalisation.png">
                        <?php }elseif(isset($oListdealsfidelity['type']) AND $oListdealsfidelity['type'] == '1'){ ?>
                            <img class="img-fluid pt-3" src="<?php echo base_url(); ?>assets/images/simple.png">
                        <?php }elseif(isset($oListdealsfidelity['type']) AND $oListdealsfidelity['type'] == '2'){ ?>
                            <img class="img-fluid pt-3" src="<?php echo base_url(); ?>assets/images/unique.png">
                        <?php }elseif(isset($oListdealsfidelity['type']) AND $oListdealsfidelity['type'] == '3'){ ?>
                            <img class="img-fluid pt-3" src="<?php echo base_url(); ?>assets/images/multiple.png">
                        <?php } ?>
                    </div>
                    <div class="col-sm-12 annonce_list_details" id="item_list_details_<?php echo $oListdealsfidelity['id']; ?>"
                         style="padding-top:10px; padding-bottom:10px; background-color:white!important;">

                        <div class="col-12 p-0" style='
    font-style: normal;
    font-variant-ligatures: normal;
    font-variant-caps: normal;
    font-family:Futura-LT-Book,sans-serif;    font-variant-numeric: normal;
    font-variant-east-asian: normal;
    font-weight: normal;
    font-stretch: normal;
    font-size: 15px;
    text-align: center;
'><?php if(isset($oListdealsfidelity['partenaire'])) echo $oListdealsfidelity['partenaire']; ?></div>
                        <div class="p_annonce_titre_text col-12 p-0" style='
    font-variant-ligatures: normal;
    font-variant-caps: normal;
    font-variant-numeric: normal;
    font-variant-east-asian: normal;
    font-stretch: normal;
    height: 67px;
    font-family:Futura-LT-Book,sans-serif;
    overflow: hidden;
    text-align: center;
    font-size: 15px;
    color: black;
'><?php
                            $apercu_description = $oListdealsfidelity['description'];
                            echo strip_tags($apercu_description);
                            ?>
                        </div>

                        <div class="annonce_list_info_btn padding0 col-xs-4 text-center" style="position: relative;bottom: 10px;left: 0px;margin-top: 10px;cursor: pointer;">
                        <a href='<?php echo $deals_main_details_url; ?>' target="_blank" title="<?php echo $oListdealsfidelity['titre']; ?>" style="text-decoration: unset">
                            <div class="btn_more">+</div>
                        </a>
                    </div>
                    <style type="text/css">
                        .btn_more{
                            width: 40px;
                            height: 40px;
                            background: #E80EAE;
                            border-radius: 25px;
                            color: white;
                            font-size: 25px;
                            border: solid .5px;
                            margin: auto;
                        }
                        .btn_more:hover{
                            background-color:#F2EDDF!important;
                            color: #59514D!important;
                            border-color: black!important;
                        }
                    </style>

                </div>


            </div>

        </div>



        <?php $ij = $ij + 1; ?>

        <?php } // end foreach $alldata ?>
    </div>

    <?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
    } else { ?>
</div></div>
<?php } ?>


<?php if (isset($_GET['content_only_list']) && $_GET['content_only_list'] == '1') {
} else { ?>
    <div class="container pb-3" style="display:table;">
        <script type="text/javascript">
            jQuery(document).ready(function() {
                var win = jQuery(window);
                //jQuery('#loading_article_adresses').hide();
                var PerPage_init = jQuery('#PerPage_article_adresses').val();
                var filter_array = [];
                var iiii = 0;

                // Each time the user scrolls
               $('#loading_article_adresses').click(function() {
                    // End of the document reached?

                    // if (jQuery(window).scrollTop() + jQuery(window).height() > jQuery(document).height() - 100) {
                        //if (jQuery(document).scrollTop() + jQuery(window).height() == getDocHeight()) {
                        jQuery('#loading_article_adresses').show();
                        var PerPage = jQuery('#PerPage_article_adresses').val();
                        var test_filtering = $.inArray(PerPage, filter_array); //alert(String(test_filtering)+' / '+String(iiii)+' / '+PerPage);
                        //alert(String(jQuery(window).scrollTop())+' / '+String(jQuery(window).height())+' / '+String(getDocHeight())+' / '+'<?php echo site_url("article/liste/");?>'+String(PerPage)+'?content_only_list=1');

                        if (test_filtering=='-1') {
                            filter_array[iiii] = PerPage;
                            jQuery.ajax({
                                url: '<?php echo site_url("soutenons/DealAndFidelite/index/");?>'+String(PerPage)+'/?content_only_list=1',
                                dataType: 'html',
                                success: function(html) {
                                    jQuery('#id_mainbody_main').append(html);
                                    // jQuery('#loading_article_adresses').hide();
                                    jQuery('#PerPage_article_adresses').val(parseInt(PerPage)+parseInt(PerPage_init));
                                }
                            });
                        } else {
                            //jQuery('#loading_article_adresses').hide();
                        }
                        iiii = iiii + 1;
                    // }
                });
            });
        </script>

        <div id="loading_article_adresses" class="" style="text-align:center;">
            <button class="btn btn-success">Voir plus</button>
        </div>
        <input id="PerPage_article_adresses" type="hidden" value="<?php if (isset($PerPage)) echo $PerPage; else echo "0"; ?>"/>

    </div>
<?php } ?>


