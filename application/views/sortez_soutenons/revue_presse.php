<?php $data["zTitle"] = 'Import Datatourisme'; ?>
<html>
<head>
    <title>Gestion de revue de presse</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('application/views/sortez_vsv/css/revue.css') ?>">
    <link rel="stylesheet" type="text/css"
          href="<?php echo base_url('application/views/sortez_vsv/bootstrap/css/bootstrap.css') ?>">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body onload="activ_api()">
<div class="container">
    <form name="formglobal" id="formglobal" method="post" enctype="multipart/form-data"
          action="<?php echo site_url('article/valid_url') ?>">

        <div class="row text-center">
            <div class="col-lg-12 text-center">
                <h2 class="titre">Importer les données d'un site web:</h2>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-3"><a class="btn btn-info" href="<?php echo site_url('front/utilisateur/contenupro') ?>">Retour
                    menu</a></div>
        </div>
        <div class="row" style="border-style: double;margin-top: 50px;border-color: dodgerblue">
            <div class="col-lg-6 text-right" style="padding-top: 5px;font-size: 20px;background-color: #70A8DB">Mode
                Url
            </div>
            <div class="col-lg-6 text-right" style="padding-top: 5px;background-color: #70A8DB">
                <label class="switch">
                    <input <?php if (isset($acturl) AND $acturl != "") {
                        echo 'checked';
                    } ?> onclick="activ_api()" type="checkbox" id="acturl" name="acturl">
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
        <div class="row" style="padding-bottom: 50px;background-color: #e6ecf2">
            <div class="col-lg-12 text-center">
                <label for="url">Entrer l'url qui contient des donnés JSON:</label>
                <input placeholder="Entrez l'url ici" id="url" type="text" value="<?php if (isset($url)) {
                    echo $url;
                } ?>" <?php if (isset($url)) {
                    echo "disabled";
                } ?> name="url" class="form-control">

            </div>
        </div>

        <div class="row text-center"
             style="margin-top50px;border-style: double;margin-top: 50px;border-color: dodgerblue">
            <div class="col-lg-7 text-right" style="padding-top: 5px;font-size: 20px;background-color: #70A8DB">Mode Api
                de parsehub
            </div>
            <div class="col-lg-5 text-right" style="background-color: #70A8DB;padding-top: 5px">
                <label class="switch">
                    <input onclick="activ_api()" type="checkbox" id="actapi" name="actapi">
                    <span class="slider round"></span>
                </label>
            </div>
        </div>
        <div class="row text-center" id="apis" style="padding-bottom: 50px;background-color:e6ecf2 ">
            <div class="col-lg-6">
                <label for="api_key">Clé api:</label>
                <input <?php if (isset($api_key)) {
                    echo "disabled";
                } ?> value="<?php if (isset($api_key)) {
                    echo $api_key;
                } ?>" type="text" name="api_key" id="api_key" class="form-control">
            </div>
            <div class="col-lg-6">
                <label for="token">Jeton de projet:</label>
                <input <?php if (isset($token)) {
                    echo "disabled";
                } ?> value="<?php if (isset($token)) {
                    echo $token;
                } ?>" type="text" name="token" id="token" class="form-control">
            </div>
            <input placeholder="parsehub: http://localhost/sortez7/article/get_api" id="url_api" type="hidden"
                   value="<?php echo site_url('article/get_api') ?>" name="url_api" class="form-control">
        </div>

        <div class="row text-center" id="prog" style="display: none;margin-bottom: 25px;text-align: center">
            <div class="col-lg-12 col-sm-12 text-center" style="display: block"><img class="img-fluid"
                                                                                     src="<?php echo base_url('assets/img/30.gif') ?>">
            </div>
        </div>
        <div class="row text-center"
             style="background-color:#70A8DB;padding-bottom: 5px;margin-top: 50px;padding-top:5px;font-size: 20px;border-style: double;border-color: dodgerblue">
            <div class="col-lg-12">Informations communes des articles</div>
        </div>
        <div class="row text-center" style="padding-bottom: 50px;background-color:e6ecf2 ">
            <div class="col-lg-4">
                <label for="code">Code postal:</label><br>
                <input id="code" <?php if (isset($codepostal) AND $codepostal != "") {
                    echo "disabled";
                } ?> type="text" required="required" class="form-control" name="code"
                       value="<?php if (isset($codepostal) AND $codepostal != "") {
                           echo $codepostal;
                       } ?>">

            </div>
            <div class="col-lg-4">
                <label for="ville">Commune:</label>
                <select disabled id="ville" required="required" class="form-control" name="villes">
                    <option>Saisissez un code postal</option>
                    <?php foreach ($allville as $ville) { ?>
                        <option <?php if (isset($aville) AND $aville == $ville->IdVille) {
                            echo "selected";
                        } ?> value="<?php echo $ville->IdVille; ?>"><?php echo $ville->ville_nom ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-lg-4">
                <label for="type">Type:</label><br>
                <select <?php if (isset($type) AND $type != '') {
                    echo "disabled";
                } ?> class="form-control" name="type" id="type">
                    <option <?php if (isset($type) AND $type == 'article') {
                        echo "selected";
                    } ?> value="article">Article
                    </option>
                    <option <?php if (isset($type) AND $type == "agenda") {
                        echo "selected";
                    } ?> value="agenda">Agenda
                    </option>
                </select>

            </div>
        </div>

        <div class="row" style="margin-top: 50px">
            <div class="col-lg-12 text-center">
            <span class="text-center">
                <?php if (isset($result_url) AND $result_url == "ok" OR (isset($result_api) AND $result_api == "ok")) {
                    echo "<span style='color: green'>Les paramètres sont valides:</span> $nbdate articles trouvés <a class='btn btn-info' href='revue_presse'>Reconfigurer</a>";
                } elseif (isset($result_url) AND $result_url == 'no' OR (isset($result_api) AND $result_api == 'no')) {
                    echo "<span style=' color: red;'>Paramètre non valide</span><a class='btn btn-info' href='revue_presse'>Reconfigurer</a>";
                } else {
                    echo "<span id='vals' class='btn btn-info' >Valider</span>";
                }
                ?>
            </span>
            </div>
        </div>
        <div class="row" style="padding-top: 50px">
            <div class="col-lg-3 text-right importer">
                <span id="import_datatourisme_go" <?php if (!isset($url) AND !isset($api_url)) {
                    echo "style='display:none";
                } ?> class="btn btn-success text-center">Proceder à l'import</span>
            </div>
            <div class="col-lg-2 text-right">
                <button onclick="reload_page()" id="cancel" style="display: none;margin-top: 30px;"
                        class="btn btn-danger text-center">Annuler
                </button>
            </div>
            <div class="col-lg-3 text-center text-left">
                <div id="pwidget">
                    <img style="display: none" id="go" class="img-fluid"
                         src="<?php echo base_url('assets/img/103.gif') ?>">
                    <div id="progressbar">
                        <div id="indicator"></div>
                    </div>
                    <span id="progressnum">0</span><span>%</span>
                </div>

            </div>
            <div class="col-lg-4 text-left">
                <div class="col-lg-12">Importé avec succès : <span id="item_sent_success_nb"
                                                                   style="color: #1dc116;"></span></div>
                <div class="col-lg-12">Déjà existant : <span id="item_sent_exist_nb" style="color: #FF0000;"></span>
                </div>
                <div class="col-lg-12">Non importé :<span id="item_sent_error_nb" style="color: #FF0000;"></span></div>
            </div>
        </div>
        <input type="submit" style="display: none;" id="subk">
    </form>
</div>
<?php $this->load->view("admin/includes/vwFooter2013"); ?>
<script type="text/javascript">
    function get_value_prog() {
        var test = document.getElementById("code").value;
        if (test != "") {
            document.getElementById('prog').style.display = "inline";
            document.getElementById("subk").click();
        } else {
            alert("Le code postal doit être renseigné");
        }


    }

    function activ_api() {
        if (document.getElementById("acturl").checked == false) {
            document.getElementById("actapi").checked = true;
            document.getElementById("api_key").removeAttribute("disabled");
            document.getElementById("token").removeAttribute("disabled");
            document.getElementById("url").setAttribute("disabled", "disabled");
        } else {
            if (document.getElementById("acturl").checked == true) {
                document.getElementById("actapi").checked = false;
                document.getElementById("api_key").setAttribute("disabled", "disabled");
                document.getElementById("token").setAttribute("disabled", "disabled");
                document.getElementById("url").removeAttribute("disabled");
            }
        }
    }

    var toFirstIdDatatourisme = parseInt(<?php if(isset($nbdate)) echo $nbdate; else echo "0";?>);
    var url = "<?php if (isset($api_url) AND $api_url != '' AND $api_url != null) {
        echo $api_url;
    } elseif (isset($url) AND $url != '' AND $url != null) {
        echo $url;
    }?>";
    var runned = "<?php if(isset($runned)) echo $runned;?>";
    var toLastIdDatatourisme = 0;
    var datatourisme_imported = 0;
    var datatourisme_exists = 0;
    var datatourisme_skipped = 0;
    var codepostal = "<?php if(isset($codepostal)) echo $codepostal?>";
    var ville = "<?php if(isset($aville)) echo $aville?>";
    var type = "<?php if(isset($type)) echo $type?>";
    var api_key = "<?php if(isset($api_key)) echo $api_key;?>";
    var token = "<?php if(isset($token)) echo $token;?>";

    jQuery(document).ready(function () {
        jQuery("#vals").click(function () {
            if (confirm("Voulez vous valider ces paramètres ?")) {
                get_value_prog();
            }
        });
    });
    jQuery(document).ready(function () {
        jQuery("#import_datatourisme_go").click(function () {
            if (confirm("Voulez vous copier l'ensemble des données ?")) {
                document.getElementById("go").style.display = "block";
                document.getElementById("cancel").style.display = "block";
                import_datatourisme_func(toFirstIdDatatourisme, toLastIdDatatourisme, url, runned, codepostal, ville, type);//21103 //toFirstIdDatatourisme
            }
        });
    });

    function import_datatourisme_func(firstId, lastId, url, runned, codepostal, ville, type) {
        jQuery.ajax({
            type: "POST",
            url: "<?php echo site_url("article/get_data"); ?>",
            data: 'toLastIdDatatourisme=' + (lastId) + '&url=' + url + '&isrunned=' + runned
            + '&codepostal=' + codepostal
            + '&ville=' + ville
            + '&type=' + type,
            dataType: "json",
            success: function (data) {
                console.log(data);
                if (Number.isInteger(data.datatourisme_imported)) datatourisme_imported = datatourisme_imported + parseInt(data.datatourisme_imported);
                if (Number.isInteger(data.datatourisme_exists)) datatourisme_exists = datatourisme_exists + parseInt(data.datatourisme_exists);
                if (Number.isInteger(data.datatourisme_skipped)) datatourisme_skipped = datatourisme_skipped + parseInt(data.datatourisme_skipped);
                jQuery("#item_sent_success_nb").html(datatourisme_imported);
                jQuery("#item_sent_error_nb").html(datatourisme_skipped);
                jQuery("#item_sent_exist_nb").html(datatourisme_exists);
                lastId++;
                var actualprogress = (lastId * 100) / firstId;  // valeur courante
                var progressnum = document.getElementById("progressnum");
                var indicator = document.getElementById("indicator");
                indicator.style.width = actualprogress + "%";
                progressnum.innerHTML = actualprogress;
                if (lastId < firstId) {
                    import_datatourisme_func(firstId, lastId, url, runned, codepostal, ville, type);
                }
                if (actualprogress == 100) {
                    alert("L'importation est terminé");
                    document.getElementById("go").style.display = "none";
                    document.getElementById("cancel").style.display = "none";

                }
            },
            error: function (data) {
                console.log(data);
                if (Number.isInteger(data.datatourisme_imported)) datatourisme_imported = datatourisme_imported + parseInt(data.datatourisme_imported);
                if (Number.isInteger(data.datatourisme_exists)) datatourisme_exists = datatourisme_exists + parseInt(data.datatourisme_exists);
                if (Number.isInteger(data.datatourisme_skipped)) datatourisme_skipped = datatourisme_skipped + parseInt(data.datatourisme_skipped);
                jQuery("#item_sent_success_nb").html(datatourisme_imported);
                jQuery("#item_sent_exist_nb").html(datatourisme_exists);
                jQuery("#item_sent_error_nb").html(datatourisme_skipped);
                lastId++;
                var actualprogress = (lastId * 100) / firstId;  // valeur courante
                var progressnum = document.getElementById("progressnum");
                var indicator = document.getElementById("indicator");
                indicator.style.width = actualprogress + "%";
                progressnum.innerHTML = actualprogress;
                if (actualprogress == 100) {
                    alert("L'importation est terminé");
                    document.getElementById("go").style.display = "none";
                    document.getElementById("cancel").style.display = "none";
                }
                if (firstId < lastId) {
                    import_datatourisme_func(firstId, lastId, url, runned, codepostal, ville, type);
                }
            }
        });
    }

    function reload_page() {
        document.location.href = "<?php echo site_url('article/revue_presse')?>"
    }
</script>
<style type="text/css">
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        display: none;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: green;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

    .titre {
        border: double;
        color: blue;
    }

    #pwidget {
        background-color: #31B0D5;
        width: 110px;
        padding: 2px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        border: 1px solid #31B0D5;
        text-align: center;
    }

    #progressbar {
        width: 100%;
        padding: 1px;
        background-color: white;
        border: 1px solid black;
        height: 28px;
        text-align: center;
    }

    #indicator {
        width: 0%;
        height: 28px;
        margin: 0;
        text-align: center;
        background-color: #449D44;
    }

    #progressnum {
        text-align: center;
        width: 100px;

    }

    body {
        background-image: url("http://localhost/sortez7/application/resources/privicarte/images/bg_all.jpg") !important;
        background-attachment: fixed;
    }

    @media screen and (min-width: 992px) {
        .container {
            background-color: white;
            height: auto;
            width: 800px;
        }
    }
</style>
</body>
</html>