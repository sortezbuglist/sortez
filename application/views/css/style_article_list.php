<style type="text/css">
    .oAgenda_subcateg {
        background-color: transparent;
        color: #000000;
        font-family: "Arial", sans-serif;
        font-size: 16px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 16px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }

    .oAgenda_nom_manifestation {
        background-color: transparent;
        color: #da1893;
        font-family: "Arial", sans-serif;
        font-size: 18px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }

    .oAgenda_date_debut {
        background-color: transparent;
        color: #000000;
        font-family: "Arial", sans-serif;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }

    .oAgenda_nom_localisation {
        background-color: transparent;
        color: #000000;
        font-family: "Arial", sans-serif;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }

    .oAgenda_ville {
        background-color: transparent;
        color: #000000;
        font-family: "Arial", sans-serif;
        font-size: 14px;
        font-style: normal;
        font-variant: normal;
        font-weight: normal;
        line-height: 20px;
        margin: 0 0 5px 0 !important;
        padding: 0 !important;
        text-decoration: none;
        vertical-align: 0;
    }
</style>