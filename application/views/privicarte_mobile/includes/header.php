<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title><?php if (isset($zTitle) && $zTitle != "") echo $zTitle; else echo "Privicarte";?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
$data['group_id_commercant_user'] = $group_id_commercant_user;
?>

<!-- Bootstrap core CSS -->
<link href="<?php echo base_url();?>application/resources/bootstrap-3.3.6-dist/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>application/resources/bootstrap-3.3.6-dist/css/bootstrap-theme.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<?php echo GetJsPath("privicarte/") ; ?>/jquery-1.11.3.min.js"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<!--<script type="text/javascript" src="<?php //echo GetJsPath("privicarte/") ; ?>/global.js"></script>-->

<?php //$this->load->view("privicarte/includes/global_js", $data); ?>

<?php $this->load->view("privicarte/includes/global_mobile_css", $data); ?>
<?php $this->load->view("privicarte/includes/global_css", $data); ?>

<link href="<?php echo GetCssPath("privicarte/") ; ?>/global_mobile.css" rel="stylesheet" type="text/css">
<link href="<?php echo GetCssPath("privicarte/") ; ?>/global.css" rel="stylesheet" type="text/css">

<style type="text/css">
.panel-heading {background-size:100% 80px;}
#accordion { color:#000000;}
@media (max-width: 479px){
#row_container_mobile { width:100% !important; margin: 0 !important;}
}
</style>

<?php if ($group_id_commercant_user == 5) { ?>

    <?php 
    $base_path_system_rand = str_replace('system/', '', BASEPATH);
    if (isset($oInfoCommercant->background_image) && $oInfoCommercant->background_image != "" && $oInfoCommercant->background_image != NULL && file_exists($base_path_system_rand."/application/resources/front/photoCommercant/images/".$oInfoCommercant->background_image) && $oInfoCommercant->bg_default_image != "1") { ?>
        <style type="text/css">
        body {
        background-image:url("<?php echo base_url()."application/resources/front/photoCommercant/images/".$oInfoCommercant->background_image; ?>") !important;
        background-repeat:no-repeat !important;
        background-attachment:fixed !important;
        background-size: 100% 100% !important;
        }
        </style>
    <?php } ?>
    

<?php } ?>

<meta name="google-translate-customization" content="ad114fb2f5d60b29-e2bf26e865a3d116-ga4de9321d692a0e0-29"></meta>

</head>


<body style="
<?php if ($group_id_commercant_user == 5 && isset($oInfoCommercant->bandeau_color) && $oInfoCommercant->bandeau_color != "") { ?>
background-color:<?php echo $oInfoCommercant->bandeau_color;?>;
background-image:  none !important;
<?php } else if ($group_id_commercant_user == 4) { ?>
background-color:#3653A2;
background-image:  none !important;
<?php } else { ?>
background-color:#3653A2;
background-image:  none !important;
<?php } ?>
color:#FFFFFF;
background-repeat:no-repeat !important; 
background-position: center bottom !important;
background-size: auto auto !important;
min-height:400px;">

<div class="row" id="row_container_mobile" style="margin: 0 auto !important;
<?php if ($group_id_commercant_user == 5 && isset($oInfoCommercant->bandeau_color) && $oInfoCommercant->bandeau_color != "") { ?>
background-color:<?php echo $oInfoCommercant->bandeau_color;?>;
background-image:  none !important;
<?php } else if ($group_id_commercant_user == 4) { ?>
background-color:#3653A2;
background-image:  none !important;
<?php } else { ?>
background-color:#3653A2;
background-image:  none !important;
<?php } ?>">

<?php $this->load->view("privicarte/includes/facebook_login_js", $data); ?>

