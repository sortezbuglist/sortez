<div class="col-xs-12 mobile_footer_menu">
    <?php $data['umpty'] = '';
    $this->load->view("privicarte_mobile/translation", $data); ?>

    <?php
    $thisss =& get_instance();
    $thisss->load->library('ion_auth');
    $this->load->model("ion_auth_used_by_club");
    $user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
    if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
    if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
    ?>

    <?php if (isset($group_id_commercant_user) && $group_id_commercant_user == 4) { ?>
        <style type="text/css">
            .mobile_footer_menu, .mobile_footer_menu ul > li, .mobile_footer_menu ul li > a {
                background-color: #3653A2 !important;
            }
        </style>
    <?php } ?>

    <ul class="list-group">


        <li class="list-group-item"><a href="<?php echo site_url($commercant_url_home . "/index"); ?>"
                                       style="text-decoration: underline;">Accueil</a></li>

        <?php if ($group_id_commercant_user == 4 || $group_id_commercant_user == 5) { ?>
            <!--<li class="list-group-item"><a href="<?php //echo site_url($commercant_url_menu_mobile);?>" style="text-decoration: underline;">Menu général</a></li>-->
        <?php } ?>

        <li class="list-group-item"><a href="<?php echo site_url($commercant_url_presentation); ?>"
                                       style="text-decoration: underline;">Présentation</a></li>

        <?php if ($group_id_commercant_user == 5) { ?>
            <?php if ($oInfoCommercant->labelactivite1 != "" && $oInfoCommercant->labelactivite1 != NULL) { ?>
                <li class="list-group-item"><a href="<?php echo site_url($commercant_url_infos); ?>"
                                               style="text-decoration: underline;"><?php echo $oInfoCommercant->labelactivite1; ?></a>
                </li>
            <?php } ?>
            <?php if ($oInfoCommercant->labelactivite2 != "" && $oInfoCommercant->labelactivite2 != NULL) { ?>
                <li class="list-group-item"><a href="<?php echo site_url($commercant_url_autresinfos); ?>"
                                               style="text-decoration: underline;"><?php echo $oInfoCommercant->labelactivite2; ?> </a>
                </li>
            <?php } ?>
        <?php } ?>

        <?php
        $thisss =& get_instance();
        $thisss->load->model('mdlville');
        if (isset($oInfoCommercant->IdVille_localisation) && $oInfoCommercant->IdVille_localisation != '') $oVille = $thisss->mdlville->getVilleById($oInfoCommercant->IdVille_localisation);
        else $oVille = 'Alpes maritimes';
        ?>

        <li class="list-group-item"><a
                    href='https://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $oInfoCommercant->adresse_localisation . ", " . $oInfoCommercant->codepostal_localisation . " &nbsp;" . $oVille->Nom; ?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $oInfoCommercant->adresse_localisation . ", " . $oInfoCommercant->codepostal_localisation . " &nbsp;" . $oVille->Nom; ?>&amp;t=m&amp;vpsrc=0'
                    style="text-decoration: underline;">Nous situer </a></li>
        <?php if (isset($oInfoCommercant->Video) && $oInfoCommercant->Video != "" && $oInfoCommercant->Video != NULL) { ?>
            <li class="list-group-item"><a href="<?php echo site_url($commercant_url_video); ?>"
                                           style="text-decoration: underline;">Vidéo</a></li>
        <?php } ?>
        <li class="list-group-item"><a href="<?php echo site_url($commercant_url_photos); ?>"
                                       style="text-decoration: underline;">Galerie photos</a></li>

        <?php if ($group_id_commercant_user == 5) { ?>
            <?php if ($nombre_annonce_com != 0) { ?>
                <li class="list-group-item"><a href="<?php echo site_url($commercant_url_annonces); ?>"
                                               style="text-decoration: underline;"><?php if (isset($oInfoCommercant->textmenuannonce) && $oInfoCommercant->textmenuannonce != "") echo $oInfoCommercant->textmenuannonce; else { ?>Notre boutique<?php } ?></a>
                </li>
            <?php } ?>
        <?php } ?>

        <?php if ($group_id_commercant_user == 4 || $group_id_commercant_user == 5) { ?>
            <?php if (isset($oLastbonplanCom)) { ?>
                <li class="list-group-item"><a href="<?php echo site_url($commercant_url_notre_bonplan); ?>"
                                               style="text-decoration: underline;">Nos bons plan</a></li>
            <?php } ?>
        <?php } ?>

        <?php if ($group_id_commercant_user == 4 || $group_id_commercant_user == 5) { ?>
            <?php if (isset($nombre_agenda_com) && $nombre_agenda_com != "0" && $group_id_commercant_user != 3) { ?>
                <li class="list-group-item"><a href="<?php echo site_url($commercant_url_agenda); ?>"
                                               style="text-decoration: underline;">Notre agenda</a></li>
            <?php } ?>
        <?php } ?>

        <li class="list-group-item"><a href="<?php echo site_url($commercant_url_nous_contacter); ?>"
                                       style="text-decoration: underline;">Formulaire de contact</a></li>
        <li class="list-group-item"><a href="https://www.sortez.org/" style="text-decoration: underline;">Sortez.org</a>
        </li>
        <!--<li class="list-group-item"><a href="https://www.creezvotresiteweb.fr/" style="text-decoration: underline;">creezvotresiteweb.fr</a></li>-->


    </ul>

</div>