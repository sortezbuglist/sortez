<?php $this->load->view("privicarte_mobile/includes/header.php", $data);?>
<link href="../../resources/privicarte/css/global.css" rel="stylesheet" type="text/css" />

<div class="row" style="text-align:center; padding:25px 0; width:100%; background-color:#000000; display:table;">
<div class="col-xs-4 padding0"><a href="javascript:void(0);"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_article.png" alt="mbl_menu_article" /></a></div>
<div class="col-xs-4 padding0"><a href="javascript:void(0);"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_fidelite.png" alt="mbl_menu_fidelite" /></a></div>
<div class="col-xs-4 padding0"><a href="<?php echo site_url("front/bonplan");?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_bonplan.png" alt="mbl_menu_bonplan" /></a></div>
<div class="col-xs-4 padding0"><a href="<?php echo site_url("front/annonce");?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_boutique.png" alt="mbl_menu_boutique" /></a></div>
<div class="col-xs-4 padding0"><a href="<?php echo site_url("front/annuaire");?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_annuaire.png" alt="mbl_menu_annuaire" /></a></div>
<div class="col-xs-4 padding0"><a href="<?php echo site_url("agenda");?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_agenda.png" alt="mbl_menu_agenda" /></a></div>
<div class="col-xs-4 padding0"><a href="<?php echo site_url("auth");?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_compte.png" alt="mbl_menu_compte" /></a></div>
<div class="col-xs-4 padding0"><a href="javascript:void(0);"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_carte.png" alt="mbl_menu_carte" /></a></div>
<div class="col-xs-4 padding0"><a href="<?php echo site_url("front/utilisateur/liste_favoris");?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_favoris.png" alt="mbl_menu_favoris" /></a></div>
<div class="col-xs-4 padding0"><a href="<?php echo site_url("contact");?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_recommander.png" alt="mbl_menu_recommander" /></a></div>
<div class="col-xs-4 padding0"><a href="<?php echo site_url("contact");?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_contact.png" alt="mbl_menu_contact" /></a></div>
<div class="col-xs-4 padding0"><a href="javascript:void(0);"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_infos.png" alt="mbl_menu_infos" /></a></div>
</div>
<div class="row" style="text-align:center; width:100%; background-color:#000000; display:table;">
<div class="col-lg-12 padding0" style="margin-top:25px;"><a href="<?php echo site_url("sommaire");?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_sommaire.png" alt="mbl_menu_sommaire" /></a></div>
<div class="col-lg-12 padding0"><a href="javascript:void(0);"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/mbl_menu_sortez.png" alt="mbl_menu_sortez" /></a></div>
</div>


<?php $this->load->view("privicarte_mobile/includes/footer.php", $data);?>