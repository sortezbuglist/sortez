<script type="text/javascript">
function compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id ; ?>()
{
	var compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id ; ?> = document.getElementById("compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id ; ?>");

	var date_actuelle = new Date();
	var date_evenement = new Date("<?php echo $oDetailAnnonce->annonce_date_fin ; ?>");
	var total_secondes = (date_evenement - date_actuelle) / 1000;

	var prefixe = "";
	if (total_secondes < 0)
	{
		prefixe = "Exp "; // On modifie le pr�fixe si la diff�rence est n�gatif

		total_secondes = Math.abs(total_secondes); // On ne garde que la valeur absolue

	}

	if (total_secondes > 0)
	{
		var jours = Math.floor(total_secondes / (60 * 60 * 24));
		var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));
		minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
		secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

		var et = "et";
		var mot_jour = "jours";
		var mot_heure = "heures,";
		var mot_minute = "minutes,";
		var mot_seconde = "secondes";

		if (jours == 0)
		{
			jours = '';
			mot_jour = '';
		}
		else if (jours == 1)
		{
			mot_jour = "jour,";
		}

		if (heures == 0)
		{
			heures = '';
			mot_heure = '';
		}
		else if (heures == 1)
		{
			mot_heure = "heure,";
		}

		if (minutes == 0)
		{
			minutes = '';
			mot_minute = '';
		}
		else if (minutes == 1)
		{
			mot_minute = "minute,";
		}

		if (secondes == 0)
		{
			secondes = '';
			mot_seconde = '';
			et = '';
		}
		else if (secondes == 1)
		{
			mot_seconde = "seconde";
		}

		if (minutes == 0 && heures == 0 && jours == 0)
		{
			et = "";
		}

		//compte_a_rebours.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ' ' + mot_heure + ' ' + minutes + ' ' + mot_minute + ' ' + et + ' ' + secondes + ' ' + mot_seconde;
		compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id ; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + '<br/>\n' + heures + ':' + minutes + ':' + secondes ;
	}
	else
	{
		compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id ; ?>.innerHTML = 'Toujours disponible !';
		//alert(date_actuelle+ " "+date_evenement);
	}

	var actualisation = setTimeout("compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id ; ?>();", 1000);
}
</script>


<div class="col-xs-12 padding0 content_mobile_detailsannonce">


  <div class="col-xs-12" style="vertical-align:middle; padding:15px;">
  	<div class="col-xs-2 padding0 detail_annonce_btn_back" style="text-align:left;"><a href="<?php echo site_url($commercant_url_annonces); ?>"><img alt="Retour" src="<?php echo base_url(); ?>application/resources/privicarte/images/mobile/mobile_return.png"></a></div>
  	<div class="col-xs-10 padding0 detail_annonce_text_back" style="margin-top: 25px; text-align:center; font-weight:bold;"><span style="margin-left:15px;">Retour vers le menu g&eacute;n&eacute;ral des annonces</span></div>
  </div>
  
  <div class="col-xs-12">
  <p><span class="font_bold">D&eacute;signation : </span><?php echo $oDetailAnnonce->texte_courte; ?></p>
<?php
$zEtat = "" ;
if ($oDetailAnnonce->etat == 0) $zEtat = "Revente" ;
else if ($oDetailAnnonce->etat == 1) $zEtat = "Neuf" ;
else if ($oDetailAnnonce->etat == 2) $zEtat = "Service" ;
?>
  <p><span class="font_bold">R&eacute;f&eacute;rence produit :</span></p>
  <p><span class="font_bold">Etat : </span><?php echo $zEtat; ?></p>
  <p><span class="font_bold">Annonce N&deg; : </span><?php echo ajoutZeroPourString($oDetailAnnonce->annonce_id, 6) ?> 
<?php if (convertDateWithSlashes($oDetailAnnonce->annonce_date_debut)!="00/00/0000") {?>
du <?php echo convertDateWithSlashes($oDetailAnnonce->annonce_date_debut) ; ?>
<?php }?></p>
  </div>
  
  <div class="col-xs-12" style="padding:15px;"><?php echo $oDetailAnnonce->texte_longue ; ?></div>
  
  
  <div class="col-xs-12 padding0">
  	<div class="col-xs-6 detail_annonce_livraison_img" style="text-align:center;">
		<?php if (isset($oDetailAnnonce->retrait_etablissement) && $oDetailAnnonce->retrait_etablissement == "1") {?>
        <img src="<?php echo GetImagePath("privicarte/");?>/mobile/livraison_domicile.png" id="retrait_etablissement_img"/>
        <?php } ?>
        <?php if (isset($oDetailAnnonce->livraison_domicile) && $oDetailAnnonce->livraison_domicile == "1") {?>
        <img src="<?php echo GetImagePath("privicarte/");?>/privicarte/livraison_domicile.png" id="livraison_domicilet_img"/>
        <?php } ?>
    </div>
    <div class="col-xs-6 detail_annonce_livraison_retrait" style="text-align:center; padding-top:30px;">
    
    	<p><img src="<?php echo GetImagePath("privicarte/");?>/time_bg_white.png" id="time_bg_white"/></p>
        <p style='background-color: transparent;
        color: #000000;
        font-family: "Arial",sans-serif;
        font-size: 30px;
        font-style: normal;
        font-variant: normal;
        font-weight: 700;
        line-height: 35px;
        text-decoration: none;
        vertical-align: 0;'>
        <span style="padding-top:5px;" id="compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id ; ?>"><noscript>Fin de l'�v�nement le 1er janvier 2017.</noscript></span>
        <script type="text/javascript">
            compte_a_rebours_<?php echo $oDetailAnnonce->annonce_id ; ?>();
        </script>
        </p>  
    
    </div>
  </div>
 
 
	<div class="col-xs-12" style="padding:15px;">
		<div class="col-xs-12" style="padding:15px; border:3px solid #000000; border-radius: 15px; text-align: center;">
        	<div class="col-xs-2 padding0 content_detailsannonce_image_liraison"><img src="<?php echo GetImagePath("privicarte/");?>/mobile/euro_bg_white.png" id="euro_bg_white"/></div>
            <div class="col-xs-5 padding0">
            
               <?php if (isset($oDetailAnnonce->ancien_prix) && $oDetailAnnonce->ancien_prix!=0 && $oDetailAnnonce->ancien_prix!="0") { ?>     
                    <p style='background-color: transparent;
                    color: #000000;
                    font-family: "Arial",sans-serif;
                    font-size: 16px;
                    font-style: normal;
                    font-variant: normal;
                    font-weight: normal;
                    line-height: 24px;
                    margin-bottom: 0;
                    text-align: center;
                    vertical-align: 0;'>Prix de vente</p>
                
                    <p style='background-color: transparent;
                    color: #000000;
                    font-family: "Arial",sans-serif;
                    font-size: 40px;
                    font-style: normal;
                    font-variant: normal;
                    font-weight: 700;
                    line-height: 25px;
                    margin: 10px 0 0 0 !important;
                    padding: 0px;
                    margin-bottom: 0;
                    text-align: center;
                    vertical-align: 0;'>
                        <?php echo $oDetailAnnonce->ancien_prix; ?>
                    </p>
                <?php }?>
                
            </div>
            <div class="col-xs-5 padding0">
			<?php if (isset($oDetailAnnonce->prix_vente) && $oDetailAnnonce->prix_vente!=0 && $oDetailAnnonce->prix_vente!="0") { ?>
                    <p style='background-color: transparent;
                    color: #000000;
                    font-family: "Arial",sans-serif;
                    font-size: 12px;
                    font-style: normal;
                    font-variant: normal;
                    font-weight: normal;
                    line-height: 24px;
                    margin-bottom: 0;
                    margin-top:10px;
                    text-align: center;
                    vertical-align: 0;'>Ancien prix</p>
                    
                    <p style='background-color: transparent;
                    color: #000000;
                    font-family: "Arial",sans-serif;
                    font-size: 24px;
                    font-style: normal;
                    font-variant: normal;
                    font-weight: 700;
                    padding: 0px;
                    line-height: 30px;
                    text-decoration:line-through;
                    vertical-align: 0;'><?php echo $oDetailAnnonce->prix_vente; ?>
                    </p>
             <?php } ?>  
            </div>
        </div>
    </div>
    
    
  
    <div class="col-xs-12">
    
    <?php if (isset($oDetailAnnonce->module_paypal) && $oDetailAnnonce->module_paypal == "1") {?>
    <table width="100%" border="0" style="text-align:center; margin:0 0 30px 0;">
      <tr>
        <td colspan="2"><img src="<?php echo GetImagePath("front/"); ?>/btn_new/wp6ac3e457_05_06.jpg" alt="" name="pcrv_54gfd43"  border="0" id="pcrv_54dfgh43"></td>
      </tr>
      <tr>
        <td><?php echo $oDetailAnnonce->module_paypal_btnpaypal ; ?></td>
      </tr>
      <tr>
        <td><?php echo $oDetailAnnonce->module_paypal_panierpaypal ; ?></td>
      </tr>
    </table>
    <?php } ?>
    
    </div>
    
    
</div>


<?php //var_dump($oDetailAnnonce);?>