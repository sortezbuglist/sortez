<style type="text/css">
<!--
.horaires_bold {
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
	font-weight: 700;
	font-variant: normal;
	color: #000000;
	text-decoration: none;
}
.horaires_normal {
	font-family: Arial, Helvetica, sans-serif;
	font-style: normal;
	font-weight: normal;
	font-variant: normal;
	color: #000000;
	text-decoration: none;
}
.horaires_normal a { color:#000000;} 
-->
</style>

<div class="col-xs-12 horaires_normal" style="background-color:#FFFFFF; text-align:center; padding:15px; color:#000000;">

<p class="horaires_bold" style="margin-bottom:20px;">
<span>
<?php echo $oInfoCommercant->NomSociete ; ?><br />
<?php echo $oInfoCommercant->Adresse1 ; ?><?php if ($oInfoCommercant->Adresse2!="") echo " - ".$oInfoCommercant->Adresse2 ; ?><br />
<?php if ($oInfoCommercant->CodePostal!="") echo $oInfoCommercant->CodePostal ; ?><?php if ($oInfoCommercant->ville != "") echo $oInfoCommercant->ville ; ?>
</span></p>

<?php if (isset($oInfoCommercant->TelFixe) && $oInfoCommercant->TelFixe != "") { ?>
<p><span class="horaires_bold"><strong>Tél :</strong></span><span> <?php echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelFixe).'">'.$oInfoCommercant->TelFixe.'</a>' ; ?></span></p>
<?php } ?>

<?php if (isset($oInfoCommercant->TelDirect) && $oInfoCommercant->TelDirect != "") { ?>
<p><span class="horaires_bold"><strong>Directe :</strong></span><span> <?php echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelDirect).'">'.$oInfoCommercant->TelDirect.'</a>' ; ?></span></p>
<?php } ?>

<?php if (isset($oInfoCommercant->TelMobile) && $oInfoCommercant->TelMobile != "") { ?>
<p><span class="horaires_bold"><strong>Mobile :</strong></span><span> <?php echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelMobile).'">'.$oInfoCommercant->TelMobile.'</a>' ; ?></span></p>
<?php } ?>

<p><span class="horaires_bold"><strong>Email :</strong></span><span> <?php echo '<a href="mailto:'.$oInfoCommercant->Email.'">'.$oInfoCommercant->Email.'</a>' ; ?></span></p>

<?php if (isset($oInfoCommercant->Horaires) && $oInfoCommercant->Horaires != "") { ?>
<p class="horaires_bold" style="margin-top:20px;"><span>Horaires :</span></p>
<p><span><?php echo $oInfoCommercant->Horaires ; ?> </span></p>
<?php } ?>

</div>
