<div class="col-xs-12 padding0" style="background-color: #FFF;padding:20px 15px; font-weight:bold; color:#000000; text-align:center;">

<div class="col-xs-12 padding0" id="info_partenaire">
	
	<?php foreach($toListeAnnonceParCommercant as $oListeAnnonceParCommercant){ ?> 
		<?php $zEtat = ($oListeAnnonceParCommercant->etat == 1) ? "Neuf" : "Revente" ; ?>
        
        <?php 
        $annonce_url_nom = RemoveExtendedChars2($oListeAnnonceParCommercant->texte_courte);
        $annonce_url_detail = $commercant_url_nom."_".$annonce_url_nom."/detail_annonce-".$oListeAnnonceParCommercant->annonce_id;
		$annonce_url_detail = site_url('front/annonce/detailAnnonce/'.$oListeAnnonceParCommercant->annonce_id);
        ?>
        
        <div class="col-xs-12" style="padding:15px;">
            <?php
            $thisss =& get_instance();
            $thisss->load->model("ion_auth_used_by_club");
            $ionauth_id = $thisss->ion_auth_used_by_club->get_ion_id_from_commercant_id($oListeAnnonceParCommercant->IdCommercant);

            $photoCommercant_path = "application/resources/front/photoCommercant/imagesbank/".$ionauth_id."/";
            $photoCommercant_path_old = "application/resources/front/images/";

            $image_home_vignette = "";
            if (isset($oListeAnnonceParCommercant->annonce_photo1) && $oListeAnnonceParCommercant->annonce_photo1 != "" && is_file($photoCommercant_path.$oListeAnnonceParCommercant->annonce_photo1)==true){ $image_home_vignette = $oListeAnnonceParCommercant->annonce_photo1;}
            else if ($image_home_vignette == "" && isset($oListeAnnonceParCommercant->annonce_photo2) && $oListeAnnonceParCommercant->annonce_photo2 != "" && is_file($photoCommercant_path.$oListeAnnonceParCommercant->annonce_photo2)==true){ $image_home_vignette = $oListeAnnonceParCommercant->annonce_photo2;}
            else if ($image_home_vignette == "" && isset($oListeAnnonceParCommercant->annonce_photo3) && $oListeAnnonceParCommercant->annonce_photo3 != "" && is_file($photoCommercant_path.$oListeAnnonceParCommercant->annonce_photo3)==true){ $image_home_vignette = $oListeAnnonceParCommercant->annonce_photo3;}
            else if ($image_home_vignette == "" && isset($oListeAnnonceParCommercant->annonce_photo4) && $oListeAnnonceParCommercant->annonce_photo4 != "" && is_file($photoCommercant_path.$oListeAnnonceParCommercant->annonce_photo4)==true){ $image_home_vignette = $oListeAnnonceParCommercant->annonce_photo4;}

            else if ($image_home_vignette == "" && isset($oListeAnnonceParCommercant->annonce_photo1) && $oListeAnnonceParCommercant->annonce_photo1 != "" && is_file($photoCommercant_path_old.$oListeAnnonceParCommercant->annonce_photo1)==true){ $image_home_vignette = $oListeAnnonceParCommercant->annonce_photo1;}
            else if ($image_home_vignette == "" && isset($oListeAnnonceParCommercant->annonce_photo2) && $oListeAnnonceParCommercant->annonce_photo2 != "" && is_file($photoCommercant_path_old.$oListeAnnonceParCommercant->annonce_photo2)==true){ $image_home_vignette = $oListeAnnonceParCommercant->annonce_photo2;}
            else if ($image_home_vignette == "" && isset($oListeAnnonceParCommercant->annonce_photo3) && $oListeAnnonceParCommercant->annonce_photo3 != "" && is_file($photoCommercant_path_old.$oListeAnnonceParCommercant->annonce_photo3)==true){ $image_home_vignette = $oListeAnnonceParCommercant->annonce_photo3;}
            else if ($image_home_vignette == "" && isset($oListeAnnonceParCommercant->annonce_photo4) && $oListeAnnonceParCommercant->annonce_photo4 != "" && is_file($photoCommercant_path_old.$oListeAnnonceParCommercant->annonce_photo4)==true){ $image_home_vignette = $oListeAnnonceParCommercant->annonce_photo4;}
            ?>
            <a href="<?php echo $annonce_url_detail;?>">
                <?php
                if ($image_home_vignette != ""){
                    if (isset($image_home_vignette) && $image_home_vignette != "" && is_file($photoCommercant_path.$image_home_vignette)==true)
                        echo '<img src="'.base_url().$photoCommercant_path.$image_home_vignette.'" width="100%"/>';
                    else echo '<img src="'.base_url().$photoCommercant_path_old.$image_home_vignette.'" width="100%"/>';
                } else {
                    $image_home_vignette_to_show = GetImagePath("front/") . "/no_image_boutique.png";
                    echo '<img src="'.$image_home_vignette_to_show.'" width="100%"/>';
                }
                ?>
            </a>

        </div>
            
            
        <div class="col-xs-12 padding0" style="margin-bottom:15px;">
                <div class="col-xs-9 div_content_txt_annonce_mobile_left" style="text-align:left;">
					<span style='text-align: justify;
    margin-bottom: 4.0px;
    line-height: 24.0px;
    font-family: "Arial", sans-serif;
    font-style: normal;
    font-weight: normal;
    color: #000000;
    background-color: transparent;
    font-variant: normal;
    font-size: 18px;
    vertical-align: 0;'><?php echo $oListeAnnonceParCommercant->texte_courte;//echo truncate($oListeAnnonceParCommercant->texte_courte, 35, " ..."); ?>
                    </span>
                    <div class="col-xs-12" style="text-align:left; padding-bottom:15px; padding-top:10px; padding-left:0; padding-right:0;">
                	<span style='    line-height: 22.50px;
    font-family: "Arial", sans-serif;
    font-style: normal;
    font-weight: 700;
    color: #000000;
    background-color: transparent;
    text-decoration: none;
    font-variant: normal;
    font-size: 18px;
    vertical-align: 0;'>Etat : </span><span style='line-height: 22.50px;
    font-family: "Arial", sans-serif;
    font-style: normal;
    font-weight: normal;
    color: #000000;
    background-color: transparent;
    text-decoration: none;
    font-variant: normal;
    font-size: 18px;
    vertical-align: 0;'><?php echo $zEtat ; ?></span><br/>
    
    <span style='    line-height: 22.50px;
    font-family: "Arial", sans-serif;
    font-style: normal;
    font-weight: 700;
    color: #000000;
    background-color: transparent;
    text-decoration: none;
    font-variant: normal;
    font-size: 18px;
    vertical-align: 0;'>Prix : </span>
    <span style='line-height: 29.70px;
    font-family: "Arial", sans-serif;
    font-style: normal;
    font-weight: 700;
    color: #000000;
    background-color: transparent;
    text-decoration: none;
    font-variant: normal;
    font-size: 22px;
    vertical-align: 0;'><?php echo $oListeAnnonceParCommercant->ancien_prix ; ?> &euro;</span>
                </div>
            	</div>
                <div class="col-xs-3 btn_plus_infos_annonce_mobile_right" style="padding-top:60px;">
                	<a href="<?php echo $annonce_url_detail;?>"><img src="<?php echo GetImagePath("privicarte/"); ?>/mobile/wp7510cf70_06.png" alt="details annonce" /></a>
                </div>
                
        </div>
    <?php //var_dump($oListeAnnonceParCommercant);?>

<?php } ?>
    
</div>
</div>