<div class="col-xs-12 padding0" style="margin-bottom:20px; margin-top:20px; display:table;">

    <form name="frmRecherchePartenaire" id="frmRecherchePartenaire" method="post"
          action="<?php echo site_url($commercant_url_home . "/article"); ?>">

        <div class="col-xs-6 padding0 divinputIdCommercant" style="text-align:left;">
            <input type="hidden" name="inputIdCommercant" id="inputIdCommercant"
                   value="<?php echo $oInfoCommercant->IdCommercant; ?>"/>

            <select id="inputStringHidden" style="background-color:#000000; color:#FFFFFF; font-size:16px;"
                    name="inputStringHidden" onChange="javascript:frmRecherchePartenaire.submit();">
                <option value="0">Recherche par catégorie</option>
                <?php foreach ($toCategorie_principale as $oCategorie_principale) { ?>
                    <option value=",<?php echo $oCategorie_principale->agenda_categid; ?>" <?php if (isset($iCategorieId) && $iCategorieId[0] == $oCategorie_principale->agenda_categid) echo "selected"; ?>>
                        <?php echo ucfirst(strtolower($oCategorie_principale->category)); ?>&nbsp;(<?php echo $oCategorie_principale->nb_article; ?>)
                    </option>
                <?php } ?>
            </select>

        </div>
        <div class="col-xs-6 padding0 divinputStringQuandHidden" style="text-align:right; ">
            <select id="inputStringQuandHidden" style="background-color:#000000; color:#FFFFFF; font-size:16px;"
                    name="inputStringQuandHidden" onChange="javascript:frmRecherchePartenaire.submit();">
                <option value="0">Tous les mois</option>
                <option value="01" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "01") echo "selected"; ?>>
                    Janvier <?php if (isset($toAgndaJanvier)) echo '(' . count($toAgndaJanvier) . ')'; ?></option>
                <option value="02" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "02") echo "selected"; ?>>
                    Février <?php if (isset($toAgndaFevrier)) echo '(' . count($toAgndaFevrier) . ')'; ?></option>
                <option value="03" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "03") echo "selected"; ?>>
                    Mars <?php if (isset($toAgndaMars)) echo '(' . count($toAgndaMars) . ')'; ?></option>
                <option value="04" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "04") echo "selected"; ?>>
                    Avril <?php if (isset($toAgndaAvril)) echo '(' . count($toAgndaAvril) . ')'; ?></option>
                <option value="05" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "05") echo "selected"; ?>>
                    Mai <?php if (isset($toAgndaMai)) echo '(' . count($toAgndaMai) . ')'; ?></option>
                <option value="06" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "06") echo "selected"; ?>>
                    Juin <?php if (isset($toAgndaJuin)) echo '(' . count($toAgndaJuin) . ')'; ?></option>
                <option value="07" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "07") echo "selected"; ?>>
                    Juillet <?php if (isset($toAgndaJuillet)) echo '(' . count($toAgndaJuillet) . ')'; ?></option>
                <option value="08" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "08") echo "selected"; ?>>
                    Août <?php if (isset($toAgndaAout)) echo '(' . count($toAgndaAout) . ')'; ?></option>
                <option value="09" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "09") echo "selected"; ?>>
                    Septembre <?php if (isset($toAgndaSept)) echo '(' . count($toAgndaSept) . ')'; ?></option>
                <option value="10" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "10") echo "selected"; ?>>
                    Octobre <?php if (isset($toAgndaOct)) echo '(' . count($toAgndaOct) . ')'; ?></option>
                <option value="11" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "11") echo "selected"; ?>>
                    Novembre <?php if (isset($toAgndaNov)) echo '(' . count($toAgndaNov) . ')'; ?></option>
                <option value="12" <?php if (isset($inputStringQuandHidden) && $inputStringQuandHidden == "12") echo "selected"; ?>>
                    Décembre <?php if (isset($toAgndaDec)) echo '(' . count($toAgndaDec) . ')'; ?></option>
            </select>
        </div>

    </form>
</div>
