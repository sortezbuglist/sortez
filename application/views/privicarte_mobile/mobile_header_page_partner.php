<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $zTitle; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap core CSS -->
<link href="<?php echo base_url();?>application/resources/bootstrap-3.3.6-dist/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>application/resources/bootstrap-3.3.6-dist/css/bootstrap-theme.css" rel="stylesheet" type="text/css">
<link href="<?php echo GetCssPath("privicarte/") ; ?>/global.css" rel="stylesheet" type="text/css">

<style type="text/css">
body{
	margin:0px;
	padding:0px;
	font-family: "Arial",sans-serif;
    font-size: 13px;
    line-height: 1.23em;
	/*background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp9e1de8e1_05_06.jpg);*/
	background-color: #e3e1e2;
}
img {
	border:none;
}
.contenerall {
	height: auto;
	width: 100%;
	margin-right: auto;
	margin-left: auto;
	background-color:#E3E1E2;
	display:table;
}
.contener_standard {
	float: left;
	width: 100%;
	position: relative;
	background-color:#003366;
	text-align:center;
}
.contener_img_mobile_head {
	float: left;
	width: 100%;
	position: relative;
	<?php if (isset($oInfoCommercant) && $oInfoCommercant->bandeau_top != ''  && $oInfoCommercant->bandeau_top != NULL) { ?>
	background-image: url(<?php echo base_url()."application/resources/front/photoCommercant/images/mobile_".$oInfoCommercant->bandeau_top; ?>);
	background-size: 100%;
	<?php } else { ?>
	background-image: url(<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp5dcf1817_06.png);
	background-size: 100%;
	<?php } ?>
	background-repeat: no-repeat;
	background-position: center top;
}
.contener_infos {
	/*float: left;
	width: 200px;
	position: relative;*/
}
.contener_logo {
	/*float: left;
	width: 100px;
	position: relative;*/
	text-align:center;
}
.contenerstandardwithmarge {
	float: left;
	width: 100%;
	padding-right: 15px;
	padding-left: 15px;
	position: relative;
	margin:15px 0;
}
.contener_infos_title {
	color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.21em;
}
.contener_infos_desc {
	color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em;
}
.contener_infos_adress {
	color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 11px;
    font-weight: 700;
    line-height: 1.27em;
}.contener_img_mobile_head_content {
	float: left;
	width: 100%;
	position: relative;
}
.contener_img_mobile_head_content_title {
	float: left;
	height: 40px;
	width: 100%;
	position: relative;
	color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;
	border: 3px solid #FFF;
	padding-top: 8px;
	padding-right: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
}
.contener_img_mobile_head_content_image {
	float: left;
	width: 100%;
	position: relative;
	text-align:center;
	background-color:#FFFFFF;
}
.contener_main {
	float: left;
	width: 100%;
	position: relative;
	/*margin-left:15px;
	margin-right:15px;*/
	margin-bottom:20px;
	margin-top:0px;
}
.contener_footer {
	float: left;
	width: 100%;
	position: relative;
	height: auto;
	text-align:center;
	margin-top:30px;
	margin-bottom:10px;
}
.contener_footer a {
	color:#000000;
}
.contener_footer a:hover {
	color: #F00;
}
.Button1, .Button1:visited {
    background-image: url("<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp58124efb_06.png");
    background-position: center 0;
    display: block;
    /*position: absolute;*/
    text-decoration: none;
	background-repeat:no-repeat;
	width:100%;height:35px; padding-top:8px; text-align:center; margin-left:14
}
.Button1:hover {
	background-position: center -35px;
}
a:link {
    color: #000000;
}
a:visited {
    color: #000000;
}

</style>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>
<script>
	$(function() {
			jQuery.ajax({
			url: '<?php echo base_url();?>front/commercant/manage_nbrevisites/<?php echo $oInfoCommercant->IdCommercant;?>',
			dataType: 'html',
			type: 'POST',
			async: true,
			success: function(data){
				//window.location.reload();
			}
		});
		
		
		
	});
</script>

</head>

<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
?>
<?php if ($group_id_commercant_user == 4 || $group_id_commercant_user == 5) { ?><?php } ?>


<body>
<div class="contenerall">
  <?php 
  if (isset($_REQUEST['nohome'])) $_SESSION['nohome'] = $_REQUEST['nohome'];
  if (isset($_SESSION['nohome']) && $_SESSION['nohome']=='true') {} else { ?>
  	<div class="contener_standard">
    	<?php if (isset($pagecategory_partner) && ($pagecategory_partner=="annonces_partner" || $pagecategory_partner=="details_annonces_partner")) { ?>
        	<a href="<?php echo site_url('front/annonce/'); ?>">
            <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpebaf05d0_06.png" width="320" height="59" alt="back">
            </a>
        <?php } else if(isset($pagecategory_partner) && $pagecategory_partner=="bonplan_partner") { ?>
            <a href="<?php echo site_url('front/bonplan/'); ?>">
            <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp7cf80293_06.png" width="320" height="59" alt="back">
            </a>
        <?php } else { ?>
        	<form name="link_mobile_to_list_partner" id="link_mobile_to_list_partner" action="<?php echo base_url();?>" method="post">
            <input type="hidden" name="from_mobile_search_page_partner" id="from_mobile_search_page_partner" value="1"/>
            <a href="javascript:void(0);" onClick="javascript:document.link_mobile_to_list_partner.submit();">
            <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpd8fdd1bb_06.png" width="320" height="59" alt="back">
            </a>
            </form>
        <?php }  ?>
    </div>
  <?php } ?>
  <div class="contenerstandardwithmarge">
    <div class="contener_infos col-xs-8 padding0">
    	<?php 
		if (isset($mdlville)) $oVille = $this->mdlville->getVilleById($oInfoCommercant->IdVille);
		?>
    	<?php if($oInfoCommercant->NomSociete != "") { ?><span class="contener_infos_title"><?php echo $oInfoCommercant->NomSociete ; ?></span><br/><?php } ?>
        <?php if($oInfoCommercant->titre_entete != "") echo '<span class="contener_infos_desc">'.$oInfoCommercant->titre_entete.'</span><br/>'; ?>
        <?php echo '<span class="contener_infos_adress">'.$oInfoCommercant->Adresse1 ; ?><?php if ($oInfoCommercant->Adresse2 != "") echo " - ".$oInfoCommercant->Adresse2 ; echo '</span><br/>';?>
		<?php echo '<span class="contener_infos_adress">'.$oInfoCommercant->CodePostal ; ?><?php if ($oInfoCommercant->ville != "") echo " - ".$oInfoCommercant->ville ; echo '</span><br/>';?> 
    </div>
    <div class="contener_logo col-xs-4 padding0">
		<?php if (isset($oInfoCommercant->logo_url) && $oInfoCommercant->logo_url!="" && $oInfoCommercant->logo_url!="http//" && $oInfoCommercant->logo_url!="http://") {?>
            <a href="<?php echo $oInfoCommercant->logo_url;?>" target="_blank" id="logo_url_to_show">
        <?php } else {?>
            <a href="<?php echo site_url($commercant_url_home."/index") ;?>">
        <?php }?>
					<?php if ($oInfoCommercant->Logo != "" && $oInfoCommercant->Logo != NULL && file_exists("application/resources/front/photoCommercant/images/".$oInfoCommercant->Logo)== true) {?>
                    <img src="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $oInfoCommercant->Logo;?>" width="65" alt="logo" title=""/>
                    <?php } else {?>
                    <img src="<?php echo GetImagePath("front/"); ?>/wp67b9ee2f_06.png" id="grp_857" alt="" onload="OnLoadPngFix()" height="78" border="0" width="65">
                    <?php } ?>
        </a>    
    </div>
  </div>
  <div class="contenerstandardwithmarge"  style="text-align:center;">
  	<table border="0" cellspacing="0" cellpadding="0" style="width:100%;">
      <tr>
        <td>
        	<a href="<?php echo site_url($commercant_url_home."/index");?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpd985cd97_06.png" width="74" height="74" alt="home"></a>
        </td>
        
        <?php if ($group_id_commercant_user == 4 || $group_id_commercant_user == 5) { ?>
        <td>
			<?php if ($oInfoCommercant->TelFixe != "" && $oInfoCommercant->TelFixe != NULL) echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelFixe).'">';
					else if ($oInfoCommercant->TelMobile != "" && $oInfoCommercant->TelMobile != NULL) echo '<a href="tel:'.supprime_espace($oInfoCommercant->TelMobile).'">';
                    else echo '<a href="Javascript:void();">';
            ?>
            <img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp3aba1eed_06.png" width="74" height="74" alt="phone"><?php echo "</a>";?>
        </td>
        <?php } ?>
        
        <td>
        	<a href="<?php echo site_url($commercant_url_nous_contacter);?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wpb6e45b55_06.png" width="74" height="74" alt="contact"></a>
        </td>
        <?php if ($group_id_commercant_user == 4 || $group_id_commercant_user == 5) { ?>
        <td>
        	<a href="<?php echo site_url($commercant_url_noussituer);?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/mobile/wp6d48364f_06.png" width="74" height="74" alt="localisation"></a>
        </td>
        <?php } ?>
      </tr>
    </table>
  </div>
  <div class="contener_img_mobile_head">
    <div class="contener_img_mobile_head_content">
      <div class="contener_img_mobile_head_content_title">
	  	<?php if (isset($pagecategory_partner) && $pagecategory_partner=="bonplan_partner") echo "Notre Bon Plan"; else echo truncate($zTitle,30,$etc = " ..."); ?>
      </div>
      <div class="contener_img_mobile_head_content_image">
            <?php if ($oInfoCommercant->PhotoAccueil != "" && $oInfoCommercant->PhotoAccueil != NULL && file_exists("application/resources/front/photoCommercant/images/".$oInfoCommercant->PhotoAccueil)== true) {?><img src="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $oInfoCommercant->PhotoAccueil;?>" width="100%" alt="<?php echo $oInfoCommercant->PhotoAccueil; ?>"><?php } 
			else if ($oInfoCommercant->Photo1 != "" && $oInfoCommercant->Photo1 != NULL && file_exists("application/resources/front/photoCommercant/images/".$oInfoCommercant->Photo1)== true) {?><img src="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $oInfoCommercant->Photo1;?>" width="100%" alt="<?php echo $oInfoCommercant->Photo1; ?>"><?php } 
			else if ($oInfoCommercant->Photo2 != "" && $oInfoCommercant->Photo2 != NULL && file_exists("application/resources/front/photoCommercant/images/".$oInfoCommercant->Photo2)== true) {?><img src="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $oInfoCommercant->Photo2;?>" width="100%" alt="<?php echo $oInfoCommercant->Photo2; ?>"><?php } 
			else if ($oInfoCommercant->Photo3 != "" && $oInfoCommercant->Photo3 != NULL && file_exists("application/resources/front/photoCommercant/images/".$oInfoCommercant->Photo3)== true) {?><img src="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $oInfoCommercant->Photo3;?>" width="100%" alt="<?php echo $oInfoCommercant->Photo3; ?>"><?php } 
			else if ($oInfoCommercant->Photo4 != "" && $oInfoCommercant->Photo4 != NULL && file_exists("application/resources/front/photoCommercant/images/".$oInfoCommercant->Photo4)== true) {?><img src="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $oInfoCommercant->Photo4;?>" width="100%" alt="<?php echo $oInfoCommercant->Photo4; ?>"><?php } 
			else if ($oInfoCommercant->Photo5 != "" && $oInfoCommercant->Photo5 != NULL && file_exists("application/resources/front/photoCommercant/images/".$oInfoCommercant->Photo5)== true) {?><img src="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $oInfoCommercant->Photo5;?>" width="100%" alt="<?php echo $oInfoCommercant->Photo5; ?>"><?php } else { ?>
			<img src="<?php echo GetImagePath("front/"); ?>/wp0f85a330_06.png" id="meta_9" alt="" onload="OnLoadPngFix()"  height="163" border="0" width="100%">
			<?php } ?>
      </div>
    </div>
  </div>
  <div class="contener_main">
  
  
  	<div style="text-align:center; background-color:#FFFFFF; padding-top:10px;">
    <!-- Begin TranslateThis Button -->
<div id="google_translate_element" style="padding:15px;"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false, multilanguagePage: true}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<!-- End TranslateThis Button -->
    </div>
  	
  