<script type="text/javascript">
jQuery(document).ready(function() {
	var win = jQuery(window);
	jQuery('#loading_bonplans_adresses').hide();
	var PerPage_init = jQuery('#PerPage_bonplans_adresses').val();
	var filter_array = [];
	var iiii = 0;

	// Each time the user scrolls
	win.scroll(function() {
		// End of the document reached?
		if (jQuery(window).scrollTop() + jQuery(window).height() > jQuery(document).height() - 100) {
			//if (jQuery(document).scrollTop() + jQuery(window).height() == getDocHeight()) {
			jQuery('#loading_bonplans_adresses').show();
			var PerPage = jQuery('#PerPage_bonplans_adresses').val();
			var test_filtering = $.inArray(PerPage, filter_array); //alert(String(test_filtering)+' / '+String(iiii));

			if (test_filtering=='-1') {
				filter_array[iiii] = PerPage;
				jQuery.ajax({
					url: '<?php echo site_url("front/bonplan/index/");?>/'+String(PerPage)+'?content_only_list=1',
					dataType: 'html',
					success: function(html) {
						jQuery('#Id_bonplans_adresses').append(html);
						jQuery('#loading_bonplans_adresses').hide();
						jQuery('#PerPage_bonplans_adresses').val(parseInt(PerPage)+parseInt(PerPage_init));
					}
				});
			} else {
				//jQuery('#loading_bonplans_adresses').hide();
			}
			iiii = iiii + 1;
		}
	});
});
function getDocHeight() {
    var D = document;
    return Math.max(
        D.body.scrollHeight, D.documentElement.scrollHeight,
        D.body.offsetHeight, D.documentElement.offsetHeight,
        D.body.clientHeight, D.documentElement.clientHeight
    );
}
</script>

<div id="loading_bonplans_adresses" class="" style="text-align:center;"><img src="<?php echo GetImagePath("sortez/");?>/loading.gif" /></div>  
<input id="PerPage_bonplans_adresses" type="hidden" value="<?php if (isset($PerPage)) echo $PerPage; else echo "0"; ?>"/>
