<?php $data["zTitle"] = 'Creation annonce'; ?>
<?php $this->load->view("frontAout2013/includes/header_mini_2", $data); ?>
	<script>
		$(function() {
			$( "#IdDateDebut" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
								 			 dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
											 monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
											 dateFormat: 'DD, d MM yy',
											 autoSize: true,
											changeMonth: true,
											changeYear: true,
											yearRange: '1900:2020'});
			$( "#IdDateFin" ).datepicker({ dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
											dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
											monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
											dateFormat: 'DD, d MM yy',
											autoSize: true,
											changeMonth: true,
											changeYear: true,
											yearRange: '1900:2020'});
			$(".complete_img_chekings").hide();
		});
	</script>
    <style type="text/css">
	.input_width {
		width:400px;
	}
	</style>
    
    <div style="background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/head_mini_retour_2.png); background-position:left; height: 62px;
    padding-left: 350px;
    padding-top: 155px;
    width: 300px;">
      <input type="button" value="Retour" onclick="javascript:annulationAjout('<?php echo site_url("front/annonce/annulationAjout");?>', '<?php echo site_url("front/annonce/listeMesAnnonces/");?>', '<?php if (!isset($oAnnonce)){ echo "ajout" ;}?>', '<?php echo $idCommercant ;?>');" />
    </div>

    <div id="divFicheAnnonce" class="content" align="center" style="text-align:center; float:left; padding-left:15px; padding-right:15px; width:620px;">
        <form name="frmCreationAnnonce" id="frmCreationAnnonce" action="<?php if (isset($oAnnonce)) { echo site_url("front/annonce/modifAnnonce/$idCommercant"); }else{ echo site_url("front/annonce/creerAnnonce/$idCommercant"); } ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="annonce[annonce_commercant_id]" id="IdCommercant" value="<?php echo $idCommercant ; ?>" />
		<input type="hidden" name="annonce[annonce_id]" id="IdAnnonce_id" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_id ; } ?>" />
            <div style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em; text-align:left; background-color:#3653A3; padding-left:15px; height:20px; padding-top:3px;'>
            Le détail de votre annonce
            </div>
			<span><?php if (isset($msg)) { echo $msg ; } ?></span>
                <table width="100%" cellpadding="3" cellspacing="3" style="text-align:left; float:left;" align="left">
                    <!--<tr>
                        <td>
                            <label>Sous-rubrique : </label>
                        </td>
                        <td>
                            <select name="annonce[annonce_sous_rubriques_id]" id="idSousRubrique">
                                <option value="0">-- Veuillez choisir --</option>
                                <?php //if(sizeof($colSousRubriques)) { ?>
                                    <?php //foreach($colSousRubriques as $objSousRubrique) { ?>
                                        <option value="<?php //echo $objSousRubrique->IdSousRubrique; ?>" <?php //if (isset($oAnnonce) && $oAnnonce->annonce_sous_rubrique_id == $objSousRubrique->IdSousRubrique) { echo "selected"; }?> ><?php //echo htmlentities($objSousRubrique->Nom); ?></option>
                                    <?php //} ?>
                                <?php //} ?>
                            </select>
                        </td>
                    </tr>-->
					

                    <tr>
                        <td>
                            <label>Titre : </label>
                        </td>
                        <td>
							<textarea rows="2" cols="20" name="annonce[annonce_description_courte]" id="idDescriptionCourt" class="input_width"><?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_description_courte ; } ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>D&eacute;scription longue : </label>
                        </td>
                        <td>
							<textarea name="annonce[annonce_description_longue]" id="idDescriptionLong"><?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_description_longue ; } ?></textarea>
                            <?php echo display_ckeditor($ckeditor0); ?>
                        </td>
                    </tr>
                  <tr>
                        <td style=" width:200px;">
                            <label>Etat : </label>
                        </td>
                        <td>
                            <select name="annonce[annonce_etat]" id="idEtat" class="input_width">
                                	<option value="c">-- Veuillez choisir --</option>
									<option value="0" <?php  if (isset($oAnnonce) && $oAnnonce->annonce_etat == 0) { echo "selected"; } ?>>Revente</option>
									<option value="1"  <?php if (isset($oAnnonce) && $oAnnonce->annonce_etat == 1) { echo "selected"; } ?>>Neuf</option>
                                    <option value="2"  <?php if (isset($oAnnonce) && $oAnnonce->annonce_etat == 2) { echo "selected"; } ?>>Service</option>
                            </select>
                        </td>
                    </tr>
                    <!--<tr>
                        <td style=" width:200px;">
                            <label>Conditions de vente : </label>
                        </td>
                        <td>
                          <select name="annonce[annonce_condition_vente]" id="idConditionVente" class="input_width">
                                <option value="0">Choisissez vos conditions de réservation ou de règlement</option>
                                <option value="1" <?php  //if (isset($oAnnonce) && $oAnnonce->annonce_condition_vente == 1) { echo "selected"; } ?>>Demande d'informations complémentaires</option>
                                <option value="2" <?php  //if (isset($oAnnonce) && $oAnnonce->annonce_condition_vente == 2) { echo "selected"; } ?>>Réservation en ligne</option>
                            </select>
                        </td>
                    </tr>-->
					<tr>
                        <td>
                            <label>Date debut : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_date_debut]" id="IdDateDebut" value="<?php if (isset($oAnnonce)) { echo convert_Sqldate_to_Frenchdate($oAnnonce->annonce_date_debut) ; } ?>"  class="input_width"/>
                        </td>
                    </tr>
					<tr>
                        <td>
                            <label>Date fin : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_date_fin]" id="IdDateFin" value="<?php if (isset($oAnnonce)) { echo convert_Sqldate_to_Frenchdate($oAnnonce->annonce_date_fin) ; } ?>"  class="input_width"/>
                        </td>
                    </tr>
                    <!--<tr>
                        <td>
                            <label>Quantité proposée : </label>
                        </td>
                        <td>
                          <input type="text" name="annonce[annonce_quantite]" id="IdQuantite" value="<?php //if (isset($oAnnonce)) { echo $oAnnonce->annonce_quantite ; } ?>"  class="input_width"/>
                        </td>
                    </tr>--><input type="hidden" name="annonce[annonce_quantite]" id="IdQuantite" value=""  class="input_width"/>
                    <!--<tr>
                        <td>
                            <label>Durée de mise à disposition : </label>
                        </td>
                        <td>
                          <input type="text" name="annonce[annonce_livraison]" id="IdLivraison" value="<?php //if (isset($oAnnonce)) { echo $oAnnonce->annonce_livraison ; } ?>"  class="input_width"/>
                        </td>
                    </tr>-->
                  <tr>
                        <td>
                            <label>Ancien prix (€) : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_prix_neuf]" id="idPrixNeuf" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_prix_neuf ; } ?>"  class="input_width"/></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Prix (€) : </label>
                        </td>
                        <td>
                            <input type="text" name="annonce[annonce_prix_solde]" id="IdPrixSolde" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_prix_solde ; } ?>"  class="input_width"/></td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 01 : </label>
                        </td>
                        <td>
                        	<?php if (isset($oAnnonce) && $oAnnonce->annonce_photo1 != "" && $oAnnonce->annonce_photo1 != NULL) { ?>
                        		<img src="<?php echo GetImagePath("front/"); ?>/<?php echo $oAnnonce->annonce_photo1; ?>" id="loading1" width="75"/>
                            <?php } ?>
							<input type="hidden" name="annonce[annonce_photo1]" id="IdPhoto1" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_photo1 ; } ?>" />
                            <input type="file" name="annoncePhoto1" id="annoncePhoto1" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'annoncePhoto1', '1');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading1" style="display:none"/><img src="<?php echo GetImagePath("front/"); ?>/btn_new/icon-16-checkin.png" id="complete1" class="complete_img_chekings"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 02 :</label>
                        </td>
                        <td>
                        	<?php if (isset($oAnnonce) && $oAnnonce->annonce_photo2 != "" && $oAnnonce->annonce_photo2 != NULL) { ?>
                        		<img src="<?php echo GetImagePath("front/"); ?>/<?php echo $oAnnonce->annonce_photo2; ?>" id="loading1" width="75"/>
                            <?php } ?>
							<input type="hidden" name="annonce[annonce_photo2]" id="IdPhoto2" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_photo2 ; } ?>" />
                            <input type="file" name="annoncePhoto2" id="annoncePhoto2" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'annoncePhoto2', '2');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading2" style="display:none"/><img src="<?php echo GetImagePath("front/"); ?>/btn_new/icon-16-checkin.png" id="complete2" class="complete_img_chekings"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 03 :</label>
                        </td>
                        <td>
                        	<?php if (isset($oAnnonce) && $oAnnonce->annonce_photo3 != "" && $oAnnonce->annonce_photo3 != NULL) { ?>
                        		<img src="<?php echo GetImagePath("front/"); ?>/<?php echo $oAnnonce->annonce_photo3; ?>" id="loading1" width="75"/>
                            <?php } ?>
							<input type="hidden" name="annonce[annonce_photo3]" id="IdPhoto3" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_photo3 ; } ?>" />
                            <input type="file" name="annoncePhoto3" id="annoncePhoto3" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'annoncePhoto3', '3');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading3" style="display:none"/><img src="<?php echo GetImagePath("front/"); ?>/btn_new/icon-16-checkin.png" id="complete3" class="complete_img_chekings"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Photo 04 :</label>
                        </td>
                        <td>
                        	<?php if (isset($oAnnonce) && $oAnnonce->annonce_photo4 != "" && $oAnnonce->annonce_photo4 != NULL) { ?>
                        		<img src="<?php echo GetImagePath("front/"); ?>/<?php echo $oAnnonce->annonce_photo4; ?>" id="loading1" width="75"/>
                            <?php } ?>
							<input type="hidden" name="annonce[annonce_photo4]" id="IdPhoto4" value="<?php if (isset($oAnnonce)) { echo $oAnnonce->annonce_photo4 ; } ?>" />
                            <input type="file" name="annoncePhoto4" id="annoncePhoto4" value="" onchange="javascript:UploadImage ('<?php echo site_url("front/upload/index/");?>', 'annoncePhoto4', '4');"/><img src="<?php echo GetImagePath("front/"); ?>/loading.gif" id="loading4" style="display:none"/><img src="<?php echo GetImagePath("front/"); ?>/btn_new/icon-16-checkin.png" id="complete4" class="complete_img_chekings"/>
                        </td>
                    </tr>
					
                    
                    <tr>
                    	<td colspan="2">
                        	<div style='color: #FFFFFF;
                            font-family: "Arial",sans-serif;
                            font-size: 13px;
                            font-weight: 700;
                            line-height: 1.23em; text-align:left; background-color:#3653A3; padding-left:15px; height:20px; padding-top:3px;'>
                                    Intégration de vos conditions
                                    </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        
<script type="text/javascript">
		 $(function(){
			 $("#checkbox_retrait_etablissement").click(function(){
					$("#Idretrait_etablissement").val("1");
					$("#Idlivraison_domicile").val("0");
			 });
			 $("#checkbox_livraison_domicile").click(function(){
					$("#Idlivraison_domicile").val("1");
					$("#Idretrait_etablissement").val("0");
			 });
			 $("#checkbox_module_paypal").click(function(){
				 if ($('#checkbox_module_paypal').attr('checked')) {
					$("#Idmodule_paypal").val("1");
				} else {
					$("#Idmodule_paypal").val("0");
				}
			 });
		 })
</script>
                        
                        
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td>
                            <br/>
                            
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="100px;">
                              <tr>
                                <td>
                                <input type="radio" name="radiobtn_livraison" id="checkbox_retrait_etablissement" <?php if(isset($oAnnonce->retrait_etablissement) && $oAnnonce->retrait_etablissement == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="annonce[retrait_etablissement]" id="Idretrait_etablissement" value="<?php if(isset($oAnnonce->retrait_etablissement) && $oAnnonce->retrait_etablissement == "1") echo '1'; else echo '0'; ?>"/>
                                </td>
                                <td><label>Retrait dans notre<br/>etablissement</label></td>
                              </tr>
                              <tr>
                                <td>
                                <input type="radio" name="radiobtn_livraison" id="checkbox_livraison_domicile" <?php if(isset($oAnnonce->livraison_domicile) && $oAnnonce->livraison_domicile == "1") echo 'checked="checked"'; ?>/>
                                <input type="hidden" name="annonce[livraison_domicile]" id="Idlivraison_domicile" value="<?php if(isset($oAnnonce->livraison_domicile) && $oAnnonce->livraison_domicile == "1") echo '1'; else echo '0'; ?>"/>
                                </td>
                                <td><label>Livraison à domicile</label></td>
                              </tr>
                            </table>
                            </td>
                            <td><img src="<?php echo GetImagePath("front/");?>/btn_new/retrait_etablissement.png" id="loading1"/></td>
                            <td><img src="<?php echo GetImagePath("front/");?>/btn_new/livraison_domicile.png" id="loading1"/></td>
                          </tr>
                        </table>

                        
                        
                        </td>
                    </tr>
                    
                    <tr>
                    	<td colspan="2">
                        	<div style='color: #FFFFFF;
                            font-family: "Arial",sans-serif;
                            font-size: 13px; padding-bottom:5px;
                            font-weight: 700;
                            line-height: 1.23em; text-align:left; background-color:#3653A3; padding-left:15px; height:20px; padding-top:3px;'>
                            <input type="checkbox" name="checkbox_module_paypal" id="checkbox_module_paypal" <?php if(isset($oAnnonce->module_paypal) && $oAnnonce->module_paypal == "1") echo 'checked="checked"'; ?>/>
                            <input type="hidden" name="annonce[module_paypal]" id="Idmodule_paypal" value="<?php if(isset($oAnnonce->module_paypal) && $oAnnonce->module_paypal == "1") echo '1'; else echo '0'; ?>"/>
                            <label>Valider le module PAYPAL pour cette annonce.</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Comment ouvrir un compte PAYPAL et intégrer un bouton de paiement automatique PAYPAL à cette annonce</td>
                        <td><img src="<?php echo GetImagePath("front/");?>/btn_new/wp593360d8_05_06.jpg" id="info_paypal"/></td>
                    </tr>
                    <tr>
                        <td>Intégration de votre bouton Paypal (Copier le code html fournit par PAYPAL).<br/><br/>Le bouton apparaîtra automatiquement sur votre page</td>
                        <td><textarea name="annonce_module_paypal_btnpaypal" id="module_paypal_btnpaypal" style="width:400px; height:120px;">
                        <?php if(isset($oAnnonce->module_paypal_btnpaypal) && $oAnnonce->module_paypal_btnpaypal != "") echo $oAnnonce->module_paypal_btnpaypal; ?>
                        </textarea></td>
                    </tr>
                    <tr>
                        <td>Si vous choisissez d’intégrer un panier, nous vous invitons à intégrer le code html PAYPAL du bouton «afficher le panier » Le bouton apparaîtra automatique-ment sur votre page</td>
                        <td><textarea name="annonce_module_paypal_panierpaypal" id="module_paypal_panierpaypal" style="width:400px; height:120px;">
                        <?php if(isset($oAnnonce->module_paypal_panierpaypal) && $oAnnonce->module_paypal_panierpaypal != "") echo $oAnnonce->module_paypal_panierpaypal; ?>
                        </textarea></td>
                    </tr>
					<tr>
                        <td colspan="2">
                        <div style='color: #FFFFFF;
                        font-family: "Arial",sans-serif;
                        font-size: 13px;
                        font-weight: 700;
                        line-height: 1.23em; background-color:#3653A3; height:50px; padding-top:15px; text-align:center;'>
                                <input type="button" value="Modification de votre annonce" onclick="javascript:testFormAnnonce();" />
                                </div>
                      </td>
                    </tr>
                </table>
        </form>
        
        
        
    </div>
    
<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>