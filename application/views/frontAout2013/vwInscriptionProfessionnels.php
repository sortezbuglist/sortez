<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("frontAout2013/includes/header_mini_2", $data); ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.4.js"></script>

	<script type="text/javascript">
	
		 
		 
		 // Use jQuery via $(...)
		 $(document).ready(function(){//debut ready fonction
			   
			   
			   //To show sousrubrique corresponding to rubrique
			   $("#RubriqueSociete").change(function(){
                                var irubId = $("#RubriqueSociete").val();
                                //alert(irubId);
								$.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>front/professionnels/testAjax/"+irubId,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        $("#trReponseRub").html(numero_reponse);
                                        //alert(numero_reponse);
                                    }
                                    });
                                    //alert ("test "+$("#RubriqueSociete").val());
                                                                    
                           });
			   
			   
			   $("#EmailSociete").blur(function(){
										   //alert('cool');
										   //var result_to_show = "";
										   var value_result_to_show = "0";
										   
										   var txtEmail = $("#EmailSociete").val();
										   //alert('<?php //echo site_url("front/professionnels/verifier_email"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   $.post(
													'<?php echo site_url("front/professionnels/verifier_email");?>',
													{ txtEmail_var: txtEmail },
													function (zReponse)
													{
														//alert (zReponse) ;
														//var zReponse_html = '';
														if (zReponse == "1") {
															value_result_to_show = "1";
														}
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtEmail_').html(result_to_show);       
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';
															$('#divErrortxtEmail_').html(result_to_show);    
															$('#txtEmail_verif').val("0");
														}
													 
												   });
											
											$.post(
													'<?php echo site_url("front/professionnels/verifier_email_ionauth");?>',
													{ txtEmail_var_ionauth: txtEmail },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														//var zReponse_html_ionauth = '';
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														
													 	if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce mail est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtEmail_').html(result_to_show);       
															$('#txtEmail_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce mail est disponible</span>';
															$('#divErrortxtEmail_').html(result_to_show);    
															$('#txtEmail_verif').val("0");
														}
														
														
												   });
												   
												   
											
											
												   
										   
										   //jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			   
			   
			   $("#txtLogin").blur(function(){
										   //alert('cool');
										   var txtLogin = $("#txtLogin").val();
										   
										   var value_result_to_show = "0";
										   
										   //alert('<?php //echo site_url("front/professionnels/verifier_login"); ?>' + '/' + txtEmail);
										   //jQuery(".EmailLoading").addClass("EmailLoading_show").removeClass("EmailLoading");
										   $.post(
													'<?php echo site_url("front/professionnels/verifier_login");?>',
													{ txtLogin_var: txtLogin },
													function (zReponse)
													{
														//alert (zReponse) ;
														var zReponse_html = '';
														if (zReponse == "1") {
															value_result_to_show = "1";
														} 
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
													 
												   });
										   
										   
										   $.post(
													'<?php echo site_url("front/professionnels/verifier_login_ionauth");?>',
													{ txtLogin_var_ionauth: txtLogin },
													function (zReponse_ionauth)
													{
														//alert (zReponse) ;
														var zReponse_html_ionauth = '';
														if (zReponse_ionauth == "1") {
															value_result_to_show = "1";
														} 
														
														
														if (value_result_to_show == "1") {
															result_to_show = '<span style="color:#F00; font-weight:bold;">Ce login est d&eacute;j&agrave; utilis&eacute;</span>';
															$('#divErrortxtLogin_').html(result_to_show);       
															$('#txtLogin_verif').val("1");
														} else {
															result_to_show = '<span style="color:#3C0; font-weight:bold;">Ce login est disponible</span>';
															$('#divErrortxtLogin_').html(result_to_show);    
															$('#txtLogin_verif').val("0");
														}
													 
												   });
										   
										   
										   //jQuery(".FieldError").removeClass("FieldError");
										   //jQuery(".EmailLoading").addClass("EmailLoading").removeClass("EmailLoading_show");
										   
										   });
			
			   
			   //To show postal code automatically
			   $("#VilleSociete").change(function(){
								var irubId = $("#VilleSociete").val();
                                //alert(irubId);
								$.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>front/professionnels/getPostalCode/"+irubId,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        $("#CodePostalSociete").val(numero_reponse);
                                        //alert(numero_reponse);
                                    }
                                    });
                                    //alert ("test "+$("#VilleSociete").val());
				});
		
			   
		
		var valabonnementht = 0;
		var valmoduleht = 0;
		
		var abonnement_premium_an = 250;
		var abonnement_platinium = 400;
		var module_agendaplus = 250;
		var module_web_ref_an = 250;
		var module_web_autres_an = 100;
		var module_restauration = 100;
		
		
		$("#check_358_premium").click(function(){
            if ($('#check_358_premium').attr('checked')) {
                $("#check_358_premium_value").val(abonnement_premium_an);
				$("#check_358_premium_div").html(abonnement_premium_an);
				calcmontantht();
            } else {
				$("#check_358_premium_value").val(0);
				$("#check_358_premium_div").html("");
				calcmontantht();
			}
        });
		$("#check_358_platinium").click(function(){
            if ($('#check_358_platinium').attr('checked')) {
                $("#check_358_platinium_value").val(abonnement_platinium);
				$("#check_358_platinium_div").html(abonnement_platinium);
				calcmontantht();
            } else {
				$("#check_358_platinium_value").val(0);
				$("#check_358_platinium_div").html("");
				calcmontantht();
			}
        });
		$("#check_358_agenda_plus").click(function(){
            if ($('#check_358_agenda_plus').attr('checked')) {
                $("#check_358_agenda_plus_value").val(module_agendaplus);
				$("#check_358_agenda_plus_div").html(module_agendaplus);
				calcmontantht();
            } else {
				$("#check_358_agenda_plus_value").val(0);
				$("#check_358_agenda_plus_div").html("");
				calcmontantht();
			}
        });
		$("#check_358_web_ref1").click(function(){
            if ($('#check_358_web_ref1').attr('checked')) {
                $("#check_358_web_ref1_value").val(module_web_ref_an);
				$("#check_358_web_ref1_div").html(module_web_ref_an);
				calcmontantht();
            } else {
				$("#check_358_web_ref1_value").val(0);
				$("#check_358_web_ref1_div").html("");
				calcmontantht();
			}
        });
		$("#check_358_web_ref_n").click(function(){
            if ($('#check_358_web_ref_n').attr('checked')) {
                $("#check_358_web_ref_n_value").val(module_web_autres_an);
				$("#check_358_web_ref_n_div").html(module_web_autres_an);
				calcmontantht();
            } else {
				$("#check_358_web_ref_n_value").val(0);
				$("#check_358_web_ref_n_div").html("");
				calcmontantht();
			}
        });
		$("#check_358_restauration").click(function(){
            if ($('#check_358_restauration').attr('checked')) {
                $("#check_358_restauration_value").val(module_restauration);
				$("#check_358_restauration_div").html(module_restauration);
				calcmontantht();
            } else {
				$("#check_358_restauration_value").val(0);
				$("#check_358_restauration_div").html("");
				calcmontantht();
			}
        });
		
		
		var totalmontantht = 0;
		var montanttva = 0;
		var valtva = 0.20;
		var montantttc = 0;
		
		function calcmontantht(){
			var check_358_premium_value = parseInt($("#check_358_premium_value").val());
			//alert("check_358_premium_value "+ check_358_premium_value);
			var check_358_platinium_value = parseInt($("#check_358_platinium_value").val());
			//alert("check_358_platinium_value "+check_358_platinium_value);
			var check_358_agenda_plus_value = parseInt($("#check_358_agenda_plus_value").val());
			//alert("check_358_agenda_plus_value "+check_358_agenda_plus_value);
			var check_358_web_ref1_value = parseInt($("#check_358_web_ref1_value").val());
			//alert("check_358_web_ref1_value "+check_358_web_ref1_value);
			var check_358_web_ref_n_value = parseInt($("#check_358_web_ref_n_value").val());
			//alert("check_358_web_ref_n_value "+check_358_web_ref_n_value);
			var check_358_restauration_value = parseInt($("#check_358_restauration_value").val());
			//alert("check_358_restauration_value "+check_358_restauration_value);
			
			//alert(check_358_premium_value);
			
			totalmontantht = check_358_premium_value + check_358_platinium_value + check_358_agenda_plus_value + check_358_web_ref1_value + check_358_web_ref_n_value + check_358_restauration_value;
			//alert(totalmontantht);
			$("#divMontantHT").html(totalmontantht+"€");
			$("#hidemontantht").val(totalmontantht+"€");
			//calcmontanttva (parseInt(totalmontantht), valtva);
			//calcmontantttc (parseInt(totalmontantht), montanttva);
			montanttva = totalmontantht * valtva;
			$("#divMontantTVA").html(_roundNumber(montanttva,2)+"€");
			$("#hidemontanttva").val(_roundNumber(montanttva,2)+"€");
			montantttc = totalmontantht + montanttva;
			$("#divMontantTTC").html(montantttc+"€");
			$("#hidemontantttc").val(montantttc+"€");
			$("#montantttcvalue_abonnement").val(montantttc);
		}
		
		function calcmontanttva (totalmontantht, valtva){
			montanttva = totalmontantht * valtva;
			$("#divMontantTVA").html(_roundNumber(montanttva,2)+"€");
		}
		
		//limit decimal
		function _roundNumber(num,dec) {
			
			return (parseFloat(num)).toFixed(dec);
		}
		
		function calcmontantttc (totalmontantht, montanttva){
			montantttc = totalmontantht + montanttva;
			$("#divMontantTTC").html(montantttc+"€");
			$("#montantttcvalue_abonnement").val(montantttc);
		}
		
		/*	
		
		//show subscription value and label
		   //$("#AbonnementSociete").change(function(){
							var irubId = $("#AbonnementSociete").val();
							//alert(irubId);
							//$("#divAbonnementdesc").html($(this).find("option:selected").text());
							
                                                        $.post(
                                                        '<?php// echo base_url();?>front/utilisateur/get_tarif_abonnement/',
                                                            { 
                                                                IdAbonnement: irubId
                                                            },
                                                            function (zReponse)
                                                            {
                                                                valabonnementht = parseInt(zReponse);
                                                                calcmontantht(valabonnementht, valmoduleht);
																$("#divAbonnementmontant").html(zReponse+"€");
																$("#divAbonnementmontant2").html(zReponse+"€");
                                                            });
                                                        
			//}); 
			*/
		
		//show module subscription value and label
		   /*$("#nb_module_nb_page").change(function(){
							var irubId = $("#nb_module_nb_page").val();
							//alert(irubId);
							$("#divModuledesc").html($(this).find("option:selected").text());
							if (irubId == 0) {
								valmoduleht = 0;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("");
								$("#divModuledesc").html("");
							}
							if (irubId == 1) {
								valmoduleht = 400;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("400€");
							}
							if (irubId == 2) {
								valmoduleht = 800;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("800€");
							}
							if (irubId == 3) {
								valmoduleht = 1200;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("1200€");
							}
							if (irubId == 4) {
								valmoduleht = 1600;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("1600€");
							}
							if (irubId == 5) {
								valmoduleht = 2000;
								calcmontantht(valabonnementht, valmoduleht);
								$("#divModulemontant").html("2000€");
							}
							
			});*/ 
			   
			   
			   
		//verify field form value 
		$("#btnSinscrire").click(function(){
										  
				var txtError = "";
				/*var EmailSociete = $("#EmailSociete").val();
				if(!isEmail(EmailSociete)) {
					$("#divErrorEmailSociete").html("Cet email n'est pas valide. Veuillez saisir un email valide");
					$("#divErrorEmailSociete").show();
					txtError += "1";
				} else {
					$("#divErrorEmailSociete").hide();
				}*/
				// :Check if a city has been selected before validating
				
				var RubriqueSociete = $('#RubriqueSociete').val();
				  if (RubriqueSociete == "") {
					txtError += "- Vous devez préciser votre activité<br/>";
				  }  
				
				var SousRubriqueSociete = $('#SousRubriqueSociete').val();
				  if (SousRubriqueSociete == "0") {
					txtError += "- Vous devez préciser une sous-rubrique<br/>";
				  } 
				
				var NomSociete = $('#NomSociete').val();
				  if (NomSociete == "") {
					txtError += "- Vous devez préciser le Nom ou enseigne<br/>";
				  }
				
				var ivilleId = $('#VilleSociete').val();
				  if (ivilleId == 0) {
					txtError += "- Vous devez sélectionner une ville<br/>";
				  }
				
				var CodePostalSociete = $('#CodePostalSociete').val();
				  if (CodePostalSociete == "") {
					txtError += "- Vous devez préciser le code postal<br/>";
				  }
				
				var EmailSociete = $("#EmailSociete").val();
				if(!isEmail(EmailSociete)) {
					txtError += "- Veuillez indiquer un email valide.<br/>";
				} 
				
				var txtEmail_verif = $("#txtEmail_verif").val();
				if(txtEmail_verif==1) {
					txtError += "- Votre Email existe déjà sur notre site <br/>";
				} 
				
				
				var NomResponsableSociete = $('#NomResponsableSociete').val();
				  if (NomResponsableSociete == "") {
					txtError += "- Vous devez préciser le Nom du Decideur <br/>";
				  }
				
				var PrenomResponsableSociete = $('#PrenomResponsableSociete').val();
				  if (PrenomResponsableSociete == "") {
					txtError += "- Vous devez préciser le Prenom du Decideur <br/>";
				  }
				  
				var ResponsabiliteResponsableSociete = $('#ResponsabiliteResponsableSociete').val();
				  if (ResponsabiliteResponsableSociete == "") {
					txtError += "- Vous devez préciser la responsabilité du Decideur <br/>";
				  }
				
				var TelDirectResponsableSociete = $('#TelDirectResponsableSociete').val();
				  if (TelDirectResponsableSociete == "") {
					txtError += "- Vous devez préciser le numero de téléphone du Decideur <br/>";
				  }
				
				var Email_decideurResponsableSociete = $("#Email_decideurResponsableSociete").val();
				if(!isEmail(Email_decideurResponsableSociete)) {
					txtError += "- Veuillez indiquer un email valide pour le decideur.<br/>";
				} 
				
				
				var AbonnementSociete = $('#AbonnementSociete').val();
				  if (AbonnementSociete == "0") {
					txtError += "- Vous devez choisir votre abonnement<br/>";
				  }  
				
				var activite1Societe = $('#activite1Societe').val();
				  if (activite1Societe == "") {
					txtError += "- Vous devez décrire votre activité<br/>";
				  }
				
				var txtLogin = $("#txtLogin").val();
				if(!isEmail(txtLogin)) {
					txtError += "- Votre login doit &ecirc;tre un email valide.<br/>";
				} 
				
				
				var txtLogin_verif = $("#txtLogin_verif").val();
				if(txtLogin_verif==1) {
					txtError += "- Votre Login existe déjà sur notre site <br/>";
				}
				
				var passs = $('#txtPassword').val();
				  if (passs == "") {
					txtError += "- Vous devez spécifier un mot de passe<br/>";
				  }
				
				if($("#txtPassword").val() != $("#txtConfirmPassword").val()) {
					txtError += "- Les deux mots de passe ne sont pas identiques.<br/>";
				}
				
				var validationabonnement = $('#validationabonnement').val();
				//alert("coche "+validationabonnement);
				if ($('#validationabonnement').is(":checked")) {} else {
					txtError += "- Vous devez valider les conditions générales<br/>";
				}
				
				
				if ($('#idreferencement0').is(":checked")) {
					$("#idreferencement").val("1");
				} else {
					$("#idreferencement").val("0");
				}
				//alert ("test "+$('#idreferencement').val());
				
				//verify captcha
				var captcha = $('#captcha').val();
				  if (captcha == "") {
					txtError += "- Vous devez remplir le captcha<br/>";
				  } else {
					  $.ajax({
                                    type: "GET",
                                    url: "<?php echo base_url(); ?>front/professionnels/verify_captcha/"+captcha,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        //alert(numero_reponse);
										$("#divCaptchavalueverify").val(numero_reponse);
                                    }
                             });
                             
				  }
				  
				 var divCaptchavalueverify = $('#divCaptchavalueverify').val();
				  if (divCaptchavalueverify == "0") {
					txtError += "- Les Textes que vous avez entré ne sont pas valides.<br/>";
				  } 
				// end verify captcha  
				  
				
				//final verification of input error
				if(txtError == "") {
					$("#frmInscriptionProfessionnel").submit();
				} else {
					$("#divErrorFrmInscriptionProfessionnel").html(txtError);
				}
			})
		
		
		
		
		
    })
		 
		 
    </script>

<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>

<script type="text/javascript">
	function btn_login_page_avantagepro() {
            //alert('qsdfgqsdf');
            var txtError = "";
            
            var user_login = $("#user_login").val();
            if(user_login=="" || user_login=="Préciser votre courriel") {
                txtError += "<br/>- Veuillez indiquer Votre login !"; 
                $("#user_login").css('border-color', 'red');
            }
            
            var user_pass = $("#user_pass").val();
            if(user_pass=="" || user_pass=="Préciser votre mot de passe") {
                txtError += "<br/>- Veuillez indiquer Votre mot de passe !";
                $("#user_pass").css('border-color', 'red');
            } 
	
            if(txtError == "") {
                $("#frmConnexion").submit();
            }
        }
	
        $(function(){    
            $("#user_login").focusin(function(){
                if($(this).val()=="Préciser votre courriel") {
                    $(this).val("");
                }
            });
            $("#user_login").focusout(function(){
                if($(this).val()=="") {
                    $(this).val("Préciser votre courriel");
                }
            });
            $("#user_pass").focusin(function(){
                if($(this).val()=="Préciser votre mot de passe") {
                    $(this).val("");
                }
            });
            $("#user_pass").focusout(function(){
                if($(this).val()=="") {
                    $(this).val("Préciser votre mot de passe");
                }
            });
        });	
		
</script>




<style type="text/css">
.Normal-C-C1 {
    color: #000054;
    font-family: "Vladimir Script",cursive;
    font-size: 48px;
    line-height: 47px;
}
.contect_all_data_pro_subscription {
	 margin-left:15px; margin-right:15px;
}
.title_sub_pro {
	color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 15px;
	padding-bottom: 5px;
    padding-top: 5px;
    font-weight: 700;
    line-height: 1.19em;
	text-align:center;
	margin-top:15px;
	margin-bottom:15px;
	background-color:#000054;
	}
.bloc_sub_pro {
	/*background-color:#3653A2;*/
	padding-bottom:0px;
	padding-top:5px;
}	
.space_sub_pro {
	height:20px;}
.table_sub_pro {
	color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    line-height: 1.23em;
	}
.table_sub_pro tr {
	height:35px;}
.table_sub_pro_abonnement td {
	border: 2px solid #000000;
	}		
.input_width {
	width:400px;
}
.td_color_1 {
	background-color:#F4F4F4;
}
.td_color_2 {
	background-color:#E5E5E5;
}
.td_color_3 {
	background-color:#B6B6B6;
}
</style>

<div style="text-align:center; background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/img_admin_platinium.png); background-repeat:no-repeat; height:170px; padding-left:150px; padding-top:30px;">
	<span class="Normal-C-C1">Souscrire un abonnement<br/>Validation de module
    </span>
</div>


<div id="contect_all_data_pro_subscription" class="contect_all_data_pro_subscription">



<div class="bloc_sub_pro">
<div class="title_sub_pro">Je suis déjà inscrit</div>
<div class="table_sub_pro">
<form id="frmConnexion" name="frmConnexion" accept-charset="utf-8" method="post" action="<?php echo site_url("auth/login"); ?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
  <tr>
    <td>Dès la reconnaissance de vos identifiants, les coordonnées de votre compte seront     intégrées automatiquement…</td>
  </tr>
  <tr>
    <td><input name="identity" id="user_login" value="Préciser votre courriel" style="width: 395px;" type="text"></td>
  </tr>
  <tr>
    <td><input name="password" id="user_pass" value="Préciser votre mot de passe" style="width: 395px;" type="password" ></td>
  </tr>
  <tr>
    <td><input id="butn_238234312" type="button" onClick="javascript:btn_login_page_avantagepro();" value="Je m'identifie" name="Retour" style="width:214px; height:22px;"></td>
  </tr>
  <tr>
    <td><input id="butn_24212312" type="button" value="Oubli de mes identifiants" name="z" onClick="javascript:document.location='<?php echo site_url("auth/forgot_password");?>';" style="width:214px; height:22px;"></td>
  </tr>
</table>
</form>
</div>
</div>

<div class="bloc_sub_pro">
<div class="title_sub_pro">Vous êtes un nouveau client professionnel ?</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
  <tr>
    <td>Nous vous invitons à remplir les champs obligatoires et de formuler votre demande….</td>
  </tr>
  <tr>
    <td><input id="butn_238234312" type="button" onClick="javascript:document.location='<?php echo site_url("magazine/infoscarte");?>';" value="Le fonctionnement" name="Retour" style="width:214px; height:22px;"></td>
  </tr>
  <tr>
    <td><input id="butn_24212312" type="button" value="Comparez nos abonnements" name="z" onClick="javascript:document.location='<?php echo site_url("front/utilisateur/comparerabonnements");?>';" style="width:214px; height:22px;"></td>
  </tr>
</table>
</div>
</div>


<form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("front/professionnels/ajouter"); ?>" method="POST" accept-charset="UTF-8" target="_self" enctype="multipart/form-data" style="margin:0px;">

<div class="bloc_sub_pro">
<div class="title_sub_pro">Votre activité</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="202px">Préciser votre activité *</td>
    <td>
    <select name="AssCommercantRubrique[IdRubrique]" id="RubriqueSociete" tabindex="1" class="input_width">
        <option value="">-- Veuillez choisir --</option>
        <?php if(sizeof($colRubriques)) { ?>
            <?php foreach($colRubriques as $objRubrique) { ?>
                <option value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
            <?php } ?>
        <?php } ?>
    </select>
    </td>
  </tr>
  <tr id='trReponseRub'  style="height:auto;">
    
  </tr>
  <tr>
    <td>Statut</td>
    <td>
    <select name="Societe[idstatut]" size="1" class="input_width" tabindex="3">
        <option selected="selected" value="">Choisir&nbsp;votre&nbsp;statut</option>
        <?php if(sizeof($colStatut)) { ?>
            <?php foreach($colStatut as $objStatut) { ?>
                <option value="<?php echo $objStatut->id; ?>"><?php echo stripcslashes($objStatut->Nom); ?></option>
            <?php } ?>
        <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>Autre, préciser</td>
    <td>
    <input name="Autre"  class="input_width" type="text" tabindex="4">
    </td>
  </tr>
  <tr>
    <td>Nom ou enseigne *</td>
    <td>
    <input type="text" name="Societe[NomSociete]" id="NomSociete" value=""  class="input_width" tabindex="5"/>
    </td>
  </tr>
  <tr>
    <td>Adresse 1</td>
    <td>
    <input type="text" name="Societe[Adresse1]" id="Adresse1Societe" value=""  class="input_width" tabindex="6"/>
    </td>
  </tr>
  <tr>
    <td>Adresse 2</td>
    <td>
    <input type="text" name="Societe[Adresse2]" id="Adresse2Societe" value=""  class="input_width" tabindex="7"/>
    </td>
  </tr>
  <tr>
    <td>Ville *</td>
    <td>
    <select name="Societe[IdVille]" id="VilleSociete"  class="input_width" tabindex="8">
      <option value="0">-- Veuillez choisir --</option>
        <?php if(sizeof($colVilles)) { ?>
            <?php foreach($colVilles as $objVille) { ?>
                <option value="<?php echo $objVille->IdVille; ?>"><?php echo htmlspecialchars(stripcslashes($objVille->Nom)); ?></option>
            <?php } ?>
        <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>Code Postal *</td>
    <td><input type="text" name="Societe[CodePostal]" id="CodePostalSociete" value=""  class="input_width" tabindex="9"/></td>
  </tr>
  <tr>
    <td>Téléphone direct</td>
    <td>
    <input type="text" name="Societe[TelFixe]" id="TelFixeSociete" value=""  class="input_width" tabindex="10"/>
    </td>
  </tr>
  <tr>
    <td>Téléphone mobile</td>
    <td><input type="text" name="Societe[TelMobile]" id="TelMobileSociete" value=""  class="input_width" tabindex="11"/></td>
  </tr>
  <tr>
    <td>Email *</td>
    <td>
    <input type="text" name="Societe[Email]" id="EmailSociete" value=""  class="input_width" tabindex="12"/>
    <div id="divErrortxtEmail_" style="width:152px; height:20px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>
    <input type="hidden" name="txtEmail_verif" id="txtEmail_verif" value="0"  class="input_width" tabindex="18"/>
    </td>
  </tr>
</table>
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>



<div class="bloc_sub_pro">
<div class="title_sub_pro">Les coordonnées du décideur</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">
    Le soussigné déclare avoir la faculté 
d’engager en son nom sa structure (commerce, entreprise, collectivité…) 
dont les coordonnées sont précisées ci-<wbr>dessus.
    </td>
  </tr>
  <tr>
    <td width="202px"s>Civilité *</td>
    <td>
    <select name="Societe[Civilite]" id="CiviliteResponsableSociete"  class="input_width" tabindex="13">
        <option value="0">Monsieur</option>
        <option value="1">Madame</option>
        <option value="2">Mademoiselle</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>Nom responsable *</td>
    <td><input type="text" name="Societe[Nom]" id="NomResponsableSociete" value=""  class="input_width" tabindex="14"/></td>
  </tr>
  <tr>
    <td>Prénom responsable *</td>
    <td><input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete" value=""  class="input_width" tabindex="15"/></td>
  </tr>
  <tr>
    <td>Fonction responsable *</td>
    <td><input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete" value=""  class="input_width" tabindex="16"/></td>
  </tr>
  <tr>
    <td>Téléphone direct *</td>
    <td><input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete" value=""  class="input_width" tabindex="17"/></td>
  </tr>
  <tr>
    <td>Email *</td>
    <td><input type="text" name="Societe[Email_decideur]" id="Email_decideurResponsableSociete" value=""  class="input_width" tabindex="18"/></td>
  </tr>
</table>
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>


<div class="bloc_sub_pro">
<div class="title_sub_pro">Votre identifiant et mot de passe</div>
<div class="table_sub_pro">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="202px">Identifiant *</td>
    <td>
    <input type="text" name="Societe[Login]" id="txtLogin" value=""  class="input_width" tabindex="20"/>
    <div id="divErrortxtLogin_" style="width:152px; overflow:hidden; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;"></div>
    <div id="inputMontantTTC">
       <input type="hidden" name="montantttcvalue_abonnement" id="montantttcvalue_abonnement"/>
       <input type="hidden" name="txtLogin_verif" id="txtLogin_verif" value="0" />
    </div>
    </td>
  </tr>
  <tr>
    <td>Mot de passe *</td>
    <td><input type="password" name="Societe_Password" id="txtPassword" value=""  class="input_width" tabindex="21"/></td>
  </tr>
  <tr>
    <td>Confirmation du mot de passe</td>
    <td><input type="password" id="txtConfirmPassword" value=""  class="input_width" tabindex="22"/></td>
  </tr>
</table>
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>

<?php 
$this->load->Model("Abonnement");
$obj_abonnement_gratuit = $this->Abonnement->GetWhere(" type='gratuit' ");
$obj_abonnement_premium = $this->Abonnement->GetWhere(" type='premium' ");
$obj_abonnement_platinum = $this->Abonnement->GetWhere(" type='platinum' ");

if (isset($obj_abonnement_gratuit) && $type=="basic") $value_abonnement_sub_pro = $obj_abonnement_gratuit->IdAbonnement;
else if (isset($obj_abonnement_premium) && $type=="premium") $value_abonnement_sub_pro = $obj_abonnement_premium->IdAbonnement;
else if (isset($obj_abonnement_platinum) && ($type=="platinium" || $type=="platinum")) $value_abonnement_sub_pro = $obj_abonnement_platinum->IdAbonnement;
else $value_abonnement_sub_pro = '1';
?>
<input type="hidden" name="AssAbonnementCommercant[IdAbonnement]" id="AbonnementSociete" value="<?php echo $value_abonnement_sub_pro;?>"/>

<div class="bloc_sub_pro">
<div class="title_sub_pro">Choisissez l’abonnement et les modules qui vous conviennent</div>


<div class="table_sub_pro">
<div style="text-align:center;margin-left: 0px;">
<table width="620" border="1" cellspacing="0" cellpadding="0" style="color:#000; text-align:center; border:2px solid;" class="table_sub_pro_abonnement">
  <tr style="height:25px;"> 
    <td width="366" class="td_color_2"><strong>Description</strong></td>
    <td width="102" class="td_color_2"><strong>Montant ht</strong></td>
    <td width="102" class="td_color_1">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">
    <input id="check_358_basic" type="checkbox" name="Message[basic]">
    <input type="hidden" id="check_358_basic_value" class="check_358_basic_value" value="0"/>
    Abonnement annuel basic
    </td>
    <td class="td_color_2">
    Gratuit
    </td>
    <td class="td_color_1"><div id="check_358_basic_div"></div></td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">
	<input id="check_358_premium" type="checkbox" name="Message[premium]">
    <input type="hidden" id="check_358_premium_value" class="check_358_premium_value" value="0"/>
        Abonnement annuel Premium
    </td>
    <td class="td_color_2">€250
    
    
    
    </td>
    <td class="td_color_1"><div id="check_358_premium_div"></div></td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">
	<input id="check_358_platinium" type="checkbox" name="Message[platinium]">
    <input type="hidden" id="check_358_platinium_value" class="check_358_platinium_value" value="0"/>
        Abonnement annuel Platinium
    </td>
    <td class="td_color_2">€400</td>
    <td class="td_color_1"><div id="check_358_platinium_div"></div></td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">
	<input id="check_358_agenda_gratuit" type="checkbox" name="Message[agenda_gratuit]">
    <input type="hidden" id="check_358_agenda_gratuit_value" class="check_358_agenda_gratuit_value" value="0"/>
        Module agenda gratuit         (basic, Premium, Platinium
    </td>
    <td class="td_color_2">Gratuit</td>
    <td class="td_color_1"><div id="check_358_agenda_gratuit_div"></div></td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">
	<input id="check_358_agenda_plus" type="checkbox" name="Message[agenda_plus]">
    <input type="hidden" id="check_358_agenda_plus_value" class="check_358_agenda_plus_value" value="0"/>
        Module agenda Plus        (Premium, Platinium)
    </td>
    <td class="td_color_2">€250</td>
    <td class="td_color_1"><div id="check_358_agenda_plus_div"></div></td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">
	<input id="check_358_web_ref1" type="checkbox" name="Message[web_ref1]">
    <input type="hidden" id="check_358_web_ref1_value" class="check_358_web_ref1_value" value="0"/>
        Module web + référencement 1ère année        (Premium, Platinium)
    </td>
    <td class="td_color_2">€250</td>
    <td class="td_color_1"><div id="check_358_web_ref1_div"></div></td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">
	<input id="check_358_web_ref_n" type="checkbox" name="Message[web_ref_n]">
    <input type="hidden" id="check_358_web_ref_n_value" class="check_358_web_ref_n_value" value="0"/>
        Module web autres années        (Premium, Platinium)
    </td>
    <td class="td_color_2">€100</td>
    <td class="td_color_1"><div id="check_358_web_ref_n_div"></div></td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">
	<input id="check_358_restauration" type="checkbox" name="Message[restauration]">
    <input type="hidden" id="check_358_restauration_value" class="check_358_restauration_value" value="0"/>
        Module  « Restauration »        (Premium, Platinium)
    </td>
    <td class="td_color_2">€100</td>
    <td class="td_color_1"><div id="check_358_restauration_div"></div></td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">&nbsp;</td>
    <td class="td_color_2">&nbsp;</td>
    <td class="td_color_1">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="td_color_2">&nbsp;</td>
    <td class="td_color_2">&nbsp;</td>
    <td class="td_color_1">&nbsp;</td>
  </tr>
  
  <tr>
    <td align="left" class="td_color_1" style="text-align:right;">&nbsp;</td>
    <td class="td_color_3">
    <strong>Montant ht</strong>
    </td>
    <td class="td_color_3"><div id="divMontantHT"></div><input type="hidden" id="hidemontantht" class="hidemontantht" name="Message[montantht]" value="0"/></td>
  </tr>
  <tr>
    <td align="left" class="td_color_1" style="text-align:right;">&nbsp;</td>
    <td class="td_color_3">
    <strong>T.V.A 20%</strong>
    </td>
    <td class="td_color_3"><div id="divMontantTVA"></div><input type="hidden" id="hidemontanttva" class="hidemontanttva" name="Message[montanttva]" value="0"/></td>
  </tr>
  <tr>
    <td align="left" class="td_color_1" style="text-align:right;">&nbsp;</td>
    <td class="td_color_3">
    <strong>Montant TTC</strong>
    </td>
    <td class="td_color_3"><div id="divMontantTTC"></div><input type="hidden" id="hidemontantttc" class="hidemontantttc" name="Message[montantttc]" value="0"/></td>
  </tr>
</table>
</div>
</div>


</div>
<div class="space_sub_pro">&nbsp;</div>


<div class="bloc_sub_pro">
<div class="title_sub_pro">Vous avez choisi un abonnement BASIC et/ou module agenda de base
</div>
<div style='
    font-family: "Arial",sans-serif;
    font-size: 12px;
    line-height: 1.23em;'>
Magazine Proximité : L'édition du mois en 3D L'espace professionnels L'espace particuliers Nos abonnements agenda-complet<br /><br />

Validez votre demande d’inscription, par retour de mail vous recevez une confirmation de réception.<br /><br />

Sous 48h00 après étude de votre demande et validation de notre service, nous vous confirmerons votre identifiant et votre mot de passe.<br /><br />

Muni de ces derniers, vous pourrez commencer à déposer vos données.<br /><br />

Attention : Le magazine Proximité peut refuser sans en donner la justification l’ouverture d’un compte qui ne correspondrait pas à son éthique. (Voir nos conditions générales).
</div>
</div>

<div class="bloc_sub_pro">
<div class="title_sub_pro">Vous avez choisi un abonnement Premium/Platinium et/ou un module payant
</div>
<div style='
    font-family: "Arial",sans-serif;
    font-size: 13px;
    line-height: 1.23em;'>
Validez votre demande d’inscription, par retour de mail vous recevez une confirmation de réception.<br /><br />

Sous 24h00 après étude de votre demande et validation de notre service, nous vous adresserons un document conforme à votre organisation administrative (précisez ci-dessous la qualité de ce document et vos conditions de règlement) :
</div>
</div>
<div class="space_sub_pro">&nbsp;</div>


<div style='
    font-family: "Arial",sans-serif;
    font-size: 12px; font-weight: bold;
    line-height: 1.23em;'>
<table border="0" cellspacing="5" cellpadding="5" style="margin-left:40px;">
  <tr>
    <td>
    <input id="check_2640" type="checkbox" name="Message[devis]" value="Devis"> Un devis
    </td>
    <td>
    <input id="check_2650" type="checkbox" name="Message[proforma]" value="Proforma"> Une facture proforma
    </td>
    <td>
    <input id="check_2660" type="checkbox" name="Message[facture]" value="Facture"> Une facture
    </td>
  </tr>
  <tr>
    <td>
    <input id="check_264" type="radio" name="Societe[Conditions_paiement]" value="Cheque"> Chèque
    </td>
    <td>
    <input id="check_265" type="radio" name="Societe[Conditions_paiement]" value="Virement"> Virement
    </td>
    <td>
    <input id="check_266" type="radio" name="Societe[Conditions_paiement]" value="Carte Bancaire avec Paypal"> Carte Bancaire avec Paypal
    </td>
  </tr>
</table>
</div>
<div style='
    font-family: "Arial",sans-serif;
    font-size: 13px; margin-top:20px;
    line-height: 1.23em;'>
Dès réception de votre règlement, nous ouvrirons vos droits et vous confirmerons votre identifiant et mot de passe.<br /><br />


Attention : Le magazine Proximité peut refuser sans en donner la justification l’ouverture d’un compte qui ne correspondrait pas à son éthique. (Voir nos conditions générales).</div>
<div class="space_sub_pro">&nbsp;</div>


<div style="margin:15px; padding:15px; font-weight:bold;">
<p>*Saisie obligatoire</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top"><input name="validationabonnement" id="validationabonnement" value="1" type="checkbox" tabindex="24"></td>
    <td>
    Je confirme* ma demande d'inscription et la validation des conditions générales.<br/>
Je souhaite recevoir des informations à propos du Club et du Magazine Proximité,     je peux me désinscrire à tout moment.
    </td>
  </tr>
</table>
</div>


<div style="text-align:center;">
<?php echo $captcha['image']; ?><br />
    <input name="captcha" value="" id="captcha" type="text" tabindex="25">
    <input name="divCaptchavalueverify" id="divCaptchavalueverify" value="" type="hidden" />
</div>


<div style="text-align:center; margin-top:20px; margin-bottom:20px;">
<input id="btnSinscrire" style="width: 227px; height: 30px;" name="envoyer" value="Validation" type="button" tabindex="26">
</div>

<div class="FieldError" id="divErrorFrmInscriptionProfessionnel" style="height: auto; color: red; font-family: arial; font-size: 12px; text-align:center;"></div>


<!--<div style="text-align:center;">
<img src="<?php //echo GetImagePath("front/"); ?>/wpimages2013/img_sb_pro_bottom.png">
</div>-->





</form>

</div>




<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>