<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("frontAout2013/includes/header_mini_2", $data); ?>

<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<script type="text/javascript">


function listeSousRubrique(){
         
        var irubId = jQuery('#RubriqueSociete').val();     
        jQuery.get(
            '<?php echo site_url("front/professionnels/testAjax"); ?>' + '/' + irubId,
            function (zReponse)
            {
                // alert (zReponse) ;
                jQuery('#trReponseRub').html("") ; 
                jQuery('#trReponseRub').html(zReponse) ;
               
             
           });
    }
	function getCP(){
	      var ivilleId = jQuery('#VilleSociete').val();
          jQuery.get(
            '<?php echo site_url("front/professionnels/getPostalCode"); ?>' + '/' + ivilleId,
            function (zReponse)
            {
                jQuery('#CodePostalSociete').val(zReponse);
           });
	}

    function getCP_localisation(){
        var ivilleId = jQuery('#IdVille_localisationSociete').val();
        jQuery.get(
                '<?php echo site_url("front/professionnels/getPostalCode_localisation"); ?>' + '/' + ivilleId,
                function (zReponse)
                {
                    jQuery('#trReponseVille_localisation').html(zReponse);


                });
    }
	
	function getLatitudeLongitudeLocalisation() {
			var geocoder =  new google.maps.Geocoder();
			var adresse_localisationSociete = $("#adresse_localisationSociete").val();
			if (adresse_localisationSociete != "") adresse_localisationSociete = adresse_localisationSociete+', '; 
			var Ville_localisationSociete = $("#IdVille_localisationSociete option:selected").text();
			var codepostal_localisationSociete = $("#codepostal_localisationSociete").val();
			geocoder.geocode( { 'address': adresse_localisationSociete+Ville_localisationSociete+', '+codepostal_localisationSociete+', fr'}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					//alert("location : " + results[0].geometry.location.lat() + " " +results[0].geometry.location.lng()); 
					$("#latitudeSociete").val(results[0].geometry.location.lat());
					$("#longitudeSociete").val(results[0].geometry.location.lng());
				} else {
					//alert("Something got wrong " + status);
				}
			});
		}
    
    function deleteFile(_IdCommercant,_FileName) {
        jQuery.ajax({
            url: '<?php echo base_url();?>front/professionnels/delete_files/' + _IdCommercant + '/' + _FileName,
            dataType: 'html',
            type: 'POST',
            async: true,
            success: function(data){
                window.location.reload();
            }
        });
		
    }
    
    jQuery(document).ready(function() {
        jQuery(".btnSinscrire").click(function(){
			//alert('test form submit');
            var txtError = "";
			
            var EmailSociete = jQuery("#EmailSociete").val();
            if(!isEmail(EmailSociete)) {
                txtError += "- L'adresse email de votre etablissement n'est pas valide.<br/>";
                $("#EmailSociete").css('border-color', 'red');
            } else {
                $("#EmailSociete").css('border-color', '#E3E1E2');
            }
			
			<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4' )) { ?>
			var Email_decideurSociete = jQuery("#Email_decideurSociete").val();
            if(!isEmail(Email_decideurSociete)) {
                txtError += "- L'adresse email du decideur n'est pas valide.<br/>";
                $("#Email_decideurSociete").css('border-color', 'red');
            } else {
                $("#Email_decideurSociete").css('border-color', '#E3E1E2');
            }
			<?php } ?>
            /*
            var LoginSociete = jQuery("#LoginSociete").val();
            if(!isEmail(LoginSociete)) {
                txtError += "1";
                jQuery("#divErrorLoginSociete").html("Votre login doit &ecirc;tre un email valide.");
                jQuery("#divErrorLoginSociete").show();
            } else {
                jQuery("#divErrorLoginSociete").hide();
            }
            */
			
			var HorairesSociete = $("#HorairesSociete").val();
            if(HorairesSociete=="") {
                txtError += "- Veuillez indiquer Vos Horaires<br/>";
                $("#HorairesSociete").css('border-color', 'red');
            } else {
                $("#HorairesSociete").css('border-color', '#E3E1E2');
            }
			
			var CaracteristiquesSociete = CKEDITOR.instances['CaracteristiquesSociete'].getData();
			//alert(CaracteristiquesSociete);
			if(CaracteristiquesSociete=="") {
                txtError += "- Veuillez indiquer la présentation de votre activité<br/>";
                $("#cke_CaracteristiquesSociete").css('border-color', 'red');
            } else {
                $("#cke_CaracteristiquesSociete").css('border-color', '#E3E1E2');
            }
			
			var Photo1Societe = $("#Photo1Societe").val();
            if(Photo1Societe=="") {
                txtError += "- Veuillez indiquer une photo<br/>";
                $("#Photo1Societe").css('color', 'red');
            } else {
                $("#Photo1Societe").css('color', '#E3E1E2');
            }
			
            if(txtError == "") {
				jQuery("#div_error_fiche_pro").hide();
                jQuery("#frmInscriptionProfessionnel").submit();
            } else {
				jQuery("#div_error_fiche_pro").html(txtError);
			}
			
			
        })
		
		
		//$('#bandeau_colorSociete').colorPicker();
		
		
		$("#adresse_localisation_diffuseur_checkbox").click(function(){
			if ($('#adresse_localisation_diffuseur_checkbox').attr('checked')) {
				$("#adresse_localisation_diffuseur").val("1");
				var adress_organisateur1 = $("#Adresse1Societe").val();
				var adress_organisateur2 = $("#Adresse2Societe").val();
				var IdVille_organisateur = $("#VilleSociete").val();
				var codepostal_organisateur = $("#CodePostalSociete").val();
				$("#adresse_localisationSociete").val(adress_organisateur1+" "+adress_organisateur2);
				$("#IdVille_localisationSociete").val(IdVille_organisateur);
				$("#codepostal_localisationSociete").val(codepostal_organisateur);
			} else {
				$("#adresse_localisation_diffuseur").val("0");
				$("#adresse_localisationSociete").val("");
				$("#IdVille_localisationSociete").val("0");
				$("#codepostal_localisationSociete").val("");
			}
		});
		
		
		
	/*	$( "#CodePostalSociete" ).keyup(function() {
		  
		  var CodePostalSociete = $(this).val();
          $.post(
				'<?php echo site_url("front/professionnels/getVille"); ?>' + '/' + ivilleId,
				{ CodePostalSociete: CodePostalSociete },
				function (zReponse)
				{
					//$('#CodePostalSociete').val(zReponse);
					alert(zReponse);
			   });
		  
		  
		});*/

		
    });
	
	
	
/*
$(document).off('blur', '#postcode').on('blur', '#postcode', function(e){
    if ($('#postcode').val() != "") {
        setTimeout(function() { 
            $('#postcode').parent().removeClass("form-error");      
            $('#postcode').parent().addClass("form-ok");
            //alert('ok');
        }
        ,500);
    }
});
*/


</script>


<style type="text/css">
.stl_long_input_platinum {
	width:413px;
}
.stl_long_input_platinum_td {
	width:180px;
	height:30px;
}
.div_stl_long_platinum {
	background-color:#3653A3;color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em;
    height:20px; padding-top:5px; padding-left:20px; margin-top:15px; margin-bottom:15px;
}
.div_error_taille_3_4 {
	color: #F00;
}
.FieldError {
	color:#FF0000;
	font-weight:bold;
}
</style>


<div style="text-align:left; background-image:url(<?php echo GetImagePath("front/");?>/wpimages2013/img_admin_platinium.png); background-repeat:no-repeat; height:200px;">
  <div style='color: #000054;
    font-family: "Vladimir Script",cursive;
    font-size: 48px;
    line-height: 47px; text-align:center; padding-left:200px; padding-top:25px;'>
        Page administrative<br/> 
        <?php if (isset($objAbonnementCommercant) && $user_groups->id=='5') { ?>
        "Platinium"
        <?php } else if (isset($objAbonnementCommercant) && $user_groups->id=='4') { ?>
        "Premium"
        <?php } else if (isset($objAbonnementCommercant) && $user_groups->id=='3') {?>
        "Basic"
        <?php } ?>
        
  </div>
  <div style="padding-left:400px; padding-top:30px;">
  <a href="<?php echo site_url("front/utilisateur/contenupro");?>" style="text-decoration:none;"><input type="button" value="Retour"/></a></div>
</div>


<div style=" padding-left:200px;">
<?php if (isset($mssg) && $mssg==1) {?> <div align="center" style="text-align:left; font-style:italic;color:#0C0;">Les modifications ont &eacute;t&eacute; enregistr&eacute;es</div><br/><?php }?>
</div>
<div style=" padding-left:120px;">
<?php if (isset($mssg) && $mssg!=1 && $mssg!="") {?> <div align="center" style="text-align:left; font-style:italic;color:#F00;">Une erreur est constatée, Veuillez  vérifier la conformité de vos données</div><br/><?php }?>
</div>
<?php //if(isset($objCommercant)) {  ?>
		   <!--<table  style="text-align:center;">
				<?php// if(isset($show_annonce_btn) && ($show_annonce_btn==1)) { ?>
				<tr>
					<td align="left"><a style="text-decoration:none;" href="<?php// echo site_url("front/annonce/listeMesAnnonces/$objCommercant->IdCommercant");?>"><input type ="button" style="width:200px;"id="btnannonce" value="gestions des annonces" onclick="document.location='<?php// echo site_url("front/annonce/listeMesAnnonces/$objCommercant->IdCommercant");?>';"/>  </a></td>
				</tr>
				<?php// } ?>
				<?php// if(isset($show_bonplan_btn) && ($show_bonplan_btn==1)) { ?>
				 <tr>
					<td align="left"><a  href="<?php// //echo site_url("front/bonplan/listeMesBonplans/$objCommercant->IdCommercant");?>"><input type ="button" style="width:200px;" id="btnbonplan" value="gestions des bons plans" onclick="document.location='<?php// echo site_url("front/bonplan/listeMesBonplans/$objCommercant->IdCommercant");?>';"/> </a></td>
				</tr>
				<?php// //} ?>
			</table>-->
		<?php //} ?>




<div>

<form name="frmInscriptionProfessionnel" id="frmInscriptionProfessionnel" action="<?php echo site_url("front/professionnels/modifier"); ?>" method="POST" enctype="multipart/form-data">

<div class="div_stl_long_platinum">Les coordonnées de votre établissement</div>


<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4' )) { ?>
  <tr>
    <td class="stl_long_input_platinum_td">
        Intégration de votre logo
    </td>
    <td>
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>
				<?php if(!empty($objCommercant->Logo)) { ?>
                    <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Logo, 100, 100,'',''); ?>
                    <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Logo');">Supprimer</a>
                <?php } ?><br/>
                <input type="file" name="Societelogo" id="logoSociete" value=""class="stl_long_input_platinum"/><br/>
                <span style='font-family: "Arial",sans-serif;
                font-size: 9px;
                line-height: 1.27em;'>(la largeur ne doit pas dépasser 270 pixels) fond transparent</span>
            </td>
          </tr>
        </table>
    </td>
  </tr>
 <tr> 
	<td class="stl_long_input_platinum_td">
        <label class="label">Lien logo : </label>
  	</td>
    <td>
        <input type="text" name="Societe[logo_url]" id="logo_url" value="<?php if(isset($objCommercant->logo_url) && $objCommercant->logo_url!="") echo htmlspecialchars($objCommercant->logo_url); else echo 'http://';?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<?php } ?>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">D&eacute;signation de votre activit&eacute; : </label>
  </td>
    <td>
        <input type="text" name="Societe[titre_entete]" id="titre_entete" value="<?php if(isset($objCommercant->titre_entete)) echo htmlspecialchars($objCommercant->titre_entete); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Rubrique : </label>
  </td>
    <td>
        <select name="AssCommercantRubrique[IdRubrique]" onchange="javascript:listeSousRubrique();" id="RubriqueSociete"class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colRubriques)) { ?>
                <?php foreach($colRubriques as $objRubrique) { ?>
                    <option <?php if(isset($objAssCommercantRubrique[0]->IdRubrique) && $objAssCommercantRubrique[0]->IdRubrique == $objRubrique->IdRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objRubrique->IdRubrique; ?>"><?php echo $objRubrique->Nom; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr id='trReponseRub'>
    <td class="stl_long_input_platinum_td">
        <label class="label">Sous-rubrique : </label>
  </td>
    <td>
    
    <?php if(isset($objAssCommercantRubrique[0]->IdRubrique) && $objAssCommercantRubrique[0]->IdRubrique !=0 && $objAssCommercantRubrique[0]->IdRubrique !="0" && $objAssCommercantRubrique[0]->IdRubrique !="" && $objAssCommercantRubrique[0]->IdRubrique !=NULL) { ?>
    	
        <?php 
		$this->load->model("SousRubrique");
		$colSousRubriques = $this->SousRubrique->GetByRubrique($objAssCommercantRubrique[0]->IdRubrique);
		?>
        
        <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete"class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colSousRubriques)) { ?>
                <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                    <option <?php if(isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?>  value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        
    <?php } else { ?>
    
        <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete"class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colSousRubriques)) { ?>
                <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                    <option <?php if(isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?>  value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo $objSousRubrique->Nom; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
        
    <?php } ?>
    </td>
</tr>
<?php   /**?>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Sous-rubrique : </label>
  </td>
    <td>
        <select name="AssCommercantSousRubrique[IdSousRubrique]" id="SousRubriqueSociete">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colSousRubriques)) { ?>
                <?php foreach($colSousRubriques as $objSousRubrique) { ?>
                    <option <?php if(isset($objAssCommercantSousRubrique[0]->IdSousRubrique) && $objAssCommercantSousRubrique[0]->IdSousRubrique == $objSousRubrique->IdSousRubrique) { ?>selected="selected"<?php } ?> value="<?php echo $objSousRubrique->IdSousRubrique; ?>"><?php echo htmlentities($objSousRubrique->Nom); ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<?php   */ ?>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Nom de la soci&eacute;t&eacute; : </label>
  </td>
    <td>
        <input type="text" name="Societe[NomSociete]" id="NomSociete" value="<?php if(isset($objCommercant->NomSociete)) echo htmlspecialchars($objCommercant->NomSociete); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4' )) { ?>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Statut : </label>
  </td>
    <td>
        <input type="text" name="Societe[statut]" id="statutSociete" value="<?php if(isset($objCommercant->statut)) echo htmlspecialchars($objCommercant->statut); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Capital : </label>
  </td>
    <td>
        <input type="text" name="Societe[capital]" id="capitalSociete" value="<?php if(isset($objCommercant->capital)) echo htmlspecialchars($objCommercant->capital); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<?php } ?>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Siret : </label>
  </td>
    <td>
        <input type="text" name="Societe[Siret]" id="SiretSociete" value="<?php if(isset($objCommercant->Siret)) echo htmlspecialchars($objCommercant->Siret); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Code APE : </label>
  </td>
    <td>
    	<input type="text" name="Societe[code_ape]" id="CodeApeSociete" value="<?php if(isset($objCommercant->code_ape)) echo htmlspecialchars($objCommercant->code_ape); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Adresse de l'&eacute;tablissement : </label>
  </td>
    <td>
        <input type="text" name="Societe[Adresse1]" id="Adresse1Societe" value="<?php if(isset($objCommercant->Adresse1)) echo htmlspecialchars($objCommercant->Adresse1); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Compl&eacute;ment d'Adresse : </label>
  </td>
    <td>
        <input type="text" name="Societe[Adresse2]" id="Adresse2Societe" value="<?php if(isset($objCommercant->Adresse2)) echo htmlspecialchars($objCommercant->Adresse2); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Code postal : </label>
  </td>
    <td id="trReponseVille">
        <input type="text" name="Societe[CodePostal]" id="CodePostalSociete" value="<?php if(isset($objCommercant->CodePostal)) echo htmlspecialchars($objCommercant->CodePostal); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Ville : </label>
  </td>
    <td>
        <select name="Societe[IdVille]" id="VilleSociete" class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colVilles)) { ?>
                <?php foreach($colVilles as $objVille) { ?>
                    <option <?php if(isset($objCommercant->IdVille) && $objCommercant->IdVille == $objVille->IdVille) { ?>selected="selected"<?php } ?> value="<?php echo $objVille->IdVille; ?>"><?php echo $objVille->Nom; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Commune : </label>
  </td>
    <td>
        <select name="Societe[id_commune]" id="VilleSociete" class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colCommune)) { ?>
                <?php foreach($colCommune as $objCommune) { ?>
                    <option <?php if(isset($objCommercant->id_commune) && $objCommercant->id_commune == $objCommune->id_commune) { ?>selected="selected"<?php } ?> value="<?php echo $objCommune->id_commune; ?>"><?php echo $objCommune->N_Com_min." - ".$objCommune->C_Com; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Departement : </label>
  </td>
    <td>
        <select name="Societe[departement_id]" id="VilleSociete" class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colDepartement)) { ?>
                <?php foreach($colDepartement as $objDepartement) { ?>
                    <option <?php if(isset($objCommercant->departement_id) && $objCommercant->departement_id == $objDepartement->departement_id) { ?>selected="selected"<?php } ?> value="<?php echo $objDepartement->departement_id; ?>"><?php echo $objDepartement->departement_nom." - ".$objDepartement->departement_code; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Latitude : (ex: -18.7972)</label>
  </td>
    <td>
        <input type="text" name="Societe[latitude]" id="latitudeSociete" value="<?php if(isset($objCommercant->latitude)) echo $objCommercant->latitude; ?>" class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Longitude : (ex: 47.4761)</label>
  </td>
    <td>
        <input type="text" name="Societe[longitude]" id="longitudeSociete" value="<?php if(isset($objCommercant->longitude)) echo htmlspecialchars($objCommercant->longitude); ?>" class="stl_long_input_platinum"/>
    </td>
</tr>


<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">T&eacute;l&eacute;phone fixe : </label>
  </td>
    <td>
        <input type="text" name="Societe[TelFixe]" id="TelFixeSociete" value="<?php if(isset($objCommercant->TelFixe)) echo htmlspecialchars($objCommercant->TelFixe); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">T&eacute;l&eacute;phone mobile : </label>
  </td>
    <td>
        <input type="text" name="Societe[TelMobile]" id="TelMobileSociete" value="<?php if(isset($objCommercant->TelMobile)) echo htmlspecialchars($objCommercant->TelMobile); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">T&eacute;l&eacute;phone Fax : </label>
  </td>
    <td>
        <input type="text" name="Societe[fax]" id="faxSociete" value="<?php if(isset($objCommercant->fax)) echo htmlspecialchars($objCommercant->fax); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Courriel : </label>
  </td>
    <td>
        <input type="text" name="Societe[Email]" id="EmailSociete" value="<?php if(isset($objCommercant->Email)) echo htmlspecialchars($objCommercant->Email); ?>"class="stl_long_input_platinum"/>
        <div class="FieldError" id="divErrorEmailSociete"></div>
    </td>
</tr>
<tr>
        <td class="stl_long_input_platinum_td">
            <label class="label">Site internet : </label>
      </td>
        <td>
            <input type="text" name="Societe[SiteWeb]" id="SiteWeb" value="<?php if(isset($objCommercant->SiteWeb) && $objCommercant->SiteWeb!="") echo htmlspecialchars($objCommercant->SiteWeb); else echo 'http://www.';  ?>"class="stl_long_input_platinum"/>
        </td>
</tr>
<tr>
        <td class="stl_long_input_platinum_td">
            <label class="label">URL Espace marchand : </label>
      </td>
        <td>
        <input type="text" name="Societe[page_web_marchand]" id="SiteWeb" value="<?php if(isset($objCommercant->page_web_marchand) && $objCommercant->page_web_marchand!="") echo htmlspecialchars($objCommercant->page_web_marchand); else echo 'http://www.'; ?>"class="stl_long_input_platinum"/>
        </td>
</tr>

<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4')) {?>


<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Lien Facebook : </label>
  </td>
    <td>
        <input type="text" name="Societe[Facebook]" id="FacebookSociete" value="<?php if(isset($objCommercant->Facebook)) echo htmlspecialchars($objCommercant->Facebook); ?>"class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Lien Twitter : </label>
  </td>
    <td>
    	<input type="text" name="Societe[google_plus]" id="google_plusSociete" value="<?php if(isset($objCommercant->google_plus)) echo htmlspecialchars($objCommercant->google_plus); ?>"class="stl_long_input_platinum"/>
  </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Lien Google + : </label>
  </td>
    <td>
        <input type="text" name="Societe[Twitter]" id="TwitterSociete" value="<?php if(isset($objCommercant->Twitter)) echo htmlspecialchars($objCommercant->Twitter); ?>" class="stl_long_input_platinum"/>
  </td>
</tr>
<?php } ?>

</table>
<br/><br/>

<div  class="div_stl_long_platinum">Les coordonnées du décideur</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
 <tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Civilit&eacute; : </label>
  </td>
    <td>
        <select name="Societe[Civilite]" id="CiviliteResponsableSociete" class="stl_long_input_platinum">
            <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "0") { ?>selected="selected"<?php } ?> value="0">Monsieur</option>
            <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "1") { ?>selected="selected"<?php } ?> value="1">Madame</option>
            <option <?php if(isset($objCommercant->Civilite) && $objCommercant->Civilite == "2") { ?>selected="selected"<?php } ?> value="2">Mademoiselle</option>
        </select>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Nom : </label>
  </td>
    <td>
        <input type="text" name="Societe[Nom]" id="NomResponsableSociete" value="<?php if(isset($objCommercant->Nom)) echo htmlspecialchars($objCommercant->Nom); ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Pr&eacute;nom : </label>
  </td>
    <td>
        <input type="text" name="Societe[Prenom]" id="PrenomResponsableSociete" value="<?php if(isset($objCommercant->Prenom)) echo htmlspecialchars($objCommercant->Prenom); ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Fonction <span class="indication">(G&eacute;rant, Directeur ...)</span>: </label>
  </td>
    <td>
        <input type="text" name="Societe[Responsabilite]" id="ResponsabiliteResponsableSociete" value="<?php if(isset($objCommercant->Responsabilite)) echo htmlspecialchars($objCommercant->Responsabilite); ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">T&eacute;l&eacute;phone direct <span class="indication">(fixe ou portable)</span> : </label>
  </td>
    <td>
        <input type="text" name="Societe[TelDirect]" id="TelDirectResponsableSociete" value="<?php if(isset($objCommercant->TelDirect)) echo htmlspecialchars($objCommercant->TelDirect); ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4' )) { ?>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Email Personnel : </label>
  </td>
    <td>
        <input type="text" name="Societe[Email_decideur]" id="Email_decideurSociete" value="<?php if(isset($objCommercant->Email_decideur)) echo htmlspecialchars($objCommercant->Email_decideur); ?>"class="stl_long_input_platinum"/>
        <div class="FieldError" id="divErrorEmail_decideurSociete"></div>
    </td>
</tr>
<?php } ?>
</table>

<div  class="div_stl_long_platinum">Votre abonnement</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

<tr>
    <td class="stl_long_input_platinum_td">
    Date de début
    : </td>
    <td>
    <?php if (isset($objAbonnementCommercant)) echo translate_date_to_fr($objAbonnementCommercant->DateDebut);?>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">Date de fin : </td>
    <td>
    <?php if (isset($objAbonnementCommercant)) echo translate_date_to_fr($objAbonnementCommercant->DateFin);?>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">Votre abonnement : </td>
    <td>
    <strong><?php 
	//echo $user_groups->id;
	if (isset($user_groups)) {
		if ($user_groups->id==3) echo "Basic";
		if ($user_groups->id==4) echo "Premium";
		if ($user_groups->id==5) echo "Platinium";
	}
	?></strong>
    </td>
</tr>
 <tr>
    <td>
        <label>Identifiant : </label>
    </td>
    <td>
        <input type="text" name="Societe[Login]" id="LoginSociete" value="<?php if(isset($objCommercant->Login)) echo htmlspecialchars($objCommercant->Login); ?>"  class="stl_long_input_platinum"/>
        <div class="FieldError" id="divErrorLoginSociete"></div>
    </td>
</tr>
<!--<tr>
    <td>
        <label>Mot de passe : </label>
    </td>
    <td>
        <input type="password" name="Societe[Password]" id="PasswordSociete" value="<?php// if(isset($objCommercant->Password)) echo htmlspecialchars($objCommercant->Password); ?>" class="stl_long_input_platinum" />
    </td>
</tr>-->

</table>

<div  class="div_stl_long_platinum">Vos données</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Présentation de votre activité : </label>
  </td>
    <td>
        <textarea name="Societe[Caracteristiques]" id="CaracteristiquesSociete" ><?php if(isset($objCommercant->Caracteristiques)) echo htmlspecialchars($objCommercant->Caracteristiques); ?></textarea>
        <?php echo display_ckeditor($ckeditor0); ?>
    </td>
</tr>
 <tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Heures et jours d’ouvertures : </label>
    </td>
    <td>
        <textarea  name="Societe[Horaires]" id="HorairesSociete" style="width:288px;"><?php if(isset($objCommercant->Horaires)) echo htmlspecialchars($objCommercant->Horaires); ?></textarea>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Fermeture annuelle : </label>
  </td>
    <td>
        <textarea  name="Societe[Vacances]" id="VacancesSociete" style="width:288px;"><?php if(isset($objCommercant->Vacances)) echo htmlspecialchars($objCommercant->Vacances); ?></textarea>
    </td>
</tr>
<!--<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Conditions de paiement : </label>
  </td>
    <td>
        <textarea  name="Societe[Conditions_paiement]" id="Conditions_paiement" style="width:288px;"><?php // if(isset($objCommercant->Conditions_paiement)) echo htmlspecialchars($objCommercant->Conditions_paiement); ?></textarea>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Fid&eacute;lisation : </label>
  </td>
    <td>
        <textarea  name="Societe[Conditions]" id="ConditionsSociete" style="width:288px;"><?php // if(isset($objCommercant->Conditions)) echo htmlspecialchars($objCommercant->Conditions); ?></textarea>
    </td>
</tr>-->

</table>


<div  class="div_stl_long_platinum">Intégration d'images</div>

<div style="padding:15px;">Vos images doit avoir les proportions 1 x 3/4 (par exemple 640x480, 800x600, 1024x768)</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 1 : </label>
        <input type="hidden" name="pdfAssocie" id="pdfAssocie" value="<?php if(isset($objCommercant->Pdf)) echo htmlspecialchars($objCommercant->Pdf); ?>" />
          <input type="hidden" name="photologoAssocie" id="photologoAssocie" value="<?php if(isset($objCommercant->Logo)) echo htmlspecialchars($objCommercant->Logo); ?>" />                                  
          <input type="hidden" name="photo1Associe" id="photo1Associe" value="<?php if(isset($objCommercant->Photo1)) echo htmlspecialchars($objCommercant->Photo1); ?>" />
          <input type="hidden" name="photo2Associe" id="photo2Associe" value="<?php if(isset($objCommercant->Photo2)) echo htmlspecialchars($objCommercant->Photo2); ?>" />
          <input type="hidden" name="photo3Associe" id="photo3Associe" value="<?php if(isset($objCommercant->Photo3)) echo htmlspecialchars($objCommercant->Photo3); ?>" />
          <input type="hidden" name="photo4Associe" id="photo4Associe" value="<?php if(isset($objCommercant->Photo4)) echo htmlspecialchars($objCommercant->Photo4); ?>" />
          <input type="hidden" name="photo5Associe" id="photo5Associe" value="<?php if(isset($objCommercant->Photo5)) echo htmlspecialchars($objCommercant->Photo5); ?>" />
          <input type="hidden" name="bandeau_topAssocie" id="bandeau_topAssocie" value="<?php if(isset($objCommercant->bandeau_top)) echo htmlspecialchars($objCommercant->bandeau_top); ?>" />
  </td>
  <?php 
  if (isset($mssg) && $mssg!='1'){
	  $img_error_verify_array = preg_split('/-/',$mssg);
  }
  
  ?>
    <td>
        <?php if(!empty($objCommercant->Photo1)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo1, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo1');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->Photo1)) { ?>
	        <input type="file" name="SocietePhoto1" id="Photo1Societe" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo1" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>

<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4')) {?>

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 2 : </label>
  </td>
    <td>
        <?php if(!empty($objCommercant->Photo2)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo2, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo2');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->Photo2)) { ?>
        	<input type="file" name="SocietePhoto2" id="Photo2Societe" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo2" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 3 : </label>
  </td>
    <td>
        <?php if(!empty($objCommercant->Photo3)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo3, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo3');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->Photo3)) { ?>
	        <input type="file" name="SocietePhoto3" id="Photo3Societe" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo3" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 4 : </label>
  </td>
    <td>
        <?php if(!empty($objCommercant->Photo4)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo4, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo4');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->Photo4)) { ?>
        	<input type="file" name="SocietePhoto4" id="Photo4Societe" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo4" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Photo 5 : </label>
  </td>
    <td>
        <?php if(!empty($objCommercant->Photo5)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->Photo5, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Photo5');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->Photo5)) { ?>
        	<input type="file" name="SocietePhoto5" id="Photo5Societe" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <div id="div_error_taille_Photo5" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("Photo5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>

<?php } ?>

</table>

<?php if (isset($objAbonnementCommercant) && $user_groups->id=='5') {?>
<div  class="div_stl_long_platinum">Votre entête et votre bannière (dimensions 900x455 pixels)</div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

<tr>
    <td class="stl_long_input_platinum_td">
        <label>Intégration d’une image : </label>
  </td>
    <td>
        <?php if(!empty($objCommercant->bandeau_top)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->bandeau_top, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','bandeau_top');">Supprimer</a>
        <?php } ?><br/>
        <input type="file" name="Societebandeau_top" id="bandeau_topSociete" value=""  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label>et définir une couleur (Texte ou fond) : </label>
  </td>
    <td>
    
        <link rel="stylesheet" href="<?php echo GetJsPath("front/") ; ?>/colorPicker/colorpicker.css" type="text/css" />
        <!--<link rel="stylesheet" media="screen" type="text/css" href="<?php// echo GetJsPath("front/") ; ?>/colorPicker/layout.css" />-->
        <style type="text/css">
        #colorSelector2 {
            position: relative;
            width: 36px;
            height: 36px;
            /*background: url(<?php// echo GetJsPath("front/") ; ?>/colorPicker/select2.png);*/
        }
        #colorSelector2 div {
            position: relative;
            width: 28px;
            height: 28px;
            background: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/select2.png) center;
        }
        #colorpickerHolder2 {
            width: 356px;
            height: 0;
            overflow: hidden;
            position: relative;
        }
        #colorpickerHolder2 .colorpicker {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_background.png);
            position: relative;
            bottom: 0;
            left: 0;
        }
        #colorpickerHolder2 .colorpicker_hue div {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_indic.gif);
        }
        #colorpickerHolder2 .colorpicker_hex {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hex.png);
        }
        #colorpickerHolder2 .colorpicker_rgb_r {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_r.png);
        }
        #colorpickerHolder2 .colorpicker_rgb_g {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_g.png);
        }
        #colorpickerHolder2 .colorpicker_rgb_b {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_rgb_b.png);
        }
        #colorpickerHolder2 .colorpicker_hsb_s {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_s.png);
            display: none;
        }
        #colorpickerHolder2 .colorpicker_hsb_h {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_h.png);
            display: none;
        }
        #colorpickerHolder2 .colorpicker_hsb_b {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_hsb_b.png);
            display: none;
        }
        #colorpickerHolder2 .colorpicker_submit {
            background-image: url(<?php echo GetJsPath("front/") ; ?>/colorPicker/custom_submit.png);
        }
        #colorpickerHolder2 .colorpicker input {
            color: #778398;
        }
        #customWidget {
            position: relative;
            height: 36px;
        }

        </style>
        <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/colorPicker/colorpicker.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/colorPicker/eye.js"></script>
        <script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/colorPicker/layout.js?ver=1.0.2"></script>
    
        <input type="hidden" name="Societe[bandeau_color]" id="bandeau_colorSociete" value="<?php if(isset($objCommercant->bandeau_color)) echo $objCommercant->bandeau_color; else echo '#3653A3'; ?>" />
        <div id="colorSelector2"><div style="background-color: <?php if(isset($objCommercant->bandeau_color)) echo $objCommercant->bandeau_color; else echo '#3653A3'; ?>"></div></div>
        <div id="colorpickerHolder2">
        </div>
    </td>
</tr>

</table>
<?php } ?>

<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4')) {?>

<div  class="div_stl_long_platinum">La localisation</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

<tr>
    <td class="stl_long_input_platinum_td">L’adresse est-elle celle<br/>précisée par le diffuseur : </td>
    <td>
    <input type="checkbox" name="adresse_localisation_diffuseur_checkbox" id="adresse_localisation_diffuseur_checkbox" <?php if(isset($objCommercant->adresse_localisation_diffuseur) && $objCommercant->adresse_localisation_diffuseur=="1") {?>checked<?php } ?>/>
    <input type="hidden" name="Societe[adresse_localisation_diffuseur]" id="adresse_localisation_diffuseur" value="<?php if(isset($objCommercant->adresse_localisation_diffuseur)) echo htmlspecialchars($objCommercant->adresse_localisation_diffuseur); ?>">
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Adresse : </label>
  </td>
    <td>
    	<input type="text" name="Societe[adresse_localisation]" id="adresse_localisationSociete" value="<?php if(isset($objCommercant->adresse_localisation)) echo $objCommercant->adresse_localisation; ?>"  class="stl_long_input_platinum" onChange="getLatitudeLongitudeLocalisation();"/>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Ville : </label>
  </td>
    <td>
        <select name="Societe[IdVille_localisation]" id="IdVille_localisationSociete" onchange = "getCP_localisation();getLatitudeLongitudeLocalisation();" class="stl_long_input_platinum">
            <option value="0">-- Veuillez choisir --</option>
            <?php if(sizeof($colVilles)) { ?>
                <?php foreach($colVilles as $objVille_localisation) { ?>
                    <option <?php if(isset($objCommercant->IdVille_localisation) && $objCommercant->IdVille_localisation == $objVille_localisation->IdVille) { echo 'selected="selected"'; $ville_to_show_onn_map = $objVille_localisation->Nom; } ?> value="<?php echo $objVille_localisation->IdVille; ?>"><?php echo $objVille_localisation->Nom; ?></option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Code Postal : </label>
  </td>
    <td id="trReponseVille_localisation">
    	<input type="text" name="Societe[codepostal_localisation]" id="codepostal_localisationSociete" value="<?php if(isset($objCommercant->codepostal_localisation)) echo $objCommercant->codepostal_localisation; ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr>
    <td colspan="2">
    <?php if (isset($ville_to_show_onn_map)) $ville_map = $ville_to_show_onn_map; else $ville_map =''; ?>
    <iframe width="630" height="483" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $objCommercant->adresse_localisation.", ".$objCommercant->codepostal_localisation." &nbsp;".$ville_map;?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $objCommercant->adresse_localisation.", ".$objCommercant->codepostal_localisation." &nbsp;".$ville_map;?>&amp;t=m&amp;vpsrc=0&amp;output=embed"></iframe>
    </td>
</tr>


</table>

<?php } ?>

<?php if (isset($objAbonnementCommercant) && $user_groups->id=='5') {?>

<div  class="div_stl_long_platinum">Integration de deux pages informatives</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Titre page 1 <span style="font-size:11px;">(Maximum 18 caractères et espaces)</span>:</label></td>
    <td>
    	<span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#F00;">&nbsp;&nbsp;(si ce champ est vide l'onglet de la page 1 est masqu&eacute;)</span><br/>
        <input type="text" name="Societe[labelactivite1]" id="labelactivite1Societe" value="<?php if(isset($objCommercant->labelactivite1)) echo htmlspecialchars($objCommercant->labelactivite1); ?>"  class="stl_long_input_platinum"/>
        <div class="FieldError" id="divErrorEmailSociete"></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Description page 1 : </label>
  </td>
    <td>
        <textarea name="Societe[activite1]" id="activite1Societe"><?php if(isset($objCommercant->activite1)) echo htmlspecialchars($objCommercant->activite1); ?></textarea>
        <?php echo display_ckeditor($ckeditor1); ?>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 1 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite1_image1)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite1_image1, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant;?>','activite1_image1');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->activite1_image1)) { ?>
        	<input type="file" name="activite1_image1" id="activite1_image1" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <input type="hidden" name="activite1_image1Associe" id="activite1_image1Associe" value="<?php if(isset($objCommercant->activite1_image1)) echo htmlspecialchars($objCommercant->activite1_image1); ?>" />
        <div id="div_error_taille_activite1_image1" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 2 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite1_image2)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite1_image2, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image2');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->activite1_image2)) { ?>
        	<input type="file" name="activite1_image2" id="activite1_image2" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <input type="hidden" name="activite1_image2Associe" id="activite1_image2Associe" value="<?php if(isset($objCommercant->activite1_image2)) echo htmlspecialchars($objCommercant->activite1_image2); ?>" />
        <div id="div_error_taille_activite1_image2" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 3 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite1_image3)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite1_image3, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image3');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->activite1_image3)) { ?>
        	<input type="file" name="activite1_image3" id="activite1_image3" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <input type="hidden" name="activite1_image3Associe" id="activite1_image3Associe" value="<?php if(isset($objCommercant->activite1_image3)) echo htmlspecialchars($objCommercant->activite1_image3); ?>" />
        <div id="div_error_taille_activite1_image3" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 4 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite1_image4)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite1_image4, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image4');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->activite1_image4)) { ?>
        	<input type="file" name="activite1_image4" id="activite1_image4" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <input type="hidden" name="activite1_image4Associe" id="activite1_image4Associe" value="<?php if(isset($objCommercant->activite1_image4)) echo htmlspecialchars($objCommercant->activite1_image4); ?>" />
        <div id="div_error_taille_activite1_image4" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 5 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite1_image5)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite1_image5, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite1_image5');">Supprimer</a>
        <?php } ?><br/>
        <?php if(empty($objCommercant->activite1_image5)) { ?>
        	<input type="file" name="activite1_image5" id="activite1_image5" value=""  class="stl_long_input_platinum"/>
        <?php } ?>
        <input type="hidden" name="activite1_image5Associe" id="activite1_image5Associe" value="<?php if(isset($objCommercant->activite1_image5)) echo htmlspecialchars($objCommercant->activite1_image5); ?>" />
        <div id="div_error_taille_activite1_image5" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite1_image5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>

</table>

<div style="padding:15px;">Vos images doit avoir les proportions 1 x 3/4 (par exemple 640x480, 800x600, 1024x768)</div>

<div style="height:3px; background-color:#3653A3; margin-top:20px; margin-bottom:20px;"></div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">

<tr>
    <td class="stl_long_input_platinum_td">Titre page 2 <span style="font-size:11px;">(Maximum 18 caractères et espaces)</span>:</td>
    <td>
    	<span style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#F00;">&nbsp;&nbsp;(si ce champ est vide l'onglet de la page 1 est masqu&eacute;)</span><br/>
        <input type="text" name="Societe[labelactivite2]" id="labelactivite2Societe" value="<?php if(isset($objCommercant->labelactivite2)) echo htmlspecialchars($objCommercant->labelactivite2); ?>"  class="stl_long_input_platinum"/>
        <div class="FieldError" id="divErrorEmailSociete"></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Description page 2 : </label>
  </td>
    <td>
        <textarea name="Societe[activite2]" id="activite2Societe" ><?php if(isset($objCommercant->activite2)) echo htmlspecialchars($objCommercant->activite2); ?></textarea>
        <?php echo display_ckeditor($ckeditor2); ?>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 1 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite2_image1)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite2_image1, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image1');">Supprimer</a>
        <?php } ?><br/>
        <input type="file" name="activite2_image1" id="activite2_image1" value=""  class="stl_long_input_platinum"/>
        <input type="hidden" name="activite2_image1Associe" id="activite2_image1Associe" value="<?php if(isset($objCommercant->activite2_image1)) echo htmlspecialchars($objCommercant->activite2_image1); ?>" />
        <div id="div_error_taille_activite2_image1" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image1", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 2 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite2_image2)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite2_image2, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image2');">Supprimer</a>
        <?php } ?><br/>
        <input type="file" name="activite2_image2" id="activite2_image2" value=""  class="stl_long_input_platinum"/>
		<input type="hidden" name="activite2_image2Associe" id="activite2_image2Associe" value="<?php if(isset($objCommercant->activite2_image2)) echo htmlspecialchars($objCommercant->activite2_image2); ?>" />        
        <div id="div_error_taille_activite2_image2" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image2", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 3 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite2_image3)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite2_image3, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image3');">Supprimer</a>
        <?php } ?><br/>
        <input type="file" name="activite2_image3" id="activite2_image3" value=""  class="stl_long_input_platinum"/>
        <input type="hidden" name="activite2_image3Associe" id="activite2_image3Associe" value="<?php if(isset($objCommercant->activite2_image3)) echo htmlspecialchars($objCommercant->activite2_image3); ?>" />
        <div id="div_error_taille_activite2_image3" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image3", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 4 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite2_image4)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite2_image4, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image4');">Supprimer</a>
        <?php } ?><br/>
        <input type="file" name="activite2_image4" id="activite2_image4" value=""  class="stl_long_input_platinum"/>
        <input type="hidden" name="activite2_image4Associe" id="activite2_image4Associe" value="<?php if(isset($objCommercant->activite2_image4)) echo htmlspecialchars($objCommercant->activite2_image4); ?>" />
        <div id="div_error_taille_activite2_image4" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image4", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Integration image 5 : </label>
  </td>
    <td>
    	<?php if(!empty($objCommercant->activite2_image5)) { ?>
            <?php echo image_thumb("application/resources/front/photoCommercant/images/" . $objCommercant->activite2_image5, 100, 100,'',''); ?>
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','activite2_image5');">Supprimer</a>
        <?php } ?><br/>
        <input type="file" name="activite2_image5" id="activite2_image5" value=""  class="stl_long_input_platinum"/>
        <input type="hidden" name="activite2_image5Associe" id="activite2_image5Associe" value="<?php if(isset($objCommercant->activite2_image5)) echo htmlspecialchars($objCommercant->activite2_image5); ?>" />
        <div id="div_error_taille_activite2_image5" class="div_error_taille_3_4"><?php if (isset($img_error_verify_array) && in_array("activite2_image5", $img_error_verify_array)) echo 'Merci de vérifier les dimensions de votre image.';?></div>
    </td>
</tr>

</table>

<div style="padding:15px;">Vos images doit avoir les proportions 1 x 3/4 (par exemple 640x480, 800x600, 1024x768)</div>

<?php } ?>


<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4')) {?>

<div  class="div_stl_long_platinum">Integration Multimedia</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Lien vid&eacute;o : </label>
  </td>
    <td>
    	<span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">&nbsp;&nbsp;&nbsp;Format : http://www.youtube.com/watch?v=gvGymDhY49E </span><br/>
        <input type="text" name="Societe[Video]" id="VideoSociete" value="<?php if(isset($objCommercant->Video)) echo htmlspecialchars($objCommercant->Video); ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
<tr valign="middle">
    <td class="stl_long_input_platinum_td" valign="top">
        <label class="label">Document PDF : </label>
  </td>
    <td  valign="top">
        <?php if(!empty($objCommercant->Pdf)) { ?>
            <?php //echo $objCommercant->Pdf; ?>
            
            <?php if ($objCommercant->Pdf != "" && $objCommercant->Pdf != NULL && file_exists("application/resources/front/photoCommercant/images/".$objCommercant->Pdf)== true) {?><a href="<?php echo base_url(); ?>application/resources/front/photoCommercant/images/<?php echo $objCommercant->Pdf ; ?>" target="_blank"><img src="<?php echo base_url(); ?>application/resources/front/images/pdf_icone.png" width="64" alt="PDF" /></a><?php } ?>
            
            
            <a href="javascript:void(0);" onclick="deleteFile('<?php echo $objCommercant->IdCommercant; ?>','Pdf');">Supprimer</a><br/>
        
        <input type="file" name="SocietePdf" id="PdfSociete" value="" style="display:none;"  class="stl_long_input_platinum"/>
    <?php } else { ?>
        <input type="file" name="SocietePdf" id="PdfSociete" value=""  class="stl_long_input_platinum"/>
    <?php } ?>
  </td>
</tr>
<tr>
    <td class="stl_long_input_platinum_td">&nbsp;
        
  </td>
    <td>
    	<span style="font-size:11px"> Préciser le titre de votre document : ex : plaquette commerciale</span><br/>
        <input type="text" name="Societe[titre_Pdf]" id="titre_PdfSociete" value="<?php if(isset($objCommercant->titre_Pdf)) echo htmlspecialchars($objCommercant->titre_Pdf); ?>"  class="stl_long_input_platinum"/>
    </td>
</tr>
</table>

<?php } ?>

<?php if (isset($user_groups) &&( $user_groups->id=='5' || $user_groups->id=='4')) {?>

<div  class="div_stl_long_platinum">Integration de mots clés</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
<tr>
    <td class="stl_long_input_platinum_td">
        <label class="label">Mots cl&eacute;s :<br />(&agrave; s&eacute;parer par des virgules) </label>
    </td>
    <td>
        <textarea  name="Societe[metatag]" id="metatag" style="width:400px; height:150px;"><?php if(isset($objCommercant->metatag)) echo htmlspecialchars($objCommercant->metatag); ?></textarea>
    </td>
</tr>
</table>

<?php } ?>

<?php if (isset($user_groups) && $user_groups->id=='5') {?>

<div  class="div_stl_long_platinum">Pages annonces</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:10px;">
<tr>
    <td class="stl_long_input_platinum_td" valign="top">
        <label class="label">Titre du menu Annonce</label>
    </td>
    <td>
        <input type="text" name="Societe[textmenuannonce]" id="textmenuannonce" value="<?php if(isset($objCommercant->textmenuannonce)) echo htmlspecialchars($objCommercant->textmenuannonce); ?>"  class="stl_long_input_platinum"/>
        <p><span style="font-size:11px">"Nos annonces" est affiché si le menu pour les annonces est vide</span></p>
    </td>
</tr>
</table>

<?php } ?>

<div style="height:40px; background-color:#3653A3; margin-top:40px; margin-bottom:20px; text-align:center; padding-top:15px;"><input type="button" class="btnSinscrire" value="Validation des Modifications" /></div>

<div id="div_error_fiche_pro" class="div_error_fiche_pro" style="color:#FF0000; font-weight:bold; margin-top:20px; margin-bottom:20px; margin-left:40px; margin-right:40px;"></div>

</form>
<div style="text-align:left;background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages2013/bg_page_conso_pro_souri.png); background-repeat:no-repeat; background-position:center center;">
<table border="0" cellspacing="5" cellpadding="5" style="text-align:center;">
  <tr>
    <td>
    <a href="http://www.creezvotresiteweb.fr/aideinscription.html" target="_blank" alt="help"><img src="<?php echo GetImagePath("front/");?>/iconeaide.png" width="189" height="183" alt="logo" title="" style="z-index:1;"/></a>
    </td>
    <td>
    <a href="<?php echo site_url($objCommercant->nom_url."/presentation");?>" target="_blank" onClick="var w = window.open(this.href,'_blank','width=950,height=1200,menubar=no,resizable=no,scrollbars=yes,status=no,toolbar=no'); if( w != null ){ w.focus(); }; return false;" alt="show">
    <img src="<?php echo GetImagePath("front/");?>/visualisezsite.png" width="206" height="166" alt="logo" title="" style="z-index:1;"/>
    </a>
    </td>
  </tr>
</table>
</div>


<div style="background-color:#3653A3; text-align:center; padding-top:20px; padding-bottom:20px; margin-top:20px; margin-bottom:200px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-left:15px; padding-right:15px; text-align:center;">
  <tr>
    <td>
    <?php if (isset($user_groups) &&( $user_groups->id=='5' ||$user_groups->id=='4' )) { ?>
    <img src="<?php echo GetImagePath("front/");?>/wpimages2013/wp6487c1fd_06.png" width="54" alt="bonplan" />
    <?php } ?>
    </td>
    <td>
    <?php if (isset($user_groups) &&( $user_groups->id=='5' ||$user_groups->id=='4' )) { ?>
    <img src="<?php echo GetImagePath("front/");?>/wpimages2013/wp4f4ef7d1_06.png" alt="bonplan" />
    <?php } ?>
    </td>
    <td>
    <?php if (isset($objAbonnementCommercant) && $user_groups->id=='5') { ?>
    <img src="<?php echo GetImagePath("front/");?>/wpimages2013/wp3b216ecc_06.png" width="54" alt="bonplan" />
    <?php } ?>
    </td>
    
  </tr>
  <tr>
    <td>
    <?php if (isset($user_groups) &&( $user_groups->id=='5' ||$user_groups->id=='4' )) { ?>
    <a href="<?php echo site_url('front/bonplan/listeMesBonplans/'.$userId);?>" style="text-decoration:none;">
    <input type="button" name="" id="" class="" value="Module bons plans" style="width:162px;">
    </a>
    <?php } ?>
    </td>
    <td>
    <?php if (isset($user_groups) &&( $user_groups->id=='5' ||$user_groups->id=='4' )) { ?>
    <a href="http://www.proximite-magazine.com/admin/qrcode.html" style="text-decoration:none;">
    <input type="button" name="input2" id="input2" class="" value="Module QRCODES" style="width:162px;">
    </a>
    <?php } ?>
    </td>
    <td>
    <?php if (isset($objAbonnementCommercant) && $user_groups->id=='5') { ?>
    <a href="<?php echo site_url('front/annonce/listeMesAnnonces/'.$userId);?>" style="text-decoration:none;">
    <input type="button" name="input" id="input" class="" value="Module annonces" style="width:162px;">
    </a>
    <?php } ?>
    </td>
    
  </tr>
</table>

<?php if (isset($objAbonnementCommercant) && $user_groups->id == '3') {?>

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="text-align:center; margin-bottom:20px; margin-top:20px;">
  <tr>
    <td colspan="2">
    <p style='color: #FFFFFF;
    font-family: "Arial",sans-serif;
    font-size: 21px;
    font-weight: 700;
    line-height: 1.19em;'>UPGRADEZ votre abonnement Basic<br/>
et découvrez de nouveaux modules et fonctions<br/>
pour booster votre activité….</p>

<style type="text/css">
.button_href_acces_comparaison {
	height: 30px;
    width: 274px;
    background-image: url("<?php echo GetImagePath("front/");?>/wpimages2013/wpab142e92_06.png");
    background-position: 0 0;
    display: block;
    text-decoration: none;
    color: #000000;
}
.button_href_acces_comparaison:hover {
	background-position:0 -30px;
}
.button_href_acces_comparaison span {
	color: #000000;
    cursor: pointer;
    display: block;
    font-family: Arial,sans-serif;
    font-size: 12px;
    font-style: normal;
    font-weight: normal;
    height: 15px;
    text-align: center;s
    text-decoration: none;
    text-transform: none;
    width: 231px;
	padding-left:20px;
}
</style>
<div style="text-align:center; padding-left:200px; margin-bottom:30px;">
<a href="<?php echo site_url('front/utilisateur/comparerabonnements');?>" class="button_href_acces_comparaison">
<span style="padding-top:5px; margin-top:5px;">Accès à la page comparative   </span>
</a>
</div>    
    </td>
  </tr>
  <tr>
    <td><a href="<?php echo site_url("front/utilisateur/infospremium/");?>"><img src="<?php echo GetImagePath("front/");?>/wpimages2013/img_abonnement_premium.png" alt="bonplan" /></a>
    </td>
    <td><a href="<?php echo site_url("front/utilisateur/infosplatinium/");?>"><img src="<?php echo GetImagePath("front/");?>/wpimages2013/img_abonnement_platinum.png" alt="bonplan" /></a>
    </td>
  </tr>
</table>

<?php } ?>

</div>


</div>




<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>