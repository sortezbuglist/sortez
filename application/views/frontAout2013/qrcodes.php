<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("frontAout2013/includes/header_mini_2", $data); ?>

<div style="text-align:center; z-index:1;padding-top:30px; padding-bottom:15px;">
    <span style='color: #000000;
        font-family: "Vladimir Script",cursive;
        font-size: 40px;
        line-height: 47px;'>Genrer d'un simple clic votre QRCODE</span>
    </div>
    
<div style="text-align:center;"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_pro.png" alt=""  border="0" ></div>    


<div style="margin-left:15px;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <div>
    <strong>Le QRCODE</strong> est un type de code-barres en deux dimensions constitué de modules noirs disposés dans un carré à fond blanc. QR veut dire en anglais Quick Response, car le contenu du code peut être décodé rapidement.<br/>
Destiné à être lu par un lecteur de code-barres,  un smartphone, ou encore une tablette tactile, il a l’avantage de pouvoir stocker plus d’informations qu’un code barres traditionnel, et surtout des données directement reconnues par des applications, permettant ainsi de déclencher facilement des actions comme :

<ul>
    <li>Naviguer vers un site internet,  une page web mobile.</li>
    <li>Ajouter une carte de visite virtuelle (vCard), dans les contacts, ou un événement (iCalendar) dans l’agenda électronique ;</li>
    <li>Déclencher un appel vers un numéro de téléphone ou envoyer un SMS ;</li>
    <li>Montrer un point géographique sur Google Maps ;</li>
    <li>Encoder un texte libre ;</li>
</ul>
</div>    
    </td>
    <td width="244"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp57a43d26_06.png" alt=""  border="0" ></td>
  </tr>
</table>
</div>

<style type="text/css">
.width_td {
	padding-left:15px;
}
</style>

<div style=' padding:15px;font-family: "Verdana",sans-serif;
    font-size: 13px;
    line-height: 1.23em;'>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="50%">
    
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="45"><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp4d4a8e1a_06.png" alt=""  border="0" ></td>
    <td class="width_td">Choisissez l’URL d’une page de votre site qui vous convient</td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpd2f9c079_06.png" alt=""  border="0" ></td>
    <td class="width_td">Intégrez le dans le champ pré-cisé ci-dessus&nbsp;</td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp6d812260_06.png" alt=""  border="0" ></td>
    <td class="width_td">Validez sur le bouton « submit »</td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp3d09b03e_06.png" alt=""  border="0" ></td>
    <td class="width_td">Votre QRCODE apparaît</td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wpccb97080_06.png" alt=""  border="0" ></td>
    <td class="width_td">Il vous suffit de cliquer sur le bouton droit de votre souris et d’enregistrer cette image</td>
  </tr>
  <tr>
    <td><img src="<?php echo GetImagePath("front/"); ?>/wpimages2013/wp5f373199_06.png" alt=""  border="0" ></td>
    <td class="width_td">Intégrer ce QRCODE sur vos documents
commerciaux</td>
  </tr>
</table>

    
    </td>
    <td width="50%" style="text-align:right;">
    <iframe src="<?php echo base_url();?>application/resources/front/qrcode/a.htm" marginheight="0" marginwidth="0" frameborder="0" height="420px" scrolling="no" width="280px"></iframe>
    </td>
  </tr>
</table>

</div>








<?php $this->load->view("frontAout2013/includes/footer_mini_2"); ?>