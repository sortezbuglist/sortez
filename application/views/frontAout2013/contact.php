<?php $data["zTitle"] = 'Accueil' ?>
<?php $this->load->view("adminAout2013/includes/vwHeader2013", $data); ?>



<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jspngfix.js"></script>
<script type="text/javascript" src="<?php echo GetJsPath("front/") ; ?>/fields.check.js"></script>

<script type="text/javascript">var blankSrc = "wpscripts/blank.gif";
</script>

<script type="text/javascript">
	
		 
		 
		 // Use jQuery via $(...)
		 $(document).ready(function(){//debut ready fonction
			   
		/*//verify Nom	   
		$("#nomcontact").focus(function(){
									$(this).val("");			 
								});
		$("#nomcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre nom*");
									}
								});	
		
		//verify Prénom	   
		$("#prenomcontact").focus(function(){
									$(this).val("");			 
								});
		$("#prenomcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre prénom*");
									}
								});	
		//verify mail	   
		$("#emailcontact").focus(function(){
									$(this).val("");			 
								});
		$("#emailcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre courriel*");
									}
								});	
		//verify tel	   
		$("#telcontact").focus(function(){
									$(this).val("");			 
								});
		$("#telcontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre numéro de téléphone*");
									}
								});	
		//verify mess	   
		$("#contenucontact").focus(function(){
									$(this).val("");			 
								});
		$("#contenucontact").blur(function(){
									if ($(this).val() == "") {
										$(this).val("Votre message*");
									}
								});	*/
			   
			   
		//verify field form value 
		$("#btnEnvoicontact").click(function(){
										  
				var txtError = "";
				
				var nomcontact = $('#nomcontact').val();
				  if (nomcontact == "" || nomcontact == "Votre nom*") {
					txtError += "- Vous devez préciser votre Nom<br/>";
				  }  
				
				var prenomcontact = $('#prenomcontact').val();
				  if (prenomcontact == "" || prenomcontact == "Votre prénom*") {
					txtError += "- Vous devez préciser votre prénom<br/>";
				  } 
				
				var emailcontact = $('#emailcontact').val();
				  if (emailcontact == "" || emailcontact == "Votre courriel*") {
					txtError += "- Vous devez préciser votre email<br/>";
				  }
				  if(!isEmail(emailcontact)) {
					txtError += "- Veuillez entrer un email valide.<br/>";
				  }
				  
				var telcontact = $('#telcontact').val();
				  if (telcontact == "" || telcontact == "Votre numéro de téléphone*") {
					txtError += "- Vous devez préciser votre téléphone<br/>";
				  }
				  
				var contenucontact = $('#contenucontact').val();
				  if (contenucontact == "" || contenucontact == "Votre message*") {
					txtError += "- Vous devez préciser le contenu de votre message<br/>";
				  }
				
				if ($('#acceptenews0').is(":checked")) {
					$("#acceptenews").val("1");
				} else {
					$("#acceptenews").val("0");
				}
				
				
				//verify captcha
				var captcha = $('#captcha').val();
				  if (captcha == "") {
					txtError += "- Vous devez remplir le captcha<br/>";
				  } else {
					  $.ajax({
                                    type: "GET",
                                    url: "<?=base_url(); ?>front/professionnels/verify_captcha/"+captcha,
                                    success: function(msg){
                                        //alert(msg);
                                        var numero_reponse = msg;
                                        //alert(numero_reponse);
										$("#divCaptchavalueverify").val(numero_reponse);
                                    }
                             });
                             
				  }
				  
				 var divCaptchavalueverify = $('#divCaptchavalueverify').val();
				  if (divCaptchavalueverify == "0") {
					txtError += "- Les Textes que vous avez entré ne sont pas valides.<br/>";
				  } 
				// end verify captcha  
				  
				
				//final verification of input error
				if(txtError == "") {
					$("#formContact").submit();
				} else {
					$("#divErrorFrmContact").html(txtError);
				}
			})
		
		
		
		
		
    })
		 
		 
    </script>

<style type="text/css">
.input_long_contact {
	width:420px;
}
</style>

<div style='margin-top:30px; margin-bottom:20px; color: #000000;
    font-family: "Vladimir Script",cursive; text-align:center;
    font-size: 48px;
    line-height: 47px;'>
    Plus d’infos… adressez nous un formulaire
</div>

<div style="text-align:center;">
<div style='margin-left: auto; margin-right: auto; width:850px; margin-top:20px; margin-bottom:20px; background-color:#C8C8C8; padding-top:30px;
	font-family: "Arial",sans-serif;
    font-size: 13px;
    font-weight: 700;
    line-height: 1.23em;'>
<form id="formContact" name="formContact" action="<?php echo site_url('front/contact/envoyer/'); ?>" method="post">
  <input type="hidden" name="contact[acceptenews]" id="acceptenews" value=""/>
  <center>
<table width="730px" border="0">
  <!--<tr>
    <td colspan="2">Merci de bien vouloir préciser vos coordonnées et votre demande<br/>
        <span style='font-family: "Verdana",sans-serif;
    font-size: 9px;
    line-height: 1.33em;'>* champs obligatoires</span></td>
  </tr>-->
  <tr>
    <td width="50%">Nom, Prénom</td>
    <td width="50%"><input id="nomcontact" name="contact[nom]" tabindex="1" maxlength="50" value="" type="text" class="input_long_contact"></td>
  </tr>
  <tr>
    <td>Nom de votre structure</td>
    <td><input id="prenomcontact" name="contact[prenom]" tabindex="2" maxlength="50" value=""  class="input_long_contact" type="text"></td>
  </tr>
  <tr>
  	<td>Adresse e-mail</td>
    <td><input id="emailcontact" name="contact[email]" tabindex="3" maxlength="100" value=""  class="input_long_contact" type="text"></td>
  </tr>
  <tr>
    <td>Téléphone</td>
    <td><input id="telcontact" name="contact[tel]" maxlength="100" tabindex="4" value=""  class="input_long_contact" type="text"></td>
  </tr>
  <tr>
  	<td>Commentaires</td>
    <td><textarea id="contenucontact" rows="8" cols="60" name="contact[contenu]" tabindex="5" style="height: 134px;"  class="input_long_contact"></textarea></td>
  </tr>
  <tr height="50">
  	<td><span style='color: #A70000;
    font-family: "Arial",sans-serif;
    font-size: 21px;
    font-weight: 700;
    line-height: 1.19em;'>Vos centres d’intérêts</span></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2">
        <table width="100%" border="0" cellspacing="3" cellpadding="3">
          <tr>
            <td>Magazine Proximité</td>
            <td><input id="check_1" type="checkbox" name="contact_interet[proximite]"></td>
            <td>L’agenda</td>
            <td style="text-align:right"><input id="check_7" type="checkbox" name="contact_interet[agenda]"></td>
          </tr>
          <tr>
            <td>L’annuaire</td>
            <td><input id="check_6" type="checkbox" name="contact_interet[annuaire]"></td>
            <td>Notre sélection</td>
            <td style="text-align:right"><input id="check_8" type="checkbox" name="contact_interet[selection]"></td>
          </tr>
        </table>
    </td>
  </tr>
  <!--<tr>
    <td colspan="2">
      <div id="txt_47" style="width:472px;height:45px;overflow:hidden;">
        <p class="Corps-P"><input id="acceptenews0" name="acceptenews0" tabindex="6" type="checkbox"> <span class="Corps-C-C0">J’accepte de recevoir par courriel des informations provenant de cet établissement.</span></p>
    </div></td>
  </tr>-->
  <tr height="30">
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
    <div style="width:190px;height:69px;">
    
    
    
    <?php echo $captcha['image']; ?><br />
    <input name="captcha" value="" id="captcha" type="text" tabindex="7">
    <input name="divCaptchavalueverify" id="divCaptchavalueverify" value="" type="hidden" />
    
    
    </div>
    </td>
  </tr>
  <tr height="30">
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td>
    <input style="width: 87px; height: 22px; margin-right:30px;" id="butn_2" value="Rétablir" type="reset" tabindex="9">
    <input style="width: 90px; height: 22px;" id="btnEnvoicontact" name="btnEnvoicontact" value="Envoyer" type="button" tabindex="8">
    </td>
  </tr>
  <tr>
    <td colspan="2"><div id="divErrorFrmContact" style="width:730px; height: auto; overflow:hidden; color:#F00; font-family:Arial, Helvetica, sans-serif; font-size:11px; margin-top:15px; margin-bottom:15px;"></div></td>
  </tr>
</table>
</center>

<script type="text/javascript" src="<?php echo GetImagePath("front/"); ?>/jsValidation.js"></script>
</form>
</div>
</div>

<div style="text-align:center;"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/img_tel_contact.png" alt=""></div>


<!--<div id="txt_45" style="position:absolute; left:67px; top:598px; width:251px; height:40px; overflow:hidden;">
<span class="Corps-C">Merci de saisir exactement les caractères qui s’affichent dans l’image</span>
</div>-->
<div id="txt_49" style="text-align:center; font-weight:bold;">

Le  magazine Proximité est une marque déposée<br/>
Editeur : Phase SARL 60 avenue de Nice 06800 Cagnes-sur-Mer  Sarl au capital de 7265 € - RCS Antibes 412.071.466<br/>
Téléphone : 00 33 (0)4 93 73 84 51 - Mobile : 00 33 (0)6 72 05 59 35 - proximite-magazine@orange.fr

</div>




<?php $this->load->view("adminAout2013/includes/vwFooter2013"); ?>