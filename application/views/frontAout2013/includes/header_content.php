<style type="text/css">
img {
	border:none;
}
.main_menu_aout2013 {
	float: left;
	height: 46px;
	width: 1024px;
	position: relative;
}
.main_menu_img {
	float: left;
	height: 46px;
	width: 256px;
	position: relative;
}
.main_menu_icon {
	float: left;
	height: 60px;
	width: 1004px;
	position: relative;
	<?php 
	//LOCALDATA FILTRE
	$this_session_localdata =& get_instance();
	$this_session_localdata->load->library('session');
	$localdata_value = $this_session_localdata->session->userdata('localdata');
	if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
		?>
		background-image:url(<?php echo GetImagePath("front/"); ?>/localdata/cagnescommerces/header_banner.png);
		<?php
	} else {
		?>
		background-image:url(<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/wpa2ab1720_06.png);
		<?php
	}
	//LOCALDATA FILTRE
	?>
	background-repeat:no-repeat;
	background-position:center top;
	text-align:right;
	padding-right:20px;
	padding-top:29px;
}
.btn_img_ico {
	float: right;
	height: 91px;
	width: 65px;
	position: relative;
}
.conteneur_translate {
	float: left;
	height: 33px;
	width: 994px;
	position: relative;
	padding-left:15px;
	padding-right:15px;
	padding-top:7px;
	font-family: "Arial",sans-serif;
    font-size: 16px;
    font-weight: 700;
    line-height: 1.25em;
}
.main_slide_top {
	float: left;
	height: 255px;
	width: 1024px;
	position: relative;
}
.main_menu_head {
	float: left;
	height: 40px;
	width: 1024px;
	position: relative;
	text-align:center;
}
.div_link_menu_head_2, .div_link_menu_head_3, .div_link_menu_head_4, .div_link_menu_head_5,
.div_link_menu_head_avantagesclub, .div_link_menu_head_jadhereclub, .div_link_menu_head_moncompte,
.div_link_menu_head_favoris,  .div_link_menu_head_agenda, .div_link_menu_head_agenda, .div_link_menu_head_decouvavantages,
.div_link_menu_head_abonnements, .div_link_menu_head_moncompte2, .div_link_menu_head_plusinfos, .div_link_menu_head_formulaire,
.div_link_menu_head_youtube, .div_link_menu_head_inscription  
{
	float: left;
	height: 28px;
	width: 198px;
	padding-top:12px;
	background-color:#fcc73c;
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
.div_link_menu_head_over_2, .div_link_menu_head_over_3, .div_link_menu_head_over_4, .div_link_menu_head_over_5,
.div_link_menu_head_over_avantagesclub, .div_link_menu_head_over_jadhereclub, .div_link_menu_head_over_moncompte,
.div_link_menu_head_over_favoris,  .div_link_menu_head_over_agenda, .div_link_menu_head_over_agenda, .div_link_menu_head_over_decouvavantages,
.div_link_menu_head_over_abonnements, .div_link_menu_head_over_moncompte2, .div_link_menu_head_over_plusinfos, .div_link_menu_head_over_formulaire,
.div_link_menu_head_over_youtube  , .div_link_menu_head_over_inscription
{
	float: left;
	height: 28px;
	width: 198px;
	padding-top:12px;
	background-color:#979797;
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
.div_link_menu_head_1 {
	float: left;
	height: 28px;
	width: 202px;
	padding-top:12px;
	background-color:#fcc73c;
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
.div_link_menu_head_over_1 {
	float: left;
	height: 28px;
	width: 202px;
	padding-top:12px;
	background-color:#979797;
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
.main_menu_head a {
	text-decoration:none;
	color:#000000;
	font-weight:bold;
	
}
.div_link_menu_head_blank {
	float: left;
	height: 40px;
	width: 7.5px;
}
#span_translate_main {
	text-align:right;
	float:right;
	
}
.div_rigthcontent_cell {
	width:198px;
	height:auto;
	float:left;
	text-align:center;
}
.div_rigthcontent_cell_blank {
	width:198px;
	height:8px;
	float:left;
	text-align:center;
}
</style>

<script type="text/javascript">
$(function () {
	$('.div_link_menu_head_1').mouseover(function(){		$(this).removeClass("div_link_menu_head_1").addClass("div_link_menu_head_over_1");	});
	$('.div_link_menu_head_1').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_1").addClass("div_link_menu_head_1");	});
	$('.div_link_menu_head_2').mouseover(function(){		$(this).removeClass("div_link_menu_head_2").addClass("div_link_menu_head_over_2");	});
	$('.div_link_menu_head_2').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_2").addClass("div_link_menu_head_2");	});
	$('.div_link_menu_head_3').mouseover(function(){		$(this).removeClass("div_link_menu_head_3").addClass("div_link_menu_head_over_3");	});
	$('.div_link_menu_head_3').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_3").addClass("div_link_menu_head_3");	});
	$('.div_link_menu_head_4').mouseover(function(){		$(this).removeClass("div_link_menu_head_4").addClass("div_link_menu_head_over_4");	});
	$('.div_link_menu_head_4').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_4").addClass("div_link_menu_head_4");	});
	$('.div_link_menu_head_5').mouseover(function(){		$(this).removeClass("div_link_menu_head_5").addClass("div_link_menu_head_over_5");	});
	$('.div_link_menu_head_5').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_5").addClass("div_link_menu_head_5");	});
	
	$('.div_link_menu_head_avantagesclub').mouseover(function(){		$(this).removeClass("div_link_menu_head_avantagesclub").addClass("div_link_menu_head_over_avantagesclub");	});
	$('.div_link_menu_head_avantagesclub').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_avantagesclub").addClass("div_link_menu_head_avantagesclub");	});
	
	$('.div_link_menu_head_jadhereclub').mouseover(function(){		$(this).removeClass("div_link_menu_head_jadhereclub").addClass("div_link_menu_head_over_jadhereclub");	});
	$('.div_link_menu_head_jadhereclub').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_jadhereclub").addClass("div_link_menu_head_jadhereclub");	});
	
	$('.div_link_menu_head_moncompte').mouseover(function(){		$(this).removeClass("div_link_menu_head_moncompte").addClass("div_link_menu_head_over_moncompte");	});
	$('.div_link_menu_head_moncompte').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_moncompte").addClass("div_link_menu_head_moncompte");	});
	
	$('.div_link_menu_head_favoris').mouseover(function(){		$(this).removeClass("div_link_menu_head_favoris").addClass("div_link_menu_head_over_favoris");	});
	$('.div_link_menu_head_favoris').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_favoris").addClass("div_link_menu_head_favoris");	});
	
	$('.div_link_menu_head_agenda').mouseover(function(){		$(this).removeClass("div_link_menu_head_agenda").addClass("div_link_menu_head_over_agenda");	});
	$('.div_link_menu_head_agenda').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_agenda").addClass("div_link_menu_head_agenda");	});
	
	$('.div_link_menu_head_decouvavantages').mouseover(function(){		$(this).removeClass("div_link_menu_head_decouvavantages").addClass("div_link_menu_head_over_decouvavantages");	});
	$('.div_link_menu_head_decouvavantages').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_decouvavantages").addClass("div_link_menu_head_decouvavantages");	});
	
	$('.div_link_menu_head_abonnements').mouseover(function(){		$(this).removeClass("div_link_menu_head_abonnements").addClass("div_link_menu_head_over_abonnements");	});
	$('.div_link_menu_head_abonnements').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_abonnements").addClass("div_link_menu_head_abonnements");	});
	
	$('.div_link_menu_head_moncompte2').mouseover(function(){		$(this).removeClass("div_link_menu_head_moncompte2").addClass("div_link_menu_head_over_moncompte2");	});
	$('.div_link_menu_head_moncompte2').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_moncompte2").addClass("div_link_menu_head_moncompte2");	});
	
	$('.div_link_menu_head_plusinfos').mouseover(function(){		$(this).removeClass("div_link_menu_head_plusinfos").addClass("div_link_menu_head_over_plusinfos");	});
	$('.div_link_menu_head_plusinfos').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_plusinfos").addClass("div_link_menu_head_plusinfos");	});
	
	$('.div_link_menu_head_formulaire').mouseover(function(){		$(this).removeClass("div_link_menu_head_formulaire").addClass("div_link_menu_head_over_formulaire");	});
	$('.div_link_menu_head_formulaire').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_formulaire").addClass("div_link_menu_head_formulaire");	});
	
	$('.div_link_menu_head_youtube').mouseover(function(){		$(this).removeClass("div_link_menu_head_youtube").addClass("div_link_menu_head_over_youtube");	});
	$('.div_link_menu_head_youtube').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_youtube").addClass("div_link_menu_head_youtube");	});
	
	$('.div_link_menu_head_inscription').mouseover(function(){		$(this).removeClass("div_link_menu_head_inscription").addClass("div_link_menu_head_over_inscription");	});
	$('.div_link_menu_head_inscription').mouseout(function(){		$(this).removeClass("div_link_menu_head_over_inscription").addClass("div_link_menu_head_inscription");	});
	
});
</script>



<?php 
//LOCALDATA FILTRE
$this_session_localdata =& get_instance();
$this_session_localdata->load->library('session');
$localdata_value = $this_session_localdata->session->userdata('localdata');
if(isset($localdata_value) && $localdata_value=="cagnescommerces"){
?>
<div class="main_menu_aout2013">
<div class="main_menu_img"><a href="http://www.proximite-magazine.com/edition-3d-shopping-sorties-loisirs.html"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/main_menu_lemagazine.png" width="256" height="46" alt="magazine"></a></div>
<div class="main_menu_img"><a href="<?php echo base_url();?>?pageview=home"><img src="<?php echo GetImagePath("front/"); ?>/localdata/cagnescommerces/main_menu_lannuaire.png" width="256" height="46" alt="annuaire" /></a></div>
<div class="main_menu_img"><a href="<?php echo site_url("agenda");?>"><img src="<?php echo GetImagePath("front/"); ?>/localdata/cagnescommerces/main_menu_agenda.png" width="256" height="46" alt="agenda" /></a></div>
<div class="main_menu_img"><a href="http://www.proximite-magazine.com/selections.html"><img src="<?php echo GetImagePath("front/"); ?>/localdata/cagnescommerces/main_menu_notreselection.png" width="256" height="46" alt="selection" /></a></div>
</div>
<?php
} else {
?>
<div class="main_menu_aout2013">
<div class="main_menu_img"><a href="http://www.proximite-magazine.com/edition-3d-shopping-sorties-loisirs.html"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/main_menu_lemagazine.png" width="256" height="46" alt="magazine"></a></div>
<div class="main_menu_img"><a href="<?php echo base_url();?>?pageview=home"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/main_menu_lannuaire.png" width="256" height="46" alt="annuaire"></a></div>
<div class="main_menu_img"><a href="<?php echo site_url("agenda");?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/main_menu_agenda.png" width="256" height="46" alt="agenda"></a></div>
<div class="main_menu_img"><a href="http://www.proximite-magazine.com/selections.html"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/main_menu_notreselection.png" width="256" height="46" alt="selection"></a></div>
</div>
<?php
}
//LOCALDATA FILTRE
?>




<div class="main_menu_icon">
  <div class="btn_img_ico"><a href="<?php echo site_url("front/contact");?>"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/btn_menu_ico_contact.png" alt="back"></a></div>
  <div class="btn_img_ico"><a href="http://www.proximite-magazine.com/"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/btn_menu_ico_home.png" alt="back"></a></div>
  <div class="btn_img_ico"><a href="javascript:history.back();"><img src="<?php echo GetImagePath("front/"); ?>/wpimages_aout2013/btn_menu_ico_back.png" alt="back"></a></div>
</div>
<div class="conteneur_translate">
Les bonnes adresses des Alpes-Maritimes et Monaco : sorties, loisirs, culture, shopping, restos…
<!--<img src="<?php //echo GetImagePath("front/"); ?>/wpimages_aout2013/translate.png" alt="translate" style="text-align:right" align="right">-->
<span id="span_translate_main">
<div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false, multilanguagePage: true}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</span>
</div>

<div class="main_slide_top">
	<!--<img src="<?php //echo GetImagePath("front/"); ?>/wpimages_aout2013/main_slide_top.png" alt="main_slide_top" width="1024" height="246">-->
    <?php $data['pagetitle'] = "main page";?>
	<?php $this->load->view("frontAout2013/includes/mainslidetop", $data); ?> 
</div>

<div class="main_menu_head">
        <a href="<?php echo base_url();?>?pageview=home"><div class="div_link_menu_head_1">Les bonnes adresses</div></a>
        <div class="div_link_menu_head_blank"></div>
        <a href="<?php echo site_url('front/annonce');?>"><div class="div_link_menu_head_2">Les annonces</div></a>
        <div class="div_link_menu_head_blank"></div>
        <a href="<?php echo site_url('front/bonplan');?>"><div class="div_link_menu_head_3">Les bons plans</div></a>
        <div class="div_link_menu_head_blank"></div>
        <a href="http://www.proximite-magazine.com/avantages-club.html"><div class="div_link_menu_head_4">Plus d'informations</div></a>
        <div class="div_link_menu_head_blank"></div>
        <a href="http://www.proximite-magazine.com/admin/comparaison-abonnements.html"><div class="div_link_menu_head_5">Comparez nos abonnements</div></a>
</div>














<!--
<div id="art_1518" style="position:absolute;left:853px;top:11px;width:156px;height:16px;white-space:nowrap;">
    <div class="Wp-Normal-P">
        <span class="Normal-C-C2">Bienvenue Monsieur Martin</span></div>
</div>

<a href="http://www.proximite-magazine.com/">
	<?php // if (CURRENT_SITE == "agenda"){ ?>
    <img src="<?php // echo GetImagePath("front/"); ?>/agenda/btn_magazine_home.png" border="0" width="215" height="31" title="" alt="" style="position:absolute;left:808px;top:24px;">
    <?php // } else { ?>
    <img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/btn_acces_magazine_nv.png" border="0" width="216" height="30" id="hs_38" title="" alt="" style="position:absolute;left:808px;top:24px;">
    <?php // } ?>
</a>

<?php // if (CURRENT_SITE == "agenda"){ ?>
<a href="http://www.club-proximite.com/"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/btn_clubproximite_home.png" border="0" width="215" height="30" title="" alt="" style="position:absolute;left:808px;top:60px;"></a>
<?php // } else { ?>
<a href="http://www.agenda-cotedazur.fr/"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/btn_acces_agenda_nv.png" border="0" width="216" height="30" id="hs_39" title="" alt="" style="position:absolute;left:808px;top:60px;"></a>
<?php // } ?>




<a href="<?php // echo base_url();?>"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/wpd985cd97_06.png" border="0" width="74" height="74" id="hs_39" title="" alt="menu-professionnels" style="position:absolute; left:729px; top:105px;"></a>
<a href="<?php // echo site_url("front/contact");?>"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/wpb6e45b55_06.png" border="0" width="74" height="74" id="hs_39" title="" alt="menu-professionnels" style="position:absolute; left:800px; top:105px;"></a>
<a href="<?php // echo base_url();?>"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/wpd42f72d0_06.png" border="0" width="74" height="74" id="hs_39" title="" alt="menu-professionnels" style="position:absolute; left:870px; top:105px;"></a>
<a href="<?php // echo base_url();?>"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/wp75dbfae0_06.png" border="0" width="74" height="74" id="hs_39" title="" alt="menu-professionnels" style="position:absolute; left:945px; top:105px;"></a>


-->


<!--<?php // if (CURRENT_SITE == "agenda" || preg_match("/agenda/", $_SERVER["REQUEST_URI"])){ ?>
<a href="<?php // echo base_url();?>"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/menu_manifestation.png" border="0" width="209" height="30" title="" alt="fonctionnement_club" style="position:absolute; left:0px; top:216px;"></a>
<a href="<?php // echo site_url("agenda/liste/");?>"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/menu_recherche.png" border="0" width="219" height="30" title="" alt="fonctionnement_club" style="position:absolute; left:220px; top:216px;"></a>
<a href="<?php // echo site_url("agenda/avantages_internautes");?>"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/menu_fonctionnement.png" border="0" width="193" height="30" title="" alt="fonctionnement_club" style="position:absolute; left:420px; top:216px;"></a>
<a href="<?php // echo site_url("front/utilisateur/session/");?>"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/menu_particulier.png" border="0" width="191" height="30" title="" alt="fonctionnement_club" style="position:absolute; left:620px; top:216px;"></a>
<a href="<?php // echo site_url("front/utilisateur/sessionp");?>"><img src="<?php // echo GetImagePath("front/"); ?>/agenda/menu_pro.png" border="0" width="213" height="30" title="" alt="fonctionnement_club" style="position:absolute; left:810px; top:216px;"></a>
<?php // } else { ?>
<a href="<?php // echo site_url('staticpages/fonctionnement-club.htm');?>"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/btn_fonctionnement_club_main.png" border="0" width="221" height="36" title="" alt="fonctionnement_club" style="position:absolute; left:0px; top:213px;"></a>
<a href="<?php // echo site_url("front/utilisateur/session/");?>"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/btn_espace_conso_main.png" border="0" width="201" height="36" title="" alt="fonctionnement_club" style="position:absolute; left:220px; top:213px;"></a>
<a href="<?php // echo site_url("front/utilisateur/sessionp");?>"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/btn_espace_pro_main.png" border="0" width="202" height="36" title="" alt="fonctionnement_club" style="position:absolute; left:420px; top:213px;"></a>
<a href="<?php // echo site_url("front/utilisateur/comparerabonnements");?>"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/btn_nos_abonnements_main.png" border="0" width="200" height="36" title="" alt="fonctionnement_club" style="position:absolute; left:620px; top:213px;"></a>
<a href="<?php // echo site_url("front/contact");?>"><img src="<?php // echo GetImagePath("front/"); ?>/wpimages2013/btn_nous_contacter_main.png" border="0" width="204" height="36" title="" alt="fonctionnement_club" style="position:absolute; left:820px; top:213px;"></a>
<?php // } ?>-->