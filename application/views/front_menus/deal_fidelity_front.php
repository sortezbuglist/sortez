
<?php
$data['zTitle'] = "Deals et Fidelités - ".$oInfoCommercant->NomSociete;
$data["slide"] = 'wp41514793_05_06.jpg';
$data["check_active"] = "deals"; //prier de ne pas toucher ce variable
?>
<?php $this->load->view('front_menusincludes/main_header',$data); ?>
<link href="<?php echo base_url();?>application/views/front_menus/includes_commande/css/style.css" rel="stylesheet" type="text/css">
<?php
$this->load->view("front_menusincludes/main_header_premium", $data);
?>
<?php //$this->load->view('front_menusincludes/header_right',$data); ?>
<?php //$this->load->view('front_menusincludes/header_top',$data); ?>
<div class="container" style="max-width: 100%!important; width: 100%!important">
    <?php $this->load->view('front_menusincludes/menu_commande',$data); ?>
    <?php $this->load->view('front_menusincludes/presentation',$data); ?>

    <?php $this->load->view('front_menusincludes/acces_site',$data); ?>
    <?php $this->load->view('front_menusincludes/menu_partner',$data); ?>

    <div class="row mt-5">
        <div class="col-12 text-center">
            <h3 class="titre_header">Deals & Fidelités</h3>
        </div>
    </div>
    <?php $this->load->view('front_menusdeal_fidelity_content',$data); ?>

    <?php $this->load->view('front_menusincludes/footer_commercants',$data); ?>

    <?php //$this->load->view('front_menusincludes/main_footer_copyright',$data); ?>
</div>

<!--to top button-->
<a id="back-to-top" href="#" class="btn btn-secondary btn-lg back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>
<style type="text/css">
    .back-to-top {
        position: fixed;
        bottom: 25px;
        right: 25px;
        display: none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
    });
</script>
