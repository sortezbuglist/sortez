<?php 
$utilise  = false;
$bonplan_valide = 0;
if (isset($id_client) ) { 
   $oAssocClientBonPlan = $this->mdlcadeau->getClientAssocBonPlan($id_client,$oBonPlan->bonplan_id); 
   $bon_plan_utilise_plusieurs = 0;						   
	if(isset($oAssocClientBonPlan) && $oAssocClientBonPlan!=null) {
		//print_r($oAssocClientBonPlan);
		$utilise = $oAssocClientBonPlan->utilise;
		$bonplan_valide = $oAssocClientBonPlan->valide;
		if($oBonPlan->bon_plan_utilise_plusieurs == 1){
		 $bon_plan_utilise_plusieurs = 1;
		}
	 } 
}
?>
        


<script type="text/javascript">
function compte_a_rebours_<?php echo $oBonPlan->bonplan_id ; ?>()
{
	var compte_a_rebours_<?php echo $oBonPlan->bonplan_id ; ?> = document.getElementById("compte_a_rebours_<?php echo $oBonPlan->bonplan_id ; ?>");

	var date_actuelle = new Date();
	var date_evenement = new Date("<?php echo $oBonPlan->bonplan_date_fin ; ?>");
	var total_secondes = (date_evenement - date_actuelle) / 1000;

	var prefixe = "";
	if (total_secondes < 0)
	{
		prefixe = "Exp "; // On modifie le préfixe si la différence est négatif

		total_secondes = Math.abs(total_secondes); // On ne garde que la valeur absolue

	}

	if (total_secondes > 0)
	{
		var jours = Math.floor(total_secondes / (60 * 60 * 24));
		var heures = Math.floor((total_secondes - (jours * 60 * 60 * 24)) / (60 * 60));
		minutes = Math.floor((total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);
		secondes = Math.floor(total_secondes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

		var et = "et";
		var mot_jour = "jours";
		var mot_heure = "heures,";
		var mot_minute = "minutes,";
		var mot_seconde = "secondes";

		if (jours == 0)
		{
			jours = '';
			mot_jour = '';
		}
		else if (jours == 1)
		{
			mot_jour = "jour,";
		}

		if (heures == 0)
		{
			heures = '';
			mot_heure = '';
		}
		else if (heures == 1)
		{
			mot_heure = "heure,";
		}

		if (minutes == 0)
		{
			minutes = '';
			mot_minute = '';
		}
		else if (minutes == 1)
		{
			mot_minute = "minute,";
		}

		if (secondes == 0)
		{
			secondes = '';
			mot_seconde = '';
			et = '';
		}
		else if (secondes == 1)
		{
			mot_seconde = "seconde";
		}

		if (minutes == 0 && heures == 0 && jours == 0)
		{
			et = "";
		}

		//compte_a_rebours.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ' ' + mot_heure + ' ' + minutes + ' ' + mot_minute + ' ' + et + ' ' + secondes + ' ' + mot_seconde;
		//compte_a_rebours_<?php //echo $oBonPlan->bonplan_id ; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + ' ' + heures + ':' + minutes + ':' + secondes ;
		compte_a_rebours_<?php echo $oBonPlan->bonplan_id ; ?>.innerHTML = prefixe + jours + ' ' + mot_jour + '<p class="margintop10"><span class="noirblanc">' + heures + '</span>:<span class="noirblanc">' + minutes + '</span>:<span class="noirblanc">' + secondes +'</span></p>' ;
	}
	else
	{
		compte_a_rebours_<?php echo $oBonPlan->bonplan_id ; ?>.innerHTML = 'Expiré !';
		//alert(date_actuelle+ " "+date_evenement);
	}

	var actualisation = setTimeout("compte_a_rebours_<?php echo $oBonPlan->bonplan_id ; ?>();", 1000);
}
</script>
<style type="text/css">
<!--
.bordergrey {
	border: 4px solid #E1E1E1;
	margin-bottom:15px;
}
.bggrey { background-color:#E1E1E1;}
.bp_red_value {
	background-color: #DA1893;
	margin-right: auto;
	margin-left: auto;
	height:60px;
	border-radius: 10px;
	color:#FFFFFF;
}
@media screen and (min-width: 992px) {
    .bp_red_value {
        width: 450px;
    }
}
.bp_simple_prix {
	font-size: 40px;
}

.bp_desc_cond {
	background-color: transparent;
    color: #000000;
    font-family: "Arial",sans-serif;
    font-size: 18px;
    font-style: italic;
    font-variant: normal;
    font-weight: normal;
    margin: 15px 0;
    text-align: center;
    vertical-align: 0;
}
.bp_simple_value, .bp_simple_eco {
	font-size: 28px;
	text-transform: uppercase;
}
.size14 { font-size:14px;}
.img_bg_bp_simple {
	background-image: url(<?php echo GetImagePath("privicarte/");?>/img_bg_bp_simple.png);
	background-repeat:no-repeat;
	background-position: center center;
	background-size: auto;
	height:275px;
}
-->
</style>



<div class="col-12 padding0 bordergrey">
        
<div class="col-lg-12 padding0" style='background-color: transparent;
    color: #da1893;
    font-family: "Arial",sans-serif;
    font-size: 24px;
    font-style: normal;
    font-variant: normal;
    font-weight: 700;
    line-height: 22.7px;
    margin: 30px 0;
    text-align: center;
    vertical-align: 0;'>
  <?php echo trim($oBonPlan->bonplan_titre); ?>
</div>   

<?php 
if (
(!isset($oBonPlan->bp_simple_prix) || $oBonPlan->bp_simple_prix == "0" || $oBonPlan->bp_simple_prix == NULL || $oBonPlan->bp_simple_prix == "") &&
(!isset($oBonPlan->bp_simple_value) || $oBonPlan->bp_simple_value == "0" || $oBonPlan->bp_simple_value == NULL || $oBonPlan->bp_simple_value == "") &&
(!isset($oBonPlan->bp_simple_eco) || $oBonPlan->bp_simple_eco == "0" || $oBonPlan->bp_simple_eco == NULL || $oBonPlan->bp_simple_eco == "")
) {} else {
?>
<div class="col-12 padding0">
<div class="bp_red_value textaligncenter row">
  <div class="col-4 bp_simple_prix"><?php if (isset($oBonPlan->bp_simple_prix) && $oBonPlan->bp_simple_prix != "0") echo $oBonPlan->bp_simple_prix; ?></div>
  <div class="col-4 bp_simple_value">
  	<div class="col-12 padding0"><?php if (isset($oBonPlan->bp_simple_value) && $oBonPlan->bp_simple_value != "0") echo $oBonPlan->bp_simple_value; ?></div>
    <div class="col-12 padding0 size14">VALEUR</div>
    </div>
  <div class="col-4 bp_simple_eco">
  	<div class="col-12 padding0"><?php if (isset($oBonPlan->bp_simple_eco) && $oBonPlan->bp_simple_eco != "0") echo $oBonPlan->bp_simple_eco; ?></div>
    <div class="col-12 padding0 size14">&Eacute;CONOMIE</div>
    </div>
</div>
</div>
<?php } ?>

<div class="col-lg-12 padding0 bp_desc_cond">
	<strong>Les conditions d'utilisation :</strong><br/><?php if (isset($oBonPlan)) echo $oBonPlan->bonplan_texte; ?>
</div>

<?php if (isset($oBonPlan->activ_bp_reserver_paypal) && $oBonPlan->activ_bp_reserver_paypal == "1") {?>
    <div class="col-lg-12 padding0 textaligncenter">
        <div class="row">
            <div class="col-lg-6 padding0">
                <?php echo html_entity_decode($oBonPlan->bp_code_btn_paypal) ; ?>
            </div>
            <div class="col-lg-6 padding0">
                <?php echo html_entity_decode($oBonPlan->bp_code_panier_paypal) ; ?>
            </div>
        </div>
    </div>
<?php } ?>

</div>


<div class="col-12 padding0 bggrey paddingtop30 paddingbottom30 textaligncenter">
    <div class="row">
  <div class="col-sm-6 padding0 img_bg_bp_simple"></div>
  <div class="col-sm-6 padding0">
  	<div class="col-12 padding0 bonplan_mobile_comptearebour">
    	<img id="time_bg_white" src="<?php echo GetImagePath("privicarte/");?>/time_bp_compte_desk.png">
    	<span id="compte_a_rebours_<?php echo $oBonPlan->bonplan_id ; ?>"><noscript>Fin de l'évènement le 1er janvier 2017.</noscript></span>
        <script type="text/javascript">
            compte_a_rebours_<?php echo $oBonPlan->bonplan_id ; ?>();
        </script>
    </div>
    <div class="col-12 padding0 margintop15">&Eacute;ch&eacute;ance : <?php if (isset($oBonPlan->bonplan_date_fin) AND $oBonPlan->bonplan_date_fin !="" AND $oBonPlan->bonplan_date_fin !=null) echo convert_Sqldate_to_Frenchdate($oBonPlan->bonplan_date_fin); ?></div>
    <div class="col-12 padding0 margintop30">
    	Pour b&eacute;n&eacute;ficier de ce bon plan vous devez<br/>lors de votre d&eacute;placement pr&eacute;senter<br/>le coupon promotionnel.
    </div>
  </div>
  <div class="col-lg-12 padding0 textaligncenter margintop30"><a onclick='javascript:window.open("<?php echo site_url("front/bonplan/print_coupon/".$oBonPlan->bonplan_id); ?>", "print_coupon", "width=700, height=800, scrollbars=yes");' style="cursor:pointer;"><img src="<?php echo GetImagePath("privicarte/");?>/print_coupon_bp.png"/></a></div>
    </div>
</div>













  
  





