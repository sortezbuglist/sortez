<div class="row marge_gli_1">
    <p class="col-12 text-center text_variante">Variante si règlement par un terminal</p>
</div>
<div class="row row glissiere_tab">
    <div class="text-left titre_gli_up title_gli pl-2">PAIEMENT AVEC UN TERMINAL A LA LIVRAISON OU A L'ENLEVEMENT</div>
</div>
<div class="row">
    <div class="col-12 pt-4">
        <p class="text_head mb-0 text-justify">1.Livraison ou enlèvement sans contact ;</p>
        <p class="text_head mb-0 text-justify">2.Livraison : notre livreur reste sur le pas de votre porte. Il vous présente le terminal de paiement par carte bancaire, il vous suffit de valider votre code ;</p>
        <p class="text_head mb-0 text-justify">3.Imprimez et signez le justificatif de bonne réceptionVente à emporter : dans notre établissement, lors de l'enlèvement vous réglez directement par carte bancaire.</p>
    </div>
</div>
<div class="row">
    <div class="col-6 offset-3 pt-3 mt-5 text-center">
        <button class="w-100 btn btn_after_input" id="validate_command" onclick="">Validation de la commande</button>
    </div>
    <div class="col-6 pt-5 text-center">
        <button class="w-100 btn btn_after_input" id="validate_command" onclick="">Impression du justificatif de réception</button>
    </div>
    <div class="col-6 pt-5 text-center">
        <button class="w-100 btn btn_after_input" id="validate_command" onclick="">Export de la commande en PDF</button>
    </div>
</div>