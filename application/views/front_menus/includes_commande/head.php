<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
function isMobile () {
    return is_numeric(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "mobile"));
}

?>
<style>
    /*.glissiere_tab{*/
    /*    background: */<?php //if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color;}elseif($group_id_commercant_user == 4){echo '#3453a2';} ?>/*!important;*/
    /*}*/
    .glissiere_tab{
        background: #3453a2;
    }
    /*.back_blue{*/
    /*    background: */<?php //if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color;}elseif($group_id_commercant_user == 4){echo '#3453a2';} ?>/*!important;*/
    /*}*/
    .back_blue{
        background: #3453a2;
    }
    /*.btn_after_input_other{*/
    /*    background: */<?php //if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color;}elseif($group_id_commercant_user == 4){echo '#3453a2';} ?>/*;*/
    /*}*/
    .btn_after_input_other{
        background: #3453a2;
    }
    /*.input_glissiere{*/
    /*    background: */<?php //if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color;}elseif($group_id_commercant_user == 4){echo '#3453a2';} ?>/*;*/
    /*}*/
    .input_glissiere{
        background: #3453a2;
    }
    /*.plus_somme::before{*/
    /*    color: */<?php //if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color;}elseif($group_id_commercant_user == 4){echo '#3453a2';} ?>/*;*/
    /*}*/
    .plus_somme::before{
        color:#3453a2;
    }
</style>
<div class="row mt-5">
    <div class="col-12 text-center">
        <h3 class="titre_header">NOS MENUS ET CARTES</h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-sm-6 mb-3">
        <div class="text-right">
            <img src="<?php echo base_url()?>assets/image/qrcode_588073.png">
        </div>
    </div>
    <div class="col-lg-6 col-sm-6 mb-3">
        <div class="text-left pt-3">
            <img src="<?php echo base_url()?>assets/image/qr-code1.png">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center pb-4">
        <span style="font-family: futura-lt-w01-book,sans-serif;">Nos menus et cartes sur votre mobile, flashez !...</span>
    </div>
</div>
<div class="row head_commande pb-3">
    <div class="col-lg-2 col-sm-12 text-center" style="margin-left: 0px">
        <img src="<?php echo base_url() ?>/assets/images/image_left.png" style="margin-top: -12px;margin-left: -8px;">
    </div>
    <div class="col-lg-8 col-sm-12">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 d-flex mb-3">
                <input id="number_card" class="form-control w-100 textarea_style_cherche" placeholder="Votre numéro de carte"
                       <?php if(isset($IdUser) && $IdUser!= null && $IdUser!= ""){ ?> value="<?php echo $num_card->num_id_card_virtual; ?>" <?php } ?> type="text"/>
                <button onclick="get_users_info()" class="btn btn_cherche" <?php if(isset($IdUser) && $IdUser!= null && $IdUser!= ""){ ?> disabled <?php } ?> >Ok</button>
            </div>
            <div class="col-lg-11 offset-lg-1">
                <p class="text_head1">Accéder directement à votre carte dématérialisée et à son numéro</p>
            </div>
            <div class="col-lg-1 d-none d-lg-block"></div>
            <div class="col-lg-5 mt-3 text-sm-center text-lg-right">
<!--                <a href="--><?php //if(isset($IdUser) && $IdUser!= null && $IdUser!= ""){ echo base_url()."front/fidelity/menuconsommateurs"; }else{ echo "#"; } ?><!--"target="_blank">-->
                    <button onclick="check_id_user()" class="btn w-100 btn_carte" id="carte_check" >Accédez à votre carte</button>
<!--                </a>-->
            </div>
            <div class="col-lg-5 mt-3 text-sm-center text-lg-left">
                <a href="https://www.soutenonslecommercelocal.fr/avantages-particuliers" target="_blank"><button onclick="javascript:void(0)" class="btn w-100 btn_carte">Foires aux questions ! </button></a>
            </div>
            <div class="col-lg-1 d-none d-lg-block"></div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-12 text-center" id="qr_content" style="margin-left: -20px;">
        <?php if(isset($IdUser) && $IdUser!= null && $IdUser!= ""){ ?>
            <img src='<?php echo base_url()?>application/resources/front/images/cards/qrcode_<?php echo $res->virtual_card_img; ?>' class='img-fluid'/>
        <?php }else{ ?>
            <img src="<?php echo base_url() ?>/assets/images/qr_right.png">
        <?php } ?>
    </div>
</div>

<?php if (!isMobile()){ ?>

<div id="show_register_mobile" class="row head_commande d-none d-lg-block d-xl-block">
    <div class="col-lg-10 offset-lg-1 pb-4">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 d-lg-block d-none text-center">
                <img src="<?php echo base_url('/assets/soutenons/logo_vivresaville.png'); ?>">
            </div>
            <div class="col-lg-12">
                <?php if(isset($num_card->num_id_card_virtual) && $num_card->num_id_card_virtual != null && $num_card->num_id_card_virtual != ""){ ?>
                    <p class="text_head1">Votre numéro de carte est <?php echo $num_card->num_id_card_virtual; ?></p>
                <?php }else{ ?>
                    <p class="text_head1">Par contre, si vous ne la possédez pas encore, vous pouvez la demander gratuitement</p>
                    <p class="text_head1">en validant le formulaire ci-dessous</p>
                <?php } ?>
            </div>

            <?php if(isset($res) && $res != null && $res!= ""){ ?>
                <h3 class="text-center w-100 pt-4">Bonjour <a href="<?= site_url('auth')?>"><?php echo " ".$res->Nom." ".$res->Prenom;  ?></a></h3>
            <?php }else{ ?>
            <div class="iframe_subscript_user_container w-sm-100">
                <iframe id="iframe_subscript_user_command" src="<?= base_url()?>front/fidelity/inscription?form_soutenons=1" width="500" height="675" scrolling="no" frameBorder="0">Browser not compatible.</iframe>
            </div>
            <?php } ?>
            <style type="text/css">
                @media screen and (min-width: 515px){
                    .iframe_subscript_user_container {
                        width: 500px;
                        margin: auto;
                    }
                }
                @media screen and (max-width: 514px){
                    .iframe_subscript_user_container {
                        width: 100%;
                        margin: 0;
                    }
                }
            </style>


        </div>
    </div>
</div>
<?php } ?>
<style>
    .modal.fade{
        display: none;
    }
    @media screen and (max-width: 700px){
        #infocliall{
            margin:auto;
        }
        .modal.fade.show{
            width: 360px!important;
        }
        #all_totalised{
            /*width: 100%!important;*/
            text-align: center;
            padding-left: 0px!important;
        }
        .chiffre_gli{
            display: none;
        }
    }
    <?php if (isMobile()){ ?>
        .title_gli{
            font-size: 20px!important;
        }
    <?php } ?>
</style>
<!--<div class="row">-->
<!--    <div class="col-6 offset-6">-->
<!--        <div class="row">-->
<!--            <div class="col-6 offset-2 pr-0">-->
<!--                <p class="text_head1">Activation de la vente à emporter ou la livraison à domicile.</p>-->
<!--            </div>-->
<!--            <div class="col-4 text-right pl-0">-->
<!--                <img src="--><?php //echo base_url('/assets/images/fleche.png') ?><!--">-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

            <input type="hidden" id="activ_glissiere_emporter_value" value="0">
            <input type="hidden" id="activ_glissiere_livraison_value" value="0">
            <input type="hidden" id="activ_glissiere_differe_value" value="0">
            <input type="hidden" id="activ_glissiere_activ1_value" value="0">
            <input type="hidden" id="activ_glissiere_activ2_value" value="0">
            <input type="hidden" id="activ_glissiere_activ3_value" value="0">
            <input type="hidden" id="activ_glissiere_activ4_value" value="0">
            <input type="hidden" id="activ_glissiere_activ7_value" value="0">
            <input type="hidden" id="activ_glissiere_activ6_value" value="0">
            <input type="hidden" id="activ_glissiere_activ7_value" value="0">
            <input type="hidden" id="id_recap_value" value="0">
            <input type="hidden" id="id_spec_gli_value" value="0">
            <input type="hidden" id="value_emp_activ" value="0">
            <input type="hidden" id="value_livr_activ" value="0">
            <input type="hidden" id="value_differe_activ" value="0">
            <input type="hidden" id="all_added_product1">
            <input type="hidden" id="all_added_product2">
            <input type="hidden" id="all_added_product3">
            <input type="hidden" id="all_added_product4">
            <input type="hidden" id="all_added_product6">
            <input type="hidden" id="all_added_product7">
            <?php if(isset($IdUser) && $IdUser != null && $IdUser!= ""){ ?>
                <input type="hidden" id="id_client" value="<?php echo $IdUser; ?>">
            <?php }else{ ?>
                <input type="hidden" id="id_client" value="0">
            <?php } ?>
            <input type="hidden" id="is_register_open" value="0">

<script type="text/javascript">
    function show_register() {
        if($("#is_register_open").val() == 0){
        $('#show_register_mobile').removeClass('d-none');
        $('#show_register_mobile').addClass('d-flex');
            $("#is_register_open").val(1);
        }else{
            $('#show_register_mobile').removeClass('d-flex');
            $('#show_register_mobile').addClass('d-none');
            $("#is_register_open").val(0);
        }
    }
    function check_id_user(){
        <?php if(isset($IdUser) && $IdUser != null && $IdUser!= ""){ ?>
            window.open("<?php echo base_url()."front/fidelity/menuconsommateurs"; ?>", "_blank", "toolbar=no, scrollbars=yes, resizable=yes, top=500, left=500, width=650, height=800");
        <?php }else{ ?>
            alert("Vous devez d'abord vous connnectez!");
        <?php } ?>
    }
</script>