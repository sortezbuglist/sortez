<?php if (isset($data_gli4) && !empty($data_gli4) && isset($title_gli4->is_activ_glissiere) && $title_gli4->is_activ_glissiere != 0) { ?>

    <?php //if (!isMobile()) { ?>

    <div class="row glissiere_tab marge_gli_1">

        <form class="m-0 w-100 row pt-3 pb-3">

            <div class="col-lg-2 offset-lg-2 col-sm-12">

                <div class="row">

                    <div class="col-lg-6 col-6 text-center">

                        <label class="switch" id="activ_glissiere_activ4">

                            <input type="checkbox">

                            <span class="slider round"></span>

                        </label>

                    </div>

                    <div class="col-lg-6 col-6 p-0">

                        <p class="titre_volet">Ouvrir ou fermer ce volet</p>

                    </div>

                </div>

            </div>

            <div class="col-lg-4 col-sm-12">

                <div id="txt_title_gli2"

                     class="text-left text-sm-center title_gli titre_gli_up pt-2 pt-sm-0">

                    <?php if (isset($title_gli4->titre_glissiere) and $title_gli4->titre_glissiere != null) {

                        echo $title_gli4->titre_glissiere;

                    } else {

                        echo "TITRE";

                    } ?></div>

            </div>

            <div class="col-lg-2 col-sm-12">

                <div class="text-right text_head">

                    <div class="row text-center">

                        <div class="col-lg-6 col-sm-6 p-0">

                            <label>Montant total TTC à régler</label>

                        </div>

                        <div class="col-lg-6 col-sm-6 p-0">

                            <input id="gli4total" disabled="disabled"

                                   class="p-2 ml-1 input_glissiere text-center text_input_montant" type="text" value="0€"/>

                        </div>

                    </div>

                </div>

            </div>

        </form>

    </div>

    <?php //}else{ ?>

        <!-- <div class="row glissiere_tab mt-2 mb-2 title_gli_sm_height">

            <form class="m-0 w-100 row pt-sm-3 pt-0 pb-0">

                <div id="switch_btn"  class="d-none">

                    <label class="switch"  id='activ_glissiere_activ4'>

                        <input type="checkbox">

                        <span class="slider round"></span>

                    </label>

                </div>

                <div class="col-9 col-md-7">

                    <div id="txt_title_gli4"

                         class="text-left text-sm-center title_gli titre_gli_up pt-2 pt-sm-0">

                        <?php /*if (isset($title_gli4->titre_glissiere) and $title_gli4->titre_glissiere != null) {

                            echo $title_gli4->titre_glissiere;

                        } else {

                            echo "TITRE";

                        }*/ ?>

                    </div>

                </div>

                <div class="col-3">

                    <img onclick="setTimeout(function(){$('#content_gli4').modal('toggle');$('#activ_glissiere_activ4_value').val('1');}, 1000);" src="<?php echo base_url()?>assets/images/fleche_go.png">

                </div>

            </form>

            <div class="row">

                <div style="font-family:futura-lt-w01-book,sans-serif;font-size:12px;color:#FFFFFF;" class="col-6 pt-3 pl-sm-4 pl-5">Montant total TTC</div>

                <div class="col-6 text-right">

                    <input style="border: none;" id="gli4total" disabled="disabled"

                           class="p-2 pr-4 ml-1 input_glissiere text-right text_input_montant" type="text" value="0€"/>

                </div>

            </div>

        </div> -->

    <?php //} ?>

    <div class="row pr-3 pb-5 pt-4 d-none" id="activ_glissiere_activ4_content">

        <h5 class="text-center decription_glissiere">

            <?php echo $title_gli4->description_gli; ?>

        </h5>

        <div class="pl-5 pr-5">

            <div class="row m-0">

                <?php $i4 = 0; foreach ($data_gli4 as $gli4) { ?>

                    <div class="col-lgg-2 pt-4 col-sms-12">

                        <div class="ombre_bloc">

                            <div class="row w-100 img-glisss m-0">

                                <?php if (isset($gli4->image) and is_file("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/menugli1/" . $gli4->image)) { ?>

                                    <img class="img-fluid" style="width: 100%;"

                                         src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/menugli1/" . $gli4->image); ?>">

                                <?php } else { ?>

                                    <img class="img-fluid" style="align-self: center"

                                         src="<?php echo base_url("/assets/images/image_default.png"); ?>">

                                <?php } ?>

                            </div>

                            <div class="col-12 text-center pt-4">

                                    <input class="id_gliss_4" value="<?php echo $gli4->id; ?>" type="hidden"

                                           id="product_gli4_<?php echo $i4; ?>"/>

                                    <?php if (isset($gli4->true_title) and $gli4->true_title != null) { ?>

                                        <p class="titre_categorie text-center" id="gli4_item_title_<?= $i4;?>">

                                            <?php echo $gli4->true_title; ?>

                                        </p>

                                    <?php } ?>

                                    <?php if (isset($gli4->titre) and $gli4->titre != null) { ?>

                                        <p class="description_categorie">

                                            <?php echo $gli4->titre; ?>

                                        </p>

                                    <?php } ?>

                            </div>

                            <div class="col-12 text-center pt-2 pb-5">

                                <div class="row">

                                    <div class="col-6">

                                        <?php if (isset($gli4->prix) and $gli4->prix != null) { ?>

                                            <input id="prix4<?php echo $i4; ?>" type="hidden" value="<?php echo $gli4->prix; ?>">

                                            <label class="col-12 text_label text-left">Prix U.</label>

                                            <div class="col-12 p-0">

                                                <input id="gli4_item_price_<?= $i4;?>" value="<?php echo "€ ".$gli4->prix; ?>"

                                                       class="w-100 nbre_gliss4_pr textarea_style_input text-center prix_u" disabled>

                                            </div>

                                        <?php } ?>

                                    </div>

                                    <div class="col-6">

                                        <label class="col-12 text_label">Quantité</label>

                                        <div class="col-12 p-0 pb-2">

                                            <input value="0"

                                                   onchange="change_total_price4('prix4<?php echo $i4 . '\''; ?>,'nbre4<?php echo $i4 . '\''; ?>,'product_gli4_<?php echo $i4 . '\''; ?>);change_recap_gli4_item_<?= $i4;?>();"

                                                   id="nbre4<?php echo $i4; ?>" type="number"

                                                   class="gli4_item_qtt_<?= $i4;?> w-100 nbre_gliss4_qt textarea_style_input p-2 text_bold text-center">

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                    <script type="text/javascript">

                        function change_recap_gli4_item_<?= $i4;?>(){

                            var recap_gli_title = $("#gli4_item_title_<?= $i4;?>").html();

                            var recap_gli_price = $("#gli4_item_price_<?= $i4;?>").val();

                            var recap_gli_qtt = $(".gli4_item_qtt_<?= $i4;?>").val();

                            var gli_recap_contents = '<tr style=" margin-left:1px ;display: flex;"  class="">\n' +

                                '                                <td style=" margin-left: 1rem;width:170px" class="">'+recap_gli_title+'</td>\n' +

                                '                                <td style=" margin-left: 11rem;"  class="">'+recap_gli_price+'</td>\n' +

                                '                                <td style=" margin-left: 18rem;"  class="">'+recap_gli_qtt+'</td>\n' +

                                '                            </tr>';

                            $("#gli_4_recap_content_<?= $i4;?>").html(gli_recap_contents);

                        }

                    </script>

                    <?php $i4++;

                } ?>

            </div>

        </div>

    </div>



    <div class="modal fade" id="content_gli4" role="dialog" style="height: 1000px;">

        <div class="modal-dialog m-0">



            <!-- Modal content-->

            <div class="modal-content">

                <div class="modal-header glissiere_tab">

                    <div class="col-12 text-center">

                        <div class="w-50 d-block m-auto" id="content_btn_gli4">

                            <div style="color: white;font-size: 15px;" class="" data-dismiss="modal" id="close_gli4" aria-label="Close">

                            <img style="width: 10%;height: auto;margin-bottom: 3px;" src="<?php echo base_url("assets/image/backs.png"); ?>">Retour Commande

                            </div>

                        </div>

                    </div>

                </div>

                <div class="modal-body" id="modal-scroll4">

                    <div class="row m-0">

                        <div id="txt_title_gli4"

                             class="text-center glissiere_tab pt-4 w-100 title_gli titre_gli_up">

                            <div class="w-100">

                                <div style="width: 50px;height: 50px;" class="chiffre_gli m-auto">4</div>

                            </div>

                            <div class="w-100 pt-2">

                                <?php if (isset($title_gli4->titre_glissiere) and $title_gli4->titre_glissiere != null) {

                                    echo $title_gli4->titre_glissiere;

                                } else {

                                    echo "TITRE";

                                } ?>

                            </div>

                            <div class="w-100 pt-2" style="font-size: 15px!important;">

                                Montant total TTC à régler

                            </div>

                            <div class="w-100 mt-3" style="font-family:avenir-lt-w01_85-heavy1475544,sans-serif;font-size:29px!important;font-weight: bold">

                                <input style="border: none" id="total_mobile_gli4" disabled="disabled"

                                       class="p-2 ml-1 input_glissiere text-center text_input_montant" type="text" value="0€"/>

                            </div>

                        </div>

                        <?php $i4 = 0; foreach ($data_gli4 as $gli4) { ?>

                            <div class="col-lgg-2 pt-4 col-sms-12 col-md-4">

                                <div class="ombre_bloc">

                                    <div class="row w-100 img-glisss m-0">

                                        <?php if (isset($gli4->image) and is_file("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/menugli1/" . $gli4->image)) { ?>

                                            <img class="img-fluid" style="width: 100%;"

                                                 src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/" . $infocom->user_ionauth_id . "/menugli1/" . $gli4->image); ?>">

                                        <?php } else { ?>

                                            <img class="img-fluid" style="align-self: center"

                                                 src="<?php echo base_url("/assets/images/image_default.png"); ?>">

                                        <?php } ?>

                                    </div>

                                    <div class="col-12 text-center pt-4">

                                        <input class="id_gliss_4" value="<?php echo $gli4->id; ?>" type="hidden"

                                               id="product_gli4_<?php echo $i4; ?>"/>

                                        <?php if (isset($gli4->true_title) and $gli4->true_title != null) { ?>

                                            <p class="titre_categorie text-center" id="gli4_item_title_mobile_<?= $i4;?>">

                                                <?php echo $gli4->true_title; ?>

                                            </p>

                                        <?php } ?>

                                        <?php if (isset($gli4->titre) and $gli4->titre != null) { ?>

                                            <p class="description_categorie">

                                                <?php echo $gli4->titre; ?>

                                            </p>

                                        <?php } ?>

                                    </div>

                                    <div class="col-12 text-center pt-2 pb-5">

                                        <div class="row">

                                            <div class="col-6">

                                                <?php if (isset($gli4->prix) and $gli4->prix != null) { ?>

                                                    <input id="prix4<?php echo $i4; ?>" type="hidden" value="<?php echo $gli4->prix; ?>">

                                                    <label class="col-12 text_label">Prix U.</label>

                                                    <div class="col-12 p-0">

                                                        <input id="gli4_item_price_mobile_<?= $i4;?>" value="<?php echo "€ ".$gli4->prix; ?>"

                                                               class="w-100 nbre_gliss4_pr textarea_style_input text-center prix_u" disabled>

                                                    </div>

                                                <?php } ?>

                                            </div>

                                            <div class="col-6">

                                                <label class="col-12 text_label">Quantité</label>

                                                <div class="col-12 p-0 pb-2">

                                                    <input value="0"

                                                           onchange="change_total_price4('prix4<?php echo $i4 . '\''; ?>,'nbre4<?php echo $i4 . '\''; ?>,'product_gli4_<?php echo $i4 . '\''; ?>);change_recap_gli4_item_mobile_<?= $i4;?>();"

                                                           id="nbre4<?php echo $i4; ?>" type="number"

                                                           class="gli4_item_qtt_mobile_<?= $i4;?> w-100 nbre_gliss4_qt textarea_style_input p-2 text_bold text-center">

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <script type="text/javascript">

                                function change_recap_gli4_item_mobile_<?= $i4;?>(){

                                    var recap_gli_title = $("#gli4_item_title_mobile_<?= $i4;?>").html();

                                    var recap_gli_price = $("#gli4_item_price_mobile_<?= $i4;?>").val();

                                    var recap_gli_qtt = $(".gli4_item_qtt_mobile_<?= $i4;?>").val();

                                    var gli_recap_contents = '<tr style=" margin-left:1px ;display: flex;" class="">\n' +

                                        '                                <td style=" margin-left: 1rem;" class="">'+recap_gli_title+'</td>\n' +

                                        '                                <td style=" margin-left: 18rem;"  class="">'+recap_gli_price+'</td>\n' +

                                        '                                <td style=" margin-left: 18rem;"  class="">'+recap_gli_qtt+'</td>\n' +

                                        '                            </tr>';

                                    $("#gli_4_recap_content_<?= $i4;?>").html(gli_recap_contents);

                                }

                            </script>

                            <?php $i4++;

                        } ?>

                    </div>

                </div>

            </div>



        </div>

        <style>

            .modal {

                overflow: hidden;

            }

            .modal .modal-body {

                height: 620px;

                overflow: auto;

            }

            .modal .modal-fixed {

                position: fixed;

                z-index : 1052;

                float: left;

                width:auto!important;

                top: 20px;

            }

            @media screen and (min-width:768px) and (max-width:1024px) {

                #activ_glissiere_activ4_content .col-lgg-2 {

                    flex: 0 0 33%;

                    max-width: 38%;

                    }

             }

       

        </style>

        <script>

            var positionElementInPage = $('#close_gli4').offset().top;

            $('#modal-scroll4').resize(function() {

                positionElementInPage = $('#close_gli4').offset().top;

            });

        </script>

    </div>



    <?php

    //if (isMobile()) { ?>

        <!-- <script>

            $(document).ready(function(){

                $("#activ_glissiere_activ4").click(function(){

                    if($("#activ_glissiere_activ4_value").val() == 0){

                        setTimeout(function(){

                        $("#content_gli4").modal("toggle");

                        $("#activ_glissiere_activ4_value").val('1');

                        }, 1000);

                    }

                    else{

                        setTimeout(function(){

                            $("#content_gli4").hide();

                            $("#activ_glissiere_activ4_value").val('0');

                    }, 1000);

                    }

                });

                $("#close_gli4").click(function(){

                    $("#content_gli4").hide();

                    $("#activ_glissiere_activ4").click();

                });

            });

        </script> -->

    <?php //}else{ ?>

        <script>

            $("#activ_glissiere_activ4").change(function (){

                if($("#activ_glissiere_activ4_value").val() == 0){

                    $("#activ_glissiere_activ4_content").removeClass('d-none');

                    $("#activ_glissiere_activ4_content").addClass('d-block');

                    $("#activ_glissiere_activ4_value").val('1');

                }

                else{

                    $("#activ_glissiere_activ4_content").removeClass('d-block');

                    $("#activ_glissiere_activ4_content").addClass('d-none');

                    $("#activ_glissiere_activ4_value").val('0');

                }

            })

        </script>

    <?php //} ?>

<?php } ?>