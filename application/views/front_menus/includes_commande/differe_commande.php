<?php if (isset($datacommande->is_activ_vente_differe) AND $datacommande->is_activ_vente_differe =="1"){ ?>
        <div class="row d-none diff_contentss">
            <div class="mt-4 col-sm-12">
                <div class="w-100 middle_title">
                    COMMANDE EN LIGNE :  ENVOIS DE DOCUMENTS (devis, factures...) ET PAIEMENT EN RETOUR
                </div>
                <div class="w-100 pt-4">
                    <p class="text_head">
                        <?php echo $datacommande->comment_differe_txt ?? ""; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="row  d-none diff_contentss">
            <div class="mt-4 col-sm-12">
                <div class="row">
                    <?php if (isset($datacommande->is_activ_devis_differe) AND $datacommande->is_activ_devis_differe == '1'){ ?>
                        <div class="col-lg-2 col-4 m-auto text_head">
                            <input id="is_activ_devis_differe" class="check_cond" type="checkbox"> Devis
                        </div>
                    <?php } ?>
                    <?php if (isset($datacommande->is_activ_pro_forma_differe) AND $datacommande->is_activ_pro_forma_differe == '1'){ ?>
                        <div class="col-lg-3 col-4 p-0 m-auto text_head">
                            <input id="is_activ_pro_forma_differe" class="check_cond" type="checkbox"> Fact. proforma
                        </div>
                    <?php } ?>
                    <?php if (isset($datacommande->is_activ_facture_differe) AND $datacommande->is_activ_facture_differe == '1'){ ?>
                        <div class="col-lg-3 col-4 m-auto text_head">
                            <input id="is_activ_facture_differe" class="check_cond" type="checkbox"> Facture
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
<?php if (isset($datacommande->is_activ_banc_type_differe) AND $datacommande->is_activ_banc_type_differe == "1"){ ?>
    <div class="row pt-2 pb-4 d-none diff_contentss">
        <?php if (isset($datacommande->is_activ_Cheque_differe) AND $datacommande->is_activ_Cheque_differe == '1'){ ?>
            <div class="col-lg-2 col-4 m-auto text_head">
                <input id="is_activ_Cheque_differe" class="check_cond" type="checkbox"> Chèque
            </div>
        <?php } ?>
        <?php if (isset($datacommande->is_activ_Virement_differe) AND $datacommande->is_activ_Virement_differe == '1'){ ?>
            <div class="col-lg-3 col-4 p-0 m-auto text_head">
                <input id="is_activ_Virement_differe" class="check_cond" type="checkbox"> Virement
            </div>
        <?php } ?>
        <?php if (isset($datacommande->is_activ_carte_differe) AND $datacommande->is_activ_carte_differe == '1'){ ?>
            <div class="col-lg-3 col-4 m-auto text_head">
                <input id="is_activ_carte_differe" class="check_cond" type="checkbox"> CB
            </div>
        <?php } ?>
    </div>
<?php } ?>