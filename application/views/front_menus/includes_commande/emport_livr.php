<?php if (isMobile()){ ?>

    <div class="text-center mb-3">
        <?php if(isset($num_card->num_id_card_virtual) && $num_card->num_id_card_virtual != null && $num_card->num_id_card_virtual != ""){ ?>
            <p class="text_head1">Votre numéro de carte est <?php echo $num_card->num_id_card_virtual; ?></p>
        <?php }else{ ?>
            <p class="text_head1">Par contre, si vous ne la possédez pas encore, vous pouvez la demander gratuitement</p>
            <p class="text_head1">en validant le formulaire ci-dessous</p>
        <?php } ?>
    </div>
    <?php if(isset($res) && $res != null && $res!= ""){ ?>
        <h3 class="text-center w-100 pt-4">Bonjour <a href="<?= site_url('auth')?>"><?php echo " ".$res->Nom." ".$res->Prenom;  ?></a></h3>
    <?php }else{ ?>
        <div class="row glissiere_tab mt-2 title_gli_sm_height">
            <div class="m-0 w-100 row pt-2 pb-2 pb-0">
                <div class="col-9 pl-4 pl-md-3">
                    <div id=""
                         class="text-left text-sm-center title_gli titre_gli_up pt-2 pt-sm-0">
                        
                    </div>
                </div>
                <div class="col-3 pt-2 pt-md-0">
                    <img onclick="show_register()" src="<?php echo base_url()?>assets/images/fleche_go.png">
                </div>
            </div>
        </div>
    <?php } ?>
<div id="show_register_mobile" class="row head_commande d-none">
    <div class="col-lg-10 offset-lg-1 pb-4">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 d-lg-block d-none text-center">
                <img src="<?php echo base_url('/assets/soutenons/logo_vivresaville.png'); ?>">
            </div>
            <div class="iframe_subscript_user_container w-sm-100">
                <iframe id="iframe_subscript_user_command" src="<?= base_url()?>front/fidelity/inscription?form_soutenons=1" width="100%" height="725" scrolling="no" frameBorder="0">Browser not compatible.</iframe>
            </div>
            <style type="text/css">
                @media screen and (min-width: 515px){
                    .iframe_subscript_user_container {
                        width: 500px;
                        margin: auto;
                    }
                }
                @media screen and (max-width: 514px){
                    .iframe_subscript_user_container {
                        width: 100%;
                        margin: 0;
                    }
                }
            </style>

        </div>
    </div>
</div>
<?php } ?>
<?php if (isset($datacommande->is_activ_emporter) and $datacommande->is_activ_emporter == '1') { ?>
    <div class="col-lg-8 col-12 m-auto">
        <p class="text_head1 mt-4 mb-3" style="color:#E80EAE">« Si votre choix se porte sur la vente à emporter, vous devez obligatoirement activer cette glissière et préciser la date et l’heure souhaitées de l’enlèvement de votre commande »</p>
    </div>
    <div class="row glissiere_tab title_gli_sm_height">
        <form class="m-0 w-100 row pt-sm-3 pt-3 pb-sm-3 pb-0">
            <div class="col-lg-2 offset-lg-2 col-sm-12">
                <div class="row">
                    <div class="col-md-6 col-sm-6 text-center col-4">
                        <label class="switch" id="activ_glissiere_emporter">
                            <input onchange="switch_emporter()" id="id_check_epm" type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <div class="col-md-6 col-sm-6 p-0 col-8">
                        <p class="titre_volet">Ouvrir ou fermer ce volet</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div id="txt_title_gli1" class="text-sm-left title_gli titre_gli_up pt-1 text-lg-center" style="margin-right: -20px;">Vente à emporter</div>
            </div>
            <div class="col-lg-4 text-right">

            </div>
        </form>
    </div>
    <div class="row p-2 pr-4 pt-4 pb-4 d-none" id="emporter_content">
        <div class="container cont_all2">
            <div class="row mb-4">
                <div class="col-lg-3 col-sm-12 d-flex">
                    <div id="txt_title_gli1" class="text-left pt-2 text_label">Jours et heures d’ouvertures </div>
                </div>
                <div class="col-lg-9 col-sm-12 pr-0">
                    <div id="txt_title_gli1"
                         class="text-left <?php if (isset($datacommande->horaire_emporter_ouvert) && $datacommande->horaire_emporter_ouvert != null){ echo "p-2";}else{ echo  "p-4";}?> title_gli_val textarea_style_input text_head"><?php if (isset($datacommande->horaire_emporter_ouvert)) echo $datacommande->horaire_emporter_ouvert ?></div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-lg-3 col-sm-12  d-flex">
                    <div id="txt_title_gli1" class="text-left pt-2 text_label">Jours et heures d’enlèvement </div>
                </div>
                <div class="col-lg-9 col-sm-12  pr-0">
                    <div id="txt_title_gli1"
                         class="text-left <?php if (isset($datacommande->horaire_emporter_enlev) && $datacommande->horaire_emporter_enlev != null){ echo "p-2";}else{ echo  "p-4";}?> title_gli_val textarea_style textarea_style_input text_head"><?php if (isset($datacommande->horaire_emporter_enlev)) echo $datacommande->horaire_emporter_enlev ?></div>
                </div>
            </div>
            <?php if (!isMobile()) { ?>
            <div class="col-12">
                <div class="row mt-2">
                    <div class="col-lg-7" id="col_jour_emporter">
                        <div class="row">
                            <div class="col-lg-5 col-sm-12 pl-0 pr-0 d-flex">
                                <div id="txt_title_gli1" class="text_label">Jour souhaité de l’enlèvement </div>
                            </div>
                            <div class="col-lg-7 col-sm-12" id="textarea_sm">
                                <input id="jour_emporter" class="form-control textarea_style text_head" type="date">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 pr-0">
                        <div class="row">
                            <div class="col-lg-8 pr-0 pl-5 d-flex" id="label_sm">
                                <div class="text_label"> Heure d’enlèvement souhaitée </div>
                            </div>
                            <div class="col-lg-4 pr-0 pl-0">
                                <input type="time"  class="form-control textarea_style text_head" id="heure_enlevement">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-1 pt-3 pb-2">
                <div class="p-2 text_head text-justify">
                    <?php if (isset($datacommande->comment_emporter_txt)) echo $datacommande->comment_emporter_txt;  ?>
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center mb-5">
                    <div class="row">
                        <div class="col-lg-2 offset-lg-2 col-sm-12">
                            <a href="#id_recap" class="round_arrow_down"></a>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <p class="text_info text_bas_sm" style="text-align: left">Accès direct à la validation de la commande</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if(isMobile()) { ?>
    <div class="modal fade" id="content_empmodal" role="dialog" style="height: 1000px;">
        <div class="modal-dialog m-0">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header glissiere_tab">
                    <div class="col-12 text-center">
                        <div class="w-50 d-block m-auto" id="content_btn_emport">
                            <div style="color: white;font-size: 15px;" class="" data-dismiss="modal" id="close_emp" aria-label="Close">
                            <img style="width: 10%;height: auto;margin-bottom: 3px;" src="<?php echo base_url("assets/image/backs.png"); ?>"> Retour Commande
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body pl-0" id="modal-scroll-emport">
                    <div class="container cont_all2">
                        <div class="row mb-4">
                            <div class="col-lg-3 col-sm-12 d-flex">
                                <div id="txt_title_gli1" class="text-left pt-2 text_label">Jours et heures d’ouvertures </div>
                            </div>
                            <div class="col-lg-9 col-sm-12 pr-0">
                                <div id="txt_title_gli1"
                                     class="text-left <?php if (isset($datacommande->horaire_emporter_ouvert) && $datacommande->horaire_emporter_ouvert != null){ echo "p-2";}else{ echo  "p-4";}?> title_gli_val textarea_style_input text_head"><?php if (isset($datacommande->horaire_emporter_ouvert)) echo $datacommande->horaire_emporter_ouvert ?></div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-lg-3 col-sm-12  d-flex">
                                <div id="txt_title_gli1" class="text-left pt-2 text_label">Jours et heures d’enlèvement </div>
                            </div>
                            <div class="col-lg-9 col-sm-12  pr-0">
                                <div id="txt_title_gli1"
                                     class="text-left <?php if (isset($datacommande->horaire_emporter_enlev) && $datacommande->horaire_emporter_enlev != null){ echo "p-2";}else{ echo  "p-4";}?> title_gli_val textarea_style textarea_style_input text_head"><?php if (isset($datacommande->horaire_emporter_enlev)) echo $datacommande->horaire_emporter_enlev ?></div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row mt-2">
                                <div class="col-lg-7" id="col_jour_emporter">
                                    <div class="row">
                                        <div class="col-lg-5 col-sm-12 pl-0 pr-0 d-flex">
                                            <div id="txt_title_gli1" class="text_label">Jour souhaité de l’enlèvement </div>
                                        </div>
                                        <div class="col-lg-7 col-sm-12" id="textarea_sm">
                                            <input id="jour_emporter" class="form-control textarea_style text_head" type="date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 pr-0">
                                    <div class="row">
                                        <div class="col-lg-8 pr-0 pl-5 d-flex" id="label_sm">
                                            <div class="text_label"> Heure d’enlèvement souhaitée </div>
                                        </div>
                                        <div class="col-lg-4 pr-0 pl-0">
                                            <input type="time" class="form-control textarea_style text_head" id="heure_enlevement">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row pl-1 pt-3 pb-2">
                            <div class="p-2 text_head text-justify">
                                <?php if (isset($datacommande->comment_emporter_txt)) echo $datacommande->comment_emporter_txt;  ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php } ?>

    <div class="row">
        <div id="emporter_error" class="col-12 text-center">

        </div>
    </div>
<?php } ?>

<?php if (isset($datacommande->is_activ_livr_a_domicile) and $datacommande->is_activ_livr_a_domicile == '1') { ?>
    <div class="col-lg-8 col-12 m-auto">
        <p class="text_head1 mt-4 mb-3 " style="color:#E80EAE">
            « Si votre choix se porte sur la livraison ou service à domicile, vous devez obligatoirement activer cette glissière et préciser la date et l’heure souhaitées de livraison ou service de votre commande »
        </p>
    </div>
    <div class="row glissiere_tab mt-sm-2 mt-lg-2 title_gli_sm_height">
        <form class="m-0 w-100 row pt-sm-3 pt-3 pb-sm-3 pb-0">
            <div class="col-lg-2 offset-lg-2 col-sm-12">
                <div class="row">
                    <div class="col-md-6 col-sm-6 text-center col-4">
                        <label class="switch" id="activ_glissiere_livraison">
                            <input onchange="switch_livraison()" type="checkbox" id="id_check_livr">
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <div class="col-md-6 col-sm-6 p-0 col-8">
                        <p class="titre_volet">Ouvrir ou fermer ce volet</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div id="txt_title_gli1" class="text-sm-left title_gli titre_gli_up pt-1 text-lg-center" style="margin-right: -20px;">Livraison à domicile</div>
            </div>
<!--            <div class="col-4 d-flex">-->
<!--                <div id="txt_title_gli1" class="text-left title_gli titre_gli_up pt-1" style="margin-right: -70px;">Livraison à domicile</div>-->
<!--            </div>-->
<!--            <div class="col-4 drop_up_down" id="activ_glissiere_livraison">-->
<!--                <div class="cercle">-->
<!--                    <div class="text-center up_1 d-none" id="activ_glissiere_livraison_up"></div>-->
<!--                    <div class="text-center down_1" id="activ_glissiere_livraison_down"></div>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div class="col-4 text-right">-->
<!--                <label class="switch" onchange="switch_livraison()" id="value_livr_activ_value">-->
<!--                    <input type="checkbox">-->
<!--                    <span class="slider round"></span>-->
<!--                </label>-->
<!--            </div>-->
        </form>
    </div>
    <?php if (!isMobile()) { ?>
    <div class="row p-2 pb-5 pt-4 d-none" id="livraison_content">
        <div class="container cont_all2 head_commande">
            <div class="row pt-2">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-6 col-sm-12 d-flex" id="textarea_sm_livr0"><div class="text_label text-left">Livraison gratuite pour toute commande supérieure à </div></div>
                        <div class="col-lg-6 col-sm-12 pl-2 pr-0" id="textarea_sm_livr1"><div class="value_title_sub textarea_style_input p-2 text_head">30€</div></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-6 col-sm-12  d-flex pl-5" id="textarea_sm_livr0">
                            <div class="text_label">
                                délai de livraison
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-12  pl-0" id="textarea_sm_livr0">
                            <div class="value_title_sub textarea_style_input p-2 text_head">
                                <?php if (isset($datacommande->commune_livr_desserv) and isset($datacommande->delai_livr_debut)) echo $datacommande->delai_livr_debut; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row mt-4">
                    <div class="col-lg-3 col-sm-12  pl-0 d-flex">
                        <div class="text_label">Jours et heures d'ouverture </div>
                    </div>
                    <div class="col-lg-9 col-sm-12  pl-0 pr-0">
                        <div class="textarea_style_input text_head p-2">Ouvert du mardi au samedi de 09h00 à 13h30 et de 18h00 à 21h00</div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row mt-4">
                    <div class="col-lg-3 col-sm-12  pl-0 d-flex">
                        <div class="text_label">Jours et heures de livraison </div>
                    </div>
                    <div class="col-lg-9 col-sm-12  pl-0 pr-0">
                        <div class="textarea_style_input text_head <?php if (isset($datacommande->horaire_livr_ouvert) && $datacommande->horaire_livr_ouvert != null){ echo "p-2" ; }else{ echo "p-3"; } ?>"><?php if (isset($datacommande->horaire_livr_ouvert)) echo $datacommande->horaire_livr_ouvert; ?></div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row mt-4">
                    <div class="col-lg-7">
                        <div class="row">
                            <div class="col-lg-5 col-sm-12  pl-0 pr-0 d-flex" >
                                <div class="text_label">Date souhaitée de la livraison </div>
                            </div>
                            <div class="col-lg-7 col-sm-12  pl-2 pr-0" id="textarea_sm_livr2">
                                <input id="jour_livraison" class="form-control textarea_style text_head" type="date">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="row">
                            <div class="col-lg-8 col-sm-12  pr-0 d-flex" >
                                <div class="text_label">Heure de livraison souhaitée  </div>
                            </div>
                            <div class="col-lg-4 col-sm-12  pr-0 pl-0" id="textarea_sm_livr3">
<!--                                <select id="heure_livraison" class="form-control textarea_style text_head">-->
<!--                                    <option value="09:00">9:00</option>-->
<!--                                    <option value="10:00">10:00</option>-->
<!--                                    <option value="11:00">11:00</option>-->
<!--                                    <option value="12:00">12:00</option>-->
<!--                                    <option value="13:00">13:00</option>-->
<!--                                    <option value="18:00">18:00</option>-->
<!--                                    <option value="19:00">19:00</option>-->
<!--                                    <option value="20:00">20:00</option>-->
<!--                                </select>-->
                                <input type="time" class="form-control textarea_style text_head" id="heure_livraison">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="row mt-3">
                    <div class="col-lg-3 col-sm-12  pl-0 d-flex">
                        <div class="text_label text-justify">Communes desservies </div>
                    </div>
                    <div class="col-lg-9 col-sm-12 pl-0 pr-0">
                        <div class="textarea_style_input text_head p-3"> <?php if (isset($datacommande->commune_livr_desserv)) echo $datacommande->commune_livr_desserv; ?></div>
                    </div>
                </div>
            </div>
            <div class="row pl-1 pr-3 pb-2 pt-3 mb-4">
                <div class="p-2 text_head text-justify">
                    <?php if (isset($datacommande->livraison_comment)) echo $datacommande->livraison_comment;  ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center mb-5">
                    <div class="row">
                        <div class="col-lg-2 offset-lg-2 col-sm-12">
                            <a href="#id_recap" class="round_arrow_down"></a>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <p class="text_info text_bas_sm" style="text-align: left">Accès direct à la validation de la commande</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center" id="error_livraison_a_dom"></div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="modal fade" id="content_livrmodal" role="dialog" style="height: 1000px;">
        <div class="modal-dialog m-0">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header glissiere_tab">
                    <div class="col-12 text-center">
                        <div class="w-50 d-block m-auto" id="content_btn_livraison">
                            <div style="color: white;font-size: 15px;" data-dismiss="modal" id="close_livr" aria-label="Close">
                            <img style="width: 10%;height: auto;margin-bottom: 3px;" src="<?php echo base_url("assets/image/backs.png"); ?>"> Retour Commande
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body pl-0" id="modal-scroll-livraison">
                    <div class="container cont_all2 head_commande">
                        <div class="row pt-2">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12 d-flex" id="textarea_sm_livr0"><div class="text_label text-left">Livraison gratuite pour toute commande supérieure à </div></div>
                                    <div class="col-lg-6 col-sm-12 pl-2 pr-0" id="textarea_sm_livr1"><div class="value_title_sub textarea_style_input p-2 text_head">30€</div></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-6 col-sm-12  d-flex pl-5" id="textarea_sm_livr0">
                                        <div class="text_label">
                                            délai de livraison
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-12  pl-0" id="textarea_sm_livr0">
                                        <div class="value_title_sub textarea_style_input p-2 text_head">
                                            <?php if (isset($datacommande->commune_livr_desserv) and isset($datacommande->delai_livr_debut)) echo $datacommande->delai_livr_debut; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row mt-4">
                                <div class="col-lg-3 col-sm-12  pl-0 d-flex">
                                    <div class="text_label">Jours et heures d'ouverture </div>
                                </div>
                                <div class="col-lg-9 col-sm-12  pl-0 pr-0">
                                    <div class="textarea_style_input text_head p-2">Ouvert du mardi au samedi de 09h00 à 13h30 et de 18h00 à 21h00</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row mt-4">
                                <div class="col-lg-3 col-sm-12  pl-0 d-flex">
                                    <div class="text_label">Jours et heures de livraison </div>
                                </div>
                                <div class="col-lg-9 col-sm-12  pl-0 pr-0">
                                    <div class="textarea_style_input text_head <?php if (isset($datacommande->horaire_livr_ouvert) && $datacommande->horaire_livr_ouvert != null){ echo "p-2" ; }else{ echo "p-3"; } ?>"><?php if (isset($datacommande->horaire_livr_ouvert)) echo $datacommande->horaire_livr_ouvert; ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row mt-4">
                                <div class="col-lg-7">
                                    <div class="row">
                                        <div class="col-lg-5 col-sm-12  pl-0 pr-0 d-flex" >
                                            <div class="text_label">Date souhaitée de la livraison </div>
                                        </div>
                                        <div class="col-lg-7 col-sm-12  pl-2 pr-0" id="textarea_sm_livr2">
                                            <input id="jour_livraison" class="form-control textarea_style text_head" type="date">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="row">
                                        <div class="col-lg-8 col-sm-12  pr-0 d-flex" >
                                            <div class="text_label">Heure de livraison souhaitée  </div>
                                        </div>
                                        <div class="col-lg-4 col-sm-12  pr-0 pl-0" id="textarea_sm_livr3">
                                            <!--                                <select id="heure_livraison" class="form-control textarea_style text_head">-->
                                            <!--                                    <option value="09:00">9:00</option>-->
                                            <!--                                    <option value="10:00">10:00</option>-->
                                            <!--                                    <option value="11:00">11:00</option>-->
                                            <!--                                    <option value="12:00">12:00</option>-->
                                            <!--                                    <option value="13:00">13:00</option>-->
                                            <!--                                    <option value="18:00">18:00</option>-->
                                            <!--                                    <option value="19:00">19:00</option>-->
                                            <!--                                    <option value="20:00">20:00</option>-->
                                            <!--                                </select>-->
                                            <input type="time" class="form-control textarea_style text_head" id="heure_livraison">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row mt-3">
                                <div class="col-lg-3 col-sm-12  pl-0 d-flex">
                                    <div class="text_label text-justify">Communes desservies </div>
                                </div>
                                <div class="col-lg-9 col-sm-12 pl-0 pr-0">
                                    <div class="textarea_style_input text_head p-3"> <?php if (isset($datacommande->commune_livr_desserv)) echo $datacommande->commune_livr_desserv; ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="row pl-1 pr-3 pb-2 pt-3 mb-4">
                            <div class="p-2 text_head text-justify">
                                <?php if (isset($datacommande->livraison_comment)) echo $datacommande->livraison_comment;  ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-center" id="error_livraison_a_dom"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php } ?>
<?php if (isset($datacommande->is_activ_vente_differe) and $datacommande->is_activ_vente_differe == '1') { ?>
    <div class="col-lg-8 col-12 m-auto">
        <p class="text_head1 mt-4 mb-3 " style="color:#E80EAE">
            « Si votre choix se porte sur la vente en ligne avec un paiement différé , vous devez obligatoirement activer cette glissière. »
        </p>
    </div>
    <div class="row glissiere_tab mt-sm-2 mt-lg-2">
        <form class="m-0 w-100 row pt-sm-3 pt-2 pb-sm-3 pb-0">
            <div class="col-lg-2 offset-lg-2 col-sm-6">
                <div class="row">
                    <div class="col-md-6 col-sm-6 text-center col-4">
                        <label  class="switch" id="activ_glissiere_differe">
                            <input onchange="switch_differe()" id="id_check_differe" type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </div>
                    <div class="col-md-6 col-sm-6 p-0 col-8">
                        <p class="titre_volet">Ouvrir ou fermer ce volet</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div id="txt_title_gli1" class="text-sm-left title_gli titre_gli_up pt-1 text-lg-center" style="<?php if(isMobile()){ echo "margin-right: -11px;";}else{ echo "margin-right: -100px;"; }?>">VENTE EN LIGNE - PAIEMENT DIFFERE</div>
            </div>
        </form>
    </div>
    <div class="row p-2 pb-5 pt-4 d-none" id="differe_content">
        <div class="container cont_all2 head_commande">
            <div class="row pt-2 pb-4">
                <div class="col-lg-12">
                    <div class="title_differe">
                        COMMANDE EN LIGNE :  ENVOIS DE DOCUMENTS (devis, factures...) POUR LES COLLECTIVITES ET ENTREPRISES AVEC UN PAIEMENT EN RETOUR (virements, chèques)
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="text_label text-center text-justify">Jours et heures de contact</div>
                    <div class="col-12 pl-0 pr-0">
                        <textarea disabled="disabled" id="comment_differe" class="textarea_style_input p-0 pl-3 pr-3 p-2 form-control text_head"><?php if (isset($datacommande->horaire_contact) AND $datacommande->horaire_contact !=null){ echo $datacommande->horaire_contact; } ?></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 offset-lg-2 text-center mb-5">
                    <div class="row">
                        <div class="col-lg-2 offset-lg-2 col-sm-12">
                            <a href="#id_recap" class="round_arrow_down"></a>
                        </div>
                        <div class="col-lg-8 col-sm-12">
                            <p class="text_info text_bas_sm" style="text-align: left">Accès direct à la validation de la commande</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 text-center" id="error_livraison_a_dom"></div>
            </div>

        </div>
    </div>

    <div class="modal fade" id="content_differemodal" role="dialog">
        <div class="modal-dialog m-0">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header glissiere_tab">
                    <div class="col-12 text-center">
                        <div class="w-50 d-block m-auto" id="content_btn_differe">
                            <div style="color: white;font-size: 15px;" data-dismiss="modal" id="close_differe" aria-label="Close">
                            <img style="width: 10%;height: auto;margin-bottom: 3px;" src="<?php echo base_url("assets/image/backs.png"); ?>"> Retour Commande
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body pl-0" id="modal-scroll-differe">
                    <div class="container cont_all2 head_commande">
                        <div class="row pt-2 pb-4">
                            <div class="col-lg-12">
                                <div class="title_differe">
                                    COMMANDE EN LIGNE :  ENVOIS DE DOCUMENTS (devis, factures...) POUR LES COLLECTIVITES ET ENTREPRISES AVEC UN PAIEMENT EN RETOUR (virements, chèques)
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="text_label text-center text-justify">Jours et heures de contact</div>
                                <div class="col-12 pl-0 pr-0">
                                    <textarea disabled="disabled" id="comment_differe" class="textarea_style_input p-0 pl-3 pr-3 p-2 form-control text_head"><?php if (isset($datacommande->horaire_contact) AND $datacommande->horaire_contact !=null){ echo $datacommande->horaire_contact; } ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 text-center" id="error_livraison_a_dom"></div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

<?php } ?>
<div class="row">
    <div class="col-12 text-center" id="choose_livr_error">

    </div>
</div>
<style>
    @media screen and (max-width: 700px) {
        #col_jour_emporter{
            padding-right: 0px!important;
        }
    }
    .round_arrow_down{
        position: relative;
        padding-top: 12px;
        padding-bottom: 12px;
        padding-left: 20px;
        padding-right: 20px;
        background: #e80eae;
        border-radius: 40px;
    }
    .round_arrow_down::before{
        content:"\f107";
        font-family: FontAwesome!important;
        font-size: 20px;
        color: #ffffff;
        top: 5px;
        left: 13px;
        position: absolute;
    }
    .text_info{
        font-size:14px;
        font-family: Futura-LT-Book, Sans-Serif;
        padding-left: 20px;
      }
</style>
<?php
if (isMobile()) { ?>
    <script>
        $("#close_emp").click(function(){
            $("#content_empmodal").hide();
        });
        $("#close_livr").click(function(){
            $("#content_livrmodal").hide();
        });
        $("#close_differe").click(function(){
            $("#content_differemodal").hide();
        });
        function switch_emporter() {
            if(document.getElementById("id_check_epm").checked == true){
                $("#content_empmodal").modal("toggle");
                $("#activ_glissiere_emporter_value").val(1);
                $('#value_emp_activ').val(1);


                if ($('#value_livr_activ').val() == "1"){
                    $('#value_livr_activ').val(0)
                    $("#activ_glissiere_livraison").click();
                }
                if ($('#value_differe_activ').val() == "1"){
                    $('#value_differe_activ').val(0);
                    $("#activ_glissiere_differe").click();
                }
                $(".emp_contentss").removeClass("d-none");
                $(".emp_contentss").addClass("d-block");
                $(".emp_contentss").removeClass("d-none");

                $(".livr_contentss").removeClass("d-block");
                $(".livr_contentss").addClass("d-none");
                $(".diff_contentss").removeClass("d-flex");
                $(".diff_contentss").addClass("d-none");


            }else{
                $("#content_empmodal").hide();
                $('#value_emp_activ').val(0);
                $("#activ_glissiere_emporter_value").val(0);
                $(".emp_contentss").removeClass("d-block");
                $(".emp_contentss").addClass("d-none");
                // $('#value_emp_activ').val('0');
                // $('#value_livr_activ').val('1');
                // $('#activ_glissiere_emporter').attr('checked', false);
                // $('#activ_glissiere_emporter').click();
                // if($('#value_livr_activ').val() == 1){
                //     $('#activ_glissiere_livraison').attr('checked', true);
                //     $('#activ_glissiere_livraison').click();
                // }
            }
        }
        function switch_livraison() {
            if(document.getElementById("id_check_livr").checked == true){
                $("#content_livrmodal").modal("toggle");
                $('#value_livr_activ').val(1);
                $("#activ_glissiere_livraison_value").val(1);


                $(".emp_contentss").removeClass("d-block");
                $(".emp_contentss").addClass("d-none");

                $(".diff_contentss").removeClass("d-flex");
                $(".diff_contentss").addClass("d-none");

                $(".livr_contentss").removeClass("d-none");
                $(".livr_contentss").addClass("d-block");

                if ($('#value_emp_activ').val() == 1){
                    $('#value_emp_activ').val(0);
                    $("#activ_glissiere_emporter").click();
                }
                if ($('#value_differe_activ').val() == 1){
                    $('#value_differe_activ').val(0);
                    $("#activ_glissiere_differe").click();
                }


            }else{
                $("#content_livrmodal").hide();
                $(".livr_contentss").removeClass("d-block");
                $(".livr_contentss").addClass("d-none");
                $('#value_livr_activ').val(0);
                $("#activ_glissiere_livraison_value").val(0);


            }

        }
        function switch_differe(){
            if(document.getElementById("id_check_differe").checked == true){
                $("#content_differemodal").modal("toggle");
                $('#value_differe_activ').val(1);
                $("#activ_glissiere_differe_value").val(1);

                if ($('#value_emp_activ').val() == "1"){
                    $('#value_emp_activ').val(0);
                    $("#activ_glissiere_emporter").click();
                }
                if ($('#value_livr_activ').val() == "1"){
                    $('#value_livr_activ').val(0)
                    $("#activ_glissiere_livraison").click();
                }
                $(".emp_contentss").removeClass("d-block");
                $(".emp_contentss").addClass("d-none");

                $(".livr_contentss").removeClass("d-block");
                $(".livr_contentss").addClass("d-none");
                $(".diff_contentss").removeClass("d-none");
                $(".diff_contentss").addClass("d-flex");



            }else{
                $("#content_differemodal").hide();
                $('#value_differe_activ').val(0);
                $("#activ_glissiere_differe_value").val(0);
                $(".diff_contentss").removeClass("d-flex");
                $(".diff_contentss").addClass("d-none");

            }

        }
    </script>
<?php }else{ ?>
    <script>
        function switch_emporter() {
            if(document.getElementById("id_check_epm").checked == true){
                $("#activ_glissiere_emporter_value").val(1);
                $('#value_emp_activ').val(1);

                $("#emporter_content").removeClass("d-none");
                $("#emporter_content").addClass("d-block");

                if ($('#value_livr_activ').val() == "1"){
                    $('#value_livr_activ').val(0)
                    $("#livraison_content").removeClass("d-block");
                    $("#livraison_content").addClass("d-none");
                    $("#activ_glissiere_livraison").click();
                }
                if ($('#value_differe_activ').val() == "1"){
                    $('#value_differe_activ').val(0);
                    $("#differe_content").removeClass("d-block");
                    $("#differe_content").addClass("d-none");
                    $("#activ_glissiere_differe").click();
                }
                $(".emp_contentss").removeClass("d-none");
                $(".emp_contentss").addClass("d-block");
                $(".emp_contentss").removeClass("d-none");

                $(".livr_contentss").removeClass("d-block");
                $(".livr_contentss").addClass("d-none");
                $(".diff_contentss").removeClass("d-flex");
                $(".diff_contentss").addClass("d-none");



            }else{
                $("#emporter_content").removeClass("d-block");
                $("#emporter_content").addClass("d-none");
                $('#value_emp_activ').val(0);
                $("#activ_glissiere_emporter_value").val(0);
                $(".emp_contentss").removeClass("d-block");
                $(".emp_contentss").addClass("d-none");

                // $('#value_emp_activ').val('0');
                // $('#value_livr_activ').val('1');
                // $('#activ_glissiere_emporter').attr('checked', false);
                // $('#activ_glissiere_emporter').click();
                // if($('#value_livr_activ').val() == 1){
                //     $('#activ_glissiere_livraison').attr('checked', true);
                //     $('#activ_glissiere_livraison').click();
                // }
            }
        }
        function switch_differe(){
            if(document.getElementById("id_check_differe").checked == true){
                $('#value_differe_activ').val(1);
                $("#activ_glissiere_differe_value").val(1);
                $("#differe_content").removeClass("d-none");
                $("#differe_content").addClass("d-block");
                if ($('#value_emp_activ').val() == "1"){
                    $('#value_emp_activ').val(0);
                    $("#activ_glissiere_emporter").click();
                    $("#emporter_content").removeClass("d-block");
                    $("#emporter_content").addClass("d-none");
                }
                if ($('#value_livr_activ').val() == "1"){
                    $('#value_livr_activ').val(0)
                    $("#activ_glissiere_livraison").click();
                    $("#livraison_content").removeClass("d-block");
                    $("#livraison_content").addClass("d-none");
                }
                $(".emp_contentss").removeClass("d-block");
                $(".emp_contentss").addClass("d-none");
                $(".livr_contentss").removeClass("d-block");
                $(".livr_contentss").addClass("d-none");
                $(".diff_contentss").removeClass("d-none");
                $(".diff_contentss").addClass("d-flex");


            }else{
                $("#differe_content").removeClass("d-block");
                $("#differe_content").addClass("d-none");
                $('#value_differe_activ').val(0);
                $("#activ_glissiere_differe_value").val(0);
                $(".diff_contentss").removeClass("d-flex");
                $(".diff_contentss").addClass("d-none");


            }

        }
        function switch_livraison() {
            if(document.getElementById("id_check_livr").checked == true){
                $('#value_livr_activ').val(1);
                $("#activ_glissiere_livraison_value").val(1);
                $("#livraison_content").removeClass("d-none");
                $("#livraison_content").addClass("d-block");

                $(".emp_contentss").removeClass("d-block");
                $(".emp_contentss").addClass("d-none");

                $(".diff_contentss").removeClass("d-flex");
                $(".diff_contentss").addClass("d-none");

                $(".livr_contentss").removeClass("d-none");
                $(".livr_contentss").addClass("d-block");

                if ($('#value_emp_activ').val() == 1){
                    $('#value_emp_activ').val(0);
                    $("#activ_glissiere_emporter").click();
                    $("#emporter_content").removeClass("d-block");
                    $("#emporter_content").addClass("d-none");
                }
                if ($('#value_differe_activ').val() == 1){
                    $('#value_differe_activ').val(0);
                    $("#activ_glissiere_differe").click();
                    $("#differe_content").removeClass("d-block");
                    $("#differe_content").addClass("d-none");
                }



            }else{

                $("#livraison_content").removeClass("d-block");
                $("#livraison_content").addClass("d-none");
                $(".livr_contentss").removeClass("d-block");
                $(".livr_contentss").addClass("d-none");
                $('#value_livr_activ').val(0);
                $("#activ_glissiere_livraison_value").val(0);
                

            }

        }
    </script>
<?php } ?>