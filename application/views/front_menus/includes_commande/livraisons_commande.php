<?php if (isset($datacommande->is_activ_livr) AND $datacommande->is_activ_livr == '1'){ ?>
    <div class="col-sm-12">
        <div class="row mt-4 d-none livr_contentss">
            <div class="col-lg-12 pt-4 pl-0">
                <div class="w-100 middle_title">
                    VENTE EN LIGNE :  LIVRAISON ET PAIEMENT A DOMICILE
                </div>
                <div class="w-100 pt-4">
                    <p class="text_head"><?php echo $datacommande->commune_livr_desserv ?? "";?></p>
                </div>
            </div>
            <div class="col-lg-12 pt-2 pb-0 pl-0 pr-0">
                <div class="w-100">
                    <span id="text_vent_livr_jour" class="text_head"></span>
                    <span id="text_vent_livr_heure" class="text_head"></span>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){

                    $('#jour_livraison').change(function(){
                        var jour = $('#jour_livraison').val();
                        var result = jour.split('-');
                        if(result[1] == '01'){var mois = 'janvier';}else if(result[1] == '02'){var mois = 'février';}else if(result[1] == '02'){var mois = 'février';}else if(result[1] == '03'){var mois = 'mars';}else if(result[1] == '04'){var mois = 'avril';}else if(result[1] == '05'){var mois = 'mai';}else if(result[1] == '06'){var mois = 'juin';}else if(result[1] == '07'){var mois = 'juillet';}else if(result[1] == '08'){var mois = 'août';}else if(result[1] == '09'){var mois = 'septembre';}else if(result[1] == '10'){var mois = 'octobre';}else if(result[1] == '11'){var mois = 'novembre';}else if(result[1] == '12'){var mois = 'décembre';}
                        $('#text_vent_livr_jour').html('Jour et heure souhaitée le <span id="id_jour_souhait_livr">'+result[2]+' '+mois+' '+result[0]+'</span>')
                    })
                    $('#heure_livraison').change(function(){
                        var heure = $('#heure_livraison').val();
                        var heure_get = heure.split(':');
                        $('#text_vent_livr_heure').html(' à <span id="id_heure_souhait_livr">'+heure_get[0]+' heure '+heure_get[1]+ ' minutes </span>')
                    })

                })
            </script>
        </div>
        <div class="row pl-0 shadowed pb-4 d-none livr_contentss">
            <div class="col-12 cond pt-2">
                <div class="row">
                    <div class="col-lg-3 col-sm-12 p-0 text_head">Conditions de règlement</div>
                    <?php if ($datacommande->is_activ_cheque_livr =="1" ||  $datacommande->is_activ_cheque_livr =="1"){ ?>
                        <div class="col-lg-1 col-2">
                            <input id="type_norm_cheque_livr" class="check_cond" type="checkbox">
                        </div>
                        <div class="col-lg-3 col-10 pr-0 pl-0">
                            <span class="txt_cond text_head">Chèque dûment rempli et signé</span>
                        </div>
                    <?php } ?>
                    <?php if ($datacommande->is_activ_termin_bank_livr =="1"  || $datacommande->is_activ_termin_bank_livr =="1"){ ?>
                        <div class="col-lg-1 col-2">
                            <input id="type_norm_bank_livr" class="check_cond" type="checkbox">
                        </div>
                        <div class="col-lg-4 col-10 pl-0">
                            <span class="txt_cond text_head">Carte bancaire avec un terminal de paiement</span>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>