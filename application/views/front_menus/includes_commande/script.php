<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/pdfmake.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.62/vfs_fonts.js" type="text/javascript"></script>
<script type="text/javascript">
    function doCapture() {
        //
        window.scrollTo(0, 0);
        // html2canvas(document.getElementById("recap_to_export")).then(function (canvas) {
        //
        //     // Get the image data as JPEG and 0.9 quality (0.0 - 1.0)
        //     console.log(canvas.toDataURL("image/jpeg", 0.9));
        // });
        html2canvas(document.querySelector("#infocliall")).then(canvas => {
            // document.body.appendChild(canvas)
            var data = canvas.toDataURL();
            // var docDefinition = {
            //     content: [
            //         {
            //         image: data,
            //         width: 500,
            //         }
            //     ]
            // };
            var send_data = {image: data, width: 500,style: 'para'}
            getvaluetype(send_data);
        });
    }
    function getvaluetype(send_data){
        html2canvas(document.querySelector("#type_vente")).then(canvas => {
            // document.body.appendChild(canvas)
            var data = canvas.toDataURL();
            // var docDefinition = {
            //     content: [
            //         {
            //         image: data,
            //         width: 500,
            //         }
            //     ]
            // };
            var send_data2 = {image: data, width: 500,}
            getvaluegli(send_data,send_data2);
        });
    }
    function getvaluegli(send_data,send_data2){
        html2canvas(document.querySelector("#content_gli")).then(canvas => {
            // document.body.appendChild(canvas)
            var data = canvas.toDataURL();
            // var docDefinition = {
            //     content: [
            //         {
            //         image: data,
            //         width: 500,
            //         }
            //     ]
            // };
            var send_data3 = {image: data, width: 500,}
            get_value_content_recap(send_data,send_data2,send_data3);
        });
    }
    function get_value_content_recap(send_data,send_data2,send_data3){
        var nom = $('#nom_client').val();
        var numero = $('#number_card').val();
        if(nom != "" && nom != null){
            if(numero!= "" && numero != null){
                var addname = 'commande '+nom+' num'+numero;
            }else{
                var addname = 'commande '+nom;
            }
                html2canvas(document.querySelector("#recap_to_export")).then(canvas => {
                    // document.body.appendChild(canvas)
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [
                            {
                                text: 'Voici votre recapitulatif de commande',
                                style: 'header',
                            },
                            send_data,
                            send_data2,
                            send_data3,
                            {
                                image: data,
                                width: 500,
                                style: 'para',
                            }
                        ],
                        styles: {
                            header: {
                                fontSize: 20,
                                bold: true,
                                alignment: 'center',
                                margin: [0, 9, 0, 9]
                            },
                            para: {
                                margin: [0, 9, 0, 9]
                            }
                        }
                    };
                    pdfMake.createPdf(docDefinition).download( addname + ".pdf");
                });
        }else{
            alert('veuillez remplir le formulaire de renseignement!');
        }
    }
    $(document).ready(function(){
        $( "#upload_file_custom" ).submit(function(event) {
            $("#commande_error").html('<div class="w-100 text-center"><img src="/application/resources/sortez/images/loading.gif" alt="loading"/></div>');
            event.preventDefault();
            $.ajax({
                url:"<?php echo site_url('front_menus/commercant/save_command_specific_file')?>",
                method:"POST",
                data:new FormData(this),
                contentType:false,
                cache:false,
                processData:false,
                success:function(data)
                {
                    console.log(data);
                    if(data =="ok"){
                        $(document).scrollTop( $("#commande_error").offset().top );
                        $("#commande_error").html('<div class="alert alert-success" role="alert">\n' +
                            '  Votre commande a été enregistré avec success !\n' +
                            '</div>');
                    }else{
                        $(document).scrollTop( $("#commande_error").offset().top );
                        $("#commande_error").html('<div class="alert alert-danger" role="alert">\n' +
                            '  Une erreur a été constatée, veuillez reverifier votre commande !\n' +
                            '</div>');
                    }
                }
            })
        });
        $( "#check_cheque_type" ).change(function() {
            var is_check = document.getElementById("check_cheque_type").checked;
            if(is_check == true){
                $("#type_paiement_custom").val("cheque");
                document.getElementById("check_bank_type").checked = false;
            }
        });
        $( "#check_bank_type" ).change(function() {
            var is_check = document.getElementById("check_bank_type").checked;
            if(is_check == true){
                $("#type_paiement_custom").val("Bancaire");
                document.getElementById("check_cheque_type").checked = false;
            }
        });
    })

    function import_command_file(){
        var id_cli = $("#id_client").val();
        if (id_cli != "0"){
            $("#command_file").click();
        }else{
            alert("vous devez preciser votre numero de carte ou en obtenir un !");
            location.href="#number_card";
        }
    }

    function  submit_custom_form() {
        if ($("#type_paiement_custom").val() == "0" || $("#type_livraison").val() == "Enlèvement ou livraison" || $("#jour_souhaite").val() == "" || $("#command_file").val() == "" ){
            alert("champ obligatoire non rempli !");
        }else{
            var type_bank = document.getElementById('check_bank_type');
            var type_cheque = document.getElementById('check_cheque_type');
            if (type_bank.cheked == false && type_cheque.checked == false){
                alert('Vous devez choisir un type de paiement');
                throw new Error("my error message");
            }
            $("#upload_file_custom").submit();
        }

    }


    function get_users_info() {
        var num = $("#number_card").val();
        var datas = "nom_card="+num;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('front_menus/commercant/check_user'); ?>",
            data: datas ,
            success: function(data_saved) {
                if (data_saved != 'ko'){
                    //var parsed = JSON.parse(data_saved);
                    //console.log(parsed)
                    //$("#id_client").val(parsed.IdUser);
                    //$("#id_client2").val(parsed.IdUser);
                    //$("#nom_client").val(parsed.Nom);
                    //$("#prenom_client").val(parsed.Prenom);
                    //$("#code_postal_client").val(parsed.CodePostal);
                    //$("#adresse_client").val(parsed.Adresse);
                    //$("#ville_client").val(parsed.IdVille);
                    //if(parsed.Portable != null){
                    //    $("#telephone_client").val(parsed.Portable);
                    //}else{
                    //    $("#telephone_client").val(parsed.Telephone);
                    //}
                    //$("#date_naissance_client").val(parsed.DateNaissance);
                    //if (parsed.Email != null){
                    //    $("#mail_client").val(parsed.Email)
                    //}else{
                    //    $("#mail_client").val(parsed.Login)
                    //}
                    //$("#qr_content").html("<img src='<?php //echo base_url()?>//application/resources/front/images/cards/qrcode_"+parsed.virtual_card_img+"' class='img-fluid'/>")
                    //$("#demande_carte").hide();
                    location.reload();
                }else{
                    alert("Votre carte n'est pas encore valide!");
                }
            },
            error: function() {
                // alert('Erreur');
            }
        });
    }
    function subscribtion() {
        var nom_client = $("#nom_client").val();
        var prenom_client = $("#prenom_client").val();
        var code_postal_client = $("#code_postal_client").val();
        var adresse_client = $("#adresse_client").val();
        var ville_client = $("#ville_client").val();
        var telephone_client = $("#telephone_client").val();
        var date_naissance_client = $("#date_naissance_client").val();
        var mail_client = $("#mail_client").val()

        if ($("#nom_client").val() !=="" && $("#prenom_client").val() !=="" && $("#code_postal_client").val() !=="" && $("#adresse_client").val() !=="" && $("#ville_client").val() !=="" && $("#telephone_client").val() !== "" && $("#mail_client").val() !=""){

            var datasz = "nom_client="+nom_client+"&prenom_client="+prenom_client+"&code_postal_client="+code_postal_client+"&adresse_client="+adresse_client+"&ville_client="+ville_client+"&telephone_client="+telephone_client+"&date_naissance_client="+date_naissance_client+"&mail_client="+mail_client
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('front_menus/commercant/ajouter_compte_part'); ?>",
                data: datasz ,
                success: function(data_saved) {
                    $("#id_client").val(data_saved);
                },
                error: function() {
                    // alert('Erreur');
                }
            });
        }else{
            $("#txt_error_add").html("Les champs sont obligatoires");
            $("#txt_error_add").css("color","red");
        }
    }
    function send_command(){
        let menu_table_number_value = $("#menu_table_number_input").val();
        if(typeof menu_table_number_value !== 'undefined' && menu_table_number_value != ""){
            $("#commande_error").html('<img src="<?php echo base_url();?>application/resources/sortez/images/loading.gif"/>');
            var to_save = $('#recap_to_export').html();
            //$('#commande_error').html(to_save);
            let datas = "contenue="+to_save+"&idCom="+"<?php echo $infocom->IdCommercant; ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('front_menus/commercant/valid_command'); ?>",
                data: datas,
                success: function(datas) {
                    let data = JSON.parse(datas);
                    console.log(data);
                    if (data == "ok"){
                        $("#commande_error").html("");
                        $("#error_livraison_a_dom").html("");
                        $("#emporter_error").html("");
                        $("#choose_livr_error").html("");
                        $("#txt_error_add").html("");

                        $(document).scrollTop( $("#commande_error").offset().top );
                        $("#commande_error").html('<div class="alert alert-success" role="alert">\n' +
                            '  Votre commande a été enregistré avec success !\n' +
                            '</div>');
                    }else if(data == "sokok"){
                        $(document).scrollTop( $("#commande_error").offset().top );
                        $("#commande_error").html('<div class="alert alert-success" role="alert">\n' +
                            '  Votre commande a été enregistré avec success !\n' +
                            '</div>');
                    }
                },
                error: function() {
                }
            });
        } else {
            let table_number_val_error = '<div class="alert alert-danger" role="alert">Veuillez indiquer votre numéro de table !</div>';
            $("#commande_error").html(table_number_val_error);
        }
    }

    function change_total_price1(idprix, idnbre) {
        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss1_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss1_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli1total").val(get+"€");
        $("#total_mobile_gli1").val(get+"€");
        $("#totalgli1").val(get);
        $("#subs_bottomgli1").html(parseFloat(get).toFixed(2)+"€");
        // alert(parseFloat($("#subs_bottomgli5").val()));
        $('#all_totalised').html(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").html()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").html()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($('#all_totalised').html()).toFixed(2)+"€");
    }

    function change_total_price2(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss2_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss2_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli2total").val(get+"€");
        $("#total_mobile_gli2").val(get+"€");
        $("#totalgli2").val(get);
        $("#subs_bottomgli2").html(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").html()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").html()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($('#all_totalised').html()).toFixed(2)+"€");
    }

    function change_total_price3(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss3_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss3_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli3total").val(get+"€");
        $("#total_mobile_gli3").val(get+"€");
        $("#totalgli3").val(get);
        $("#subs_bottomgli3").html(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").html()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").html()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($('#all_totalised').html()).toFixed(2)+"€");
    }

    function change_total_price4(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss4_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss4_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli4total").val(get+"€");
        $("#total_mobile_gli4").val(get+"€");
        $("#totalgli4").val(get);
        $("#subs_bottomgli4").html(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").html()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").html()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($('#all_totalised').html()).toFixed(2)+"€");
    }

    function change_total_price6(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss6_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss6_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli6total").val(get+"€");
        $("#total_mobile_gli6").val(get+"€");
        $("#totalgli6").val(get);
        $("#subs_bottomgli6").html(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").html()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").html()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($('#all_totalised').html()).toFixed(2)+"€");
    }

    function change_total_price7(idprix, idnbre) {

        var get = 0;
        var class_name_prix = document.getElementsByClassName('nbre_gliss7_pr');
        for(i=0; i<class_name_prix.length; i++){
            var qtt = document.getElementsByClassName('nbre_gliss7_qt')[i].value;
            get = get + (parseFloat(class_name_prix[i].value.replace('€ ' , '')) * qtt);
        }
        $("#gli7total").val(get+"€");
        $("#total_mobile_gli7").val(get+"€");
        $("#totalgli7").val(get);
        $("#subs_bottomgli7").html(parseFloat(get).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($("#totalgli1").val())+parseFloat($("#totalgli2").val())+parseFloat($("#totalgli3").val())+parseFloat($("#totalgli4").val())+parseFloat($("#totalgli6").val())+parseFloat($("#totalgli7").val())+"€")
        if( parseFloat($("#all_totalised").html()) <= parseFloat(<?php echo $datacommande->price_to_reach; ?>)){
            $("#text_warn").html("Désolé ! votre montant d'achat est inférieur, vous ne pouvez pas en bénéficier !...");
            $("#is_graced").val(0);
            $("#is_bravo").removeClass("d-flex");
            $("#is_bravo").addClass("d-none");
        }else{
            $("#text_warn").html("Bravo ! votre montant d'achat est supérieur,vous pouvez en bénéficier !...");
            $("#is_graced").val(1);
            $("#is_bravo").removeClass("d-none");
            $("#is_bravo").addClass("d-flex");
            $("#rest_to_give").val(parseFloat($("#all_totalised").html()) - parseFloat(<?php echo $datacommande->price_to_win; ?>));
        }
        $("#rest_to_give").val(parseFloat($("#rest_to_give").val()).toFixed(2)+"€");
        $('#all_totalised').html(parseFloat($('#all_totalised').html()).toFixed(2)+"€");
    }
</script>
<script type="text/javascript">
    $("#id_recap").change(function (){
        if($("#id_recap_value").val() == 0){
            $("#content_recap").removeClass('d-none');
            $("#content_recap").addClass('d-block');
            $("#id_recap_value").val('1');
        }
        else{
            $("#content_recap").removeClass('d-block');
            $("#content_recap").addClass('d-none');
            $("#id_recap_value").val('0');
        }
    });
    $("#is_activ_facture_differe").change(function(){
        if ($('#is_activ_facture_differe').is(":checked"))
        {
            $("#is_activ_facture_differe_hidden").val(1);
        }else{
            $("#is_activ_facture_differe_hidden").val(0);
        }
    });
    $("#is_activ_pro_forma_differe").change(function(){
        if ($('#is_activ_pro_forma_differe').is(":checked"))
        {
            $("#is_activ_pro_forma_differe_hidden").val(1);
        }else{
            $("#is_activ_pro_forma_differe_hidden").val(0);
        }
    });
    $("#is_activ_devis_differe").change(function(){
        if ($('#is_activ_devis_differe').is(":checked"))
        {
            $("#is_activ_devis_differe_hidden").val(1);
        }else{
            $("#is_activ_devis_differe_hidden").val(0);
        }
    });

    $("#type_norm_cheque_livr").click(function (){
        if(document.getElementById('type_norm_bank_livr') != null){
            document.getElementById('type_norm_bank_livr').checked = false;
        }
        if(document.getElementById('accept_paypal') != null){
            document.getElementById('accept_paypal').checked = false;
        }
        $("#type_paiment_cli").val("Chèque dûment rempli et signé");
    });
    $("#accept_paypal").click(function (){
        if(document.getElementById('type_norm_bank_livr') != null){
            document.getElementById('type_norm_bank_livr').checked = false;
        }
        if(document.getElementById('type_norm_cheque_livr') != null){
            document.getElementById('type_norm_cheque_livr').checked = false;
        }
        $("#type_paiment_cli").val("Par Paypal");
    });
    $("#type_norm_bank_livr").click(function (){
        if(document.getElementById('type_norm_cheque_livr') !=null ){
            document.getElementById('type_norm_cheque_livr').checked = false;
        }
        if(document.getElementById('accept_paypal') !=null ){
            document.getElementById('accept_paypal').checked = false;
        }
        $("#type_paiment_cli").val("Carte bancaire avec un terminal de paiement");
    });
    $("#is_activ_devis_differe").click(function (){
        $("#type_facture_differe").val('Devis');
        if(document.getElementById('is_activ_pro_forma_differe') != null){
            document.getElementById('is_activ_pro_forma_differe').checked = false;
        }
        if(document.getElementById('is_activ_facture_differe') != null){
            document.getElementById('is_activ_facture_differe').checked = false;
        }

        // document.getElementById('is_activ_Cheque_differe').checked = false;
        // document.getElementById('is_activ_Virement_differe').checked = false;
        // document.getElementById('is_activ_carte_differe').checked = false;

    });

    $("#is_activ_pro_forma_differe").click(function (){
        $("#type_facture_differe").val('Facture pro forma');
        if(document.getElementById('is_activ_devis_differe') != null){
            document.getElementById('is_activ_devis_differe').checked = false;
        }
        if(document.getElementById('is_activ_facture_differe') != null){
            document.getElementById('is_activ_facture_differe').checked = false;
        }
        // document.getElementById('is_activ_Cheque_differe').checked = false;
        // document.getElementById('is_activ_Virement_differe').checked = false;
        // document.getElementById('is_activ_carte_differe').checked = false;
    });

    $("#is_activ_facture_differe").click(function (){
        $("#type_facture_differe").val('Facture');
        if(document.getElementById('is_activ_devis_differe') != null){
            document.getElementById('is_activ_devis_differe').checked = false;
        }
        if(document.getElementById('is_activ_pro_forma_differe') != null){
            document.getElementById('is_activ_pro_forma_differe').checked = false;
        }
        // document.getElementById('is_activ_Cheque_differe').checked = false;
        // document.getElementById('is_activ_Virement_differe').checked = false;
        // document.getElementById('is_activ_carte_differe').checked = false;
    });

    $("#is_activ_Cheque_differe").click(function (){
        // document.getElementById('is_activ_devis_differe').checked = false;
        // document.getElementById('is_activ_pro_forma_differe').checked = false;
        // document.getElementById('is_activ_facture_differe').checked = false;
        if(document.getElementById('is_activ_Virement_differe') != null){
            document.getElementById('is_activ_Virement_differe').checked = false;
        }
        if(document.getElementById('is_activ_carte_differe') != null){
            document.getElementById('is_activ_carte_differe').checked = false;
        }
        $("#type_paiment_cli").val("Chèque");
    });

    $("#is_activ_Virement_differe").click(function (){
        // document.getElementById('is_activ_devis_differe').checked = false;
        // document.getElementById('is_activ_pro_forma_differe').checked = false;
        if(document.getElementById('is_activ_facture_differe') != null){
            document.getElementById('is_activ_facture_differe').checked = false;
        }
        if(document.getElementById('is_activ_Cheque_differe') != null){
            document.getElementById('is_activ_Cheque_differe').checked = false;
        }
        if(document.getElementById('is_activ_carte_differe') != null){
            document.getElementById('is_activ_carte_differe').checked = false;
        }


        $("#type_paiment_cli").val("Virement");
    });

    $("#is_activ_carte_differe").click(function (){
        // document.getElementById('is_activ_devis_differe').checked = false;
        // document.getElementById('is_activ_pro_forma_differe').checked = false;
        // document.getElementById('is_activ_facture_differe').checked = false;
        if(document.getElementById('is_activ_Cheque_differe') != null){
            document.getElementById('is_activ_Cheque_differe').checked = false;
        }
        if(document.getElementById('is_activ_Virement_differe') != null){
            document.getElementById('is_activ_Virement_differe').checked = false;
        }

        $("#type_paiment_cli").val("Carte bancaire");
    });

    $("#type_norm_cheque").click(function (){
        if(document.getElementById('type_norm_bank') != null) {
            document.getElementById('type_norm_bank').checked = false;
        }
        if(document.getElementById('type_norm_espace') != null) {
            document.getElementById('type_norm_espace').checked = false;
        }
        $("#type_paiment_cli").val("Chèque");
    });
    $("#type_norm_bank").click(function (){
        if(document.getElementById('type_norm_cheque') != null){
            document.getElementById('type_norm_cheque').checked = false;
        }
        if(document.getElementById('type_norm_espace') != null) {
            document.getElementById('type_norm_espace').checked = false;
        }
        $("#type_paiment_cli").val("Carte bancaire");
    });
    $("#type_norm_espace").click(function (){
        if(document.getElementById('type_norm_cheque') != null){
            document.getElementById('type_norm_cheque').checked = false;
        }
        if(document.getElementById('type_norm_bank') != null) {
            document.getElementById('type_norm_bank').checked = false;
        }
        $("#type_paiment_cli").val("Espèces");
    });
    // $("#activ_glissiere_emporter").change(function (){
    //     if($("#value_emp_activ").val() == 0){
    //         $("#emporter_content").removeClass('d-none');
    //         $("#emporter_content").addClass('d-block');
    //         switch_emporter();
    //     }
    //     else{
    //         $("#emporter_content").removeClass('d-block');
    //         $("#emporter_content").addClass('d-none');
    //         switch_emporter();
    //     }
    // })
    // $("#activ_glissiere_livraison").change(function (){
    //     if($("#value_livr_activ").val() == 0){
    //         $("#livraison_content").removeClass('d-none');
    //         $("#livraison_content").addClass('d-block');
    //         switch_livraison();
    //     }
    //     else{
    //         $("#livraison_content").removeClass('d-block');
    //         $("#livraison_content").addClass('d-none');
    //         switch_livraison();
    //     }
    // })
    //
    // $("#activ_glissiere_differe").change(function (){
    //     if($("#value_differe_activ").val() == 0){
    //         $("#differe_content").removeClass('d-none');
    //         $("#differe_content").addClass('d-block');
    //         switch_differe();
    //     }
    //     else{
    //         $("#livraison_content").removeClass('d-block');
    //         $("#livraison_content").addClass('d-none');
    //         switch_differe();
    //     }
    // })

    function check_show_categ_partner(){

    }
</script>