<div id="recap_to_export">

    <link href="<?php echo base_url();?>assets/css/bootstrap_latest.css" rel="stylesheet" type="text/css">

    <link href="<?php echo base_url();?>application/views/front_menus/includes_commande/css/style.css" rel="stylesheet" type="text/css">

    <?php if (isset($data_gli1) && !empty($data_gli1) && isset($title_gli1->is_activ_glissiere) && $title_gli1->is_activ_glissiere != 0) { ?>

<div class="row row glissiere_tab marge_gli_1 pt-4 pb-4 title_gli_sm_height">

    <div class="col-lg-2 offset-lg-2 col-sm-12">

    </div>

    <div style="text-align: center; padding: 15px; margin: 10px;color:white" id="id_recap"  class="col-lg-4 titre_gli_up title_gli text-sm-left text-md-center pt-3 pt-md-0">RECAPITULATIF</div>

    <div class="col-lg-4"></div>

</div>

<div class="container pt-2 d-block m-auto" id="content_recap">

    <div style="text-align:center" class=" font-weight-bold">

        <span id="menu_table_number_span" style="width: 300px; margin: 0 auto; font-size: 24px; font-weight: bold;"></span>

    </div>


    <div class="row">


    <!-- debut table -->
        <table class="col-lg-12 mt-4 col-sm-12">

            <div class="m-0">

                <input type="hidden" id="totalgli1" value="0">

                <input value="0" id="totalgli2" type="hidden" />

                <input value="0" id="totalgli3" type="hidden" />

                <input value="0" id="totalgli4" type="hidden" />

                <input value="0" id="totalgli6" type="hidden" />

                <input value="0" id="totalgli7" type="hidden" />



                <input value="0" id="is_activ_facture_differe_hidden" type="hidden" />

                <input value="0" id="is_activ_pro_forma_differe_hidden" type="hidden" />

                <input value="0" id="is_activ_devis_differe_hidden" type="hidden" />



                <input value="0" id="is_activ_Virement_differe_hidden" type="hidden" />

                <input value="0" id="is_activ_carte_differe_hidden" type="hidden" />

                <input value="0" id="is_activ_Cheque_differe_hidden" type="hidden" />



                <input value="" id="type_paiment_cli" type="hidden" />


                <input type="hidden" value="" id="type_facture_differe">



                            <!-- gli1 -->

                    <tr class="row gli_num_1 recap_commad_item_line" id="recap_commad_item_line" style="  margin-left: 1rem;display: flex;  <?php if (isset($title_gli1->is_activ_glissiere) AND ($title_gli1->is_activ_glissiere != 0)){echo "display:flex;";}else{ echo "display:none;";} ?>">

                        <td id="gli_1_recap_container">
                            <div class="recap_gli_title gli_1_title">
                                <?php if (isset($title_gli1->titre_glissiere) and $title_gli1->titre_glissiere != null) echo $title_gli1->titre_glissiere;?>
                            </div>
                        </td>
                            <td style=" margin-left:37rem;display: flex;" class="">
                            <span id="subs_bottomgli1">0&euro;</span>
                            </td>
                    </tr>
                            <tr style=" margin-left:1em;display: flex;" class=" font-weight-bold">
                                <td class="">Produit</td>
                                <td style=" margin-left: 21em;" class="">Prix</td>
                                <td style=" margin-left: 17rem;" class="">Nombre</td>
                            </tr>

                            <?php for ($iii = 0; $iii <= 30; $iii++) { ?>

                                <tr style=" margin-left: 1rem;display: flex;"  id="gli_1_recap_content_<?= $iii;?>"></tr>
                            <?php } ?>


                                <!-- gli2 -->

                    <tr class="row gli_num_2 recap_commad_item_line " style="  margin-left: 1rem;display: flex; <?php if (isset($title_gli2->is_activ_glissiere) AND ($title_gli2->is_activ_glissiere != 0)){echo "display:flex;";}else{ echo "display:none;";} ?>">
                        <td id="gli_2_recap_container">
                            <div class="recap_gli_title gli_2_title">
                                <?php if (isset($title_gli2->titre_glissiere) and $title_gli2->titre_glissiere != null) echo $title_gli2->titre_glissiere;?>
                            </div>
                        </td>
                            <td style=" margin-left:38rem;display: flex;" class="">
                                <span id="subs_bottomgli2">0&euro;</span>
                            </td>
                    </tr>
                            <tr  style=" margin-left:1em;display: flex;" class=" font-weight-bold">
                                <td >Produit</td>
                                <td style=" margin-left: 21em;">Prix</td>
                                <td style=" margin-left: 17rem;" >Nombre</td>
                            </tr>

                            <?php for ($iii = 0; $iii <= 30; $iii++) {?>
                                <tr style=" margin-left: 1rem;display: flex;"  id="gli_2_recap_content_<?= $iii;?>"></tr>
                            <?php } ?>

 

                                <!-- gli3 -->

                    <tr class="row gli_num_3 recap_commad_item_line " style=" margin-left: 1rem;display: flex;<?php if (isset($title_gli3->is_activ_glissiere) AND ($title_gli3->is_activ_glissiere != 0)){ echo "display:flex;";}else{ echo "display:none;";} ?>">

                        <td class="" id="gli_3_recap_container">
                            <div class="recap_gli_title gli_3_title">
                                <?php if (isset($title_gli3->titre_glissiere) and $title_gli3->titre_glissiere != null) echo $title_gli3->titre_glissiere;?>
                            </div>
                        </td>
                            <td style=" margin-left:37rem;display: flex;" class="">
                            <span id="subs_bottomgli3">0&euro;</span>
                            </td>
                     </tr>
                            <tr style=" margin-left:1em;display: flex;"  class="font-weight-bold">
                                <td class="">Produit</td>
                                <td style=" margin-left: 21em;"  class="">Prix</td>
                                <td style=" margin-left: 17rem;"  class="">Nombre</td>
                            </tr>

                            <?php for ($iii = 0; $iii <= 30; $iii++) {?>
                                <tr style=" margin-left: 1rem;display: flex;"  id="gli_3_recap_content_<?= $iii;?>"></tr>
                            <?php } ?>


                                <!-- gli4 -->

                    <tr class="row gli_num_4 recap_commad_item_line " style=" margin-left: 1rem;display: flex;<?php if (isset($title_gli4->is_activ_glissiere) AND ($title_gli4->is_activ_glissiere != 0)){ echo "display:flex;";}else{ echo "display:none;"; } ?>" >
                        <td class="" id="gli_4_recap_container">
                            <div class="recap_gli_title gli_4_title">
                                <?php if (isset($title_gli4->titre_glissiere) and $title_gli4->titre_glissiere != null) echo $title_gli4->titre_glissiere;?>
                            </div>
                        </td>
                            <td style=" margin-left:36rem;display: flex;" class="">
                                    <span id="subs_bottomgli4" >0&euro;</span>
                            </td>
                     </tr>
                            <tr style=" margin-left:1em;display: flex;" class=" font-weight-bold">
                                <td class="">Produit</td>
                                <td style=" margin-left: 21em;"   class="">Prix</td>
                                <td style=" margin-left: 17rem;"  class="">Nombre</td>
                            </tr>

                            <?php for ($iii = 0; $iii <= 30; $iii++) {?>
                                <tr style=" margin-left: 1rem;display: flex;"  id="gli_4_recap_content_<?= $iii;?>"></tr>
                            <?php } ?>

                    
                        <!-- gli5 -->

                    <tr class="row gli_num_6 recap_commad_item_line " style=" margin-left: 1rem;display: flex; <?php if (isset($title_gli6->is_activ_glissiere) AND ($title_gli6->is_activ_glissiere != 0 )){ echo "display:flex;";} else{ echo "display:none;";} ?>">

                        <td class="" id="gli_6_recap_container">
                            <div class="recap_gli_title gli_6_title">
                                <?php if (isset($title_gli6->titre_glissiere) and $title_gli6->titre_glissiere != null) echo $title_gli6->titre_glissiere;?>
                            </div>
                        </td>
                            <td style=" margin-left:37rem;display: flex;" class="">
                                <span id="subs_bottomgli6">0&euro;</span>
                            </td>
                     </tr>
                            <tr style=" margin-left: 1rem;display:flex;" class="font-weight-bold">
                                <td  class="">Produit</td>
                                <td style=" margin-left: 21em;"  class="">Prix</td>
                                <td  style=" margin-left: 17rem;" class="">Nombre</td>
                            </tr>

                            <?php for ($iii = 0; $iii <= 30; $iii++) {?>

                                <tr style=" margin-left: 1rem;display: flex;" id="gli_6_recap_content_<?= $iii;?>"></tr>

                            <?php } ?>

                       
                 
                                <!-- gli6 -->

                    <tr style=" margin-left: 1rem;display: flex;<?php if (isset($title_gli7->is_activ_glissiere) AND ($title_gli7->is_activ_glissiere != 0)){ echo "display:flex;"; }else{echo "display:none;"; } ?>" >
                        <td class="" id="gli_7_recap_container">
                            <div class="recap_gli_title gli_7_title">
                                <?php if (isset($title_gli7->titre_glissiere) and $title_gli7->titre_glissiere != null) echo $title_gli7->titre_glissiere;?>
                            </div>
                        </td>

                        <td style=" margin-left:36rem;display: flex;" >
                            <span id="subs_bottomgli7">0&euro;</span>
                        </td>
                    </tr>

                    <tr style=" margin-left: 1rem;display:flex;" class="font-weight-bold">
                                <td >Produit</td>
                                <td style=" margin-left: 21em;">Prix</td>
                                <td style=" margin-left: 17rem;">Nombre</td>
                    </tr>
                         <?php for ($iii = 0; $iii <= 30; $iii++) {?>
                        <tr style=" margin-left: 1rem;display: flex;" id="gli_7_recap_content_<?= $iii;?>" ></tr>
                         <?php } ?>  
           
                       
               
             </div>

        </table>

        <!-- fin table -->

        <div  class="col-lg-6 offset-lg-3 mt-4 text-center col-sm-12" id="montant_total">

            <div class="row m-0">

                <div style="text-align: center; padding: 15px; margin: 10px;" class="col-lg-12 pt-2 d-flex back_blue col-sm-12">

                    <label   class="text_label_mont text-uppercase">Montant total ttc à régler</label>

                    <span class="egale">

                        <img src="<?php echo base_url('/assets/images/egale.png')?>">

                    </span>

                    <span id="all_totalised" class="w-50 back_blue p-2 prix_tot">0&euro;</span>

                </div>

            </div>

        </div>

    </div>

    <?php if (isset($datacommande->price_to_reach) AND $datacommande->price_to_reach !=null && isset($datacommande->price_to_win) AND $datacommande->price_to_win !=null && $datacommande->is_activ_prom == '1' ){ ?>

    <div class="col-sm-12">

        <div class="row pt-4 mt-3 pb-4 prom_row shadowed">

            <div class="col-lg-3 text-center col-sm-12">

                <img src="<?php echo base_url()?>assets/images/cible.png">

            </div>

            <div class="col-lg-9 col-sm-12">

                <div class="title_prom text-center">PROFITEZ DE NOTRE OFFRE PROMOTIONNELLE</div>

                <div class="row">

                    <div class="col-lg-6">

                        <div class="row">

                            <div class="col-lg-8 col-8 pl-0 text-prom" id="content_sm_remise">

                                Si vous dépassez un

                                achat d'un montant de

                            </div>

                            <div class="col-lg-4 col-4 pl-0 pt-2" id="content_sm_remise">

                                <input value="<?php echo $datacommande->price_to_reach; ?>€" class="w-100 mr-2 input_prom" type="text" disabled>

                            </div>

                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="row">

                            <div class="col-lg-8 col-8 text-prom">

                                Vous bénéficiez d'une

                                remise immédiate de

                            </div>

                            <div class="col-lg-4 col-4 pl-0 pt-2" id="content_sm_remise">

                                <input value="<?php echo $datacommande->price_to_win; ?>€" class="w-100 input_prom" type="text" disabled>

                            </div>

                        </div>

                    </div>

                </div>

                <input type="hidden" id="is_graced" value="0">

                <div class="row pt-4 pb-4">

                    <div id="text_warn" style="line-height:35px;font-size: 25px!important" class="col-lg-12 col-sm-12 text-center title_prom">

                        Désolé ! votre montant d'achat est inférieur,

                        vous ne pouvez pas en bénéficier !...

                    </div>

                </div>

                <div class="row d-none" id="is_bravo">

                    <div class="col-lg-6">

                        <div class="row">

                            <div class="col-lg-8 col-8 pl-0 pt-2 text-prom" id="content_sm_remise">

                                Remise TTC à déduire

                            </div>

                            <div class="col-lg-4 col-4 pl-0 pt-2" id="content_sm_remise">

                                <input value="<?php echo $datacommande->price_to_win; ?>€" class="w-100 mr-2 input_prom" type="text" disabled>

                            </div>

                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="row">

                            <div class="col-lg-8 col-8 text-prom" id="content_sm_remise">

                                Nouveau montant

                                total TTC à régler

                            </div>

                            <div class="col-lg-4 col-4 pl-0 pt-2" id="content_sm_remise">

                                <input id="rest_to_give" value="0" class="w-100 input_prom" type="text" disabled>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        </div>

    <?php } ?>

    <div class="col-lg-12 emp_contentss d-none">

    <?php if (isset($datacommande->is_activ_type_livr_grat) AND $datacommande->is_activ_type_livr_grat == '1'){ ?>

        <div class="d-none emp_contentss">

            <div class="col-lg-12 pt-4 pb-0 pl-0 pr-0">

                <div class="w-100 middle_title">

                    VENTE EN LIGNE A EMPORTER / RETRAIT ET PAIEMENT EN MAGASIN

                </div>

                <div class="w-100 pt-4">

                    <p class="text_head"><?php echo $datacommande->comment_emporter_txt ?? ""; ?></p>

                </div>

            </div>

            <div class="col-lg-12  pt-2 pb-0 pl-0 pr-0">

                <div class="w-100">

                    <span id="text_vent_emport_jour" class="text_head"></span>

                    <span id="text_vent_emport_heure" class="text_head"></span>

                </div>

            </div>

        </div>

        <script type="text/javascript">

            $(document).ready(function(){



                $('#jour_emporter').change(function(){

                    var jour = $('#jour_emporter').val();

                    var result = jour.split('-');

                    if(result[1] == '01'){var mois = 'janvier';}else if(result[1] == '02'){var mois = 'février';}else if(result[1] == '02'){var mois = 'février';}else if(result[1] == '03'){var mois = 'mars';}else if(result[1] == '04'){var mois = 'avril';}else if(result[1] == '05'){var mois = 'mai';}else if(result[1] == '06'){var mois = 'juin';}else if(result[1] == '07'){var mois = 'juillet';}else if(result[1] == '08'){var mois = 'août';}else if(result[1] == '09'){var mois = 'septembre';}else if(result[1] == '10'){var mois = 'octobre';}else if(result[1] == '11'){var mois = 'novembre';}else if(result[1] == '12'){var mois = 'décembre';}

                    $('#text_vent_emport_jour').html('Jour et heure souhaitée le <span id="id_jour_souhait">'+result[2]+' '+mois+' '+result[0]+'</span>')

                })

                $('#heure_enlevement').change(function(){

                    var heure = $('#heure_enlevement').val();

                    var heure_get = heure.split(':');

                    $('#text_vent_emport_heure').html(' à <span id="id_heure_souhait">'+heure_get[0]+' heure '+heure_get[1]+ ' minutes </span>')

                })





            })

        </script>

        <div class="pb-4">

<!--            <input type="hidden" id="type_paiment_cli" value="0">-->

            <div class="col-lg-12 cond pt-2">

                <div class="row">

                    <div class="col-lg-3 p-0 col-sm-12 text_head">Conditions de règlement</div>

                    <?php if ($datacommande->is_activ_cheque_enlev =="1" ||  $datacommande->is_activ_cheque_enlev =="1"){ ?>

                        <div class="col-lg-1 col-1">

                            <input id="type_norm_cheque" class="check_cond" type="checkbox">

                        </div>

                        <div class="col-lg-2 col-2 pl-0">

                            <span class="txt_cond text_head" >Chèque</span>

                        </div>

                    <?php } ?>

                    <?php if ($datacommande->is_activ_termin_banc_enlev =="1"  || $datacommande->is_activ_termin_banc_enlev =="1"){ ?>

                        <div class="col-lg-1 col-1">

                            <input id="type_norm_bank" class="check_cond" type="checkbox">

                        </div>

                        <div class="col-lg-2 pl-0 col-3">

                            <span class="txt_cond text_head">Carte bancaire</span>

                        </div>

                    <?php } ?>

                    <?php if ($datacommande->is_activ_espece_enlev =="1"  || $datacommande->is_activ_espece_enlev =="1"){ ?>

                        <div class="col-lg-1 col-1">

                            <input id="type_norm_espace" class="check_cond" type="checkbox">

                        </div>

                        <div class="col-lg-2 pl-0 col-3">

                            <span class="txt_cond text_head">Espèces</span>

                        </div>

                    <?php } ?>

                </div>

            </div>

        </div>

    <?php } ?>

    </div>

    <?php $this->load->view('front_menus/includes_commande/livraisons_commande') ?>

    <?php $this->load->view('front_menus/includes_commande/differe_commande') ?>

    <?php $this->load->view('front_menus/includes_commande/paypal_commande') ?>

</div>

<?php } ?>

</div>

<div class="container m-auto">

    <div class="row">

        <div class="col-lg-6 d-block m-auto text-center pt-4" id="commande_error"></div>

    </div>

    <div class="row mb-5" id="row_sm">

        <div class="col-lg-4 offset-lg-4 pl-1 pt-3 text-center col-sm-12">

            <button class="w-100 btn btn_after_input m-auto" onclick="send_command()" id="validate_command" >Validation de la commande</button>

        </div>

    </div>

</div>

<script src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>

<input id="conditions_terminal_value" type="hidden" value="0">

<input id="conditions_generale_value" type="hidden" value="0">

<input id="conditions_paypal_value" type="hidden" value="0">

<script>

    $('#conditions_terminal').change(function(){

        if($('#conditions_terminal_value').val() == 0){

            $('#conditions_terminal_value').val('1');

        }else{

            $('#conditions_terminal_value').val('0');

        }

    });

    $('#conditions_generale').change(function(){

        if($('#conditions_generale_value').val() == 0){

            $('#conditions_generale_value').val('1');

        }else{

            $('#conditions_generale_value').val('0');

        }

    });

    $('#conditions_paypal').change(function(){

        if($('#conditions_paypal_value').val() == 0){

            $('#conditions_paypal_value').val('1');

        }else{

            $('#conditions_paypal_value').val('0');

        }

    });

</script>

<style>

    .arrow_up{

        content:"\f077"!important;

        font-family: FontAwesome!important;

        color:white;

        font-size: 18px;

        position: absolute;

        top: 10px;

        left: 0;

    }

    .round_arrow{

        padding-top: 40px;

        padding-bottom: 40px;

        padding-left: 50px;

        padding-right: 50px;

        background:rgba(232, 14, 174, 1);

        border-radius: 60px;

        position: relative;

    }

    .round_arrow::after{

        content: "\f106"!important;

        font-family: FontAwesome!important;

        color: white;

        font-size: 60px;

        position: absolute;

        top: 0px;

        left: 31px;

    }

    .modal{

        width: 100%!important;

    }

    .modal-dialog,.modal-content,.modal-header,.modal-body{

        width: 100%!important;

    }

    .modal.fade.show{

        width: 100%!important;

    }

    .recap_commad_item_line {

       /* border-bottom: solid #ccc;

        padding-top: 15px;*/

    }

    .recap_gli_title {

        font-size: 20px;

        font-weight: bold;

        text-transform: uppercase;

    }

</style>