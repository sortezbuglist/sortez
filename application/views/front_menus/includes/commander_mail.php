
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="padding: 10px 0 30px 0;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="75%"
                           style="border: 1px solid #cccccc; border-collapse: collapse;">
                           <tr>
                                <td style="text-align: center; padding: 40px 15px 10px 15px; font-family: Arial, sans-serif;">
                                    <h2>MAGAZINE SORTEZ<br> COURRIER DE RECEPTION DE COMMANDE « MENU DIGITAL »</h2>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center; padding: 0px 15px 0px 15px; font-family: Arial, sans-serif;">
                                    Commande adressée le <?php date_default_timezone_set('Europe/Paris'); ?>
                                    <span class="font-weight-bold"><?= convert_Sqldate_to_Frenchdate(date("Y-m-d"))." à ".strftime('%H:%M:%S');?></span>
                                </td>
                            </tr>
                            <tr>
                                <td align="center"
                                    style="text-align: justify; padding: 20px 15px 15px 15px; color: #000000; font-size: 14px; font-family: Arial, sans-serif;">
                                    Une commande vient d' être envoyé ci-dessous les détails:
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 20px; color: #153643; font-family: Arial, sans-serif; font-size: 14px; line-height: 20px;">
                                   <?php echo html_entity_decode($contenue); ?>

                                
                                </td>
                            </tr>
                        
                            <tr>
                                <td style="padding: 20px; color: #153643; font-family: Arial, sans-serif; line-height: 20px; text-align: center;">
                                        <span style="font-size: 14px;"> Cette commande a été conçue avec le Market Place du Magazine Sortez - www.magazine-sortez.org<br>
                                        Priviconcept SAS au capital de 1000€,  427, Chemin de Vosgelade, Le Mas Raoum,  06140 Vence</span><br>
                                        <span style="font-size: 12px;">Siret : 820 043 693 00010 - Code NAF : 6201</span>
                                </td>
                            </tr>
                        
                    </table>
                </td>
            </tr>
        </table>