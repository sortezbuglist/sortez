<div class="container">
    <div class="row">
        <div class="container">
            <div class="row rrow">
                <div class="col-2 offset-1">
                    <button type="button" class="btn btn_menu_content">Retour</button>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn_menu_content">Page Accueil</button>
                </div>
                <div class="col-2">
                    <a href="<?php echo site_url($oInfoCommercant->nom_url.'/commandes_commercants') ?>" class="btn_menu_content">
                        <button type="button" class="btn btn_menu_content">
                            Je commande
                        </button>
                    </a>
                </div>
                <div class="col-2">
                    <a href="<?php echo site_url($oInfoCommercant->nom_url.'/annonces_commercants') ?>" class="btn_menu_content">
                        <button type="button" class="btn btn_menu_content">
                            Ma boutique
                        </button>
                    </a>
                </div>
                <div class="col-2">
                    <button type="button" class="btn btn_menu_content">Deal & fidélté</button>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .btn_menu_content{
        background: rgba(52, 83, 162, 1);
        border-radius: 20px;
        color: #ffffff;
        width: 100%;
    }
    .btn_menu_content:hover{
        background: rgba(232, 14, 174, 1);
        color: #ffffff;
        text-decoration: none;
    }
</style>
<div class="cont_all">
    <div class="row">
        <?php if(isset($oInfoCommercant->adresse_localisation) and $oInfoCommercant->adresse_localisation != null and isset($oInfoCommercant->codepostal_localisation) and $oInfoCommercant->codepostal_localisation !=null){ ?>
            <div class="col-lgs-2 back_local ml-2">
                <div class="row  text-center">
                    <div class="col-12">
                        <!--                    <p class="titre_com">Localisation</p>-->
                    </div>
                    <div class="col-12 pt-3">
                        <a title="Localisation" data-fancybox data-type="iframe" data-src="https://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $oInfoCommercant->adresse_localisation.", ".$oInfoCommercant->codepostal_localisation;?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $oInfoCommercant->adresse_localisation.", ".$oInfoCommercant->codepostal_localisation;?>&amp;t=m&amp;vpsrc=0&amp;output=embed" id="id_localisation_agenda_1100" class="fancybox_localisation_agenda_1100 various fancybox.iframe">
                            <img src="<?php echo base_url('/assets/images/localisation.webp') ?> " style="border-radius: 60px">
                        </a>
                    </div>
                    <div class="col-12">
                        <p class="titre_com">Visualisez notre  plan !</p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if(isset($oInfoCommercant->Video) and $oInfoCommercant->Video != null){ ?>
            <div class="col-lgs-2 back_video ml-2">
                <div class="row  text-center">
                    <div class="col-12">
                        <!--                    <p class="titre_com">Vidéo</p>-->
                    </div>
                    <div class="col-12 pt-3">
                        <a data-fancybox="" data-animation-duration="500" data-src="<?php echo $oInfoCommercant->Video;?>"  title="Video youtube">
                            <img src="<?php echo base_url('/assets/images/video.webp') ?> " style="border-radius: 60px">
                        </a>
                    </div>
                    <div class="col-12">
                        <p class="titre_com">Visualisez !</p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php if(isset($link_doc_pdf_partner_to_show) and $link_doc_pdf_partner_to_show != "#"){ ?>
            <div class="col-lgs-2 back_pdf ml-2">
                <div class="row  text-center">
                    <div class="col-12">
                        <!--                    <p class="titre_com">Plaquette PDF</p>-->
                    </div>
                    <div class="col-12 pt-3">
                        <a href="<?php echo $link_doc_pdf_partner_to_show;?>" <?php if($link_doc_pdf_partner_to_show != "#"){ ?>target="_blank" title="<?php if (isset($oInfoCommercant->titre_Pdf) && $oInfoCommercant->titre_Pdf != "") echo $oInfoCommercant->titre_Pdf; else echo "Plaquette commerciale PDF";?>"<?php }else{?>title="Pas de fichier pdf pour <?php echo $oInfoCommercant->NomSociete; ?>"<?php } ?>>
                            <img src="<?php echo base_url('/assets/images/pdf.webp') ?> " style="border-radius: 60px">
                        </a>
                    </div>
                    <div class="col-12">
                        <p class="titre_com">Téléchargez !</p>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-lgs-2 back_contact ml-2">
            <div class="row  text-center">
                <div class="col-12">
                    <!--                    <p class="titre_com">Nous contacter</p>-->
                </div>
                <div class="col-12 pt-3">
                    <a data-fancybox="" data-animation-duration="500" data-src="#divContactPartnerForm" href="javascript:;" id="IdCsontactPartnerForm" title="Contactez-nous!">
                        <img src="<?php echo base_url('/assets/images/mail.webp') ?> " style="border-radius: 60px">
                    </a>
                </div>
                <div class="col-12">
                    <p class="titre_com">Adressez un mail !</p>
                </div>
            </div>
        </div>
        <div class="col-lgs-2 back_carte ml-2">
            <div class="row  text-center">
                <div class="col-12">
                    <!--                    <p class="titre_com">Carte vivresaville</p>-->
                </div>
                <div class="col-12 pt-3">
                    <a href="<?php echo site_url("/front/fidelity/menuconsommateurs") ?>" target="_blank" title="Accés à votre compte!">
                        <img src="<?php echo base_url('/assets/images/carte1.webp') ?> " style="border-radius: 60px">
                    </a>
                </div>
                <div class="col-12">
                    <p class="titre_com">Accès à votre compte</p>
                </div>
            </div>
        </div>
        <div class="col-lgs-2 back_soutenons ml-2">
            <div class="row text-center">
                <div class="col-12">
                    <!--                    <p class="titre_com">soutenonslecommercelocal.fr</p>-->
                </div>
                <div class="col-12 pt-3">
                    <a href="https://www.soutenonslecommercelocal.fr/" target="_blank" title="soutenonslecommercelocal.fr">
                        <img src="<?php echo base_url('/assets/images/1.webp') ?> " style="border-radius: 60px">
                    </a>
                </div>
                <div class="col-12">
                    <p class="titre_com">Accès au site</p>
                </div>
            </div>
        </div>
    </div>
</div>
