<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
?>
<div class="row pt-2" style="margin-top: 100px!important;">
    <div class="col-12 background_footer_comms pt-5 pb-5">
        <div class="col-12">
            <p class="text_label2_1"><?php echo $oInfoCommercant->NomSociete;?></p>
            <p class="text_label2_1"><?php echo $oInfoCommercant->adresse_localisation;?></p>
            <p class="text_label2_1">Tél : <?php echo $oInfoCommercant->TelDirect;?> - Mobile : <?php echo $oInfoCommercant->TelMobile;?></p>
            <p class="text_label2_1">Courriel : <?php echo $oInfoCommercant->Email;?> - Site web : <?php echo $oInfoCommercant->SiteWeb;?></p>
        </div>
        <div class="col-12">
            <p class="text_label2_2 pt-3">Ce site est hébergé par la SAS priviconcept sis au 535 route des lucioles - Sophia Antipolis, 06560 Valbonne</p>

            <p class="text_label2_2">Le contenu (photos et rédactionels) est sous la responsabilité du partenaire</p>

            <p class="text_label2_2">Cette page et ces données sont référencées sur les sites suivants : "magazine-sortez.org", "vivresaville.fr", "soutenonslecommercelocal.fr"</p>
        </div>
    </div>
</div>
<style>
    .background_footer_comms{
        background: <?php //if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $group_id_commercant_user == 5){echo $oInfoCommercant->bandeau_color;}elseif($group_id_commercant_user == 4){echo 'black';} ?>;
        background: #3453a2;
    }
    .text_label2_1{
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 18px;
        align-self: center;
        text-align: center;
        color: #ffffff;
        letter-spacing: 0.05em;
        margin-bottom: 0px!important;
    }
    #footer_section_copyright{
        margin-top: 0px!important;
    }
    .text_label2_2{
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 14px;
        align-self: center;
        text-align: center;
        color: #ffffff;
        letter-spacing: 0.05em;
        margin-bottom: 0px!important;
    }
</style>