<div class="d-block d-mg-none d-lg-none d-xl-none">
    <div class="col-lg-12 text-center padding0 mt-2 mb-2">
        <?php if (isset($oInfoCommercant->adresse_localisation) and $oInfoCommercant->adresse_localisation != null and isset($oInfoCommercant->codepostal_localisation) and $oInfoCommercant->codepostal_localisation != null) { ?>
            <a title="Localisation"
               data-fancybox
               data-type="iframe"
               data-src="https://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo $oInfoCommercant->adresse_localisation . ", " . $oInfoCommercant->codepostal_localisation; ?>&amp;aq=0&amp;ie=UTF8&amp;hq=&amp;hnear=<?php echo $oInfoCommercant->adresse_localisation . ", " . $oInfoCommercant->codepostal_localisation; ?>&amp;t=m&amp;vpsrc=0&amp;output=embed"
               id="id_localisation_agenda_1100_mobile"
               class="fancybox_localisation_agenda_1100 various fancybox.iframe"
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 plan">
                    <div class="col-4">
                        <img class="img-fluid w-75"
                             src="<?php echo base_url('/assets/soutenons/plan_mobile.png') ?> "
                             style="margin-left: 5px;">
                    </div>
                    <div class="col-8 text-center pt-4">
                        Visualisez
                        votre
                        plan
                        !
                    </div>
                </div>
            </a>
        <?php } ?>
        <?php if (isset($oInfoCommercant->Video) and $oInfoCommercant->Video != null) { ?>
            <a data-fancybox=""
               data-animation-duration="500"
               data-src="<?php echo $oInfoCommercant->Video; ?>"
               title="Video youtube"
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 video">

                    <div class="col-4">
                        <img class="img-fluid w-75"
                             src="<?php echo base_url('/assets/soutenons/video_mobile.png') ?> "
                             style="margin-left: 5px;">
                    </div>
                    <div class="col-8 text-center pt-4">
                        Visualisez
                        !
                    </div>

                </div>
            </a>
        <?php } ?>
        <?php if (isset($link_doc_pdf_partner_to_show) and $link_doc_pdf_partner_to_show != "#") { ?>
            <a href="<?php echo $link_doc_pdf_partner_to_show; ?>"
               <?php if ($link_doc_pdf_partner_to_show != "#"){ ?>target="_blank"
               title="<?php if (isset($oInfoCommercant->titre_Pdf) && $oInfoCommercant->titre_Pdf != "") echo $oInfoCommercant->titre_Pdf; else echo "Plaquette commerciale PDF"; ?>"
               <?php }else{ ?>title="Pas de fichier pdf pour <?php echo $oInfoCommercant->NomSociete; ?>"<?php } ?>
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 pdf">

                    <div class="col-4">
                        <img class="img-fluid w-75"
                             src="<?php echo base_url('/assets/soutenons/pdf_mobile.png') ?> "
                             style="margin-left: 5px;">
                    </div>
                    <div class="col-8 text-center pt-4">
                        Téléchargez
                        !
                    </div>

                </div>
            </a>
        <?php } ?>
        <a data-fancybox=""
           data-animation-duration="500"
           data-src="#divContactPartnerForm"
           href="javascript:;"
           id="IdCsontactPartnerForm_mobile"
           title="Contactez-nous!"
           style="text-decoration: unset">
            <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 mail_mobile">

                <div class="col-4">
                    <img class="img-fluid w-75"
                         src="<?php echo base_url('/assets/soutenons/mail_mobile.png') ?> "
                         style="margin-left: 5px;">
                </div>
                <div class="col-8 text-center pt-4">
                    Adressez
                    un
                    mail
                    !
                </div>

            </div>
        </a>
        <?php if (isset($oInfoCommercant->SiteWeb) && $oInfoCommercant->SiteWeb != "") { ?>
            <?php
            $file = $oInfoCommercant->SiteWeb;
            $file_headers = @get_headers($file);
            if (!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $exists = 0;
            } else {
                $exists = 1;
            }
            if ($exists == 1) {
                ?>
                <a href="<?php echo $oInfoCommercant->SiteWeb; ?>"
                   target="_blank"
                   title="Accés à notre site!"
                   style="text-decoration: unset">
                    <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 bloc_new">

                        <div class="col-4">
                            <img class="img-fluid w-75"
                                 src="<?php echo base_url('/assets/soutenons/bloc_new_mobile.png') ?> "
                                 style="margin-left: 5px;">
                        </div>
                        <div class="col-8 text-center pt-4">
                            Accès
                            à
                            notre
                            site
                            web
                            !
                        </div>

                    </div>
                </a>
            <?php } ?>
        <?php } ?>
        <a href="https://www.soutenonslecommercelocal.fr/"
           target="_blank"
           title="soutenonslecommercelocal.fr"
           style="text-decoration: unset">
            <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 soutenons_mobile">

                <div class="col-4">
                    <img class="img-fluid w-75"
                         src="<?php echo base_url('/assets/soutenons/soutenons_mobile.png') ?> "
                         style="margin-left: 5px;">
                </div>
                <div class="col-8 text-center pt-4">
                    Accès
                    au
                    site
                </div>

            </div>
        </a>

        <?php
        $thisss = get_instance();
        $thisss->load->model('Mdl_menu');
        $datamenu = $thisss->Mdl_menu->get_menu_data_by_idcom($oInfoCommercant->IdCommercant);
        if (!empty($datamenu) AND $datamenu->is_activ_default_btn == 1 && $datamenu->image_menu_gen ==null){ ?>
            <a href="<?php echo site_url().$oInfoCommercant->nom_url;?>/menus_commercants"
               title="soutenonslecommercelocal.fr"
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 soutenons_mobile">
                    <div class="col-4">
                        <img class="image_rounded w-75" src="<?php echo base_url("assets/image/default_menu_img.jpg"); ?>">
                    </div>
                    <div class="col-8 text-center pt-4">
                        <?php if (isset($datamenu->menu_value) AND $datamenu->menu_value != null ){echo $datamenu->menu_value;}else{echo "Nos cartes & menus";} ?>
                    </div>
                </div>
            </a>
        <?php } ?>

        <?php if (!empty($datamenu) AND $datamenu->is_activ_default_btn != 1 && $datamenu->image_menu_gen !=null && is_file("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/menu_icon/".$datamenu->image_menu_gen)){ ?>
            <a href="<?php echo site_url().$oInfoCommercant->nom_url;?>/menus_commercants"
               title="soutenonslecommercelocal.fr"
               style="text-decoration: unset">
                <div class="row mobile_img_square_menu mt-3 pt-3 pb-3 soutenons_mobile">
                    <div class="col-4">
                        <img class="image_rounded w-75" src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/menu_icon/".$datamenu->image_menu_gen); ?>">
                    </div>
                    <div class="col-8 text-center pt-4">
                        <?php if (isset($datamenu->menu_value) AND $datamenu->menu_value != null ){echo $datamenu->menu_value;}else{echo "Nos cartes & menus";} ?>
                    </div>
                </div>
            </a>
        <?php } ?>

    </div>
</div>
<style type="text/css">
    .image_rounded{
        border-radius: 45px;
    }
    .mobile_img_square_menu {
        color: #ffffff;
        font-family: Futura-LT-Book, Sans-Serif;
        font-size: 18px;
    }

    .mobile_img_square_menu.plan {
        background-color: #164F18;
    }

    .mobile_img_square_menu.video {
        background-color: #FF6161;
    }

    .mobile_img_square_menu.mail_mobile {
        background-color: #75CBA8;
    }

    .mobile_img_square_menu.bloc_new {
        background-color: #3453A2;
    }
    .mobile_img_square_menu.soutenons_mobile {
        background-color: #E80EAE;
    }
</style>