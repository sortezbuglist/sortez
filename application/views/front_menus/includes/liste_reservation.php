<?php
$image_path="application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/";
?>
<div class="row">
    <div class="col-lg-12 text-center">
        <p style="color: black;font-size: 15px">
            Vous trouverez ci-dessous le détail de nos plats du jour d’aujourd’hui et à venir<br>

            Les réservations doivent nous être adressées et validées avant la fin de chaque terme.
        </p>
    </div>
</div>
<div class="row p-2">
    <?php foreach ($reservation as $res_list) {?>
        <?php if (isset($res_list->date_debut_plat) AND isset($res_list->date_fin_plat)){
            $date_jour_fin=$this->Mdl_plat_du_jour->get_jour($res_list->date_fin_plat);
           $date_mois_fin=$this->Mdl_plat_du_jour->get_mois($res_list->date_fin_plat);
           $date_annee_fin=$this->Mdl_plat_du_jour->get_annee($res_list->date_fin_plat);

        }

            ?>
    <div class="col-card-sortez p-2">
        <form id="res_client<?php echo $res_list->id;?>" method="post" action="<?php echo site_url('front/commercant/valid_reservation_client')?>">
        <div class="p-3" style="box-shadow: 0px 2px 2px 2px rgba(0,0,0,0.1)">
        <div class="text-center mt-0" style="margin: auto;background-image: url('<?php echo base_url('assets/img/cadre_noir_res.png')?>');background-repeat: no-repeat;background-size: 100%;font-size: 16px;color: white;line-height: 4"><?php if ($res_list->date_debut_plat == date('Y-m-d')){ echo 'AUJOURD’HUI';}else{echo convert_Sqldate_to_Frenchdate($res_list->date_debut_plat);}  ?></div>
        <div class="pt-3 pb-3" style="overflow: hidden;width: 100%;height: 220px;">
            <a href="<?php  echo site_url().$oInfoCommercant->nom_url.'/details_plat_commercants/'.$res_list->id ?>"><img style="width: 100%;height: 100%;overflow: hidden;" src="<?php if (isset($res_list->photo)/* AND is_file($image_path.$res_list->photo)*/){echo base_url().$image_path.$res_list->photo;}else{echo base_url('assets/img/plat_symbole.jpg');} ?>"></a>
        </div>
            <div class="pt-3 mb-3 text-center" style="background-image: url('<?php echo base_url('assets/img/bg_price.png')?>');background-repeat: no-repeat;background-size: 100% 100%;height: 150px">
                <p class="pl-3 pr-3" style="height: 50px;color: white"><?php echo $res_list->description_plat; ?>
                </p>
                <p class="pb-3" style="font-size: 25px;color: white"><?php echo $res_list->prix_plat;?></p>
            </div>
        <div class="pl-2">
        <div class="row" id="countdown<?php echo $res_list->id;?>">
        <div style="background-color: black;color: white;font-size: 8px" class="m-1 col-2 text-center p-0"> JOURS&nbsp;<br>
        <span style="font-size: 15px" id="jour_show<?php echo $res_list->id;?>" class="pt-1">00</span>
        </div>
        <div style="background-color: black;color: white;font-size: 8px" class=" m-1 col-2 text-center  p-0"> HEURES<br>
            <span  style="font-size: 15px" id="heure_show<?php echo $res_list->id;?>" class="pt-1">01</span>
        </div>
        <div style="background-color: black;color: white;font-size: 8px" class="m-1 col-2 text-center  p-0"> MINUTES<br>
            <span style="font-size: 15px" id="minute_show<?php echo $res_list->id;?>" class="pt-1">18</span>
        </div>
            <div class="col-1 p-0"></div>
        <div style="background-color: black;color: white;font-size: 8px" class="m-1  col-3 text-center  p-0">SOLDE<br>
            <span style="font-size: 15px" class="pt-1"><?php echo $res_list->nbre_plat_propose ?></span>
        </div>
        </div>
        </div>
        <div class="row pt-3 pb-3">
            <div style="color: black;font-size: 14px" class="col-lg-8 col-sm-7 col-8">Déjeuner</div>
            <div class="select_plat col-lg-4 col-sm-4 col-4">
                <select id="heure_res<?php echo $res_list->id;?>" name="reservation[heure_reservation]" style="width: 100%">
                    <option value="11:00">11:00</option>
                    <option value="11:30">11:30</option>
                    <option value="12:00">12:00</option>
                    <option value="12:30">12:30</option>
                    <option value="13:00">13:00</option>
                    <option value="13:30">13:30</option>
                    <option value="14:00">14:00</option>
                    <option value="14:30">14:30</option>
                    <option value="15:00">15:00</option>
                </select>
            </div>
        </div>
        <div class="row pb-3">
            <div style="color: black;font-size: 14px" class="col-lg-8 col-sm-7 col-8">Nombre de personnes</div>
            <div class="select_plat col-lg-4 col-sm-4 col-4">
                <select name="reservation[nbre_pers_reserved]" id="nbre_pers<?php echo $res_list->id;?>" style="width: 100%">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
        </div>
        <div class="row pb-3">
            <div style="color: black;font-size: 14px" class="col-lg-8 col-sm-7 col-8">Plat(s) du jour</div>
            <div class="col-lg-4 col-sm-4 col-4 select_plat">
                <select name="reservation[nbre_platDuJour]" id="plat_jour<?php echo $res_list->id;?>" style="width: 100%">
                    <option value="0">non</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
        </div>
<!--        <div class="row pb-0 pt-2">-->
<!--            <div class="text-center col-12" style="color: black;font-size: 14px">-->
<!--                Intégrez le numéro de votre carte-->
<!--            </div>-->
<!--        </div>-->
<!--            <div class="row pb-2 pt-2">-->
<!--                <div id="status--><?php //echo $res_list->id;?><!--" class="col-lg-12 text-center  col-12" style="color: white;font-size: 14px">-->
<!--                </div>-->
<!--            </div>-->
<!--        <div class="row pb-3">-->
<!--<!--            <div class="col-12 text-center" style="">-->
<!--<!--              <input id="num_carte--><?php ////echo $res_list->id;?><!--<!--" type="number" name="reservation[num_carte]">-->
<!--<!--            </div>-->
<!--        </div>-->
        <div class="row pb-4 pt-2">
            <div class="col-12 text-center  col-12" style="">
                <div style="color: white" id="btnres<?php echo $res_list->id ?>" class="boutoninsc">JE R&Eacute;SERVE</div>
            </div>
        </div>
    </div>
        </form>
    </div>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.countdown.js')?>"></script>
        <script type="text/javascript">
            jQuery("#btnres<?php echo $res_list->id;?>").click(function () {
                var nbre_pers_reserved=$("#nbre_pers<?php echo $res_list->id;?>").val();
                var nbre_platDuJour=$("#plat_jour<?php echo $res_list->id;?>").val();
                var heure_reservation= $("#heure_res<?php echo $res_list->id;?>").val();
                // alert(nbre_pers_reserved);
                // alert(nbre_platDuJour);
                window.location.href = "<?php echo base_url()."front_menuscommercant/reservation_plat/".$res_list->id; ?>"+"/"+nbre_pers_reserved+"/"+nbre_platDuJour+"/"+heure_reservation;
            });

            //jQuery("#btnres<?php //echo $res_list->id;?>//").click(function () {
            //    var idplat="<?php //echo $res_list->id;?>//";
            //    var IdCommercant="<?php //echo $res_list->IdCommercant;?>//";
            //    var heure_reservation= $("#heure_res<?php //echo $res_list->id;?>//").val();
            //    var nbre_pers_reserved=$("#nbre_pers<?php //echo $res_list->id;?>//").val();
            //    var nbre_platDuJour=$("#plat_jour<?php //echo $res_list->id;?>//").val();
            //    var num_carte=$("#num_carte<?php //echo $res_list->id;?>//").val();
            //    if (confirm('Voulez-vous reserver ce plat?')) {
            //        if ($('#num_carte<?php //echo $res_list->id;?>//').val() !=null){
            //
            //            jQuery.ajax({
            //                url: '<?php //echo site_url('front/commercant/valid_reservation_client')?>//',
            //                type: 'POST',
            //                data:'IdPlat='+idplat+"&IdCommercant="+IdCommercant+"&heure_reservation="+heure_reservation+"&nbre_pers_reserved="+nbre_pers_reserved+"&nbre_platDuJour="+nbre_platDuJour+"&num_carte="+num_carte,
            //                async: true,
            //                success: function (data) {
            //                    if (data=="Plat reservé") {
            //                        alert(data);
            //                    $("#status"+idplat).html('<span style="color: green;font-weight: bold;font-siz:18px">Reservé</span>');
            //                    }else{
            //                        alert(data);
            //                    }
            //                }
            //            });
            //
            //        }
            //    }
            //
            //});
            //jQuery(document).ready(function () {
            //$(function(){
            //    var jour_show = $('#jour_show<?php //echo $res_list->id;?>//'),
            //        heure_show = $('#heure_show<?php //echo $res_list->id;?>//'),
            //        minute_show = $('#minute_show<?php //echo $res_list->id;?>//'),
            //        ts = new Date(<?php //echo intval($date_annee_fin); ?>//,<?php //echo intval($date_mois_fin)-1?>//,<?php //echo intval($date_jour_fin)?>//),
            //        idplat='<?php //echo $res_list->id;?>//';
            //        //alert(ts);
            //    if((new Date()) > ts){
            //        jQuery.ajax({
            //            url: '<?php //echo site_url('front/commercant/desactivate_plat')?>//',
            //            type: 'POST',
            //            data:'IdPlat='+idplat,
            //            async: true,
            //            success: function (data) {
            //                if (data === "ok") {
            //                    $("#status"+idplat).html('<span style="color: red;font-weight: bold;font-size:18px">Expiré</span>');
            //                }
            //            }
            //        });
            //    }
            //
            //    $('#countdown<?php //echo $res_list->id;?>//').countdown({
            //        timestamp	: ts,
            //        callback	: function(days, hours, minutes){
            //
            //            if (days <= 0 && hours <= 0 && minutes <= 0 ){
            //                jQuery.ajax({
            //                    url: '<?php //echo site_url('front/commercant/desactivate_plat')?>//',
            //                    type: 'POST',
            //                    data:'IdPlat='+idplat,
            //                    async: true,
            //                    success: function (data) {
            //                        if (data === "ok") {
            //                            $("#status"+idplat).html('<span style="color: red;font-weight: bold;font-siz:18px">Expiré</span>');
            //                        }
            //                    }
            //                });
            //            } else{
            //                jour_show.html(days);
            //                heure_show.html(hours);
            //                minute_show.html(minutes);
            //            }
            //        }
            //    });
            //});
            //});

        </script>
    <?php }?>
</div>
<style type="text/css">
    .position{
        display: none;
    }
    @media (max-width: 479px) {
        .select_plat  {
            padding-left: 8px!important;
            padding-right: 20px!important;
        }
    }
    @media (min-width: 768px) {
        .select_plat  {
            padding-left: 8px!important;
            padding-right: 18px!important;
        }
    }

</style>