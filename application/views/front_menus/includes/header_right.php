<div class="row">
    <div class="container_wix text-center">
        <div class="row pt-4 d-lg-flex">

            <div class="col-lg-3 col-sm-12 col-md-3 col-md-3 pt-1" >
                <a class="menus_2 btn btn-lg" href="https://www.soutenonslecommercelocal.fr/contact">Nous contacter par mail</a>
            </div>

            <div class="col-lg-4 col-sm-12 col-md-3 col-md-3 pt-1" >
                <a class="menus_2 btn btn-lg" href="https://www.soutenonslecommercelocal.fr/newsletter">Inscription à notre newsletter</a>
            </div>

            <div class="col-lg-3 col-sm-12 col-md-3 col-md-3 pt-1" >
                <a class="menus_2 btn btn-lg" href="<?php echo site_url('auth/login') ?>">Accés à mon compte</a>
            </div>

            <div class="col-lg-2 col-sm-12 col-md-3 col-md-3 pr-0 pt-1" >
                <a class="menus_2 btn btn-lg webmaster" href="<?php echo site_url('auth/login') ?>">Webmaster login</a>
            </div>

        </div>
        <div class="row pt-4 d-lg-flex mb-4">
            <div class="col-lg-3 mt-4">
                <img src="<?php echo base_url('/assets/soutenons/logo_sortez.png') ?>">
                <img src="<?php echo base_url('/assets/soutenons/logo_left.png') ?>" style="margin-top: 57px;">
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-lg-12">
                        <img src="<?php echo base_url('/assets/soutenons/logo_soutenons.png') ?>">
                    </div>
                    <div class="col-lg-12">
                        <span class="text_soutenons_1">La vente à emporter & la livraison à domicile d'un simple clic !...</span>
                    </div>
                    <div class="col-lg-12">
                        <p class="text_soutenons_2 pt-3 pb-2" style="color:#605e5e;">"soutenonslecommercelocal.fr " est une réalisation du magazine Sortez !</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <img src="<?php echo base_url('/assets/soutenons/logo_vivresaville.png') ?>">
                <img src="<?php echo base_url('/assets/soutenons/logo_right.png') ?>">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function down(){
            document.getElementById("sous_menu_2").style.display("block");
    }
</script>