<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#head").click(function () {

        if (jQuery("#livreorcontent")[0] && $('#livreorcontent').hasClass("d-none")) {

            if (jQuery("#livreorcontent")[0]) jQuery("#livreorcontent").removeClass('d-none');
            if (jQuery("#livreorcontent")[0]) jQuery("#livreorcontent").addClass('d-flex');

            if (jQuery("#livreorcontent")[0]) jQuery("#livreorcontent").removeClass('d-none');
            if (jQuery("#livreorcontent")[0]) jQuery("#livreorcontent").addClass('d-flex');

            if (jQuery("#autoslide_off_5")[0]) jQuery("#autoslide_off_5").removeClass('d-none');
            if (jQuery("#autoslide_off_5")[0]) jQuery("#autoslide_off_5").addClass('d-flex');

            if ($('#livreorcontent').hasClass("padded")==false){

                jQuery("#livreorcontent").addClass('padded');
                jQuery("#livreorcontent").css('height',jQuery("#autoslide_off_5").height()+40);
                jQuery("#livreorcontent").css('height',jQuery("#autoslide_off_5").height()+40);

                jQuery("#livreorcontent").css('height',jQuery("#autoslide_off_5").css('height'));
                jQuery("#livreorcontent").css('height',jQuery("#autoslide_off_5").css('height'));

            }

        } else if (jQuery("#livreorcontent")[0] && $('#livreorcontent').hasClass("d-flex")) {

            if (jQuery("#livreorcontent")[0]) jQuery("#livreorcontent").removeClass('d-flex');
            if (jQuery("#livreorcontent")[0]) jQuery("#livreorcontent").addClass('d-none');

            if (jQuery("#livreorcontent")[0]) jQuery("#livreorcontent").removeClass('d-flex');
            if (jQuery("#livreorcontent")[0]) jQuery("#livreorcontent").addClass('d-none');

            if (jQuery("#autoslide_off_5")[0]) jQuery("#autoslide_off_5").removeClass('d-flex');
            if (jQuery("#autoslide_off_5")[0]) jQuery("#autoslide_off_5").addClass('d-none');
        }
        // alert('lalalalala');
    });
        var irubId ='<?php echo $oInfoCommercant->IdCommercant?>';
        jQuery.get(
        '<?php echo site_url("livreor/voir"); ?>' + '/' + irubId,
        function (zReponse) {
            // alert (zReponse) ;
            jQuery('#trReponseRub').html("");
            jQuery('#trReponseRub').html(zReponse);
        });
});
function fillstar(Obj,valeur){
            
            var stars=document.getElementsByName(Obj.name);
            document.getElementById('vote').value = valeur;
             
            for(i=0;i<stars.length;i++){
             
                if (i<=Obj.id){stars[i].src="<?php echo Base_Url('assets/images/fullstars.png')?>"}
                else{stars[i].src="<?php echo Base_Url('assets/images/emptystars.png')?>"}
                }
            }
    function enregistrer(){

            let Id_commercant=$('#Id_commercant').val();
            let Nom=$('#Nom').val();
            let mail=$('#mail').val();
            let mobile=$('#tel').val();
            let message=$('#message').val();
            let date=$('#date').val();
            let vote=$('#vote').val();
            let data="Id_commercant="+Id_commercant+"&Nom="+Nom+"&mail="+mail+"&mobile="+mobile+"&message="+message+"&date="+date+"&vote="+vote;
            if (Nom !="" && mail!="" && mobile!=""){
                if(validateEmail(mail)==true){
                   jQuery.ajax({
                        url: "<?php echo site_url('livreor/save');?>",
                        dataType: 'text',
                        type: 'POST',
                        data: data,
                        success: function (data) {
                           if (data=='ok'){
                               alert('Message réussie!')
                                $('#Nom').val('');
                                $('#mail').val('');
                                $('#tel').val('');
                                $('#message').val('');
                                var stars=document.getElementsByName("vote");
                                for(i=0;i<5;i++){
                                stars[i].src="<?php echo Base_Url('assets/images/emptystars.png')?>";
                                 }
                                 jQuery('#trReponseRub').html("");
                                  var irubId ='<?php echo $oInfoCommercant->IdCommercant?>';
                                    jQuery.get(
                                    '<?php echo site_url("livreor/voir"); ?>' + '/' + irubId,
                                    function (zReponse) {
                                        // alert (zReponse) ;
                                        jQuery('#trReponseRub').html(zReponse);
                                    });
                           }else if (data=='ko'){
                               alert('Une erreur s\'est produite');
                           }
                        },
                        error: function (data) {
                            console.log(data);
                            alert('Une erreur s\'est produite');
                        }
                    }); 
               }else{
                alert('Email non valide')    
                }
            }else{
                alert('veuillez remplirs les champs obligatoires')
            }

        }
        function validateEmail(sEmail) {
                    var filter = /^[\w-.+]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
                    if (filter.test(sEmail)) {
                        return true;
                    }
                    else {
                        return false;
                    }
            }
</script>
<style type='text/css'>
    #valider{
        cursor: pointer;
    }
    .imag {
        height:30px;width:30px;
    }
    .arrondie-devant{
        border-radius: 5px 0px 0px 5px;
    }
    .arrondie-derriére{
        border-radius: 0px 5px 5px 0px;
    }
    .right{
        text-align: right;
    }
</style>
<div class="row <?php if ($group_id_commercant_user == 4) { ?>mt-5<?php } elseif ($group_id_commercant_user == 5) { ?>mt-4<?php } ?> " style="">
    <div class="pl-3 pr-3 pb-0" style="width: 100%">
        <div class="pl-4 pb-0 pt-0 pr-0">
            <div id="head" onclick="livreor()" 
                 class="col-12 text-justify titre_glissiere1">Avis-Témoignages-Commentaires
                <img style="height: 28px;padding-top: 12px;" class="float-right img-fluid"
                     src="<?php echo base_url('assets/img/wpc59cfc3d_06.png') ?>">
            </div>
        </div>
    </div>
</div>
<div class="row d-none" id="autoslide_off_5">
    <div id="livreorcontent"
         class=" col-sm-12 text-justify pt-0 pr-3 d-none"
         style="<?php if ($glisieres->isActive_presentation_4 == '1' AND $glisieres->presentation_4_contenu1 != '') { ?>display: block;height: auto;<?php } else { ?>display: none;height: auto;<?php } ?>">
        <div class="container pl-3" style="padding-right: 11px">
            <img class="img-fluid" style="margin-left: 10px;width: 100%" src="<?php echo Base_Url('assets/images/avis.png')?>"/>
            <input id="vote" type="hidden" name="valeur[vote]" value="" />
            <input id ="Id_commercant" type="hidden" name="valeur[idcommercant]" value="<?php echo $oInfoCommercant->IdCommercant?>"/>
            <input id = "date" type="hidden" name="valeur[date]" value="<?php echo date('Y-m-d H:i:s');?>"/>
            <div class="row" style="">
                <div class="pl-0 pb-0" style="width: 100%; padding-right: 5px;">
                    <div class="pl-4 pb-0 pt-0 pr-0">
                        <section class="col-sm-12 pl-3" style="background-color: #EEEEEE;">
                            <h2 class="m-0 p-4" style="
                                                font-weight: bold;
                                                font-size: 25px;
                                                font-family: Arial, sans-serif;
                                                text-transform: uppercase;
                                                text-align: center;">
                                        Ajouter un commentaire
                            </h2>
                            <div class="row">
                                <div class ="col-sm-6 form-group">     
                                    <input id="Nom" type="text" class="form-control" name="valeur[nom]" value="" placeholder ='Nom*' />
                                    <input id="mail" type="E-mail" class="form-control" name="valeur[mail]" value="" placeholder ='E-mail*' />
                                    <input id="tel" type="text" class="form-control" name="valeur[mobile]" value="" placeholder ='Mobile*' />
                                    <span style="float: right;font-size: 5;">* Saisie obligatoire</span>
                                </div>          
                                <div class ="col-sm-6 form-group"> 
                                    <textarea id="message" name="valeur[message]" class="form-control form-group" rows="7" cols="60" style="height: 115px"></textarea>
                                    <div class="row pr-3">
                                        <div class="col-sm-6">
                                            <img class="imag arrondie-devant" style="float:left;cursor:pointer;" src="<?php echo Base_Url('assets/images/emptystars.png')?>" name="vote" id="0"  onclick="fillstar(this,1)" />
                                            <img class="imag" style="float:left;cursor:pointer;" src="<?php echo Base_Url('assets/images/emptystars.png')?>" name="vote" id="1" onclick="fillstar(this,2)" />
                                            <img class="imag" style="float:left;cursor:pointer;" src="<?php echo Base_Url('assets/images/emptystars.png')?>" name="vote" id="2"   onclick="fillstar(this,3)" />
                                            <img class="imag" style="float:left;cursor:pointer;" src="<?php echo Base_Url('assets/images/emptystars.png')?>" name="vote" id="3" onclick="fillstar(this,4)" />
                                            <img class="imag arrondie-derriére" style="cursor:pointer;" src="<?php echo Base_Url('assets/images/emptystars.png')?>" name="vote"  id="4"  onclick="fillstar(this,5)" />
                                        </div>
                                        
                                        <div id ="valider" onclick="enregistrer()" class="col-sm-6"
                                                    style="
                                                            width: 200px;
                                                            background-color: rgb(133, 194, 36);
                                                            height: 40px;
                                                            text-align: center;
                                                            line-height: 2.60;
                                                            color: white;
                                                            text-transform: uppercase;
                                                            font-weight: bold;
                                                    ">
                                        Validation</div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div id="trReponseRub" style="">
                            
            </div>
        </div>        
    </div> 
</div>