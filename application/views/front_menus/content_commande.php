<?php
$thisss =& get_instance();
$thisss->load->library('ion_auth');
$this->load->model("ion_auth_used_by_club");
$user_ion_auth_id = $this->ion_auth_used_by_club->get_ion_id_from_commercant_id($oInfoCommercant->IdCommercant);
if (isset($user_ion_auth_id)) $user_groups = $thisss->ion_auth->get_users_groups($user_ion_auth_id)->result(); else $user_groups = 0;
if ($user_groups != 0) $group_id_commercant_user = $user_groups[0]->id; else $group_id_commercant_user = 0;
function isMobile () {
    return is_numeric(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "mobile"));
}

?>
<!--<div class="container">-->
<!--    --><?php //$this->load->view('front_menus/includes_commande/head'); ?>
<!--</div>-->
<!--<div id="type_vente">-->
<!--    --><?php //$this->load->view('front_menus/includes_commande/emport_livr'); ?>
<!--</div>-->
<!-- <div class="row mt-5">
    <div class="col-12 text-center">
        <h3 class="titre_header">NOS MENUS ET CARTES</h3>
            <div class="col-lg-2 col-sm-12 pt-1">
                <button onclick="translatede()" type="button" class="btn btn-lg btn-outline-danger" id="google_translate_element">Langue</button>
            </div>
    </div>
</div> -->
<div class="container">
  <div class="row">
    <div class="col text-center">
      <h3 class="titre_header">NOS MENUS ET CARTES</h3>
      <button onclick="translatede()" type="button" class="btn btn-lg btn-outline-danger" id="google_translate_element" style="width: 50%!important">Langue</button>
    </div>
  </div>
</div>
<div class="row">
    <div class="col-lg-6 col-sm-6 mb-3">
        <div class="text-right pt-5">
            <?php if (isset($commande->url_qr_code_menu) AND is_file("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/menu_qr_code/".$commande->url_qr_code_menu)){ ?>
                <img src="<?php echo base_url("application/resources/front/photoCommercant/imagesbank/".$oInfoCommercant->user_ionauth_id."/menu_qr_code/".$commande->url_qr_code_menu)?>">
          <?php  }else{ ?>
                <img src="<?php echo base_url()?>assets/image/qrcode.jpg">
            <?php } ?>
        </div>
    </div>
    <div class="col-lg-6 col-sm-6 mb-3">
        <div class="text-left pt-3">
            <img src="<?php echo base_url()?>assets/image/qr-code1.png">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center pb-4">
        <span style="font-family: futura-lt-w01-book;font-size:25px;color:#000000;">Nos menus et cartes sur votre mobile, flashez !...</span>
    </div>
</div>
<div class="row text-center">
    <div class="col-lg-12 pb-4">
        <span style="width: 200px; margin: 0 auto;">
            <script type="text/javascript">
                function menu_table_number_function(table_number){
                    jQuery("#menu_table_number_span").html('Table N°0'+table_number);
                }
            </script>
            <input type="number" min="0" id="menu_table_number_input" onchange="menu_table_number_function(this.value);" class="" style="border: solid #e80eae!important;height: 50px;font-size: 30px;line-height: 30px;text-align: center;font-weight: bold;width: 200px;"/>
        </span>
    </div>
    <div class="col-lg-12 font-weight-bold text-center">Préciser le numéro de table</div>
</div>
<input type="hidden" id="activ_glissiere_emporter_value" value="0">
<input type="hidden" id="activ_glissiere_livraison_value" value="0">
<input type="hidden" id="activ_glissiere_differe_value" value="0">
<input type="hidden" id="activ_glissiere_activ1_value" value="0">
<input type="hidden" id="activ_glissiere_activ2_value" value="0">
<input type="hidden" id="activ_glissiere_activ3_value" value="0">
<input type="hidden" id="activ_glissiere_activ4_value" value="0">
<input type="hidden" id="activ_glissiere_activ7_value" value="0">
<input type="hidden" id="activ_glissiere_activ6_value" value="0">
<input type="hidden" id="activ_glissiere_activ7_value" value="0">
<input type="hidden" id="id_recap_value" value="0">
<input type="hidden" id="id_spec_gli_value" value="0">
<input type="hidden" id="value_emp_activ" value="0">
<input type="hidden" id="value_livr_activ" value="0">
<input type="hidden" id="value_differe_activ" value="0">
<input type="hidden" id="all_added_product1">
<input type="hidden" id="all_added_product2">
<input type="hidden" id="all_added_product3">
<input type="hidden" id="all_added_product4">
<input type="hidden" id="all_added_product6">
<input type="hidden" id="all_added_product7">
<?php if(isset($IdUser) && $IdUser != null && $IdUser!= ""){ ?>
    <input type="hidden" id="id_client" value="<?php echo $IdUser; ?>">
<?php }else{ ?>
    <input type="hidden" id="id_client" value="0">
<?php } ?>
<input type="hidden" id="is_register_open" value="0">
<div id="content_gli">
    <?php $this->load->view('front_menus/includes_commande/gli1'); ?>
    <?php $this->load->view('front_menus/includes_commande/gli2'); ?>
    <?php $this->load->view('front_menus/includes_commande/gli3'); ?>
    <?php $this->load->view('front_menus/includes_commande/gli4'); ?>
    <?php $this->load->view('front_menus/includes_commande/gli5'); ?>
    <?php $this->load->view('front_menus/includes_commande/gli6'); ?>
</div>



    <?php $this->load->view('front_menus/includes_commande/recap'); ?>
    <?php //$this->load->view('front_menus/includes_commande/variante'); ?>
    <?php $this->load->view('front_menus/includes_commande/script'); ?>

<style>
    @media screen and (max-width: 400px){
        .btn_menu {
            border: 2px solid #ffffff !important;
            border-radius: 20px !important;
            background: ;
            background: #3453a2!important;
            color: #ffffff !important;
            width: 100%;
            font-size: 15px;
        }
        .mobile_centered {
            display: block;
            margin: auto;
        }
    }
</style>

