
<?php
    $data['zTitle'] = "Commandes - ".$oInfoCommercant->NomSociete;
    $data["slide"] = 'wp41514793_05_06.jpg';
    $data["check_active"] = "commandes";
?>
<?php $this->load->view('front_menus/includes/main_header',$data); ?>
    <link href="<?php echo base_url();?>application/views/front_menus/includes_commande/css/style.css" rel="stylesheet" type="text/css">
<?php
$this->load->view("front_menus/includes/main_header_premium", $data);
include 'application/views/front_soutenons/includes_commande/css/style_premium.php';
?>

<div class="container" style="max-width: 100%!important; width: 100%!important">
<?php if (!isset($_GET['content_only'])){ ?>
    <?php $this->load->view('front_menus/includes/menu_commande',$data); ?>
    <?php $this->load->view('front_menus/includes/presentation',$data); ?>
    <?php $this->load->view('front_soutenons/includes/menu_partner',$data); ?>
    <?php $this->load->view('front_soutenons/includes/acces_site',$data); ?>
    
<?php } ?>
<?php $this->load->view('front_menus/content_commande',$data); ?>
<?php if (!isset($_GET['content_only'])){ ?>
    <?php $this->load->view('front_menus/includes/footer_commercants',$data); ?>
<?php } ?>
<?php //$this->load->view('front_menus/includes/main_footer_copyright',$data); ?>
</div>

<!--to top button-->
<a id="back-to-top" href="#" class="btn btn-secondary btn-lg back-to-top" role="button"><i class="fas fa-chevron-up"></i></a>
<style type="text/css">
    .back-to-top {
        position: fixed;
        bottom: 25px;
        right: 25px;
        display: none;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
    });
</script>
