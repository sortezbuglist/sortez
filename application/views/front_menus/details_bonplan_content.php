<style>
    .retour_menu{
        position: relative;
        font-family:Futura-LT-Book, Sans-Serif;
        font-weight:bold;
        color:#3453a2;
        text-decoration:none;
        font-size:15px;
        padding-left: 30px;
    }
    .retour_menu::before{
        content: "\f0a8";
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        text-decoration: inherit;
        /*--adjust as necessary--*/
        color: #3453a2;
        font-size: 40px;
        padding-right: 0.5em;
        position: absolute;
        top: -20px;
        left: -15px;
    }
</style>
<div class="container">
    <div class="col-lg-12 mb-3">
        <a class="retour_menu" href="<?php echo site_url("soutenons/DealAndFidelite"); ?>" style="text-decoration: unset!important;" >Retour &agrave; la liste des deals et fidélités</a>
    </div>
    <div class="col-lg-12 p-0 mb-4" style="background-color:transparent;">
        <?php
        if (isset($oBonPlan) && $oBonPlan->bonplan_type == "3") $this->load->view("front_menuspage_bonplan_partenaire_contents_multiple", $data);
        else if (isset($oBonPlan) && $oBonPlan->bonplan_type == "2") $this->load->view("front_menuspage_bonplan_partenaire_contents_unique", $data);
        else $this->load->view("front_menuspage_bonplan_partenaire_contents_simple", $data);
        ?>
    </div>
</div>
