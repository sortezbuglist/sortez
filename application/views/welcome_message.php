<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Lucida Grande, Verdana, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

</style>
</head>
<body>

<h1>Welcome to CodeIgniter!</h1>

<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

<p>If you would like to edit this page you'll find it located at:</p>
<code>application/views/welcome_message.php</code>

<p>The corresponding controller for this page is found at:</p>
<code>application/controllers/welcome.php</code>

<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>


<p><br />Page rendered in {elapsed_time} seconds</p>





<div
    style="width:600px;
    margin-left:auto;
    margin-right:auto;
    font-family:Verdana, Arial, Helvetica, sans-serif;
    color: #000;">
    <table width="600" border="0" cellspacing="0" cellpadding="0"
           style="background-color:#ffffff; color:#000000;table-layout: fixed;">
        <tr>
            <td>

                <div style="width: 100%; text-align: center;">
                    <a href="https://www.sortez.org/">
                    <img
                        src="https://www.sortez.org/application/resources/sortez/images/logo.png"
                        style="border:none; text-align: center;"
                        alt="Magazine Sortez"/>
                    </a>
                </div>

                <div style="
                text-align: center;
                text-align: center;
                line-height: 45px;
                font-family: Futura Md, sans-serif;
                font-style: normal;
                font-weight: normal;
                background-color: transparent;
                font-variant: normal;
                font-size: 20px;
                vertical-align: 0;">
                    L'essentiel des sorties, des loisirs et de la vie locale !...
                </div>

                <div style="text-transform: uppercase;
                text-align: center;
                text-align: center;
                line-height: 45px;
                font-family: Futura Md, sans-serif;
                font-style: normal;
                font-weight: normal;
                background-color: transparent;
                font-variant: normal;
                font-size: 32px;
                vertical-align: 0;">
                    DÉCOUVREZ LES BONNES ADRESSES !…
                </div>

                <div style="text-transform: uppercase;
                text-align: center;
                text-align: center;
                line-height: 45px;
                font-family: Futura Md, sans-serif;
                font-style: normal;
                font-weight: normal;
                background-color: transparent;
                font-variant: normal;
                font-size: 26px;
                vertical-align: 0;">
                    PLUS DE 1 000 ÉTABLISSEMENTS RÉFÉRENCÉS
                </div>

                <div style="
                text-align: center;
                text-align: center;
                line-height: 45px;
                font-family: Futura Md, sans-serif;
                font-style: normal;
                font-weight: normal;
                background-color: transparent;
                font-variant: normal;
                font-size: 20px;
                vertical-align: 0;">
                    Actualisation du Jeudi, 21 Avril 2022                </div>


                <table width="100%" border="0" cellspacing="0" cellpadding="0"
                       style="padding: 0 15px;table-layout: fixed;">
                    <tr>
                        <td width="60%">
                            <div style="text-align: justify; padding-right: 20px; margin: 15px 0 30px;">
                                <p>Découvrez nos partenaires et les autres bonnes adresses référencées par notre rédaction dans les domaines du sport, du patrimoine, de la culture, du shopping et de la vie associative.</p>
                            </div>
                        </td>
                    </tr>
                </table>
<div id="conteneur">

<table id="index3" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>

    <table id="index3" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>

    <table id="index3" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>
    <table id="index2" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>

                            <table id="index1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>
                            <table id="index1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>
                            <table id="index1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>
                            <table id="index1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>
                            <table id="index1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1982/thumbs/thumbnail_878b9623-41b0-4475-9e29-426efb3ab98c.jpg" width="300px" height="175px" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>
                            <table id="index1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>
                            <table id="index1" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="https://www.sortez.org/magazine/presentation/"
                                                                       target="_blank">
                                                                        <img src="https://www.sortez.org/application/resources/front/photoCommercant/imagesbank/1992/3988ca98-1c2f-43fc-945f-b27e07740a62.png" width="100%" id="pic_965" name="pic_965"/>                                                                    </a>
                                                                    <div
                                                                        style="background: rgba(0, 0, 0, 0.6) none repeat scroll 0px 0px;
                                                                        color: rgb(255, 255, 255);
                                                                        height: 40px; line-height: 40px;
                                                                        margin-left: auto;
                                                                        margin-right: auto;
                                                                        margin-top: -40px;
                                                                        z-index: 1000 ! important;
                                                                        position: absolute;
                                                                        width: 116px;font-family:Verdana, Arial, Helvetica, sans-serif;">COMMUNICATION</div>
                                                                </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #000000;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                        <span style=''>EDITIONS JOURNAUX, PRESSE, MAGAZINES</span>
                                                    </div>

                                                    <div class="p_bp_details" id="bp_details_nomste" style='background-color: transparent;
                                                        line-height: 20px;
                                                        padding-top: 15px;
                                                        margin: 0;
                                                        text-decoration: none; text-align:center;
                                                        vertical-align: 0;
                                                        font-family:Verdana, Arial, Helvetica, sans-serif;
                                                        font-size: 16px;
                                                        font-style: normal;
                                                        color: #cf1590;
                                                        font-variant: normal;
                                                        font-weight: 700;'>
                                                                 <span
                                                            style=''>Magazine Sortez</span>
                                                    </div>

                                                    <div class='p_bp_details' id='bp_details_quart'
                                                         style='text-align:center; margin: 0; color: #000000; padding: 5px;'>
                                                       
Sortez.org est une solution de gestion multi-sites et multi-utilisateurs sur le web qui utilise des outils innovants, personnels et fédérateurs de création et de diffusion exponentielle de l’information.

Sortez.org fédère et stimule l’information ...
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 33%;">
                                                    <div class="col-xs-12 padding0" style="text-align:right;">
                                                        <a href='<?php echo site_url($oListeBonPlan->nom_url . "/presentation/"); ?>'
                                                           style="color:#FFFFFF; text-decoration:none;" title="<?php echo $oListeBonPlan->NomSociete; ?>"><img
                                                                src="<?php echo GetImagePath("privicarte/"); ?>/plus.png"/></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        
                            </table>
                        </div>

<<!-- div id="conteneur">
    <table id="index1">
        <tr><th>Index 1</th></tr>
    </table>
    <table id="index2">
        <tr><th>Index 2</th></tr>
    <table>
    <table id="index3">
        <tr><th>Index 3</th></tr>
    <table>
        <table id="index1">
        <tr><th>Index 1</th></tr>
    </table>
    <table id="index2">
        <tr><th>Index 2</th></tr>
    <table>
    <table id="index3">
        <tr><th>Index 3</th></tr>
    <table>
</div> -->
<style type="text/css">
    table#index1, table#index2, table#index3 {
    width: 30%;
    border: 2px solid black;
}
table#index1 { float: left;margin-left: 20px; margin-top: 20px; }
table#index2 { float: left; margin-left: 20px; margin-top: 20px;}
table#index3 { float: left;margin-left: 20px; margin-top: 20px;}
 
#conteneur {
    /*width:500px;*/
margin:auto;
}
</style>
</body>
</html>