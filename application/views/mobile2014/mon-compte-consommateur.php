<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
<div class="wrapper head" style="display:table; text-align: center; width: 100%;">
    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
       <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
            <?php //$this->load->view('mobile2014/partial/header_titre')?>
            <div class="titre">Mon compte consommateur</div>
</div>
<div class="wrapper page-black">
    <!--liens-->
    <div class="wrap links">
        <!--  <div class="pad-tb-5">
            <a href="<?php echo site_url('front/utilisateur/menuconsommateurs')?>" class="pure-button pure-button-primary">Retour au menu général</a>
        </div>-->
        <div class="pad-tb-5">
            <a href="<?php echo site_url("front/fidelity/inscription/");?>" class="pure-button pure-button-primary">Mes données</a>
        </div>
        <div class="pad-tb-5">
            <a href="<?php echo site_url('front/user_fidelity/order_fidelity_card')?>" class="pure-button pure-button-primary">Ma carte sur mon mobile</a>
        </div>
        <!--div class="pad-tb-5">
            <a href="#<?php //echo site_url("front/user_fidelity/save_card_mobile")?>" class="pure-button pure-button-primary">Demander une carte physique</a>
        </div-->
        <div class="pad-tb-5">
            <a href="<?php echo site_url("front/user_fidelity/fidelity_event_list_mobile")?>" class="pure-button pure-button-primary">Ma consommation</a>
        </div>
        <div class="pad-tb-5">
            <a href="<?php echo site_url("front/user_fidelity/mes_reservations_mobie")?>" class="pure-button pure-button-primary">Mes réservations en cours</a>
        </div>
         <div class="pad-tb-5">
            <a href="<?php echo site_url("front/mobile/localisation")?>" class="pure-button pure-button-primary">Configurer la localisation</a>
        </div>
        <div class="pad-tb-5">
            <a href="<?php echo site_url()?>" class="pure-button pure-button-primary" target="_blank">Accès direct à sortez.org</a>
        </div>
        <div class="pad-tb-5">
            <a href="<?php echo site_url('auth/logout')?>" class="pure-button pure-button-primary">Déconnexion</a>
        </div>

    </div>
    <!--liens-->
    <div class="clr"></div>
</div>
<?php $this->load->view('mobile2014/partial/footer')?>