<?php $this->load->view('mobile2014/partial/head')?>
<div style="height: 97%;" >
<div class="wrapper head">
    <!--LOGO-->
     <?php $this->load->view('mobile2014/partial/logo_head_view')?>
    <!--LOGO-->
</div>
<div class="wrapper padded middle">
    <!--liens-->
    <div class="wrap accueil-promo">
        <div class="list">
            <a href="#"><img src="<?php echo img_url('photo-promo_1.png') ?>" alt=""/></a>
        </div>
        <div class="list">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td v-align="middle" width="30%">
                        <div class="pad-10">
                        <a href="#"><img src="<?php echo img_url('acc-1.png') ?>" alt=""/></a>
                    </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo site_url('front/bonplan')?>">
                                Decouvrez les bonnes affaires déposées par les professionnels partenaires...
                            </a>
                    </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/bonplan')?>"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="list">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td v-align="middle" width="30%">
                        <div class="pad-10">
                        <a href="#"><img src="<?php echo img_url('acc-2.png') ?>" alt=""/></a>
                    </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo site_url("front/particuliers/inscription");?>">
                              Bénéficier de ces avantages en souscrivant un abonnement gratuit à privicarte...
                            </a>
                    </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url("front/particuliers/inscription");?>"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="list">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td v-align="middle" width="30%">
                        <div class="pad-10">
                        <a href="#"><img src="<?php echo img_url('acc-3.png') ?>" alt=""/></a>
                    </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo site_url("auth/login");?>">
                               Vous êtes déjà inscrit ! <br/>
                                Accédez directement à votre carte personnelle...
                            </a>
                    </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url("auth/login");?>"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!--liens-->
        <div class="clr"></div>
    <?php $this->load->view('mobile2014/partial/menu')?>
</div>
    <div class="clr"></div>


</div>
<script type="text/javascript">
$('body').css({backgroundColor: "#000"});
</script>
   <?php $this->load->view('mobile2014/partial/footer')?>
   