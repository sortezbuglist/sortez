<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
	<div class="wrapper head">
	    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
	
	             <?php $this->load->view('mobile2014/partial/header_titre')?>
	</div>
<div class="wrapper page-black">
    <div>
	    <!--forms-->
	    <div class="wrap forms pad-tb-25">
	      <form class="pure-form pure-form-stacked" method="post" action="<?php echo site_url('auth/forgot_password')?>" id="formMobileAuth">
	          <div class="pure-u-5-5">
	                  <div class="pad-10 titre align-center error">
	                    <?php echo $message;?>
	                  </div>
	              </div>
	          <div class="pad-tb-10">
	          <label for="">Indiquer votre email pour redefinir<br /> votre mot de passe</label>
	            <?php echo form_input($email,'','class="pure-input-1"');?>
	          </div>
	      </form>
	    </div>
	    <!--forms-->
	    <!--liens-->
	    <div class="wrap links pad-tp-100" >
	        <div class="pad-tb-10">
	            <a type='submit'  onClick='submitForm();' id="btn_submit" class="pure-button pure-button-primary">Envoyer le mot de passe</button></a>
	        </div>
	    </div>
        <!--liens-->
    </div>
</div>
<script>
function submitForm(){
	$('#formMobileAuth').submit();
};

$(function(){
	$( "#formMobileAuth" ).validate({
		rules: {
			email: {
				required: true,
				email: true,
			}
		},
		messages: {
			email: {
				required: "ce champ est requis",
				email : "Veuillez entrer un mail valid",
			}
		}
	});
	
});
</script>
<?php //$this->load->view('mobile2014/partial/footer')?>
