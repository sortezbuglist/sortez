<?php $this->load->view('mobile2014/partial/head')?>
<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
   <?php $this->load->view('mobile2014/partial/titre_head_view')?>
</div>
<div class="wrapper page-black">
    <div>
	    <!--forms-->
	    <div class="wrap forms">
	    
		<?php 
			$attributes = array('class' => 'pure-form pure-form-stacked', 'id' => 'formMobileAuth');
			echo form_open('auth/reset_password_mobile/' . $code,$attributes);?>	    
	          <div class="pure-u-5-5">
	                  <div class="pad-10 titre align-center error">
	                    <?php echo $message;?>
	                  </div>
	              </div>
	          <div class="pad-tb-10">
	          <label for="">Nouveau mot de passe</label>
	            <?php echo form_input($new_password,'','class="pure-input-1" id="password"');?>
	          </div>
	          <div class="pad-tb-10">
		          <label for="">confirmation du mot de passe</label>
		                  <?php echo form_input($new_password_confirm,'','class="pure-input-1"');?>
		                  <?php echo form_input($user_id);?>
							<?php echo form_hidden($csrf); ?>
		          
		          <!--  <input type="password" class="pure-input-1"/>-->
		       </div>
	      </form>
	    </div>
	    <!--forms-->
	    <!--liens-->
	    <div class="wrap links">
	        <div class="pad-tb-10">
	            <a type='submit'  onClick='submitForm();' id="btn_submit" class="pure-button pure-button-primary">Envoyer le mot de passe</button></a>
	        </div>
	    </div>
        <!--liens-->
    </div>
</div>
<script>
function submitForm(){
	$('#formMobileAuth').submit();
};

$(function(){
	$( "#formMobileAuth" ).validate({
		rules: {
			new : {
				required: true,
                minlength : 6
            },
            new_confirm : {
            	required: true,
                minlength : 6,
                equalTo : "#new"
            },
		},
		messages: {
			new: {
				required: "ce champ est requis",
				  minlength : "minimum 6",
			},
			new_confirm : {
	            	required: "ce champ est requis",
	                minlength : "minimum 6",
	                equalTo : "Les 2 mots de passe ne sont pas identiques"
	            },
		}
	});
	
});
</script>
<?php //$this->load->view('mobile2014/partial/footer')?>
