<?php $this->load->view('mobile2014/partial/head')?>
<div class="wrapper head">
    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
     <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
     <?php $this->load->view('mobile2014/partial/header_titre')?>
</div>
<div class="wrapper page-white">
    <!--forms-->
    <div class="fonctionnement">
          <div id="fonctionement-tabs">
          <!--  
              <ul class="pad-tb-5 pure-g btnul2">
                  <li class="pure-u-1-5"><a href="#fonctionement-1" class="pure-button btn-onglet">1</a></li>
                  <li class="pure-u-1-5"><a href="#fonctionement-2" class="pure-button btn-onglet">2</a></li>
                  <li class="pure-u-1-5"><a href="#fonctionement-3" class="pure-button btn-onglet">3</a></li>
                  <li class="pure-u-1-5"><a href="#fonctionement-4" class="pure-button btn-onglet">4</a></li>
                  <li class="pure-u-1-5"><a href="#fonctionement-5" class="pure-button btn-onglet">5</a></li>
              </ul>-->
              <div id="fonctionement-1">
                  <div class="pad-lr-15 align-center">
                    <!--    <img src="<?php echo img_url('photo-promo.png')?>" alt=""/>-->
                      <p>Nos partenaires professionnels concèdent des bons plans et des conditions de fidélisatons.</p>
                      <p>
                          Pour en bénéficier, les consommateurs doivent souscrire un abonnement gratiut à notre système de carte de fidélité Privicarte.
                      </p>


                      <div class="pure-g">
                          <div class="pure-u-2-5">
                              <img src="<?php echo img_url('photo-main.png')?>" alt=""/>
                          </div>
                          <div class="pure-u-3-5 align-left">
                            <p>Après avoir validé leur inscription, ils accèdent immédiatement à leur carte de fidélité sur leur smartphone.</p>
                              <p>
                                  Lors de leur déplacement chez le professionnel, il leur suffit de présenter cette carte pour bénéficier des avantages.
                                  <br/><br/><br/>
                              </p>
                          </div>
                      </div>
                  </div>
              </div>
              <div id="fonctionement-2">
                  <div class="pad-lr-15 -align-center">
                      <img src="<?php echo img_url('photo-carte.png')?>" alt=""/>
                      <p>
                          <strong>Des cartes physiques au format carte bancaire </strong> <br/>
                          sont également disponibles, il suffit de se rendre chez l'un des partenaires jpour la retiter
                      </p>
                      <p>
                          <strong>Les cartes sont personnalisées</strong> <br/>
                          au nom du site associé <br/>
                          Ex: maville-commerces.fr
                      </p>
                      <img src="<?php echo img_url('photo-shopping-girls.png')?>" alt=""/>
                      <br/>
                      <br/>

                  </div>
              </div>
              <div id="fonctionement-3">
                    
                  <div class="pure-g">
                      <div class="pure-u-1-6"></div>
                      <div class="pure-u-5-6">
                          <div class="pad-lr-10">
                              <h4>Les professionnels ont le choix entre plusieurs formulaes de fidélisation :</h4>
                              <h2>Les offres de fidélisation</h2>
                          </div>
                      </div>
                  </div>

                  <div class="pure-g">
                      <div class="pure-u-1-6">
                          <div class="pad-tb-10"><img src="<?php echo img_url('purecent.png')?>" alt=""/></div>
                        </div>
                      <div class="pure-u-5-6">
                          <div class="pad-lr-10">
                              <h4>Les professionnels ont le choix entre plusieurs formules de fidélisation :</h4>
                              <p>le partenaire concèdent une remise tout au long de l'année sur l'ensemble desa achats
                                  <br/>
                                  Notre système capitalise vos achants
                              </p>
                          </div>
                      </div>
                  </div>

                  <div class="pure-g">
                      <div class="pure-u-1-6">
                          <div class="pad-tb-10">
                              <img src="<?php echo img_url('cochon.png')?>" alt=""/>                </div>
                      </div>
                      <div class="pure-u-5-6">
                          <div class="pad-lr-10">
                              <h4>La capitalisation</h4>
                              <p>le partenaire offre un bon d'achat pour un montant cumulé défini. Ex. 20&euro; offerts pour 200&euro; d'achat cumulés. notre système cumule vos achats et vous averti de la progression de votre consommation.

                              </p>
                          </div>
                      </div>
                  </div>

                  <div class="pure-g">
                      <div class="pure-u-1-6">
                          <div class="pad-tb-10">
                              <img src="<?php echo img_url('tampon.png')?>" alt=""/>                </div>
                      </div>
                      <div class="pure-u-5-6">
                          <div class="pad-10">
                              <h4>Coup de tampons</h4>
                              <p>Le partenaire offre un produit ou un service pour un nombre cumulé défini. Ex: 10 pizzas achetées = 1 pizza offer. Notre système cumule vos points et vous averti de la pregression de votre consommation

                              </p>
                          </div>
                      </div>
                  </div>
                  <br/>
                  <br/>
              </div>
              <div id="fonctionement-4">
                  <div class="pure-g">
                      <div class="pure-u-1-6"></div>
                      <div class="pure-u-5-6">
                          <div class="pure-g">
                              <div class="pure-u-1-4">
                                  <img src="<?php echo img_url('tag.png')?>" alt=""/>
                              </div>
                              <div class="pure-u-3-4">
                                  <h1>Bons plans</h1>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="pure-g">
                      <div class="pure-u-1-6">
                      </div>
                      <div class="pure-u-5-6">
                          <div class="pad-10">
                              <p>Ce sont des offres exceptionnelles valables uns seule fois (1ère visite) ou plusieurs fois suivant la volonté du partenaire</p>
                              <p>Lors de votre déplacement, vous présetez votre carte et bénéficiez immédiatement de cet avantage</p>
                          </div>
                      </div>
                      </div>

                  <div class="pure-g">
                      <div class="pure-u-1-6"><div class="pad-tb-15">
                          <img src="<?php echo img_url('photo-infopng.png')?>" alt=""/></div>
                      </div>
                      <div class="pure-u-5-6">
                          <div class="pad-10">
                              <p>Certains bons plans sont limités à une certaines quantité d'offre. Pour en bénéficier  vous devez obligatoirement les réserveren ligne. Le capital de base sera dinimué automatiquement et le partenaire reçoit un message  de cnfirmation. Un double vous est également adressé.</p>

                              <a href="#"><img src="<?php echo img_url('btn.png')?>" alt=""/></a>
                          </div>
                      </div>
                      </div>
                  <br/>
                  <br/>
              </div>

              <div id="fonctionement-5">
                  <img src="<?php echo img_url('photo-responsive-device.png')?>" alt=""/> <br/>
                  <img src="" alt=""/>
                  <div class="pad-10 align-center">
                         <p>De plus sur leur mobile ou ordinateur, ils accèdent à un compte personnel et sécurisé : validation, gestion, historiques des consommations, alarmes &hellip;

                         </p>
                      <p>
                          Votre carte multi-commerces <br/>
                          est utilisable sur l'ensemble de réseau <br/>
                          PRIVICARTE
                      </p>
                              <img src="<?php echo img_url('photo-main-carte.png')?>" alt=""/>
                  </div>

              </div>
          </div>

</div>
    </div>
    <!--forms-->
    <?php $this->load->view('mobile2014/partial/menu')?>
<?php $this->load->view('mobile2014/partial/footer')?>