<?php $this->load->view('mobile2014/partial/head-pro')?>
 <?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="wrap forms accordeon">
      <form class="pure-form pure-form-stacked">
          <br/>
              <div class="text-box f-size16">
              <p></p>
              <br/>
              <p>Le montant de l'achat a été ajouté<br/>
              sur le compte du client
              <br/>
              Le nouveau solde est de :
              </p>

              <div class="pure-g bg-red">
                  <div class="<?php echo blue_class_progress($solde_capital_client, $objectif_capital)?>">
                      <div class="pad-lr-5 "><?php echo (isset($solde_capital_client)) ? $solde_capital_client : 0?>&euro;</div>
                  </div>
                  <div class="<?php echo red_class_progess($solde_capital_client, $objectif_capital)?>"><div class="pad-lr-5"><?php echo isset($objectif_capital) ? $objectif_capital : 0?>&euro;</div></div>
              </div>
              <p>un mail de remerciement a été adressé au consommateur
              </p>

          </div>
          <br /><br />
	      <div class="capitalisation accordeon-child"">
              	<a class="pure-button pure-button-primary" href="<?php echo site_url('front/fidelity_pro/detail_client/'.$user->IdUser)?>"> Historique de sa consommation</a>
          </div>
 		  <div class="pad-tb-5 accordeon">
              <a class="pure-button pure-button-primary" href="<?php echo site_url('/front/fidelity_pro/validation_operation/'.$user->IdUser)?>">Retour menu avantages</a>
          </div>

      </form>
    </div>
    <!--forms-->
    <div class="clr"></div>
</div>
<?php $this->load->view('mobile2014/partial/footer')?>