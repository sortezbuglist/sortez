<?php //var_dump($client);?>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<style type="text/css">
    .bouton{
        height: 40px;
        line-height: 3;
        color: white!important;
        font-size: 17px;
        cursor: pointer;
    }
    a:hover{
        text-decoration: none;
    }
    a:visited{
        color: white;
    }
    .bg-pink{
        background-color: rgb(208, 0, 118);
    }
    .bg-marroon{
        background-color: #D4AE87;
    }
</style>
</div>
<?php //var_dump($fidelity); ?>

<div class="wrapper page-white">
    <!--forms-->
    <div class="consommation">
        <div class="product first">
            <div class="details pad-tb-10">
				<div class="txt-product">
                            <strong><?php echo $client->Nom.' '.$client->Prenom?></strong>
                            <span><?php echo $client->NomVille?> </span>
                            <table border="0" width="100%">
                                <tr>
                                    <td width="60"><div class="pad-lr-5"><a href="<?php echo site_url('front/fidelity_pro/user/'.$client->IdUser);?>"><img src="<?php echo img_url("id_card.png") ?>" alt="" width="50"/></a></div></td>
                                    <td width="60"><div class="pad-lr-5"><a href="mailto:<?php echo $client->Email;?>"><img src="<?php echo img_url("email.png") ?>" alt="" width="50"/></a></div></td>
                                    <td><div class="pad-lr-5"><a href="tel:<?php echo $client->Telephone;?>"><img src="<?php echo img_url("mobile_phone.png") ?>" alt="" width="50"/></a></div></td>
                                </tr>
                            </table>
                </div>
            </div>
            <div class="row text-center">
                <?php //echo $infocom->referencement_fidelite;?>
                <?php if (isset($infocom->referencement_fidelite) AND $infocom->referencement_fidelite =='1'){ ?> <div class="col-4 bouton bg-blue"><a href="<?php echo site_url('front/fidelity_pro/details_fidelity/'.$infocom->user_ionauth_id)?>"><div class="text-white">Detail fidelité</div></a></div><?php } ?>
                <?php  if (isset($infocom->user_ionauth_id)){ ?><div class="col-<?php if (isset($infocom->referencement_fidelite) AND $infocom->referencement_fidelite =='1'){ ?>4<?php }else{ echo '6';} ?> bouton bg-pink"><a href="<?php echo site_url('front/fidelity_pro/details_bonplan/'.$infocom->user_ionauth_id)?>"><div  class="text-white">Detail bonplan</div></a></div><?php } ?>
                <?php  if (isset($infocom->user_ionauth_id)){ ?><div class="col-<?php if (isset($infocom->referencement_fidelite) AND $infocom->referencement_fidelite =='1'){ ?>4<?php }else{ echo '6';} ?> bouton bg-marroon"><a href="<?php echo site_url('front/fidelity_pro/details_plat/'.$idcom)?>"><div  class="text-white">Detail plat</div></a></div><?php } ?>
            </div>
        </div>
    </div>


    <div class="clr"></div>
</div>
    <!--forms-->
</div>

</body>
</html>