<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>

<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
   <div class="wrap forms accordeon">
      <div class="pure-form pure-form-stacked">
    
    		  <div class="pad-tb-5">
	              <button class="pure-input-1 pure-button pure-button-primary accordion"  id="tampon"> Offre "remise directe"</button>
	          </div>
	          <div class="text-box-fixed  f-size16 tampon accordeon-child ">
	              <p align="center">
	                  <p>Le montant de l'achat a été ajouté<br/>
              				sur le compte du client
				              <br/>
				              Le nouveau solde est de :
				              </p>
	              </p>
	              <div class="pure-g bg-blue radius-5 pad-tb-5 marg-bt-10">
			                <div class="pure-u-1-2 bg-blue align-left">
			                    <div class="pad-lr-5">Cumul consommation</div>
			                </div>
			                <div class="pure-u-1-2 bg-blue align-right price radius-5"">
			                	<div class="pad-lr-5"><?php echo (isset($assoc_client_bon_plan) && $assoc_client_bon_plan->solde_remise) ? $assoc_client_bon_plan->solde_remise : 0?> &euro;</div>
			                </div>
			      </div>
              <p>un mail de remerciement a été adressé au consommateur
              </p>
	          </div>
	          
	      <div class="pad-tb-5 marg-tp-10">
              <a href="<?php echo site_url('front/fidelity_pro/fidelity_card_operations/'.$user->IdUser)?>" class="pure-button pure-button-primary" > Retour menu avantages</a><br>
          </div>    
           <div class="pad-tb-5">
          <a href="<?php echo site_url("front/fidelity_pro/detail_client/".$user->IdUser)?>" class="pure-button pure-button-primary" > Historique de sa consommation</a>
          </div>   
    </div>
    <!--forms-->
    <div class="clr"></div>
</div>
</div>
<?php $this->load->view('mobile2014/partial/footer')?>