<?php //var_dump($client);?>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>

<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
    <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        .bouton{
            height: 40px;
            line-height: 2.5;
            color: white!important;
            font-size: 17px;
            cursor: pointer;
        }
        .bg-pink{
            background-color: rgb(208, 0, 118);
        }
        .fontinfoclient{
            font-size: 15px!important;
        }
        .interlign{
            /*Line-Height: 8pt;*/
            line-height: 10px;
        }
    </style>
</div>
<?php //var_dump($fidelity); ?>
<div class="wrapper page-white">
    <!--forms-->
    <div class="consommation">
        <div class="product first">
            <div class="details pad-tb-10">
                <div class="txt-product">
                    <div class="interlign">
                        <span style="font-size: 20px!important;"><strong><?php echo $client->Nom.' '.$client->Prenom?></strong></span>
                        <span style="font-size: 18px!important;"><?php echo $client->NomVille?> </span>
                    </div>
                    <table border="0" width="100%">
                        <tr>
                            <td width="60"><div class="pad-lr-5"><a href="<?php echo site_url('front/fidelity_pro/user/'.$client->IdUser);?>"><img src="<?php echo img_url("id_card.png") ?>" alt="" width="50"/></a></div></td>
                            <td width="60"><div class="pad-lr-5"><a href="mailto:<?php echo $client->Email;?>"><img src="<?php echo img_url("email.png") ?>" alt="" width="50"/></a></div></td>
                            <td><div class="pad-lr-5"><a href="tel:<?php echo $client->Telephone;?>"><img src="<?php echo img_url("mobile_phone.png") ?>" alt="" width="50"/></a></div></td>
                        </tr>
                    </table>
                </div>
            </div>
            <?php if($offre=="capital" AND !empty($fidelity)){?>
                <span class="fontinfoclient">Capitalisation : <?php  echo (!empty($fidelity->description)) ? $fidelity->description : ''?></span>
                <span class="fontinfoclient">Date de début : <?php  echo (!empty(convert_Sqldate_to_Frenchdate($fidelity->date_debut))) ? $fidelity->date_debut : ''?></span>
                <span class="fontinfoclient">Date de fin : <?php  echo (!empty(convert_Sqldate_to_Frenchdate($fidelity->date_fin))) ? $fidelity->date_fin : ''?></span>
                <span class="fontinfoclient">Montant : <?php  echo (!empty($fidelity->montant)) ? $fidelity->date_fin : ''?></span>
                <div class="pure-g bg-red">
                    <div class="<?php if(is_numeric($fidelity->solde_capital) AND is_numeric($fidelity->objectif)) echo blue_class_progress($fidelity->solde_capital, $fidelity->objectif)?> fontinfoclient">
                        <div class="pad-lr-5 fontinfoclient"><?php echo (!empty($fidelity->solde_capital)) ? $fidelity->solde_capital : 0?>&nbsp; &euro;</div>
                    </div>
                    <div class="<?php if(is_numeric($fidelity->solde_capital) AND is_numeric($fidelity->objectif)) echo red_class_progess($fidelity->solde_capital, $fidelity->objectif)?> fontinfoclient"><div class="pad-lr-5 fontinfoclient"><?php echo (!empty($fidelity->objectif)) ? $fidelity->objectif : 0?> &nbsp; &euro;</div></div>
                </div>
                <form action="<?php echo site_url('front/fidelity_pro/reinistialisation')?>" class="pure-form pure-form-stacked no-padd" method="POST" id="form-actualisation">
                    <div class="input">
                        <input type="text" class="pure-input-1" placeholder="Noter le report à nouveau" name="montant_init">
                        <input type="hidden" value="<?php echo $fidelity->fiche_id?>" name="fiche_id" />
                        <input type="hidden" value="<?php echo $offre?>" name="type_offre" />
                        <input type="hidden" value="<?php echo $fidelity->id_user?>" name="id_user" />
                    </div>
                    <button class="pure-input-1 pure-button pure-button-primary bg-blue fontinfoclient" > Remettre à zéro</button>

                </form>
            <?php }?>

            <?php if($offre=="tampon"  AND !empty($fidelity)){?>
                <span class="fontinfoclient">Coup de Tampons : <?php echo (!empty($fidelity->description)) ? $fidelity->description : ''?></span>
                <span class="fontinfoclient">Date de début : <?php  echo (!empty(convert_Sqldate_to_Frenchdate($fidelity->date_debut))) ? $fidelity->date_debut : ''?></span>
                <span class="fontinfoclient">Date de fin : <?php  echo (!empty(convert_Sqldate_to_Frenchdate($fidelity->date_fin))) ? $fidelity->date_fin : ''?></span>
                <span class="fontinfoclient">Montant : <?php  echo (!empty($fidelity->tampon_value)) ? $fidelity->tampon_value : ''?></span>

                <div class="pure-g bg-red">
                    <div class="<?php if (is_numeric($fidelity->solde_tampon) AND is_numeric($fidelity->tampon_value)) echo blue_class_progress($fidelity->solde_tampon, $fidelity->tampon_value)?> fontinfoclient">
                        <div class="pad-lr-5 fontinfoclient"><?php echo (!empty($fidelity->solde_tampon)) ? $fidelity->solde_tampon : 0?></div>
                    </div>
                    <div class="<?php if(is_numeric($fidelity->solde_tampon) AND is_numeric($fidelity->tampon_value)) echo red_class_progess($fidelity->solde_tampon, $fidelity->tampon_value)?> fontinfoclient">
                        <div class="pad-lr-5 fontinfoclient"><?php echo (!empty($fidelity->tampon_value)) ? $fidelity->tampon_value : 0?></div>
                    </div>
                </div>
                <form action="<?php echo site_url('front/fidelity_pro/reinistialisation')?>" class="pure-form pure-form-stacked no-padd" method="POST" id="form-actualisation">
                    <div class="input">
                        <input type="text" class="pure-input-1" placeholder="Noter le report à nouveau" name="montant_init">
                        <input type="hidden" value="<?php echo $fidelity->fiche_id?>" name="fiche_id" />
                        <input type="hidden" value="<?php echo $offre?>" name="type_offre" />
                        <input type="hidden" value="<?php echo $fidelity->id_user?>" name="id_user" />
                    </div>
                    <button class="pure-input-1 pure-button pure-button-primary bg-blue fontinfoclient" > Remettre à zéro</button>

                </form>
            <?php }?>
            <?php if($offre=="remise"  AND !empty($fidelity)){?>
                <div class="interlign">
                    <span class="fontinfoclient">Remise directe : <?php echo (!empty($fidelity->description)) ? $fidelity->description : ''?></span>
                    <span class="fontinfoclient">Date de début : <?php  echo (!empty(convert_Sqldate_to_Frenchdate($fidelity->date_debut))) ? $fidelity->date_debut : ''?></span>
                    <span class="fontinfoclient">Date de fin : <?php  echo (!empty(convert_Sqldate_to_Frenchdate($fidelity->date_fin))) ? $fidelity->date_fin : ''?></span>
                    <span class="fontinfoclient">Montant : <?php  echo (!empty($fidelity->montant)) ? $fidelity->montant : ''?></span>
                </div>
                <div class="pure-g bg-blue">
                    <div class="pure-u-1-2 bg-blue align-left">
                        <div class="pad-lr-5 fontinfoclient">Cumul consommation</div>
                    </div>
                    <div class="pure-u-1-2 bg-blue align-right">
                        <div class="pad-lr-5 fontinfoclient">
                            <b>
                                <?php echo (!empty($fidelity->solde_remise)) ? $fidelity->solde_remise : 0;?> &nbsp; &euro;
                            </b>
                        </div>
                    </div>
                </div>
                <form action="<?php echo site_url('front/fidelity_pro/reinistialisation')?>" class="pure-form pure-form-stacked no-padd" method="POST" id="form-actualisation">
                    <div class="input">
                        <input type="text" class="pure-input-1" placeholder="Noter le report à nouveau" name="montant_init">
                        <input type="hidden" value="<?php echo $fidelity->fiche_id?>" name="fiche_id" />
                        <input type="hidden" value="<?php echo $offre?>" name="type_offre" />
                        <input type="hidden" value="<?php echo $fidelity->id_user?>" name="id_user" />
                    </div>
                    <button class="pure-input-1 pure-button pure-button-primary bg-blue fontinfoclient" > Remettre à zéro</button>

                </form>
            <?php }?>

        </div>

        <?php if(!empty($list_activity)):?>
            <?php foreach ($list_activity as $capitalisation) :?>
                <?php if($client->param_captital_active && !empty($capitalisation->capital_value_added)):?>
                    <div class="product product-row fontinfoclient">
                        <?php echo date('d/m/Y',strtotime($capitalisation->date_use));?> : capitalisation <?php echo $capitalisation->capital_value_added?>&nbsp; &euro;
                    </div>
                <?php endif;?>
                <?php if($client->param_tampon_active && !empty($capitalisation->tampon_value_added)):?>
                    <div class="product product-row fontinfoclient">
                        <?php echo date('d/m/Y',strtotime($capitalisation->date_use));?> : coup de tampons <?php echo $capitalisation->tampon_value_added?>
                    </div>
                <?php endif;?>
                <?php if($client->param_remise_active && !empty($capitalisation->remise_value_added)):?>
                    <div class="product product-row fontinfoclient">
                        <?php echo date('d/m/Y',strtotime($capitalisation->date_use));?> : Remise <?php echo $capitalisation->remise_value_added?> &nbsp; &euro;
                    </div>
                <?php endif;?>
            <?php endforeach;?>
        <?php endif;?>

    </div>


    <div class="clr"></div>
</div>
<!--forms-->
</div>
</body>
</html>

