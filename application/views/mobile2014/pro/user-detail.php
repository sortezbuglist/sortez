<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-white">
    <!--forms-->
    <div class="wrap forms consommation">
      <form class="pure-form pure-form-stacked">
          <br/>
          <br/>
          <br/>
          <br/>
          <div class="input">
              <input type="text" class="pure-input-1 clearable " placeholder="" value="<?php echo $user->Nom?>"/>
          </div>

          <div class="input">
              <input type="text" class="pure-input-1 clearable " placeholder="Jean Louis" value="<?php echo $user->Prenom?>"/>
          </div>

          <div class="input">
              <input type="text" class="pure-input-1 clearable" placeholder="" value="<?php echo translate_date_to_fr($user->DateNaissance)?>"/>
          </div>

          <div class="input">
              <input type="text" class="pure-input-1 clearable" placeholder="" value="<?php echo $user->Adresse?>"/>
          </div>

          <div class="input">
              <input type="text" class="pure-input-1 clearable" placeholder="" value="<?php echo $user->NomVille?>"/>
          </div>

          <div class="input">
              <input type="text" class="pure-input-1 clearable" placeholder="" value="<?php echo $user->Telephone?>"/>
          </div>
          <div class="input">
              <input type="email" class="pure-input-1 clearable" placeholder="" value="<?php echo $user->Email?>"/>
          </div>


      </form>
    </div>
    <!--forms-->
    <div class="clr"></div>
</div>
</body>
</html>