<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="pad-tpp-25"></div>
    <div class="wrap forms  links">
      <div class="pure-form pure-form-stacked">
      	 <?php if($bonplanshow):?>
          <div class="pad-tb-5">
              <a href="<?php echo site_url('front/fidelity_pro/fidelity_card_operations/'.$IdUser.'/bonplan/'.$id_client_bonplan)?>"class="pure-button pure-button-primary" id="bonplan">Bon plan </a>
          </div>
          <?php endif;?>
          <?php if($offre_active):?>
	      <div class="pad-tb-5">
	           <a href="<?php echo site_url('front/fidelity_pro/fidelity_card_operations/'.$IdUser)?>" class="pure-button pure-button-primary"  id="tampon"><?php echo $offre_active?></a>
	      </div>
	      <?php endif;?>
          <div class="pad-tb-5">
              <a href="<?php echo site_url('front/fidelity_pro/validation_operation_command/'.$IdUser)?>" class="pure-button pure-button-primary"  id="tampon">Commande en ligne</a>
          </div>
      </div>
    </div>
    <div class="clr"></div>
</div>
</body>
</html>