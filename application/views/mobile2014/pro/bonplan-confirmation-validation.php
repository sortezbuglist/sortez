<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="wrap forms ">
      <form class="pure-form pure-form-stacked">
          <div class="text-box  f-size16">
                <div class="align-center titre red no-padd">
                    Ce bon plan est  validé
                </div>
              <p>Un email de remerciement est adressé <br/>
              directement au consommateur</p>
              <div class="pure-g">
                  <div class="pure-u-1-2">
                      <div class="pure-g">
                          <div class="pure-u-1-8"></div>
                          <div class="pure-u-1-2 align-left f-size11 v-middle">
                              Solde <br/>disponible <br/>à ce jours
                          </div>
                          <div class="pure-u-1-3">
                              <div class="bg-blue solde2 pad-tb-5">
								<?php if ($oBonplan->bonplan_type == "1") echo (isset($oBonplan->bonplan_quantite)) ? $oBonplan->bonplan_quantite : 0; ?>
                                <?php if ($oBonplan->bonplan_type == "2") echo (isset($oBonplan->bp_unique_qttprop)) ? $oBonplan->bp_unique_qttprop : 0; ?>
                                <?php if ($oBonplan->bonplan_type == "3") echo (isset($oBonplan->bp_multiple_qttprop)) ? $oBonplan->bp_multiple_qttprop : 0; ?>
                              </div>
                          </div>

                      </div>
                  </div>
                  <div class="pure-u-1-2">
                      <div class="pure-g">
                          <div class="pure-u-1-8"></div>
                          <div class="pure-u-1-2 align-left f-size11 v-middle">
                              Nouveau solde <br/> reservation <br/>à ce jours
                          </div>
                          <div class="pure-u-1-3">
                              <div class="bg-red solde2 pad-tb-5">
                                <?php if (isset($objReservationNonValideCom)) echo strval(count($objReservationNonValideCom)); ?>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>


          </div>
            <p></p>
            <!--  
          <div class="pad-tb-10 accordeon">
              <button class="pure-button pure-button-primary" > Historique de sa consommation</button>
          </div>
          -->
		  <div class="pad-tb-5 accordeon">
              <a class="pure-button pure-button-primary" href="<?php site_url('/front/fidelity/fidelity_card_operations/'.$id_user)?>">Retour menu avantage</a>
          </div>

      </form>
    </div>
    <!--forms-->
    <div class="clr"></div>
</div>
<script type="text/javascript">
<!--
$('#retour_avantage').on('click',function(e){
e.preventDefault();
	document.location.href = '<?php echo site_url('front/fidelity/fidelity_card_operations/'.$id_user.'/bonplan')?>';
});
//-->
</script>
<?php $this->load->view('mobile2014/partial/footer')?>