<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--liens-->
    <div class="wrap links">
    <!--  
        <div class="pad-tb-5">
            <a href="#" class="pure-button pure-button-primary">Retour au menu général</a>
        </div>
        -->
        <div class="pad-tb-5">
            <a href="<?php echo site_url('front/fidelity_pro/card_scan'); ?>" class="pure-button pure-button-primary">Valider une visite</a>
        </div>
<!--        <div class="pad-tb-5">-->
<!--            <a href="--><?php //echo site_url('front/fidelity_pro/card_scan_command'); ?><!--" class="pure-button pure-button-primary">Valider une commande</a>-->
<!--        </div>-->
        <div class="pad-tb-5">
            <a href="<?php echo site_url('front/fidelity_pro/liste_reservation')?>" class="pure-button pure-button-primary">Les réservations en cours</a>
        </div>
        <div class="pad-tb-5">
            <a href="<?php echo site_url('front/fidelity_pro/all_command')?>" class="pure-button pure-button-primary">Mes Commandes en cours</a>
        </div>
        <div class="pad-tb-5">
            <a href="<?php echo site_url('front/fidelity_pro/liste_clients')?>" class="pure-button pure-button-primary">Liste de mes clients</a>
        </div>
        <div class="pad-tb-5">
            <a href="<?php echo site_url('front/fidelity/mes_parametres');?>" class="pure-button pure-button-primary">Mes paramètres</a>
        </div>
        <div class="pad-tb-5">
            <a href="<?php echo site_url('front/fidelity_pro/delivrance_carte');?>" class="pure-button pure-button-primary">Délivance d'une carte </a>
        </div>
        <div class="pad-tb-5">
            <a href="<?php echo site_url('auth/logout');?>" class="pure-button pure-button-primary">Déconnexion</a>
        </div>

    </div>
    <!--liens-->
    <div class="clr"></div>
</div>
<?php $this->load->view('mobile2014/partial/footer')?>