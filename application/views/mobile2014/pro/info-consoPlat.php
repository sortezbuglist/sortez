<?php //var_dump($infoconso);?>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>

<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
    <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        .bouton{
            height: 40px;
            line-height: 2.5;
            color: white!important;
            font-size: 17px;
            cursor: pointer;
        }
        .bg-pink{
            background-color: rgb(208, 0, 118);
        }
        .fontinfoclient{
            font-size: 15px!important;
        }
        .interlign{
            /*Line-Height: 8pt;*/
            line-height: 10px;
        }
    </style>
</div>
<?php //var_dump($infoconso); ?>
<div class="wrapper page-white">
    <?php if(!empty($conso)){?>
    <span style="font-size: 20px!important;"><strong><?php echo $conso[0]->Nom.' '.$conso[0]->Prenom?></strong></span>
    <table border="0" width="100%">
        <tr>
            <td width="60"><div class=""><a href="<?php echo site_url('front/fidelity_pro/user/'.$conso[0]->IdUser);?>"><img src="<?php echo img_url("id_card.png") ?>" alt="" width="50"/></a></div></td>
            <td width="60"><div class="pad-lr-5"><a href="mailto:<?php echo $conso[0]->Email;?>"><img src="<?php echo img_url("email.png") ?>" alt="" width="50"/></a></div></td>
            <td><div class="pad-lr-5"><a href="tel:<?php echo $conso[0]->Telephone;?>"><img src="<?php echo img_url("mobile_phone.png") ?>" alt="" width="50"/></a></div></td>
        </tr>
    </table>
    <?php } ?>
    <?php if(!empty($conso)){?>
        <?php foreach ($conso as $infoconso) ?>
    <!--forms-->
    <div class="consommation">
        <div class="product first">
            <div class="details pad-tb-10">
                <div class="txt-product">
                    <div class="interlign">
                        <table>
                            <tr>
                                 <td width="300"><span class="fontinfoclient">Nom Plat : <?php echo (!empty($infoconso->description_plat)) ? $infoconso->description_plat : ''?></span></td>
                                 <td width="50"><a href="<?php echo site_url('front/fidelity_pro/delete_res/'.$infoconso->idRes)?>"><img style="width: 60px" src="<?php echo base_url('assets/img/delete.jpg')?>"></a></td>
                            </tr>
                        </table>
                        <span class="fontinfoclient">Date de début : <?php if ($infoconso->date_debut_plat !=null AND $infoconso->date_debut_plat !='') echo  convert_Sqldate_to_Frenchdate($infoconso->date_debut_plat);?></span>
                        <span class="fontinfoclient">Date de fin : <?php  if ($infoconso->date_debut_plat !=null AND $infoconso->date_debut_plat !='') echo  convert_Sqldate_to_Frenchdate($infoconso->date_fin_plat) ;?></span>
                        <span class="fontinfoclient">Validé le : <?php  if (isset($infoconso->date_validation) AND $infoconso->date_validation !=null AND $infoconso->date_validation !='') echo  convert_Sqldate_to_Frenchdate($infoconso->date_validation) ;?></span>
                        <span class="fontinfoclient">Nbre pris : <?php  if (isset($infoconso->nbre_platDuJour)) echo $infoconso->nbre_platDuJour;?></span>
                        <span class="fontinfoclient">Nbre pers : <?php if (isset($infoconso->nbre_pers_reserved)) echo  $infoconso->nbre_pers_reserved ;?></span>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="clr"></div>
    <?php }else{?>
        <h4>Pas de donnée</h4>
    <?php }?>
</div>
<!--forms-->
</div>
</body>
</html>