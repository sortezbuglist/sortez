<script type="text/javascript" src="<?php echo base_url();?>assets/js/aviary.editor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#delete_remise_photo").click(function(){
			var idcom = jQuery("#idcom").val();
			$.ajax({
			type: "POST",
			url: '<?php echo site_url("front/fidelity_pro/delete_remise_photo"); ?>',
			data: 'idcommercant=' + idcom,
			success: function (zReponse) {
				console.log(zReponse);
				if(zReponse=="error") {
					alert("Une erreur est survenue ! Veuillez re-essayer !");
				} else if(zReponse=="ok"){
					alert("Image supprimer avec succèss");	
					location.reload(true);					
				}

			},
			error: function (zReponse) {
				console.log(zReponse);
			}
		});
		});		
	});	
</script>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="wrap forms ">
      <span class="error"><?php echo (isset($message_error)) ? $message_error : ''?></span>
      <form class="pure-form pure-form-stacked" action="<?php echo site_url('front/fidelity_pro/editremise'); ?>" method="post" name="frmremise" id="frmremise">
              <br/>
           <div class="pure-u-1">
	          <div class="input inputdate-form pad-5">
               <label for="module_tampon" class="module">Activation du module
	              <input type="checkbox" class="checkbox" name="remise[is_activ]" value="1" id="module_tampon" <?php echo (isset($oRemise)  && is_object($oRemise) && $oRemise->is_activ) ? "checked" : ""?>/> </label>
	            </div>  
          </div>
          <div class="pure-g">
              <div class="pure-u-7-8">
                  <div class="input inputdate-form">
                      <label for="">Montant remise : </label>
                      <input type="text" class="pure-input-1" placeholder="" name="remise[montant]" value="<?php if (isset($oRemise) && is_object($oRemise)) echo $oRemise->montant;?>"/>
                  </div>
              </div>
              <div class="pure-u-1-8"><div class="unit">
              	<select class="pure-input-1" name="remise[value_type]"/>
                    <option value="1" <?php if (isset($oRemise) && is_object($oRemise) && $oRemise->value_type=='1') { ?>selected="selected"<?php } ?>>%</option>
                    <option value="2" <?php if (isset($oRemise) && is_object($oRemise) && $oRemise->value_type=='2') { ?>selected="selected"<?php } ?>>&euro;</option>
                </select>
                </div></div>
          </div>
          <textarea class="textarea-box" name="remise[description]" id="description" cols="30" rows="5"><?php if (isset($oRemise) && is_object($oRemise)) echo $oRemise->description;?></textarea>
          <div class="pure-u-1">
	          <div class="input inputdate-form">
	              <label for="">Date debut:</label>
	              <input type="text" class="pure-input-1" name="remise[date_debut]" id="date_debut" value="<?php echo (isset($oRemise) && is_object($oRemise)) ? convert_Sqldate_to_Frenchdate($oRemise->date_debut) : 'cliquez ici';?>"/>
	          </div>
          </div>
          <div class="pure-u-1">
	          <div class="input inputdate-form">
	              <label for="">Date fin:</label>
	              <input type="text" class="pure-input-1" name="remise[date_fin]" id="date_fin" value="<?php echo (isset($oRemise) && is_object($oRemise)) ? convert_Sqldate_to_Frenchdate($oRemise->date_fin): 'cliquez ici';?>"/>
	          </div>
          </div>
          <!-- Ajout d'image -->
		  <div class="pure-u-1">
			<div class="input inputdate-form pad-5">
				<div id="Articlebackground_image_container">

					<?php if (isset($oRemise->image1) && $oRemise->image1!= null ) { ?>
						<input type="hidden" id="idcom" value="<?php echo $oRemise->id_commercant; ?>">
						<img src="<?php echo('/application/resources/front/photoCommercant/imagesbank/'.$oRemise->id_ionauth.'/remise_photo/'.$oRemise->image1) ?>">
                        <a href="javascript:void(0);" class="btn btn-danger" onclick="return false;" id="delete_remise_photo"><button>Supprimer</button></a>
    				<?php }else {?>
                        <a href="javascript:void(0);" title="Photo1" class="btn btn-info" onclick="javascript:window.open('<?php echo site_url('media/index/'.$oRemise->id_commercant.'-remise_photo-image1') ?>','Commercant image','width=1045, height=725, scrollbars=yes');" id="Articlebackground_link"><button>Ajouter une image</button>
    					</a>
					<?php  }?>
				</div>
			</div>
		  </div>
		  <!-- Fin -->

          <div class="pad-tb-5 accordeon">
              <button class="pure-button pure-button-primary" > Validation </button>
          </div>
			<input name="remise[id]" id="id" type="hidden" value="<?php echo (isset($oRemise) && is_object($oRemise)) ? $oRemise->id : 0;?>" />

      </form>
      <script type="text/javascript">
<!--
$(function() {
	$("#date_debut").datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:2020',
		onSelect: function(dateText) {
			$( "#date_debut" ).valid();
		}
	});
	$("#date_fin").datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:2020',
		onSelect: function(dateText) {
			$( "#date_fin" ).valid();
		}
	});
	$( "#frmremise" ).validate({
		//debug: true,
		rules: {
			'remise[description]' : {
				required: true
			},
			'remise[date_fin]': {
				required: true
			},
			'remise[date_debut]': {
				required: true
			},
			'remise[montant]': {
				required: true
			}
		}
	});
});
// -->
</script>
    </div>
    <!--forms-->
    <div class="clr"></div>
</div>
<?php $this->load->view('mobile2014/partial/footer')?>