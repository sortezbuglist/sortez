<?php
$this->load->view('mobile2014/partial/head-pro')?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<?php if(!$print):?>
    <?php $this->load->view('mobile2014/partial/menu_pro')?>
<?php endif;?>
<style type="text/css">
    .btn-menu-2{
        background-color: rgb(207, 21, 144);
    }
</style>
<div class="row">
    <div class="col-12 pt-3 pb-3 text-center">
        <h3>Detail du commande:</h3>
    </div>
</div>
<div class="wrapper page-white">
    <div class="consommation">
        <?php if(!empty($details)):?>
            <?php if (isset($commande) AND !empty($commande)){ ?>
                <div class="row">

                    <div class="col-4 font-weight-bold">Type de commande: </div>
                    <div class="col-8 "><?php echo $commande->type_command; ?></div>
                    <div class="col-4 font-weight-bold">Type de livraison: </div>
                    <div class="col-8 "><?php echo $commande->type_livraison; ?></div>
                    <div class="col-4 font-weight-bold">Jour souhaitée: </div>
                    <div class="col-8 "><?php echo translate_date_to_fr($commande->jour_enlev); ?></div>
                    <div class="col-4 font-weight-bold">heure souhaitée: </div>
                    <div class="col-8 "><?php echo $commande->heure_enleve; ?></div>
                    <div class="col-4 font-weight-bold">Type de paiement:</div>
                    <div class="col-8 "><?php echo $commande->type_payement; ?></div>
                    <div class="col-4 font-weight-bold">Prix total:</div>
                    <div class="col-8 "><?php echo "€".$commande->total_price; ?></div>
                    <div class="col-4 font-weight-bold">Question/Commentaires:</div>
                    <div class="col-8 "><?php echo $commande->comment; ?></div>
                    <?php if ($commande->remise_promotion_price !=0 && $commande->remise_promotion_price !=null ){?>
                    <div class="col-4 font-weight-bold">Remise promotionnelle:</div>
                    <div class="col-8 "><?php echo $commande->comment; ?></div>
                    <?php } ?>
                    <?php if ($commande->command_files !=null && is_file("application/resources/front/command_clients/".$IdUser."/".$commande->command_files)){  ?>
                    <div class="col-12 font-weight-bold">
                        La pièce jointe est téléchargeable via ce <a
                                href="<?php echo base_url() ?>application/resources/front/command_clients/<?php echo $IdUser; ?>/<?php echo $commande->IdUser; ?>">lien</a>
                    </div>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php
            $thiss = &get_instance();
            $thiss->load->model('Mdl_soutenons');
            ?>
            <?php foreach ($details as $detail):?>
            <?php
              $product = $thiss->Mdl_soutenons->get_product_by_id_comm($detail->id_produit);
            ?>
                <div class="mt-3 border-bottom mb-3">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td class="font-weight-bold border" width="50%" v-align="top">Nom du produit:</td>
                                <td class="font-weight-bold border" width="50%" v-align="top">Quantité:</td>
                                <td class="font-weight-bold border" width="50%" v-align="top">Prix unitaire:</td>
                            </tr>
                            <tr>
                                <td class="p-2 border" width="50%" ><?php echo $product->titre; ?></td>
                                <td class="p-2 border" width="50%" ><?php echo $detail->nbre; ?></td>
                                <td class="p-2  border" width="50%" ><?php echo "€".$product->prix; ?></td>
                            </tr>
                        </table>
                </div>
            <?php endforeach;?>
        <?php else:?>
            <h4>Vous n'avez pas de commande</h4>
        <?php endif;?>
    </div>
</div>
<?php if(!$print):?>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".btnPrint").printPage();
        });
    </script>
<?php endif;?>
</body>
</html>