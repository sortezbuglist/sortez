<?php //echo "<pre>";print_r($list_client); ?>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>

<div class="wrapper head">
    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
    <!--LOGO-->
</div>
<div class="wrapper page-white">
    <!--forms-->
    <div class="consommation">
        <?php if(!empty($list_client)) { ?>
            <?php foreach ($list_client as $client){
                if($client->date_validation != '0000-00-00') { ?>
                    <?php if($client->valide) { ?>
                        <div class="product first id<?php echo $client->id_client ?>" >

                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="details f-size14 pad-10 no-marg">
                                            <?php echo $client->Prenom.' '.$client->Nom;?>
                                        </div>
                                    </td>
                                    <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity_pro/detail_client/'.$client->IdUser)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
                                </tr>
                            </table>
                            <div class="pure-g bg-blue">
                                <div class="pure-u-1-2 bg-blue align-left">
                                    <div class="pad-lr-5">Cumul consommation</div>
                                </div>
                                <div class="pure-u-1-2 bg-blue align-right"><div class="pad-lr-5"><b><?php echo (!empty($client->solde_capital)) ? $client->solde_capital : 0?> &euro;</b></div></div>
                            </div>

                            <div class="pure-g bg-red marg-bt-10">
                                <div class="pure-u-1-2 bg-red  align-left ">
                                    <div class="pad-lr-5"><?php echo  date("d/m/Y",strtotime($client->date_validation));?></div>
                                </div>
                                <div class="pure-u-1-2 bg-red  align-right"><div class="pad-lr-5"><b>BON PLAN VALIDE</b></div></div>
                            </div>
                        </div>
                    <?php }?>
                    <?php if($client->valide == 0) { ?>
                        <div class="product first id<?php echo $client->id_client ?>">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="width:100%; display:table; float:right;text-align:left;">
                                <tr>
                                    <td style="padding-left:10px; color:#000000 !important;">Bonplan : <?php echo $mdlbonplan->getById($client->bonplan_id)->bonplan_titre; ?></td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px; color:#000000 !important;">
                                        <?php
                                        $date_visite_client_check = $assoc_client_bonplan_model->getById($client->id)->date_visit;
                                        if ($date_visite_client_check != "0000-00-00" AND $date_visite_client_check !='' AND $date_visite_client_check !=null ) echo "Date de visite pr&eacute;vue par le client : ".convert_Sqldate_to_Frenchdate($date_visite_client_check);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px; color:#000000 !important;">
                                        Nombre de place r&eacute;serv&eacute;e par le client :
                                        <?php
                                        $nb_place_check = $assoc_client_bonplan_model->getById($client->id)->nb_place;
                                        if (isset($nb_place_check)) echo $nb_place_check;  else echo "1";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left:10px; color:#000000 !important;">
                                        <?php
                                        $bonplan_client_check = $mdlbonplan->getById($client->bonplan_id);
                                        if ($bonplan_client_check->bonplan_type == '2' && $bonplan_client_check->bp_unique_date_fin != "0000-00-00") echo "Date d'expiration du bonplan : ".convert_Sqldate_to_Frenchdate($bonplan_client_check->bp_unique_date_fin);
                                        if ($bonplan_client_check->bonplan_type == '3' && $bonplan_client_check->bp_multiple_date_fin != "0000-00-00") echo "Date d'expiration du bonplan : ".convert_Sqldate_to_Frenchdate($bonplan_client_check->bp_multiple_date_fin);
                                        ?>
                                    </td>
                                </tr>
                            </table>

                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td>
                                        <div class="details f-size14 pad-10 no-marg">
                                            <b><?php echo $client->Prenom.' '.$client->Nom;?></b>
                                        </div>
                                    </td>
                                    <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity_pro/fidelity_card_operations/'.$client->IdUser.'/bonplan/'.$client->id)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
                                </tr>
                            </table>
                            <table border="0" width="100%">
                                <tr>
                                    <td width="40"><div class="pad-lr-5"><a href="<?php echo site_url('front/fidelity_pro/user/'.$client->IdUser);?>"><img src="<?php echo img_url("id_card.png") ?>" alt="" width="30"/></a></div></td>
                                    <td width="40"><div class="pad-lr-5"><a href="mailto:<?php echo $client->Email;?>"><img src="<?php echo img_url("email.png") ?>" alt="" width="30"/></a></div></td>
                                    <td><div class="pad-lr-5"><a href="tel:<?php echo $client->Telephone;?>"><img src="<?php echo img_url("mobile_phone.png") ?>" alt="" width="30"/></a></div></td>
                                </tr>
                            </table>

                            <div class="pure-g bg-red marg-bt-10">
                                <div class="pure-u-1-3 bg-red  align-left ">
                                    <div class="pad-lr-5"><?php if ($client->datetime_validation !="" AND $client->datetime_validation !=null AND $client->datetime_validation !="0000-00-00 00:00:00") {echo  convert_datetime_to_fr($client->datetime_validation);}else{echo convert_Sqldate_to_Frenchdate($client->date_validation); }?></div>
                                </div>
                                <div class="pure-u-2-3 bg-red  align-right"><div class="pad-lr-5"><b>BON PLAN PAS ENCORE VALID&Eacute;</b></div></div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php }?>
        <?php }else { ?>
            <div style="padding-top: 25px" class="product first pt">
                Aucun enregistrement bonplan trouvé
            </div>
        <?php  } ?>

        <?php if(!empty($list_plat)){?>
            <?php foreach ($list_plat as $clients_plat){ ?>
                    <div class="padded">
                        <div style="margin-bottom: 15px;padding-left: 18px;">
                            <span style="margin-top: 10px;font-size:14px;font-weight: 700; ">Plat du jour</span>
                        </div>
                        <table style="border: none!important">
                            <tr>
                                <td width="50" style="padding-left:18px; color:#000000 !important;">
                                    <a href="<?php echo site_url('front/fidelity_pro/user/'.$clients_plat->IdUser);?>"><img src="<?php echo img_url("id_card.png") ?>" alt="" width="30"/></a></td>
                                <td width="50" style="padding-left:18px; color:#000000 !important;">
                                    <a href="mailto:<?php echo $clients_plat->Email;?>"><img src="<?php echo img_url("email.png") ?>" alt="" width="30"/></a></td>
                                <td width="50" style="padding-left:18px; color:#000000 !important;">
                                    <a href="tel:<?php echo $clients_plat->Telephone;?>"><img src="<?php echo img_url("mobile_phone.png") ?>" alt="" width="30"/></a></td>
                            </tr>
                        </table>
                        <table  class="bordered"  width="100%" border="0" cellspacing="0" cellpadding="0" style="width:100%; display:table; float:right;text-align:left;">
                            <tr>
                                <td style="padding-left:18px; color:#000000 !important;">
                                    <?php
                                    if (isset($clients_plat->Nom) && $clients_plat->Nom != "") echo "Client : ".$clients_plat->Nom. ' '.$clients_plat->Prenom;
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:18px; color:#000000 !important;">Nom du Plat : <?php echo $clients_plat->description_plat; ?></td>
                                <td width="10%" ><a onclick="return confirm('valider ce réservation?')" href="<?php echo site_url('front/fidelity_pro/valid_plat/'.$clients_plat->idRes.'/'.$clients_plat->IdCommercant.'/'.$clients_plat->IdUser)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
                            </tr>
                            <tr>
                                <td style="padding-left:18px; color:#000000 !important;">
                                    <?php
                                    $date_visite_client_check = $clients_plat->date_reservation;
                                    if ($date_visite_client_check != "0000-00-00" AND $date_visite_client_check !='' AND $date_visite_client_check !=null ) echo "Date de visite pr&eacute;vue par le client : ".convert_Sqldate_to_Frenchdate($date_visite_client_check);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:18px; color:#000000 !important;">
                                    Nombre de plat r&eacute;serv&eacute;e par le client :
                                    <?php
                                    if (isset($clients_plat->nbre_platDuJour)) echo $clients_plat->nbre_platDuJour;  else echo "1";
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:18px; color:#000000 !important;">
                                    <?php
                                    if (isset($clients_plat->date_fin_plat) && $clients_plat->date_fin_plat != "0000-00-00") echo "Date d'expiration du plat : ".convert_Sqldate_to_Frenchdate($clients_plat->date_fin_plat);
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left:18px; color:#000000 !important;">
                                    <?php
                                    if (isset($clients_plat->heure_reservation) && $clients_plat->heure_reservation != "") echo "Heure de réservation : ".$clients_plat->heure_reservation;
                                    ?>
                                </td>
                             </tr>
                        </table>
                    </div>
        <?php  } ?>
        <?php  }else{ ?>
                <div style="padding-top: 25px" class="product first pt">
                    Aucun reservation de plat trouvé
                </div>
        <?php  } ?>


        <?php if(!empty($liste_table)){?>
            <?php foreach ($liste_table as $clients_table){ ?>
                <div class="padded">
                    <table  class="bordered"  width="100%" border="0" cellspacing="0" cellpadding="0" style="width:100%; display:table; float:right;text-align:left;">
                        <tr >
                            <td style="padding-top: 15px;padding-bottom: 15px;padding-left: 18px">
                                <h4>Réservation table</h4>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">Client :  <?php echo $clients_table->prenom.' '.$clients_table->Nom;?></td>
                            <td width="10%" v-align="top"><a onclick="return confirm('Retirer ce réservation?')" href="<?php echo site_url('front/fidelity_pro/valid_table/'.$clients_table->idRes)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                $date_res_client_check = $clients_table->date_de_reservation;
                                if ($date_res_client_check != "0000-00-00" AND $date_res_client_check !='' AND $date_res_client_check !=null ) echo "Reservé le : ".convert_Sqldate_to_Frenchdate($date_res_client_check);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                $date_visite_client_check = $clients_table->date_res;
                                if ($date_visite_client_check != "0000-00-00" AND $date_visite_client_check !='' AND $date_visite_client_check !=null ) echo "Date de visite : ".convert_Sqldate_to_Frenchdate($date_visite_client_check);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                Nombre de place r&eacute;serv&eacute;e par le client :
                                <?php
                                if (isset($clients_table->nbre_adulte)) echo $clients_table->nbre_adulte." Adulte(s), et "; if (isset($clients_table->nbre_enfant)) echo $clients_table->nbre_enfant." Enfant(s)";  else echo "1";;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_table->heure_midi) && $clients_table->heure_midi != "") echo "Heure prévue (midi) : ".$clients_table->heure_midi;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_table->heure_soir) && $clients_table->heure_soir != "") echo "Heure prévue (soir) : ".$clients_table->heure_soir;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_table->date_fin_plat) && $clients_table->date_fin_plat != "") echo "Mail: ".$clients_table->mail;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_table->tel) && $clients_table->tel != "") echo "Téléphone : ".$clients_table->tel;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_table->message_client) && $clients_table->message_client != "") echo "Message : ".$clients_table->message_client;
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            <?php  } ?>
        <?php  }else{ ?>
            <div style="padding-top: 25px" class="product first pt">
                Aucun reservation Table trouvé
            </div>
        <?php  } ?>






        <?php if(!empty($liste_sejour)){?>
            <?php foreach ($liste_sejour as $clients_sejour){ ?>
                <div class="padded">
                    <table class="bordered" width="100%" border="0" cellspacing="0" cellpadding="0" style="width:100%; display:table; float:right;text-align:left;">
                        <tr>
                            <td style="padding-top: 15px;padding-bottom: 15px;padding-left: 18px">
                                <h4>Réservation séjour</h4>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">Client :  <?php echo $clients_sejour->prenom.' '.$clients_sejour->Nom;?></td>
                            <td width="10%" v-align="top"><a onclick="return confirm('Retirer ce réservation?')" href="<?php echo site_url('front/fidelity_pro/valid_sejour/'.$clients_sejour->idRes)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                $date_res_client_check = $clients_sejour->date_de_reservation;
                                if ($date_res_client_check != "0000-00-00" AND $date_res_client_check !='' AND $date_res_client_check !=null ) echo "Reservé le : ".convert_Sqldate_to_Frenchdate($date_res_client_check);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                $date_visite_client_check = $clients_sejour->date_debut_res;
                                if ($date_visite_client_check != "0000-00-00" AND $date_visite_client_check !='' AND $date_visite_client_check !=null ) echo "Date d' arrivée : ".convert_Sqldate_to_Frenchdate($date_visite_client_check);
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                 <?php
                                $date_fin_res= $clients_sejour->date_fin_res;
                                 if ($date_fin_res != "0000-00-00" AND $date_fin_res !='' AND $date_fin_res !=null ) echo "Date de depart : ".convert_Sqldate_to_Frenchdate($date_fin_res);
                                 ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                Nombre de pers r&eacute;serv&eacute;e :
                                <?php
                                if (isset($clients_sejour->nbre_adulte)) echo $clients_sejour->nbre_adulte." Adulte(s), et "; if (isset($clients_sejour->nbre_enfant)) echo $clients_sejour->nbre_enfant." Enfant(s)";  else echo "1";;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_sejour->heure_midi) && $clients_sejour->heure_midi != "") echo "Heure prévue (midi) : ".$clients_sejour->heure_midi;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_sejour->heure_soir) && $clients_sejour->heure_soir != "") echo "Heure prévue (soir) : ".$clients_sejour->heure_soir;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_sejour->date_fin_plat) && $clients_sejour->date_fin_plat != "") echo "Mail: ".$clients_sejour->mail;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_sejour->tel) && $clients_sejour->tel != "") echo "Téléphone : ".$clients_sejour->tel;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:18px; color:#000000 !important;">
                                <?php
                                if (isset($clients_sejour->message_client) && $clients_sejour->message_client != "") echo "Message : ".$clients_sejour->message_client;
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            <?php  } ?>
        <?php  }else{ ?>
            <div style="padding-top: 25px" class="product first pt">
                Aucun reservation Sejour trouvé
            </div>
        <?php  } ?>




    </div>
    <div class="clr"></div>
</div>
<style type="text/css">
    td{
       font-size: 14px;
    }
    .padded{
        margin-top: 25px;
        margin-bottom: 15px;
    }
    .bordered{
        border-bottom: solid;
        margin-top: 10px;
        padding-top: 10px;

    }
</style>
    <!--forms-->
</body>
</html>