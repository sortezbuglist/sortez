<?php //echo "<pre>";print_r($list_client); ?>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>

    <div class="wrap">
        <div class="pure-g">
            <div class="pure-u-3-5">
               <!--  
                <form action="">
                <div class="search align-center">
                    <input type="text" />
                    <button class="submit" type="submit"><span class="fa fa-check"></span></button>
                </div>
                </form>
                -->
            </div>
            <div class="pure-u-2-5">
                <div class="btn-menu pad-10">
                    <a href="<?php echo site_url('front/fidelity/filtre_client')?>">Filtrer</a>
                </div>
            </div>
        </div>
        <div class="pure-g">
            <div class="pure-u-1-3">
                <div class="btn-menu-2">
                    <a href="#" class="radius-brtr-0">Imprimez</a>
                </div>
            </div>
            <div class="pure-u-1-3">
                <div class="btn-menu-2">
                    <a href="#" class="radius-all-0">Exp. contacts</a>
                </div>
            </div>
            <div class="pure-u-1-3">
                <div class="btn-menu-2">
                    <a href="#" class="radius-bltl-0">Export CSV</a>
                </div>
            </div>
        </div>
    </div>
    <!--LOGO-->
</div>
<div class="wrapper page-white">
    <a href="#" class="link-rose">Adresser un message direct <br/> à cette séléction</a>
    <!--forms-->
    <div class="consommation">
    	<?php if(!empty($list_client)):?>
    		<?php foreach ($list_client as $client):?>
		        <div class="product first">
		                <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                    <tr>
		                        <td>
		                            <div class="details f-size14 pad-10 no-marg">
		                            	<?php echo $client->Prenom.' '.$client->Nom;?>
		                            </div>
		                        </td>
		                        <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity/detail_client/'.$client->IdUser)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
		                    </tr>
		                </table>
		
		            <div class="pure-g bg-blue">
		                <div class="pure-u-1-2 bg-blue align-left">
		                    <div class="pad-lr-5">Cumul consommation</div>
		                </div>
		                <div class="pure-u-1-2 bg-blue align-right"><div class="pad-lr-5"><b><?php echo (!empty($client->solde_capital)) ? $client->solde_capital : 0?> &euro;</b></div></div>
		            </div>
		
		            <div class="pure-g bg-red marg-bt-10">
		                <div class="pure-u-1-2 bg-red  align-left ">
		                    <div class="pad-lr-5"><?php echo  date("d/m/Y",strtotime($client->date_validation));?></div>
		                </div>
		                <div class="pure-u-1-2 bg-red  align-right"><div class="pad-lr-5"><b>BON PLAN VALIDE</b></div></div>
		            </div>
		        </div>
		       <?php endforeach;?> 
	      <?php endif?>  
    </div>
    <div class="clr"></div>
</div>
    <!--forms-->
</div>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
</body>
</html>