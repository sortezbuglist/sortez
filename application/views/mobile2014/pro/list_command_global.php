<?php
$this->load->view('mobile2014/partial/head-pro')?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<?php if(!$print):?>
    <?php $this->load->view('mobile2014/partial/menu_pro')?>
<?php endif;?>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" />
<style type="text/css">
    .btn-menu-2{
        background-color: rgb(207, 21, 144);
    }
</style>
<div class="row">
    <div class="col-12 pt-3 pb-3 text-center">
        <h3>Liste de tous les commandes:</h3>
    </div>
</div>
<?php
$thisss = &get_instance();
$thisss->load->model('User');
?>
<div class="wrapper page-white">
    <div class="consommation">
        <div class="row">
            <div class="col-12 pt-3 pb-3 text-center">
                <h5>Filtrer par Etat:</h5>
                <form id="filter_command" action="" method="post">
                    <select onchange="change_etat_command()" id="etat_commande" name="etat_commande" class="form-control w-50 m-auto">
                        <option <?php if (isset($filter) && $filter == '') echo  'selected'; ?> class="all_col" value="Tout">Tout</option>
                        <option <?php if (isset($filter) && $filter == 'Nouvelle Commande') echo  'selected'; ?> class="new_col" value="Nouvelle Commande">Nouvelle Commande</option>
                        <option <?php if (isset($filter) && $filter == 'En attente du paiement') echo  'selected'; ?> class="primary_col" value="En attente du paiement">En attente du paiement</option>
                        <option <?php if (isset($filter) && $filter == 'Paiement accepté') echo  'selected'; ?> class="success_col" value="Paiement accepté">Paiement accepté</option>
                        <option <?php if (isset($filter) && $filter == 'En cours de préparation') echo  'selected'; ?> class="warn_col" value="En cours de préparation">En cours de préparation</option>
                        <option <?php if (isset($filter) && $filter == 'Expédié') echo  'selected'; ?> class="grenat_col" value="Expédié">Expédié</option>
                        <option <?php if (isset($filter) && $filter == 'Livré') echo  'selected'; ?> class="green_fort_col" value="Livré">Livré</option>
                        <option <?php if (isset($filter) && $filter == 'Annulé') echo  'selected'; ?> class="danger_col" value="Annulé">Annulé</option>
                        <option <?php if (isset($filter) && $filter == 'Remboursé') echo  'selected'; ?> class="rembours_col" value="Remboursé">Remboursé</option>
                        <option <?php if (isset($filter) && $filter == 'Erreur de paiement') echo  'selected'; ?> class="paiement_error_col" value="Erreur de paiement">Erreur de paiement</option>
                    </select>
                </form>
            </div>
        </div>
        <?php if(!empty($commande)):?>
<!--            --><?php //foreach ($commande as $comm):?>
<!--                <div class="mt-3 border-bottom pb-3">-->
<!--                        <div class="row mt-3">-->
<!--                            <div class="col-6">-->
<!--                                <div class="pad-lr-5">--><?php //echo $comm->type_command; ?><!-- du --><?php //echo convert_datetime_to_fr($comm->created_at); ?><!--</div>-->
<!--                            </div>-->
<!--                            <div class="col-6 pr-4">-->
<!--                                <a href="--><?php //echo site_url()?><!--/front/Fidelity_pro/detail_command_global/--><?php //echo $comm->id; ?><!--"><div style="width;50px;border-radius:50%" class="btn btn-info text-white float-right ml-2">+</div></a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                </div>-->
<!--            --><?php //endforeach;?>
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th class="text-center">Type commande</th>
                    <th class="text-center">Date du commande</th>
                    <th class="text-center">Nom du Client</th>
                    <th class="text-center">Etat</th>
                    <th class="text-center">Voir Détails</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($commande as $comm):?>
                    
                <tr>
                    <td><?php echo $comm->type_command; ?></td>
                    <td><?php echo convert_datetime_to_fr($comm->created_at); ?></td>
                    <td><?php echo $thisss->User->GetfullnameById($comm->id_client); ?></td>
                    <td class="text-center
<?php if (isset($comm) && $comm->etat_commande == '') echo  'all_col'; ?>
<?php if (isset($comm) && $comm->etat_commande == 'Nouvelle Commande') echo  'new_col'; ?>
<?php if (isset($comm) && $comm->etat_commande == 'En attente du paiement') echo  'primary_col'; ?>
<?php if (isset($comm) && $comm->etat_commande == 'Paiement accepté') echo  'success_col'; ?>
<?php if (isset($comm) && $comm->etat_commande == 'En cours de préparation') echo  'warn_col'; ?>
<?php if (isset($comm) && $comm->etat_commande == 'Expédié') echo  'grenat_col'; ?>
<?php if (isset($comm) && $comm->etat_commande == 'Livré') echo  'green_fort_col'; ?>
<?php if (isset($comm) && $comm->etat_commande == 'Annulé') echo  'danger_col'; ?>
<?php if (isset($comm) && $comm->etat_commande == 'Remboursé') echo  'rembours_col'; ?>
<?php if (isset($comm) && $comm->etat_commande == 'Erreur de paiement') echo  'paiement_error_col'; ?>
"><?php echo $comm->etat_commande; ?></td>
                    <td><a href="<?php echo site_url()?>front/Fidelity_pro/detail_command_global/<?php echo $comm->id; ?>"><div style="width;50px;border-radius:50%" class="btn btn-info text-white float-right ml-2">+</div></a></td>
                </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        <?php else:?>
            <h4>Aucun resultat à afficher</h4>
        <?php endif;?>
    </div>
</div>
<?php if(!$print):?>
    <script type="text/javascript">
        function change_etat_command() {
            if (document.getElementById('etat_commande').value == "Nouvelle Commande"){
                document.getElementById('etat_commande').style.background = "#339dd9";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == ""){
                document.getElementById('etat_commande').style.background = "white";
                document.getElementById('etat_commande').style.color = "black";
            }
            if (document.getElementById('etat_commande').value == "En attente du paiement"){
                document.getElementById('etat_commande').style.background = "#0069d9";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Paiement accepté"){
                document.getElementById('etat_commande').style.background = "#218838";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "En cours de préparation"){
                document.getElementById('etat_commande').style.background = "#ffc107";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Expédié"){
                document.getElementById('etat_commande').style.background = "#6e4aa8";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Livré"){
                document.getElementById('etat_commande').style.background = "#18881e";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Annulé"){
                document.getElementById('etat_commande').style.background = "#c82333";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Remboursé"){
                document.getElementById('etat_commande').style.background = "#FC741E";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Erreur de paiement"){
                document.getElementById('etat_commande').style.background = "#92000B";
                document.getElementById('etat_commande').style.color = "white";
            }
            $('#filter_command').submit();
        }
        $(document).ready(function() {
            if (document.getElementById('etat_commande').value == "Nouvelle Commande"){
                document.getElementById('etat_commande').style.background = "#339dd9";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == ""){
                document.getElementById('etat_commande').style.background = "white";
                document.getElementById('etat_commande').style.color = "black";
            }
            if (document.getElementById('etat_commande').value == "En attente du paiement"){
                document.getElementById('etat_commande').style.background = "#0069d9";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Paiement accepté"){
                document.getElementById('etat_commande').style.background = "#218838";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "En cours de préparation"){
                document.getElementById('etat_commande').style.background = "#ffc107";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Expédié"){
                document.getElementById('etat_commande').style.background = "#6e4aa8";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Livré"){
                document.getElementById('etat_commande').style.background = "#18881e";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Annulé"){
                document.getElementById('etat_commande').style.background = "#c82333";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Remboursé"){
                document.getElementById('etat_commande').style.background = "#FC741E";
                document.getElementById('etat_commande').style.color = "white";
            }
            if (document.getElementById('etat_commande').value == "Erreur de paiement"){
                document.getElementById('etat_commande').style.background = "#92000B";
                document.getElementById('etat_commande').style.color = "white";
            }
            $('#example').DataTable( {
                "language": {
                    "lengthMenu": "Afficher _MENU_ resultat par page",
                    "zeroRecords": "Pas de resultat",
                    "info": "page _PAGE_ de _PAGES_",
                    "infoEmpty": "Pas de resultat",
                    "infoFiltered": "(filtré de _MAX_ total records)",
                    "search":"Rechercher",
                    "oPaginate": {
                    "sNext": "Page suivant",
                    "sPrevious": "Page Precedant",
                }
                },
            } );
        } );
        $(document).ready(function() {
            $(".btnPrint").printPage();
        });
    </script>
<?php endif;?>
<style type="text/css">
    .new_col{
        color: white;
        background-color: #339dd9;
        border-color: #339dd9;
    }
    .all_col{
        color: black;
        background-color: white;
        border-color: white;
    }
    .primary_col{
        color: white;
        background-color: #0069d9;
        border-color: #0069d9;
    }
    .success_col{
        color: white;
        background-color: #218838;
        border-color: #218838;
    }
    .warn_col{
        color: white;
        background-color: #ffc107;
        border-color: #ffc107;
    }
    .grenat_col{
        color: white;
        background-color: #6e4aa8;
        border-color: #6e4aa8;
    }
    .green_fort_col{
        color: white;
        background-color: #18881e;
        border-color: #18881e;
    }
    .danger_col{
        color: white;
        background-color: #c82333;
        border-color: #18881e;
    }
    .rembours_col{
        color: white;
        background-color: #FC741E;
        border-color: #FC741E;
    }
    .paiement_error_col{
        color: white;
        background-color: #92000B;
        border-color: #92000B;
    }
</style>
</body>
</html>