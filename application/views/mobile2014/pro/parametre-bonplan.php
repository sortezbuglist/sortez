<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="wrap forms ">
      <form class="pure-form pure-form-stacked">
          <br/>
          <div class="text-box f-size1 marg-bt-10">
             <p>Description du bon plan :
              <br/>
             <?php  echo (isset($oBonplan)) ? $oBonplan->bonplan_texte : '' ?>
             </p>
              <p>Validité : du  <?php if (isset($oBonplan)) { echo date('d/m/Y',strtotime($oBonplan->bonplan_date_debut)) ; } ?> au <?php if (isset($oBonplan)) { echo date('d/m/Y',strtotime($oBonplan->bonplan_date_fin)) ; } ?></p>
              <p>
                  Condition d'utilisation :
                  <br/>
              </p>
              <p>
                  <?php  echo (isset($oBonplan) && $oBonplan->bonplan_condition_vente ==2) ?  'Réservation en ligne' : '' ?>
              </p>
              <div class="pure-g">
                  <div class="pure-u-1-8"></div>
                  <div class="pure-u-3-8 align-left f-size16 v-middle">
Solde <br/>disponible <br/>à ce jours
                  </div>
                  <div class="pure-u-1-4">
                      <div class="bg-red solde pad-tb-10">
                           <?php echo (isset($oBonplan->bonplan_quantite)) ? $oBonplan->bonplan_quantite : 0 ?>
                      </div>
                  </div>
                  <div class="pure-u-1-4"></div>
              </div>
          </div>

      </form>
    </div>
    <!--forms-->
    <div class="clr"></div>
</div>
<?php $this->load->view('mobile2014/partial/footer')?>