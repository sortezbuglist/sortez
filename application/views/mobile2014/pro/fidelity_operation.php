<?php $this->load->view('mobile2014/partial/head-pro')?>
    <?php $this->load->view('mobile2014/partial/menu_pro')?>
    
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="wrap forms accordeon">
      <div class="pure-form pure-form-stacked">
          <br/>
          <!--  
          <div class="pad-tb-5">
              <button class="pure-input-1 pure-button pure-button-primary radius-blbr-0 accordion" id="bonplan">Bon plan </button>
          </div>-->

          <?php if (!empty($assoc_clients)){ ?>
          <?php foreach ($assoc_clients as $assoc_client){ ?>
          <?php if($bonplanshow =="bonplan"){?>
          <form id="form-bonplan" method="post" action="<?php echo site_url('front/fidelity_pro/card_bonplan_validate');?>">
	          <div class="text-box-fixed p-4 f-size16 bonplan accordeon-child">
	          	<div class="align-center" style="font-size: 25px;font-weight: bold;">
	          		<?php echo $assoc_client->bonplan_titre; ?>
	          	</div>
	                <div class="align-center titre red ">
	                   <?php if(isset($assoc_client) && is_object($assoc_client) && $assoc_client->valide){?>
	                   	 Ce bon plan n'est pas accepté<br />Il a déjà été validé
	                   <?php }else{?>
	                    Ce bon plan est autorisé.<br/>
	                    Vous pouvez le valider
	                 <?php } ?>
	                </div>
	              <div class="pure-g">
	                  <div class="pure-u-1-2">
	                      <div class="pure-g">
	                          <div class="pure-u-1-8"></div>
	                          <div class="pure-u-1-2 align-left f-size12 v-middle">
	                              Solde <br/>disponible <br/>à ce jours
	                          </div>
	                          <div class="pure-u-1-3">
	                              <div class="bg-red solde2 pad-tb-5">
	                                  <?php if ($assoc_client->bonplan_type == "1") echo (isset($assoc_client->bonplan_quantite)) ? $assoc_client->bonplan_quantite : 0; ?>
                                      <?php if ($assoc_client->bonplan_type == "2") echo (isset($assoc_client->bp_unique_qttprop)) ? $assoc_client->bp_unique_qttprop : 0; ?>
                                      <?php if ($assoc_client->bonplan_type == "3") echo (isset($assoc_client->bp_multiple_qttprop)) ? $assoc_client->bp_multiple_qttprop : 0; ?>
	                              </div>
	                          </div>
	                      </div>
	                  </div>
	                  <div class="pure-u-1-2">
	                      <div class="pure-g">
	                          <div class="pure-u-1-8"></div>
	                          <div class="pure-u-1-2 align-left f-size12 v-middle">
	                              Solde <br/>reservation <br/>à ce jours
	                          </div>
	                          <div class="pure-u-1-3">
	                              <div class="bg-blue solde2 pad-tb-5">
	                                   <?php //if ($assoc_client->bonplan_type == "2") echo (isset($assoc_client->bonplan_quantite_depart_unique) && isset($assoc_client->bp_unique_qttprop)) ? intval($assoc_client->bonplan_quantite_depart_unique)-intval($assoc_client->bp_unique_qttprop) : 0 ?>
                                        <?php //if ($assoc_client->bonplan_type == "3") echo (isset($assoc_client->bonplan_quantite_depart_multiple) && isset($assoc_client->bp_multiple_qttprop)) ? intval($assoc_client->bonplan_quantite_depart_multiple)-intval($assoc_client->bp_multiple_qttprop) : 0 ?>
                                        <?php
                                       $thiss=get_instance();
                                       $thiss->load->model('assoc_client_bonplan_model');
                                       $objReservationNonValideCom=$thiss->assoc_client_bonplan_model->get_where(array('assoc_client_bonplan.id_bonplan'=>$assoc_client->bonplan_id,'assoc_client_bonplan.valide'=>0));
                                       if (isset($objReservationNonValideCom)) echo strval(count($objReservationNonValideCom)); ?>
	                              </div>
	                          </div>

	                      </div>
	                  </div>
	              </div>
					<p align="center"><?php //echo (isset($assoc_client->bonplan_texte)) ? $assoc_client->bonplan_texte : ''?></p>

	          </div>
	    	   <?php if(!$assoc_client->valide){?>
	          <div class="pad-tb-5 bonplan">
	              <button class="pure-input-1 pure-button pure-button-primary bg-red radius-tltr-0" id="validation_bonplan">Valider cette transaction</button>
	          </div>
	            <input name="id_client_bonplan" id="id_client_bonplan" type="hidden" value="<?php echo $assoc_client->id;?>" />
	            <input name="id_card" id="id_card" type="hidden" value="<?php if (isset($oCard) && is_object($oCard)) echo $oCard->id; ?>" />
				<input name="id_bonplan" id="id_bonplan"  type="hidden" value="<?php if (isset($assoc_client) && is_object($assoc_client)) echo $assoc_client->bonplan_id; ?>" />
				<input name="id_commercant" id="id_commercant"  type="hidden" value="<?php if (isset($id_commercant)) echo $id_commercant; ?>" />
				<input name="id_user" id="id_user" type="hidden" value="<?php if (isset($oUser) && is_object($oUser)) echo $oUser->IdUser; ?>"/>
                  <?php }else{ ?>
                      <div class="pad-tb-5 bonplan">
                          <a onclick="javascript:void(0)" class="pure-input-1 pure-button pure-button-primary bg-red radius-tltr-0" id="">Validation impossible</a>
                      </div>
                  <?php } ?>
 		  </form>
 		  <p></p>
 		  <?php } ?>
              <?php } ?>
    <?php }else{ ?>
             <h3 style="color: white;"> Pas de réservation en cours</h3>
          <?php } ?>
          <?php if (!$bonplanshow) {?>
          <?php if(isset($oTampon) && is_object($oTampon) && $oTampon->is_activ){?>
          <form id="form-tampon" method="post" action="<?php echo site_url('front/fidelity_pro/add_card_tampon_mobile');?>">
              <div class="pad-tb-5">
                  <button class="pure-input-1 pure-button pure-button-primary accordion"  id="tampon"> Offre "coup de tampons"</button>
              </div>
              <div class="text-box-fixed  f-size16 tampon accordeon-child ">
                  <p align="center">
                      <?php if (isset($oTampon) && is_object($oTampon)) echo $oTampon->titre; ?><br/>
                  </p>
                  <div class="pure-g">
                      <div class="pure-u-1">
                          <div class="input">
                              <input type="text" class="pure-input-1 pad-tb-5" placeholder="Préciser le nombre d'unité" id="unite_tampon_value" name="unite_tampon_value"/>
                          </div>
                      </div>
                  </div>
                  <div class="pure-g bg-red no-radius">
                      <div class="<?php if (is_numeric($tampon_client_value) AND is_numeric($tampon_commercant_value)) echo blue_class_progress($tampon_client_value, $tampon_commercant_value)?>">
                          <div class="pad-lr-5 "><?php echo isset($tampon_client_value) ? $tampon_client_value : 0?></div>
                      </div>
                      <div class="<?php if (is_numeric($tampon_client_value) AND is_numeric($tampon_commercant_value)) echo red_class_progess($tampon_client_value, $tampon_commercant_value)?>"><div class="pad-lr-5"><?php echo isset($tampon_commercant_value) ? $tampon_commercant_value : 0?></div></div>
                  </div>
              </div>
              <div class="pad-tb-5 tampon accordeon-child">
                  <button class="pure-input-1 pure-button pure-button-primary bg-red radius-tltr-0" id="validation_tampon">Valider cette transaction</button>
              </div>
              <div class="tampon accordeon-child"">
              <a class="pure-button pure-button-primary" href="<?php echo site_url('front/fidelity_pro/detail_client/'.$oUser->IdUser)?>" > Historique de sa consommation</a>
      </div>
        <input name="id_card" id="id_card" type="hidden" value="<?php if (isset($oCard) && is_object($oCard)) echo $oCard->id; ?>" />
        <input name="id_bonplan" id="id_bonplan"  type="hidden" value="<?php if (isset($assoc_client) && is_object($assoc_client)) echo $assoc_client->bonplan_id; ?>" />
        <input name="id_commercant" id="id_commercant"  type="hidden" value="<?php if (isset($id_commercant)) echo $id_commercant; ?>" />
        <input name="id_user" id="id_user" type="hidden" value="<?php if (isset($oUser) && is_object($oUser)) echo $oUser->IdUser; ?>"/>
        </form>
        <?php } ?>
        <?php if(isset($oCapital) && is_object($oCapital) && $oCapital->is_activ){?>
        <div class="pad-tb-5">
            <button class="pure-input-1 pure-button pure-button-primary accordion" id="capitalisation"> Offre "coup de capitalisation"</button>
        </div>
        <form id="form-capitalisation" method="post" action="<?php echo site_url('front/fidelity_pro/add_card_capital_mobile');?>">
            <div class="text-box-fixed  f-size16 capitalisation accordeon-child">
                <p align="center">
                    <?php echo (isset($oCapital)) ? $oCapital->description : ''?>
                </p>
                <div class="pure-g">
                    <div class="pure-u-7-8">
                        <div class="input">
                            <input type="text" class="pure-input-1 pad-tb-5" placeholder="Préciser ici le montant de l'achat" name="unite_capital_value" id="unite_capital_value"/>
                        </div>
                    </div>
                    <div class="pure-u-1-8"><div class="pad-10 unit">&euro;</div></div>
                </div>
                <div class="pure-g bg-red ">
                    <?php $solde_capital = (isset($oFicheclient->solde_capital)) ? $oFicheclient->solde_capital : 0?>
                    <div class="<?php if (is_numeric($solde_capital) AND is_numeric($oCapital)) echo blue_class_progress($solde_capital, $oCapital->montant)?>">
                        <div class="pad-lr-5 "><?php echo (isset($oFicheclient) && !empty($oFicheclient)) ? $oFicheclient->solde_capital : 0?>&euro;</div>
                    </div>
                    <div class="<?php if (is_numeric($solde_capital) AND is_numeric($oCapital)) echo red_class_progess($solde_capital, $oCapital->montant)?>">
                        <div class="pad-lr-5"><?php echo (isset($oCapital)) ? $oCapital->montant : 0?>&euro;</div>
                    </div>
                </div>
            </div>
            <div class="pad-tb-5 capitalisation accordeon-child">
                <button class="pure-input-1 pure-button pure-button-primary bg-red radius-tltr-0" id="validation_capitalisation">Valider cette transaction</button>
            </div>
            <div class="capitalisation accordeon-child"">
            <a class="pure-button pure-button-primary" href="<?php echo site_url('front/fidelity_pro/detail_client/'.$oUser->IdUser)?>"> Historique de sa consommation</a>
    </div>
    <input name="id_card" id="id_card" type="hidden" value="<?php if (isset($oCard) && is_object($oCard)) echo $oCard->id; ?>" />
    <input name="id_bonplan" id="id_bonplan"  type="hidden" value="<?php if (isset($assoc_client) && is_object($assoc_client)) echo $assoc_client->bonplan_id; ?>" />
    <input name="id_commercant" id="id_commercant"  type="hidden" value="<?php if (isset($id_commercant)) echo $id_commercant; ?>" />
    <input name="id_user" id="id_user" type="hidden" value="<?php if (isset($oUser) && is_object($oUser)) echo $oUser->IdUser; ?>"/>
    </form>
    <?php } ?>
	      <?php if(isset($objRemise) && is_object($objRemise) && $objRemise->is_activ){
	      ?> 
	      <form id="form-remise" method="post" action="<?php echo site_url('front/fidelity_pro/add_card_remise_mobile');?>" class="marg-tp-10">   
	      		<div class="pad-tb-5">
	              <button class="pure-input-1 pure-button pure-button-primary accordion" id="capitalisation"> Offre "Remise directe"</button>
	          </div> 
			  <div class="text-box-fixed  f-size16 capitalisation accordeon-child">
	              <p align="center">
	                  <?php echo (isset($objRemise)) ? $objRemise->description : ''?>
	              </p>
	              <div class="pure-g">
	                  <div class="pure-u-7-8">
	                      <div class="input">
	                          <input type="text" class="pure-input-1 pad-tb-5" placeholder="Préciser ici le montant de l'achat" name="unite_remise_value" id="unite_capital_value"/>
	                      </div>
	                  </div>
	                  <div class="pure-u-1-8"><div class="pad-10 unit">&euro;</div></div>
	              </div>
	               <div class="pure-g bg-blue pad-tb-5">
			                <div class="pure-u-1-2 bg-blue align-left">
			                    <div class="pad-lr-5">Cumul consommation</div>
			                </div>
			                <div class="pure-u-1-2 bg-blue align-right price radius-5"">
			                	<div class="pad-lr-5"><?php echo (isset($assoc_client_remise) && isset($assoc_client_remise->solde_remise)) ? $assoc_client_remise->solde_remise : 0?> &euro;</div>
			                </div>
			      </div>
	          </div>
	          <div class="pad-tb-5 capitalisation accordeon-child">
	              <button class="pure-input-1 pure-button pure-button-primary bg-red radius-tltr-0" id="validation_capitalisation">Valider cette transaction</button>
	          </div>
	           <div class="capitalisation accordeon-child"">
	          	<a class="pure-button pure-button-primary" href="<?php echo site_url('front/fidelity_pro/detail_client/'.$oUser->IdUser)?>"> Historique de sa consommation</a>
	      	  </div>
				<input name="id_card" id="id_card" type="hidden" value="<?php if (isset($oCard) && is_object($oCard)) echo $oCard->id; ?>" />
				<input name="id_bonplan" id="id_bonplan"  type="hidden" value="<?php if (isset($assoc_client) && is_object($assoc_client)) echo $assoc_client->bonplan_id; ?>" />
				<input name="id_commercant" id="id_commercant"  type="hidden" value="<?php if (isset($id_commercant)) echo $id_commercant; ?>" />
				<input name="id_user" id="id_user" type="hidden" value="<?php if (isset($oUser) && is_object($oUser)) echo $oUser->IdUser; ?>"/>
				<input name="percent_remise" id="percent_remise" type="hidden" value="<?php if (isset($objRemise) && is_object($objRemise)) echo $objRemise->montant; ?>"/>
	      </form>
        <?php } ?>
<?php } ?>
	       <div class="pad-tb-5 accordeon">
              <a class="pure-button pure-button-primary" href="<?php echo site_url('/front/fidelity_pro/validation_operation/'.$oUser->IdUser)?>">Retour menu avantages</a>
          </div>
          
      </div>
    </div>
       <!--forms-->
    <div class="clr"></div>
</div>
<script type="text/javascript">
<!--
(function($) {
	
	 /* var allPanels = $('.accordeon-child').hide();
	  $('button.accordion').click(function(e) {
		e.preventDefault();
		var target =$(this).attr('id');
	   
	    if(){
	    	 allPanels.slideUp();
	    	$('.'+target).slideDown();
	    }
	    return false;
	  });*/
		
	  	var id_card = $("#id_card").val();
		var id_bonplan = $("#id_bonplan").val();
		var id_commercant = $("#id_commercant").val();
		var id_user = $("#id_user").val();
		var target_request = '<?php echo $record_num = end($this->uri->segment_array()); ?>';
		if(target_request == 'bonplan' || target_request == 'capitalisation' || target_request == 'tampon'){
			 $('.'+target_request).slideDown();
		} 	
		
	 $('#validation_tampon').on('click',function(e){
		 $("#form-tampon").submit();
		 /* e.preventDefault();
		  var unite_tampon_value = $('#unite_tampon_value').val();
		  var form = $("#form-tampon");
		  if(form.validate()){
			  $.ajax({
			  		type: "POST",
			  		url: ; ?>",
			  		data: { 
				  		id_card: id_card,
			  			id_bonplan: id_bonplan,
			  			id_commercant: id_commercant,
			  			id_user: id_user,
			  			unite_tampon_value: unite_tampon_value
			  			},
			  		success: function (response){
			  			
			  		},
			  		async: false
			    });
		  }*/
	  });
	  //formfidelisation
	  $( "#form-tampon" ).validate({
			//debug: true,
			rules: {
				'unite_tampon_value' : {
					 number: true,
					 required: true,
					 <?php echo isset($tampon_commercant_value) ? "max : $tampon_commercant_value" : ""?>	
				}
			}
	  });
	  $( "#form-capitalisation" ).validate({
			//debug: true,
			rules: {
				'unite_capital_value' : {
					 number: true,
					 required: true,
				}
			},
			messages: {
				'unite_capital_value' :{
					number: 'Veuillez fournir un montant valide'
					}
			}
		});
	  $( "#form-remise" ).validate({
			//debug: true,
			rules: {
				'unite_remise_value' : {
					 number: true,
					 required: true,
				}
			},
			messages: {
				'unite_remise_value' :{
					number: 'Veuillez fournir un montant valide'
					}
			}
		});
	    
 })(jQuery);
//-->
</script>
</body>
</html>