<?php
$this->load->view('mobile2014/partial/head-pro')?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<?php if(!$print):?>
    <?php $this->load->view('mobile2014/partial/menu_pro')?>
<?php endif;?>
<style type="text/css">
    .btn-menu-2{
        background-color: rgb(207, 21, 144);
    }
</style>
<div class="row">
    <div class="col-12 pt-3 pb-3 text-center">
        <a class="" href="<?php echo site_url()?>front/fidelity_pro/all_command"><button class="btn btn-primary">Retour liste</button></a>
    </div>
</div>
<div class="row" style="border-top: solid 1px">
    <div class="col-12 pt-3 pb-3 text-center">
        <h4>Information du client:</h4>
    </div>
</div>
<?php //var_dump($client); ?>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
        <td>
            <div class="details f-size14 pad-10 no-marg">
                <b><?php  echo ucfirst($client->Prenom).' '.ucfirst($client->Nom);?></b>
            </div>
        </td>
        <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity_pro/user/'.$client->IdUser)?>" class=""><img src="<?php echo base_url()?>assets/img/id_card.png"></a></td>
        <td width="10%" v-align="top"><a href="mailto:<?php echo $client->Email; ?>" class=""><img src="<?php echo base_url()?>assets/img/email.png"></a></td>
        <td width="10%" v-align="top"><a href="tel:<?php if ($client->Telephone == null){echo $client->Portable;}else{echo $client->Telephone;}  ?>" class=""><img src="<?php echo base_url()?>assets/img/mobile_phone.png"></a></td>
    </tr>
</table>
<div class="row pt-2" style="border-top: solid 1px">
    <div class="col-12 pt-3 pb-3 text-center">
        <h4>Detail du commande:</h4>
    </div>
</div>
<div class="wrapper page-white">
    <div class="consommation">
        <?php if(!empty($details)):?>
            <?php if (isset($commande) AND !empty($commande)){ ?>
                <?php //var_dump($commande); ?>
                <div class="row">
                    <div class="col-4 font-weight-bold">Type de commande: </div>
                    <div class="col-8 "><?php echo $commande[0]["type_command"]; ?></div>
                    <div class="col-4 font-weight-bold">Type de livraison: </div>
                    <div class="col-8 "><?php echo $commande[0]["type_livraison"]; ?></div>
                    <div class="col-4 font-weight-bold">Jour souhaitée: </div>
                    <div class="col-8 "><?php echo translate_date_to_fr($commande[0]["jour_enlev"]); ?></div>
                    <div class="col-4 font-weight-bold">heure souhaitée: </div>
                    <div class="col-8 "><?php echo $commande[0]["heure_enleve"]; ?></div>
                    <div class="col-4 font-weight-bold">Type de paiement:</div>
                    <div class="col-8 "><?php echo $commande[0]["type_payement"]; ?></div>
                    <div class="col-4 font-weight-bold">Prix total:</div>
                    <div class="col-8 "><?php echo "€".$commande[0]["total_price"]; ?></div>
                    <div class="col-4 font-weight-bold">Question/Commentaires:</div>
                    <div class="col-8 "><?php echo $commande[0]["comment"]; ?></div>

                    <?php if ($commande->remise_promotion_price !=0 && $commande->remise_promotion_price !=null ){?>
                        <div class="col-4 font-weight-bold">Remise promotionnelle:</div>
                        <div class="col-8 "><?php echo $commande[0]["comment"]; ?></div>
                    <?php } ?>
                    <?php if ($commande->command_files !=null && is_file("application/resources/front/command_clients/".$IdUser."/".$commande->command_files)){  ?>
                        <div class="col-12 font-weight-bold">
                            La pièce jointe est téléchargeable via ce <a
                                href="<?php echo base_url() ?>application/resources/front/command_clients/<?php echo $IdUser; ?>/<?php echo $commande[0]["IdUser"]; ?>">lien</a>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php
            $thiss = &get_instance();
            $thiss->load->model('Mdl_soutenons');
            ?>
            <?php foreach ($details as $detail):?>
                <?php
                $product = $thiss->Mdl_soutenons->get_product_by_id_comm($detail["id_produit"]);
                ?>
                <div class="mt-3 border-bottom mb-3">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td class="font-weight-bold border" width="50%" v-align="top">Nom du produit:</td>
                            <td class="font-weight-bold border" width="50%" v-align="top">Quantité:</td>
                            <td class="font-weight-bold border" width="50%" v-align="top">Prix unitaire:</td>
                        </tr>
                        <tr>
                            <td class="p-2 border" width="50%" ><?php echo $product->titre; ?></td>
                            <td class="p-2 border" width="50%" ><?php echo $detail["nbre"]; ?></td>
                            <td class="p-2  border" width="50%" ><?php echo "€".$product->prix; ?></td>
                        </tr>
                    </table>
                </div>
            <?php endforeach;?>
            <form action="<?php echo site_url()?>front/Fidelity_pro/save_etat_commandes" method="post">
                <input type="hidden" name="id_commande" value="<?php echo $id_commande; ?>" />
                <div class="row mt-3">
                    <div class="col-4 pt-2">
                        Modifier L'etat de la commande
                    </div>
                    <div class="col-4">
                        <select onchange="change_etat_command()" id="etat_commande" name="etat_commande" class="form-control">
                            <option <?php if (isset($commande) && $commande->etat_commande == 'Nouvelle Commande') echo  'selected'; ?> class="new_col" value="Nouvelle Commande">Nouvelle Commande</option>
                            <option <?php if (isset($commande) && $commande->etat_commande == 'En attente du paiement') echo  'selected'; ?> class="primary_col" value="En attente du paiement">En attente du paiement</option>
                            <option <?php if (isset($commande) && $commande->etat_commande == 'Paiement accepté') echo  'selected'; ?> class="success_col" value="Paiement accepté">Paiement accepté</option>
                            <option <?php if (isset($commande) && $commande->etat_commande == 'En cours de préparation') echo  'selected'; ?> class="warn_col" value="En cours de préparation">En cours de préparation</option>
                            <option <?php if (isset($commande) && $commande->etat_commande == 'Expédié') echo  'selected'; ?> class="grenat_col" value="Expédié">Expédié</option>
                            <option <?php if (isset($commande) && $commande->etat_commande == 'Livré') echo  'selected'; ?> class="green_fort_col" value="Livré">Livré</option>
                            <option <?php if (isset($commande) && $commande->etat_commande == 'Annulé') echo  'selected'; ?> class="danger_col" value="Annulé">Annulé</option>
                            <option <?php if (isset($commande) && $commande->etat_commande == 'Remboursé') echo  'selected'; ?> class="rembours_col" value="Remboursé">Remboursé</option>
                            <option <?php if (isset($commande) && $commande->etat_commande == 'Erreur de paiement') echo  'selected'; ?> class="paiement_error_col" value="Erreur de paiement">Erreur de paiement</option>
                        </select>
                    </div>
                    <div class="col-4">
                        <input type="submit" class="btn btn-info w-100" value="Valider" />
                    </div>
                </div>
            </form>
        <?php else:?>
            <h4>Vous n'avez pas de commande</h4>
        <?php endif;?>
    </div>
</div>
<?php if(!$print):?>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".btnPrint").printPage();
        });
    </script>
<?php endif;?>
<style type="text/css">
    .new_col{
        color: white;
        background-color: #339dd9;
        border-color: #339dd9;
    }
    .primary_col{
        color: white;
        background-color: #0069d9;
        border-color: #0069d9;
    }
    .success_col{
        color: white;
        background-color: #218838;
        border-color: #218838;
    }
    .warn_col{
        color: white;
        background-color: #ffc107;
        border-color: #ffc107;
    }
    .grenat_col{
        color: white;
        background-color: #6e4aa8;
        border-color: #6e4aa8;
    }
    .green_fort_col{
        color: white;
        background-color: #18881e;
        border-color: #18881e;
    }
    .danger_col{
        color: white;
        background-color: #c82333;
        border-color: #18881e;
    }
    .rembours_col{
        color: white;
        background-color: #FC741E;
        border-color: #FC741E;
    }
    .paiement_error_col{
        color: white;
        background-color: #92000B;
        border-color: #92000B;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        if (document.getElementById('etat_commande').value == "Nouvelle Commande"){
            document.getElementById('etat_commande').style.background = "#339dd9";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "En attente du paiement"){
            document.getElementById('etat_commande').style.background = "#0069d9";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Paiement accepté"){
            document.getElementById('etat_commande').style.background = "#218838";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "En cours de préparation"){
            document.getElementById('etat_commande').style.background = "#ffc107";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Expédié"){
            document.getElementById('etat_commande').style.background = "#6e4aa8";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Livré"){
            document.getElementById('etat_commande').style.background = "#18881e";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Annulé"){
            document.getElementById('etat_commande').style.background = "#c82333";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Remboursé"){
            document.getElementById('etat_commande').style.background = "#FC741E";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Erreur de paiement"){
            document.getElementById('etat_commande').style.background = "#92000B";
            document.getElementById('etat_commande').style.color = "white";
        }
    });
    function change_etat_command() {
        if (document.getElementById('etat_commande').value == "Nouvelle Commande"){
            document.getElementById('etat_commande').style.background = "#339dd9";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "En attente du paiement"){
            document.getElementById('etat_commande').style.background = "#0069d9";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Paiement accepté"){
            document.getElementById('etat_commande').style.background = "#218838";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "En cours de préparation"){
            document.getElementById('etat_commande').style.background = "#ffc107";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Expédié"){
            document.getElementById('etat_commande').style.background = "#6e4aa8";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Livré"){
            document.getElementById('etat_commande').style.background = "#18881e";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Annulé"){
            document.getElementById('etat_commande').style.background = "#c82333";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Remboursé"){
            document.getElementById('etat_commande').style.background = "#FC741E";
            document.getElementById('etat_commande').style.color = "white";
        }
        if (document.getElementById('etat_commande').value == "Erreur de paiement"){
            document.getElementById('etat_commande').style.background = "#92000B";
            document.getElementById('etat_commande').style.color = "white";
        }
    }
</script>
</body>
</html>