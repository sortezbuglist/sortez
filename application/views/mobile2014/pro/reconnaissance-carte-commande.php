<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
    <style type="text/css">
        body {
            width: 100%;
            text-align: center;
        }

        img {
            border: 0;
        }

        #main {
            margin: 15px auto;
            background: white;
            width: 100%;
            height: 50px!important;
            overflow: hidden;

        }

        #header {
            background: white;
            margin-bottom: 15px;
        }

        #mainbody {
            background: white;
            width: 100%;
            display: none;
        }

        #footer {
            background: white;
        }

        #v {
            width: 100%;
            height: 100%;
        }

        #qr-canvas {
            display: none;
        }

        #qrfile {
            width: 320px;
            height: 240px;
        }

        #mp1 {
            text-align: center;
            font-size: 35px;
        }

        #imghelp {
            position: relative;
            left: 0px;
            top: -160px;
            z-index: 100;
            font: 18px arial, sans-serif;
            background: #f0f0f0;
            margin-left: 35px;
            margin-right: 35px;
            padding-top: 10px;
            padding-bottom: 10px;
            border-radius: 20px;
        }

        .selector {
            margin: 0;
            padding: 0;
            cursor: pointer;
            margin-bottom: -5px;
        }

        #outdiv {
            padding: 0!important;
            width: 320px;
            height: 100px;
            border: solid;
            border-width: 3px 3px 3px 3px;
        }

        #result {
            border: solid;
            border-width: 1px 1px 1px 1px;
            padding: 20px;
            width: 100%!important;
            margin: auto!important;
        }

        ul {
            margin-bottom: 0;
            margin-right: 40px;
        }

        li {
            display: inline;
            padding-right: 0.5em;
            padding-left: 0.5em;
            font-weight: bold;
            border-right: 1px solid #333333;
        }

        li a {
            text-decoration: none;
            color: black;
        }

        #footer a {
            color: black;
        }

        .tsel {
            padding: 0;
        }

    </style>

    <script type="text/javascript"
            src="<?php echo base_url()?>assets/js/llqrcode.js"></script>
    <script type="text/javascript"
            src="<?php echo base_url()?>assets/js/plusone.js"></script>
    <script type="text/javascript"
            src="<?php echo base_url()?>assets/js/webqr.js"></script>

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-24451557-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

    </script>
    <div class="wrapper head">
        <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
        <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
    </div>
    <div class="page-black" id="page_black" style="min-height: 500px!important;">

        <!--forms-->
        <div class="wrap forms ">
            <form class="pure-form pure-form-stacked" action="<?php echo site_url('front/fidelity_pro/card_scan'); ?>" method="post" name="frmscancard" id="frmscancard">
                <div id="main">
                    <div id="mainbody" class="w-100">
                        <table class="tsel w-100"
                               border="0"
                               width="100%">
                            <tr>
                                <td valign="top"
                                    align="center"
                                    width="50%">
                                    <table class="tsel"
                                           border="0">
                                        <tr>
                                            <td colspan="2">
                                                <img class="selector"
                                                     id="webcamimg"
                                                     src="<?php echo base_url()?>assets/img/vid.png"
                                                     onclick="setwebcam()"
                                                     align="center"/>
                                                <img class="selector"
                                                     id="qrimg"
                                                     src="<?php echo base_url()?>assets/img/cam.png"
                                                     onclick="setimg()"
                                                     style="display:none;"
                                                     align="right"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"
                                                align="center">
                                                <div >
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <script async
                                src="<?php echo site_url()?>assets/f.txt"></script>
                        <!-- webqr_2016 -->
                        <ins class="adsbygoogle"
                             style="display:block"
                             data-ad-client="ca-pub-8418802408648518"
                             data-ad-slot="2527990541"
                             data-ad-format="auto"></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    &nbsp
                </div>
                <canvas id="qr-canvas"
                        width="800"
                        height="600"></canvas>
                <script type="text/javascript">load();</script>
                <div id="outdiv" class="loadblock">
                    <div class="loading"></div>
                    <span id="span_card_scan"><?php if (isset($mssg) && $mssg!="") echo $mssg; ?></span>
                </div>
                <br/>
                <div class="input">
                    <input type="text" class="pure-input-1 align-center " placeholder="Ou préciser le numero de la carte" id="num_card_scan" name="num_card_scan"/>
                </div>

                <div class="pad-tb-5  accordeon">
                    <button disabled class="pure-button w-100 m-auto pure-button-primary" href="javascript:void(0);" id="result"></button>
                </div>
                <div class="pad-tb-5 accordeon">
                    <button class="pure-button pure-button-primary" href="javascript:void(0);" id="link_btn_jaune_validate"> Validation </button>
                </div>
            </form>
        </div>
        <!--forms-->
        <div class="clr"></div>
    </div>
    <script type="text/javascript">

        $(document).ready(function() {
            setTimeout(function () {
                $('#main').css('height','50px');
                $('#page_black').css('min-height','500px');
            },5000);
            $("#link_btn_jaune_validate").click(function(e){
                e.preventDefault();
                var zErreur = "" ;
                var num_card_scan = "";

                num_card_scan = $("#num_card_scan").val();

                if(num_card_scan !=''){
                    $('#span_card_scan').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');

                    $.post(
                        '<?php echo site_url("front/fidelity_pro/check_scan_card_command");?>',
                        {
                            num_card_scan: num_card_scan
                        },
                        function (zReponse)
                        {
                            data = $.parseJSON(zReponse);
                            zReponse=data.status;
                            if (zReponse == "1") {
                                $('#span_card_scan').html('<span style="color:#090"><p style="color:#81d267"><strong>Votre carte est Valide</strong></p><p><img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/></p></span>');
                                $.post(
                                    '<?php echo site_url("front/fidelity_pro/getuser_card_scan");?>',
                                    {
                                        num_card_scan: num_card_scan
                                    },
                                    function (zReponse2)
                                    {
                                        document.location = '<?php echo site_url("front/fidelity_pro/validation_operation_command");?>'+"/"+zReponse2;
                                    });

                            } else if (zReponse == "2") {
                                $('#span_card_scan').html('<span style="color:#FF0000"><strong>Votre carte est répertorié, mais n\'est pas activé !</strong></span>');
                            } else if (zReponse == "3") {
                                $('#span_card_scan').html('<span style="color:#FF0000"><strong>Votre carte n\'est pas encore liée à un compte Client !</strong></span>');
                            } else {
                                $('#span_card_scan').html('<span style="color:#FF0000"><strong>Cette carte est invalide !</strong></span>');
                            }
                        });
                }
            });
        });

    </script>
<?php $this->load->view('mobile2014/partial/footer')?>