<?php
$this->load->view('mobile2014/partial/head-pro')?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<?php if(!$print):?>
    <?php $this->load->view('mobile2014/partial/menu_pro')?>
<?php endif;?>
<style type="text/css">
    .btn-menu-2{
        background-color: rgb(207, 21, 144);
    }
</style>
<div class="row">
    <div class="col-12 pt-3 pb-3 text-center">
        <h3>Liste des commandes:</h3>
    </div>
</div>
<div class="wrapper page-white">
    <div class="consommation">
        <?php if(!empty($commande)):?>
        <?php $nb=0; ?>
            <?php foreach ($commande as $comm):?>
                <div class="mt-3 border-bottom pb-3">
                    <?php if (isset($nb) AND $nb == 0){ ?>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                            <tr>
                                <td>
                                    <div class="details f-size14 pad-10 no-marg">
                                        <b><?php  echo ucfirst($comm->Prenom).' '.ucfirst($comm->Nom);?></b>
                                    </div>
                                </td>
                                <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity_pro/user/'.$comm->IdUser)?>" class=""><img src="<?php echo base_url()?>assets/img/id_card.png"></a></td>
                                <td width="10%" v-align="top"><a href="mailto:<?php echo $comm->Email; ?>" class=""><img src="<?php echo base_url()?>assets/img/email.png"></a></td>
                                <td width="10%" v-align="top"><a href="tel:<?php if ($comm->Telephone == null){echo $comm->Portable;}else{echo $comm->Telephone;}  ?>" class=""><img src="<?php echo base_url()?>assets/img/mobile_phone.png"></a></td>
                            </tr>
                        </table>
                        <?php } $nb=1; ?>
                        <?php if($offre_active === "Commande") :?>
                            <div class="row mt-3">
                                <div class="col-6">
                                    <div class="pad-lr-5"><?php echo $comm->type_command; ?> du <?php echo convert_datetime_to_fr($comm->created_at); ?></div>
                                </div>
                                <div class="col-6 pr-4">
                                    <a href="<?php echo site_url()?>/front/Fidelity_pro/detail_command/<?php echo $comm->id; ?>/<?php echo $comm->IdUser; ?>"><div style="width;50px;border-radius:50%" class="btn btn-info text-white float-right ml-2">+</div></a>
                                    <a onclick="return confirm('Supprimer cette Commande ?')" href="<?php echo site_url()?>/front/Fidelity_pro/delete_command/<?php echo $comm->id; ?>/<?php echo $comm->IdUser; ?>"><div style="width;50px;border-radius:50%" class="btn btn-danger text-white float-right">x</div></a>
                                </div>
                            </div>
                    <?php endif;?>
                </div>
            <?php endforeach;?>
        <?php else:?>
            <h4>Vous n'avez pas de commande</h4>
        <?php endif;?>
    </div>
</div>
<?php if(!$print):?>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".btnPrint").printPage();
        });
    </script>
<?php endif;?>
</body>
</html>