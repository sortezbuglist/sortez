<script type="text/javascript" src="<?php echo base_url();?>assets/js/aviary.editor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#delete_tampon_photo").click(function(){
			var idcom = jQuery("#idcom").val();
			$.ajax({
			type: "POST",
			url: '<?php echo site_url("front/fidelity_pro/delete_tampon_photo"); ?>',
			data: 'idcommercant=' + idcom,
			success: function (zReponse) {
				console.log(zReponse);
				if(zReponse=="error") {
					alert("Une erreur est survenue ! Veuillez re-essayer !");
				} else if(zReponse=="ok"){
					alert("Image supprimer avec succèss");
					location.reload(true);					
				}

			},
			error: function (zReponse) {
				console.log(zReponse);
			}
		});
		});		
	});	
</script>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="wrap forms ">
      <form class="pure-form pure-form-stacked" action="<?php echo site_url('front/fidelity_pro/edittampon'); ?>" method="post" name="frmtampon" id="frmtampon">
          <br/>
          <div class="pure-u-1">
	          <div class="input inputdate-form pad-5">
               <label for="module_tampon" class="module">Activation du module
	              <input type="checkbox" class="checkbox" name="tampon[is_activ]" value="1" id="module_tampon" <?php echo (isset($oTampon)  && is_object($oTampon) && $oTampon->is_activ) ? "checked" : ""?>/> </label>
	            </div>
          </div>
             <textarea class="textarea-box" name="tampon[description]" id="description" cols="30" rows="5"><?php if (isset($oTampon) && is_object($oTampon)) echo $oTampon->description;?></textarea>
          <div class="input inputdate-form marg-bt-10">
              <label for="">Date debut:</label>
              <input type="text" class="pure-input-1" name="tampon[date_debut]" id="date_debut" value="<?php echo (isset($oTampon) && is_object($oTampon)) ? convert_Sqldate_to_Frenchdate($oTampon->date_debut): "cliquez ici";?>"/>
          </div>
          <div class="input inputdate-form marg-bt-10">
              <label for="">Date fin: </label>
              <input type="text" class="pure-input-1" name="tampon[date_fin]" id="date_fin" value="<?php echo (isset($oTampon) && is_object($oTampon)) ? convert_Sqldate_to_Frenchdate($oTampon->date_fin) : "cliquez ici";?>" />
          </div>

          <div class="input inputdate-form marg-bt-10">
              <label for="">Objectif</label>
              <input type="text" class="pure-input-1 f-size14" name="tampon[tampon_value]" id="tampon_value" value="<?php echo (isset($oTampon) && is_object($oTampon)) ? $oTampon->tampon_value :0;?>"/>
		  </div>
		  <!-- Ajout d'image -->
		  <div class="pure-u-1">
			<div class="input inputdate-form pad-5">
				<div id="Articlebackground_image_container">
					<?php if (isset($oTampon->image1) && $oTampon->image1!= null ) { ?>
						<input type="hidden" id="idcom" value="<?php echo $oTampon->id_commercant; ?>">
						<img src="<?php echo('/application/resources/front/photoCommercant/imagesbank/'.$oTampon->id_ionauth.'/fidelity_photo/'.$oTampon->image1) ?>">
						<a href="javascript:void(0);" class="btn btn-danger" onclick="return false;" id="delete_tampon_photo"><button class="btn btn-info">Supprimer</button></a>
    				<?php }else {?>
                        <a href="javascript:void(0);" title="Photo1" class="btn btn-info" onclick="javascript:window.open('<?php echo site_url('media/index/'.$oTampon->id_commercant.'-fidelity_photo-image1') ?>','Commercant image','width=1045, height=725, scrollbars=yes');" id="Articlebackground_link"><button type="button" class="btn btn-info">Ajouter une image</button></a>
					<?php  }?>
				</div>
			</div>
		  </div>
		  <!-- Fin -->
          <div class="pad-tb-5 accordeon">
              <button class="pure-button pure-button-primary" id="validate"> Validation </button>
          </div>
			<input name="tampon[id]" id="id" type="hidden" value="<?php if (isset($oTampon) && is_object($oTampon)) echo $oTampon->id; else echo "0";?>" />
      </form>
    </div>
    <!--forms-->
    <div class="clr"></div>
</div>
<script type="text/javascript">
<!--

$("#date_debut").datepicker({
	dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
	dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
	monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
	dateFormat: 'DD, d MM yy',
	autoSize: true,
	changeMonth: true,
	changeYear: true,
	yearRange: '1900:2040',
	onSelect: function(dateText) {
		$( "#date_debut" ).valid();
	}
});
$("#date_fin").datepicker({
	dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
	dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
	monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
	dateFormat: 'DD, d MM yy',
	autoSize: true,
	changeMonth: true,
	changeYear: true,
	yearRange: '1900:2040',
	onSelect: function(dateText) {
		$( "#date_fin" ).valid();
	}
});

	$( "#frmtampon" ).validate({
		//debug: true,
		rules: {
			'tampon[tampon_value]' : {
				required: true
			},
			'tampon[date_fin]': {
				required: true
			},
			'tampon[date_debut]': {
				required: true
			},
			'tampon[description]': {
				required: true
			}
		}
	});
//-->
</script>
<?php $this->load->view('mobile2014/partial/footer')?>