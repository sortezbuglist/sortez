<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>

<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper">
    <!--liens-->
    <div class="consommation-filtre ">
        <div class="list">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td valign="middle" width="25%">
                        <div class="pad-10">
                            <a href="<?php echo site_url('front/fidelity_pro/liste_clients/bonplan')?>"><img src="<?php echo img_url("tag.png") ?>" alt=""/></a>
                        </div>
                    </td>
                    <td valign="middle">
                        <div class="pad-5">
                            <a href="<?php echo site_url('front/fidelity_pro/liste_clients/bonplan')?>">
                                Visualiser uniquement les bons plans<br/>
                                Ordre croissant
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/fidelity_pro/liste_clients/bonplan')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                    </td>
                </tr>
                <!--  
                <tr>
                    <td valign="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url("chart_down.png") ?>" alt=""/></a>
                        </div>
                    </td>
                    <td valign="middle">
                        <div class="pad-5">
                            <a href="<?php echo site_url('front/fidelity_pro/liste_clients/fidelisation')?>">
                               Bon plan avec un classement par date la plus récente à la plus ancienne
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/fidelity_pro/liste_clients/fidelisation')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                    </td>
                </tr>
                <!--  
                <tr>
                    <td valign="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url("chart_up.png") ?>" alt=""/></a>
                        </div>
                    </td>
                    <td valign="middle">
                        <div class="pad-5">
                            <a href="<?php echo site_url('front/fidelity_pro/liste_clients/tous/asc')?>">
                                De la date la plus ancienne à la plus récente
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/fidelity_pro/liste_clients/tous/asc')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                    </td>
                </tr>
                -->
                <!--  
                <tr>
                    <td valign="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url("promo_purecent.png") ?>" alt=""/></a>
                        </div>
                    </td>
                    <td valign="middle">
                        <div class="pad-5">
                            <a href="#">
                                Visualiser uniquement les actions remise directe <br/>
                                Ordre croissant
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="#" class="bg-grey-round"><span class="fa fa-check"></span></a>
                    </td>
                </tr>
                -->
                <tr>
                    <td valign="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url("promo_cochon.png") ?>" alt=""/></a>
                        </div>
                    </td>
                    <td valign="middle">
                        <div class="pad-5">
                            <a href="<?php echo site_url('front/fidelity_pro/liste_clients/solde_asc')?>">
                                 Fidélisation avec un classement par objectif cumulé le plus faible au plus fort
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="#" class="bg-grey-round"><span class="fa fa-check"></span></a>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" width="25%">
                        <div class="pad-10">
                            <a href="<?php echo site_url('front/fidelity_pro/liste_clients/fidelisation')?>"><img src="<?php echo img_url("promo_tampons.png") ?>" alt=""/></a>
                        </div>
                    </td>
                    <td valign="middle">
                        <div class="pad-5">
                            <a href="<?php echo site_url('front/fidelity_pro/liste_clients/solde_desc')?>">
                                Fidélisation avec un classement par objectif cumulé le plus fort au plus faible
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/fidelity_pro/liste_clients/fidelisation')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                    </td>
                </tr>
                <tr>
                    <td valign="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url("ico-calendar.png") ?>" alt=""/></a>
                        </div>
                    </td>
                    <td valign="middle">
                        <div class="pad-5">
                            <a href="<?php echo site_url('front/fidelity_pro/filtre_mois')?>">
                                Filtrez par mois<br/>
                                d'anniversaire
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/fidelity_pro/filtre_mois')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                    </td>
                </tr>
            </table>
        </div>

    </div>
    <div class="clr"></div>
</div>
</body>
</html>
