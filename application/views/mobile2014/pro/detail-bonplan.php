<?php //var_dump($client);?>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
    <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        .bouton{
            height: 40px;
            line-height: 3;
            color: white!important;
            font-size: 17px;
            cursor: pointer;
        }
        a:hover{
            text-decoration: none;
        }
        a:visited{
            color: white;
        }
        .bg-pink{
            background-color: rgb(208, 0, 118);
        }
        .fontlabel{
            color: #0653a3!important;   
            font-size: 15px;         
        }
        .fontinfo{
            color: #000000!important;
            font-size: 15px;
        }
        .titrecarte{
            font-weight: bold;
        }
        .fontinfosociete{
            color: red!important;            
        }
    </style>
</div>
<?php //var_dump($fidelity); ?>
<div class="wrapper page-white">
    <div class="consommation">
        <div class="product product-row">
            <?php if (isset($fidelity->date_validation)){ echo date('d/m/Y',strtotime($fidelity->date_validation));}else{echo "Non precisé";};?> : Bon plan validé
        </div>
    </div>

    <div class="clr"></div>
    <?php if (isset($user_bonplan) AND !empty($user_bonplan)){ ?>
    <div class="col-12 text-center fontinfosociete"><h3><?php echo $user_bonplan[0]->NomSociete  .' '.$user_bonplan[0]->Prenom  ?></h3></div>
    <?php }  ?>
    <?php if (isset($user_bonplan) AND !empty($user_bonplan)){ ?>
    <?php foreach ($user_bonplan as $bonplan){ ?>

<?php if (isset($bonplan->bonplan_type) AND $bonplan->bonplan_type =='2' ){ ?>

    <div class="row">
            <div class="col-12 titrecarte"><h4>Bonplan Unique:</h4></div>
            <div class="col-4 fontlabel">Titre</div><div class="col-8 fontinfo"><?php echo $bonplan->bonplan_titre;?></div>
            <div class="col-4 fontlabel">Expiré le</div><div class="col-8 fontinfo"><?php echo convert_Sqldate_to_Frenchdate($bonplan->bp_unique_date_fin);?></div>
            <div class="col-4 fontlabel">Prix</div><div class="col-8 fontinfo"><?php echo $bonplan->bp_unique_prix;?></div>
            <div class="col-4 fontlabel">Valideé le</div><div class="col-8 fontinfo"><?php echo convert_Sqldate_to_Frenchdate($bonplan->date_validation);?></div>
    </div>

        <?php }  ?>

        <?php if (isset($bonplan->bonplan_type) AND $bonplan->bonplan_type =='3' ){ ?>

        <div class="row">
            <div class="col-12 titrecarte"><h4>Bonplan Multiple:</h4></div>
            <div class="col-4 fontlabel">Titre</div><div class="col-8 fontinfo"><?php echo $bonplan->bonplan_titre;?></div>
            <div class="col-4 fontlabel">Expiré le</div><div class="col-8 fontinfo"><?php echo convert_Sqldate_to_Frenchdate($bonplan->bp_multiple_date_fin);?></div>
            <div class="col-4 fontlabel">Prix</div><div class="col-8 fontinfo"><?php echo $bonplan->bp_multiple_prix ;?></div>
            <div class="col-4 fontlabel">Validé le</div><div class="col-8 fontinfo"><?php echo convert_Sqldate_to_Frenchdate($bonplan->date_validation);?></div>
            </div>

            <?php }  ?>
            <form action="<?php echo site_url('front/fidelity_pro/reinistialisation')?>" class="pure-form pure-form-stacked no-padd mb-4" method="POST" id="form-actualisation">
                <div class="input pb-4">
                    <input type="hidden" value="bonplan" name="type_offre" />
                    <input type="hidden" value="<?php echo $bonplan->IdUser; ?>" name="id_user" />
                    <input type="hidden" value="<?php echo $bonplan->bonplan_id; ?>" name="bonplan_id" />
                    <input type="hidden" value="<?php echo $bonplan->id; ?>" name="assoc_client_bonplan_id" />
                </div>
                <div class="text-center"><button id="delete" class="pure-input-1 pure-button pure-button-primary bg-blue" style="font-size: 15px;">Réinitialiser</button></div>
            </form>
    <?php } ?>
    <?php }else{echo 'Pas de donnée';} ?>
</div>
</body>
</html>