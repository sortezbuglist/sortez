<?php $this->load->view('mobile2014/partial/head')?>
<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
   <?php $this->load->view('mobile2014/partial/titre_head_view')?>
        <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="wrap forms">
      <form class="pure-form pure-form-stacked">

          <div class="pure-g cart-phys">
              <div class="pure-u-1">
                  <p class="f-size18">
                   Le compte suivant a été bien créé. 
                  </p>
              </div>
              <div class="pure-u-1">
                  <div class="pad-10 align-left">
                      <br/>
                      <span>
						<p class="f-size16">Login : <?php echo $login;?></p>
						<p class="f-size16">Mot de passe : <?php echo $pwd;?></p>             
					 </span>
                  </div>
              </div>
          </div>
          <div class="wrap links">
           		<div class="pad-tb-5">
            		<a href="<?php echo site_url('front/fidelity/contenupro')?>" class="pure-button pure-button-primary">Retour au menu général</a>
          		</div>
			</div>
      </form>
    </div>
    <!--forms--> <div class="clr"></div>
    </div>
</div>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<?php $this->load->view('mobile2014/partial/footer')?>