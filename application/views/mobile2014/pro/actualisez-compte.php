<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-white">
    <!--forms-->
    <div class="consommation wrap forms">

    <div class="product first">
            <div class="details pad-tb-10">
				<div class="txt-product">
                            <strong><?php echo $client->Nom.' '.$client->Prenom?></strong>
                            <span><?php echo $client->NomVille?> </span>
                            <table border="0" width="100%">
                                <tr>
                                    <td width="60"><div class="pad-lr-5"><a href="<?php echo site_url('front/fidelity_pro/user/'.$client->IdUser);?>"><img src="<?php echo img_url("id_card.png") ?>" alt="" width="50"/></a></div></td>
                                    <td width="60"><div class="pad-lr-5"><a href="mailto:<?php echo $client->Email;?>"><img src="<?php echo img_url("email.png") ?>" alt="" width="50"/></a></div></td>
                                    <td><div class="pad-lr-5"><a href="tel:<?php echo $client->Telephone;?>"><img src="<?php echo img_url("mobile_phone.png") ?>" alt="" width="50"/></a></div></td>
                                </tr>
                            </table>
                </div>
            </div>
            <?php //if($fidelity): print_r($fidelity) 
				$solde = (is_object($fidelity)) ? $fidelity->solde : 0;
				$objectif = (is_object($fidelity) && isset($fidelity->objectif)) ? $fidelity->objectif : 0;
				$description = (is_object($fidelity)) ? $fidelity->description : '';
				$fiche_id = (is_object($fidelity)) ? $fidelity->fiche_id : 0;
				//var_dump($fiche_id);
			?>

	            <?php if($offre == "capital" || $offre == "tampon"):?>
		            <span><?php echo ($offre == "capital") ? "Capitalisation" : "Tampon"?> : <?php echo (!empty($description)) ? $description : ''?></span>
		            <?php if(intval($solde) < intval($objectif)):?>
		            <div class="pure-g bg-red no-radius">
		                <div class="<?php echo blue_class_progress($solde, $objectif)?>">
		                    <div class="pad-lr-5"><?php echo (is_object($fidelity)) ? $solde : 0?><?php echo ($offre === "capital") ? "&euro;" : "" ?></div>
		                </div>
		                <div class="<?php echo red_class_progess($solde, $objectif)?>">
		                	<div class="pad-lr-5">
		                		<?php echo (!empty($objectif)) ? $objectif : 0?><?php echo ($offre == "capital") ? "&euro;" : "" ?>
		                	</div>
		                </div>
		            </div>
		            <?php else: ?>
		            <div class="pure-g bg-red align-right pad-tb-10 f-size14">
				                	<div class="pad-lr-5">
				                				<?php echo (!empty($solde)) ? $solde : 0?>;
				                	</div>
				    </div>
		            <?php endif;?>
		        <?php endif;?>

		         <?php if($offre === "remise"):?>
		          		<span>Remise directe : <?php echo (!empty($description)) ? $description : ''?></span>
			            <div class="pure-g bg-blue">
				                <div class="pure-u-1-2 bg-blue align-left">
				                    <div class="pad-lr-5">Cumul consommation</div>
				                </div>
				                <div class="pure-u-1-2 bg-blue align-right">
				                	<div class="pad-lr-5">
				                		<b>
				                				<?php echo (!empty($solde)) ? $solde : 0;?> &euro;
				                		</b>
				                	</div>
				                </div>
				            </div>
		          <?php endif;?>
		           <?php if($offre):?>
		           <form action="<?php echo site_url('front/fidelity_pro/reinistialisation')?>" class="pure-form pure-form-stacked no-padd" method="POST" id="form-actualisation">
	                <div class="input">
	                    <input type="text" class="pure-input-1" placeholder="Noter le report à nouveau"/ name="montant_init">
	                    <input type="hidden" value="<?php echo $fiche_id?>" name="fiche_id" />
	                    <input type="hidden" value="<?php echo $offre?>" name="type_offre" />
	                    <input type="hidden" value="<?php echo $client->IdUser?>" name="id_user" />
	                </div>
	            	<button class="pure-input-1 pure-button pure-button-primary bg-blue" > Remettre à zéro</button>
	
	            </form>
	            <?php endif;?>
            <?php //endif;?>
			<?php if(!empty($user_bonplan)): ?>	
	            <div class="pure-g bg-red marg-bt-10">
	                <div class="pure-u-1-3 bg-red  align-left ">
	                    <div class="pad-lr-5"><?php echo  date("d/m/Y",strtotime(end($user_bonplan)->date_validation));?></div>
	                </div>
	                <div class="pure-u-2-3 bg-red  align-right"><div class="pad-lr-5"><b><?php echo (end($user_bonplan)->valide) ? "BON PLAN VALIDE" : "BON PLAN NON VALIDE";?></b></div></div>
	            </div>
	            <form action="<?php echo site_url('front/fidelity_pro/reinistialisation')?>" class="pure-form pure-form-stacked no-padd" method="POST" id="form-actualisation">
	                <div class="input">
	                    <input type="hidden" value="<?php echo end($user_bonplan)->id_client?>" name="id_user" />
	                    <input type="hidden" value="<?php echo end($user_bonplan)->assoc_client_bonplan_id?>" name="assoc_client_bonplan_id" />
	                </div>
	            	<button class="pure-input-1 pure-button pure-button-primary bg-blue" > Remettre à zéro</button>
            </form>
	         <?php endif;?>   
        </div>
    </div>
    <div class="clr"></div>
</div>
    <!--forms-->
<script type="text/javascript">
<!--
$( "#form-actualisation" ).validate({
			//debug: true,
			rules: {
				'montant_init' : {
					 number: true,
				}
			},
			messages: {
				'montant_init' :{
					number: 'Veuillez fournir un montant valide'
					}
			}
		});
//-->
</script>
</body>
</html>