<script type="text/javascript" src="<?php echo base_url();?>assets/js/aviary.editor.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#delete_capitalisation_photo").click(function(){
			var idcom = jQuery("#idcom").val();
			$.ajax({
			type: "POST",
			url: '<?php echo site_url("front/fidelity_pro/delete_capitalisation_photo"); ?>',
			data: 'idcommercant=' + idcom,
			success: function (zReponse) {
				console.log(zReponse);
				if(zReponse=="error") {
					alert("Une erreur est survenue ! Veuillez re-essayer !");
				} else if(zReponse=="ok"){
					alert("Image supprimer avec succèss");	
					location.reload(true);					
				}

			},
			error: function (zReponse) {
				console.log(zReponse);
			}
		});
		});		
	});	
</script>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="wrap forms ">
      <form class="pure-form pure-form-stacked" action="<?php echo site_url('front/fidelity_pro/editcapital'); ?>" method="post" name="frmcapital" id="frmcapital">
          <br/>
          <div class="pure-u-1">
	          <div class="input inputdate-form pad-5">
	              <label for="module_capital" class="module">Activation du module
	              <input type="checkbox" class="checkbox" name="capital[is_activ]" value="1" id="module_capital" <?php echo (isset($oCapital)  && is_object($oCapital) && $oCapital->is_activ) ? "checked" : ""?>/> </label>
	          </div>
          </div>
          <textarea class="textarea-box" name="capital[description]" id="description" cols="30" rows="5"><?php if (isset($oCapital) && is_object($oCapital)) echo $oCapital->description;?></textarea>
          <div class="pure-u-1">
	          <div class="input inputdate-form">
	              <label for="">Date debut:</label>
	              <input type="text" class="pure-input-1" name="capital[date_debut]" id="date_debut" value="<?php echo (isset($oCapital) && is_object($oCapital)) ? convert_Sqldate_to_Frenchdate($oCapital->date_debut) : 'cliquez ici';?>"/>
	          </div>
          </div>
          <div class="pure-u-1">
	          <div class="input inputdate-form">
	              <label for="">Date fin: </label>
	              <input type="text" class="pure-input-1" name="capital[date_fin]" id="date_fin" value="<?php echo (isset($oCapital) && is_object($oCapital)) ? convert_Sqldate_to_Frenchdate($oCapital->date_fin) : 'cliquez ici';?>"/>
	          </div>
          </div>
          <div class="pure-g">
              <div class="pure-u-7-8">
                  <div class="input inputdate-form">
                      <label for="">Objectif</label>
                      <input type="text" class="pure-input-1" name="capital[montant]" id="montant"  value="<?php echo (isset($oCapital) && is_object($oCapital)) ? $oCapital->montant : 0;?>"/>
                  </div>
              </div>
              <div class="pure-u-1-8"><div class="pad-10 unit">&euro;</div></div>
          </div>
          <!-- Ajout d'image -->
		  <div class="pure-u-1">
			<div class="input inputdate-form pad-5">
				<div id="Articlebackground_image_container">
					<?php if (isset($oCapital->image1) && $oCapital->image1!= null ) { ?>
						<input type="hidden" id="idcom" value="<?php echo $oCapital->id_commercant; ?>">
						<img src="<?php echo('/application/resources/front/photoCommercant/imagesbank/'.$oCapital->id_ionauth.'/capitalisation_photo/'.$oCapital->image1) ?>">
                        <a href="javascript:void(0);" class="btn btn-danger" onclick="return false;" id="delete_capitalisation_photo"><button>Supprimer</button></a>
    				<?php }else {?>
                        <a href="javascript:void(0);" title="Photo1" class="btn btn-info" onclick="javascript:window.open('<?php echo site_url('media/index/'.$oCapital->id_commercant.'-capitalisation_photo-image1') ?>','Commercant image','width=1045, height=725, scrollbars=yes');" id="Articlebackground_link"><button type="button" class="btn btn-info">Ajouter une image</button>
    					</a>
					<?php  }?>
				</div>
			</div>
		  </div>
		  <!-- Fin -->
          <div class="pad-tb-5 accordeon">
              <button class="pure-button pure-button-primary" > Validation </button>
          </div>
		<input name="capital[id]" id="id" type="hidden" value="<?php if (isset($oCapital) && is_object($oCapital)) echo $oCapital->id; else echo "0";?>" />

      </form>
    </div>
    <!--forms-->
    <div class="clr"></div>
</div>
<script type="text/javascript">
<!--
$(function() {
	$("#date_debut").datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:2020',
		onSelect: function(dateText) {
			$( "#date_debut" ).valid();
		}
	});
	$("#date_fin").datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
		changeYear: true,
		yearRange: '1900:2020',
		onSelect: function(dateText) {
			$( "#date_fin" ).valid();
		}
	});
	$( "#frmcapital" ).validate({
		//debug: true,
		rules: {
			'capital[description]' : {
				required: true
			},
			'capital[date_fin]': {
				required: true
			},
			'capital[date_debut]': {
				required: true
			},
			'capital[montant]': {
				required: true
			}
		}
	});
});
//-->
</script>
<?php $this->load->view('mobile2014/partial/footer')?>