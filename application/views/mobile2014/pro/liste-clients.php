<?php //echo "<pre>";
$this->load->view('mobile2014/partial/head-pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
<?php if(!$print):?>
    <div class="wrap">
        <div class="pure-g">
            <div class="pure-u-3-5">
               <!--  
                <form action="">
                <div class="search align-center">
                    <input type="text" />
                    <button class="submit" type="submit"><span class="fa fa-check"></span></button>
                </div>
                </form>
                -->
            </div>
            <div class="pure-u-2-5">
                <div class="btn-menu pad-10">
                    <a href="<?php echo site_url('front/fidelity/filtre_client')?>">Filtrer</a>
                </div>
            </div>
        </div>
        <div class="pure-g">
            <div class="pure-u-1-2">
                <div class="btn-menu-2">
                    <a href="<?php echo site_url('front/fidelity/liste_contact/1/asc/print')?>" class="radius-brtr-0 btnPrint">Imprimez</a>
                </div>
            </div>
            <div class="pure-u-1-2">
                <div class="btn-menu-2">
                    <a href="<?php echo site_url('front/fidelity/liste_contact/1/asc/csv')?>" class="radius-bltl-0">Export CSV</a>
                </div>
            </div>
        </div>
    </div>
    <!--LOGO-->
</div>
<?php endif;?>
<div class="wrapper page-white">
<?php if(!$print):?>

<form method="post" action="<?php echo site_url('front/fidelity/send_message_client')?>" id="clientMessage">

	<?php if(!empty($email_clients)) :foreach ($email_clients as $email):?>
		<input type="hidden" value="<?php echo $email?>" name="email_clients[]">
	<?php endforeach; endif;?>
    <a href="mailto:<?php echo $mail_to_clients?>"  class="link-rose">Adresser un message direct <br/> à cette séléction</a>
</form>
 <?php endif;?>
    <!--forms-->
    <div class="consommation">
    	<?php if(!empty($list_client)):?>
    		<?php foreach ($list_client as $client): if($client->date_validation != '0000-00-00'):?>
    			<?php if($client->valide):?>
				    <div class="product first">
		
		                <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                    <tr>
		                        <td>
		                            <div class="details f-size14 pad-10 no-marg">
		                            	<?php echo $client->Prenom.' '.$client->Nom;?>
		                            </div>
		                        </td>
		                        <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity/detail_client/'.$client->IdUser)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
		                    </tr>
		                </table>
		
			            <div class="pure-g bg-blue">
			                <div class="pure-u-1-2 bg-blue align-left">
			                    <div class="pad-lr-5">Cumul consommation</div>
			                </div>
			                <div class="pure-u-1-2 bg-blue align-right">
			                	<div class="pad-lr-5">
			                		<b>
			                		<?php if($client->param_captital_active){
			                				echo (!empty($client->solde_capital)) ? $client->solde_capital: 0 ;echo " &euro;";
			                			}elseif ($client->param_tampon_active){
			                				echo (!empty($client->solde_tampon)) ? $client->solde_tampon : 0 ;
			                			}elseif($client->param_remise_active){
			                				echo (!empty($client->solde_remise)) ? $client->solde_remise : 0;echo " &euro;";
			                			}  		?> 
			                		
			                		</b>
			                	</div>
			                </div>
			            </div>
		
			            <div class="pure-g bg-red marg-bt-10">
			                <div class="pure-u-1-2 bg-red  align-left ">
			                    <div class="pad-lr-5"><?php echo  date("d/m/Y",strtotime($client->date_validation));?></div>
			                </div>
			                <div class="pure-u-1-2 bg-red  align-right"><div class="pad-lr-5"><b>BON PLAN VALIDE</b></div></div>
			            </div>
		       		</div>
		       	<?php endif;?>	
		        <?php if($client->valide == 0):?>
		        <div class="product first">
		                <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                    <tr>
		                        <td>
		                            <div class="details f-size14 pad-10 no-marg">
		                            	<?php echo $client->Prenom.' '.$client->Nom;?>
		                            </div>
		                        </td>
		                        <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity/fidelity_card_operations/'.$client->IdUser.'/bonplan')?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
		                    </tr>
		                </table>
		                <div class="pure-g bg-red marg-bt-10">
			                <div class="pure-u-1-3 bg-red  align-left ">
			                    <div class="pad-lr-5"><?php echo  date("d/m/Y",strtotime($client->date_validation));?></div>
			                </div>
			                <div class="pure-u-2-3 bg-red  align-right"><div class="pad-lr-5"><b>BON PLAN NON VALIDE</b></div></div>
			            </div>
		        </div>
		        <?php endif;?>
		        
		        <!-- 
		        <?php if(isset($capitalisation) && $capitalisation && !is_null($client->solde_capital) && $client->valide):?>
		        <div class="product first">
		                <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                    <tr>
		                        <td>
		                            <div class="details f-size14 pad-10 no-marg">
		                            	<?php echo $client->Prenom.' '.$client->Nom;?>
		                            </div>
		                        </td>
		                        <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity/detail_client/'.$client->IdUser)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
		                    </tr>
		                </table>
		
		            <span>Capitalisation : <?php echo (!empty($client->description_capital)) ? $client->description_capital : ''?></span>
		            <div class="pure-g bg-red">
		                <div class="pure-u-1-2 bg-blue pad-tb-5 align-right price">
		                    <div class="pad-lr-5"><?php echo (!empty($client->solde_capital)) ? $client->solde_capital : 0?>&euro;</div>
		                </div>
		                <div class="pure-u-1-2 bg-red pad-tb-5 align-right price "><div class="pad-lr-5"><?php echo (!empty($client->objectif_capital)) ? $client->objectif_capital : 0?> &euro;</div></div>
		            </div>
		        </div>
		        <?php endif;?>
		        
		        <?php if(isset($tampon) && $tampon && !is_null($client->solde_tampon) && $client->valide):?>
		        <div class="product first">
		                <table cellpadding="0" cellspacing="0" border="0" width="100%">
		                    <tr>
		                        <td>
		                            <div class="details f-size14 pad-10 no-marg">
		                            	<?php echo $client->Prenom.' '.$client->Nom;?>
		                            </div>
		                        </td>
		                        <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity/detail_client/'.$client->IdUser)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
		                    </tr>
		                </table>
		
		            <span>Coups de Tampon : <?php echo (!empty($client->description_tampon)) ? $client->description_capital : ''?></span>
		            <div class="pure-g bg-red">
		                <div class="pure-u-1-2 bg-blue pad-tb-5 align-right price">
		                    <div class="pad-lr-5"><?php echo (!empty($client->solde_tampon)) ? $client->solde_tampon : 0?></div>
		                </div>
		                <div class="pure-u-1-2 bg-red pad-tb-5 align-right price "><div class="pad-lr-5"><?php echo (!empty($client->tampon_value)) ? $client->tampon_value : 0?></div></div>
		            </div>
		        </div>
		        <?php endif;?>
		         -->
		       <?php endif; endforeach;?> 
		  <?php else :?>
		   <div class="product first">
		  	Aucun enregistrement trouvé
		  	</div>     
	      <?php endif?>  
    </div>
    <div class="clr"></div>
</div>
    <!--forms-->
</div>

<?php 
if(!$print):
$this->load->view('mobile2014/partial/menu_pro');
endif;
?>
<script type="text/javascript">
<!--
$(document).ready(function() {
    $(".btnPrint").printPage();
  });
//-->
</script>
</body>
</html>