<?php //var_dump($assoc_client_bonplan);?>
<?php $this->load->view('mobile2014/partial/head-pro')?>
<div class="wrapper head">
    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
    <?php $this->load->view('mobile2014/partial/menu_pro')?>
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper page-black">
	<!--forms-->
	<div class="wrap forms">
		<form class="pure-form pure-form-stacked" id="formCreationConsommateur" action="<?php echo site_url("front/particuliers/ajouter_via_pro"); ?>" method="POST">
			<div id="forms-tabs">
				<!--  <ul class="pad-tb-5 pure-g btnul">
					<li class="pure-u-1-2  ui-tabs-active ui-state-active"
						id="li_page1"><a href="#form-1" class="pure-button btn-onglet"
						id="page1">Page 1</a></li>
					<li class="pure-u-1-2" id="li_page2"><a href="#form-2"
						class="pure-button btn-onglet" id="page2">Page 2</a></li>
				</ul>-->
				<div id="form-1">
					<div class="input">
			               <select class="" name="Particulier[Civilite]"  style="width:100%;" id="select2">
			                            <option value="0">Monsieur</option>
			                            <option value="1">Madame</option>
			                            <option value="2">Mademoiselle</option>
			                </select>
	         		 </div>
					<div class="input">
						<input type="text" class="pure-input-1 clearable "
							placeholder="Nom" name="Particulier[Nom]" />
					</div>
					<div class="input">
						<input type="text" class="pure-input-1 clearable "
							placeholder="Prénom" name="Particulier[Prenom]" />
					</div>
					<div class="input">
						<input type="text" class="pure-input-1 clearable datepicker"
							placeholder="Date de naissance" name="Particulier[DateNaissance]" />
					</div>
					<div class="input">
						<input type="text" class="pure-input-1 clearable"
							placeholder="Adresse" name="Particulier[Adresse]" />
					</div>
					<!--  
					<div class="input">
						<input type="text" class="pure-input-1 clearable" placeholder="CP" name="Particulier[CodePostal]" onchange ="getListCityByCP($(this).val());"/>
					</div>
					
					<div class="input">
		          	  <select name="Particulier[IdVille]"  id="ville" style="width:100%">
		                
		              </select>
		           </div>
		           -->
		          <div class="input">
		              <input type="text" class="pure-input-1 " placeholder="Codel postal" value="<?php echo isset($oParticulier) ? $oParticulier->CodePostal : '';?>" name="Particulier[CodePostal]" onchange ="getListCityByCP($(this).val());"/>
		          </div>
		          <div class="input">
		          	  <select name="" style ="width:100%;" id="departement_id">
		              </select>
		           </div>
		         <div class="input">
		          	  <select name="Particulier[IdVille]" style ="width:100%;" id="ville">
		              </select>
		           </div>
					<div class="input">
						<input type="text" class="pure-input-1 clearable"
							placeholder="Téléphone mobile" name="Particulier[Telephone]" />
					</div>
				</div>
				<!--  div id="form-2" style="display: none"-->
				<div>
					<div class="input pure-g">
						<div class="pure-u-2-3">
							<input type="text" placeholder="Identifiant" class="pure-input-1" name="Particulier[Login]"/>
						</div>
						<div class="pure-u-1-3 pad-tb-10 mail-carte">@sortez.org
						</div>
					</div>
					<!--  
                  <div class="input">
                      <label for="">Mot de passe</label>
                      <input type="text" class="pure-input-1 clearable " placeholder="Noter le numeros de carte"/>
                  </div>
                  <div class="pad-tb-5">
                      <div class="align-center">
                          <img src="img/carte.png" alt=""/>
                      </div>
                  </div>
			-->
			<div class="accordeon">
					<div class="pad-tb-5">
						<button class=" pure-button pure-button-primary">Validation de la carte</button>
					</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<!--forms-->
	<div class="clr"></div>
</div>

<script type="text/javascript">
$("#ville,#departement_id").select2({
	  placeholder: "Veuillez remplir le code postal ",
	  allowClear: true
	});

$( ".datepicker" ).datepicker({
	dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
	dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
	monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
	dateFormat: 'DD, d MM yy',
	autoSize: true,
	changeMonth: true,
    changeYear: true,
	yearRange: '1900:2020',
	onSelect: function(dateText) {
		$( this ).valid();
	}
});
$(function(){
	$("#ville").select2({
	  placeholder: "Veuillez remplir le code postal ",
	  allowClear: true
	});
	$('#select2').select2();
});

function getListCityByCP(cp){
	if(cp != '' || typeof cp != 'undefined'){
		$.ajax({
			  type: "POST",
			  url: "<?php echo site_url("front/particuliers/getListCityByCP/"); ?>/"+cp,
			  data: 'cp='+cp,
			  success: function(data){
				  $("#ville").empty().append(data.list);
				  $("#departement_id").empty().append(data.list_departement);
				  $("#ville").select2();
				  $("#departement_id").select2();
			  },
			  dataType: 'json'
		});
	}
}
$( "#formCreationConsommateur" ).validate({
	rules: {
		'Particulier[Nom]': {
			required: true
		},
		'Particulier[Prenom]': {
			required: true
		},
		'Particulier[DateNaissance]': {
			required: true
		},
	/*	'Particulier[Adresse]': {
			required: true
		},*/
		'Particulier[CodePostal]': {
			required: true,
			minlength: 5,
			maxlength :5
		},
		'Particulier[Telephone]': {
			required: true
		},
		'Particulier[Login]': {
			required: true,
		  	remote: {
				 url: "<?php echo site_url('front/fidelity/checkmail_login')?>",
					 type: "post",
					 data: {
						 login: function() {
						 	return $("input[name$='Particulier[Login]']").val()+'@privicarte.fr';
						 }
					 }
				}	
			}
		},
		'Particulier[IdVille]': {
			required: true,
		},
	messages: {
		'Particulier[Login]': {
			remote: "Ce mail est déjà associé à un compte"
		},
	}
});
</script>
<?php $this->load->view('mobile2014/partial/footer')?>