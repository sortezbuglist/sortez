<?php $this->load->view('mobile2014/partial/head-pro')?>
<?php $this->load->view('mobile2014/partial/menu_pro')?>
<div class="wrapper head">
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro')?>
</div>
<div class="wrapper">
    <!--liens-->
    <div class="consommation-filtre ">
        <div class="list2">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td valign="top" width="23%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url("ico-calendar.png");?>" alt=""/></a>
                        </div>
                    </td>
                    <td valign="top">
                        <table cellpadding="0" cellspacing="0" border="0" width="100%" >
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                   <span> Janvier (<?php echo $mois['janvier'];?>)</span>
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/1')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Fevrier (<?php echo $mois['fevrier'];?>)
                                </td>
                                <td>
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/2')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Mars (<?php echo $mois['mars'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/3')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Avril (<?php echo $mois['avril'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/4')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Mai (<?php echo $mois['mai'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/5')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Juin (<?php echo $mois['juin'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/6')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Juillet (<?php echo $mois['juillet'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/7')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Août (<?php echo $mois['aout'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/8')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Septembre (<?php echo $mois['septembre'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/9')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Octobre (<?php echo $mois['octobre'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/10')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Novembre (<?php echo $mois['novembre'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/11')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                            <tr class="bordered">
                                <td valign="bottom" width="80%" class="pad-lr-10">
                                    Décembree (<?php echo $mois['decembre'];?>)
                                </td>
                                <td >
                                    <a href="<?php echo site_url('front/fidelity_pro/liste_clients/mois/12')?>" class="bg-grey-round"><span class="fa fa-check"></span></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

    </div>
    <div class="clr"></div>
</div>
</body>
</html>



