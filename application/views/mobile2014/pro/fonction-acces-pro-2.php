<?php
$this->load->view('mobile2014/partial/head-pro') ?>
<?php $this->load->view('mobile2014/partial/menu_pro') ?>

    <div class="wrapper head">
        <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
        <?php $this->load->view('mobile2014/partial/header_titre_menu_carte_pro') ?>

    </div>
    <div class="wrapper">

        <!--liens-->
        <div class="wrap links">
            <span class="success"><?php echo (isset($message_success)) ? $message_success : '' ?></span>
            <?php if (isset($toCommercant->bonplan) && $toCommercant->bonplan == "1") { ?>
                <div class="pad-tb-5">
                    <a href="<?php echo site_url('front/bonplan/ficheBonplan/' . $comm_id . '/?from_privicarte=1'); ?>"
                       class="pure-button pure-button-primary">Paramètre
                        du
                        bon
                        plan</a>
                </div>


                <?php if ($show_remise_active || $show_all): ?>
                    <div class="pad-tb-5">
                        <a href="<?php echo site_url('front/fidelity_pro/remisedirect') ?>"
                           class="pure-button pure-button-primary">Paramètre
                            "remise
                            directe"</a>
                    </div>
                <?php endif; ?>
                <?php if ($show_capital_active || $show_all): ?>
                    <div class="pad-tb-5">
                        <a href="<?php echo site_url('front/fidelity_pro/capital') ?>"
                           class="pure-button pure-button-primary">Paramètre
                            "coup
                            de
                            capitalisation"</a>
                    </div>
                <?php endif; ?>
                <?php if ($show_tampon_active || $show_all): ?>
                    <div class="pad-tb-5">
                        <a href="<?php echo site_url('front/fidelity_pro/tampon') ?>"
                           class="pure-button pure-button-primary">Paramètre
                            "coup
                            de
                            tampons"</a>
                    </div>
                <?php endif; ?>

            <?php } else { ?>

                <div class="alert alert-danger"
                     role="alert">
                    <strong>Votre abonnement ne vous permet pas d'accéder à ces parametres !</strong>
                    
                </div>

            <?php } ?>

        </div>
        <!--liens-->
        <div class="clr"></div>
    </div>
<?php $this->load->view('mobile2014/partial/footer') ?>


<style type="text/css">
    .bd-example>.alert+.alert {
        margin-top: 1rem;
    }
    .alert-danger {
        background-color: #f2dede;
        border-color: #ebcccc;
        color: #a94442;
    }
    .alert {
        padding: .75rem 1.25rem;
        margin-bottom: 1rem;
        border: 1px solid transparent;
        border-radius: .25rem;
    }
</style>
