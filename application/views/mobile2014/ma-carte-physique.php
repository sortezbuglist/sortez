<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
<div class="wrapper head">
    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
     <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
                <?php $this->load->view('mobile2014/partial/header_titre')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="padded">
        <div class="wrap forms">
            <form class="pure-form pure-form-stacked">
                <div id="forms-tabs">
                	<!--  
                    <ul class="pad-tb-5 pure-g btnul btnsave">
                        <li class="pure-u-1-2"><a href="#content-1" class="pure-button btn-onglet">Premier</a></li>
                        <li class="pure-u-1-2"><a href="#content-2" class="pure-button btn-onglet">Deuxième</a></li>
                    </ul>
                    -->
                    <div id="content-1">
                        <div class="pure-g cart-phys">
                            <div class="pure-u-1">
                                <p class="f-size16">
                                    Comment se procurer des cartes privicarte au format carte bancaire?
                                </p>
                            </div>
                            <div class="pure-u-3-5">
                                <div class="pad-15">
                                    <img src="<?php echo img_url('carteprivicarte.png')?>" alt=""/>
                                </div>
                            </div>
                            <!-- 
                            <div class="pure-u-2-5">
                                <div class="pad-15">
                                    <a href="<?php echo site_url('front/user_fidelity/print_card') ?>">
                                        <img src="<?php echo img_url('print.png')?>" alt=""/>
                                    </a>
                                </div>
                            </div>
 -->
                            <div class="pure-u-1">
                                <p class="f-size14">
                                    Imprimez votre bon de retrait! <br/>Puis déplacez vous chez le partenaire de votre choix. <br/>
                                    Sur présentaton de ce bon, <br/>il vous remettra une carte personnelle
                                </p>
                            </div>
                        </div>

                        <div class="pad-tb-5 links">
                            <a href="<?php echo site_url('front/user_fidelity/print_card') ?>" class="pure-button pure-button-primary" >Imprimer votre bon de retrait</a>
                        </div>
                    </div>
                    <div id="content-2" class="cart-phys">

                        <div class="pure-g cart-phys">
                         <div class="pure-u-2-5">
                                <div class="pad-30">
                                    <img src="<?php echo img_url('flecheverte.png')?>" alt=""/>
                                </div>
                            </div>
                            <div class="pure-u-3-5">
                                <p class="f-size14">
                                    Pour rendre valide, vous devez noter son numéro et valider sa liaison avec votre compte consommateur
                                    <br/>
                                    Une seule carte valide par compte
                                </p>
                            </div>

                            <div class="pure-u-1">
		                          <div class="pad-tb-5">
		                              <input type="text" class="pure-input-1 clearable" placeholder="Noter ici le numéro de votre carte" name="num_card_verification" id="num_card_verification"/>
		                          </div>
		                          <div class="pad-tb-5">
		                              <input type="button" class="pure-input-1 pure-button pure-button-primary" value="Liez cette carte à votre compte" onclick="javascript:check_num_card();" />
		                          </div>
		                          <div class="pad-tb-5">
		                           		<span id="span_check_num_card"></span>
		                          </div>
	                      </div>


                        </div>

                    </div>
                </div>


            </form>
        </div>
    <!--forms-->
    <div class="clr"></div>
    </div>
</div>
<script type="text/javascript">
function check_num_card(){
	
	var zErreur = "" ;
	var num_card_verification = $("#num_card_verification").val();
	
	$('#span_check_num_card').html('<img src="<?php echo base_url();?>application/resources/front/images/wait.gif" alt="loading...."/>');
	
	$.post(
		'<?php echo site_url("front/user_fidelity/check_num_card");?>',
		{ 
		num_card_verification: num_card_verification
		},
		function (zReponse2)
		{
			if (zReponse2 == "1") {
				$('#span_check_num_card').html('<span style="color:#090">Votre carte est maintenant lié à votre compte !</span>');
			} else if (zReponse2 == "2") {
				$('#span_check_num_card').html('<span style="color:#FF0000">Votre carte est déjà lié à votre compte!</span>');
			} else if (zReponse2 == "3") {
				$('#span_check_num_card').html('<span style="color:#FF0000">Votre carte n\a pas encore été activée !</span>');
			} else {
				$('#span_check_num_card').html('<span style="color:#FF0000">Ce numéro de carte est invalide !</span>');
			}
	   });
	
}
</script>
<?php $this->load->view('mobile2014/partial/footer')?>