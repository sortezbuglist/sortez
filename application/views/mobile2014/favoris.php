<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
<div class="list_bonplan">
<div class="wrapper page-white">
<?php  if(!empty($toCommercant) && $toCommercant): foreach($toCommercant as $oCommercant):?> 
		 <div class="bordbot">
 		<hr>  
		 <div class="pure-g bplan">
            <div class="pure-u-2-5">
            	<?php 
                    $image_home_vignette = "";
                    if (isset($oCommercant->PhotoAccueil) && $oCommercant->PhotoAccueil != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->PhotoAccueil)==true){ $image_home_vignette = $oCommercant->PhotoAccueil;}
                    else if (isset($oCommercant->Photo1) && $oCommercant->Photo1 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo1)==true){ $image_home_vignette = $oCommercant->Photo1;}
                    else if (isset($oCommercant->Photo2) && $oCommercant->Photo2 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo2)==true){ $image_home_vignette = $oCommercant->Photo2;}
                    else if (isset($oCommercant->Photo3) && $oCommercant->Photo3 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo3)==true){ $image_home_vignette = $oCommercant->Photo3;}
                    else if (isset($oCommercant->Photo4) && $oCommercant->Photo4 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo4)==true){ $image_home_vignette = $oCommercant->Photo4;}
                    else if (isset($oCommercant->Photo5) && $oCommercant->Photo5 != "" && is_file("application/resources/front/photoCommercant/images/".$oCommercant->Photo5)==true){ $image_home_vignette = $oCommercant->Photo5;}
                    ?>
                    <img src="<?php if ($image_home_vignette != ""){ echo GetImagePath("front/photoCommercant/"); ?>/<?php echo $image_home_vignette ; }else{echo GetImagePath("front/")."/wp71b211d2_06.png";}?>" width="120" border="0" alt="<?php ;?>">
            </div>
            <div class="pure-u-3-5">
                <div class="bonplan-text f-size14">
                     <?php echo ($oCommercant->Nom);?>
                    <strong><?php echo ($oCommercant->NomSociete) ; ?></strong>
                     <!--   <?php echo $oCommercant->rue ; ?><?php echo ($oCommercant->quartier) ; ?>-->
                   <?php echo $oCommercant->ville ; ?>
                    <br/>
                    
                </div>
            </div>
        </div>
        <div class="pure-g bplan-offer bg-pink bplan">
            <div class="pure-u-1-3 bg-pink">
                <div class="pad-tb-20  align-center txt_bonplan">
                	<b>BON</b><br/><span>PLAN</span>
                </div>
            </div>
            <div class="pure-u-1-3 bg-pink desc-100">
                <div class="pad-tb-20 pad-lr-10 f-size14 align-center">
                	<?php  echo $oCommercant->titre_entete ; ?>
                </div>
            </div>
            <div class="pure-u-1-3 bg-pink ">
            	 <a class="btn-link" href="<?php echo site_url($oCommercant->nom_url."/notre_bonplan");?>"><span class="fa fa-chevron-right"></span></a>
            </div>
        </div>
        
        <?php $fidelity = get_fidelity($oCommercant->IdCommercant)?>
        <?php if($fidelity->has_fidelity) :
        	$description= '';
        	if($fidelity->is_tampon_active){
        		$description = $fidelity->tampon->description;
        	}
        	if($fidelity->is_capital_active){
        		$description = $fidelity->capital->description;
        	}
        	if($fidelity->is_remise_active){
        		$description = $fidelity->remise->description;
        	} 
        ?>
	         <div class="pure-g bplan-offer bg-blue bplan fidelisation">
	            <div class="pure-u-1-3 bg-blue">
	                <div class="pad-tb-20 pad-lr-10 f-size14 align-center txt_fidelite">
	                	<b>OFFRE</b> <span>FIDELITE</span> 
	                </div>
	            </div>
	            <div class="pure-u-1-3 bg-blue desc-100">
	                <div class="pad-tb-20 pad-lr-10 f-size14 align-center">
	                    <?php echo truncate(strip_tags($description),100," ...");?>
	                </div>
	            </div>
	             <div class="pure-u-1-3 bg-blue ">
            	 <a class="btn-link" href="<?php echo site_url($oCommercant->nom_url."/notre_bonplan");?>"><span class="fa fa-chevron-right"></span></a>
            	</div>
	        </div>
        <?php endif;?>
       <div class="capitalisation accordeon-child btn-menu">
              	<a class="pure-button pure-button-primary pad-tb-10 pad-lr-60 f-size14" href="<?php echo site_url('front/user_fidelity/detail_offre/'.$oCommercant->IdCommercant)?>"> Ma consommation</a>
          </div>
    </div>
<?php endforeach;?>
<?php else :?>
 <div class="paragraphe">
	Vous n'avez pas de favoris
</div>	
<?php endif;?>
    <!--liens-->
    <div class="clr"></div>
</div>
</div>    
<?php $this->load->view('mobile2014/partial/footer')?>