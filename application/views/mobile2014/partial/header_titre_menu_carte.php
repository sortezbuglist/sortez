<!--LOGO-->
    <div class="wrap">
        <div class="pure-g">
            <div class="pure-u-3-5">
                <div class="pad-tb-15 titre align-center">
                    <?php echo $titre;?>
                </div>
            </div>
            <div class="pure-u-2-5">
                <div class="btn-menu">
                    <a href="<?php echo site_url('front/utilisateur/menuconsommateurs')?>">Menu carte</a>
                </div>
            </div>
        </div>
    </div>
    <!--LOGO-->