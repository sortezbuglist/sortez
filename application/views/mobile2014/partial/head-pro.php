<!DOCTYPE html>
<html>
<!--[if lte IE 7]> <html class="no-js ie7 oldie" lang="fr"> <![endif]-->
<!--[if IE 8]> <html class="no-js ie8 oldie" lang="fr"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<head>
    <title>Privicarte</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="<?php echo css_url('grids-core')?>" rel="stylesheet">
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php echo css_url('grids-responsive-old-ie-min')?>">
    <![endif]-->
    <!--[if gt IE 8]><!-->
    <link rel="stylesheet" href="<?php echo css_url('grids-responsive-min')?>">
    <!--<![endif]-->
    <link href="<?php echo css_url('pure-min')?>" rel="stylesheet">
    <link href="<?php echo css_url('grids-responsive-min')?>" rel="stylesheet">
    <link href="<?php echo css_url('forms')?>" rel="stylesheet">
    <link href="<?php echo css_url('forms-r')?>" rel="stylesheet">
    <link href="<?php echo css_url('grids-responsive-min')?>" rel="stylesheet">
    <link href="<?php echo css_url('ui/jquery-ui-1.10.4.custom.min')?>" rel="stylesheet">
    <link href="<?php echo css_url('font-awesome.min')?>" rel="stylesheet">
    <link href="<?php echo css_url('style-pro')?>" rel="stylesheet">
    <link href="<?php echo css_url('select2.min')?>" rel="stylesheet">
    <link href="<?php echo css_url('flat/blue')?>" rel="stylesheet">
    <link href="<?php  echo css_url('custom')?>" rel="stylesheet">
    
    <script type="text/javascript" src="<?php echo js_url('lib/jquery-1.10.2')?>"></script>
    <script type="text/javascript" src="<?php echo js_url('lib/ui/jquery-ui-1.10.4.custom.min')?>"></script>
    <script type="text/javascript" src="<?php echo js_url('lib/jquery.placeholder.min')?>"></script>
    <script type="text/javascript" src="<?php echo js_url('lib/jquery.clearsearch')?>"></script>
   	<script type="text/javascript" src="<?php echo js_url('jquery.validate.min')?>"></script>
   	<script type="text/javascript" src="<?php echo js_url('messages_fr')?>"></script>
    <script type="text/javascript" src="<?php echo js_url('global')?>"></script>
        <script type="text/javascript" src="<?php echo js_url('jquery.printPage')?>"></script>
     <script type="text/javascript" src="<?php echo js_url('select2.min')?>"></script>
     <script type="text/javascript" src="<?php echo js_url('icheck.min')?>"></script>
    
    
    <!--[if lt IE 9]><script src="<?php echo js_url('html5shiv')?>"></script><![endif]-->
    <!--[if gte IE 9]>
        <style type="text/css">
            .gradient {
                filter: none;
            }
        </style>
    <![endif]-->
</head>
<body>