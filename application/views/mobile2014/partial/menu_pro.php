<div class="menu_pro">
        <div class="pure-g">
            <div class="pure-u-1-6"><div class="pad-lr-5 pad-tb-10"><a href="javascript:history.back(1)"><img src="<?php echo img_url('icon-retour.png') ?>" alt=""/></a></div></div>
            <div class="pure-u-1-6"><div class="pad-lr-5 pad-tb-10"><a href="<?php echo site_url('front/fidelity/contenupro')?>"><img src="<?php echo img_url('icon-menu.png') ?>" alt=""/></a></div></div>
            <div class="pure-u-1-6"><div class="pad-lr-5 pad-tb-10"><a href="<?php echo site_url('front/fidelity_pro/liste_clients')?>"><img src="<?php echo img_url('icon-clients.png') ?>" alt=""/></a></div></div>
            <div class="pure-u-1-6"><div class="pad-lr-5 pad-tb-10"><a href="<?php echo site_url('front/fidelity_pro/card_scan')?>"><img src="<?php echo img_url('icon-qrcode.png') ?>" alt=""/></a></div></div>
            <div class="pure-u-1-6"><div class="pad-lr-5 pad-tb-10"><a href="<?php echo site_url('front/fidelity/mes_parametres')?>"><img src="<?php echo img_url('icon-parametres.png') ?>" alt=""/></a></div></div>
            <div class="pure-u-1-6"><div class="pad-lr-5 pad-tb-10"><a href="<?php echo site_url('front/fidelity/compteconsommateur_mobile')?>"><img src="<?php echo img_url('icon-infos.png') ?>" alt=""/></a></div></div>
        </div>
    </div>