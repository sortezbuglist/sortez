<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
<div class="wrapper head">
                <?php $this->load->view('mobile2014/partial/header_titre')?>
</div>
<div class="wrapper page-white">
    <!--liens-->
    <div class="localisation">
        <div class="list">
			<form name="geolocalisation_form_user" id="geolocalisation_form_user" method="post"/>
				<table border="0" cellspacing="0" cellpadding="5" style="text-align:center;" align="center">
				  <tr>
				    <td colspan="2"><span style="font-family: Arial, Helvetica, sans-serif;
				    font-weight: 700;
				    line-height: 18px;
				    margin-bottom: 0;
				    font-size:15px;
				    text-align: center;">Choisissez la distance de recherche<br/>des partenaires et des évènements situés à proximité.</span></td>
				  </tr>
				  <tr>
				    <td colspan="2" style="color:#00FF00">&nbsp;<?php if (isset($mssg_return) && $mssg_return!="") echo $mssg_return;?></td>
				  </tr>
				  <tr>
				    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_2" class="geodistance_2" value="2" <?php if (isset($oUser) && $oUser->geodistance=="2") echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
				    <td class="line_left">2 kilomètres</td>
				  </tr>
				  <tr>
				    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_5" class="geodistance_5" value="5" <?php if (isset($oUser) && $oUser->geodistance=="5") echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
				    <td class="line_left">5 kilomètres</td>
				  </tr>
				  <tr>
				    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_10" class="geodistance_10" value="10" <?php if (isset($oUser) && $oUser->geodistance=="10") echo "checked"; else if ($oUser->geodistance=="" || $oUser->geodistance==NULL) echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
				    <td class="line_left">10 kilomètres (par défaut)</td>
				  </tr>
				  <tr>
				    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_20" class="geodistance_20" value="20" <?php if (isset($oUser) && $oUser->geodistance=="20") echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
				    <td class="line_left">20 kilomètres</td>
				  </tr>
				  <tr>
				    <td class="line_right"><input type="radio" name="geodistance" id="geodistance_50" class="geodistance_50" value="50" <?php if (isset($oUser) && $oUser->geodistance=="50") echo "checked";?> onClick="javascript:document.geolocalisation_form_user.submit();"/></td>
				    <td class="line_left">50 kilomètres</td>
				  </tr>
				</table>
			</form>
		</div>			
    </div>
</div>
<div class="clr"></div>

            <?php $this->load->view('mobile2014/partial/footer')?>