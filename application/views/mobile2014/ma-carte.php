<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
   <?php $card_location='application/resources/front/images/cards/';
         $real_path=base_url('application/resources/front/images/cards/')//$this->load->view('mobile2014/partial/titre_head_view')?>
        <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="padded">
        <?php if(isset($obj_user_card->num_id_card_virtual) && file_exists($card_location."qrcode_".$obj_user_card->num_id_card_virtual.".png")) :?>
        <div class="wrap content-image">
            <div class="carte">
                <img src="<?php echo $real_path."qrcode_".$obj_user_card->num_id_card_virtual.'.png';?>" alt="" width="275"/>
                <p>
                     Carte n° <?php echo $obj_user_card->num_id_card_virtual?>
                </p>
            </div>
        </div>
        <div class="text align-center">
            Cette carte de fidelit&eacute; est utilisable sur la totalit&eacute; du r&eacute;seau Sortez.
            <!--  <img src="<?php echo img_url('logo.png')?>" alt="" width="200"/>-->
        </div>
    <!--forms-->
    <!--liens-->
        <div class="wrap links">
            <div class="pad-tb-10">
                <a href="<?php echo site_url('front/user_fidelity/fidelity_event_list_mobile')?>" class="pure-button pure-button-primary">Ma consommation</a>
            </div>
        </div>
        <?php else:?>
     <div class="wrap content-image">
       	<div class="error">Erreur! Veuillez recommander votre carte</div>
     </div>  		
     <?php endif;?>
    <!--liens-->
        <div class="clr"></div>
    </div>
</div>

<?php $this->load->view('mobile2014/partial/footer')?>