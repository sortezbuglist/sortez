<?php $this->load->view('mobile2014/partial/head')?>
<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
</div>
<div class="wrapper">
    <!--liens-->
    <div class="consommation-filtre ">
        <div class="list">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                <tr>
                    <td v-align="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url('home.png') ?>" alt=""/></a>
                        </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo base_url()?>">
                               Page d'accueil
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo base_url()?>" class="bg-black-round"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
                <tr>
                    <td v-align="middle" width="25%">
                        <div class="pad-10">
                            <a href="<?php echo site_url('front/utilisateur/compteconsommateur_mobile')?>"><img src="<?php echo img_url('database.png') ?>" alt=""/></a>
                        </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo base_url()?>">
                               Retour menu général
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo base_url()?>" class="bg-black-round"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
                <tr>
                    <td v-align="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url('tag.png') ?>" alt=""/></a>
                        </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo ($have_card) ? site_url('front/bonplan') : 'javascript:alert_no_carte()';?>">
                               Les bonnes affaires
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/bonplan');?>" class="bg-black-round"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
                <tr>
                    <td v-align="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url('tableau.png') ?>" alt=""/></a>
                        </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo site_url('front/user_fidelity/order_fidelity_card')?>">
                               Ma carte Privicarte
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/user_fidelity/order_fidelity_card')?>" class="bg-black-round"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
                <tr>
                    <td v-align="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url('user.png') ?>" alt=""/></a>
                        </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo site_url('front/utilisateur/compteconsommateur_mobile')?>">
                               Mon compte
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/utilisateur/compteconsommateur_mobile')?>" class="bg-black-round"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
                <!--
                <tr>
                    <td v-align="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url('ic-localisation.png') ?>" alt=""/></a>
                        </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo site_url('front/mobile/localisation')?>">
                               Configurer la localisation
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="<?php echo site_url('front/mobile/localisation')?>" class="bg-black-round"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
                -->
                <tr>
                    <td v-align="middle" width="25%">
                        <div class="pad-10">
                            <a href="#"><img src="<?php echo img_url('process.png') ?>" alt=""/></a>
                        </div>
                    </td>
                    <td v-align="middle">
                        <div class="pad-10">
                            <a href="<?php echo site_url('auth/login_pro')?>">
                               Accès professionnel
                            </a>
                        </div>
                    </td>
                    <td>
                        <a href="#" class="bg-black-round"><span class="fa fa-chevron-right"></span></a>
                    </td>
                </tr>
            </table>
        </div>

    </div>
    <div class="clr"></div>
</div>
<script type="text/javascript">
<!--
	function alert_no_carte(){
		alert('Veuillez d\'abord verifier votre carte Privicarte');
	}
//-->
</script>
</body>
</html>
