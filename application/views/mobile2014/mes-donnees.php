<?php $this->load->view('mobile2014/partial/head')?>
<div class="wrapper ">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
    <?php $this->load->view('mobile2014/partial/titre_head_view')?>
    <!--forms-->
    <div class="wrap forms">
      <form class="pure-form pure-form-stacked" id="formModificationDonnee" action="<?php echo site_url("front/particuliers/ajouter"); ?>" method="POST">
          <div class="pure-u-5-5">
             <div class="pad-10 titre align-center error">
                   <?php echo (isset($message)) ? $message : '';?>
              </div>
               <div class="pad-10 titre align-center success">
                    <?php echo (isset($success_message)) ? $success_message : '';?>
              </div>
          </div>
          <div class="pad-tb-5">
              <input type="text" class="pure-input-1 clearable " placeholder="Nom" name="Particulier[Nom]" value="<?php echo isset ($oParticulier) ? $oParticulier->Nom : '';?>"/>
          </div>

          <div class="pad-tb-5">
              <input type="text" class="pure-input-1 clearable " placeholder="Prénom" name="Particulier[Prenom]" value="<?php echo isset($oParticulier) ? $oParticulier->Prenom : '';?>"/>
          </div>

          <div class="pad-tb-5">
              <input type="text" data-role="date" class="pure-input-1 clearable datepicker" placeholder="Date de naissance" name="Particulier[DateNaissance]" value="<?php echo (isset($oParticulier) && is_object($oParticulier)) ? convert_Sqldate_to_Frenchdate($oParticulier->DateNaissance) : ''?>"/>
          </div>

 		  <div class="pad-tb-5">
               <select class="" name="Particulier[Civilite]"  style="width:100%;">
                            <option value="0">Monsieur</option>
                            <option value="1">Madame</option>
                            <option value="2">Mademoiselle</option>
                </select>
          </div>
          
          <div class="pad-tb-5">
              <input type="text" class="pure-input-1 clearable" placeholder="Adresse" name="Particulier[Adresse]" value="<?php echo isset($oParticulier) ? $oParticulier->Adresse : '';?>"/>
          </div>
          
          <div class="pad-tb-5">
          	  <select name="Particulier[IdVille]" style ="width:100%;" onchange ="changeCP($(this).val());">
                <option value="">-- Veuillez choisir --</option>
                     <?php if(sizeof($colVilles)) { ?>
                           <?php foreach($colVilles as $objVille) { ?>
                              <option value="<?php echo $objVille->IdVille; ?>" <?php if( isset ($oParticulier) && $oParticulier->IdVille == $objVille->IdVille) { ?>selected="selected"<?php } ?>><?php echo htmlspecialchars(stripcslashes($objVille->Nom)); ?></option>
                            <?php } ?>
                      <?php } ?>
              </select>
           </div>
                        
          <div class="pad-tb-5">
              <input type="text" class="pure-input-1 " placeholder="CP" value="<?php echo isset($oParticulier) ? $oParticulier->CodePostal : '';?>" name="Particulier[CodePostal]"/>
          </div>

          <div class="pad-tb-5">
              <input type="text" class="pure-input-1 clearable" placeholder="Téléphone mobile" value="<?php echo isset ($oParticulier) ? $oParticulier->Telephone : '';?>" name="Particulier[Telephone]"/>
          </div>

          <div class="pad-tb-5">
              <input type="email" class="pure-input-1 clearable" placeholder="Email login" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Login; }?>" name="Particulier[Login]"/>
              <input type="hidden" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Login; }?>" name="origin_mail_login"/>
          </div>

          <div class="pad-tb-5">
              <input type="hidden" name="Particulier[IdUser]"  id="IdUser" value="<?php echo (isset($oParticulier) && is_object($oParticulier)) ? $oParticulier->IdUser : 0;?>" />
              <input type="submit" class="pure-input-1 pure-button pure-button-primary" value="Je modifie mes données"/>
          </div>

      </form>
    </div>
    
    <!--forms-->
    <div class="clr"></div>
</div>
<script>

$(function(){

	$( ".datepicker" ).datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
	    changeYear: true,
		yearRange: '1900:2020'
	});
	
	$( "#formModificationDonnee" ).validate({
		rules: {
			'Particulier[Nom]': {
				required: true
			},
			'Particulier[Prenom]': {
				required: true
			},
			'Particulier[DateNaissance]': {
				required: true
			},
			'Particulier[Adresse]': {
				required: true
			},
			'Particulier[CodePostal]': {
				required: true,
				minlength: 5,
				maxlength :5
			},
			'Particulier[Telephone]': {
				required: true
			},
			'Particulier[Login]': {
				required: true,
			  	email: true,
			  	remote: {
					 url: "<?php echo site_url('front/particuliers/checkmail')?>",
						 type: "post",
						 data: {
							 origin_mail_login: function() {
							 	return $("input[name$='origin_mail_login']").val();
							 }
						 }
					}	
				}
			},
			'Particulier[IdVille]': {
				required: true,
			},
		messages: {
			'Particulier[Nom]': {
				required: "ce champ est requis",
			},
			'Particulier[Prenom]': {
				required: "ce champ est requis",
			},
			'Particulier[DateNaissance]': {
				required: "ce champ est requis",
			},
			'Particulier[Adresse]': {
				required: "ce champ est requis",
			},
			'Particulier[CodePostal]': {
				required: "ce champ est requis",
				maxlength : "Le CP doit être de 5 chiffres"
			},
			'Particulier[Telephone]': {
				required: "ce champ est requis",
			},
			'Particulier[Login]': {
				required: "ce champ est requis",
				email: "Entrer un mail valide",
				remote: "Ce mail est déjà associé à un compte"
			},
			'Particulier[IdVille]': {
				required: "Veuillez choisir une ville",
			},
		}
	});
});

function changeCP(ville_id){
	if(ville_id != ''){
	 	jQuery.get('<?php echo site_url("front/particuliers/getPostalCodeadmin"); ?>' + '/' + ville_id,
	            function (zReponse)
	            {
	                $("input[name$='Particulier[CodePostal]']").val(zReponse) ;       
	 			});
	}else{
		 $("input[name$='Particulier[CodePostal]']").val('') ; 
	}
}
</script>
<?php $this->load->view('mobile2014/partial/footer')?>