<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
<style type="text/css">
    .btn_details_fd{
        height: 30px;
        background-color: rgb(6, 83, 163);
        color: white;
    }
    .btn_details_bp{
        height: 30px;
        background-color: rgb(204, 25, 145);
        color: white;
    }
    .btn_details_plt{
        height: 30px;
        background-color: rgb(196, 141, 86);
        color: white;
    }
</style>
<div class="container wrapper head">

        <div class="row text-center">

            <div class="pad-tb-15 col-12 titre text-center align-center">
                <?php echo $titre;?>
            </div>

        </div>

    <div class="row">
        <div class="col-4 text-center"><a href="<?php echo site_url('front/user_fidelity/details_fidelity')?>"><div class="btn_details_fd pt-2 pb-1 text-white">Détails fidelité</div></a></div>
        <div class="col-4 text-center"><a href="<?php echo site_url('front/user_fidelity/details_bonplan')?>"><div class="btn_details_bp pt-2 pb-1 text-white">Détails bonplans</div></a></div>
        <div class="col-4 text-center"><a href="<?php echo site_url('front/user_fidelity/details_plat')?>"><div class="btn_details_plt pt-2 pb-1 text-white">Détails Plat</div></a></div>
    </div>
</div>
<?php $this->load->view('mobile2014/partial/footer')?>