<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
	<div class="wrapper head">
	             <?php $this->load->view('mobile2014/partial/header_titre')?>
	</div>
<div class="wrapper page-white">
	<div class="consommation">
        <div class="product first">
            <div class="details">
                <div class="pure-g">
                    <div class="pure-u-1-2">
		                <?php //print_r($commercant);
		                //$base_path_system_rand = str_replace('system/', '', BASEPATH);
		                //$photoCommercant = 'application/resources/front/photoCommercant/images/'.$commercant->Photo1;?>
		                <a href="<?php echo site_url($commercant->nom_url."/notre_bonplan");?>" target="_blank"><img src="<?php echo photo_commercant($commercant) ?>" alt=""/></a>
                	</div>
                	<div class="pure-u-1-2">
                        <div class="txt-product">
                    		<b><?php echo ucfirst($commercant->NomSociete);?></b>
                   			 <span><?php echo $commercant->NomSimple?></span>
			                    <table border="0">
			                        <tr>
			                            <td><a href="<?php echo site_url($commercant->nom_url."/index");?>" target="_blank"><img src="<?php echo img_url('badge-consommation.png') ?>" alt="" width="50"/></a></td>
			                            <td><a href="mailto:<?php echo $commercant->Email;?>"><img src="<?php echo img_url('email.png') ?>" alt="" width="50"/></a></td>
			                            <td><a href="tel:<?php echo $commercant->TelFixe;?>"><img src="<?php echo img_url('mobile_phone.png') ?>" alt="" width="50"/></a></td>
			                        </tr>
			                    </table>
                    	</div>
                	</div>
            	</div>
            </div>
            <?php if($offre->is_tampon_active):?>
             <span>Coup de tampons : <?php echo $offre->tampon->description?></span>
	            <div class="pure-g bg-red">
	                    <div class="<?php echo blue_class_progress($fiche_tampon->solde_tampon, $offre->tampon->tampon_value)?>">
	                    	<div class="pad-lr-5"><?php echo (isset($fiche_tampon->solde_tampon) && $fiche_tampon->solde_tampon!="") ? $fiche_tampon->solde_tampon :0?></div>
	                    </div>
	                    <div class="<?php echo red_class_progess($fiche_tampon->solde_tampon, $offre->tampon->tampon_value)?>">
	                    	<div class="pad-lr-5"><?php echo $offre->tampon->tampon_value;?></div>
	                    </div>
	            </div>
           <?php endif;?>
           <?php if($offre->is_capital_active):?>
	            <span>Capitalisation : <?php echo (isset($offre) && isset($offre->capital)) ? $offre->capital->description : ''?></span>
	            <div class="pure-g bg-red">
	                <div class="<?php echo blue_class_progress($fiche_capital->solde_capital,$offre->capital->montant)?>">
	                    <div class="pad-lr-5"><?php echo (!empty($fiche_capital->solde_capital)) ? $fiche_capital->solde_capital : 0?>&euro;</div>
	                </div>
	                <div class="<?php echo red_class_progess($fiche_capital->solde_capital, $offre->capital->montant)?> "><div class="pad-lr-5"><?php echo (!empty($offre->capital->montant)) ? $offre->capital->montant : 0?> &euro;</div></div>
	            </div>
	         <?php endif;?>
	        <?php if($offre->is_remise_active):?>
	          		<span>Remise directe : <?php echo (!empty($offre->remise->description)) ? $offre->remise->description: ''?></span>
		            <div class="pure-g bg-blue pad-tb-5">
			                <div class="pure-u-2-3 bg-blue align-left">
			                    <div class="pad-lr-5">Cumul consommation</div>
			                </div>
			                <div class="pure-u-1-3 bg-blue align-right">
			                	<div class="pad-lr-5">
			                		<b>
			                				<?php echo (!empty($fiche_remise->solde_remise)) ? $fiche_remise->solde_remise : 0;?> &euro;
			                		</b>
			                	</div>
			                </div>
			            </div>
	          <?php endif;?> 
           </div>
          <?php if(!empty($list_activity)):?>
        	<?php foreach ($list_activity as $activity) :?>
		         <?php if($offre->is_capital_active && !empty($activity->capital_value_added)):?>
			        <div class="product product-row">
			            <?php echo date('d/m/Y',strtotime($activity->date_use));?> : capitalisation <?php echo $activity->capital_value_added?>&euro;
			        </div>
			     <?php endif;?>
			     <?php if($offre->is_tampon_active && !empty($activity->tampon_value_added)):?>
			        <div class="product product-row">
			            <?php echo date('d/m/Y',strtotime($activity->date_use));?> : coup de tampons <?php echo $activity->tampon_value_added?>
			        </div>
			     <?php endif;?> 
			     <?php if($offre->is_remise_active && !empty($activity->remise_value_added)):?>
			        <div class="product product-row">
			            <?php echo date('d/m/Y',strtotime($activity->date_use));?> : Remise <?php echo $activity->remise_value_added?> &euro;
			        </div>
			     <?php endif;?>      
			    <?php endforeach;?>
	    <?php endif;?>  
       </div>   
       
       <?php if(!empty($user_bonplan)): foreach($user_bonplan as $bonplan):?>    
	     <div class="product product-row">
			 <?php echo date('d/m/Y',strtotime($bonplan->date_validation));?> : <?php echo ($bonplan->valide) ? "Bon plan validé" : "Bon plan non validé" ; ?>
		 </div>
		 <?php endforeach;endif;?>   
   </div>         
