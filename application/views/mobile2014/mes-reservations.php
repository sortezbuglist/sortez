<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
<style type="text/css">
<!--
.blacktext td {
color:#000000;
}
-->
</style>

	<div class="wrapper head">
	             <?php $this->load->view('mobile2014/partial/header_titre')?>
	</div>
<div class="wrapper page-white">
		<div class="">
		<?php if(!empty($reservations)) :?>
			<?php foreach($reservations as $reservation):?>
			 	<div class="product">
			           <?php  echo  ucfirst($reservation->NomSociete) ?>
			            <span><?php echo $reservation->NomSimple?></span>
			            <span>Conditions: <?php echo $reservation->bonplan_titre;?></span>
			            <!--  <a href="<?php echo site_url($reservation->nom_url."/notre_bonplan");?>" class="btn"><img src="<?php echo img_url('puce-link.png'); ?>" alt="" style="width:25px; margin-top: 10px; margin-right: 10px;"/></a>-->
			            <div class="pure-g bg-red">
			                    <div class="pure-u-1 bg-red pad-tb-5 align-left">
			                        <div class="pad-lr-5"><strong>En attente de validation</strong></div>
			                    </div>
			            </div>
			            <div class="mrg5"></div>
			            <div class="pure-g bg-red">
			                <div class="pure-u-2-3 bg-red pad-tb-5 align-left">
			                        <div class="pad-lr-5"><strong>Offre à utiliser avant le : </strong></div>
			                </div>
			                <div class="pure-u-1-3 bg-red pad-tb-5 align-right">
			                        <div class="pad-lr-5">
										<?php if ($reservation->bonplan_type == '2') { echo convert_Sqldate_to_Frenchdate($reservation->bp_unique_date_fin); } ?>
                                        <?php if ($reservation->bonplan_type == '3') { echo convert_Sqldate_to_Frenchdate($reservation->bp_multiple_date_fin); } ?>
                                    </div>
			                </div>
			            </div>
                        <table border="0" class="blacktext" style="margin-top:10px; width:100%">
			                <tbody>
			                 <tr>
                                <td style="text-align:left;">
                                
                                    <table border="0">
                                        <tbody>
                                         <tr><td colspan="3">Contacter le professionnel :</td></tr>
                                         <tr>
                                            <td><a href="<?php echo site_url($reservation->nom_url."/notre_bonplan");?>"><img src="<?php echo img_url('badge-consommation.png') ?>" alt="" width="50"/></a></td>
                                            <td><a href="mailto:<?php echo $reservation->Email;?>"><img src="<?php echo img_url('email.png') ?>" alt="" width="50"/></a></td>
                                            <?php if(isset($reservation->TelMobile) && $reservation->TelMobile!="" && is_numeric($reservation->TelMobile)) { ?>
                                             <td><a href="tel:<?php echo $reservation->TelMobile;?>"><img src="<?php echo img_url('mobile_phone.png') ?>" alt="" width="50"/></a></td>
                                            <?php } ?>
                                        </tr>
                                        </tbody>
                                    </table>
                                
                                </td>
                                <td style="text-align:right;">
                                
                                	<table border="0" style="float: right;">
                                        <tbody>
                                         <tr><td>Nombre de place r&eacute;servée : <?php if (isset($reservation->nb_place) && $reservation->nb_place != '0') echo $reservation->nb_place; else echo "Non renseigné";?></td></tr>
                                         <tr><td>Date de visite pr&eacute;vue : <?php if (isset($reservation->date_visit) && $reservation->date_visit != '0000-00-00') echo convert_Sqldate_to_Frenchdate($reservation->date_visit);  else echo "Non renseignée"; ?></td></tr>
                                         <tr><td>
                                         	<?php if ($reservation->bonplan_type == '2') { ?>Date d'expiration du Bonplan : <?php echo convert_Sqldate_to_Frenchdate($reservation->bp_unique_date_fin); ?><?php } ?>
                                            <?php if ($reservation->bonplan_type == '3') { ?>Date d'expiration du Bonplan : <?php echo convert_Sqldate_to_Frenchdate($reservation->bp_multiple_date_fin); ?><?php } ?>
                                         </td></tr>
                                        </tbody>
                                    </table>
                                
                                </td>
                            </tr>
			                </tbody>
			            </table>
			            
                        
			       </div>
			   <?php endforeach;?>    
		   <?php else:?>
		   			<div class="product">Vous n'avez aucune reservation</div>    
	       <?php endif;?>
       </div>
    <?php if(!empty($list_plat)){?>
        <?php foreach ($list_plat as $clients_plat){ ?>

            <div class="product first id<?php echo $clients_plat->id ?>" >

                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td>
                            <div class="details f-size14 pad-10 no-marg">
                                <?php echo $clients_plat->Prenom.' '.$clients_plat->Nom;?>
                            </div>
                        </td>
                        <td width="10%" v-align="top"><a href="<?php echo site_url('front/fidelity_pro/detail_client/'.$clients_plat->IdUser)?>" class="bg-grey-round decale no-marg"><span class="fa fa-check pad-tb-5"></span></a></td>
                    </tr>
                </table>

                <div class="pure-g bg-red marg-bt-10">
                    <div class="pure-u-1-2 bg-red  align-left ">
                        <div class="pad-lr-5"><?php echo  date("d/m/Y",strtotime($clients_plat->date_reservation));?></div>
                    </div>
                </div>
            </div>

            <div class="padded">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="width:100%; display:table; float:right;text-align:left;">
                    <tr >
                        <td style="padding-top: 15px;padding-bottom: 15px;padding-left: 10px">
                            <h4>Plat du jour</h4>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px; color:#000000 !important;">
                            <?php
                            if (isset($clients_plat->Nom) && $clients_plat->Nom != "") echo "Partenaire : ".$clients_plat->NomSociete;
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px; color:#000000 !important;">Nom du Plat : <?php echo $clients_plat->description_plat; ?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px; color:#000000 !important;">
                            <?php
                            $date_visite_client_check = $clients_plat->date_reservation;
                            if ($date_visite_client_check != "0000-00-00" AND $date_visite_client_check !='' AND $date_visite_client_check !=null ) echo "Date de visite : ".convert_Sqldate_to_Frenchdate($date_visite_client_check);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px; color:#000000 !important;">
                            Nombre de plat r&eacute;serv&eacute;e :
                            <?php
                            if (isset($clients_plat->nbre_platDuJour)) echo $clients_plat->nbre_platDuJour;  else echo "1";
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px; color:#000000 !important;">
                            <?php
                            if (isset($clients_plat->date_fin_plat) && $clients_plat->date_fin_plat != "0000-00-00") echo "Date d'expiration du plat : ".convert_Sqldate_to_Frenchdate($clients_plat->date_fin_plat);
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:10px; color:#000000 !important;">
                            <?php
                            if (isset($clients_plat->heure_reservation) && $clients_plat->heure_reservation != "") echo "Heure de réservation : ".$clients_plat->heure_reservation;
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
        <?php  } ?>
    <?php  }else{ ?>
        <div style="padding-top: 25px" class="product first pt">
            Aucun reservation de plat trouvé
        </div>
    <?php  } ?>
</div>
<div class="clr"></div>
<?php $this->load->view('mobile2014/partial/footer')?>