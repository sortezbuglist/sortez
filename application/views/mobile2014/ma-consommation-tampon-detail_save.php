<?php $this->load->view('mobile2014/partial/head')?>
<div class="wrapper head">
    <?php $this->load->view('mobile2014/partial/logo_head_view')?>
     <?php $this->load->view('mobile2014/partial/header_titre_menu_carte')?>
</div>
<div class="wrapper page-white">
    <div class="consommation">
        <div class="product first">
            <div class="details">
                <div class="pure-g">
                    <div class="pure-u-1-3">
		                <?php 
		                $base_path_system_rand = str_replace('system/', '', BASEPATH);
		                $photoCommercant = 'application/resources/front/images/'.$oFidelityDetails->bonplan_photo;?>
		                    <a href="#"><img src="<?php echo is_file($base_path_system_rand.$photoCommercant) ? site_url($photoCommercant) : site_url('application/resources/front/images/wp71b211d2_06.png') ?>" alt=""/></a>
                	</div>
                	<div class="pure-u-2-3">
                        <div class="txt-product">
                    		<?php echo $oFidelityDetails->NomSociete;?>
                   			 <span><?php echo $oFidelityDetails->ville_commercant?></span>
			                    <table border="0">
			                        <tr>
			                            <td><a href="mailto:<?php echo $oFidelityDetails->Email;?>"><img src="<?php echo img_url('badge-consommation.png') ?>" alt="" width="60"/></a></td>
			                            <td><a href="tel:<?php echo $oFidelityDetails->TelFixe;?>"><img src="<?php echo img_url('phone-consommation.png') ?>" alt="" width="50"/></a></td>
			                        </tr>
			                    </table>
                    	</div>
                	</div>
            	</div>
            </div>
            <?php if (isset($oFidelityDetails->tampon_value_added) && $oFidelityDetails->tampon_value_added!=""):?>
	            <span>Coup de tampons : <?php echo $oFidelityDetails->bonplan_titre?></span>
	            <div class="pure-g bg-red">
	                    <div class="pure-u-<?php echo round((24*$oFidelityDetails->cumul_tampon )/$oFidelityDetails->objectif_tampon, 0, PHP_ROUND_HALF_UP);   ?>-24 bg-red pad-tb-24 align-right price"><div class="pad-lr-5"><?php echo (isset($oFidelityDetails->cumul_tampon) && $oFidelityDetails->cumul_tampon!="") ? $oFidelityDetails->cumul_tampon :0?></div></div>
	                    <div class="pure-u-<?php echo round(24 - round((24*$oFidelityDetails->cumul_tampon )/$oFidelityDetails->objectif_tampon, 0, PHP_ROUND_HALF_UP))?>-24 bg-blue pad-tb-24 align-right price "><div class="pad-lr-5"><?php echo (isset($oFidelityDetails->objectif_tampon) && $oFidelityDetails->objectif_tampon!="") ? $oFidelityDetails->objectif_tampon : 0; ?></div></div>
	            </div>
	         </div>
	         <?php if(isset($list_activity) && !empty($list_activity)) :
	         		foreach($list_activity as $activity) :
	         			if(!empty($activity->tampon_value_added)):
	         ?>
	         <div class="product">
	            <?php echo date("d/m/Y", strtotime($activity->date_use));?> : coup de tampons <?php echo $oFidelityDetails->tampon_value_added;?>
	         </div>
	        <?php endif;endforeach;endif;?>
	        
            <?php elseif (isset($oFidelityDetails->capital_value_added) && $oFidelityDetails->capital_value_added!=""):?>
            	<?php if($oFidelityDetails->objectif_capital > $oFidelityDetails->cumul_capital):?>
		            <span>Capitalisation : <?php echo $oFidelityDetails->bonplan_titre?></span>
		             <div class="pure-g bg-red">
		            <?php 
		            	$ui_begin = round((24*$oFidelityDetails->cumul_capital )/$oFidelityDetails->objectif_capital, 0, PHP_ROUND_HALF_UP);   
						$ui_begin = ($ui_begin < 4) ? 4 : $ui_begin;
						$ui_begin = ($ui_begin > 20) ? 20 : $ui_begin;
						
						$ui_end =   round(24 - round($ui_begin, 0, PHP_ROUND_HALF_UP));
		            ?>
		                    <div class="pure-u-<?php echo $ui_begin?>-24 bg-red pad-tb-5 align-right price">
		                        <div class="pad-lr-5"><?php echo (isset($oFidelityDetails->cumul_capital) && $oFidelityDetails->cumul_capital!="") ? $oFidelityDetails->cumul_capital : 0?> &euro;</div>
		                     </div>
		                    <div class="pure-u-<?php echo $ui_end?>-24 bg-blue pad-tb-5 align-right price ">
		                        <div class="pad-lr-5"><?php echo (isset($oFidelityDetails->objectif_capital) && $oFidelityDetails->objectif_capital!="") ? $oFidelityDetails->objectif_capital : 0;?>&euro;</div>
		                    </div>
		            </div>
			  </div>
			  <div class="product">
            	<?php echo date("d/m/Y", strtotime($oFidelityDetails->date_use));?> : capitalisation <?php echo $oFidelityDetails->cumul_capital;?> &euro;
        	   </div>
        		 <?php else:?>
		         	<span>Bon plan :<?php echo $oFidelityDetails->bonplan_titre;?> </span>
		            <a href="<?php echo site_url("front/user_fidelity/fidelity_event_details_mobile/".$oFidelityDetails->id)?>" class="btn"><img src="<?php echo img_url('puce-link.png') ?>" style="width:25px; margin-top: 10px; margin-right: 10px;"/></a>
		            <div class="pure-g bg-blue">
		                    <div class="pure-u-1-2 bg-blue pad-tb-5 align-left">
		                        <div class="pad-lr-5">Cumul consommation</div>
		                    </div>
		                    <div class="pure-u-1-2 bg-blue pad-tb-5 align-right price "><div class="pad-lr-5"><?php echo  $oFidelityDetails->objectif_capital;?> &euro;</div></div>
		            </div>
		            <br />
		            <div class="pure-g  bg-red">
		                <div class="pure-u-1-2 bg-red pad-tb-5 align-left">
		                       <div class="pad-lr-5"> <?php echo date("d/m/Y", strtotime($oFidelityDetails->date_use));?></div>
		                </div>
		                <div class="pure-u-1-2 bg-red pad-tb-5 align-right price ">
		                    <div class="pad-lr-5">Bon plan validé</div>
		                </div>
		             </div>  
		          </div>
			  <div class="product">
            	<?php echo date("d/m/Y", strtotime($oFidelityDetails->date_use));?> : Bon plan validé
        	   </div>     
		         <?php endif;?>   
			<?php endif?>
    		</div>
        </div>
    <div class="clr"></div>
   <?php $this->load->view('mobile2014/partial/footer')?>