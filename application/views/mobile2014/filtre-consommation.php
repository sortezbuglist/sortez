<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
<div class="wrapper head">
    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
   <?php //$this->load->view('mobile2014/partial/titre_head_view')?>
        <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
             <?php $this->load->view('mobile2014/partial/header_titre')?>
        
</div>
<div class="wrapper page-white">
    <div class="consommation-filtre">
        <div class="pure-g bordered">
            <div class="pure-u-1-5 align-center">
             	<div class="pad-5"><img src="<?php echo img_url('ico-filtre-dete-recent.png') ?>" alt=""/></div>
            </div>
            <div class="pure-u-3-5 align-left">
                <div class="pad-10">
                    De la date la plus récente à la plus ancienne
                </div>
            </div>
            <div class="pure-u-1-5">
                <div class="pad-10 align-right">
                    <a href="<?php echo site_url('front/user_fidelity/fidelity_event_list_mobile/date_desc')?>"><img src="<?php echo img_url('list-puce-black.png') ?>" alt=""/></a>
                </div>
            </div>
        </div>
        <div class="pure-g bordered">
            <div class="pure-u-1-5 align-center">
                  <div class="pad-5"><img src="<?php echo img_url('ico-filtre-dete-ancienn.png') ?>" alt=""/></div>
            </div>
            <div class="pure-u-3-5 align-left">
                <div class="pad-10">
                    De la date la plus ancienne  à la plus récente
                </div>
            </div>
            <div class="pure-u-1-5">
                <div class="pad-10 align-right">
                    <a href="<?php echo site_url('front/user_fidelity/fidelity_event_list_mobile/date_asc')?>"><img src="<?php echo img_url('list-puce-black.png') ?>" alt=""/></a>
                </div>
            </div>
        </div>
        <div class="pure-g bordered">
            <div class="pure-u-1-5 align-center">
                <div class="pad-5"><img src="<?php echo img_url('ico-filtre-.png') ?>" alt=""/></div>
            </div>
            <div class="pure-u-3-5 align-left">
                <div class="pad-10">
                   Visualiser uniquement les bons plans <br /> Ordre croissant
                </div>
            </div>
            <div class="pure-u-1-5">
                <div class="pad-10 align-right">
                    <a href="<?php echo site_url('front/bonplan')?>"><img src="<?php echo img_url('list-puce-black.png') ?>" alt=""/></a>
                </div>
            </div>
        </div>
         <div class="pure-g bordered">
            <div class="pure-u-1-5 align-center">
                <div class="pad-5"><img src="<?php echo img_url('ico-filtre-bonplan.png') ?>" alt=""/></div>
            </div>
            <div class="pure-u-3-5 align-left">
                <div class="pad-10">
                     Visualiser uniquement les actions remise directe <br /> Ordre croissant
                </div>
            </div>
            <div class="pure-u-1-5">
                <div class="pad-10 align-right">
                    <a href="<?php echo site_url('front/user_fidelity/fidelity_event_list_mobile/remise')?>"><img src="<?php echo img_url('list-puce-black.png') ?>" alt=""/></a>
                </div>
            </div>
        </div>
        <div class="pure-g bordered">
            <div class="pure-u-1-5 align-center">
                <div class="pad-5"><img src="<?php echo img_url('ico-filtre-capitalisatoin.png') ?>" alt=""/></div>
            </div>
            <div class="pure-u-3-5 align-left">
                <div class="pad-10">
 					Visualiser uniquement les actions de capitalisation <br /> Ordre croissant                </div>
            </div>
            <div class="pure-u-1-5">
                <div class="pad-10 align-right">
                    <a href="<?php echo site_url('front/user_fidelity/fidelity_event_list_mobile/capital')?>"><img src="<?php echo img_url('list-puce-black.png') ?>" alt=""/></a>
                </div>
            </div>
        </div>
        <div class="pure-g bordered">
            <div class="pure-u-1-5 align-center">
                <div class="pad-5"><img src="<?php echo img_url('ico-filtre-tampons.png') ?>" alt=""/></div>
            </div>
            <div class="pure-u-3-5 align-left">
                <div class="pad-10">
                    Visualiser uniquement les actions de coups de tampons<br /> Ordre croissant
                </div>
            </div>
            <div class="pure-u-1-5">
                <div class="pad-10 align-right">
                    <a href="<?php echo site_url('front/user_fidelity/fidelity_event_list_mobile/tampon')?>"><img src="<?php echo img_url('list-puce-black.png') ?>" alt=""/></a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="clr"></div>
   <?php $this->load->view('mobile2014/partial/footer')?>