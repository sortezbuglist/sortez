<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
    <style type="text/css">
        .btn_details_fd{
            height: 30px;
            background-color: rgb(6, 83, 163);
            color: white;
        }
        .label{
            color: rgb(6, 83, 163);
            font-size: 16px;
        }
        .value{
            color: rgb(192, 24, 136);
            font-size: 16px;
        }
    </style>
    <div class="wrapper head">
        <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
        <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
        <div class="pure-g">
            <div class="pure-u-3-5">
                <div class="pad-tb-15 titre align-center">
                    <?php echo $titre;?>
                </div>
            </div>
            <!-- <div class="pure-u-2-5">
            <div class="btn-menu">
                <a href="<?php echo site_url('front/user_fidelity/filter')?>">Filtrer</a>
            </div>
        </div>
        -->
        </div>
    </div>
    <div class="wrapper page-white">
        <!--TITRE-->

        <div class="consommation">
            <?php if(!empty($list_plat)){?>
                <?php foreach ( $list_plat as $ofiche_plat) { ?>
                    <div class="row">
                        <div class="col-10">
                            <div class="product row">
                                <div class="col-4 label">Nom du plat</div>
                                <div class="col-8 value"><?php if (isset($ofiche_plat->description_plat))echo $ofiche_plat->description_plat; ?></div>

                                <div class="col-4 label"> Partenaire</div>
                                <div class="col-8 value"><?php if (isset($ofiche_plat->NomSociete))echo $ofiche_plat->NomSociete; ?></div>

                                <div class="col-4 label">Etat</div>
                                <div class="col-8 value">Validé</div>

                                <div class="col-4 label">Validé le</div>
                                <div class="col-8 value"><?php if (isset($ofiche_plat->date_validation) AND $ofiche_plat->date_validation !="0000-00-00" AND $ofiche_plat->date_validation !="" AND $ofiche_plat->date_validation !=null){echo convert_Sqldate_to_Frenchdate($ofiche_plat->date_validation);} ?></div>
                            </div>
                        </div>
                        <div class="col-2 pl-4 pr-4 pt-0"><a href="<?php echo site_url('front/user_fidelity/delete_historical_plat/'.$ofiche_plat->idRes)?>"><img class="img-fluid" src="<?php echo base_url('assets/img/delete.jpg')?>"></a></div>
                    </div>
                <?php } ?>

            <?php }else{?>
                <div class="product">
                    Vous n'avez aucune donnée
                </div>
            <?php } ?>
        </div>
        <div class="clr"></div>
    </div>
<?php $this->load->view('mobile2014/partial/footer')?>