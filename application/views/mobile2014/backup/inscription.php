<?php $this->load->view('mobile2014/partial/head')?>
<?php
$actual_link_soutenons = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
//var_dump(parse_url($actual_link_soutenons));
$all_data_from_url = parse_url($actual_link_soutenons);
if(isset($all_data_from_url["query"]) && $all_data_from_url["query"]=="form_soutenons=1"){}else{
    $this->load->view('mobile2014/partial/menu');
}
?>
    <style>
        .forms label.error {
            color: #D54F4F;
            font-size: 11px;
            font-style: italic;
            position: absolute !important;
            right: 30px !important;
            margin-top: -30px !important;
        }
        label#iCheck-error.error {
            position: absolute !important;
            right: inherit !important;
            margin-top: 40px !important;
         }
        .submit_button_click, .submit_button_click:hover {
            background-image: url('<?= base_url()?>application/resources/sortez/images/loading.gif') !important;
            background-repeat: no-repeat !important;
            background-size: 30px !important;
            background-position: 30px !important;
        }
    </style>
<div class="wrapper head">
    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
     <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
     <?php $this->load->view('mobile2014/partial/header_titre')?>
</div>
<div class="wrapper page-black inscription">
    <div class="padded">
    <!--forms-->
    <div class="wrap forms">
    <?php if((isset($message) && !empty($message))   || isset($success_message)&& !empty($success_message)) :?>
    	<div class="pure-u-1">
             <div class="pad-5 titre align-center error">
                   <?php echo (isset($message)) ? $message : '';?>
              </div>
               <div class="pad-5 titre align-center success">
                    <?php echo (isset($success_message)) ? $success_message : '';?>
              </div>
          </div>
       <?php endif;?>   
      <form class="pure-form pure-form-stacked" id="formModificationDonnee" onsubmit="add_class_click_soutenons()" action="<?php echo site_url("front/fidelity/ajouter");if(isset($all_data_from_url["query"]) && $all_data_from_url["query"]=="form_soutenons=1") echo '?form_soutenons=1';?>" method="POST">
          <div>
          <!--  
          	<?php if(!isset($oParticulier)):?>
              <ul class="pad-tp-5 pure-g btnul">
                  <li class="pure-u-1-2  ui-tabs-active ui-state-active" id="li_page1"><a href="#form-1" class="pure-button btn-onglet" id="page1">Page 1</a></li>
                  <li class="pure-u-1-2" id="li_page2"><a href="#form-2" class="pure-button btn-onglet" id="page2">Page 2</a></li>
              </ul>
            <?php endif;?>
            -->  
              <div id="form-1">
              	 <!--  
                 <div class="pad-tp-5">
		               <select class="" name="Particulier[Civilite]"  style="width:100%;">
		                            <option value="0">Monsieur</option>
		                            <option value="1">Madame</option>
		                            <option value="2">Mademoiselle</option>
		                </select>
         		 </div>
         		 -->
		          <div class="pad-tb-5">
		              <input type="text" class="pure-input-1 clearable " placeholder="Nom" name="Particulier[Nom]" value="<?php echo isset ($oParticulier) ? $oParticulier->Nom : '';?>"/>
		          </div>
		
		          <div class="pad-tp-5">
		              <input type="text" class="pure-input-1 clearable " placeholder="Prénom" name="Particulier[Prenom]" value="<?php echo isset($oParticulier) ? $oParticulier->Prenom : '';?>"/>
		          </div>
		
		          <div class="pad-tp-5">
		              <input type="text" data-role="date" class="pure-input-1 clearable datepicker" placeholder="Date de naissance" name="Particulier[DateNaissance]" value="<?php echo (isset($oParticulier) && is_object($oParticulier)) ? convert_Sqldate_to_Frenchdate($oParticulier->DateNaissance) : ''?>"/>
		          </div>
		          <div class="pad-tp-5">
		              <input type="text" class="pure-input-1 clearable" placeholder="Adresse" name="Particulier[Adresse]" value="<?php echo isset($oParticulier) ? $oParticulier->Adresse : '';?>"/>
		          </div>
 				  <div class="pad-tp-5">
		              <input type="text" class="pure-input-1 " placeholder="Codel postal" value="<?php echo isset($oParticulier) ? $oParticulier->CodePostal : '';?>" name="Particulier[CodePostal]" onchange ="getListCityByCP($(this).val());"/>
		          </div>
		          <!-- <div class="pad-tp-5"> -->
		          	  <!-- <select name="" style ="width:100%;" id="departement_id"> -->
		          	  	<?php //if(isset($oParticulier) AND isset($oParticulier->departement_code) AND isset($oParticulier->departement_nom)):?>
		          	  			<!-- <option value="<?php //echo $oParticulier->departement_code?>"><?php //echo $oParticulier->departement_nom ?></option> -->
		          	  	<?php //endif;?>
		              <!-- </select> -->
		           <!-- </div> -->
		           
		          <div class="pad-tp-5">
		          <!-- onchange ="changeCP($(this).val());" -->
		          	  <select name="Particulier[IdVille]" style ="width:100%;" id="ville">
		          	  <?php if(isset($oParticulier) && !empty($user_ville)): foreach ($user_ville as $ville):?>
		          	  	<option value="<?php echo $ville->IdVille?>" <?php echo ($ville->IdVille == $oParticulier->IdVille) ? 'selected' : ''?>><?php echo $ville->NomSimple?></option>
		          	  <?php endforeach;endif;?>
		              </select>
		           </div>
		          <!--  
		          <div class="pad-tp-5">
		              <input type="text" class="pure-input-1 clearable" placeholder="Téléphone fixe" value="<?php echo isset ($oParticulier) ? $oParticulier->Telephone : '';?>" name="Particulier[Telephone]"/>
		          </div>
		          -->
		          
		          <div class="pad-tp-5">
		              <input type="text" class="pure-input-1 clearable" placeholder="Mobile" value="<?php echo isset ($oParticulier) ? $oParticulier->Portable : '';?>" name="Particulier[Portable]"/>
		          </div>
		
              </div>
              <div id="form-2">
                   <div class="pad-tp-5">
		              <input type="email" class="pure-input-1 clearable" placeholder="Mail" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Login; }?>" name="Particulier[Login]" id="email_login"/>
		              <input type="hidden" value="<?php if( isset ($oParticulier)) { echo $oParticulier->Login; }?>" name="origin_mail_login"/>
		          </div>


				<?php if(!isset($oParticulier)):?>

					<div class="pad-tp-5">
						<input type="password" class="pure-input-1 clearable " placeholder="Mot de passe" id="password" name="Particulier_Password"/>
					</div>

					<div class="pad-tp-5">
						<input type="password" class="pure-input-1 clearable" placeholder="Confirmation mot de passe" name="passwordConfirm"/>
					</div>

				  <!--  
                  <div class="pad-tp-5">
                      <input type="text" class="pure-input-1 clearable " placeholder="Confirmation courriel identifiant" name="EmailConfirm"/>
                  </div>-->



                  <div class="pad-tb-5">
                  	<div class="radio">
  						 <label for="cgu"> <input type="radio" name="iCheck" class="icheck" id="cgu" name="cgu" required data-msg-required="Veuillez lire et accepter les conditions générales d'utilisation"> Je soussigné, confirme être majeur, et accepte sans réserves les conditions générales d'utilisation de la carte Privicarte</label>
					</div>
                  </div>
					<br/>
                  

                  <div class="pad-tp-5">
                      <input type="submit" id="submit_click_soutenons" class="pure-input-1 pure-button pure-button-primary" value="Validation de l'inscription"/>
                  </div>
                    <?php
                    if(isset($all_data_from_url["query"]) && $all_data_from_url["query"]=="form_soutenons=1"){}else{
                        ?>
                        <div class="pad-tb-5 links">
                            <a class="pure-button pure-button-primary" href="<?php echo '/mobile-mentions-legales.html'?>">Conditions générales d'utlisation</a>
                        </div>
                        <?php
                    }
                    ?>
                  <?php else:?>
                   <div class="pad-tb-5">
		              <input type="submit" id="submit_click_soutenons" class="pure-input-1 pure-button pure-button-primary" value="Je modifie mes données"/>
		          </div>
                  <?php endif;?>
              </div>
          </div>
              <input type="hidden" name="Particulier[IdUser]"  id="IdUser" value="<?php echo (isset($oParticulier) && is_object($oParticulier)) ? $oParticulier->IdUser : 0;?>" />
      </form>
    </div>
    <!--forms-->
    <div class="clr"></div>
    </div>
</div>
<script>
function add_class_click_soutenons(){
    $('#submit_click_soutenons').addClass('submit_button_click');
}

$(function(){
		$("#ville,#departement_id").select2({
		  placeholder: "Veuillez remplir le code postal ",
		  allowClear: true
		});

		$('.icheck').iCheck({
		    checkboxClass: 'icheckbox_flat-blue',
		    radioClass: 'iradio_flat-blue'
		  });
		  
	
	   $('#page2').on('click',function(e){
	        e.preventDefault();
	        var form = $( "#formModificationDonnee" );
	        form.validate();
			if(form.valid()){
				$('#form-1').css('visibility','hidden').css('position','fixed');
				$('#form-2').removeAttr('style');
				$('#li_page2').addClass('ui-tabs-active ui-state-active');
				$('#li_page1').removeClass('ui-tabs-active ui-state-active');
			}
	        return false;
	    });
 
	   $('#page1').on('click',function(e){
	        e.preventDefault();
			$('#form-2').css('display','none');
			$('#form-1').removeAttr('style');
	        return false;
	    });


		
	$( ".datepicker" ).datepicker({
		dayNamesMin: ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		monthNames: ['Janvier','Févier','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
		dateFormat: 'DD, d MM yy',
		autoSize: true,
		changeMonth: true,
	    changeYear: true,
		yearRange: '1900:2020',
		onSelect: function(dateText) {
			$( this ).valid();
		}
	});
	
	$( "#formModificationDonnee" ).validate({
		rules: {
			cgu : {
				required: true,
			},
			EmailConfirm : {
				required: true,
				equalTo : "#email_login"
			},
			'Particulier[Nom]': {
				required: true
			},
			'Particulier[Prenom]': {
				required: true
			},
			'Particulier[DateNaissance]': {
				required: true
			},
		/*	'Particulier[Adresse]': {
				required: true
			},*/
			'Particulier[CodePostal]': {
				required: true,
				minlength: 5,
				maxlength :5
			},
			'Particulier[Telephone]': {
				required: true
			},
            <?php if(!isset($oParticulier)){ ?>
			Particulier_Password : {
				required: true,
                minlength : 5
            },
            passwordConfirm : {
            	required: true,
                minlength : 5,
                equalTo : "#password"
            },
            <?php } ?>
			'Particulier[Login]': {
				required: true,
			  	email: true,
			  	remote: {
					 url: "<?php echo site_url('front/fidelity/checkmail')?>",
						 type: "post",
						 data: {
							 origin_mail_login: function() {
							 	return $("input[name$='origin_mail_login']").val();
							 }
						 }
					}	
				}
			},
			'Particulier[IdVille]': {
				required: true,
			},
		messages: {
			'Particulier[Login]': {
				remote: "Ce mail est déjà associé à un compte"
			},
			cgu : {
				required: "Veuillez lire et accepter les conditions générales d'utilisation",
			},
		}
	});
});

function changeCP(ville_id){
	if(ville_id != ''){

		$.ajax({
			  type: "POST",
			  url: "<?php echo site_url("front/fidelity/json_getPostalCodeadmin"); ?>",
			  data: 'ville_id='+ville_id,
			  success: function(data){
				  $("input[name$='Particulier[CodePostal]']").val(data.cp) ;  
			  },
			  dataType: 'json'
			});

	}else{
		 $("input[name$='Particulier[CodePostal]']").val('') ; 
	}
}

function changeVille(cp){
	
	if(cp != ''){
		$.ajax({
			  type: "POST",
			  url: "<?php echo site_url("front/fidelity/json_getVille"); ?>",
			  data: 'cp='+cp,
			  success: function(data){
				  $('#ville').val(data.ville_id);
			  },
			  dataType: 'json'
			});

	}else{
		 $("input[name$='Particulier[CodePostal]']").val('') ; 
	}
}

function getListCityByCP(cp){
	if(cp != '' || typeof cp != 'undefined'){
		$.ajax({
			  type: "POST",
			  url: "<?php echo site_url("front/fidelity/getListCityByCP/"); ?>/"+cp,
			  data: 'cp='+cp,
			  success: function(data){
				  $("#ville").empty().append(data.list);
				  $("#departement_id").empty().append(data.list_departement);
				  $("#ville").select2();
				  $("#departement_id").select2();
			  },
			  dataType: 'json'
		});
	}
}
</script>
<?php $this->load->view('mobile2014/partial/footer')?>