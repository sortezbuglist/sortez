<?php $this->load->view('mobile2014/partial/head')?>
<div class="wrapper head">
    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
     <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
        <div class="pure-g">
            <div class="pure-u-3-5">
            	<div class="pad-tb-15 titre align-center">
                    <?php echo $titre;?>
                </div>
            </div>
            <div class="pure-u-2-5">
                <div class="btn-menu">
                    <a href="<?php echo site_url('front/user_fidelity/filter')?>">Filtrer</a>
                </div>
            </div>
    </div>
</div>
<div class="wrapper page-white">
    <!--TITRE-->
   
    <div class="consommation">
    <?php if(!empty($oColCard_event_list2)):?>
     <?php foreach($oColCard_event_list2 as $colCard_event_list) : ?>
     <div class="product">
	    <?php 
	    echo $colCard_event_list->NomSociete;
		?>
     	<?php if (isset($colCard_event_list->tampon_value_added) && $colCard_event_list->tampon_value_added!=""): ?>
     		
	            <span>Coup de tampon : <?php echo $colCard_event_list->bonplan_titre;?></span>
	            <a href="<?php echo site_url("front/user_fidelity/fidelity_event_details_mobile/".$colCard_event_list->id)?>" class="btn"><img src="<?php echo img_url('puce-link.png') ?>" style="width:25px; margin-top: 10px; margin-right: 10px;"/></a>
	            <div class="pure-g bg-red">
	                    <div class="pure-u-1-2 bg-red pad-tb-5 align-right price"><div class="pad-lr-5"><?php echo (isset($colCard_event_list->cumul_tampon) && $colCard_event_list->cumul_tampon!="") ? $colCard_event_list->cumul_tampon :0?></div></div>
	                    <div class="pure-u-1-2 bg-blue pad-tb-5 align-right price "><div class="pad-lr-5"><?php echo (isset($colCard_event_list->objectif_tampon) && $colCard_event_list->objectif_tampon!="") ? $colCard_event_list->objectif_tampon : 0; ?></div></div>
	            </div>
	     <?php elseif(isset($colCard_event_list->capital_value_added) && $colCard_event_list->capital_value_added!="") :?>
	     	<?php if($colCard_event_list->objectif_capital > $colCard_event_list->cumul_capital):?>
	            <span>Capitalisation : <?php echo $colCard_event_list->bonplan_titre;  echo $colCard_event_list->id?></span>
	            <a href="<?php echo site_url("front/user_fidelity/fidelity_event_details_mobile/".$colCard_event_list->id)?>" class="btn"><img src="<?php echo img_url('puce-link.png') ?>" style="width:25px; margin-top: 10px; margin-right: 10px;"/></a>
	            <div class="pure-g bg-red">
	            <?php 
	            	$ui_begin = round((24*$colCard_event_list->cumul_capital )/$colCard_event_list->objectif_capital, 0, PHP_ROUND_HALF_UP);   
					$ui_begin = ($ui_begin < 4) ? 4 : $ui_begin;
					$ui_begin = ($ui_begin > 20) ? 20 : $ui_begin;
					
					$ui_end =   round(24 - round($ui_begin, 0, PHP_ROUND_HALF_UP));
	            ?>
	                    <div class="pure-u-<?php echo $ui_begin?>-24 bg-red pad-tb-5 align-right price">
	                        <div class="pad-lr-5"><?php echo (isset($colCard_event_list->cumul_capital) && $colCard_event_list->cumul_capital!="") ? $colCard_event_list->cumul_capital : 0?> &euro;</div>
	                     </div>
	                    <div class="pure-u-<?php echo $ui_end?>-24 bg-blue pad-tb-5 align-right price ">
	                        <div class="pad-lr-5"><?php echo (isset($colCard_event_list->objectif_capital) && $colCard_event_list->objectif_capital!="") ? $colCard_event_list->objectif_capital : 0;?>&euro;</div>
	                    </div>
	            </div>
	     	<?php else :?>
	            <span>Bon plan :<?php echo $colCard_event_list->bonplan_titre;?> </span>
	            <a href="<?php echo site_url("front/user_fidelity/fidelity_event_details_mobile/".$colCard_event_list->id)?>" class="btn"><img src="<?php echo img_url('puce-link.png') ?>" style="width:25px; margin-top: 10px; margin-right: 10px;"/></a>
	            <div class="pure-g bg-blue">
	                    <div class="pure-u-1-2 bg-blue pad-tb-5 align-left">
	                        <div class="pad-lr-5">Cumul consommation</div>
	                    </div>
	                    <div class="pure-u-1-2 bg-blue pad-tb-5 align-right price "><div class="pad-lr-5"><?php echo  $colCard_event_list->objectif_capital;?> &euro;</div></div>
	            </div>
	            <br />
	            <div class="pure-g  bg-red">
	                <div class="pure-u-1-2 bg-red pad-tb-5 align-left">
	                       <div class="pad-lr-5"> <?php echo date("d/m/Y", strtotime($colCard_event_list->date_use));?></div>
	                </div>
	                <div class="pure-u-1-2 bg-red pad-tb-5 align-right price ">
	                    <div class="pad-lr-5">Bon plan validé</div>
	                </div>
	             </div>   
	          <?php endif;?>      
	     <?php endif;?>
	     </div>   
	<?php endforeach;?>
	<?php else :?>
	 <div class="product">
		Vous n'avez aucune donnée
		</div>
	<?php endif;?>
    </div>
    <div class="clr"></div>
</div>
<?php $this->load->view('mobile2014/partial/menu')?>
<?php $this->load->view('mobile2014/partial/footer')?>