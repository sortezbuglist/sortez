<?php $this->load->view('mobile2014/partial/head')?>
<div class="wrapper head">
    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
   <?php //$this->load->view('mobile2014/partial/titre_head_view')?>
        <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
             <?php $this->load->view('mobile2014/partial/header_titre')?>
        
</div>
<div class="wrapper page-black">
    <!--forms-->
    <div class="wrap forms">
      <form class="pure-form pure-form-stacked">

          <div class="pure-g cart-phys">
              <div class="pure-u-1">
                  <p class="f-size18">
                    Nous vous remercions d'avoir souscris un abonnement à la carte Vivresaville.
                  </p>
              </div>
              <div class="pure-u-1-2">
                  <div class="pad-10">
                  <img src="<?php echo img_url('photo-souscription.png') ?>" alt=""/>
                  </div>
              </div>
              <div class="pure-u-1-2">
                  <div class="pad-10 align-left">
                      <p class="f-size14">
                          Un E-mail de confirmation a été envoyé à l'adresse e-mail enregistré pour activer votre compte,<br/>
                          N'ayant pas reçu le mail, veuillez vérifier dans votre boite Spam avant de contacter un administrateur Sortez.<br/>
                          Par la suite, vous pouvez visualiser votre carte Vivresaville, l'utiliser auprès de nos partenaires et compte personnel et sécurisé
                      </p>
                  </div>
              </div>
          </div>
          <?php
          $actual_link_soutenons = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
          //var_dump(parse_url($actual_link_soutenons));
          $all_data_from_url = parse_url($actual_link_soutenons);
              ?>
              <div class="wrap links">
                  <div class="pad-tb-5">
                      <a href="<?php echo site_url('auth/login')?>" class="pure-button pure-button-primary" target="_blank">Veuillez accéder à votre compte</a>
                  </div>
              </div>
      </form>
    </div>
    <!--forms--> <div class="clr"></div>
    </div>
</div>
<?php
if(isset($all_data_from_url["query"]) && $all_data_from_url["query"]=="form_soutenons=1"){}else {
    $this->load->view('mobile2014/partial/menu');
}
?>
<?php $this->load->view('mobile2014/partial/footer')?>