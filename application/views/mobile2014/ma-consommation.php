	<?php $this->load->view('mobile2014/partial/head')?>
<?php $this->load->view('mobile2014/partial/menu')?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <style type="text/css">
        .fontinfo{
            font-size: 15px!important;
            color: #ed2685!important;
        }
        .interlign{
            /*Line-Height: 8pt;*/
            line-height: 10px;
        }
        .titreuserfd{
            text-align: center!important;
            font-size: 25px!important;
        }
        .fontlabel{
            color: #0653a3!important;  
            font-size: 15px!important;           
        }
        .infocolor{
            color: #ed2685!important;            
        }
        .blocinfo{
            min-width:165px!important;
        }
    </style>

	<div class="wrapper head">
	    <?php //$this->load->view('mobile2014/partial/logo_head_view')?>
	     <?php //$this->load->view('mobile2014/partial/header_titre_menu_carte')?>
	        <!-- <div class="pure-g align-center titre"> -->
	            <!-- <div class="pure-u-3-5"> -->
	            	<!-- <div class="pad-tb-15 titre align-center"> -->
                        <div class="row">
                            <div class="col-12 align-center titreuserfd titre p-0">
	                    <span style="font-size: 20px!important;"><?php echo $titre;?></span>
                            </div>
                        </div>
	                <!-- </div> -->
	            <!-- </div> -->
	            <!-- <div class="pure-u-2-5">
	                <div class="btn-menu">
	                    <a href="<?php echo site_url('front/user_fidelity/filter')?>">Filtrer</a>
	                </div>
	            </div> -->
	       <!-- </div> -->
	</div>
	<div class="wrapper page-white">
	    <!--TITRE-->
	   
	    <div class="consommation">
	    	<?php if(!empty($fiche_capital) OR !empty($fiche_tampon) OR !empty($fiche_remise) ){?>
	     			<div class="product">
                        <?php if (isset($fiche_tampon) AND !empty($fiche_tampon)){ ?>
                            <?php foreach ( $fiche_tampon as $ofiche_tampon) { ?>
                                <div class="row pt-2">
                                <div class="col-4 fontlabel blocinfo">Partenaire : </div> <div class="fontinfo col-4 blocinfo"><?php if (isset($ofiche_tampon->NomSociete)) echo $ofiche_tampon->NomSociete;?></div>
                                </div>
                                <div class="row pb-2">
                                <div class=" col-4 fontlabel blocinfo">Coup de Tampons :</div><div class="fontinfo col-4 blocinfo"><?php if (isset($ofiche_tampon->description)) echo $ofiche_tampon->description;?></div>
                                </div>                                
                                <table border="0" width="100%">
                                <tr>
                                    <td width="60"><div class="pad-lr-5"><a href="<?php echo site_url('front/user_fidelity/user/'.$ofiche_tampon->id_commercant);?>"><img src="<?php echo img_url("id_card.png") ?>" alt="" width="50"/></a></div></td>
                                    <td width="60"><div class="pad-lr-5"><a href="mailto:<?php echo $ofiche_tampon->Email;?>"><img src="<?php echo img_url("email.png") ?>" alt="" width="50"/></a></div></td>
                                    <td width="60"><div class="pad-lr-5"><a href="tel:<?php echo $ofiche_tampon->TelMobile;?>"><img src="<?php echo img_url("mobile_phone.png") ?>" alt="" width="50"/></a></div></td>
                                    <td><div><a href="<?php echo base_url();?><?php echo $ofiche_tampon->nom_url;?>" target="_blank"><img src="<?php echo img_url("site_web.jpg") ?>" alt="" width="50"/></a></div></td>
                                </tr>
                            </table>
                                <div class="pure-g bg-red">
                                    <div class="<?php echo blue_class_progress($ofiche_tampon->solde_tampon, $ofiche_tampon->tampon_value)?>">
                                        <div class="pad-lr-5"><?php echo (!empty($ofiche_tampon->solde_tampon)) ? $ofiche_tampon->solde_tampon : 0?></div>
                                    </div>
                                    <div class="<?php echo red_class_progess($ofiche_tampon->solde_tampon, $ofiche_tampon->tampon_value)?>"><div class="pad-lr-5"><?php echo (!empty($ofiche_tampon->tampon_value)) ? $ofiche_tampon->tampon_value : 0?></div></div>
                                </div>
                     <?php } ?>
                         <?php } ?>
                        <?php if (isset($fiche_capital) AND !empty($fiche_capital)){ ?>
                        <?php foreach ( $fiche_capital as $ofiche_capital) { ?>
                                <div class="row pt-2">
                                <div class="fontlabel col-4 blocinfo">Partenaire : </div><div class="fontinfo col-4 blocinfo"><?php if (isset($ofiche_capital->NomSociete)) echo $ofiche_capital->NomSociete;?></div>
                                </div>
                                <div class="row pb-2">
                                <div class="fontlabel col-4 blocinfo">Capitalisation :</div><div class="fontinfo col-4 blocinfo"><?php if (isset($ofiche_capital->description))echo $ofiche_capital->description;?></div>
                                </div>                                
                                <table border="0" width="100%">
                                <tr>
                                    <td width="60"><div class="pad-lr-5"><a href="<?php echo site_url('front/user_fidelity/user/'.$ofiche_tampon->id_commercant);?>"><img src="<?php echo img_url("id_card.png") ?>" alt="" width="50"/></a></div></td>
                                    <td width="60"><div class="pad-lr-5"><a href="mailto:<?php echo $ofiche_tampon->Email;?>"><img src="<?php echo img_url("email.png") ?>" alt="" width="50"/></a></div></td>
                                    <td width="60"><div class="pad-lr-5"><a href="tel:<?php echo $ofiche_tampon->TelMobile;?>"><img src="<?php echo img_url("mobile_phone.png") ?>" alt="" width="50"/></a></div></td>
                                    <td><div><a href="<?php echo base_url();?><?php echo $ofiche_tampon->nom_url;?>" target="_blank"><img src="<?php echo img_url("site_web.jpg") ?>" alt="" width="50"/></a></div></td>
                                </tr>
                                </table>
                                <div class="pure-g bg-red">
                                    <div class="<?php echo blue_class_progress($ofiche_capital->solde_capital, $ofiche_capital->objectif)?>">
                                        <div class="pad-lr-5"><?php echo (!empty($ofiche_capital->solde_capital)) ? $ofiche_capital->solde_capital : 0?>&euro;</div>
                                    </div>
                                    <div class="<?php echo red_class_progess($ofiche_capital->solde_capital, $ofiche_capital->objectif)?> "><div class="pad-lr-5"><?php echo (!empty($ofiche_capital->objectif)) ? $ofiche_capital->objectif : 0?> &euro;</div></div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <?php if (isset($fiche_remise) AND !empty($fiche_remise)){ ?>
                        <?php foreach ( $fiche_remise as $ofiche_remise) { ?>
                                <div class="row pt-2">
                                <div class="fontlabel col-4 blocinfo">Partenaire :</div><div class="fontinfo col-4 blocinfo"><?php if (isset($ofiche_remise->NomSociete)) echo $ofiche_remise->NomSociete;?></div>
                                </div>
                                <div class="row pb-2">
                                <div class="fontlabel col-4 blocinfo">Remise directe :</div><div class="fontinfo col-4 blocinfo"><?php if (isset($ofiche_remise->description)) echo $ofiche_remise->description;?></div>
                                </div>                                
                                <table border="0" width="100%">
                                <tr>
                                    <td width="60"><div class="pad-lr-5"><a href="<?php echo site_url('front/user_fidelity/user/'.$ofiche_tampon->id_commercant);?>"><img src="<?php echo img_url("id_card.png") ?>" alt="" width="50"/></a></div></td>
                                    <td width="60"><div class="pad-lr-5"><a href="mailto:<?php echo $ofiche_tampon->Email;?>"><img src="<?php echo img_url("email.png") ?>" alt="" width="50"/></a></div></td>
                                    <td width="60"><div class="pad-lr-5"><a href="tel:<?php echo $ofiche_tampon->TelMobile;?>"><img src="<?php echo img_url("mobile_phone.png") ?>" alt="" width="50"/></a></div></td>
                                    <td><div><a href="<?php echo base_url();?><?php echo $ofiche_tampon->nom_url;?>" target="_blank"><img src="<?php echo img_url("site_web.jpg") ?>" alt="" width="50"/></a></div></td>
                                </tr>
                            </table>
                                <div class="pure-g bg-blue fontinfo">
                                    <div class="pure-u-1-2 bg-blue align-left">
                                        <div class="pad-lr-5">Cumul consommation</div>
                                    </div>
                                    <div class="pure-u-1-2 bg-blue align-right">
                                        <div class="pad-lr-5">
                                            <b>
                                                <?php echo (!empty($ofiche_remise->solde_remise)) ? $ofiche_remise->solde_remise : 0;?> &euro;
                                            </b>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
		     </div>   


		<?php }else{?>
		 <div class="product">
			Vous n'avez aucune donnée
			</div>
            <?php } ?>
	    </div>
	    <div class="clr"></div>
	</div>
<?php $this->load->view('mobile2014/partial/footer')?>