<div class="titre">
    <h1 class="titre_societe">
        <?php echo $oInfoCommercant->NomSociete; ?>
    </h1>
</div>
<div class="contenus">
    <?php
        if(isset($data_seo->Description_1) && $data_seo->Description_1 != null){
            $aujourdhui = date("Y-m-d");
            $nbr_jours =  NbJours($data_seo->date_debut, $aujourdhui);
            $date_1 = $data_seo->jour;
            $date_2 = $data_seo->jour + $data_seo->jour;
            $date_3 = $data_seo->jour + $data_seo->jour + $data_seo->jour;
            $date_4 = $data_seo->jour + $data_seo->jour + $data_seo->jour + $data_seo->jour;
            $date_5 = $data_seo->jour + $data_seo->jour + $data_seo->jour + $data_seo->jour + $data_seo->jour;

            if($nbr_jours <= $date_1){
                echo $data_seo->Description_1;
            }else if($nbr_jours <= $date_2){
                echo $data_seo->Description_2;
            }else if($nbr_jours <= $date_3){
                echo $data_seo->Description_3;
            }else if($nbr_jours <= $date_4){
                echo $data_seo->Description_4;
            }else if($nbr_jours <= $date_5){
                echo $data_seo->Description_5;
            }else{
                $data_send['date_debut'] = date("Y-m-d");
                $data_send['IdCommercant'] = $data_seo->IdCommercant;
                $thissses = get_instance();
                $thissses->load->model('Seo_model');
                $save = $thissses->Seo_model->saverecords($data_send,$data_seo->IdCommercant);
                if($save != ''){
                    header("Refresh:0");
                }
            }
        }
        else{
            echo "Vous n'avez pas encore de déscription!";
        }

    ?>
    <?php
        function NbJours($debut, $fin) {

            $tDeb = explode("-", $debut);
            $tFin = explode("-", $fin);

            $diff = mktime(0, 0, 0, $tFin[1], $tFin[2], $tFin[0]) -
                mktime(0, 0, 0, $tDeb[1], $tDeb[2], $tDeb[0]);

            return(($diff / 86400)+1);

        }
    ?>
</div>

<br>
<button class="button font contenus" type="button" class="presentation_commercants_">
    <a href="<?php echo base_url($oInfoCommercant->nom_url.'/presentation_commercants') ?>">Présentation commerçant<!-- Page d'accueil --></a>

</button>
<br><br>
<div class="footer font">
    <?php
    $this->load->view("Homepage/footer");
    ?>
</div>