<head>
    <title>Accueil</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('application/views/Homepage/css/responsive_accueil.css'); ?>">
</head>
<?php
        $path_img_gallery_bg = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/bg/';
        $path_img_gallery_bg_sample = 'application/resources/front/photoCommercant/imagesbank/bg_sample/';
        $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
       
        if (file_exists($path_img_gallery_bg . $oInfoCommercant->background_image)) {
            $background_image_to_show = base_url() . $path_img_gallery_bg . $oInfoCommercant->background_image;

        } else if (file_exists($path_img_gallery_bg_sample . $oInfoCommercant->background_image)) {
            $background_image_to_show = base_url() . $path_img_gallery_bg_sample . $oInfoCommercant->background_image;
 
        } else {
            $background_image_to_show = base_url() . $path_img_gallery_old . $oInfoCommercant->background_image;
      
        }
if (isset($oInfoCommercant->id_glissiere) AND $oInfoCommercant->id_glissiere != null) {
    $thi = get_instance();
    $glisieres = $this->load->mdlcommercant->get_glissiere_by_id_com($oInfoCommercant->IdCommercant);
    $data['glisieres'] = $glisieres;
    $data['oInfoCommercant'] = $oInfoCommercant;

}

?>
<?php
     //var_dump($oInfoCommercant);
     //var_dump($glisieres->Transparent_du_banniere);
     //var_dump($video_background);
//	var_dump($background_image_to_show);
	
?>
<style type="text/css">
body{
    <?php if(isset($oInfoCommercant->background_image) && $oInfoCommercant->background_image != null){ ?>
	    background: url("<?php echo $background_image_to_show; ?>") no-repeat center center fixed;
    <?php }else{ ?>
        background-image: url(<?php echo base_url('wpimages/wpe3440087_06.jpg') ?>) !important;
    <?php } ?>
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
	background-size: 100% 100%;
	background-repeat: no-repeat;
    background-color: rgba(255,255,255,0.5);
}

.font{
	background-color: <?php if (isset($oInfoCommercant->bandeau_color) AND $oInfoCommercant->bandeau_color!=null AND $oInfoCommercant->bandeau_color!= '' ) {echo $oInfoCommercant->bandeau_color;}else{echo '#3453a2';} ?> ;
}

#videoMessage{
    background-color: rgba(0,0,0,<?php echo $glisieres->Transparent_du_banniere; ?>);
}
</style>

<div class="container mt-5 mb-5 d-flex h-100" id="container_custom">
    <div class="contenu justify-content-center align-self-center" id="contenu_custom">
        <div class="header font">
            <?php
               $this->load->view("Homepage/header",$data);
            ?>
<!--             <div class="row">-->
<!--                <div class="image_decoration">-->
<!--                    <img src="--><?php //echo base_url('assets/soutenons/decoration_home.png') ?><!--" width="100%" class="img-fluid">-->
<!--                </div>-->
<!--            </div>-->
        </div>
        <div class="menu-milieu font">

            <?php
                $this->load->view("Homepage/contenu",$data);
            ?>
        </div>

    </div>
</div>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>-->
<!--<script type="text/javascript">-->
<!--    $(document).ready(function(){-->
<!--        console.log($(window).width());-->
<!--        var isMobile = false; //initiate as false-->
<!--        // device detection-->
<!--        if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)-->
<!--            || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {-->
<!--            isMobile = true;-->
<!--        }-->
<!--        if(isMobile == true){-->
<!--            console.log($(window).width());-->
<!--            $('#container_custom').removeClass('mt-5 mb-5');-->
<!--            $('#container_custom').addClass('d-flex h-100');-->
<!--            $('#contenu_custom').addClass('justify-content-center align-self-center');-->
<!--        }-->
<!--        console.log(isMobile);-->
<!--    });-->
<!--</script>-->
