<?php
$thiss = get_instance();
$thiss->load->model('mdlcommercant');
$video_background = $thiss->load->mdlcommercant->get_video_bg_by_idcom($oInfoCommercant->IdCommercant);

?>
<?php
    $path_img_gallery_bn = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/bn/';
    $path_video_gallery_logo_old = 'application/resources/front/photoCommercant/images/';
    $path_video_gallery_bg_image = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id.'/bg/';
    $path_video_gallery_logo_new = 'application/resources/front/photoCommercant/imagesbank/'.$oInfoCommercant->user_ionauth_id.'/lg/';
    $path_video_gallery_bg = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/bgvideo/';
    if (isset($video_background->background_video) && $video_background->background_video!= null && file_exists($path_video_gallery_bg . $video_background->background_video))
        $Video = base_url() . $path_video_gallery_bg . $video_background->background_video;
    else
        $Video = '';



?>
<?php
    // var_dump($oInfoCommercant);
     //var_dump($video_background);
	//var_dump($Video);
	
?>
<header>

            <div class="overlay"></div>
        <div id="videoDiv_home" class="row">
            <div id="videoBlock_home">
                    <?php if($Video != ''){ ?>
                      <video id="video_home"  preload autoplay muted playsinline loop>
                        <source src="<?php echo $Video; ?> " type="<?php echo $video_background->Type; ?>">
                      </video>
                    <?php
                            }else if (file_exists($path_img_gallery_bn . $oInfoCommercant->bandeau_top) && isset($oInfoCommercant->bandeau_top) && $oInfoCommercant->bandeau_top != "") {
                                echo '<img style="max-width:100%;width:100%;height:100%" src="' . base_url() . $path_img_gallery_bn . $oInfoCommercant->bandeau_top . '" "/>';
                            }else{
                                echo '<img style="max-width:100%;width:100%;height:100%" src="'.base_url('application/resources/privicarte/images/bandeau_top_default.jpg').'" "/>';
                            }
                    ?>
            </div>
        </div>
    <div id="videoMessage">
        <div class="videoClick" >
              <div class="container">
                <?php
                $path_img_gallery_lg = 'application/resources/front/photoCommercant/imagesbank/' . $oInfoCommercant->user_ionauth_id . '/lg/';
                $path_img_gallery_old = 'application/resources/front/photoCommercant/images/';
                if (file_exists($path_img_gallery_lg . $oInfoCommercant->Logo))
                    $logo_image_to_show = base_url() . $path_img_gallery_lg . $oInfoCommercant->Logo;
                else if (file_exists($path_img_gallery_old . $oInfoCommercant->Logo))
                    $logo_image_to_show = base_url() . $path_img_gallery_old . $oInfoCommercant->Logo;
                else
                    $logo_image_to_show = '';

                if ($oInfoCommercant->Logo != "" && $oInfoCommercant->Logo != NULL) {
                    ?>
                    <div class="row">
                        <div class="image_logo">
                            <?php if (isset($logo_image_to_show) && $logo_image_to_show!='') { ?>
                                <img width="auto" height="215" src="<?php echo $logo_image_to_show; ?>" alt="<?php echo $oInfoCommercant->NomSociete; ?>"/>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

              </div>
        </div>
    </div>
</header>