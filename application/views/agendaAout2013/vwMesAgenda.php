<?php $data["zTitle"] = 'Agenda'; ?>
<?php $this->load->view("adminAout2013/includes/vwHeader2013", $data); ?>


<script type="text/javascript" charset="utf-8">
	$(function() {
			$(".tablesorter")
				.tablesorter({widthFixed: true, widgets: ['zebra'], headers: {7: { sorter: false}, 8: {sorter: false} }})
				.tablesorterPager({container: $("#pager")});
		});
</script>    <div id="divMesAnnonces" class="content" align="center">
        <form name="frmMesAnnonces" id="frmMesAnnonces" action="" method="POST" enctype="multipart/form-data">
			
            
            <p><span align="left"><a style = "text-decoration:none;" href="<?php echo site_url("front/utilisateur/contenupro") ;?>"><input type = "button" id = "btnnew" value ="retour au menu" /></a></span></p>
            
            
            <?php if (isset($limit_annonce_add) && $limit_annonce_add==1) { echo "Vous avez atteint la limite de 20 annonces.";} else {?>
            <p><span align="left">
            <a style = "text-decoration:none;" href="<?php echo site_url("front/utilisateur/ficheAgenda/$idCommercant") ;?>">
				<input type = "button" id = "btnnew" value ="Ajouter un événement" onclick="document.location='<?php echo site_url("front/utilisateur/ficheAgenda/$idCommercant") ;?>';"/>
				</a></span></p>
                <?php }?>
                
				
				
            <h1>Mon Agenda</h1>
				
				<div id="container">
					<table cellpadding="1" class="tablesorter">
						<thead>
							<tr>                    
								<th>Titre</th>
								<th>Description</th>
								<th>Date debut</th>
								<th>Date fin</th>
								<th>Date ajout</th>
								<th>A la Une</th>
								<th>Etat</th>
								<th width="10"></th>
								<th width="10"></th>
							</tr>
						</thead>
						<tbody> 
                        <?php if (count($toListeAgenda)>0) { ?>
							<?php foreach($toListeAgenda as $oListeAgenda){ ?>
								<tr>                    
									<td><?php echo $oListeAgenda->nom_manifestation ; ?></td>
									<td><?php echo truncate(strip_tags($oListeAgenda->description),150," ..."); ?></td>
									<td><?php 
									if (convertDateWithSlashes($oListeAgenda->date_debut)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeAgenda->date_debut) ; ?></td>
									<td><?php 
									if (convertDateWithSlashes($oListeAgenda->date_fin)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeAgenda->date_fin) ; ?></td>
									<td><?php 
									if (convertDateWithSlashes($oListeAgenda->date_depot)!="00/00/0000")
									echo convert_Sqldate_to_Frenchdate($oListeAgenda->date_depot) ; ?></td>
									<td><?php 
									//echo $oListeAgenda->alaune; 
									if ($oListeAgenda->alaune != "1") echo  "Non"; else echo "Oui";
									?></td>
									<td><?php 
									//echo $oListeAgenda->IsActif; 
									if ($oListeAgenda->IsActif == "1") echo  "Activé";
									if ($oListeAgenda->IsActif == "0") echo  "A valider";
									if ($oListeAgenda->IsActif == "2") echo  "Annulé";
									if ($oListeAgenda->IsActif == "3") echo  "Supprimé";
									?></td>
									<td><a href="<?php echo site_url("front/utilisateur/ficheAgenda/" . $oListeAgenda->IdCommercant . "/" . $oListeAgenda->id ) ; ?>">Modifier</a></td>
									<td><a href="<?php echo site_url("front/utilisateur/deleteAgenda/" . $oListeAgenda->id . "/" . $oListeAgenda->IdCommercant) ; ?>" onclick="if (!confirm('voulez-vous vraiment supprimer cette événnement?')){ return false ; }">Supprimer</a></td>
								</tr>
							<?php } ?>
                        <?php } ?>
						</tbody>
					</table>
					<div id="pager" class="pager" style="text-align:center;">
							<img src="<?php echo GetImagePath("front/"); ?>/first.png" class="first"/>
							<img src="<?php echo GetImagePath("front/"); ?>/prev.png" class="prev"/>
							<input type="text" class="pagedisplay"/>
							<img src="<?php echo GetImagePath("front/"); ?>/next.png" class="next"/>
							<img src="<?php echo GetImagePath("front/"); ?>/last.png" class="last"/>
							<select class="pagesize" style="visibility:hidden">
								<option selected="selected"  value="10">10</option>
								<option value="20">20</option>
								<option value="30">30</option>
								<option  value="40">40</option>
							</select>
					</div>
				</div>
        </form>
    </div>
<?php $this->load->view("adminAout2013/includes/vwFooter2013"); ?>