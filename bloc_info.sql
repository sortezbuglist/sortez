-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 17 jan. 2019 à 09:59
-- Version du serveur :  5.7.21
-- Version de PHP :  7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sortez`
--

-- --------------------------------------------------------

--
-- Structure de la table `bloc_info`
--

DROP TABLE IF EXISTS `bloc_info`;
CREATE TABLE IF NOT EXISTS `bloc_info` (
  `id` int(11) NOT NULL,
  `IdGlissiere` int(11) DEFAULT NULL,
  `IsActif` varchar(1) DEFAULT NULL,
  `Num_bloc` varchar(255) DEFAULT NULL,
  `nbre_bloc` varchar(5) DEFAULT NULL,
  `photo1` varchar(255) DEFAULT NULL,
  `photo2` varchar(255) DEFAULT NULL,
  `champ1_content` text,
  `champ2_content` text,
  `champbp_content` text,
  `champfd_content` text,
  `IsActif_button1` varchar(2) DEFAULT NULL,
  `IsActif_button2` varchar(2) DEFAULT NULL,
  `IsActif_buttonbp` varchar(2) DEFAULT NULL,
  `IsActif_buttonfd` varchar(2) DEFAULT NULL,
  `button1_text` varchar(255) DEFAULT NULL,
  `button2_text` varchar(255) DEFAULT NULL,
  `buttonbp_text` varchar(255) DEFAULT NULL,
  `buttonfd_text` varchar(255) DEFAULT NULL,
  `local_link1` varchar(255) DEFAULT NULL,
  `local_link2` varchar(255) DEFAULT NULL,
  `local_linkbp` varchar(255) DEFAULT NULL,
  `local_linkfd` varchar(255) DEFAULT NULL,
  `external_link1` varchar(255) DEFAULT NULL,
  `external_link2` varchar(255) DEFAULT NULL,
  `external_linkbp` varchar(255) DEFAULT NULL,
  `external_link1fd` varchar(255) DEFAULT NULL,
  `Num_page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
