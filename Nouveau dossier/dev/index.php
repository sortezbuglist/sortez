<?php

function checksub($domain)
{
    $exp = explode('.', $domain);
    if (count($exp) > 3) {
        return true;
    } else {
        return false;
    }
}

if (checksub($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])) {
    $current_url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $current_url = str_replace("www.", "", $current_url);
    $current_url = explode(".", $current_url);

    if ($_SERVER['HTTP_HOST'] == "antibes-juan-les-pins.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.antibes-juan-les-pins.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/6';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "cagnes-sur-mer.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.cagnes-sur-mer.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/4';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "cannes.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.cannes.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/3';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "les-vallees-de-la-roya-bevera.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.les-vallees-de-la-roya-bevera.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/11';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "nice.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.nice.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/8';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "saint-laurent-du-var.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.saint-laurent-du-var.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/10';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "villefranche-sur-mer.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.villefranche-sur-mer.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/7';?>";</script><?php
    } else {

        ?>
        <script type="application/javascript">
            window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $current_url[0] . '/presentation';?>";
            //alert("<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $current_url[0] . '/presentation';?>");
        </script>
        <?php
    }

}

if ($_SERVER['SERVER_NAME'] == "randawilly.ovh" || $_SERVER['SERVER_NAME'] == "www.randawilly.ovh") {
    ?>

    <!DOCTYPE html>

    <html lang="fr">

    <head>

        <meta charset="UTF-8">

        <title>vivresaville-sorties-loisirs-articles-agenda</title>

        <meta name="generator" content="Serif WebPlus X8 (16,0,4,32)">

        <meta name="viewport" content="width=1250">

        <link rel="stylesheet" type="text/css" href="wpscripts_vsv/wpstyles.css">

        <style type="text/css">

            .OBJ-1 {
                border-radius: 20px / 20px;
            }

            @font-face {
                font-family: 'Futura Md';
                src: url('wpscripts_vsv/wp545f5f8e.ttf');
            }

            .OBJ-2, .OBJ-2:link, .OBJ-2:visited {
                background-image: url('wpimages_vsv/wpdb9f4e8f_06.png');
                background-repeat: no-repeat;
                background-position: 0px 0px;
                text-decoration: none;
                display: block;
                position: absolute;
            }

            .OBJ-2:hover {
                background-position: 0px -140px;
            }

            .OBJ-2:active, a:link.OBJ-2.Activated, a:link.OBJ-2.Down, a:visited.OBJ-2.Activated, a:visited.OBJ-2.Down, .OBJ-2.Activated, .OBJ-2.Down {
                background-position: 0px -70px;
            }

            .OBJ-2.Disabled, a:link.OBJ-2.Disabled, a:visited.OBJ-2.Disabled, a:hover.OBJ-2.Disabled, a:active.OBJ-2.Disabled {
                background-position: 0px -210px;
            }

            .OBJ-2:focus {
                outline-style: none;
            }

            button.OBJ-2 {
                background-color: transparent;
                border: none 0px;
                padding: 0;
                display: inline-block;
                cursor: pointer;
            }

            button.OBJ-2:disabled {
                pointer-events: none;
            }

            .OBJ-2.Inline {
                display: inline-block;
                position: relative;
                line-height: normal;
            }

            .OBJ-2 span, .OBJ-2:link span, .OBJ-2:visited span {
                color: #dc188e;
                font-family: "Futura Md", sans-serif;
                font-weight: normal;
                text-decoration: none;
                text-align: center;
                text-transform: none;
                font-style: normal;
                left: 13px;
                top: 10px;
                width: 645px;
                height: 46px;
                line-height: 46px;
                font-size: 34px;
                display: block;
                position: absolute;
                cursor: pointer;
            }

            .OBJ-2:hover span {
                color: #dc198f;
            }

            .OBJ-3 {
                line-height: 70px;
            }

            .OBJ-4, .OBJ-4:link, .OBJ-4:visited {
                background-image: url('wpimages_vsv/wpd44b3652_06.png');
                background-repeat: no-repeat;
                background-position: 0px 0px;
                text-decoration: none;
                display: block;
                position: absolute;
            }

            .OBJ-4:hover {
                background-position: 0px -120px;
            }

            .OBJ-4:active, a:link.OBJ-4.Activated, a:link.OBJ-4.Down, a:visited.OBJ-4.Activated, a:visited.OBJ-4.Down, .OBJ-4.Activated, .OBJ-4.Down {
                background-position: 0px -60px;
            }

            .OBJ-4.Disabled, a:link.OBJ-4.Disabled, a:visited.OBJ-4.Disabled, a:hover.OBJ-4.Disabled, a:active.OBJ-4.Disabled {
                background-position: 0px -180px;
            }

            .OBJ-4:focus {
                outline-style: none;
            }

            button.OBJ-4 {
                background-color: transparent;
                border: none 0px;
                padding: 0;
                display: inline-block;
                cursor: pointer;
            }

            button.OBJ-4:disabled {
                pointer-events: none;
            }

            .OBJ-4.Inline {
                display: inline-block;
                position: relative;
                line-height: normal;
            }

            .OBJ-4 span, .OBJ-4:link span, .OBJ-4:visited span {
                color: #ffffff;
                font-family: Tahoma, sans-serif;
                font-weight: normal;
                text-decoration: none;
                text-align: center;
                text-transform: none;
                font-style: normal;
                left: 0px;
                top: 16px;
                width: 500px;
                height: 27px;
                line-height: 27px;
                font-size: 21px;
                display: block;
                position: absolute;
                cursor: pointer;
            }

            .OBJ-5 {
                line-height: 60px;
            }

            @font-face {
                font-family: 'Futura Hv';
                src: url('wpscripts_vsv/wpe3dd8930.ttf');
            }

            .P-1 {
                text-align: center;
                margin-bottom: 1.3px;
                line-height: 46.7px;
                font-family: "Futura Hv", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #000000;
                background-color: transparent;
                font-variant: normal;
                font-size: 40.0px;
                vertical-align: 0;
            }

            .P-2 {
                text-align: center;
                margin-bottom: 5.3px;
                line-height: 48.0px;
                font-family: "Futura Hv", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #000000;
                background-color: transparent;
                font-variant: normal;
                font-size: 40.0px;
                vertical-align: 0;
            }

            .P-3 {
                text-align: center;
                margin-bottom: 5.3px;
                line-height: 48.0px;
                font-family: "Futura Hv", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #000000;
                background-color: transparent;
                font-variant: normal;
                font-size: 48.0px;
                vertical-align: 0;
            }

            .C-1 {
                font-family: "Futura Hv", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #da188e;
                background-color: transparent;
                text-decoration: none;
                font-variant: normal;
                font-size: 48.0px;
                vertical-align: 0;
            }

            .P-4 {
                text-align: center;
                margin-bottom: 2.7px;
                line-height: 34.7px;
                font-family: "Verdana", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #000000;
                background-color: transparent;
                font-variant: normal;
                font-size: 32.0px;
                vertical-align: 0;
            }

        </style>

        <script type="text/javascript" src="wpscripts_vsv/jquery.js"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                $("a.ActiveButton").bind({
                    mousedown: function () {
                        if ($(this).attr('disabled') === undefined) $(this).addClass('Activated');
                    }, mouseleave: function () {
                        if ($(this).attr('disabled') === undefined) $(this).removeClass('Activated');
                    }, mouseup: function () {
                        if ($(this).attr('disabled') === undefined) $(this).removeClass('Activated');
                    }
                });

            });

        </script>

        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    </head>

    <body style="height:980px;background:url('wpimages_vsv/wpc625edec_06.jpg') no-repeat fixed center top / 100% 100% transparent;">

    <div id="divMain"
         style="background:transparent;margin-left:auto;margin-right:auto;position:relative;width:1250px;height:980px;">

        <img alt="" src="wpimages_vsv/wp3f83823b_06.png"
             style="position:absolute;left:390px;top:79px;width:492px;height:153px;">

        <img alt="" src="wpimages_vsv/wp5efeea30_06.png"
             style="position:absolute;left:344px;top:283px;width:584px;height:80px;">

        <div class="OBJ-1" style="position:absolute;left:246px;top:283px;width:779px;height:100px;">

            <a href="http://www.randawilly.ovh/vivresaville/accueil"
               title="Découvrez les sites des villes partenaires rassemblant les informations institutionnelles et privées (annuaire complet, articles, agenda, bons, plans, conditions de fidélisation, boutiques en ligne) déposées par les acteurs administratifs, associatifs et économiques de la cité."
               id="nav_112_B1" class="OBJ-2 ActiveButton Down OBJ-3"
               style="display:block;position:absolute;left:54px;top:5px;width:670px;height:70px;">

                <span>Choisissez&nbsp;votre&nbsp;département</span>

            </a>

        </div>

        <div style="position:absolute;left:76px;top:384px;width:1144px;height:268px;overflow:hidden;">

            <p class="Corps P-1"><br></p>

            <p class="Corps P-2">DÉCOUVREZ LES SITES FÉDÉRATEURS</p>

            <p class="Corps P-3">« vivre<span class="C-1">sa</span>ville.fr »</p>

            <p class="Corps P-2">CRÉÉS AVEC LES COLLECTIVITÉS</p>

            <p class="Corps P-2">TERRITORIALES PARTENAIRES !…</p>

            <p class="Normal P-4"><br></p>

        </div>

        <img alt="" src="wpimages_vsv/wpa9e4b551_06.png"
             style="position:absolute;left:173px;top:686px;width:925px;height:182px;">

        <div id="nav_112_B1M"
             style="position:absolute;visibility:hidden;width:540px;height:80px;background:transparent url('wpimages_vsv/wp55f00916_06.png') no-repeat scroll left top;">

            <a href="http://www.randawilly.ovh/vivresaville/accueil" id="nav_112_B1M_L1"
               class="OBJ-4 ActiveButton OBJ-5"
               style="display:block;position:absolute;left:20px;top:10px;width:500px;height:60px;">

                <span>Alpes-Maritimes&nbsp;&amp;&nbsp;Monaco</span>

            </a>

        </div>

    </div>

    <script type="text/javascript" src="wpscripts_vsv/jsMenu.js"></script>

    <script type="text/javascript">

        wpmenustack.setRollovers([['nav_112_B1', 'nav_112_B1M', {"m_vertical": true}]]);

        wpmenustack.setMenus(['nav_112_B1M'], {"m_vOffset": 10, "m_vAlignment": 1});

    </script>

    </body>

    </html>


    <?php
}

else
{
    ?>
    <!DOCTYPE html>

    <html lang="fr">

    <head>

        <meta charset="UTF-8">

        <title>Le Magazine Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco</title>

        <meta name="generator" content="Serif WebPlus X8 (16,0,4,32)">

        <meta name="viewport" content="width=1250">

        <meta name="keywords" content="animations, spectacles, concerts, théâtres, salons, musées, expositions, alpes-maritimes, nice, antibes, monaco, cannes, Théâtres, concerts, spectacles, animations, magazine, sortez, sortir, cannes, alpes-maritimes">

        <meta name="description" content="Sur les Alpes-Maritimes et Monaco, découvrez l'essentiel des sorties et des loisirs et bien plus encore !... animations, spectacles, concerts, théâtres, salons, musées, expositions...&#xD;&#xA;">

        <meta name="author" content="Priviconcept">

        <meta name="robots" content="index,follow">

        <link rel="stylesheet" type="text/css" href="wpscripts/wpstyles.css">

        <style type="text/css">

            .OBJ-1 { background:transparent url('http://www.sortez.org/wpimages/wpf8daa116_06.png') no-repeat 0px 0px; }

            .OBJ-2,.OBJ-2:link,.OBJ-2:visited { background-image:url('http://www.sortez.org/wpimages/wp855f4952_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }

            .OBJ-2:hover { background-position:0px -70px; }

            .OBJ-2:active,a:link.OBJ-2.Activated,a:link.OBJ-2.Down,a:visited.OBJ-2.Activated,a:visited.OBJ-2.Down,.OBJ-2.Activated,.OBJ-2.Down { background-position:0px -35px; }

            .OBJ-2:focus { outline-style:none; }

            button.OBJ-2 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }

            button.OBJ-2:disabled { pointer-events:none; }

            .OBJ-2.Inline { display:inline-block;position:relative;line-height:normal; }

            .OBJ-2 span,.OBJ-2:link span,.OBJ-2:visited span { color:#ffffff;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:8px;top:8px;width:154px;height:17px;line-height:17px;font-size:13px;display:block;position:absolute;cursor:pointer; }

            .OBJ-3 { line-height:35px; }

            .OBJ-4,.OBJ-4:link,.OBJ-4:visited { background-image:url('http://www.sortez.org/wpimages/wp855f4952_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }

            .OBJ-4:hover { background-position:0px -70px; }

            .OBJ-4:active,a:link.OBJ-4.Activated,a:link.OBJ-4.Down,a:visited.OBJ-4.Activated,a:visited.OBJ-4.Down,.OBJ-4.Activated,.OBJ-4.Down { background-position:0px -35px; }

            .OBJ-4:focus { outline-style:none; }

            button.OBJ-4 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }

            button.OBJ-4:disabled { pointer-events:none; }

            .OBJ-4.Inline { display:inline-block;position:relative;line-height:normal; }

            .OBJ-4 span,.OBJ-4:link span,.OBJ-4:visited span { color:#ffffff;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:8px;top:8px;width:155px;height:17px;line-height:17px;font-size:13px;display:block;position:absolute;cursor:pointer; }

            .OBJ-5,.OBJ-5:link,.OBJ-5:visited { background-image:url('http://www.sortez.org/wpimages/wpb55e4830_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }

            .OBJ-5:hover { background-position:0px -70px; }

            .OBJ-5:active,a:link.OBJ-5.Activated,a:link.OBJ-5.Down,a:visited.OBJ-5.Activated,a:visited.OBJ-5.Down,.OBJ-5.Activated,.OBJ-5.Down { background-position:0px -35px; }

            .OBJ-5:focus { outline-style:none; }

            button.OBJ-5 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }

            button.OBJ-5:disabled { pointer-events:none; }

            .OBJ-5.Inline { display:inline-block;position:relative;line-height:normal; }

            .OBJ-5 span,.OBJ-5:link span,.OBJ-5:visited span { color:#ffffff;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:8px;top:8px;width:155px;height:17px;line-height:17px;font-size:13px;display:block;position:absolute;cursor:pointer; }

            .OBJ-6,.OBJ-6:link,.OBJ-6:visited { background-image:url('http://www.sortez.org/wpimages/wpfe3c87b8_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }

            .OBJ-6:hover { background-position:0px -70px; }

            .OBJ-6:active,a:link.OBJ-6.Activated,a:link.OBJ-6.Down,a:visited.OBJ-6.Activated,a:visited.OBJ-6.Down,.OBJ-6.Activated,.OBJ-6.Down { background-position:0px -35px; }

            .OBJ-6:focus { outline-style:none; }

            button.OBJ-6 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }

            button.OBJ-6:disabled { pointer-events:none; }

            .OBJ-6.Inline { display:inline-block;position:relative;line-height:normal; }

            .OBJ-6 span,.OBJ-6:link span,.OBJ-6:visited span { color:#ffffff;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:8px;top:8px;width:156px;height:17px;line-height:17px;font-size:13px;display:block;position:absolute;cursor:pointer; }

            .OBJ-7 { background:transparent url('http://www.sortez.org/wpimages/wp0d30096e_06.png') no-repeat 0px 0px; }

            .OBJ-8,.OBJ-8:link,.OBJ-8:visited { background-image:url('http://www.sortez.org/wpimages/wp980d9a8d_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }

            .OBJ-8:hover { background-position:0px -118px; }

            .OBJ-8:active,a:link.OBJ-8.Activated,a:link.OBJ-8.Down,a:visited.OBJ-8.Activated,a:visited.OBJ-8.Down,.OBJ-8.Activated,.OBJ-8.Down { background-position:0px -59px; }

            .OBJ-8:focus { outline-style:none; }

            button.OBJ-8 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }

            button.OBJ-8:disabled { pointer-events:none; }

            .OBJ-8.Inline { display:inline-block;position:relative;line-height:normal; }

            .OBJ-8 span,.OBJ-8:link span,.OBJ-8:visited span { color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:11px;top:19px;width:376px;height:17px;line-height:17px;font-size:13px;display:block;position:absolute;cursor:pointer; }

            .OBJ-9 { line-height:59px; }

            .OBJ-10,.OBJ-10:link,.OBJ-10:visited { background-image:url('http://www.sortez.org/wpimages/wp7ca545dc_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }

            .OBJ-10:hover { background-position:0px -118px; }

            .OBJ-10:active,a:link.OBJ-10.Activated,a:link.OBJ-10.Down,a:visited.OBJ-10.Activated,a:visited.OBJ-10.Down,.OBJ-10.Activated,.OBJ-10.Down { background-position:0px -59px; }

            .OBJ-10:focus { outline-style:none; }

            button.OBJ-10 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }

            button.OBJ-10:disabled { pointer-events:none; }

            .OBJ-10.Inline { display:inline-block;position:relative;line-height:normal; }

            .OBJ-10 span,.OBJ-10:link span,.OBJ-10:visited span { color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:11px;top:19px;width:152px;height:17px;line-height:17px;font-size:13px;display:block;position:absolute;cursor:pointer; }

            .OBJ-11,.OBJ-11:link,.OBJ-11:visited { background-image:url('http://www.sortez.org/wpimages/wpbe280e0f_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }

            .OBJ-11:hover { background-position:0px -118px; }

            .OBJ-11:active,a:link.OBJ-11.Activated,a:link.OBJ-11.Down,a:visited.OBJ-11.Activated,a:visited.OBJ-11.Down,.OBJ-11.Activated,.OBJ-11.Down { background-position:0px -59px; }

            .OBJ-11:focus { outline-style:none; }

            button.OBJ-11 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }

            button.OBJ-11:disabled { pointer-events:none; }

            .OBJ-11.Inline { display:inline-block;position:relative;line-height:normal; }

            .OBJ-11 span,.OBJ-11:link span,.OBJ-11:visited span { color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:11px;top:19px;width:154px;height:17px;line-height:17px;font-size:13px;display:block;position:absolute;cursor:pointer; }

            .OBJ-12,.OBJ-12:link,.OBJ-12:visited { background-image:url('http://www.sortez.org/wpimages/wp9a697053_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }

            .OBJ-12:hover { background-position:0px -118px; }

            .OBJ-12:active,a:link.OBJ-12.Activated,a:link.OBJ-12.Down,a:visited.OBJ-12.Activated,a:visited.OBJ-12.Down,.OBJ-12.Activated,.OBJ-12.Down { background-position:0px -59px; }

            .OBJ-12:focus { outline-style:none; }

            button.OBJ-12 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }

            button.OBJ-12:disabled { pointer-events:none; }

            .OBJ-12.Inline { display:inline-block;position:relative;line-height:normal; }

            .OBJ-12 span,.OBJ-12:link span,.OBJ-12:visited span { color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:11px;top:19px;width:115px;height:17px;line-height:17px;font-size:13px;display:block;position:absolute;cursor:pointer; }

            @font-face { font-family: 'Futura Md'; src: url('wpscripts/wp545f5f8e.ttf'); }

            .P-1 { text-align:center;line-height:24.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .P-2 { text-align:center;line-height:22.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }

            .P-3 { text-align:center;margin-bottom:4.0px;line-height:26.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:23.0px;vertical-align:0; }

            @font-face { font-family: 'Futura Hv'; src: url('wpscripts/wpe3dd8930.ttf'); }

            .P-4 { text-align:center;line-height:42.7px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:35.0px;vertical-align:0; }

            .P-5 { text-align:center;margin-bottom:0.0px;line-height:42.7px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:35.0px;vertical-align:0; }

            .P-6 { text-align:center;margin-bottom:0.0px;line-height:28.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .P-7 { line-height:16.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:24.0px;vertical-align:0; }

            .P-8 { text-align:center;line-height:28.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .P-9 { text-align:center;margin-bottom:0.0px;line-height:42.7px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:35.0px;vertical-align:0; }

            .P-10 { text-align:center;margin-bottom:2.7px;line-height:42.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .P-11 { text-align:center;margin-bottom:0.0px;line-height:1px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:35.0px;vertical-align:0; }

            .C-1 { line-height:46.00px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;text-decoration:none;font-variant:normal;font-size:34.7px;vertical-align:0; }

            .P-12 { text-align:center;margin-bottom:0.0px;line-height:24.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .P-13 { text-align:center;margin-bottom:10.7px;line-height:26.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .P-14 { text-align:center;margin-bottom:5.3px;line-height:26.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .P-15 { text-align:center;margin-bottom:0.0px;line-height:26.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .P-16 { text-align:center;margin-bottom:13.3px;line-height:1px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .C-2 { line-height:28.00px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;text-decoration:none;font-variant:normal;font-size:21.3px;vertical-align:0; }

            .P-17 { text-align:center;margin-bottom:13.3px;line-height:21.3px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:21.0px;vertical-align:0; }

            .OBJ-13 { border:none;background:transparent; }

        </style>

        <script type="text/javascript" src="wpscripts/jquery.js"></script>

        <script type="text/javascript" src="wpscripts/jquery.wputils.js"></script>

        <script type="text/javascript" src="wpscripts/jquery.wplightbox.js"></script>

        <script type="text/javascript">

            wpRedirectMinScreen(500,900,'http://www.sortez.org/site/menu-mobile.html',0);

            $(document).ready(function() {

                $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

                $('.wplightbox').wplightbox(

                    {"loadBtnSrc":"http://www.sortez.org/wpimages/lightbox_load.gif","playBtnSrc":"http://www.sortez.org/wpimages/lightbox_play.png","playOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_play_over.png","pauseBtnSrc":"http://www.sortez.org/wpimages/lightbox_pause.png","pauseOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_pause_over.png","border_e":"http://www.sortez.org/wpimages/lightbox_e_6.png","border_n":"http://www.sortez.org/wpimages/lightbox_n_6.png","border_w":"http://www.sortez.org/wpimages/lightbox_w_6.png","border_s":"http://www.sortez.org/wpimages/lightbox_s_6.png","border_ne":"http://www.sortez.org/wpimages/lightbox_ne_6.png","border_se":"http://www.sortez.org/wpimages/lightbox_se_6.png","border_nw":"http://www.sortez.org/wpimages/lightbox_nw_6.png","border_sw":"http://www.sortez.org/wpimages/lightbox_sw_6.png","closeBtnSrc":"http://www.sortez.org/wpimages/lightbox_close_2.png","closeOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_close_over_2.png","nextBtnSrc":"http://www.sortez.org/wpimages/lightbox_next_2.png","nextOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_next_over_2.png","prevBtnSrc":"http://www.sortez.org/wpimages/lightbox_prev_2.png","prevOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_prev_over_2.png","blankSrc":"wpscripts/blank.gif","bBkgrndClickable":true,"strBkgrndCol":"#000000","nBkgrndOpacity":0.5,"strContentCol":"#ffffff","nContentOpacity":0.8,"strCaptionCol":"#555555","nCaptionOpacity":1.0,"nCaptionType":1,"bCaptionCount":true,"strCaptionFontType":"Tahoma,Serif","strCaptionFontCol":"#ffffff","nCaptionFontSz":15,"bShowPlay":true,"bAnimateOpenClose":true,"nPlayPeriod":2000}

                );

            });

        </script>

        <link rel="icon" href="favicon.ico" type="image/x-icon" sizes="16x16 32x32 48x48 64x64">

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" sizes="16x16 32x32 48x48 64x64">

        <link rel="apple-touch-icon" href="favicon.ico" sizes="60x60 76x76 120x120 152x152">

    </head>

    <body style="height:5500px;background:url('http://www.sortez.org/wpimages/wp313df1f0_06.jpg') no-repeat fixed center top / cover transparent;">
        
    <div id="divMain" style="background:transparent;margin-left:auto;margin-right:auto;position:relative;width:1250px;height:5500px;">

        <img src="http://www.sortez.org/wpimages/wpe375cfed_06.png" alt="" width="1250" height="330" style="position:absolute;left:0px;top:67px;width:1250px;height:330px;">

        <div class="OBJ-1" style="position:absolute;left:10px;top:356px;width:1220px;height:41px;">

            <a href="index.html" id="nav_119_B1" class="OBJ-2 ActiveButton Down OBJ-3" style="display:block;position:absolute;left:0px;top:0px;width:168px;height:35px;">

                <span>Accueil</span>

            </a>

            <a href="http://www.randevprojectpriviconcept.ovh/front/annuaire" id="nav_119_B2" class="OBJ-2 ActiveButton OBJ-3" style="display:block;position:absolute;left:175px;top:0px;width:168px;height:35px;">

                <span>L&#39;annuaire</span>

            </a>

            <a href="http://www.randevprojectpriviconcept.ovh/article" id="nav_119_B3" class="OBJ-2 ActiveButton OBJ-3" style="display:block;position:absolute;left:349px;top:0px;width:168px;height:35px;">

                <span>L&#39;actualité</span>

            </a>

            <a href="http://www.randevprojectpriviconcept.ovh/agenda" id="nav_119_B4" class="OBJ-2 ActiveButton OBJ-3" style="display:block;position:absolute;left:524px;top:0px;width:168px;height:35px;">

                <span>L&#39;agenda</span>

            </a>

            <a href="http://www.randevprojectpriviconcept.ovh/front/bonplan" id="nav_119_B5" class="OBJ-4 ActiveButton OBJ-3" style="display:block;position:absolute;left:699px;top:0px;width:168px;height:35px;">

                <span>&nbsp;Les&nbsp;bons&nbsp;plans</span>

            </a>

            <a href="http://www.randevprojectpriviconcept.ovh/front/fidelity" id="nav_119_B6" class="OBJ-5 ActiveButton OBJ-3" style="display:block;position:absolute;left:875px;top:0px;width:169px;height:35px;">

                <span>La&nbsp;fidélité</span>

            </a>

            <a href="http://www.randevprojectpriviconcept.ovh/front/annonce" id="nav_119_B7" class="OBJ-6 ActiveButton OBJ-3" style="display:block;position:absolute;left:1050px;top:0px;width:170px;height:35px;">

                <span>Les&nbsp;boutiques</span>

            </a>

        </div>

        <map id="map1" name="map1">

            <area shape="rect" coords="1180,8,1234,61" data-lightbox="{&quot;galleryId&quot;:&quot;wplightbox&quot;,&quot;width&quot;:600,&quot;height&quot;:1000}" class="wplightbox" href="http://sortez.org/portail/front/professionnels/TwitterPrivicarteForm" alt="">

            <area shape="rect" coords="1123,8,1178,61" data-lightbox="{&quot;galleryId&quot;:&quot;wplightbox&quot;,&quot;width&quot;:600,&quot;height&quot;:1000}" class="wplightbox" href="http://sortez.org/portail/front/professionnels/FacebookPrivicarteForm" alt="">

            <area shape="poly" coords="1216,29,1216,27,1215,26,1216,25,1218,25,1218,26,1219,27,1218,27">

            <area shape="poly" coords="1211,46,1216,44,1219,38,1222,29,1224,30,1225,27,1221,23,1215,19,1211,25,1208,27,1203,23,1200,20,1200,24,1199,23,1202,28,1204,31,1199,35,1193,36,1188,33,1189,36,1194,42,1198,45,1202,47,1210,47">

            <area shape="poly" coords="1218,27,1218,25,1216,25,1215,26,1216,27,1216,28,1218,28">

            <area shape="poly" coords="1201,23,1200,22,1201,22">

            <area shape="poly" coords="1210,46,1216,42,1220,36,1223,30,1225,28,1220,23,1215,20,1211,25,1209,28,1204,23,1200,21,1199,24,1203,30,1205,31,1200,35,1197,36,1190,34,1188,33,1192,39,1197,44,1202,47,1210,47">

            <area shape="poly" coords="1167,9,1169,8,1171,7,1173,6,1175,5,1176,3,1178,2,1179,0,1140,0,1141,2,1145,6,1146,7,1148,8,1150,8,1152,9,1153,10,1166,10">

            <area shape="poly" coords="1153,47,1153,37,1152,39,1151,41,1149,42,1148,44,1147,46,1146,48,1146,49,1153,49">

            <area shape="poly" coords="1147,44,1149,41,1151,38,1153,34,1153,32,1158,32,1158,27,1153,27,1153,24,1158,24,1159,25,1159,19,1149,19,1146,22,1146,27,1143,30,1146,32,1146,45">

            <area shape="poly" coords="1153,35,1153,31,1158,31,1158,27,1153,27,1154,23,1158,24,1159,24,1159,19,1150,19,1147,22,1146,26,1143,27,1143,31,1146,31,1146,46">

            <area shape="poly" coords="1153,44,1153,37,1146,49,1153,49">

            <area shape="poly" coords="1158,56,1167,51,1173,43,1175,33,1172,23,1166,15,1160,11,1156,10,1147,11,1138,15,1131,22,1128,32,1129,42,1134,50,1142,55,1146,57,1157,57">

            <area shape="poly" coords="39,56,48,51,54,43,55,35,53,26,49,18,44,13,36,10,25,10,16,15,10,21,7,31,8,41,13,49,21,57,39,57" href="javascript:history.back()" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wpb018a763_06.png" alt="" width="1250" height="67" usemap="#map1" style="position:absolute;left:0px;top:0px;width:1250px;height:67px;">

        <div class="OBJ-7" style="position:absolute;left:223px;top:8px;width:895px;height:59px;">

            <a data-lightbox="{&quot;galleryId&quot;:&quot;wplightbox&quot;,&quot;width&quot;:650,&quot;height&quot;:900}" class="wplightbox OBJ-8 ActiveButton OBJ-9" href="annonce-infos.html" id="nav_124_B1" style="display:block;position:absolute;left:0px;top:0px;width:395px;height:59px;">

                <span>informations&nbsp;et&nbsp;souscriptions</span>

            </a>

            <a href="http://www.randevprojectpriviconcept.ovh/auth/login" target="_blank" id="nav_124_B2" class="OBJ-10 ActiveButton OBJ-9" style="display:block;position:absolute;left:402px;top:0px;width:171px;height:59px;">

                <span>Mon&nbsp;compte</span>

            </a>

            <a href="http://www.randevprojectpriviconcept.ovh//front/utilisateur/liste_favoris" target="_blank" id="nav_124_B3" class="OBJ-11 ActiveButton OBJ-9" style="display:block;position:absolute;left:581px;top:0px;width:173px;height:59px;">

                <span>Mes&nbsp;favoris</span>

            </a>

            <a data-lightbox="{&quot;galleryId&quot;:&quot;wplightbox&quot;,&quot;width&quot;:600,&quot;height&quot;:500}" class="wplightbox OBJ-12 ActiveButton OBJ-9" href="http://sortez.org/contact" id="nav_124_B4" style="display:block;position:absolute;left:761px;top:0px;width:134px;height:59px;">

                <span>Contact</span>

            </a>

        </div>

        <img alt="" src="http://www.sortez.org/wpimages/wp509b7ae3_05_06.jpg" style="position:absolute;left:991px;top:69px;width:229px;height:265px;">

        <img alt="" src="http://www.sortez.org/wpimages/wp125709c3_06.png" style="position:absolute;left:978px;top:170px;width:256px;height:168px;">

        <img alt="" src="http://www.sortez.org/wpimages/wp7febcb75_06.png" style="position:absolute;left:0px;top:68px;width:476px;height:275px;">

        <img alt="Découvrez l’essentiel de l’information départementale déposé en ligne par les acteurs institutionnels et privés !…" src="http://www.sortez.org/wpimages/wp0252a740_06.png" style="position:absolute;left:363px;top:216px;width:525px;height:50px;">

        <img alt="" src="http://www.sortez.org/wpimages/wp09647b79_06.png" style="position:absolute;left:509px;top:94px;width:277px;height:26px;">

        <img alt="Associations, artisans, collectivités, commerçants, professionnels…." src="http://www.sortez.org/wpimages/wp2a909ad0_06.png" style="position:absolute;left:238px;top:288px;width:740px;height:26px;">

        <div style="position:absolute;left:68px;top:8px;width:180px;height:30px;">

            <div id="google_translate_element" style="padding-left: 15px; padding-top: 13px"></div><script type="text/javascript">

                function googleTranslateElementInit() {

                    new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false, multilanguagePage: true}, 'google_translate_element');

                }

            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

        </div>

        <img alt="" src="http://www.sortez.org/wpimages/wp7b874d64_06.png" style="position:absolute;left:426px;top:90px;width:398px;height:115px;">

        <img alt="" src="http://www.sortez.org/wpimages/wp1c465b25_06.png" style="position:absolute;left:0px;top:397px;width:1250px;height:5103px;">

        <img alt="" src="http://www.sortez.org/wpimages/wp93968b1e_05_06.jpg" style="position:absolute;left:30px;top:3837px;width:1190px;height:300px;">

        <div style="position:absolute;left:30px;top:1115px;width:1190px;height:169px;overflow:hidden;">

            <p class="Normal P-1">Les articles et l’édition du mois de décembre 2017 en PDF sont disponibles. Vous y trouverez plus de 360 informations</p>

            <p class="Normal P-1">sur les loisirs de ce département (animations, spectacles, concerts, théâtres…).</p>

            <p class="Normal P-1"><br></p>

            <p class="Normal P-1">Le magazine papier est diffusé sur les principaux axes des communes du littoral</p>

            <p class="Normal2 P-1">et disponible auprès de 500 dépositaires certains commerces locaux, </p>

            <p class="Normal P-1">les offices de tourisme, les mairies et adresses culturelles…</p>

            <p class="Normal P-2"><br></p>

            <p class="Article P-3"><br></p>

        </div>

        <img src="http://www.sortez.org/wpimages/wpacb03430_06.png" alt="" width="1250" height="2126" style="position:absolute;left:0px;top:1449px;width:1250px;height:2126px;">

        <div style="position:absolute;left:20px;top:439px;width:1200px;height:151px;overflow:hidden;">

            <p class="Normal P-4">ALPES-<wbr> MARITIMES &amp; MONACO</p>

            <p class="Normal P-4">DÉCOUVREZ L’ESSENTIEL DE L’ACTUALITÉ DU MOIS DE DÉCEMBRE</p>

            <p class="Normal P-4">ET BIEN PLUS ENCORE !…</p>

        </div>

        <map id="map2" name="map2">

            <area shape="rect" coords="95,11,207,37" href="http://www.randevprojectpriviconcept.ovh/article" alt="">

            <area shape="rect" coords="0,0,301,48" href="http://www.randevprojectpriviconcept.ovh/article" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wpc3ae5b89_06.png" alt="" width="300" height="47" usemap="#map2" style="position:absolute;left:111px;top:1029px;width:300px;height:47px;">

        <map id="map3" name="map3">

            <area shape="rect" coords="33,11,268,37" href="http://www.randevprojectpriviconcept.ovh/site/edition-mois.html" target="_blank" alt="">

            <area shape="rect" coords="0,0,301,48" href="http://www.randevprojectpriviconcept.ovh/site/edition-mois.html" target="_blank" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wp239e430e_06.png" alt="" width="300" height="47" usemap="#map3" style="position:absolute;left:475px;top:1029px;width:300px;height:47px;">

        <map id="map4" name="map4">

            <area shape="rect" coords="29,11,272,37" href="http://sortez.org/agenda" target="_blank" alt="">

            <area shape="rect" coords="0,0,301,48" href="http://sortez.org/agenda" target="_blank" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wpd49d8ccf_06.png" alt="" width="300" height="47" usemap="#map4" style="position:absolute;left:839px;top:1029px;width:300px;height:47px;">

        <map id="map5" name="map5">

            <area shape="rect" coords="84,11,218,37" href="http://sortez.org/front/annonce" target="_blank" alt="">

            <area shape="rect" coords="0,0,301,48" href="http://sortez.org/front/annonce" target="_blank" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wp85e5a739_06.png" alt="" width="300" height="47" usemap="#map5" style="position:absolute;left:782px;top:2074px;width:300px;height:47px;">

        <map id="map6" name="map6">

            <area shape="rect" coords="99,11,203,37" href="http://sortez.org/front/annuaire" target="_blank" alt="">

            <area shape="rect" coords="0,0,301,48" href="http://sortez.org/front/annuaire" target="_blank" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wpc6e82907_06.png" alt="" width="300" height="47" usemap="#map6" style="position:absolute;left:171px;top:2074px;width:300px;height:47px;">

        <img alt="" src="http://www.sortez.org/wpimages/wp35e9dcb3_06.png" style="position:absolute;left:30px;top:3067px;width:1190px;height:300px;">

        <div style="position:absolute;left:30px;top:2259px;width:1190px;height:128px;overflow:hidden;">

            <p class="Corps4 P-5">DÉCOUVREZ LES ACTIONS PROMOTIONNELLES</p>

            <p class="Corps4 P-5">DÉPOSÉES PAR NOS PARTENAIRES !…</p>

        </div>

        <img alt="" src="http://www.sortez.org/wpimages/wpaaadb316_05_06.jpg" style="position:absolute;left:33px;top:2376px;width:580px;height:299px;">

        <img alt="" src="http://www.sortez.org/wpimages/wpf96c5e23_05_06.jpg" style="position:absolute;left:639px;top:2375px;width:580px;height:299px;">

        <div style="position:absolute;left:30px;top:2704px;width:586px;height:129px;overflow:hidden;">

            <p class="Corps P-6">Découvrez des offres uniques, exceptionnelles,</p>

            <p class="Corps P-6">motivantes pour le consommateur….</p>

            <p class="Corps-artistique2 P-7"><br></p>

        </div>

        <div style="position:absolute;left:638px;top:2704px;width:582px;height:119px;overflow:hidden;">

            <p class="Corps-artistique2 P-8">Découvrez des offres de fidélisation : des remises Cash,</p>

            <p class="Corps-artistique2 P-8">des offres de capitalisation ou coups de tampons…</p>

        </div>

        <map id="map7" name="map7">

            <area shape="rect" coords="55,11,197,37" href="http://www.randevprojectpriviconcept.ovh/front/fidelity" target="_blank" alt="">

            <area shape="rect" coords="0,0,251,48" href="http://www.randevprojectpriviconcept.ovh/front/fidelity" target="_blank" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wp210b1933_06.png" alt="" width="250" height="47" usemap="#map7" style="position:absolute;left:804px;top:2803px;width:250px;height:47px;">

        <map id="map8" name="map8">

            <area shape="rect" coords="53,13,199,39" href="http://www.randevprojectpriviconcept.ovh/portail.php" target="_blank" alt="">

            <area shape="rect" coords="0,0,251,48" href="http://www.randevprojectpriviconcept.ovh/portail.php" target="_blank" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wpd00cc49d_06.png" alt="" width="250" height="47" usemap="#map8" style="position:absolute;left:198px;top:2803px;width:250px;height:47px;">

        <div style="position:absolute;left:30px;top:2949px;width:1190px;height:128px;overflow:hidden;">

            <p class="Corps4 P-5">POUR EN BÉNÉFICIER, IL VOUS SUFFIT DE DEMANDER</p>

            <p class="Corps4 P-5">GRATUITEMENT NOTRE CARTE PRIVILÈGE VIVRESAVILLE !…</p>

        </div>

        <img alt="" src="http://www.sortez.org/wpimages/wp5c0e08c7_05_06.jpg" style="position:absolute;left:636px;top:1633px;width:585px;height:300px;">

        <img alt="" src="http://www.sortez.org/wpimages/wped819cd8_05_06.jpg" style="position:absolute;left:31px;top:1634px;width:585px;height:300px;">

        <div style="position:absolute;left:30px;top:1512px;width:1190px;height:115px;overflow:hidden;">

            <p class="Corps4 P-9">DÉCOUVREZ LES BONNES ADRESSES &amp;</p>

            <p class="Corps4 P-9">LES BOUTIQUES EN LIGNE DE NOS PARTENAIRES…</p>

        </div>

        <map id="map9" name="map9">

            <area shape="rect" coords="70,11,270,37" href="infos-consommateurs.html" alt="">

            <area shape="rect" coords="0,0,336,48" href="infos-consommateurs.html" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wpb04560a8_06.png" alt="" width="335" height="47" usemap="#map9" style="position:absolute;left:458px;top:3455px;width:335px;height:47px;">

        <map id="map10" name="map10">

            <area shape="rect" coords="74,11,274,37" href="infos-pro.html" alt="">

            <area shape="rect" coords="0,0,336,48" href="infos-pro.html" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wp3b3b3ed6_06.png" alt="" width="335" height="47" usemap="#map10" style="position:absolute;left:458px;top:4334px;width:335px;height:47px;">

        <map id="map11" name="map11">

            <area shape="rect" coords="121,94,254,136" href="http://www.randevprojectpriviconcept.ovh/front/particuliers/inscription" target="_blank" alt="">

            <area shape="poly" coords="267,139,275,133,276,129,276,96,270,89,267,88,101,88,93,94,91,99,91,131,97,139,100,140,267,140" href="http://www.randevprojectpriviconcept.ovh/front/particuliers/inscription" target="_blank" alt="">

            <area shape="rect" coords="87,26,281,80" href="http://www.randevprojectpriviconcept.ovh/front/particuliers/inscription" target="_blank" alt="">

            <area shape="poly" coords="340,159,348,151,348,25,341,18,340,17,19,17,12,24,11,30,11,151,17,158,20,160,339,160" href="http://www.randevprojectpriviconcept.ovh/front/particuliers/inscription" target="_blank" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wp1fac21ec_06.png" alt="" width="365" height="171" usemap="#map11" style="position:absolute;left:92px;top:3128px;width:365px;height:171px;">

        <div style="position:absolute;left:30px;top:3381px;width:1190px;height:63px;overflow:hidden;">

            <p class="Corps-artistique P-10">Dès validation, cette dernière, dématérialisée, apparaîtra sur votre compte personnel (ordinateur, tablette, mobile)…</p>

            <p class="Corps P-6"><br></p>

            <p class="Corps-artistique2 P-7"><br></p>

        </div>

        <div style="position:absolute;left:30px;top:3662px;width:1190px;height:175px;overflow:hidden;">

            <p class="Corps P-9">COLLECTIVITÉS, ASSOCIATIONS, PROFESSIONNELS,</p>

            <p class="Corps P-9">RÉFÉRENCEZ GRATUITEMENT VOTRE ÉTABLISSEMENT </p>

            <p class="Corps P-9">ET ACCÉDEZ À UNE NOUVELLE AUDIENCE DÉPARTEMENTALE !…</p>

            <p class="Corps P-11"><span class="C-1"><br></span></p>

            <p class="Corps P-11"><span class="C-1"><br></span></p>

        </div>

        <div style="position:absolute;left:30px;top:4156px;width:1190px;height:129px;overflow:hidden;">

            <p class="Corps22 P-12">Sur un compte personnel et sécurisé, &nbsp;vous bénéficiez d’un kit de démarrage clés en mains et gratuit</p>

            <p class="Corps22 P-13">qui vous donnera accès à des outils de gestion, de référencement et marketing innovants…</p>

            <p class="Corps22 P-12">Il vous suffira de créer ou modifier d’un simple clic vos données (textes, images, liens, vidéo…)</p>

            <p class="Corps22 P-14">dans des champs préconfigurés sans faire appel à un webmaster…</p>

            <p class="Corps22 P-15"><br></p>

            <p class="Normal P-16"><span class="C-2"><br></span></p>

            <p class="Normal322 P-17"><br></p>

        </div>

        <map id="map12" name="map12">

            <area shape="rect" coords="122,94,240,136" href="http://www.randevprojectpriviconcept.ovh/front/professionnels/inscription" target="_blank" alt="">

            <area shape="poly" coords="263,139,271,133,272,129,272,96,266,89,263,88,96,88,88,94,87,99,87,132,93,139,96,140,263,140" href="http://www.randevprojectpriviconcept.ovh/front/professionnels/inscription" target="_blank" alt="">

            <area shape="rect" coords="61,25,299,79" href="http://www.randevprojectpriviconcept.ovh/front/professionnels/inscription" target="_blank" alt="">

            <area shape="poly" coords="340,159,348,151,348,25,341,18,340,17,19,17,12,24,11,30,11,151,17,158,20,160,339,160" href="http://www.randevprojectpriviconcept.ovh/front/professionnels/inscription" target="_blank" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wp54ed0719_06.png" alt="" width="365" height="171" usemap="#map12" style="position:absolute;left:68px;top:3898px;width:365px;height:171px;">

        <iframe id="ifrm_3" name="ifrm_3" src="http://www.sortez.org/bannieres/pub-sortez/index.html" class="OBJ-13" style="position:absolute;left:30px;top:1311px;width:1190px;height:150px;"></iframe>

        <img alt="" src="http://www.sortez.org/wpimages/wp8d959582_06.png" style="position:absolute;left:0px;top:597px;width:1250px;height:400px;">

        <div style="position:absolute;left:637px;top:1949px;width:584px;height:110px;overflow:hidden;">

            <p class="Corps P-15">Découvrez les boutiques en ligne de nos partenaires… </p>

            <p class="Corps P-15">Certaines possèdent</p>

            <p class="Corps P-15">un module de règlement en ligne avec PAYPAL</p>

        </div>

        <img alt="" src="http://www.sortez.org/wpimages/wpb9fae35f_06.png" style="position:absolute;left:657px;top:1738px;width:193px;height:161px;">

        <div style="position:absolute;left:29px;top:1949px;width:584px;height:110px;overflow:hidden;">

            <p class="Corps P-15">Vous avez également accès à un annuaire qui référence</p>

            <p class="Corps P-15">plus de 1 000 adresses, administrations, commerçants,</p>

            <p class="Corps P-15">artisans, professionnels, associations, patrimoine...</p>

        </div>

        <map id="map13" name="map13">

            <area shape="rect" coords="412,897,658,945" href="http://www.randevprojectpriviconcept.ovh/site/mentions-legales.html" target="_blank" alt="">

            <area shape="rect" coords="477,907,651,933">

            <area shape="poly" coords="445,942,454,938,461,931,464,921,463,911,457,903,450,897,447,896,435,896,426,901,419,908,417,918,418,928,423,936,432,941,436,943">

            <area shape="poly" coords="447,932,447,929,444,929,444,912,434,912,434,917,437,917,437,929,434,929,434,934,447,934">

            <area shape="poly" coords="442,909,444,908,444,903,442,902,439,902,438,904,437,906,438,908,440,910">

            <area shape="poly" coords="445,942,454,938,461,931,464,921,463,911,457,903,449,897,446,896,435,896,426,901,419,908,417,918,418,928,424,936,432,942,436,943">

            <area shape="poly" coords="297,942,306,938,313,931,316,921,315,911,309,903,302,897,299,896,287,896,278,901,271,908,269,918,271,928,276,936,284,942,288,943" href="https://accounts.google.com/ServiceLogin?service=oz&amp;passive=1209600&amp;continue=https://plus.google.com/share?url%3Dhttp://privicarte.fr%26gpsrc%3Dgplp0&amp;btmpl=popup#identifier" target="_blank" alt="">

            <area shape="poly" coords="294,931,289,931,288,930,287,929,287,926,288,925,295,925,295,930">

            <area shape="poly" coords="287,925,286,924,287,924">

            <area shape="poly" coords="304,926,304,924,308,924,308,921,304,921,304,917,302,917,302,921,298,921,298,924,302,924,302,928,304,928">

            <area shape="poly" coords="292,916,289,916,289,915,288,914,288,908,289,907,291,907,292,908,292,909,293,910,293,913,292,914">

            <area shape="poly" coords="294,932,298,930,300,928,297,925,295,921,295,920,288,920,287,918,292,918,296,916,298,912,297,908,300,906,286,906,283,909,283,915,286,918,283,921,286,924,282,926,282,930,284,932,287,932,289,933,293,933">

            <area shape="poly" coords="304,924,308,924,308,921,304,921,304,917,302,917,302,921,298,921,298,924,302,924,302,928,304,928">

            <area shape="poly" coords="294,931,289,931,288,930,287,929,287,926,288,925,295,925,295,930">

            <area shape="poly" coords="291,916,289,916,288,915,288,908,289,907,292,907,292,910,293,911,293,914">

            <area shape="poly" coords="293,932,297,931,300,928,296,922,295,920,288,920,287,918,291,918,295,916,298,913,297,909,300,906,291,906,291,905,290,906,286,907,282,911,284,915,286,918,283,921,286,924,282,926,283,930,287,932,287,933,293,933">

            <area shape="poly" coords="297,942,306,938,313,931,316,921,315,911,309,903,301,897,298,896,287,896,278,901,272,908,269,918,271,928,276,936,284,942,288,943">

            <area shape="poly" coords="239,914,240,913,239,912,239,911,237,911,237,913,236,914,237,915,239,915" href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org" target="_blank" alt="">

            <area shape="rect" coords="221,907,223,909" href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org" target="_blank" alt="">

            <area shape="poly" coords="230,934,223,933,217,929,212,924,209,920,215,922,216,923,222,920,226,918,222,913,221,911,222,910,220,911,223,908,228,913,230,914,234,909,238,906,242,910,247,915,241,921,238,927,232,933" href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org" target="_blank" alt="">

            <area shape="poly" coords="235,941,244,936,250,928,252,918,249,908,242,901,232,896,222,897,213,902,207,909,204,918,206,928,211,936,218,941,221,942,235,942" href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org" target="_blank" alt="">

            <area shape="poly" coords="239,915,238,915,236,913,237,912,237,911,239,911,240,912,240,914">

            <area shape="rect" coords="221,910,223,912">

            <area shape="poly" coords="230,933,236,930,240,924,242,918,247,914,242,909,238,906,233,911,229,914,222,907,221,908,222,909,220,911,225,916,226,918,220,921,219,922,212,921,209,920,213,925,218,930,223,933,229,934">

            <area shape="poly" coords="239,914,240,913,239,912,239,911,237,911,237,915,239,915">

            <area shape="rect" coords="221,910,223,912">

            <area shape="poly" coords="230,933,236,929,240,923,243,917,247,914,242,909,236,907,232,912,229,914,223,908,220,910,224,916,226,918,220,922,212,922,209,920,213,925,218,930,222,933,225,934">

            <area shape="rect" coords="221,907,223,909">

            <area shape="poly" coords="186,895,195,891,202,884,204,874,203,864,197,856,190,851,186,850,175,850,166,855,160,862,157,872,159,882,165,890,173,895,176,896">

            <area shape="poly" coords="167,942,176,938,183,931,186,921,185,911,179,903,172,897,169,896,157,896,148,901,141,908,139,918,141,928,146,936,154,942,158,943" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//sortez.org" target="_blank" alt="">

            <area shape="poly" coords="164,933,164,923,163,925,161,926,160,928,159,930,158,932,157,934,164,934">

            <area shape="poly" coords="158,929,160,925,163,922,164,918,164,917,169,917,169,912,164,912,164,909,170,909,170,904,161,904,158,907,157,911,154,914,157,917,157,930">

            <area shape="poly" coords="164,920,164,917,169,917,169,912,164,912,164,909,170,909,170,904,161,904,158,907,157,911,154,912,154,917,157,917,157,931">

            <area shape="poly" coords="164,929,164,922,157,934,164,934">

            <area shape="poly" coords="167,942,176,938,183,931,186,921,185,911,179,903,171,897,168,896,157,896,148,901,142,908,139,918,140,928,146,936,154,942,158,943">

            <area shape="poly" coords="370,929,343,929,343,916,342,914,345,914,347,915,350,916,354,918,357,920,360,917,365,917,367,918,370,921" data-lightbox="{&quot;galleryId&quot;:&quot;wplightbox&quot;,&quot;width&quot;:500,&quot;height&quot;:600}" class="wplightbox" href="http://sortez.org/portail/contact" alt="">

            <area shape="poly" coords="362,942,371,938,378,931,381,921,380,911,374,903,367,897,364,896,352,896,343,901,336,908,334,918,336,928,341,936,349,941,353,943" data-lightbox="{&quot;galleryId&quot;:&quot;wplightbox&quot;,&quot;width&quot;:500,&quot;height&quot;:600}" class="wplightbox" href="http://sortez.org/portail/contact" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wp29d18e34_06.png" alt="" width="1250" height="1037" usemap="#map13" style="position:absolute;left:0px;top:4463px;width:1250px;height:1037px;">

        <div style="position:absolute;left:30px;top:4514px;width:1190px;height:162px;overflow:hidden;">

            <p class="Corps P-5">COLLECTIVITÉS, ASSOCIATIONS, PROFESSIONNELS</p>

            <p class="Corps P-5">NOUS AVONS LES OUTILS ET L’AUDIENCE NÉCESSAIRE</p>

            <p class="Corps P-5">POUR VOUS AIDER À ATTIRER DE NOUVEAUX CONTACTS !…</p>

        </div>

        <img alt="" src="http://www.sortez.org/wpimages/wp75c697d6_06.png" style="position:absolute;left:30px;top:4676px;width:1190px;height:300px;">

        <map id="map14" name="map14">

            <area shape="rect" coords="74,11,274,37" href="https://spark.adobe.com/page/WcFEg1TTyA4T7/" target="_blank" alt="">

            <area shape="rect" coords="0,0,336,48" href="https://spark.adobe.com/page/WcFEg1TTyA4T7/" target="_blank" alt="">

        </map>

        <img src="http://www.sortez.org/wpimages/wp780f2470_06.png" alt="" width="335" height="47" usemap="#map14" style="position:absolute;left:454px;top:5152px;width:335px;height:47px;">

        <img alt="" src="http://www.sortez.org/wpimages/wpf2456d40_06.png" style="position:absolute;left:278px;top:4702px;width:713px;height:414px;">

        <div style="position:absolute;left:1077px;top:5379px;width:173px;height:121px;">

            <div id="navigation_up_down_4949"><noscript>Javascript is disable - <a href="http://www.supportduweb.com/">http://www.supportduweb.com/</a> - <a href="http://www.supportduweb.com/generateur-boutons-navigation-haut-bas-scroll-defilement-racourcis-page-monter-descendre-up-down.html">Générateur de boutons de navigation</a></noscript><script type="text/javascript" src="http://services.supportduweb.com/navigation_up_down/1-4949-right.js"></script></div>

        </div>

        <div style="position:absolute;left:0px;top:5385px;width:269px;height:108px;">

            <script type="application/javascript" src="https://www.sellsy.com/?_f=snippet&hash=JUQ3VSUzRSU4RSslQTkuJThCJTk2MCUxMyVCNyVCRCUyNCU4OSU5Q0slNjAlRjYlREElMTAlRjYlMDMlRTglRjU0JUVEJTNGJTE0cVAlMEQlRDhXJUI1JUVGJTAyJTg4JTNGJUU4JTlGJTFBJUE5YSUxRSVERSUzQSVCOSU4MmIlQUQlRjBBJTExbyVFNmIlMkElQTUlRTZxJUREJTNFJTI0"></script>

        </div>

    </div>

    <script type="text/javascript">

        GetFrameUrl('ifrm_3');

    </script>

    </body>

    </html>


    <?php
}
?>