<?php

class mdlville extends CI_Model
{

    private $_table = "villes";

    function __construct()
    {
        parent::__construct();
    }

    function GetAutocompleteVille($queryString = 0)
    {
        $Sql = "select * from villes where Nom like '%$queryString%'  order by Nom ";
        $Query = $this->db->query($Sql);
        return $Query->result();
    }

    function getVilleByNomSimple($nom = "")
    {
        $Sql = "SELECT IdVille FROM villes WHERE NomSimple ='" . $nom . "' ";
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function GetAll()
    {
        $qryVilles = $this->db->query("
            SELECT
                IdVille,
                Nom,
                CodePostal
            FROM
                villes
            ORDER BY Nom ASC
        ");
        if ($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }

    function GetAllByIdCommercant()
    {
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $qryVilles = $this->db->query("
            SELECT
                NomSociete
            FROM
                commercants
            WHERE IdVille=" . $localdata_IdVille . "
        ");
        if ($qryVilles->num_rows() > 0) {
            return $qryVilles->result();
        }
    }

    function GetVilleByCodePostal_localisation($CodePostal = "0")
    {

        if ($CodePostal != "0") {
            $qry = "
                    SELECT
                        IdVille,
                        Nom,
                        CodePostal
                    FROM
                        villes
                    WHERE
                        villes.CodePostal LIKE '%" . $CodePostal . "%'
                 
                    ORDER BY Nom ASC
                    ";

            $qryVille = $this->db->query($qry);
            if ($qryVille->num_rows() > 0) {
                return $qryVille->result();
            }
        }
    }

    function GetVilleByCodePostal($CodePostal = "0", $departement_id = "0")
    {

        if (isset($CodePostal) && $CodePostal != "0" && $CodePostal != "") {
            $qry = "
                    SELECT
                        IdVille,
                        Nom,
                        CodePostal
                    FROM
                        villes
                    WHERE
                        villes.CodePostal LIKE '%" . $CodePostal . "%'

                        ";
            if (isset($departement_id) && $departement_id != "0" && $departement_id != "") $qry .= " AND villes.ville_departement = '" . $departement_id . "' ";

            $qry .= "
                    ORDER BY Nom ASC
                    ";

            $qryVille = $this->db->query($qry);
            if ($qryVille->num_rows() > 0) {
                return $qryVille->result();
            }
        }
    }

    function GetBonplanVilles()
    {

        $sqlcat = "
          SELECT
            villes.IdVille,
            villes.Nom,
            villes.CodePostal,
            villes.NomSimple,
            commercants.NomSociete as commercant,
			COUNT(IdCommercant) as nbCommercant
            FROM
            bonplan
            Inner Join commercants ON bonplan.bonplan_commercant_id = commercants.IdCommercant
            Inner Join villes ON villes.IdVille = commercants.IdVille
			where commercants.IsActif = 1 ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sqlcat .= " AND commercants.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            villes.IdVille, commercants.NomSociete ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetBonplanVilles_by_departement($departement_id = "0")
    {
        $sqlcat = "
          SELECT
            villes.IdVille,
            villes.Nom,
            villes.CodePostal,
            villes.NomSimple,
            commercants.NomSociete as commercant,
            COUNT(IdCommercant) as nbCommercant
            FROM
            bonplan
            Inner Join commercants ON bonplan.bonplan_commercant_id = commercants.IdCommercant
            Inner Join villes ON villes.IdVille = commercants.IdVille ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        $sqlcat .= " where commercants.IsActif = 1 ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        //LOCALDATA FILTRE
        /* if(isset($_SESSION['localdata']) && $_SESSION['localdata']=="cagnescommerces"){
             $sqlcat .= " AND commercants.IdVille = '2031' ";
         }*/
        //LOCALDATA FILTRE

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sqlcat .= " AND commercants.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            villes.IdVille, commercants.NomSociete ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetFidelityVilles()
    {

        $sqlcat = "
          SELECT
            villes.IdVille,
            villes.Nom,
            villes.CodePostal,
            villes.NomSimple,
            commercants.NomSociete as commercant,
            COUNT(IdCommercant) as nbCommercant
            FROM
            villes
            Inner Join commercants ON commercants.IdVille = villes.IdVille
            LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
            where commercants.IsActif = 1
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital))
          ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sqlcat .= " AND commercants.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            villes.IdVille, commercants.NomSociete 
            ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetFidelityVilles_by_departement($departement_id = "0")
    {
        $sqlcat = "
          SELECT
            villes.IdVille,
            villes.Nom,
            villes.CodePostal,
            villes.NomSimple,
            commercants.NomSociete as commercant,
            COUNT(IdCommercant) as nbCommercant
            FROM
            villes
            Inner Join commercants ON commercants.IdVille = villes.IdVille
            LEFT OUTER JOIN card_capital ON card_capital.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_remise ON card_remise.id_commercant = commercants.IdCommercant
            LEFT OUTER JOIN card_tampon ON card_tampon.id_commercant = commercants.IdCommercant
            ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        $sqlcat .= " 
            where commercants.IsActif = 1
            AND (commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_remise) 
            OR commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_tampon) 
            or commercants.IdCommercant IN (SELECT id_commercant as IdCommercant from card_capital))
        ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        //LOCALDATA FILTRE
        /* if(isset($_SESSION['localdata']) && $_SESSION['localdata']=="cagnescommerces"){
             $sqlcat .= " AND commercants.IdVille = '2031' ";
         }*/
        //LOCALDATA FILTRE

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND commercants.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sqlcat .= " AND commercants.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND commercants.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
            GROUP BY
            villes.IdVille, commercants.NomSociete ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAnnonceVilles()
    {
        $qryCategorie = $this->db->query("
            SELECT
            villes.IdVille,
            villes.Nom,
            villes.CodePostal,
            villes.NomSimple,
            commercants.IdCommercant,
			COUNT(commercants.IdCommercant) as nb_commercant
            FROM
            annonce
            Inner Join commercants ON commercants.IdCommercant = annonce.annonce_commercant_id
            Inner Join villes ON villes.IdVille = commercants.IdVille
            where commercants.IsActif = 1
            GROUP BY
            villes.IdVille
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAnnonceVilles_pvc()
    {
        $qryCategorie = $this->db->query("
            SELECT
            villes.IdVille,
            villes.Nom,
            villes.CodePostal,
            villes.NomSimple,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            annonce
            Inner Join commercants ON commercants.IdCommercant = annonce.annonce_commercant_id
            Inner Join villes ON villes.IdVille = commercants.IdVille
            where commercants.IsActif = 1
            GROUP BY
            villes.IdVille
        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAnnonceVilles_pvc_by_departement($departement_id = "0")
    {
        $qrySql = "
            SELECT
            villes.IdVille,
            villes.Nom,
            villes.CodePostal,
            villes.NomSimple,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            annonce
            Inner Join commercants ON commercants.IdCommercant = annonce.annonce_commercant_id
            Inner Join villes ON villes.IdVille = commercants.IdVille ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $qrySql .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        $qrySql .= " where commercants.IsActif = 1 ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $qrySql .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        $qrySql .= "
            
            GROUP BY
            villes.IdVille
        ";

        //echo $qrySql;

        $qryCategorie = $this->db->query($qrySql);


        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAgendaVilles()
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(agenda.id) as nb_agenda
                FROM
                agenda
                INNER JOIN villes ON agenda.IdVille_localisation = villes.IdVille
                WHERE
                (agenda.IsActif = 1 OR
                agenda.IsActif = 2)
                ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sql_qr .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sql_qr .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sql_qr .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ";

        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetFestivalVilles()
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(festival.id) as nb_agenda
                FROM
                festival
                INNER JOIN villes ON festival.IdVille_localisation = villes.IdVille
                WHERE
                (festival.IsActif = 1 OR
                festival.IsActif = 2)
                ";
        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAnnuaireVilles()
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(annuaire.id) as nb_annuaire
                FROM
                annuaire
                INNER JOIN villes ON annuaire.IdVille_localisation = villes.IdVille
                WHERE
                (annuaire.IsActif = 1 OR
                annuaire.IsActif = 2)
                ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sql_qr .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sql_qr .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sql_qr .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ";

        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAgendaVilles_pvc()
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(agenda.id) as nbCommercant
                FROM
                agenda
                INNER JOIN villes ON agenda.IdVille_localisation = villes.IdVille
                WHERE
                (agenda.IsActif = 1 OR
                agenda.IsActif = 2)
                ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sql_qr .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sql_qr .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sql_qr .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ";

        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAnnuaireVilles_pvc()
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(annuaire.id) as nbCommercant
                FROM
                agenda
                INNER JOIN villes ON annuaire.IdVille_localisation = villes.IdVille
                WHERE
                (annuaire.IsActif = 1 OR
                annuaire.IsActif = 2)
                ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sql_qr .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sql_qr .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sql_qr .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ";

        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetArticleVilles_pvc()
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(article.id) as nbCommercant
                FROM
                article
                INNER JOIN villes ON article.IdVille_localisation = villes.IdVille
                WHERE
                (article.IsActif = 1 OR
                article.IsActif = 2)
                ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sql_qr .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sql_qr .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sql_qr .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ";

        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();

        } else {
            return false;
        }
    }

    function GetFestivalVilles_pvc()
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(festival.id) as nbCommercant
                FROM
                festival
                INNER JOIN villes ON festival.IdVille_localisation = villes.IdVille
                WHERE
                (festival.IsActif = 1 OR
                festival.IsActif = 2)
                ";

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sql_qr .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sql_qr .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sql_qr .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ";

        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAgendaVilles_pvc_by_departement($departement_id = "0")
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(agenda.id) as nbCommercant
                FROM
                agenda
                INNER JOIN villes ON agenda.IdVille_localisation = villes.IdVille ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sql_qr .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        "
                WHERE
                (agenda.IsActif = 1 OR
                agenda.IsActif = 2)
                ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sql_qr .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sql_qr .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sql_qr .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sql_qr .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ";

        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetArticleVilles_pvc_by_departement($departement_id = "0")
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(article.id) as nbCommercant
                FROM
                article
                INNER JOIN villes ON article.IdVille_localisation = villes.IdVille ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sql_qr .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        "
                WHERE
                (article.IsActif = 1 OR
                article.IsActif = 2)
                ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sql_qr .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sql_qr .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sql_qr .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sql_qr .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ";

        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetFestivalVilles_pvc_by_departement($departement_id = "0")
    {
        $sql_qr = "
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(festival.id) as nbCommercant
                FROM
                festival
                INNER JOIN villes ON festival.IdVille_localisation = villes.IdVille ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sql_qr .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        "
                WHERE
                (festival.IsActif = 1 OR
                festival.IsActif = 2)
                ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sql_qr .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sql_qr .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sql_qr .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sql_qr .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sql_qr .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ";

        $qryCategorie = $this->db->query($sql_qr);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAgendaVillesByIdCommercant($IdCommercant)
    {
        $sqlcat = "SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(agenda.id) as nb_agenda
                FROM
                agenda
                INNER JOIN villes ON agenda.IdVille_localisation = villes.IdVille
                WHERE
                (agenda.IsActif = 1 OR
                agenda.IsActif = 2) AND
                agenda.IdCommercant = '" . $IdCommercant . "'
                ";

        //LOCALDATA FILTRE
        /*if(isset($_SESSION['localdata']) && $_SESSION['localdata']=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        }*/
        //LOCALDATA FILTRE

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sqlcat .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAnnuaireVilleByIdCommercant($IdCommercant)
    {
        $sqlcat = "SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(annuaire.id) as nb_annuaire
                FROM
                annuaire
                INNER JOIN villes ON annuaire.IdVille_localisation = villes.IdVille
                WHERE
                (annuaire.IsActif = 1 OR
                annuaire.IsActif = 2) AND
                annuaire.IdCommercant = '" . $IdCommercant . "'
                ";

        //LOCALDATA FILTRE
        /*if(isset($_SESSION['localdata']) && $_SESSION['localdata']=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        }*/
        //LOCALDATA FILTRE

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sqlcat .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetArticleVillesByIdCommercant($IdCommercant)
    {
        $sqlcat = "SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(festival.id) as nb_agenda
                FROM
                festival
                INNER JOIN villes ON festival.IdVille_localisation = villes.IdVille
                WHERE
                (festival.IsActif = 1 OR
                festival.IsActif = 2) AND
                festival.IdCommercant = '" . $IdCommercant . "'
                ";

        //LOCALDATA FILTRE
        /*if(isset($_SESSION['localdata']) && $_SESSION['localdata']=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        }*/
        //LOCALDATA FILTRE

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sqlcat .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetAgendaVillesByIdCommercant_by_departement($IdCommercant, $departement_id = "0")
    {
        $sqlcat = "SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(agenda.id) as nb_agenda
                FROM
                agenda
                INNER JOIN villes ON agenda.IdVille_localisation = villes.IdVille ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        $sqlcat .= "
                WHERE
                (agenda.IsActif = 1 OR
                agenda.IsActif = 2) AND
                agenda.IdCommercant = '" . $IdCommercant . "'
                ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        //LOCALDATA FILTRE
        /*if(isset($_SESSION['localdata']) && $_SESSION['localdata']=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        }*/
        //LOCALDATA FILTRE

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sqlcat .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetArticleVillesByIdCommercant_by_departement($IdCommercant, $departement_id = "0")
    {
        $sqlcat = "SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(article.id) as nb_article
                FROM
                article
                INNER JOIN villes ON article.IdVille_localisation = villes.IdVille ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        $sqlcat .= "
                WHERE
                (article.IsActif = 1 OR
                article.IsActif = 2) AND
                article.IdCommercant = '" . $IdCommercant . "'
                ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $sqlcat .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        //LOCALDATA FILTRE
        /*if(isset($_SESSION['localdata']) && $_SESSION['localdata']=="cagnescommerces"){
            $sqlcat .= " AND villes.IdVille = '2031' ";
        }*/
        //LOCALDATA FILTRE

        //LOCALDATA FILTRE
        $this_session_localdata =& get_instance();
        $this_session_localdata->load->library('session');
        $localdata_value = $this_session_localdata->session->userdata('localdata');
        $localdata_IdVille = $this_session_localdata->session->userdata('localdata_IdVille');
        $localdata_IdDepartement = $this_session_localdata->session->userdata('localdata_IdDepartement');
        if (isset($localdata_value) && $localdata_value == "cagnescommerces") {
            $sqlcat .= " AND villes.IdVille = '2031' ";
        } else if (isset($localdata_IdVille) && $localdata_IdVille != "" && $localdata_IdVille != "0") {
            $sqlcat .= " AND villes.IdVille = '" . $localdata_IdVille . "' ";
        } else if (isset($localdata_IdDepartement) && $localdata_IdDepartement != "" && $localdata_IdDepartement != "0") {
            $sqlcat .= " AND villes.IdVille IN (SELECT villes.IdVille FROM villes WHERE villes.ville_departement = '" . $localdata_IdDepartement . "')";
        }
        //LOCALDATA FILTRE


        $sqlcat .= "
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC ";

        $qryCategorie = $this->db->query($sqlcat);
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetAgendaVillesByIdUsers_ionauth($user_ionauth_id)
    {
        $qryCategorie = $this->db->query("
                SELECT
                villes.Nom,
                villes.IdVille,
                COUNT(agenda.id) as nb_agenda
                FROM
                agenda
                INNER JOIN villes ON agenda.IdVille_localisation = villes.IdVille
                WHERE
                (agenda.IsActif = 1 OR
                agenda.IsActif = 2) AND
                agenda.IdUsers_ionauth = '" . $user_ionauth_id . "'
                GROUP BY
                villes.IdVille
                ORDER BY
                villes.Nom ASC

        ");
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetCommercantVilles_by_departement($departement_id = "0")
    {
        $qrySql = "
            SELECT
            villes.IdVille,
            villes.Nom,
            villes.CodePostal,
            villes.NomSimple,
            commercants.IsActif,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            commercants
            Inner Join villes ON commercants.IdVille = villes.IdVille
            Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $qrySql .= " Inner Join departement ON villes.ville_departement = departement.departement_code ";
        }

        $qrySql .= "
            WHERE
            commercants.IsActif =  '1'  AND commercants.referencement_annuaire = 1 AND
            ass_commercants_rubriques.IdRubrique <> 0
            ";

        if (isset($departement_id) && $departement_id != "" && $departement_id != "0" && $departement_id != null) {
            $qrySql .= " AND departement.departement_id = '" . $departement_id . "'";
        }

        $qrySql .= "
            GROUP BY
            villes.IdVille
            ORDER BY
            villes.Nom ASC
        ";

        $qryCategorie = $this->db->query($qrySql);

        //INNER JOIN ass_commercants_sousrubriques ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
        //DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }


    function GetCommercantVilles()
    {
        $qryCategorie = $this->db->query("
            SELECT
            villes.IdVille,
            villes.Nom,
            villes.CodePostal,
            villes.NomSimple,
            commercants.IsActif,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            commercants
            Inner Join villes ON commercants.IdVille = villes.IdVille
            Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            WHERE
            commercants.IsActif =  '1'  AND commercants.referencement_annuaire = 1 AND
            ass_commercants_rubriques.IdRubrique <> 0

            GROUP BY
            villes.IdVille
            ORDER BY
            villes.Nom ASC
        ");
        //INNER JOIN ass_commercants_sousrubriques ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
        //DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetCommercantCommunes()
    {
        $qryCategorie = $this->db->query("
            SELECT
            com_cant.id_commune,
            com_cant.N_Com_min,
            com_cant.C_Com,
            commercants.IsActif,
            COUNT(commercants.IdCommercant) as nbCommercant
            FROM
            commercants
            Inner Join com_cant ON commercants.id_commune = com_cant.id_commune
            Inner Join ass_commercants_abonnements ON commercants.IdCommercant = ass_commercants_abonnements.IdCommercant
            INNER JOIN ass_commercants_rubriques ON ass_commercants_rubriques.IdCommercant = commercants.IdCommercant
            WHERE
            commercants.IsActif =  '1' AND
            ass_commercants_rubriques.IdRubrique <> 0

            GROUP BY
            com_cant.id_commune
            ORDER BY
            com_cant.N_Com_min ASC
        ");
        //INNER JOIN ass_commercants_sousrubriques ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
        //DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function GetCommercantDepartement()
    {
        $qryCategorie = $this->db->query("
            SELECT
            departement.departement_id,
            departement.departement_nom,
            departement.departement_code,
            COUNT(commercants.IdCommercant) as nbCommercant 
            FROM
            departement 
            Inner Join commercants ON commercants.departement_id = departement.departement_id
            WHERE
            commercants.IsActif =  '1' AND commercants.referencement_annuaire = 1 

            GROUP BY
            departement.departement_id
            ORDER BY
            departement.departement_nom ASC
        ");
        //INNER JOIN ass_commercants_sousrubriques ON commercants.IdCommercant = ass_commercants_sousrubriques.IdCommercant
        //DATEDIFF(ass_commercants_abonnements.DateFin ,CURDATE()) >0
        if ($qryCategorie->num_rows() > 0) {
            return $qryCategorie->result();
        }
    }

    function supprimeVille($prmId)
    {

        $qryBonplan = $this->db->query("DELETE FROM villes WHERE IdVille = ?", $prmId);
        return $qryBonplan;
    }

    function insertVille($prmData)
    {
        $this->db->insert("villes", $prmData);
        return $this->db->insert_id();
    }

    function updateVille($prmData)
    {
        $this->db->where("IdVille", $prmData["IdVille"]);
        $this->db->update("villes", $prmData);
        $objAnnonce = $this->getVilleById($prmData["IdVille"]);
        return $objAnnonce->IdVille;
    }

    function getVilleById($id = 0)
    {
        $Sql = "select * from villes where IdVille =" . $id;
        $Query = $this->db->query($Sql);
        return $Query->row();
    }

    function getVilleByCP($cp)
    {
        $this->db->select('*');
        $this->db->from($this->_table);
        $this->db->join('departement', 'villes.ville_departement = departement.departement_code');
        $this->db->where($this->_table . '.CodePostal', $cp);
        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result() : false;
    }

    function getWhere($where=''){
        $sql = "
            SELECT
                *
            FROM
                villes
            WHERE 
              0=0 
        ";
        if (isset($where) && $where != '') $sql .= " AND ".$where;
        $request = $this->db->query($sql);
        if($request->num_rows() > 0) {
            return $request->result();
        }
    }

}