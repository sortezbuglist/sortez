<?php

function checksub($domain)
{
    $exp = explode('.', $domain);
    if (count($exp) > 3) {
        return true;
    } else {
        return false;
    }
}

if (checksub($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'])) {
    $current_url = $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $current_url = str_replace("www.", "", $current_url);
    $current_url = explode(".", $current_url);

    if ($_SERVER['HTTP_HOST'] == "antibes-juan-les-pins.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.antibes-juan-les-pins.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/6';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "cagnes-sur-mer.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.cagnes-sur-mer.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/4';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "cannes.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.cannes.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/3';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "les-vallees-de-la-roya-bevera.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.les-vallees-de-la-roya-bevera.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/11';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "nice.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.nice.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/8';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "saint-laurent-du-var.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.saint-laurent-du-var.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/10';?>";</script><?php
    } elseif ($_SERVER['HTTP_HOST'] == "villefranche-sur-mer.vivresaville.fr" || $_SERVER['HTTP_HOST'] == "www.villefranche-sur-mer.vivresaville.fr") {
        ?><script type="application/javascript">window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'].'vivresaville/ListVille/CheckVille/7';?>";</script><?php
    } else {

        ?>
        <script type="application/javascript">
            window.location.href = "http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $current_url[0] . '/presentation';?>";
            //alert("<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . $current_url[0] . '/presentation';?>");
        </script>
        <?php
    }

}

if ($_SERVER['SERVER_NAME'] == "127.0.0.1" || $_SERVER['SERVER_NAME'] == "127.0.0.1") {
    ?>

    <!DOCTYPE html>

    <html lang="fr">

    <head>

        <meta charset="UTF-8">

        <title>vivresaville-sorties-loisirs-articles-agenda</title>

        <meta name="generator" content="Serif WebPlus X8 (16,0,4,32)">

        <meta name="viewport" content="width=1250">

        <link rel="stylesheet" type="text/css" href="wpscripts_vsv/wpstyles.css">

        <style type="text/css">

            .OBJ-1 {
                border-radius: 20px / 20px;
            }

            @font-face {
                font-family: 'Futura Md';
                src: url('wpscripts_vsv/wp545f5f8e.ttf');
            }

            .OBJ-2, .OBJ-2:link, .OBJ-2:visited {
                background-image: url('http://www.sortez.org/wpimages_vsv/wpdb9f4e8f_06.png');
                background-repeat: no-repeat;
                background-position: 0px 0px;
                text-decoration: none;
                display: block;
                position: absolute;
            }

            .OBJ-2:hover {
                background-position: 0px -140px;
            }

            .OBJ-2:active, a:link.OBJ-2.Activated, a:link.OBJ-2.Down, a:visited.OBJ-2.Activated, a:visited.OBJ-2.Down, .OBJ-2.Activated, .OBJ-2.Down {
                background-position: 0px -70px;
            }

            .OBJ-2.Disabled, a:link.OBJ-2.Disabled, a:visited.OBJ-2.Disabled, a:hover.OBJ-2.Disabled, a:active.OBJ-2.Disabled {
                background-position: 0px -210px;
            }

            .OBJ-2:focus {
                outline-style: none;
            }

            button.OBJ-2 {
                background-color: transparent;
                border: none 0px;
                padding: 0;
                display: inline-block;
                cursor: pointer;
            }

            button.OBJ-2:disabled {
                pointer-events: none;
            }

            .OBJ-2.Inline {
                display: inline-block;
                position: relative;
                line-height: normal;
            }

            .OBJ-2 span, .OBJ-2:link span, .OBJ-2:visited span {
                color: #dc188e;
                font-family: "Futura Md", sans-serif;
                font-weight: normal;
                text-decoration: none;
                text-align: center;
                text-transform: none;
                font-style: normal;
                left: 13px;
                top: 10px;
                width: 645px;
                height: 46px;
                line-height: 46px;
                font-size: 34px;
                display: block;
                position: absolute;
                cursor: pointer;
            }

            .OBJ-2:hover span {
                color: #dc198f;
            }

            .OBJ-3 {
                line-height: 70px;
            }

            .OBJ-4, .OBJ-4:link, .OBJ-4:visited {
                background-image: url('http://www.sortez.org/wpimages_vsv/wpd44b3652_06.png');
                background-repeat: no-repeat;
                background-position: 0px 0px;
                text-decoration: none;
                display: block;
                position: absolute;
            }

            .OBJ-4:hover {
                background-position: 0px -120px;
            }

            .OBJ-4:active, a:link.OBJ-4.Activated, a:link.OBJ-4.Down, a:visited.OBJ-4.Activated, a:visited.OBJ-4.Down, .OBJ-4.Activated, .OBJ-4.Down {
                background-position: 0px -60px;
            }

            .OBJ-4.Disabled, a:link.OBJ-4.Disabled, a:visited.OBJ-4.Disabled, a:hover.OBJ-4.Disabled, a:active.OBJ-4.Disabled {
                background-position: 0px -180px;
            }

            .OBJ-4:focus {
                outline-style: none;
            }

            button.OBJ-4 {
                background-color: transparent;
                border: none 0px;
                padding: 0;
                display: inline-block;
                cursor: pointer;
            }

            button.OBJ-4:disabled {
                pointer-events: none;
            }

            .OBJ-4.Inline {
                display: inline-block;
                position: relative;
                line-height: normal;
            }

            .OBJ-4 span, .OBJ-4:link span, .OBJ-4:visited span {
                color: #ffffff;
                font-family: Tahoma, sans-serif;
                font-weight: normal;
                text-decoration: none;
                text-align: center;
                text-transform: none;
                font-style: normal;
                left: 0px;
                top: 16px;
                width: 500px;
                height: 27px;
                line-height: 27px;
                font-size: 21px;
                display: block;
                position: absolute;
                cursor: pointer;
            }

            .OBJ-5 {
                line-height: 60px;
            }

            @font-face {
                font-family: 'Futura Hv';
                src: url('wpscripts_vsv/wpe3dd8930.ttf');
            }

            .P-1 {
                text-align: center;
                margin-bottom: 1.3px;
                line-height: 46.7px;
                font-family: "Futura Hv", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #000000;
                background-color: transparent;
                font-variant: normal;
                font-size: 40.0px;
                vertical-align: 0;
            }

            .P-2 {
                text-align: center;
                margin-bottom: 5.3px;
                line-height: 48.0px;
                font-family: "Futura Hv", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #000000;
                background-color: transparent;
                font-variant: normal;
                font-size: 40.0px;
                vertical-align: 0;
            }

            .P-3 {
                text-align: center;
                margin-bottom: 5.3px;
                line-height: 48.0px;
                font-family: "Futura Hv", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #000000;
                background-color: transparent;
                font-variant: normal;
                font-size: 48.0px;
                vertical-align: 0;
            }

            .C-1 {
                font-family: "Futura Hv", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #da188e;
                background-color: transparent;
                text-decoration: none;
                font-variant: normal;
                font-size: 48.0px;
                vertical-align: 0;
            }

            .P-4 {
                text-align: center;
                margin-bottom: 2.7px;
                line-height: 34.7px;
                font-family: "Verdana", sans-serif;
                font-style: normal;
                font-weight: normal;
                color: #000000;
                background-color: transparent;
                font-variant: normal;
                font-size: 32.0px;
                vertical-align: 0;
            }

        </style>

        <script type="text/javascript" src="wpscripts_vsv/jquery.js"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                $("a.ActiveButton").bind({
                    mousedown: function () {
                        if ($(this).attr('disabled') === undefined) $(this).addClass('Activated');
                    }, mouseleave: function () {
                        if ($(this).attr('disabled') === undefined) $(this).removeClass('Activated');
                    }, mouseup: function () {
                        if ($(this).attr('disabled') === undefined) $(this).removeClass('Activated');
                    }
                });

            });

        </script>

        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

    </head>

    <body style="height:980px;background:url('http://www.sortez.org/wpimages_vsv/wpc625edec_06.jpg') no-repeat fixed center top / 100% 100% transparent;">

    <div id="divMain"
         style="background:transparent;margin-left:auto;margin-right:auto;position:relative;width:1250px;height:980px;">

        <img alt="" src="http://www.sortez.org/wpimages_vsv/wp3f83823b_06.png"
             style="position:absolute;left:390px;top:79px;width:492px;height:153px;">

        <img alt="" src="http://www.sortez.org/wpimages_vsv/wp5efeea30_06.png"
             style="position:absolute;left:344px;top:283px;width:584px;height:80px;">

        <div class="OBJ-1" style="position:absolute;left:246px;top:283px;width:779px;height:100px;">

            <a href="http://www.vivresaville.fr/vivresaville/accueil"
               title="Découvrez les sites des villes partenaires rassemblant les informations institutionnelles et privées (annuaire complet, articles, agenda, bons, plans, conditions de fidélisation, boutiques en ligne) déposées par les acteurs administratifs, associatifs et économiques de la cité."
               id="nav_112_B1" class="OBJ-2 ActiveButton Down OBJ-3"
               style="display:block;position:absolute;left:54px;top:5px;width:670px;height:70px;">

                <span>Choisissez&nbsp;votre&nbsp;département</span>

            </a>

        </div>

        <div style="position:absolute;left:76px;top:384px;width:1144px;height:268px;overflow:hidden;">

            <p class="Corps P-1"><br></p>

            <p class="Corps P-2">DÉCOUVREZ LES SITES FÉDÉRATEURS</p>

            <p class="Corps P-3">« vivre<span class="C-1">sa</span>ville.fr »</p>

            <p class="Corps P-2">CRÉÉS AVEC LES COLLECTIVITÉS</p>

            <p class="Corps P-2">TERRITORIALES PARTENAIRES !…</p>

            <p class="Normal P-4"><br></p>

        </div>

        <img alt="" src="http://www.sortez.org/wpimages_vsv/wpa9e4b551_06.png"
             style="position:absolute;left:173px;top:686px;width:925px;height:182px;">

        <div id="nav_112_B1M"
             style="position:absolute;visibility:hidden;width:540px;height:80px;background:transparent url('http://www.sortez.org/wpimages_vsv/wp55f00916_06.png') no-repeat scroll left top;">

            <a href="http://www.vivresaville.fr/vivresaville/accueil" id="nav_112_B1M_L1"
               class="OBJ-4 ActiveButton OBJ-5"
               style="display:block;position:absolute;left:20px;top:10px;width:500px;height:60px;">

                <span>Alpes-Maritimes&nbsp;&amp;&nbsp;Monaco</span>

            </a>

        </div>

    </div>


    <script type="text/javascript" src="wpscripts_vsv/jsMenu.js"></script>

    <script type="text/javascript">

        wpmenustack.setRollovers([['nav_112_B1', 'nav_112_B1M', {"m_vertical": true}]]);

        wpmenustack.setMenus(['nav_112_B1M'], {"m_vOffset": 10, "m_vAlignment": 1});

    </script>

    </body>

    </html>


    <?php
}

else {
    ?>
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Le Magazine Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco</title>
        <meta name="generator" content="Serif WebPlus X8">
        <meta name="viewport" content="width=1250">
        <meta name="keywords" content="animations, spectacles, concerts, théâtres, salons, musées, expositions, alpes-maritimes, nice, antibes, monaco, cannes, Théâtres, concerts, spectacles, animations, magazine, sortez, sortir, cannes, alpes-maritimes">
        <meta name="description" content="Sur les Alpes-Maritimes et Monaco, découvrez l'essentiel des sorties et des loisirs et bien plus encore !... animations, spectacles, concerts, théâtres, salons, musées, expositions...&#xD;&#xA;">
        <meta name="author" content="Priviconcept">
        <meta name="robots" content="index,follow">
        <link rel="stylesheet" type="text/css" href="wpscripts/wpstyles.css">
        <style type="text/css">
            @font-face { font-family: 'Futura Hv'; src: url('wpscripts/wpe3dd8930.ttf'); }
            .P-1 { text-align:center;margin-bottom:0.0px;line-height:38.7px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:29.0px;vertical-align:0; }
            @font-face { font-family: 'Futura Md'; src: url('wpscripts/wp545f5f8e.ttf'); }
            .P-2 { text-align:center;margin-bottom:0.0px;line-height:42.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:32.0px;vertical-align:0; }
            .P-3 { margin-bottom:0.0px;line-height:24.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-4 { line-height:16.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-5 { line-height:24.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-6 { margin-bottom:4.0px;line-height:24.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-7 { margin-bottom:0.0px;line-height:24.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-8 { line-height:24.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .OBJ-1 { background:transparent url('http://www.sortez.org/wpimages/wpb755b032_06.png') no-repeat 0px 0px; }
            .OBJ-2,.OBJ-2:link,.OBJ-2:visited { background-image:url('http://www.sortez.org/wpimages/wpe19c503b_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }
            .OBJ-2:hover { background-position:0px -118px; }
            .OBJ-2:active,a:link.OBJ-2.Activated,a:link.OBJ-2.Down,a:visited.OBJ-2.Activated,a:visited.OBJ-2.Down,.OBJ-2.Activated,.OBJ-2.Down { background-position:0px -59px; }
            .OBJ-2:focus { outline-style:none; }
            button.OBJ-2 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }
            button.OBJ-2:disabled { pointer-events:none; }
            .OBJ-2.Inline { display:inline-block;position:relative;line-height:normal; }
            .OBJ-2 span,.OBJ-2:link span,.OBJ-2:visited span { color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:13px;top:19px;width:87px;height:16px;line-height:16px;font-size:13px;display:block;position:absolute;cursor:pointer; }
            .OBJ-3 { line-height:59px; }
            .OBJ-4,.OBJ-4:link,.OBJ-4:visited { background-image:url('http://www.sortez.org/wpimages/wpc20b0068_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }
            .OBJ-4:hover { background-position:0px -118px; }
            .OBJ-4:active,a:link.OBJ-4.Activated,a:link.OBJ-4.Down,a:visited.OBJ-4.Activated,a:visited.OBJ-4.Down,.OBJ-4.Activated,.OBJ-4.Down { background-position:0px -59px; }
            .OBJ-4:focus { outline-style:none; }
            button.OBJ-4 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }
            button.OBJ-4:disabled { pointer-events:none; }
            .OBJ-4.Inline { display:inline-block;position:relative;line-height:normal; }
            .OBJ-4 span,.OBJ-4:link span,.OBJ-4:visited span { color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:13px;top:19px;width:345px;height:16px;line-height:16px;font-size:13px;display:block;position:absolute;cursor:pointer; }
            .OBJ-5,.OBJ-5:link,.OBJ-5:visited { background-image:url('http://www.sortez.org/wpimages/wpfcfabd98_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }
            .OBJ-5:hover { background-position:0px -118px; }
            .OBJ-5:active,a:link.OBJ-5.Activated,a:link.OBJ-5.Down,a:visited.OBJ-5.Activated,a:visited.OBJ-5.Down,.OBJ-5.Activated,.OBJ-5.Down { background-position:0px -59px; }
            .OBJ-5:focus { outline-style:none; }
            button.OBJ-5 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }
            button.OBJ-5:disabled { pointer-events:none; }
            .OBJ-5.Inline { display:inline-block;position:relative;line-height:normal; }
            .OBJ-5 span,.OBJ-5:link span,.OBJ-5:visited span { color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:13px;top:19px;width:333px;height:16px;line-height:16px;font-size:13px;display:block;position:absolute;cursor:pointer; }
            .OBJ-6,.OBJ-6:link,.OBJ-6:visited { background-image:url('http://www.sortez.org/wpimages/wpc6069d92_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }
            .OBJ-6:hover { background-position:0px -118px; }
            .OBJ-6:active,a:link.OBJ-6.Activated,a:link.OBJ-6.Down,a:visited.OBJ-6.Activated,a:visited.OBJ-6.Down,.OBJ-6.Activated,.OBJ-6.Down { background-position:0px -59px; }
            .OBJ-6:focus { outline-style:none; }
            button.OBJ-6 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }
            button.OBJ-6:disabled { pointer-events:none; }
            .OBJ-6.Inline { display:inline-block;position:relative;line-height:normal; }
            .OBJ-6 span,.OBJ-6:link span,.OBJ-6:visited span { color:#000000;font-family:Arial,sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:uppercase;font-style:normal;left:13px;top:19px;width:165px;height:16px;line-height:16px;font-size:13px;display:block;position:absolute;cursor:pointer; }
            .P-9 { margin-bottom:4.0px;line-height:24.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-10 { text-align:center;line-height:1px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:29.0px;vertical-align:0; }
            .C-1 { line-height:38.00px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;text-decoration:none;font-variant:normal;font-size:29.3px;vertical-align:0; }
            .P-11 { text-align:center;margin-bottom:0.0px;line-height:1px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:35.0px;vertical-align:0; }
            .C-2 { line-height:46.00px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;text-decoration:none;font-variant:normal;font-size:34.7px;vertical-align:0; }
            .OBJ-7,.OBJ-7:link,.OBJ-7:visited { background-image:url('http://www.sortez.org/wpimages/wp62c71fcc_06.png');background-repeat:no-repeat;background-position:0px 0px;text-decoration:none;display:block;position:absolute; }
            .OBJ-7:hover { background-position:0px -140px; }
            .OBJ-7:active,a:link.OBJ-7.Activated,a:link.OBJ-7.Down,a:visited.OBJ-7.Activated,a:visited.OBJ-7.Down,.OBJ-7.Activated,.OBJ-7.Down { background-position:0px -70px; }
            .OBJ-7.Disabled,a:link.OBJ-7.Disabled,a:visited.OBJ-7.Disabled,a:hover.OBJ-7.Disabled,a:active.OBJ-7.Disabled { background-position:0px -210px; }
            .OBJ-7:focus { outline-style:none; }
            button.OBJ-7 { background-color:transparent;border:none 0px;padding:0;display:inline-block;cursor:pointer; }
            button.OBJ-7:disabled { pointer-events:none; }
            .OBJ-7.Inline { display:inline-block;position:relative;line-height:normal; }
            .OBJ-7 span,.OBJ-7:link span,.OBJ-7:visited span { color:#dc188e;font-family:"Futura Md",sans-serif;font-weight:normal;text-decoration:none;text-align:center;text-transform:none;font-style:normal;left:74px;top:20px;width:279px;height:26px;line-height:26px;font-size:19px;display:block;position:absolute;cursor:pointer; }
            .OBJ-7:hover span { color:#dc198f; }
            .OBJ-8 { line-height:70px; }
            .P-12 { text-align:center;margin-bottom:26.7px;text-indent:-33.7px;line-height:1px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:29.0px;vertical-align:0; }
            .P-13 { margin-left:96.0px;margin-bottom:10.7px;text-indent:-49.7px;line-height:25.3px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-14 { margin-left:48.0px;margin-bottom:10.7px;line-height:25.3px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-15 { text-align:center;line-height:38.7px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:29.0px;vertical-align:0; }
            .P-16 { text-align:center;line-height:1px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:29.0px;vertical-align:0; }
            .C-3 { line-height:38.00px;font-family:"Futura Hv", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;text-decoration:none;font-variant:normal;font-size:29.3px;vertical-align:0; }
            .C-4 { line-height:28.00px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#000000;background-color:transparent;text-decoration:none;font-variant:normal;font-size:21.3px;vertical-align:0; }
            .P-17 { margin-bottom:6.7px;line-height:28.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-18 { line-height:26.7px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-19 { margin-bottom:6.7px;line-height:24.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
            .P-20 { margin-bottom:0.0px;line-height:28.0px;font-family:"Futura Md", sans-serif;font-style:normal;font-weight:normal;color:#ffffff;background-color:transparent;font-variant:normal;font-size:20.0px;vertical-align:0; }
        </style>
        <script type="text/javascript" src="wpscripts/jquery.js"></script>
        <script type="text/javascript" src="wpscripts/jquery.wputils.js"></script>
        <script type="text/javascript" src="wpscripts/jquery.wplightbox.js"></script>
        <script type="text/javascript">
            //wpRedirectMinScreen(500,900,'http://www.sortez.org/mobile-index.html',0);
            $(document).ready(function() {
                $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});
                $('.wplightbox').wplightbox(
                    {"loadBtnSrc":"http://www.sortez.org/wpimages/lightbox_load.gif","playBtnSrc":"http://www.sortez.org/wpimages/lightbox_play.png","playOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_play_over.png","pauseBtnSrc":"http://www.sortez.org/wpimages/lightbox_pause.png","pauseOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_pause_over.png","border_e":"http://www.sortez.org/wpimages/lightbox_e_6.png","border_n":"http://www.sortez.org/wpimages/lightbox_n_6.png","border_w":"http://www.sortez.org/wpimages/lightbox_w_6.png","border_s":"http://www.sortez.org/wpimages/lightbox_s_6.png","border_ne":"http://www.sortez.org/wpimages/lightbox_ne_6.png","border_se":"http://www.sortez.org/wpimages/lightbox_se_6.png","border_nw":"http://www.sortez.org/wpimages/lightbox_nw_6.png","border_sw":"http://www.sortez.org/wpimages/lightbox_sw_6.png","closeBtnSrc":"http://www.sortez.org/wpimages/lightbox_close_2.png","closeOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_close_over_2.png","nextBtnSrc":"http://www.sortez.org/wpimages/lightbox_next_2.png","nextOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_next_over_2.png","prevBtnSrc":"http://www.sortez.org/wpimages/lightbox_prev_2.png","prevOverBtnSrc":"http://www.sortez.org/wpimages/lightbox_prev_over_2.png","blankSrc":"wpscripts/blank.gif","bBkgrndClickable":true,"strBkgrndCol":"#000000","nBkgrndOpacity":0.5,"strContentCol":"#ffffff","nContentOpacity":0.8,"strCaptionCol":"#555555","nCaptionOpacity":1.0,"nCaptionType":1,"bCaptionCount":true,"strCaptionFontType":"Tahoma,Serif","strCaptionFontCol":"#ffffff","nCaptionFontSz":15,"bShowPlay":true,"bAnimateOpenClose":true,"nPlayPeriod":2000}
                );
                $.get( "http://localhost/sortez/accueil/main_department_list/", function( data ){
                    //alert( "Data Loaded: " + data );
                    $("#sortez_main_department_list").html(data);
                });
            });
        </script>
        <link rel="icon" href="favicon.ico" type="image/x-icon" sizes="16x16 32x32 48x48 64x64">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" sizes="16x16 32x32 48x48 64x64">
        <link rel="apple-touch-icon" href="favicon.ico" sizes="60x60 76x76 120x120 152x152">
    </head>
    <body style="height:4700px;background:url('http://www.sortez.org/wpimages/wp98f0f982_06.jpg') no-repeat fixed center top / cover transparent;">
    <div id="divMain" style="background:transparent;margin-left:auto;margin-right:auto;position:relative;width:1250px;height:4700px;"><img alt="LES BOUTIQUEs en ligne"
        <img src="http://www.sortez.org/wpimages/wpe763ca96_06.png" alt="" width="1250" height="582" style="position:absolute;left:0px;top:3799px;width:1250px;height:582px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp7f264ea5_05_06.jpg" style="position:absolute;left:774px;top:3851px;width:475px;height:467px;">
        <img src="http://www.sortez.org/wpimages/wp626e3b5e_06.png" alt="" width="1250" height="1276" style="position:absolute;left:0px;top:1362px;width:1250px;height:1276px;">
        <div style="position:absolute;left:22px;top:1944px;width:1211px;height:170px;overflow:hidden;">
            <p class="Corps4 P-1">POUR BÉNÉFICIER DES BONS PLANS ET DES CONDITIONS DE FIDÉLISATION,</p>
            <p class="Corps4 P-1">LE CONSOMMATEUR DOIT POSSÉDER LA CARTE PRIVILÈGE VIVRESAVILLE.FR</p>
            <p class="Corps4 P-1">ELLE EST DÉLIVRÉE GRATUITEMENT</p>
            <p class="Corps4 P-2"><br></p>
        </div>
        <!-- DEBUT W  O  W  S  L   I  D  E-->
        <div>
            <iframe id="ifrm_17" name="ifrm_17" src="http://www.sortez.org/bannieres/index/index.html" class="OBJ-1" style="position:absolute;left:0px;top:224px;width:1250px;height:200px;"></iframe>
        </div>
        <!-- FIN   W  O  W  S  L   I  D  E-->
        <div style="position:absolute;left:67px;top:1145px;width:346px;height:164px;overflow:hidden;">
            <p class="Corps P-3">Vous avez accès à un annuaire qui référence nos partenaires (administrations, commerçants, artisans, associations, des adresses culturelles et patrimoniales..)…</p>
            <p class="Corps-artistique2 P-4"><br></p>
        </div>
        <div style="position:absolute;left:465px;top:1145px;width:357px;height:132px;overflow:hidden;">
            <p class="Corps-artistique2 P-5">Vous avez accès aux articles de nos partenaires, de nos éditions en ligne et à certaines données venant de l’open data…</p>
        </div>
        <div style="position:absolute;left:885px;top:1145px;width:345px;height:132px;overflow:hidden;">
            <p class="Corps P-6">Vous avez accès aux événements de nos partenaires…</p>
            <p class="Corps P-3">Vous trouverez également des données venant de l’open data….</p>
        </div>
        <img alt="Les articles" src="http://www.sortez.org/wpimages/wpebda89c2_06.png" style="position:absolute;left:548px;top:1100px;width:143px;height:31px;">
        <img alt="L’ANNUAIRE" src="http://www.sortez.org/wpimages/wpda6ae93c_06.png" style="position:absolute;left:163px;top:1101px;width:142px;height:31px;">
        <img alt="L’agenda événementiel" src="http://www.sortez.org/wpimages/wped9596d8_06.png" style="position:absolute;left:895px;top:1099px;width:288px;height:33px;">
        <div style="position:absolute;left:474px;top:1758px;width:329px;height:132px;overflow:hidden;">
            <p class="Corps P-7">Découvrez des offres uniques, exceptionnelles,</p>
            <p class="Corps P-7">motivantes pour le consommateur…</p>
            <p class="Corps-artistique2 P-4"><br></p>
        </div>
        <div style="position:absolute;left:878px;top:1758px;width:354px;height:132px;overflow:hidden;">
            <p class="Corps-artistique2 P-8">Découvrez des offres de fidélisation : des remises Cash, des offres de capitalisation ou coups de tampons…</p>
        </div>
        <img alt="" src="http://www.sortez.org/wpimages/wp9e6e204d_06.png" style="position:absolute;left:0px;top:2114px;width:1250px;height:300px;">
        <div class="OBJ-1" style="position:absolute;left:20px;top:8px;width:1210px;height:60px;">
            <a href="essai-index.html" id="nav_124_B1" class="OBJ-2 ActiveButton Down OBJ-3" style="display:block;position:absolute;left:0px;top:0px;width:112px;height:59px;">
                <span>Accueil</span>
            </a>
            <div id="nav_124_B2" class="OBJ-3" style="display:block;position:absolute;left:172px;top:0px;width:370px;height:59px;">
                <button type="button" disabled="" class="OBJ-4 ActiveButton Disabled" style="width:370px;height:59px;">
                    <span>les&nbsp;éditions&nbsp;du&nbsp;Magazine&nbsp;Sortez</span>
                </button>
            </div>
            <a data-lightbox="{&quot;galleryId&quot;:&quot;wplightbox&quot;,&quot;width&quot;:650,&quot;height&quot;:900}" class="wplightbox OBJ-5 ActiveButton OBJ-3" href="annonce-infos.html" id="nav_124_B3" style="display:block;position:absolute;left:602px;top:0px;width:358px;height:59px;">
                <span>Informations&nbsp;&amp;&nbsp;souscriptions</span>
            </a>
            <a href="http://www.sortez.org/auth/login" target="_blank" id="nav_124_B4" class="OBJ-6 ActiveButton OBJ-3" style="display:block;position:absolute;left:1020px;top:0px;width:190px;height:59px;">
                <span>Mon&nbsp;compte</span>
            </a>
        </div>

        <!--sortez_main_department_list-->
        <div id="sortez_main_department_list"></div>

        <style type="text/css">
            #sortez_main_department_list {
                position:absolute;left:290px;top:131px;width:690px;height:auto;
                text-align: center;
            }
            #sortez_main_department_list select {
                font-family: "Futura Md",sans-serif;
                color: #dc198f;
                font-size: 20px;
                padding: 15px 100px;
                border-radius: 10px;
                border: solid;
                cursor: pointer;
                margin-top: 40px;
            }
        </style>

        <img alt="LES BOUTIQUEs en ligne" src="http://www.sortez.org/wpimages/wp2256afec_06.png" style="position:absolute;left:96px;top:1688px;width:265px;height:32px;">
        <div style="position:absolute;left:67px;top:1758px;width:345px;height:132px;overflow:hidden;">
            <p class="Corps P-9">Vous avez accès aux boutiques de nos partenaires…</p>
            <p class="Corps P-7">Certaines possèdent un module de règlement en ligne avec PAYPAL…</p>
        </div>
        <img alt="" src="http://www.sortez.org/wpimages/wpb8376aee_06.png" style="position:absolute;left:116px;top:827px;width:244px;height:244px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp0fa3c6e8_06.png" style="position:absolute;left:504px;top:827px;width:244px;height:244px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp929b1b77_06.png" style="position:absolute;left:916px;top:827px;width:244px;height:244px;">
        <img src="http://www.sortez.org/wpimages/wpf6015ae7_06.png" alt="" width="277" height="264" style="position:absolute;left:110px;top:1424px;width:277px;height:264px;">
        <img alt="" src="http://www.sortez.org/wpimages/wpffc3f0a2_06.png" style="position:absolute;left:525px;top:1425px;width:244px;height:244px;">
        <img alt="" src="http://www.sortez.org/wpimages/wpb8e69575_06.png" style="position:absolute;left:927px;top:1425px;width:244px;height:244px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp058b05af_06.png" style="position:absolute;left:0px;top:3799px;width:1250px;height:582px;">
        <div style="position:absolute;left:20px;top:4007px;width:1210px;height:127px;overflow:hidden;">
            <p class="Corps-artistique P-10"><span class="C-1">SUR CERTAINS DÉPARTEMENTS, NOTRE RÉDACTION</span></p>
            <p class="Corps-artistique P-10"><span class="C-1">PUBLIE ET DIFFUSE UN MAGAZINE MENSUEL ET GRATUIT</span></p>
            <p class="Corps-artistique P-10"><span class="C-1">SUR L’ESSENTIEL DES SORTIES ET DES LOISIRS</span></p>
            <p class="Corps P-11"><span class="C-2"><br></span></p>
            <p class="Corps P-11"><span class="C-2">DE NOS PARTENAIRES PROFESSIONNELS</span></p>
            <p class="Corps P-11"><span class="C-2"><br></span></p>
        </div>
        <img alt="" src="http://www.sortez.org/wpimages/wpe0d2771f_06.png" style="position:absolute;left:451px;top:3863px;width:348px;height:106px;">
        <map id="map1" name="map1">
            <area shape="rect" coords="74,11,262,37" href="france-editions.html" alt="">
            <area shape="rect" coords="0,0,336,48" href="france-editions.html" alt="">
        </map>
        <img src="http://www.sortez.org/wpimages/wpd63d9b72_06.png" alt="" width="335" height="47" usemap="#map1" style="position:absolute;left:457px;top:4248px;width:335px;height:47px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp3e296ced_06.png" style="position:absolute;left:32px;top:4192px;width:398px;height:210px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp640b73bb_06.png" style="position:absolute;left:1079px;top:4142px;width:168px;height:165px;">
        <map id="map2" name="map2">
            <area shape="rect" coords="412,193,658,241" href="http://www.sortez.org/site/mentions-legales.html" target="_blank" alt="">
            <area shape="rect" coords="477,203,651,229">
            <area shape="poly" coords="446,238,455,234,461,226,464,217,463,209,458,200,450,194,446,192,435,192,425,197,419,205,417,213,418,223,423,232,430,237,434,239,446,239">
            <area shape="poly" coords="447,228,447,225,444,225,444,208,434,208,434,213,437,213,437,225,434,225,434,230,447,230">
            <area shape="poly" coords="442,205,444,205,444,200,442,198,439,198,438,200,437,202,438,204,439,206,442,206">
            <area shape="poly" coords="446,238,455,234,461,227,464,217,462,207,457,198,448,193,445,192,435,193,426,197,419,204,417,214,418,224,424,232,432,238,435,239,445,239">
            <area shape="poly" coords="298,238,307,234,313,227,316,217,315,209,310,200,302,194,298,192,287,192,277,197,271,205,269,213,270,223,275,232,282,237,286,239,298,239" href="https://accounts.google.com/ServiceLogin?service=oz&amp;passive=1209600&amp;continue=https://plus.google.com/share?url%3Dhttp://privicarte.fr%26gpsrc%3Dgplp0&amp;btmpl=popup#identifier" target="_blank" alt="">
            <area shape="poly" coords="294,227,289,227,288,226,287,225,287,222,288,221,294,221,295,222,295,226">
            <area shape="poly" coords="304,223,304,220,308,220,308,217,304,217,304,213,302,213,302,217,298,217,298,220,302,220,302,224">
            <area shape="poly" coords="290,213,289,212,288,211,288,205,289,203,291,203,292,204,292,205,293,207,293,210">
            <area shape="poly" coords="294,228,298,226,300,225,298,221,296,217,293,216,288,216,287,215,288,214,293,214,296,212,298,208,297,204,300,202,287,202,283,204,282,208,284,212,286,214,283,217,287,221,283,221,282,224,284,228,286,229,294,229">
            <area shape="poly" coords="304,220,308,220,308,217,304,217,304,213,302,213,302,217,298,217,298,220,302,220,302,224,304,224">
            <area shape="poly" coords="294,227,288,227,288,226,287,225,287,222,288,221,294,221,295,222,295,226">
            <area shape="poly" coords="292,212,289,212,288,211,288,205,289,204,289,203,291,203,292,204,292,205,293,207,293,210,292,211">
            <area shape="poly" coords="294,228,298,226,300,224,297,221,295,217,293,216,287,216,288,213,289,214,293,214,296,212,298,209,297,205,300,202,286,202,283,205,282,207,285,213,287,214,283,216,285,220,287,221,283,222,282,226,285,228,286,229,294,229">
            <area shape="poly" coords="298,238,307,234,313,227,316,217,314,207,309,198,301,193,297,192,287,193,278,197,272,204,269,214,270,224,276,232,284,238,288,239">
            <area shape="poly" coords="239,210,240,209,239,208,239,207,237,207,237,209,236,210,237,211,239,211" href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org" target="_blank" alt="">
            <area shape="rect" coords="220,206,222,208" href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org" target="_blank" alt="">
            <area shape="poly" coords="228,231,224,230,218,226,211,219,209,216,214,219,221,217,226,214,222,208,221,207,222,206,221,205,223,204,229,210,234,205,239,203,243,209,247,211,241,217,238,223,232,229" href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org" target="_blank" alt="">
            <area shape="rect" coords="221,203,223,205" href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org" target="_blank" alt="">
            <area shape="poly" coords="233,238,242,234,249,227,252,217,251,209,246,200,238,194,234,192,224,193,215,196,208,203,204,214,206,223,211,232,218,237,225,239" href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org" target="_blank" alt="">
            <area shape="poly" coords="239,211,238,211,236,209,237,208,237,207,239,207,239,208,240,209,239,210">
            <area shape="rect" coords="221,206,223,208">
            <area shape="poly" coords="232,229,237,224,241,218,244,212,247,211,242,206,236,203,232,208,229,210,224,205,222,204,220,207,225,212,221,217,215,219,209,216,213,222,219,226,225,230">
            <area shape="rect" coords="221,203,223,205">
            <area shape="poly" coords="239,210,240,209,239,208,239,207,237,207,237,211,239,211">
            <area shape="poly" coords="230,229,236,225,240,219,243,213,246,210,241,205,236,203,232,208,229,210,223,204,220,207,222,206,221,208,225,213,226,214,220,218,214,219,209,216,213,221,218,226,224,230">
            <area shape="rect" coords="221,203,223,205">
            <area shape="poly" coords="187,191,196,187,202,179,204,169,202,159,196,151,189,147,186,146,176,146,167,150,160,157,157,167,158,177,164,185,172,191,175,192,186,192">
            <area shape="poly" coords="168,238,177,234,183,227,186,217,185,209,180,200,172,194,168,192,157,192,147,197,141,205,139,213,140,223,145,232,152,237,156,239,168,239" href="https://www.facebook.com/sharer/sharer.php?u=http%3A//sortez.org" target="_blank" alt="">
            <area shape="poly" coords="164,229,164,219,162,221,161,223,160,224,160,226,158,227,157,229,157,230,164,230">
            <area shape="poly" coords="158,226,160,222,162,219,164,215,164,213,169,213,169,208,164,208,165,205,170,205,170,200,162,200,158,202,157,206,154,208,154,213,157,213,157,227">
            <area shape="poly" coords="164,216,164,213,169,213,169,208,164,208,164,205,170,205,170,200,162,200,158,202,157,206,154,208,154,213,157,213,157,227">
            <area shape="poly" coords="164,226,164,219,157,230,164,230">
            <area shape="poly" coords="168,238,177,234,183,227,186,217,184,207,179,198,170,193,167,192,157,193,148,197,142,204,139,214,140,224,146,232,154,238,157,239,167,239">
            <area shape="poly" coords="370,225,343,225,343,213,342,210,345,211,350,211,353,214,356,216,360,214,364,213,365,214,370,214" data-lightbox="{&quot;galleryId&quot;:&quot;wplightbox&quot;,&quot;width&quot;:500,&quot;height&quot;:600}" class="wplightbox" href="http://sortez.org/portail/contact" alt="">
            <area shape="poly" coords="363,238,372,234,378,227,381,217,380,209,375,200,367,194,363,192,352,192,342,197,336,205,334,213,335,223,340,232,351,239,363,239" data-lightbox="{&quot;galleryId&quot;:&quot;wplightbox&quot;,&quot;width&quot;:500,&quot;height&quot;:600}" class="wplightbox" href="http://sortez.org/portail/contact" alt="">
        </map>
        <img src="http://www.sortez.org/wpimages/wp9ba819ab_06.png" alt="" width="1250" height="333" usemap="#map2" style="position:absolute;left:0px;top:4367px;width:1250px;height:333px;">
        <div style="position:absolute;left:1077px;top:4579px;width:173px;height:121px;">
            <div id="navigation_up_down_4949"><noscript>Javascript is disable - <a href="http://www.supportduweb.com/">http://www.supportduweb.com/</a> - <a href="http://www.supportduweb.com/generateur-boutons-navigation-haut-bas-scroll-defilement-racourcis-page-monter-descendre-up-down.html">Générateur de boutons de navigation</a></noscript><script type="text/javascript" src="http://services.supportduweb.com/navigation_up_down/1-4949-right.js"></script></div>
        </div>
        <img alt="Collectivités, associations, professionnels !… Devenez partenaire de sortez.org Et bénéficiez d’outils innovants, adaptés à votre activité Pour attirer de nouveaux contacts" src="http://www.sortez.org/wpimages/wp384593c6_06.png" style="position:absolute;left:159px;top:2699px;width:931px;height:160px;">
        <img alt="Découvrez l’agenda événementiel de votre département,  et pour certains territoires bien plus encore !…" src="http://www.sortez.org/wpimages/wp4808f527_06.png" style="position:absolute;left:165px;top:419px;width:957px;height:129px;">
        <!--<img alt="" src="http://www.sortez.org/wpimages/wp18fb6960_06.png" style="position:absolute;left:412px;top:284px;width:432px;height:59px;">-->
        <!--<div style="position:absolute;left:264px;top:274px;width:758px;height:107px;">
            <a href="essai-actualite.html" title="Découvrez les sites des villes partenaires rassemblant les informations institutionnelles et privées (annuaire complet, articles, agenda, bons, plans, conditions de fidélisation, boutiques en ligne) déposées par les acteurs administratifs, associatifs et économiques de la cité." id="nav_125_B1" class="OBJ-7 ActiveButton OBJ-8" style="display:block;position:absolute;left:161px;top:5px;width:437px;height:70px;">
                <span>Choisissez&nbsp;votre&nbsp;département...</span>
            </a>
        </div>-->
        <img src="http://www.sortez.org/wpimages/wpf8b93dda_06.png" alt="" width="268" height="263" style="position:absolute;left:807px;top:121px;width:268px;height:263px;">
        <map id="map3" name="map3">
            <area shape="rect" coords="45,11,170,37" href="collectivites.html" alt="">
            <area shape="rect" coords="0,0,228,48" href="collectivites.html" alt="">
        </map>
        <img src="http://www.sortez.org/wpimages/wp16029c82_06.png" alt="" width="227" height="47" usemap="#map3" style="position:absolute;left:44px;top:3390px;width:227px;height:47px;">
        <img alt="Vous êtes un organisateur De spectacles" src="http://www.sortez.org/wpimages/wpbc2d39f1_06.png" style="position:absolute;left:657px;top:3279px;width:224px;height:93px;">
        <img alt="Vous êtes Une COLLECTIVITé" src="http://www.sortez.org/wpimages/wp09df87fb_06.png" style="position:absolute;left:55px;top:3279px;width:209px;height:62px;">
        <img alt="Vous êtes Un professionnel" src="http://www.sortez.org/wpimages/wp1cce83f2_06.png" style="position:absolute;left:962px;top:3277px;width:223px;height:66px;">
        <map id="map4" name="map4">
            <area shape="rect" coords="45,11,170,37" href="association.html" alt="">
            <area shape="rect" coords="0,0,228,48" href="association.html" alt="">
        </map>
        <img src="http://www.sortez.org/wpimages/wp188de034_06.png" alt="" width="227" height="47" usemap="#map4" style="position:absolute;left:349px;top:3390px;width:227px;height:47px;">
        <map id="map5" name="map5">
            <area shape="rect" coords="45,11,170,37" href="professionnels.html" alt="">
            <area shape="rect" coords="0,0,228,48" href="professionnels.html" alt="">
        </map>
        <img src="http://www.sortez.org/wpimages/wp188de034_06.png" alt="" width="227" height="47" usemap="#map5" style="position:absolute;left:959px;top:3390px;width:227px;height:47px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp5101d2e4_06.png" style="position:absolute;left:41px;top:3021px;width:244px;height:244px;">
        <img alt="" src="http://www.sortez.org/wpimages/wpd1be6286_06.png" style="position:absolute;left:343px;top:3022px;width:244px;height:244px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp0e4ba70a_06.png" style="position:absolute;left:953px;top:3022px;width:244px;height:244px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp4f156eb8_05_06.jpg" style="position:absolute;left:0px;top:3499px;width:1250px;height:300px;">
        <div style="position:absolute;left:20px;top:567px;width:1210px;height:198px;overflow:hidden;">
            <p class="Corps P-12"><span class="C-1">SORTEZ.ORG EST UN MÉDIA FÉDÉRATEUR DÉPARTEMENTAL</span></p>
            <p class="Corps P-13">Sortez.org est une solution innovante, fédératrice, clés en main dédié à la diffusion de l’information institutionnelle et privée.</p>
            <p class="Corps P-14">Les acteurs inscrits (collectivités, associations, commerçants…) annoncent librement leurs actualités et leurs actions promotionnelles.</p>
            <p class="Corps P-14">Leurs données rejoignent leurs pages individuelles informatives et nos annuaires mutualisés avec des recherches multi-<wbr>critères.</p>
        </div>
        <img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" style="position:absolute;left:20px;top:634px;width:26px;height:26px;">
        <img alt="" src="http://www.sortez.org/wpimages/wp4fc5ce04_06.png" style="position:absolute;left:0px;top:3500px;width:1250px;height:300px;">
        <div style="position:absolute;left:251px;top:3544px;width:748px;height:189px;overflow:hidden;">
            <p class="Corps-artistique P-15">COLLECTIVITÉS, ASSOCIATIONS, PROFESSIONNELS</p>
            <p class="Corps-artistique P-15">AVEC NOTRE PACK DE DÉMARRAGE</p>
            <p class="Corps P-16"><span class="C-3">DÉMARREZ ET TESTEZ GRATUITEMENT NOTRE CONCEPT</span></p>
        </div>
        <img alt="" src="http://www.sortez.org/wpimages/wp531ed261_06.png" style="position:absolute;left:0px;top:3538px;width:262px;height:210px;">
        <map id="map6" name="map6">
            <area shape="rect" coords="45,11,170,37" href="http://www.sortez.org/infos-pro.html" alt="">
            <area shape="rect" coords="0,0,228,48" href="http://www.sortez.org/infos-pro.html" alt="">
        </map>
        <img src="http://www.sortez.org/wpimages/wp16029c82_06.png" alt="" width="227" height="47" usemap="#map6" style="position:absolute;left:512px;top:3697px;width:227px;height:47px;">
        <div style="position:absolute;left:58px;top:2928px;width:1162px;height:70px;overflow:hidden;">
            <p class="Corps"><span class="C-4">Les partenaires disposent d’une multitude d’outils de communication innovants, clés en main, personnels ou mutualisés, gratuits ou à moindre coût dont certains sont habituellement utilisés par les grandes enseignes.</span></p>
        </div>
        <img alt="" src="http://www.sortez.org/wpimages/wpd3e19cb8_06.png" style="position:absolute;left:648px;top:3022px;width:244px;height:244px;">
        <img alt="Vous êtes Une association" src="http://www.sortez.org/wpimages/wpee608705_06.png" style="position:absolute;left:350px;top:3279px;width:223px;height:62px;">
        <map id="map7" name="map7">
            <area shape="rect" coords="45,11,170,37" href="theatre.html" alt="">
            <area shape="rect" coords="0,0,228,48" href="theatre.html" alt="">
        </map>
        <img src="http://www.sortez.org/wpimages/wp188de034_06.png" alt="" width="227" height="47" usemap="#map7" style="position:absolute;left:654px;top:3390px;width:227px;height:47px;">
        <img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" style="position:absolute;left:20px;top:669px;width:26px;height:26px;">
        <img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" style="position:absolute;left:20px;top:706px;width:26px;height:26px;">
        <img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" style="position:absolute;left:20px;top:1142px;width:26px;height:26px;">
        <img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" style="position:absolute;left:420px;top:1146px;width:26px;height:26px;">
        <img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" style="position:absolute;left:837px;top:1147px;width:26px;height:26px;">
        <img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" style="position:absolute;left:10px;top:2930px;width:26px;height:26px;">
        <img src="http://www.sortez.org/wpimages/wp68d4f3de_06.png" alt="" width="26" height="26" style="position:absolute;left:20px;top:1757px;width:26px;height:26px;">
        <img src="http://www.sortez.org/wpimages/wp68d4f3de_06.png" alt="" width="26" height="26" style="position:absolute;left:432px;top:1760px;width:26px;height:26px;">
        <img src="http://www.sortez.org/wpimages/wp68d4f3de_06.png" alt="" width="26" height="26" style="position:absolute;left:840px;top:1758px;width:26px;height:26px;">
        <div style="position:absolute;left:62px;top:2414px;width:1168px;height:125px;overflow:hidden;">
            <p class="Normal2 P-17"><br></p>
            <p class="Normal2 P-18">Dès validation de votre compte, vous accédez immédiatement à une carte privilège dématérialisée et bénéficiez de la validation et de la gestion automatisée des bons plans et des conditions de fidélisation !</p>
            <p class="Corps P-19"><br></p>
            <p class="Corps P-19"><br></p>
            <p class="Corps P-20"><br></p>
            <p class="Corps-artistique2 P-4"><br></p>
        </div>
        <img src="http://www.sortez.org/wpimages/wp68d4f3de_06.png" alt="" width="26" height="26" style="position:absolute;left:20px;top:2456px;width:26px;height:26px;">
        <map id="map8" name="map8">
            <area shape="rect" coords="70,11,270,37" href="infos-consommateurs.html" alt="">
            <area shape="rect" coords="0,0,336,48" href="infos-consommateurs.html" alt="">
        </map>
        <img src="http://www.sortez.org/wpimages/wpb04560a8_06.png" alt="" width="335" height="47" usemap="#map8" style="position:absolute;left:459px;top:2550px;width:335px;height:47px;">
        <map id="map9" name="map9">
            <area shape="rect" coords="121,94,254,136" href="http://www.sortez.org/front/particuliers/inscription" target="_blank" alt="">
            <area shape="poly" coords="267,139,275,133,276,129,276,96,270,89,267,88,101,88,93,94,91,99,91,131,97,139,100,140,267,140" href="http://www.sortez.org/front/particuliers/inscription" target="_blank" alt="">
            <area shape="rect" coords="87,26,281,80" href="http://www.sortez.org/front/particuliers/inscription" target="_blank" alt="">
            <area shape="poly" coords="340,159,348,151,348,25,341,18,340,17,19,17,12,24,11,30,11,151,17,158,20,160,339,160" href="http://www.sortez.org/front/particuliers/inscription" target="_blank" alt="">
        </map>
        <img src="http://www.sortez.org/wpimages/wp7c4dd1c2_06.png" alt="" width="365" height="171" usemap="#map9" style="position:absolute;left:74px;top:2176px;width:365px;height:171px;">
    </div>
    </body>
    </html>

    <?php
}
?>