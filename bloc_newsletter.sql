-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 28 fév. 2019 à 06:33
-- Version du serveur :  5.7.21
-- Version de PHP :  7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sortez`
--

-- --------------------------------------------------------

--
-- Structure de la table `bloc_newsletter`
--

DROP TABLE IF EXISTS `bloc_newsletter`;
CREATE TABLE IF NOT EXISTS `bloc_newsletter` (
  `idbloc` int(11) NOT NULL AUTO_INCREMENT,
  `IdCommercant` int(11) DEFAULT NULL,
  `IsActif` varchar(1) DEFAULT NULL,
  `Num_bloc` varchar(255) DEFAULT NULL,
  `nbre_bloc` varchar(5) DEFAULT NULL,
  `is_activ_bloc` varchar(255) DEFAULT NULL,
  `type_bloc` varchar(255) DEFAULT NULL,
  `bloc_1_image1_content` varchar(255) DEFAULT NULL,
  `bloc1_content` text,
  `is_activ_btn_bloc1` varchar(255) DEFAULT NULL,
  `btn_bloc1_content1` varchar(255) DEFAULT NULL,
  `bloc1_existed_link1` varchar(255) DEFAULT NULL,
  `bloc1_custom_link1` varchar(255) DEFAULT NULL,
  `bloc_1_image2_content` varchar(255) DEFAULT NULL,
  `bloc1_content2` text,
  `is_activ_btn_bloc2` varchar(255) DEFAULT NULL,
  `btn_bloc1_content2` varchar(255) DEFAULT NULL,
  `bloc1_existed_link2` varchar(255) DEFAULT NULL,
  `bloc1_custom_link2` varchar(255) DEFAULT NULL,
  `is_activ_bloc2` varchar(255) DEFAULT NULL,
  `type_bloc2` varchar(255) DEFAULT NULL,
  `bloc_1_2_image1_content` varchar(50) DEFAULT NULL,
  `bloc1_2_content` text,
  `is_activ_btn_bloc1_2` varchar(255) DEFAULT NULL,
  `btn_bloc1_2_content1` varchar(255) DEFAULT NULL,
  `bloc1_2_existed_link1` varchar(255) DEFAULT NULL,
  `bloc1_2_custom_link1` varchar(255) DEFAULT NULL,
  `bloc_1_2_image2_content` varchar(50) DEFAULT NULL,
  `bloc1_2_content2` text,
  `is_activ_btn_bloc2_2` varchar(255) DEFAULT NULL,
  `btn_bloc1_2_content2` varchar(255) DEFAULT NULL,
  `bloc1_2_existed_link2` varchar(255) DEFAULT NULL,
  `bloc1_2_custom_link2` varchar(255) DEFAULT NULL,
  `is_activ_bloc3` varchar(255) DEFAULT NULL,
  `type_bloc3` varchar(255) DEFAULT NULL,
  `bloc_1_3_image1_content` varchar(50) DEFAULT NULL,
  `is_activ_btn_bloc1_3` varchar(255) DEFAULT NULL,
  `btn_bloc1_3_content1` varchar(255) DEFAULT NULL,
  `bloc1_3_existed_link1` varchar(255) DEFAULT NULL,
  `bloc1_3_custom_link1` varchar(255) DEFAULT NULL,
  `bloc_1_3_image2_content` varchar(50) DEFAULT NULL,
  `bloc1_3_content2` text,
  `is_activ_btn_bloc2_3` varchar(255) DEFAULT NULL,
  `btn_bloc1_3_content2` varchar(255) DEFAULT NULL,
  `bloc1_3_existed_link2` varchar(255) DEFAULT NULL,
  `bloc1_3_custom_link2` varchar(255) DEFAULT NULL,
  `is_activ_bloc4` int(10) DEFAULT NULL,
  `type_bloc4` varchar(50) DEFAULT NULL,
  `bloc_1_4_image1_content` varchar(50) DEFAULT NULL,
  `bloc1_4_content` text,
  `is_activ_btn_bloc1_4` varchar(10) DEFAULT NULL,
  `btn_bloc1_4_content1` varchar(50) DEFAULT NULL,
  `bloc1_4_existed_link1` varchar(10) CHARACTER SET koi8u DEFAULT NULL,
  `bloc1_4_custom_link1` varchar(100) DEFAULT NULL,
  `bloc_1_4_image2_content` varchar(50) DEFAULT NULL,
  `bloc1_4_content2` text,
  `is_activ_btn_bloc2_4` int(10) DEFAULT NULL,
  `btn_bloc1_4_content2` varchar(100) DEFAULT NULL,
  `bloc1_4_existed_link2` varchar(10) DEFAULT NULL,
  `bloc1_4_custom_link2` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idbloc`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
