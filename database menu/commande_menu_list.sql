-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 02, 2020 at 12:35 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sortez`
--

-- --------------------------------------------------------

--
-- Table structure for table `commande_menu_list`
--

DROP TABLE IF EXISTS `commande_menu_list`;
CREATE TABLE IF NOT EXISTS `commande_menu_list` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idCom` varchar(255) NOT NULL,
  `comment` text DEFAULT NULL,
  `id_client` bigint(20) NOT NULL,
  `total_price` text NOT NULL,
  `type_livraison` varchar(255) DEFAULT NULL,
  `jour_enlev` date DEFAULT NULL,
  `heure_enleve` varchar(255) DEFAULT NULL,
  `type_payement` varchar(255) DEFAULT NULL,
  `command_files` varchar(255) DEFAULT NULL,
  `type_command` varchar(255) DEFAULT NULL,
  `remise_promotion_price` int(11) DEFAULT NULL,
  `etat_commande` varchar(255) DEFAULT 'En cours',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
