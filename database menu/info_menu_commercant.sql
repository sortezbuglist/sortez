-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 02, 2020 at 10:11 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sortez`
--

-- --------------------------------------------------------

--
-- Table structure for table `info_menu_commercant`
--

DROP TABLE IF EXISTS `info_menu_commercant`;
CREATE TABLE IF NOT EXISTS `info_menu_commercant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `idCommercant` bigint(20) NOT NULL,
  `horaire_emporter_ouvert` text DEFAULT NULL,
  `horaire_emporter_enlev` text DEFAULT NULL,
  `is_activ_emporter` int(11) DEFAULT 0,
  `horaire_livr_ouvert` text DEFAULT NULL,
  `commune_livr_desserv` varchar(255) DEFAULT NULL,
  `is_activ_livr` int(2) DEFAULT NULL,
  `delai_livr_debut` int(2) DEFAULT NULL,
  `delai_livr_fin` int(2) DEFAULT NULL,
  `delai_livr_en_heure` int(2) DEFAULT NULL,
  `livraison_comment` text DEFAULT NULL,
  `is_activ_paypal` int(11) DEFAULT NULL,
  `paypal_content` text DEFAULT NULL,
  `comment_emporter_txt` text DEFAULT NULL,
  `livraison_grtuit_fee` int(11) DEFAULT NULL,
  `jour_heur_livr` text DEFAULT NULL,
  `pay_domicile` int(11) DEFAULT NULL,
  `menu_value` varchar(255) DEFAULT NULL,
  `is_activ_comment_default_enlev` int(11) DEFAULT NULL,
  `is_activ_paypal_enlev` int(11) DEFAULT NULL,
  `cgv_file` varchar(255) DEFAULT NULL,
  `is_activ_comment_default_livr` int(11) DEFAULT NULL,
  `is_activ_prix_livr_grat` int(11) DEFAULT NULL,
  `is_activ_del_livr` int(11) DEFAULT NULL,
  `is_activ_visite` int(11) DEFAULT NULL,
  `is_activ_webservice` int(11) DEFAULT NULL,
  `is_activ_paypal_livr` int(11) DEFAULT NULL,
  `new_paypal_comment` text DEFAULT NULL,
  `is_activ_comm_enlev` int(11) DEFAULT NULL,
  `is_activ_comm_livr_dom` int(11) DEFAULT NULL,
  `is_activ_termin_banc_enlev` int(11) DEFAULT NULL,
  `is_activ_cheque_enlev` int(11) DEFAULT NULL,
  `is_activ_cheque_livr` int(11) DEFAULT NULL,
  `is_activ_termin_bank_livr` int(11) DEFAULT NULL,
  `is_activ_com_spec` int(11) DEFAULT NULL,
  `is_activ_card_bank_bottom` int(11) DEFAULT NULL,
  `is_activ_cheque_bottom` int(11) DEFAULT NULL,
  `is_activ_comment_bottom` int(11) DEFAULT NULL,
  `comment_comm_spec_bottom_txt` text DEFAULT NULL,
  `is_activ_espece_enlev` int(11) DEFAULT NULL,
  `cgv_link` varchar(255) DEFAULT NULL,
  `is_activ_type_livr_grat` int(11) DEFAULT NULL,
  `is_activ_type_livr` int(11) DEFAULT NULL,
  `is_activ_livr_a_domicile` int(11) DEFAULT NULL,
  `is_activ_livr_paypal` int(11) DEFAULT NULL,
  `is_activ_paypal_comment` int(11) DEFAULT NULL,
  `horaire_contact` varchar(255) DEFAULT NULL,
  `is_activ_vente_differe` int(11) DEFAULT NULL,
  `is_activ_env_document` int(11) DEFAULT NULL,
  `is_activ_comment_default_differe` int(11) DEFAULT NULL,
  `comment_differe_txt` text DEFAULT NULL,
  `is_activ_fact_type_differe` int(11) DEFAULT NULL,
  `is_activ_devis_differe` int(11) DEFAULT NULL,
  `is_activ_pro_forma_differe` int(11) DEFAULT NULL,
  `is_activ_facture_differe` int(11) DEFAULT NULL,
  `is_activ_banc_type_differe` int(11) DEFAULT NULL,
  `is_activ_Cheque_differe` int(11) DEFAULT NULL,
  `is_activ_Virement_differe` int(11) DEFAULT NULL,
  `is_activ_carte_differe` int(11) DEFAULT NULL,
  `is_activ_help_img` int(11) DEFAULT NULL,
  `price_to_reach` int(11) DEFAULT NULL,
  `price_to_win` int(11) DEFAULT NULL,
  `is_activ_prom` int(11) DEFAULT NULL,
  `doc_file` varchar(255) DEFAULT NULL,
  `condition_content` varchar(255) NOT NULL,
  `is_actif_condition` int(3) NOT NULL,
  `is_accept_condition` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
