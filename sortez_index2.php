<?php
	$a= $_SERVER['HTTP_USER_AGENT'];
		if (preg_match('/iPad/', $a)) {
			echo "string";
		}else{
			echo $a;
		}
?>

<!DOCTYPE html>
<html lang="en">
<head>

	<title>Le Magazine Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco</title>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="img/bootstrap.css">

	<link rel="stylesheet" href="font/style.css">

	<link rel="stylesheet" type="text/css" href="assets/index/css/style.css">

	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="assets/index/css/bootstrap-grid.min.css">

	<link rel="stylesheet" type="text/css" href="assets/index/css/bootstrap-reboot.min.css">

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<script type="text/javascript" src="https://www.sortez.org/wpscripts/jquery.js"></script>

	<script type="text/javascript" src="https://www.sortez.org/wpscripts/jquery.wputils.js"></script>

	<script type="text/javascript" src="https://www.sortez.org/wpscripts/jquery.wplightbox.js"></script>

	<script src="https://code.jquery.com/jquery-3.2.1.js"></script>

	<link href="assets/css/fancy.css" rel="stylesheet">

	<script src="assets/js/fancy.js"></script>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<script type="text/javascript">

		//wpRedirectMinScreen(500,900,'https://www.sortez.org/mobile-index.html',0);
        $(document).ready(function() {
            $( "#go_news" ).click(function() {
                mail_user = $('#direct_mail').val();
                if (!validateEmail(mail_user)) {
                                alert('Adresse email invalide');
                            }else{
                        $.ajax({
                            type : 'POST',
                            url : 'https://www.sortez.org/front/utilisateur/lightbox_mail_save/',
                            data : 'publightbox_mail=' + mail_user,
                            success:function (data) {
                                alert('votre adresse mail a été enregistré !');
                                set_ow_cookie();
                            }
                        });
                    }
                });
    });
		$(document).ready(function () {

			$("a.ActiveButton").bind({ mousedown: function () { if ($(this).attr('disabled' === undefined) $(this).addClass('Activated'; }, mouseleave: function () { if ($(this).attr('disabled' === undefined) $(this).removeClass('Activated'; }, mouseup: function () { if ($(this).attr('disabled' === undefined) $(this).removeClass('Activated'; } });

			$.get("https://www.sortez.org/front/annuaire/main_commune_list/", function (data) {

				console.log(data);

				$("#sortez_main_department_list").html(data);

			});

		});

	</script>

	<script type="text/javascript">

        //wpRedirectMinScreen(500,900,'https://www.sortez.org/mobile-index.html',0);

        $(document).ready(function() {

            $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

            

            $.get( "https://www.sortez.org/front/annuaire/get_count_annuaire/", function( data ){

                //alert( "Data Loaded: " + data );

                console.log(data);

                $("#count_annuaire").html(data);

                 $("#count_annuaire2").html(data);

            });

        });



        $(document).ready(function() {

            $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

            $.get( "https://www.sortez.org/article/get_count_article/", function( data ){

                //alert( "Data Loaded: " + data );

                console.log(data);

                $("#count_article").html(data);

                $("#count_article2").html(data);

            });

        });



        $(document).ready(function() {

            $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

            $.get( "https://www.sortez.org/agenda/get_count_agenda/", function( data ){

                //alert( "Data Loaded: " + data );

                console.log(data);

                $("#count_agenda").html(data);

                $("#count_agenda2").html(data);

            });

        });

		

        $(document).ready(function() {

            $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

            $.get( "https://www.sortez.org/front/bonplan/get_count_bonplan/", function( data ){

                //alert( "Data Loaded: " + data );

                console.log(data);

                $("#count_bonplan").html(data);

                $("#count_bonplan2").html(data);

            });

        });

		$(document).ready(function() {
            $.get("https://www.sortez.org/front/cadeau/get_click_day/", function( data ){
                console.log(data);
                $("#nb_enligne").html(data);
            });
            $.get( "https://www.sortez.org/front/Utilisateur/get_count_prospect/", function( data ){

                console.log(data);

                $("#nb_inscrits").html(23000+parseInt(data));

            });

        });

        $(document).ready(function() {

            $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

            $.get( "https://www.sortez.org/front/fidelity/get_count_fidelity/", function( data ){

                //alert( "Data Loaded: " + data );

                console.log(data);

                $("#count_fidelity").html(data);

                $("#count_fidelity2").html(data);

            });

        });

        $(document).ready(function() {

			$(".fancybox").fancybox({

				close:

				'sdfsd',

  			});

			checkCookie_ow();

            $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});

            $.get( "https://www.sortez.org/front/annonce/get_count_annonce/", function( data ){

                //alert( "Data Loaded: " + data );

                console.log(data);

                $("#count_annonce").html(data);

                $("#count_annonce2").html(data);

			});

	});

	

	$(document).ready(function() {

	$( "#subscribe_newsletter" ).click(function() {

		mail_user = $('#direct_mail').val();

		if (!validateEmail(mail_user)) {

						alert('Adresse email invalide');

					}else{

				$.ajax({

					type : 'POST',

					url : 'https://www.sortez.org/front/utilisateur/lightbox_mail_save/',

					data : 'publightbox_mail=' + mail_user,           

					success:function (data) {

						alert('votre adresse mail a été enregistré !');

						set_ow_cookie();

					}          

				});

			}

		});

	});

	$(document).ready(function() {

	$( "#save_prospect" ).click(function() {

		mail_user = $('#email').val();

		if (!validateEmail(mail_user)) {

						alert('Adresse email invalide');

					}else{

				$.ajax({

					type : 'POST',

					url : 'https://www.sortez.org/front/utilisateur/lightbox_mail_save/',

					data : 'publightbox_mail=' + mail_user,           

					success:function (data) {

						console.log(data);

						alert('votre adresse E-mail a été enregistré !!');

						set_ow_cookie();

						parent.jQuery.fancybox.getInstance().close();

					}          

				});

			}

		});

	});

	function setCookie_ow(cname, cvalue, exdays) {

    var d = new Date();

    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));

    var expires = "expires=" + d.toGMTString();

    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

}

function getCookie_ow(cname) {

    var name = cname + "=";

    var decodedCookie = decodeURIComponent(document.cookie);

    var ca = decodedCookie.split(';');

    for (var i = 0; i < ca.length; i++) {

        var c = ca[i];

        while (c.charAt(0) == ' ') {

            c = c.substring(1);

        }

        if (c.indexOf(name) == 0) {

            return c.substring(name.length, c.length);

        }

    }

    return "";

}

function checkCookie_ow() {

    var user = getCookie_ow("lightbox_on");

    if (user != "") {

        //alert("Welcome again " + user);

        jQuery("#test_ow_cookies").removeClass("d-block");

        jQuery("#test_ow_cookies").addClass("d-none");

    } else {

			$('#newsletters_box').click();

    }

}

function set_ow_cookie(){

    setCookie_ow("lightbox_on", "6548732154654", 30);

    jQuery("#test_ow_cookies").removeClass("d-block");

    jQuery("#test_ow_cookies").addClass("d-none");

}

	function validateEmail(sEmail) {

				var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

				if (filter.test(sEmail)) {

					return true;

				}

				else {

					return false;

				}

			}

    </script>

	<style type="text/css">

        #language_span .goog-te-gadget-simple .goog-te-menu-value span{
            font-size: 7px!important;
        }

@media only screen and (max-width: 1024px) {

 #newsletter_index{

 	width: 90%!important;



 }

 .fancybox-slide{

 	top: -150px!important;

 }

 #email{

 	padding-top: 5px; 

 	padding-left: 5px;



 	 }

#sous_pro{

	margin-top:-90px;

}

}

@media screen and (max-width: 997px){
	body{
					position: relative;

				}
				#cont_header{
					margin: 0;
					padding: 0;
					position: absolute;
					width: 100%;
					max-width: 100%;
				}
				.pixx{
					margin: 0;
					padding: 0;
					position: absolute;
					width: 100%;
					max-width: 100%;				}
				

				
		}


@media only screen and (max-width: 800px) {

#sous_pro{

	margin-top:-50px;

}

}

@media only screen and (min-width: 1025px) {

 #newsletter_index{

 	width: 50%!important;



 }

 .fancybox-slide{

 	top: 0px!important;

 }

 #email{

 	padding-top: 5px; 

 	padding-left: 5px;



 	 }

}



	#direct_mail:focus{

		outline: none;

	}

		.goog-te-gadget-simple .goog-te-menu-value span{

			font-size: 9px!important;

		}

		.goog-te-gadget-simple{

			width:100%!important;

		}

		#google_translate_element{

			padding-top: 0px!important;

		}

		.centered{

			margin-bottom: 10px;

			justify-content: center;

			display: flex;

			align-items: center;

		}

		body {

			background-image: url("https://www.sortez.org/wpimages/wp797c333e_06.jpg");

			background-repeat: no-repeat;

			background-attachment: fixed;

		}

		.marge{

			margin-bottom: 3%;

		}

		.marg1{

			margin-bottom:1%; 

		}

		.marge2{

			margin-top: 5%;

		}



		.goog-te-gadget-simple {

			background-color: rgba(0, 0, 0, .7) !important;

		}



		.menus {

			font-family: "Futura Md", sans-serif;

			font-size: 12px;

		}



		.goog-te-gadget-simple .goog-te-menu-value span {

			text-transform: uppercase;

			font-size: 12px;

			color: white

			 !important;

		}



		#google_translate_element {

			margin-top: -5px;

		}



		#google_translate_element:hover {

			cursor: pointer;

		}



		iframe.goog-te-menu-frame.skiptranslate {

			visibility: visible;

			box-sizing: content-box;

			width: 266px;

			height: 263px;

			right: 0px !important;

			left: unset !important;

			top: 0px;

		}



		@media screen and (min-width: 992px) {



			#img_green {

				height: 325px;

			}



			#img_green2 {

				width: 1210px;

				height: 550px;

			}



			#btnplusinfo {

				position: absolute;



			}

		}


		@media screen and (max-width: 980px) {

			#img_green2 {

				width: 1210px;

				height: 200px;

			}



			#btnplusinfo {

				position: static;

				margin-top: 45px !important;



			}

		}

		/*//mamitiana*/
		@media screen and (max-width: 769px) {

			#subscribe_newsletter{
				margin-top: -12px !important;
				margin-left: 158px !important;
				-moz-margin-top: -12px !important;
				-moz-margin-left: 158px !important;
				-webkit-margin-top: -12px !important;
				-webkit-margin-left: 225px !important;
			}

			#sous_pro{
				width:40% !important;
			}
		}

		.but{
			margin-left: 10px;
		}



		.ab {

			font-size: 20px;

			text-decoration: none !important;

		}

		.bc{
			background-color: #DA1181;

			border-top-right-radius: 15px;

			border-top-left-radius: 15px;

			height: 100%;

			padding-top: 10px;

			color: white;

			font-size: 9px;

			font-weight: bolder;


		}



		.cole1 {

			background-color: #DA1181;

			border-top-right-radius: 15px;

			border-top-left-radius: 15px;

			height: 100%;

			padding-top: 10px;

			color: white;

			font-size: 15px;

			font-weight: bolder;



		}
		@media screen and (max-width: 1200px) {
			.cole1{
				font-size: 12px;
			}
		}
		 
		



		.cole:hover {

			

			color: #000000;

		}



		.listeimgs1 li {

			list-style-image: url('assets/img/index/wpe7b68eeb_06.png';)

		}



		.menuimgs {

			margin-top: -10px !important;

		}



		.desciptionmenuimgs {

			background-color: #ffffff;

			height: 170px;

		}

		

		.space{

			margin-bottom: 10px;

		}

		.btnmenuimgs {

			background-color: #ffffff !important;

			height: 70px;

		}



		.btnblanc {

			list-style-image: url('assets/img/index/wp8a72da95_06.png';

			font-size: 21px;)

		}

	</style>

	<style type="text/css">

	@font-face {

		font-family: 'Futura Md'; src: url('font/wp545f5f8e.ttf'); 

	}

		

		@font-face { 
			font-family: 'Futura Hv'; 
			src: url('https://www.sortez.org/wpscripts/wpe3dd8930.ttf'); }

		

		@font-face{

			font-family: 'Futura Hv BT Heavy';

			src: local('Futura Hv BT Heavy'), url('font/futurah.woff') format('woff');

		}

		.font1 {

			font-weight: normal;

			font-style: normal;

			font-family: "Futura Md BT Medium", sans-serif;

			



		}



		.font {

			font-size: 30px;

			font-weight: bold;

			font-family: 'Futura Md BT Medium';

		}

		.font2 {

			font-family: "Futura Md BT Medium", sans-serif;

			font-size: 25px;

			font-weight: initial;

		}

		.font3 {

			font-family: "Futura Hv BT Heavy", sans-serif;

			font-size: 25px;

			font-weight: initial;

		}



		.heighter {

			height: 200px;

		}



		.heighter1 {

			height: 250px;

		}



		.hautr {

			height: 450px;

		}



		.heighter2 {

			height: 450px;

		}



		.heighter3 {

			height: 150px;

		}



		.heighter4 {

			height: 50px;

		}



		body {

			background-image: url("img/fond.jpg");

			background-repeat: no-repeat;

			background-size: 100% 100%

		}



		.color {

			color: #ffffff

		}
		.bord{
			border-right: dotted;
			border-width:2px;
			 height: 20px;

			}
		 @media screen and (max-width: 992px) {
		 	.bord{
		 		border-right: none;
		 	}
			 	
			 }
			 @media screen and (max-width: 1200px) {
			 	.container{
			 		width: 100%;
			 	}
			 	
			 }

		</style>

		

</head>



<body>

<a style="display:none" id="newsletters_box" data-fancybox="" data-animation-duration="500" data-src="#newsletter_index"></a>

	<div class="container" id="cont_header">

		<div class="row menus pt-3 d-lg-flex "

			style="color:white!important;padding-top: 10px;padding-bottom: 10px;background-color:rgba(0, 0, 0, .7);margin-left: 0px;margin-right: 0px">

			<div class="col-lg-1 col-sm-4 col-md-4 bord"

				style="text-align: center;"><a

					style="font-size: 12px;color: white" class="menus_2" href="http://www.sortez.org">ACCUEIL</a></div>

			<div class="col-lg-2 col-sm-4 col-md-4 bord"

				style="text-align: center;"><a

					style="font-size: 12px;color: white" class="menus_2" href="edition-mois.html">L'&Eacute;DITION

					DU MOIS</a></div>

			<div class="col-lg-3 col-sm-4 col-md-4 bord"

				style="text-align: center;"><a

					style="font-size: 12px;color: white" class="menus_2" href="annonce-infos.html">INFORMATION

					& SOUSCRIPTION</a></div>

			<div class="col-lg-2 col-sm-4 col-md-4 bord"

				style="text-align: center;"><a

					style="font-size: 12px;color: white" class="menus_2" href="auth/login">MON

					COMPTE</a></div>

			<div class="col-lg-2 col-sm-4 col-md-4 bord"

				style="text-align: center;"><a

					style="font-size: 12px;color: white" class="menus_2" href="auth/login">MES

					FAVORIS</a></div>

					<!-- Google translate Begin-->
				
				 <div id="language_span" style="width: 150px; display:block;margin:auto">

			    <div id="google_translate_element" style="padding-left: 15px; padding-top: 13px"></div>

				<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

				<script type="text/javascript">

					function googleTranslateElementInit() {

			  		new google.translate.TranslateElement({pageLanguage: 'fr', includedLanguages: 'ar,de,en,es,it,ja,nl,no,pl,pt,ru,sv,zh-CN', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false, multilanguagePage: true}, 'google_translate_element');

					}

					</script>

   				</div>
   					<!-- Google translate end-->

				</div>

		<div class="main_rubrique_menu d-none d-lg-block"

			style="border-bottom: double;border-bottom:10px solid #DA1181;background-color: rgba(0,0,0,0.7);height: 50px">

			<div class="row text-center h-100" id="ul_main_menu_rubrique">

				<a class="col-lg-2 col-sm-4 ab" href="http://www.sortez.org/front/annuaire">

					<div class="cole1 cole"><span id="count_annuaire2">L'ANNUAIRE</span>

					</div>

				</a>

				<a class="col-lg-2 col-sm-4 ab" href="http://www.sortez.org/article">

					<div class="cole1 cole"><span id="count_article2">L'ACTUALIT&Eacute;</span>

					</div>

				</a>

				<a class="col-lg-2 col-sm-4 ab" href="http://www.sortez.org/agenda">

					<div class="cole1 cole"><span id="count_agenda2">L'AGENDA</span>

					</div>

				</a>

				<a class="col-lg-2 col-sm-4 ab" href="http://www.sortez.org/front/bonplan">

					<div class="cole1 cole "><span id="count_bonplan2">LES BONS PLANS</span>

					</div>

				</a>

				<a class="col-lg-2 col-sm-4 ab" href="http://www.sortez.org/front/fidelity">

					<div class="cole1 cole "><span id="count_fidelity2">LA FIDELIT&Eacute;</span>

					</div>

				</a>

				<a class="col-lg-2 col-sm-4 ab" href="http://www.sortez.org/front/annonce">

					<div class="cole1 cole"><span id="count_annonce2">LES BOUTIQUES</span>

					</div>

				</a>



			</div>

		</div>



<!-- <style type="text/css"> 

	.nav-link:hover{

		color: black!important;

	}

</style>

<nav class="navbar navbar-expand-lg navbar-light bg-light d-lg-none" style="background-color: #DA1181!important;">

         <a class="navbar-brand" href="#" style="color: white!important; font-weight: bold;">RUBRIQUES</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

            <span class="navbar-toggler-icon"></span>

            </button>

   <div class="collapse navbar-collapse" id="navbarSupportedContent">

    <ul class="navbar-nav mr-auto">

      <li class="nav-item active">

        <a class="nav-link" href="http://www.sortez.org/front/annuaire" style="background-color: #DA1181; color: white; font-weight: bold;">

        	<div>

        		<span id="count_annuaire">L' ANNUAIRE</span>

        	</div>

        	

        </a>

      </li>

      <li class="nav-item">

        <a class="nav-link" href="http://www.sortez.org/article" style="background-color: #DA1181; color: white; font-weight: bold;">

        	<div>

        		<span id="count_article">L'ACTUALITÉ</span>

        	</div>

        	</a>

      </li>

      <li class="nav-item">

        <a class="nav-link" href="http://www.sortez.org/agenda" style="background-color: #DA1181; color: white; font-weight: bold;">

        <div>

        	<span id="count_agenda">L'AGENDA</span>

        </div>

        </a>

      </li>

      <li class="nav-item">

        <a class="nav-link" href="http://www.sortez.org/front/bonplan" style="background-color: #DA1181; color: white; font-weight: bold;"><div>

        	<span id="count_bonplan">LES BONS PLANS</span>

        </div>

        </a>

      </li>

      <li class="nav-item">

        <a class="nav-link" href="http://www.sortez.org/front/fidelity" style="background-color: #DA1181; color: white; font-weight: bold;"><div>

        	<span id="count_fidelity">LA FIDELITÉ</span>

        </div>

    	</a>

      </li>

      <li class="nav-item">

        <a class="nav-link" href="http://www.sortez.org/front/annonce" style="background-color: #DA1181; color: white; font-weight: bold;"><div>

        	<span id="count_annonce">LES BOUTIQUES</span>

        </div>

        </a>

      </li>

    </ul>

  </div>

</nav>-->
	
	<div class="main_rubrique_menu  d-lg-none"

			style="border-bottom: double;border-bottom:10px solid #DA1181;background-color: rgba(0,0,0,0.7);height: 50px">

			<div class="row text-center h-100" id="ul_main_menu_rubrique">

				<a class=" col-2 "  href="http://www.sortez.org/front/annuaire">

					<div class="bc cole " style="margin-right:-5px "><span id="count_annuaire2">L'ANNUAIRE</span>

					</div>

				</a>

				<a class=" col-2 "  href="http://www.sortez.org/article">

					<div class="bc cole " style="margin-left:-5px;margin-right: -5px "><span id="count_article">L'ACTUALIT&Eacute;</span>

					</div>

				</a>

				<a class=" col-2 "  href="http://www.sortez.org/agenda">

					<div class="bc cole " style="margin-left:-5px;margin-right: -5px "><span id="count_agenda">L'AGENDA</span>

					</div>

				</a>

				<a class=" col-2 "  href="http://www.sortez.org/front/bonplan">

					<div class="bc cole " style="margin-left:-5px;margin-right: -5px "><span id="count_bonplan">LES BONS PLANS</span>

					</div>

				</a>

				<a class=" col-2 "  href="http://www.sortez.org/front/fidelity">

					<div class="bc cole " style="margin-left:-5px;margin-right: -5px "><span id="count_fidelity">LA FIDELIT&Eacute;</span>

					</div>

				</a>

				<a class=" col-2 "  href="http://www.sortez.org/front/annonce">

					<div class="bc cole" style="margin-left: -5px"><span id="count_annonce">LES BOUTIQUES</span>

					</div>

				</a>



			</div>

		</div>
		
		






		<!-- debut -->
		<div class="container pixx" style="background-color: rgba(0, 0, 0, 0.5);">   

		<div id="newsletter_index" style="border: 5px solid #fff;border-radius: 20px;text-align: center;background-color: #DC1A95; color:#fff; font-family: sans-serif; width: 55%; margin-left: 25%; margin:auto; height: auto;display: none;padding-bottom:0">

		<div style="font-size:37px; margin-top:30px;" >

			<div style="margin-bottom:5px;margin-top:5px;font-family: Futura Hv, sans-serif;">

				BIENVENUE SUR NOTRE SITE!

	        </div>

	        <div style="margin-bottom:5px;margin-top:5px;margin-bottom: 35px;font-family: Futura Hv, sans-serif;">

				INSCRIVEZ-VOUS GRATUITEMENT <br>

				POUR RECEVOIR NOS NEWSLETTERS

			</div>

		 </div>

		<div>



				<div class="row" style="background-color: black; height: 70px; width: 70%; display: flex; margin: auto; border-radius: 10px; ">

					<div class="col-10"  style="padding-top: 5px; padding-left: 5px;" >

						<input id="email" type="email" name="email" placeholder="Entrer votre E-mail*" style=" height: 60px; border-radius: 10px; display:flex; padding-left: 30px; width: 100%;" >

					</div>

					<div class="col-2" style="padding-top:5px;padding-left:0px">
						<span id="save_prospect" style="font-size: 40px; font-weight: bold">Go!</span>
					</div>

					

				</div>

					<div style="color:#fff; font-size:16px; font-weight:bold;margin:40px 0px 20px 0px;font-size:25px;"><input style="width:20px;height:20px;" type="checkbox" name="checkbox">Enregistrer votre E-mail

					</div>

					<!--<div style="background-color:#4f6ba8; border: 3px solid #fff; border-radius: 15px;left:25%; margin:auto; height:auto; margin-top: 30px;width: 70%">

						<a href="" style="text-decoration: none; display: flex;">

							<img src="img/logo_facebook.png" style="border: 1px solid #4f6ba8; border-top-left-radius:15px; border-bottom-left-radius: 15px;height: 66px;">

							<button style="border:1px solid #4f6ba8; width:auto;font-size: 17px;background-color:#4f6ba8;margin-left:15px;margin-top: 5px;">

								<div style="color: #fff;">Se connecter</div>

								<div style="padding-top:5px; color:#fff; margin-bottom: 5px;">avec facebook</div>

							</button>

						</a>

						

					</div>-->

		</div>
     <div class="row">
		<div class="col-6">
        <div class="m-auto" style="border-top-right-radius:10px; border-top-left-radius: 10px; background-color:#000; color:#fff; font-size:17px; font-weight:bold; text-align: center; margin-bottom: 0px; width: 80%;">
			<div style="font-size: 30px;">

			   <span id="nb_inscrits"></span>

			</div>internautes déjà inscrits
		</div>	
		</div>

		<div class="col-6 m-auto" >
		<div class="m-auto" style="border-top-right-radius:10px; border-top-left-radius: 10px; background-color:#000; color:#fff; font-size:17px; font-weight:bold; text-align: center; margin-bottom: 0px; width: 80%;">
			   <div style="font-size: 30px;">

			          <span id="nb_enligne"></span>

               </div>internautes en ligne
		</div>

		</div>
		 </div>   
	</div>

			<!-- <div class="row">

				<img class="img-fluid" src="img/Bantete.png">				

			</div> -->

			<div class="row" style="background-image: url(img/ban.png); background-repeat: no-repeat;">

				<img class="img-fluid" src="img/index.png" style="text-align: center; margin-left: 150px; display: block; margin: auto; margin-top: 50px;">

			</div>

			<div class="row">

				<div class="col-12 text-center heighter marge2">

					<img class="img-fluid" src="img/Sortez.png">

				</div>

				<div class="row font3 color"style="margin-top: -5%">

					<div class="col-12 text-center" style="font-size: 25px;font-weight: bold;">DE FRÉJUS À MENTON,

						DÉCOUVREZ L’ESSENTIEL DES SORTIES ET DES LOISIRS…

					</div>

					<div class="col-12 text-center marge" style="font-size: 25px;font-weight: bold;">ET BIEN PLUS ENCORE !…

					</div>



				</div>

			</div>

			<div class="row">

				<div class="col-1 but ">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11">

					<div class="font1 color marg1">

						Sortez.org est un média régional qui propose une solution dynamique et innovante print et web

						pour stimuler l’information associative, commerciale et institutionnelle de Fréjus à Menton.

					</div>

				</div>



				<div class="col-1 but">

					<img class="img-fluid" src="img/buton.png">

				</div>


 
				<div class="col-11 ">

					<div class="font1 color marg1">

						Cette solution comprend un magazine de presse mensuel et gratuit lié à une plateforme web

						fédératrice de contenus regroupant des outils marketing performants dont certains sont

						habituellement utilisés par les grandes enseignes.

					</div>

				</div>

				<div class="col-1 but">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11">

					<div class="font1 color marge">

						Avec notre magazine et notre site web, tous les mois nous touchons plus de 250 000 lecteurs et internautes.

					</div>

				</div>



				<div class="col-12  text-center"style="margin-top: 30px">

					<iframe src="https://www.sortez.org/bannieres/pub-sortez/index.html"

						style="width: 95%;height: 90%;">

					</iframe>



				</div>

			</div>





			<div class="row">

				<div class="col-12 text-center font color marge2"style="font-size: 25px">

					DÉCOUVREZ NOTRE MAGAZINE DE PRESSE MENSUEL ET GRATUIT …

				</div>

			</div>





			<div class="row">

				<div class="col-12 text-center font2 color marge"style="font-size: 22px">

					LE SEUL GUIDE SUR L’ESSENTIEL DES SORTIES ET DES LOISIRS DE FRÉJUS À MENTON

				</div>

			</div>





			<div class="row font1">

				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid w-100" src="img/sary1.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/sar1-1.png">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter" style="background-color: #ffffff;padding-top: 20px">

										Visualisez et téléchargez

										l’édition du mois en cours

										version PDF ...

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff; ">

												<a href=" https://www.sortez.org/edition-mois.html"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid w-100" src="img/sary3.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/sary3-1.png">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter " style="background-color: #ffffff;padding-top: 20px">

										La localisation

										de nos dépositaires

									</div>

									<div class="row">

										<div class="container">

											<a href="http://www.sortez.org/article"><div class="col-12 text-right" style="background-color: #ffffff">

												<a href="https://www.google.com/maps/d/viewer?hl=fr&mid=1Q8ieFf0c_AqkLlH3bfcn3EQ-bGSbzqoq&ll=43.71729661946088%2C7.198493450000001&z=11"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>



				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid w-100" src="img/sary4.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/sary4-1.png">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter " style="background-color: #ffffff;padding-top: 20px">



										Plus d’informations

										sur le Magazine Sortez :

										présentation, diffusion,

										dimensions publicitaires …

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff">

												<a href=" https://www.sortez.org/lemag"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid w-100" src="img/newsletter.jpeg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/newsletters.png">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter " style="background-color: #ffffff;padding-top: 20px">

										Pour recevoir nos newsletters

										inscrivez vous  !







									</div>

									<div class="row">

										<div class="container">

											 <div class="col-12 text-center" style="background-color: #ffffff;padding-bottom: 30%;margin-top: -90px;padding-top:30px">

										

       											<div class="input-container" style="border: 3px solid #DC1A95;padding: 0px">

       											<i class="fa fa-envelope icon" style="font-size: x-large;color: #DC1A95;padding-top: 5px"></i>

   												 <input type="email" id="direct_mail" placeholder="E-mail" name="email" style="border: 3px solid #ffffff;width: 68%" >

   												 <img id="subscribe_newsletter" src="img/mailsub.png" style="margin-top: -12px;margin-right: -5px; margin-bottom: -5px;width: 37px;height: 37px;margin-left: 2px;">

   												 </div>

   												 

   											 </div>

   										 </div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

					</div>

				<div class="row">

				<div class="col-12 text-center font3 color marge2"style="font-size: 25px">

					DÉCOUVREZ L’ACTUALITÉ DÉPOSÉE PAR LA VIE INSTITUTIONNELLE ET PRIVÉE

				</div>

			</div>





			<div class="row">

				<div class="col-12 text-center font2 color marge">

					BONNES ADRESSES - ARTICLES - REVUE DE PRESSE - ÉVÉNEMENTS

				</div>

			</div>





			<div class="row font1">

				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid w-100" src="img/wp7fd5fe2a_05_06.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/l'annuaire.png">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter" style="background-color: #ffffff;padding-top: 20px">

										Visualisez les bonnes adresses

										commerçants, artisans, collectivités, patrimoine, loisirs,

										sports, adresses culturelles….

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff">

												<a href="https://www.sortez.org/front/annuaire"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>



				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid w-100" src="img/wp4a05f366_05_06.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/l'actualite.png">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter " style="background-color: #ffffff;padding-top: 20px">

										Visualisez nos articles

										et ceux de nos partenaires :

										vie institutionnelle, associative,

										commerciale …

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff">

												<a href="http://www.sortez.org/article"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>



				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid w-100" src="img/wpcdcbf93d_05_06.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/l'actualite2.png">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter " style="background-color: #ffffff;padding-top: 20px">

										Tous les matins vers 6h00,

										visualisez notre revue

										de presse locale :

										journaux, radio, télévision

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff">

												<a href="http://www.sortez.org/article"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>



				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid w-100" src="img/wp363d39f1_05_06.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/agenda.png">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter " style="background-color: #ffffff;padding-top: 20px">

										Visualisez l’agenda événementiel

										de notre magazine et ceux de nos partenaires

										(animations, cinémas, concerts, spectacles, salons…)

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff">

												<a href="http://www.sortez.org/agenda"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="row">

				<div class="container">

					<div class="col-12 text-center font3 color marge2"style="font-size: 22px">

						DÉCOUVREZ LES ACTIONS PROMOTIONNELLES DÉPOSÉES PAR LE COMMERCE LOCAL...

					</div>

					<div class="row">

						<div class="col-12 text-center font2 color marge"style="font-size: 20px">

							BOUTIQUES EN LIGNE-BONS PLANS-CONDITIONS DE FIDÉLISATION-CARTE DE FIDÉLITÉ

						</div>

					</div>



				</div>

			</div>



			<div class="row font1">



				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid w-100" src="img/wp7fd5fe2a_05_06.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/boutique.png">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter" style="background-color: #ffffff;padding-top: 20px">

										Visualisez les annonces

										de nos partenaires

										(neuf, revente, service)

										Certaines prestations disposent

										d’un paiement en ligne Paypal.

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff">

												<a href="http://www.sortez.org/front/annonce"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>





				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid  w-100" src="img/de.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/plan.jpg">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter" style="background-color: #ffffff;padding-top: 20px">

										Visualisez les bons plans

										déposés par nospartenaires :

										offres de 1ère visite,

										coupons promotionnels,

										offre unique, offre multiple.

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff">

												<a href="http://www.sortez.org/front/bonplan"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>



				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid  w-100" src="img/tampon.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/fidelite.jpg">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter" style="background-color: #ffffff;padding-top: 20px">

										Visualisez les conditions de

										fidélisation définies par nos

										Partenaires : Remises Directes,

										les offres de capitalisation

										et les offres coups de tampon

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff">

												<a href="http://www.sortez.org/front/fidelity"><img class="img-fluid marge" src="img/plus.png"></a>

											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>



				<div class="col-lg-3 col-md-12 text-center">

					<img class="img-fluid  w-100" src="img/IDcart.jpg">

					<div class="row">

						<div class="container">

							<div class="col-12 text-center">

							</div>

							<img class="img-fluid w-100" src="img/carte.jpg">

							<div class="row">

								<div class="container">

									<div class="col-12 text-center heighter" style="background-color: #ffffff;padding-top: 20px">

										Pour bénéficier des bons plans

										et des conditions de fidélisation.

										Le consommateur doit posséder

										la carte de fidélité Vivresaville.fr,

										cette dernière est délivrée gratuitement.

									</div>

									<div class="row">

										<div class="container">

											<div class="col-12 text-right" style="background-color: #ffffff">

												<a href="http://www.sortez.org/front/particuliers/inscription"><img id="sous_pro" style="width:60%;float:left" src="img/bouton.jpg"></a>

												<a href="http://www.sortez.org/front/fidelity"><img class="img-fluid marge" src="img/plus.png"></a>



											</div>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="row ">

				<div class="col-12 text-center font3 color marge2 ">

					COLLECTIVITÉS, ASSOCIATIONS, PROFESSIONNELS !…

				</div>

				<div class="col-12 text-center font3 color ">

					NOUS AVONS LES OUTILS ET L’AUDIENCE NÉCESSAIRE

				</div>

				<div class="col-12 text-center font3 color  ">

					POUR VOUS AIDER À ATTIRER DE NOUVEAUX CONTACTS

				</div>

			</div>

			<div class="row mt-5">

				<div class="col-12 text-center hautr ">

					<img class="img-fluid " src="img/aimant.png">

				</div>



			</div>



			

			<div class="row space">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11">

					<div class="font1 color">

						Les partenaires disposent d’une multitude d’outils de communication innovants, clés en main, mutualisés, gratuits ou à moindre coût dont certains sont habituellement utilisés par les grandes enseignes.

					</div>

				</div>

			</div>

			<div class="row space">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11">

					<div class="font1 color">

						Sur un compte personnel et sécurisé, les partenaires disposent d’une interface intuitive, ils intégrent leurs données (liens, textes, images, vidéo…) d’un simple clic dans les champs disponibles sans faire appel à un webmaster.

					</div>

				</div>

			</div>



			<div class="row space marg1">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11">

					<div class="font1 color">

						Les partenaires bénéficient d’une dynamique fédératrice régionale avec une audience importante liée à notre magazine mensuel et gratuit.

					</div>

				</div>

			</div>



			

			

			<div class="row">

				<div class="col-12 text-center heighter1 marge2">

					<a href="infos-pro.html"><img class="img-fluid" alt="" src="img/info.png"></a>

				</div>

			

			<!--"DOESN'T EXIST"

				</div>

			<div class="row">

				<div class="col-lg-2 col-md-4 centeredtext-center">

					<img class="img-fluid" src="img/1.1.png">

				</div>

				<div class="col-lg-2 col-md-4 centeredtext-center">

					<img class="img-fluid" src="img/2.png">

				</div>

				<div class="col-lg-2 col-md-4 centeredtext-center">

					<img class="img-fluid" src="img/3.png">

				</div>

				<div class="col-lg-2 col-md-4 centeredtext-center">

					<img class="img-fluid" src="img/4.png">

				</div>

				<div class="col-lg-2 col-md-4 centeredtext-center">

					<img class="img-fluid" src="img/img/5.png">

				</div>

				<div class="col-lg-2 col-md-4 centeredtext-center heighter1">

					<img class="img-fluid" src="img/6.png">

				</div>

			</div>

			<div class="row">

				<div class="col-12 text-center font heighter color">

					LA PROMOTION DE NOTRE CONCEPT

				</div>

			</div>

			<div class="row">

				<div class="col-1 heighter3">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11">

					<div class="font1 color">

						La promotion principale de notre site web s’effectue par l’intermédiaire de notre magazine

						mensuel et gratuit.

						<div class="row col-12 color">

							Le magazine Sortez est le 1er magazine mensuel et gratuit diffusé sur le Var-Est, les

							Alpes-Maritimes et Monaco.

						</div>

						<div class="row col-12 color">

							Le magazine Sortez est diffusé tous les mois à 50 000 exemplaires chez plus de 500

							dépositaires (offices de tourisme, mairies, commerces, adresses culturelles…).

							<div class="row col-12 color">

								Le magazine Sortez a un impact direct sur plus de 250 000 lecteurs et internautes par

								mois.

							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="row">

				<div class="col-1 heighter4">

					<img class="img-fluid" src="buton.png">

				</div>

				<div class="col-11">

					<div class="font1 color">

						Une promotion régulière sur les réseaux sociaux et plus particulièrement su Facebook.

					</div>

				</div>

			</div>

			<div class="row">

				<div class="col-1 heighter3 ">

					<img class="img-fluid" src="img/buton.png">

				</div>

				<div class="col-11">

					<div class="font1 color">

						Un envoi régulier de newsletters sur les internautes inscrits mais aussi aux porteurs de notre

						carte de fidélié vivresaville.fr

					</div>

				</div>

			</div>

			<div class="row">

				<div class="col-12 text-center heighter3">

					<img class="img-fluid" src="img/En savoir.png">

				</div>

				-->

			</div>

			<div class="row ">

				<div class="col-12 text-center font3 color ">

					COLLECTIVITÉS, ASSOCIATIONS OU FÉDÉRATIONS DE COMMERÇANTS,

				</div>

				<div class="col-12 text-center font3 color ">

					DÉCOUVREZ NOTRE CONCEPT MUTUALISÉ VIVRESAVILLE.FR

				</div>

				<div class="col-12 text-center font3 color ">

					ET BOOSTEZ L’INFORMATION INSTITUTIONNELLE, COMMERCIALE & ASSOCIATIVE

				</div>

				<div class="col-12 text-center font3 color space"style="margin-bottom: 1.5%">

					DE VOTRE VILLE OU DE VOTRE TERRITOIRE

				</div>

			</div>

			<div class="row">

				<div class="col-12 text-center hautr">

					<img class="img-fluid" src="img/mention.png">

				</div>

			</div>

			<div class="row space"style="margin-top: -4%">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11">

					<div class="font1 color">

						Vivresaville.fr est un concept de communication mutualisé, innovant et complet. Il est basé sur la création de sites web locaux, personnalisés et gratuits élaborés en partenariat avec les collectivités locales ou territoriales et les associations de commerçants. 

					</div>

				</div>

			</div>

			<div class="row ">

				<div class="col-12 text-center font3 color ">

					CHAQUE SITE EST MIS GRATUITEMENT À LA DISPOSITION,

				</div>

				<div class="col-12 text-center font3 color ">

					D’UNE VILLE, D’UNE ASSOCIATION* !

				</div>

				<div class="col-12 text-center font3 color marge">

					*sous conditions d’un nombre minimun de partenaires

				</div>

			</div>

			<div class="row space ">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11">

					<div class="font1 color">

						Le fonctionnement, les abonnements, les outils et les conditions sont identiques à ceux présentés sur le site sortez.org. 

					</div>

				</div>

			</div>

			<div class="row space">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11">

					<div class="font1 color">

						Chaque site Vivresaville.fr ouvre gratuitement le référencement des acteurs locaux (collectivités, associations, professionnels) de son secteur.

					</div>

				</div>

			</div>

			<div class="row space">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11 ">

					<div class="font1 color">

						Sur un compte personnel et sécurisé, les acteurs locaux déposent et diffusent leurs données en temps réel et d’un simple clic.

					</div>

				</div>

			</div>

			<div class="row space">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11 ">

					<div class="font1 color">

						Vivresaville.fr favorise leur accès à des outils marketing, clés en mains, gratuits ou optionnels, personnels ou groupés dont certains sont habituellement utilisés par les grandes enseignes.

					</div>

				</div>

			</div>

			<div class="row space">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11 ">

					<div class="font1 color">

						Vivresaville.fr leur ouvre gratuitement la porte du web et de la mobilité (site web, QRCODE, e.boutique…)

					</div>

				</div>

			</div>

			<div class="row space">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11 ">

					<div class="font1 color">

						Vivresaville.fr développe le sentiment d’appartenance à une commune, à un territoire.

					</div>

				</div>

			</div>

			<div class="row space">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11 ">

					<div class="font1 color">

						Les bases de données de Vivresaville.fr sont liées à notre site principal sortez.org qui centralise et référence la totalité des données  sur ses annuaires départementaux.

					</div>

				</div>

			</div>

			<div class="row space">

				<div class="col-1">

					<img class="img-fluid" src="img/buton.png">

				</div>



				<div class="col-11 heighter3 ">

					<div class="font1 color">

						Avec le magazine Sortez, le concept bénéficie d’une communication fédératrice entre Fréjus et Menton.

					</div>

				</div>

			</div>

			<div class="row">

				<div class="col-12 text-center heighter1">

					<a href="http://www.vivresaville.fr/"><img class="img-fluid" src="img/info.png"></a>

				</div>

				

				 

			</div>
<!-- 
			<div class="row"style="background-color: #ffffff">

				<div class="col-12 text-center"style="font-size: 25px; color: #DC249A; margin-top: 2%;margin-bottom: 4%">

					NOS PRINCIPAUX PARTENAIRES 

				</div>			

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/LOGO_OMC.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logo-odc-hauteur_noir.jpg">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logo_TDG.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logo_theatre_princesse_grace_.jpg">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/les loups de mercantour.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/Mairie_monaco.jpg">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logo palais.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logo DAC-MONACO.jpg">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logofpp.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logo-villeneuve-loubet.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoPaysDeLérins.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logolacolle.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logocannesurmer.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logo-cci-nice-cote-azur.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/LOGO-EAC-266x266.jpg">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logomusee-escoffier.jpg">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/LogoCommunauté_d'agglomération_de_Sophia_Antipolis_.svg.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/touret.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logo-marineland-cote-azur.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/LogoNice_Logo.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoVilleDeMenton.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoAntibesJuan.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoSaintLaurantDu.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoLePalestre.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoThéatredelAlph.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoLaComédieDeNice.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoGolfeJuan.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoBiot.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoVenceSour.png">

				</div>

				<div class="col-lg-2 col-md-4 centered">

					<img class="img-fluid" src="img/logoSjcfOff.png">

				</div>



				<div class="col-12 text-center"style="font-size: 25px; color: #DC249A; margin-top: 2%;margin-bottom: 4%">

					....ET BIEN D'AUTRES ENCORE...

				</div> -->

			

			

			

			<div class="row pt-3 pb-3 w-100 m-auto"

				style="background-color: #DC188E!important;color: #ffffff;font-size: 15px;color:#ffffff!important">

				<div class="col-lg-3">

					<span class="titre_men">LES ANNUAIRES</span></a><br><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/front/annuaire' ?>"><span class="color">&diams;&nbsp;L’annuaire</span></a><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/article' ?>"><span class="color">&diams;&nbsp;L’actualité</span></a>

					<ul>

						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/article' ?>">

							<li> Les articles de Sortez & partenaires</li>

						</a>

						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/revuedepresse' ?>">

							<li>La revue de presse</li>

						</a>

					</ul>

					<a id="linken" href="<?php echo 'https://www.sortez.org/agenda' ?>"><span class="color">&diams;&nbsp;L’agenda</span></a>

					<ul>

						<li>L’agenda événementiel</li>

						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/front/festivals' ?>">

							<li>Les festivals</li>

						</a>

					</ul>



					<a id="linken" href="<?php echo 'https://www.sortez.org/front/bonplan' ?>"><span class="color">&diams;&nbsp;Les bons

							plans</span></a><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/front/fidelity' ?>"><span class="color">&diams;&nbsp;La

							fidélisation</span></a><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/front/annonce' ?>"><span class="color">&diams;&nbsp;Les

							boutiques</span></a>





				</div>

				<div class="col-lg-3">

					<span class="titre_men">LE MAGAZINE SORTEZ</span></a><br><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/edition-mois.html' ?>"><span class="color">&diams;&nbsp;L’édition du

							mois</span></a><br>

					<a id="linken" href="http://fliphtml5.com/bookcase/eirf"><span class="color">&diams;&nbsp;Les

							archives</span></a><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/article' ?>"><span class="color">&diams;&nbsp;Les articles

							numériaues</span></a><br><br><br>





					<span class="titre_men">MON COMPTE CONSOMMATEUR</span></a><br><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/infos-consommateurs.html' ?>"><span class="color">&diams;&nbsp;Infos

							consommateur</span></a><br>

					<a id="linken"

						href="<?php echo 'https://www.sortez.org/front/utilisateur/menuconsommateurs' ?>"><span class="color">&diams;&nbsp;Accès

							admin</span></a><br>

					<a id="linken"

						href="<?php echo 'https://www.sortez.org/front/utilisateur/liste_favoris' ?>"><span class="color">&diams;&nbsp;Mes

							favoris</span></a><br>

					<a id="linken"

						href="<?php echo 'https://www.sortez.org/front/user_fidelity/show_fidelity_card' ?>"><span class="color">&diams;&nbsp;Ma

							carte</span></a><br>

					<a id="linken"

						href="<?php echo 'https://www.sortez.org/front/particuliers/inscription' ?>"><span class="color">&diams;&nbsp;Inscriptions</span></a><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/mentions-legales' ?>"><span class="color">&diams;&nbsp;Condition

							générales</span></a><br>





				</div>

				<div class="col-lg-3">

					<span class="titre_men">MON COMPTE PROFESSIONNEL</span></a><br><br>



					<a id="linken" href="<?php echo 'https://www.sortez.org/infos-pro' ?>"><span class="color">&diams;&nbsp;Infos

							professionnel</span></a><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/auth/login' ?>"><span class="color">&diams;&nbsp;Accès

							admin</span></a><br>

					<a id="linken"

						href="<?php echo 'https://www.sortez.org/front/professionnels/inscription' ?>"><span class="color">&diams;&nbsp;Inscriptions</span></a><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/mentions-legales' ?>"> <span class="color">&diams;&nbsp;Conditions

							générales</span></a><br><br>





					<span class="titre_men">NOS COORDONNÉES</span></a><br><br>



					<a id="linken" href="<?php echo 'https://www.sortez.org/contact' ?>"><span class="color">&diams;&nbsp;Nous

							contacter</span></a><br>

					<a id="linken" href="<?php echo 'https://www.sortez.org/mentions-legales' ?>"><span class="color">&diams;&nbsp;Mentions

							l&eacute;gales</span></a><br>

				</div>

				<div class="col-lg-3">

					<span class="titre_men">DOCUMENTATION COMMERCIALE</span></a><br><br>



					<a id="linken" href="<?php echo 'https://www.sortez.org/infos-consommateurs' ?>"><span class="color">&diams;&nbsp;Infos

							consommateurs</span></a>

					<ul>

						<li>L’inscription et la carte</li>

					</ul>

					<span class="color">&diams;&nbsp;Infos professionnels «le magazine»</span>

					<ul>

						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/magazine-sortez.pdf' ?>">

							<li>Le magazine</li>

						</a>



						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/Dimensions Pub.pdf' ?>">

							<li>les dimensions « pub »</li>

						</a>



						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/Sortez-packarticleok.pdf' ?>">

							<li>Le pack « articles »</li>

						</a>



						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/Sortez-packselectionok.pdf' ?>">

							<li>le pack « sélections »</li>

						</a>

					</ul>

					<span class="color">&diams;&nbsp;Infos professionnels « le web »</span>

					<ul>

						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/pack-infos.html' ?>">

							<li>Le pack « infos »</li>

						</a>



						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/pack-promo.html' ?>">

							<li>Le pack « promos »</li>

						</a>



						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/boutiques.html' ?>">

							<li>Le pack « boutique en ligne »</li>

						</a>



						<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/pack-export.html' ?>">

							<li> Les packs« Export »</li>

						</a>

					</ul>

					<a id="linken" class="color" href="<?php echo 'https://www.sortez.org/contact' ?>"><span class="color">&diams;&nbsp;La

							tarification</span></a>

				</div>

			<div class="resaux" style="border-top-style: double; margin-top: 20px;padding-left: 20px; padding-bottom: 20px;width: 100%" >

					<div style="margin-top: 20px; margin-bottom: 5px; ">	

					<span>PARTAGER</span>	

		  			<a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//sortez.org" style="margin-left: 1.5%">

		  				<img alt src="img/wp11e7dc3f_06.png">

		  			</a>

		  			<a href=""style="margin-left: 1.5%">

		  				<img alt src="img/wp9e062be9_06.png">

		  			</a>

		  			<a href="https://accounts.google.com/ServiceLogin?service=oz&passive=1209600&continue=https://plus.google.com/share?url%3Dhttp://privicarte.fr%26gpsrc%3Dgplp0&btmpl=popup#identifier" style="margin-left: 1.5%">

		  				<img alt src="img/wp3d2af194_06.png">

		  			</a>

		  			<a href="http://sortez.org/portail/contact" style="margin-left: 1.5%">

		  				<img alt src="img/wp2b9894a5_06.png">

		  			</a>

		  			<a href="http://www.sortez.org/site/mentions-legales.html" style="margin-left: 3%">

		  				<img alt src="img/wp3c2a9adc_06.png">

		  			</a>

		  			<span>MENTIONS LEGALES</span>

					</div>

  				</div>

  				</div>

  				<nav class="navbar navbar-expand-sm color" style=";background-color: #000; bottom: 0;z-index: 1030; width: 100%">

  				<p class="copyright" href="#">&copy; 2018 sortez.org Tous droits réservés</p>

				</nav>

			</div>

			</div>

</body>



</html>