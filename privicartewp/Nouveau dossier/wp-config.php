<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Le script de création wp-config.php utilise ce fichier lors de l'installation.
 * Vous n'avez pas à utiliser l'interface web, vous pouvez directement
 * renommer ce fichier en "wp-config.php" et remplir les variables à la main.
 * 
 * Ce fichier contient les configurations suivantes :
 * 
 * * réglages MySQL ;
 * * clefs secrètes ;
 * * préfixe de tables de la base de données ;
 * * ABSPATH.
 * 
 * @link https://codex.wordpress.org/Editing_wp-config.php 
 * 
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'admin_sortez');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'admin_sortez');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'qs54@s654');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2H- IHUI]FGVf0^<d2]r(J5L|@3?kD-w3;(V2(| e %6{O@R:z9q;dnWmta:=^] ');
define('SECURE_AUTH_KEY',  '| c)Qe?Kb>myVzu`B.jQ3vrJDA_#BmD>W_ 2NCI)k3lkyzop6EQH=$P@f:t^T>SV');
define('LOGGED_IN_KEY',    'b8<LSq9hx^a*ty:p###Qp-@,F4A`A)zhzI%1Ye|]fOM/K6:P#v6IZj2nRk~?e/qZ');
define('NONCE_KEY',        '?2y2{7!;QpE@WsUu2T;Rr+Bp5Ec{0OkExeMu<O^K2dDO}NB!?n+d;/q(gAqsTZml');
define('AUTH_SALT',        ':UTK}rp)G+i#QG+dm5:PP<i}HC:,7-P]k2xxm1eVk?Z+t_ara*pKe-|Ux4QFte]n');
define('SECURE_AUTH_SALT', 'oCha)JQAp=r?/Ls|#pi=r!b|`^Y2c`,Z|G9[<B+tOtkc]Y.d`,1nQA0usl-k+t/P');
define('LOGGED_IN_SALT',   'EjuzQfWqf:1W9S9<J]4X<~N0jlkxv?P_p{EgXBM k!_Ym[|)@M!4;=Vl({C0|0Uz');
define('NONCE_SALT',       ' bofF fl,Jgd<c(D/lLC8<G4Ln *t&+=&7?+1def|h=ovyKCE=BH>>)DDwyGXdfw');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wpvc_';

/** 
 * Pour les développeurs : le mode déboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 * 
 * Pour obtenir plus d'information sur les constantes 
 * qui peuvent être utilisée pour le déboguage, consultez le Codex.
 * 
 * @link https://codex.wordpress.org/Debugging_in_WordPress 
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');