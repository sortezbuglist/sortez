-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 07 jan. 2019 à 14:27
-- Version du serveur :  5.7.21
-- Version de PHP :  7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sortez`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

DROP TABLE IF EXISTS `annonce`;
CREATE TABLE IF NOT EXISTS `annonce` (
  `annonce_id` int(11) NOT NULL AUTO_INCREMENT,
  `annonce_commercant_id` int(11) DEFAULT NULL,
  `annonce_sous_rubriques_id` int(11) DEFAULT NULL,
  `annonce_etat` int(11) DEFAULT NULL COMMENT '0:occasion 1:neuf',
  `annonce_date_debut` date DEFAULT NULL,
  `annonce_date_fin` date DEFAULT NULL,
  `annonce_description_courte` text,
  `annonce_description_longue` text,
  `annonce_prix_neuf` varchar(255) DEFAULT NULL,
  `annonce_prix_solde` varchar(255) DEFAULT NULL,
  `annonce_photo1` varchar(250) DEFAULT NULL,
  `annonce_photo2` varchar(250) DEFAULT NULL,
  `annonce_photo3` varchar(250) DEFAULT NULL,
  `annonce_photo4` varchar(250) DEFAULT NULL,
  `annonce_condition_vente` int(1) DEFAULT NULL,
  `annonce_quantite` varchar(255) DEFAULT NULL,
  `annonce_livraison` varchar(255) DEFAULT NULL,
  `retrait_etablissement` tinyint(4) DEFAULT NULL,
  `livraison_domicile` tinyint(4) DEFAULT NULL,
  `module_paypal` tinyint(4) DEFAULT NULL,
  `module_paypal_btnpaypal` text,
  `module_paypal_panierpaypal` text,
  `publication` tinyint(1) DEFAULT '0',
  `order_partner` int(11) DEFAULT NULL,
  `slide_type` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`annonce_id`),
  KEY `idx_annonce_commercant_id` (`annonce_commercant_id`),
  KEY `idx_annonce_id` (`annonce_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
