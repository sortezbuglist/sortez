<?php

class Xmls extends CI_Controller {
    function index() {

        $this->afficher_data_xml();

    }



    function save($ID){//sauvegarde des donnees brutes et leur id
        //fonction qui sauvegarde les donnees
        $this->load->model('xml_models');
        $this->load->helper('clubproximite');
        $data=$this->xml_models->getliens_by_id($ID);

        $xml = simplexml_load_file(	$data->source);

//var_dump($xml->channel->item[0]);
//echo count($xml->channel->item);
        if (isset($xml->channel->item)){
            foreach($xml->channel->item as $donnee){
//var_dump($donnee->title);


                $field =array('nom_manifestation'=>(string)$donnee->title,
                    'siteweb'=>(string)$donnee->link,
                    'description'=>(string)$donnee->description,
                    'date_depot'=>convert_xml_date_to_sql_date($donnee->pubDate),
                    'source_id'=>$data->id);
//var_dump($field);

                $this->xml_models->save($field);



            }
            $field2 =array('article_id'=>(integer)$this->db->insert_id());
            $this->xml_models->save2($field2);
            redirect('admin/xmls/sous_menu_rss');

        }else{echo 'Mauvais lien';}
    }
    function afficher_data_xml(){//accueil rss
        //fonction qui afficher les resultat sauvegardés
        $this->load->model('xml_models');
        $this->load->view('admin/Flux_rss/vue_xml');

    }
    function delete($ID)//supprime un donnée brute
    {
        //foncton qui supprime les donnnees selectionné
        $this->load->model('xml_models');
        $result = $this->xml_models->delete($ID);
        redirect('admin/xmls/saved/');

    }

    function sauvegarder($ID)//affiche les formulaires categ/sous categ
    {


        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;


        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";


        $data["colVilles"] = $this->mdlville->GetAll();
        $data["objCommercant"] = $this->Commercant->GetById($commercant_UserId);
        $data["colCategorie"] = $this->mdl_categories_agenda->GetAll();
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubrique();
        $data['pagecategory'] = "admin_commercant";
        $data['record'] = $this->xml_models->modifier($ID);
        $this->load->view("admin/Flux_rss/option", $data);


    }

    function getSousCategoryTypeArticle()
    {
        $article_categid = $this->input->post("article_categid");
        $data['colSousCategorie'] = $this->mdl_categories_article->GetAllSousCategByTypeArticle($article_categid);
        $this->load->view("front2013/SelectListeSousCategByTypeArticle", $data);
    }

    /* COPYING AGENDA FONCTIONS */

    function GetallSubcateg_by_categ($id = "0")
    {
        //$data["colSousCategorie"] = $this->mdl_categories_article->GetAllSousrubriqueByRubrique($id);
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubriqueByRubrique($id); // article use agenda's categories & subcategories

        $data['id'] = $id;
        $this->load->view("sortez/article/vwReponseSubcateg2", $data);
    }

    function sauver()
    {
        $this->load->helper('clubproximite');
        $objArticle["date_depot"] = convert_Frenchdate_to_Sqldate2($objArticle["date_depot"]);
        $field = array('date_depot' => $objArticle);
        //var_dump($field);
        $this->xml_models->savedate_depot($field);


    }

    function sous_menu_rss()//fonction affichant les listes des liens crees
    {
        $this->load->model('xml_models');
        $records = $this->xml_models->afficher_liens();
        $this->load->view('admin/Flux_rss/vw_lien_rss',['records' =>$records]);
    }
    function ajouter_liens(){//fonction qui affiche le formulaire du lien
        $this->load->model('xml_models');
        $this->load->view('admin/Flux_rss/fiche_liens');
    }

    function sauver_liens()//fonction qui sauvegarde le lien
    {
        $this->load->model('xml_models');
        $field =array(
            'source'=>$this->input->post('source')

        );
        $this->xml_models->sauver_source($field);
        redirect('admin/xmls/sous_menu_rss');


        /* //save source_id
         $fieldid =array('source_id'=>(integer)$this->db->insert_id());
         $this->xml_models->save_src_id($fieldid);
         redirect(site_url('admin/xmls/sous_menu_rss'));*/
    }

    function supprimer_source($ID )//fonction qui supprime un lien crée
    {
        $this->load->model('xml_models');
        $result = $this->xml_models->supprimer_source($ID);
        if ($result){
            redirect('admin/xmls/sous_menu_rss');
        }
        else{echo "erreur de suppression";}
    }

    function saved()//fonction qui affiche les listes desdonnees brutes sauvegardés
    {
        $this->load->model('xml_models');
        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;


        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";


        $data["colVilles"] = $this->mdlville->GetAll();
        $data["objCommercant"] = $this->Commercant->GetById($commercant_UserId);
        $data["colCategorie"] = $this->mdl_categories_agenda->GetAll();
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubrique();
        $data['pagecategory'] = "admin_commercant";
        $data['records']=$this->xml_models->afficher_xml();
        $this->load->view('admin/Flux_rss/vue_donnees_brutes',$data);
    }
    function save_to_sortez($ID){
        $this->load->model('xml_models');


        $this->xml_models->save_to_sortez($ID);

        redirect('admin/xmls/saved');


    }
    public function saved_categ(){
        $this->load->model('xml_models');
        $records=$this->xml_models->afficher_categ_saved();
        $this->load->view('admin/Flux_rss/vue_saved_categ',['records' =>$records]);
    }
    function getall_saved_sortez_and_save($ID){
        $this->load->model('xml_models');
        $this->load->helper('clubproximite');
        $record=$this->xml_models->get_all_saved_sortez($ID);

        foreach ($record as $donnee){


            $field =array(
                'nom_manifestation'=>$donnee->nom_manifestation,
                'siteweb'=>$donnee->siteweb,
                'description'=>$donnee->description,
                'date_depot'=>$donnee->date_depot,
                'article_categid'=>$donnee->article_categid,
                'article_subcategid'=>$donnee->article_subcategid,
                'activ_fb_comment'=>$donnee->activ_fb_comment,
                'source_id'=>$donnee->source_id,

            );
            $this->xml_models->save_to_true_sortez($field);
            $field2=array('titre'=>$donnee->nom_manifestation,
            );
            $this->xml_models->save_to_true_sortez_mine($field2);
            redirect('admin/xmls/saved');


        }
    }
    function save_all_id_selected(){
        $this->load->model('xml_models');

        $field =array(
            'article_categid'=>$this->input->post('categid'),
            'article_subcategid'=>$this->input->post('subcategid'));

        $this->xml_models->save_all_categ_checked($field);

        redirect('admin/xmls/saved');

        {







        }
    }
    function assign_categ(){
        $this->load->model('xml_models');
        $data['recordeds'] = $this->xml_models->afficher_liens();
        $user_ion_auth = $this->ion_auth->user()->row();
        $commercant_UserId = $this->ion_auth_used_by_club->get_commercant_id_from_ion_id($user_ion_auth->id);
        $data["userId"] = $commercant_UserId;


        $data["no_main_menu_home"] = "1";
        $data["no_left_menu_home"] = "1";


        $data["colVilles"] = $this->mdlville->GetAll();
        $data["objCommercant"] = $this->Commercant->GetById($commercant_UserId);
        $data["colCategorie"] = $this->mdl_categories_agenda->GetAll();
        $data["colSousCategorie"] = $this->mdl_categories_agenda->GetAllSousrubrique();
        $data['pagecategory'] = "admin_commercant";
        $data['records']=$this->xml_models->afficher_xml();
        $this->load->view('admin/Flux_rss/assign_categ',$data);

    }
    function save_categ_rss_source($ID){
        $this->load->model('xml_models');
        $this->load->helper('clubproximite');
        $data=$this->xml_models->getliens_by_id($ID);

        $xml = simplexml_load_file($this->input->post('src'));

//var_dump($xml->channel->item[0]);
//echo count($xml->channel->item);
        if (isset($xml->channel->item)){
            foreach($xml->channel->item as $donnee){
//var_dump($donnee->title);


                $field =array('nom_manifestation'=>(string)$donnee->title,
                    'siteweb'=>(string)$donnee->link,
                    'description'=>(string)$donnee->description,
                    'date_depot'=>convert_xml_date_to_sql_date($donnee->pubDate),
                    'source_id'=>$data->id);
//var_dump($field);

                $this->xml_models->save($field);



            }
            $field2 =array('article_id'=>(integer)$this->db->insert_id());
            $this->xml_models->save2($field2);
            $fieldz =array(
                'article_categid'=>$this->input->post('categid'),
                'article_subcategid'=>$this->input->post('subcategid'));
            // var_dump($fieldz);

            $this->xml_models->save_all_categ_src($fieldz);

            redirect(site_url('admin/xmls/sous_menu_rss'));

        }else{echo 'Mauvais lien';}

    }
    function redirect(){
        $test="jk";
        if (isset($test) AND $test=="jk"){
            redirect('admin/xmls/saved/');
        }
        else echo "error";

    }
}?>