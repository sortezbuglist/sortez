<!DOCTYPE html>
<html>
<head>
    <title>Le Magazine Sortez : L'essentiel des sorties et des loisirs Alpes-Maritimes et Monaco</title>
    <meta name="generator" content="Serif WebPlus X8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=1250">
    <meta name="keywords" content="animations, spectacles, concerts, théâtres, salons, musées, expositions, alpes-maritimes, nice, antibes, monaco, cannes, Théâtres, concerts, spectacles, animations, magazine, sortez, sortir, cannes, alpes-maritimes">
    <meta name="description" content="Sur les Alpes-Maritimes et Monaco, découvrez l'essentiel des sorties et des loisirs et bien plus encore !... animations, spectacles, concerts, théâtres, salons, musées, expositions...&#xD;&#xA;">
    <meta name="author" content="Priviconcept">
    <meta name="robots" content="index,follow">
    <link rel="stylesheet" type="text/css" href="wpscripts/wpstyles.css">
    <link rel="stylesheet" type="text/css" href="home/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="home/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="home/css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="home/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="home/css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="home/css/bootstrap-reboot.min.css">
    <link rel="icon" href="favicon.ico" type="image/x-icon" sizes="16x16 32x32 48x48 64x64">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" sizes="16x16 32x32 48x48 64x64">
    <link rel="apple-touch-icon" href="favicon.ico" sizes="60x60 76x76 120x120 152x152">
</head>
<body  style="background:url('http://www.sortez.org/wpimages/wp98f0f982_06.jpg') no-repeat fixed center top / cover transparent;">

    <div class="container">
            <div class="row" style="margin-top: 50px">
                <div class="col-lg-3 col-sm-12" style="font-size: 13px;text-align: center">
                   <a href="essai-index.html">ACCUEIL</a>
                </div>
                <div class="col-lg-3 col-sm-12" style="font-size: 13px;text-align: center">
                    LES EDITIONS DU MAGAZINE SORTEZ
                </div>
                <div class="col-lg-3 col-sm-12" style="font-size: 13px;text-align: center">
                    <a  href="annonce-infos.html" >
                        <span> INFORMATIONS&nbsp;&amp;&nbsp;SOUSCRIPTIONS</span>
                    </a>

                </div>
                <div class="col-lg-3 col-sm-12" style="font-size: 13px;text-align: center">
                   <a href="http://www.sortez.org/auth/login"> MON&nbsp;COMPTE</a>
                </div>
            </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script><br><br><br>
        <style type="text/css">
            #sortez_main_department_list {
                text-align: center;

            }
            @media screen and (min-width: 1000px){
            #sortez_main_department_list select {
                font-family: "Futura Md",sans-serif;
                color: #dc198f;
                font-size: 20px;
                padding: 15px 100px;
                border-radius: 10px;
                border: solid;
                cursor: pointer;
                margin-top: 100px!important;
                position: absolute;
                z-index: 5;
                display: block;
                margin: auto;
                margin-right: 28%;
                margin-left: 28%!important;
                font-family: "Futura Md",sans-serif;
                color: #dc198f;
                font-size: 20px;
                padding: 15px 100px;
                border-radius: 10px;
                border: solid;
                cursor: pointer;

            }
            }
            @media screen and (max-width: 1000px) {
                #sortez_main_department_list select{
                    font-family: "Futura Md",sans-serif;
                    color: #dc198f;
                    font-size: 20px;
                    padding: 15px 100px;
                    border-radius: 10px;
                    border: solid;
                    cursor: pointer;
                    margin-top: 50px!important;
                    position: absolute;
                    z-index: 5;
                    display: block;
                    margin: auto;
                    margin-right: 0px!important;
                    margin-left: 0px!important;
                    font-family: "Futura Md",sans-serif;
                    color: #dc198f;
                    font-size: 20px;
                    padding: 15px 100px;
                    border-radius: 10px;
                    border: solid;
                    cursor: pointer;
            }
            }
            @media screen and (min-width: 1000px){
            .nb_total_agenda_content_department{
                width: 228px!important;height: auto!important;z-index: 5;
                top:auto!important;
                 right: auto!important;
                margin-left: 65%!important;
                margin-top: -90px!important;
            }
            }
            @media screen and (max-width: 1000px){
                .nb_total_agenda_content_department{
                    width: 228px!important;height: auto!important;z-index: 5;
                    top:auto!important;
                    right: auto!important;
                    margin-left: 50%!important;
                    margin-top: -120px!important;
                }
            }
        </style>
<div class="col-lg-12 col-sm-12" id="sortez_main_department_list" style="display: block!important;margin: auto!important"></div>
        <script type="text/javascript" src="wpscripts/jquery.js"></script>
        <script type="text/javascript" src="wpscripts/jquery.wputils.js"></script>
        <script type="text/javascript" src="wpscripts/jquery.wplightbox.js"></script>
        <script type="text/javascript">
            //wpRedirectMinScreen(500,900,'https://www.sortez.org/mobile-index.html',0);
            $(document).ready(function() {
                $("a.ActiveButton").bind({ mousedown:function(){if ( $(this).attr('disabled') === undefined ) $(this).addClass('Activated');}, mouseleave:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}, mouseup:function(){ if ( $(this).attr('disabled') === undefined ) $(this).removeClass('Activated');}});
                $('.wplightbox').wplightbox(
                    {"loadBtnSrc":"wpimages/lightbox_load.gif","playBtnSrc":"wpimages/lightbox_play.png","playOverBtnSrc":"wpimages/lightbox_play_over.png","pauseBtnSrc":"wpimages/lightbox_pause.png","pauseOverBtnSrc":"wpimages/lightbox_pause_over.png","border_e":"wpimages/lightbox_e_6.png","border_n":"wpimages/lightbox_n_6.png","border_w":"wpimages/lightbox_w_6.png","border_s":"wpimages/lightbox_s_6.png","border_ne":"wpimages/lightbox_ne_6.png","border_se":"wpimages/lightbox_se_6.png","border_nw":"wpimages/lightbox_nw_6.png","border_sw":"wpimages/lightbox_sw_6.png","closeBtnSrc":"wpimages/lightbox_close_2.png","closeOverBtnSrc":"wpimages/lightbox_close_over_2.png","nextBtnSrc":"wpimages/lightbox_next_2.png","nextOverBtnSrc":"wpimages/lightbox_next_over_2.png","prevBtnSrc":"wpimages/lightbox_prev_2.png","prevOverBtnSrc":"wpimages/lightbox_prev_over_2.png","blankSrc":"wpscripts/blank.gif","bBkgrndClickable":true,"strBkgrndCol":"#000000","nBkgrndOpacity":0.5,"strContentCol":"#ffffff","nContentOpacity":0.8,"strCaptionCol":"#555555","nCaptionOpacity":1.0,"nCaptionType":1,"bCaptionCount":true,"strCaptionFontType":"Tahoma,Serif","strCaptionFontCol":"#ffffff","nCaptionFontSz":15,"bShowPlay":true,"bAnimateOpenClose":true,"nPlayPeriod":2000}
                );
                $.get( "https://www.sortez.org/agenda/main_department_list/", function( data ){
                    //alert( "Data Loaded: " + data );
                    $("#sortez_main_department_list").html(data);
                });
            });
        </script>
    <div class="col-lg-12 col-sm-12" style="display: block;margin: auto;height: auto;">
        <iframe id="ifrm_17" name="ifrm_17" src="https://www.sortez.org/bannieres/index/index.html" class="OBJ-1" style="z-index:-1; border:0;width: 100%;height:300px;margin-top: 50px"></iframe>
    </div>
        <br> <br> <br>
    <div class="row">
<div class="col-lg-12 col-sm-12" style="font-weight: bolder;font-size: 35px;text-align: center;line-height: 30px;margin-bottom: 10px">Découvrez l’agenda événementiel de votre département,  et pour certains territoires bien plus encore !…</div>
    <div class="col-lg-12 col-sm-12" style="margin-bottom: 80px">
        <p style="text-align: center;font-size: 25px;font-weight: bold;"><span class="C-1">SORTEZ.ORG EST UN MÉDIA FÉDÉRATEUR DÉPARTEMENTAL</span></p>
        <p style="margin-left: auto;
    margin-bottom: 10.7px;
    text-indent: -49.7px;
    line-height: 25.3px;
    font-family: Futura Md, sans-serif;
        font-style: normal;
        font-weight: normal;
        color: #000000;
        background-color: transparent;
        font-variant: normal;
        font-size: 18px;
        vertical-align: 0;" class="Corps P-13"><img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" >Sortez.org est une solution innovante, fédératrice, clés en main dédié à la diffusion de l’information institutionnelle et privée.</p>
        <p style="margin-left: auto;
    margin-bottom: 10.7px;
    text-indent: -49.7px;
    line-height: 25.3px;
    font-family: Futura Md, sans-serif;
        font-style: normal;
        font-weight: normal;
        color: #000000;
        background-color: transparent;
        font-variant: normal;
        font-size: 18px;
        vertical-align: 0;" class="Corps P-14"><img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" >Les acteurs inscrits (collectivités, associations, commerçants…) annoncent librement leurs actualités et leurs actions promotionnelles.</p>
        <p style="margin-left: auto;
    margin-bottom: 10.7px;
    text-indent: -49.7px;
    line-height: 25.3px;
    font-family: Futura Md, sans-serif;
        font-style: normal;
        font-weight: normal;
        color: #000000;
        background-color: transparent;
        font-variant: normal;
        font-size: 18px;
        vertical-align: 0;" class="Corps P-14"><img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" >Leurs données rejoignent leurs pages individuelles informatives et nos annuaires mutualisés avec des recherches multi-<wbr>critères.</p>
    </div>
        <div class="col-lg-4 col-sm-12">
            <img alt="" src="http://www.sortez.org/wpimages/wpb8376aee_06.png" style="display: block;margin: auto;"><br>
            <img alt="L’ANNUAIRE" src="http://www.sortez.org/wpimages/wpda6ae93c_06.png" style="display: block;margin: auto;"><br>
            <p style="font-family:Futura Md,sans-serif;font-weight: normal;font-weight: bold"><img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" >&nbsp;&nbsp;&nbsp;Vous avez accès à un annuaire qui référence nos partenaires (administrations, commerçants, artisans, associations, des adresses culturelles et patrimoniales..)…</p>
        </div>
        <div class="col-lg-4 col-sm-12">
            <img alt="" src="http://www.sortez.org/wpimages/wp0fa3c6e8_06.png" style="display: block;margin: auto;" ><br>
            <img alt="Les articles" src="http://www.sortez.org/wpimages/wpebda89c2_06.png" style="display: block;margin: auto;"><br>
            <p style="font-weight: bold"><img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26" >&nbsp;&nbsp;&nbsp;Vous avez accès aux articles de nos partenaires, de nos éditions en ligne et à certaines données venant de l’open data…</p>
        </div>
        <div class="col-lg-4 col-sm-12">
            <img alt="" src="http://www.sortez.org/wpimages/wp929b1b77_06.png" style="display: block;margin: auto;" ><br>
            <img alt="L’agenda événementiel" src="http://www.sortez.org/wpimages/wped9596d8_06.png" style="display: block;margin: auto;"><br>
                <p style="font-weight: bold"><img src="http://www.sortez.org/wpimages/wpf3887a2b_06.png" alt="" width="26" height="26">&nbsp;&nbsp;&nbsp;Vous avez accès aux événements de nos partenaires…</p>
            <span style="font-weight: bold">Vous trouverez également des données venant de l’open data….</span>
        </div>
            <div style="display: block;margin: auto;margin-top:100px;background-color: #DC1A95;" class="col-lg-4 col-sm-12">
                <br> <br>
                <img src="http://www.sortez.org/wpimages/wpf6015ae7_06.png" width="244" height="244"  style="display: block;margin: auto"><br>
                <p style="text-align: center;color: white;font-weight: 4px;font-size: 22px;font-family: Arial">LES BOUTIQUES EN LIGNE</p><br>
                <p style="text-align: center;color: white;font-weight: 3px;font-size: 18px;font-family: Arial"><img src="http://www.sortez.org/wpimages/wp68d4f3de_06.png" alt="" width="26" height="26" style="">&nbsp;&nbsp;Vous avez accès aux boutiques de nos partenaires…<br>
                    Certaines possèdent un module de règlement en ligne avec PAYPAL…<br></p>
            </div>
            <div style="display: block;margin: auto;margin-top:100px;background-color: #DC1A95" class="col-lg-4 col-sm-12">
                <br> <br>
                <img alt="" src="http://www.sortez.org/wpimages/wpffc3f0a2_06.png" style="display: block;margin: auto;"><br>
                <P style="text-align: center;color: white;font-weight: 4px;font-size: 22px;font-family: Arial">LES BONS PLANS</P><br>
                <p style="text-align: center;color: white;font-weight: 3px;font-size: 18px;font-family: Arial"><img src="http://www.sortez.org/wpimages/wp68d4f3de_06.png" alt="" width="26" height="26" style="">&nbsp;&nbsp;Découvrez des offres uniques, exceptionnelles,<br>
                    motivantes pour le consommateur…<br><br></p>
            </div>
            <div style="display: block;margin: auto;margin-top:100px;background-color: #DC1A95" class="col-lg-4 col-sm-12">
                <br> <br>
            <img alt="" src="http://www.sortez.org/wpimages/wpb8e69575_06.png" style="display: block;margin: auto;" ><br>
                <P style="text-align: center;color: white;font-weight: 4px;font-size: 22px;font-family: Arial">LA FIDELISATION</P><br>
                <p style="text-align: center;color: white;font-weight: 3px;font-size: 18px;font-family: Arial"><img src="http://www.sortez.org/wpimages/wp68d4f3de_06.png" alt="" width="26" height="26" style="">&nbsp;&nbsp;Découvrez des offres de fidélisation : des remises Cash, des offres de capitalisation ou coups de tampons…<br><br></p>
            </div>
        <div class="col-lg-12 col-sm-12" style="text-align: center;color: white;font-weight:bold;font-size: 25px;background-color:#DC1A95 "><br><br>POUR BÉNÉFICIER DES BONS PLANS ET DES CONDITIONS DE FIDÉLISATION,
            LE CONSOMMATEUR DOIT POSSÉDER LA CARTE PRIVILÈGE VIVRESAVILLE.FR
            ELLE EST DÉLIVRÉE GRATUITEMENT<br><br><br>
        </div >
        <div style="background:url('http://www.sortez.org/wpimages/wp9e6e204d_06.png') no-repeat;width: 100%;height: 100%">
            <div class="col-lg-6 col-sm-12" style="height:100%">
           <a href="https://www.sortez.org/front/particuliers/inscription"> <img src="home/srtr.png" style="display: block;margin:auto;margin-top: 50px;"></a>
                </div>
        </div>
        <div  class="col-lg-12 col-sm-12" style="background-color:#DC1A95 ">
            <p style="color:white;text-align: center;font-weight: 3px;font-size: 18px;font-family: Arial;margin-top: 50px"><img src="http://www.sortez.org/wpimages/wp68d4f3de_06.png" alt="" width="26" height="26" style="">Dès validation de votre compte, vous accédez immédiatement à une carte privilège dématérialisée et bénéficiez de la validation et de la gestion automatisée des bons plans et des conditions de fidélisation !</p><br>
            <div class="col-lg-4 col-sm-12" style="background-color: black;color: white;text-align: center;width:auto;height: auto;display: block;margin: auto;font-size: 20px"><a href="https://www.sortez.org/infos-consommateurs.html" style="color: white">PLUS D'INFORMATIONS</a></div>

        </div>
        <br><br><br><br><br><br><br><br><br>
        <div class="col-lg-12 col-sm-12" style="display: block;margin: auto;margin-top: 50px;text-align: center;">
           <p style="font-size: 35px;font-weight: bolder;line-height: 40px">
               COLLECTIVITÉS,ASSOCIATIONS, PROFESSIONNELS !...<br>
               DEVENEZ PARTENAIRE DE SORTEZ.ORG <br>
               ET BÉNÉFICIEZ D'OUTILS INNOVANTS, ADAPTÉS A VOTRE  ACTIVITÉS <br>POUR ATTIRER DE NOUVEAUX CONTACTS
           </p>
        </div>
        <div  class="col-lg-12 col-sm-12">
            <p style="font-family: 'Futura Md', sans-serif;font-weight:normal"><img src="home/point.png"> &nbsp;Les partenaires disposent d’une multitude d’outils de communication innovants, clés en main, personnels ou mutualisés, gratuits ou à moindre coût dont certains sont habituellement utilisés par les grandes enseignes.</p>
        </div>
        <div class="col-lg-3 col-sm-12" style="display: block;margin: auto;">
            <img src="home/vous.png" style="display: block;margin: auto"><br>
            <p style="text-align: center;font-size: 25px;font-weight: bold">VOUS ETES<br> UNE<br> COLLECTIVITÉ</p>
            <button  style="background-color: black;color: white;display: block;margin: auto;height: 50px;font-family:Serif"><a href="https://www.sortez.org/collectivites.html" style="color: white">PLUS D'INFOS</a></button>

        </div>
        <div class="col-lg-3 col-sm-12" style="display: block;margin: auto;">
            <img src="home/assoc.png" style="display: block;margin: auto">
            <p style="text-align: center;font-size: 25px;font-weight: bold;margin-bottom: 35px;">VOUS ETES<br> UNE<br> ASSOCIATION</p>
            <button  style="background-color: black;color: white;display: block;margin: auto;height: 50px;font-family:Serif"><a href="https://www.sortez.org/association.html" style="color: white">PLUS D'INFOS</a></button>
        </div>
        <div class="col-lg-3 col-sm-12" style="display: block;margin: auto;">
            <img src="home/org.png" style="display: block;margin: auto">
            <P style="text-align: center;font-size: 25px;font-weight: bold;margin-bottom: 35px">VOUS ETES UNE ORGANISATEUR DE SPECTACLES</P>
            <button  style="background-color: black;color: white;display: block;margin: auto;height: 50px;font-family:Serif"><a href="https://www.sortez.org/theatre.html" style="color: white">PLUS D'INFOS</a></button>
        </div>
        <div class="col-lg-3 col-sm-12" style="display: block;margin: auto;">
            <img src="home/prof.png" style="display: block;margin: auto">
            <P style="text-align: center;font-size: 25px;font-weight: bold;display: block;margin: auto;margin-bottom: 35px">VOUS ETES<br>UNE PROFESSIONNEL</P>
            <button style="background-color: black;color: white;display: block;margin: auto;height: 50px;font-family:Serif"><a href="https://www.sortez.org/professionnels.html" style="color: white">PLUS D'INFOS</a></button>
        </div>
        <div class="row" style="background:url('home/grd.jpg') no-repeat;margin: auto;margin-top: 50px;font-size: 15px;font-weight: bold;color: white;text-align: center">
<div class="col-lg-3 col-sm-12"><img src="home/grat.png" style="margin-top: 50px;margin-bottom: 40px"></div>
          <div id="grt" class="col-lg-6 col-sm-12" style="margin-top: 60px;font-size: 25px;"> COLLECTIVITÉS, ASSOCIATIONS, PROFESSIONNELS
AVEC NOTRE PACK DE DÉMARRAGE
DÉMARREZ ET TESTEZ GRATUITEMENT NOTRE CONCEPT
          <br><br>
              <style type="text/css">
              @media screen and (max-width: 1000px){
              #grt{
                  background:url('home/grd.jpg') no-repeat;
              }
              }
              </style>
              <button style="background-color: black;color: white;display: block;margin: auto;height: 50px;"><a style="color: white;font-family:Serif" href="https://www.sortez.org/infos-pro.html">PLUS D'INFOS</a></button></div>
        </div>
<div class="col-lg-12 col-sm-12" style="background-color: #84DBF7;height: 100%;display: block;margin: auto;margin-top: ">
    <img src="home/mag.png" style="display: block;margin: auto;margin-top: 50px;"><br>
    <div class="col-lg-8 col-sm-12" style="text-align: center;font-size: 25px;display: block;margin: auto;font-weight: bolder">SUR CERTAINS DÉPARTEMENTS, NOTRE RÉDACTION
        PUBLIE ET DIFFUSE UN MAGAZINE MENSUEL ET GRATUIT
        SUR L’ESSENTIEL DES SORTIES ET DES LOISIRS</div>
    <div class="row">
        <div class="col-lg-4 col-sm-12" style="margin-top: 50px;display: block;margin: auto;"><img src="home/book.png"></div>
        <div class="col-lg-4 col-sm-12" style="margin-top: 50px;display: block;margin: auto;"><button  style="background-color: black;color: white;display: block;margin: auto;height: 50px"><a style="color: white;font-family:Serif" href="https://www.sortez.org/france-editions.html">ACCES AUX EDITIONS</a></button></div>
        <div class="col-lg-4 col-sm-12" style="margin-top: 50px;display: block;margin: auto;"><img src="home/loop.png"></div>
    </div>
</div>
        <div >
            <img style=";margin: auto;width:100%" src="home/footer.png">
        </div>
        <div  class="col-lg-12 col-sm-12"  style="background-color: #DC1A95;color: white;font-weight: normal;width: 100%;padding-top: 25px;">
           <SPAN style="font-size: 20px;">PARTAGER</SPAN>&nbsp;&nbsp;<a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//sortez.org"><img src="home/fb.png"></a>
               &nbsp;&nbsp;<a href="https://twitter.com/login?redirect_after_login=%2Fhome%3Fstatus%3Dhttp%253A%2F%2Fsortez.org"><img src="home/twit.png"></a>
            <a href="https://accounts.google.com/ServiceLogin?service=oz&passive=1209600&continue=https://plus.google.com/share?url%3Dhttp://privicarte.fr%26gpsrc%3Dgplp0&btmpl=popup#identifier"><img src="home/google.png"></a>
            <a href="http://sortez.org/portail/contact"><img src="home/msg.png"></a>
            <a href="https://www.sortez.org/site/mentions-legales.html"><img src="home/ment.png"></a><a style="font-size: 20px;margin-left;color: white" href="https://www.sortez.org/site/mentions-legales.html">MENTIONS LEGALES</a>
        </div>
        <div style="background-color: black" class="col-lg-12 col-sm-12"  >
<img style="width: 100%;margin-left: auto;" src="home/copyright.png">
        </div>
    </div>
    </div>
</body>
</html>