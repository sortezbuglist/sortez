-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 24 jan. 2019 à 09:50
-- Version du serveur :  5.7.21
-- Version de PHP :  7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sortez`
--

-- --------------------------------------------------------

--
-- Structure de la table `glissieres`
--

DROP TABLE IF EXISTS `glissieres`;
CREATE TABLE IF NOT EXISTS `glissieres` (
  `id_glissiere` int(11) NOT NULL AUTO_INCREMENT,
  `IdCommercant` int(11) DEFAULT NULL,
  `id_ionauth` int(11) DEFAULT NULL,
  `presentation_1_titre` varchar(255) DEFAULT NULL,
  `presentation_1_contenu1` text,
  `presentation_2_titre` varchar(255) DEFAULT NULL,
  `presentation_2_contenu1` text,
  `page1_1_titre` varchar(255) DEFAULT NULL,
  `page1_1_contenu1` text,
  `page1_2_titre` varchar(255) DEFAULT NULL,
  `page1_2_contenu1` text,
  `page2_1_titre` varchar(255) DEFAULT NULL,
  `page2_1_contenu1` text,
  `page2_2_titre` varchar(255) DEFAULT NULL,
  `page2_2_contenu1` text,
  `isActive_presentation_1` int(1) DEFAULT NULL,
  `isActive_presentation_2` int(1) DEFAULT NULL,
  `isActive_presentation_3` int(1) DEFAULT NULL,
  `isActive_presentation_4` int(1) DEFAULT NULL,
  `isActive_page1_1` int(1) DEFAULT NULL,
  `isActive_page1_2` int(1) DEFAULT NULL,
  `isActive_page1_3` int(1) DEFAULT NULL,
  `isActive_page1_4` int(1) DEFAULT NULL,
  `isActive_page2_1` int(1) DEFAULT NULL,
  `isActive_page2_2` int(1) DEFAULT NULL,
  `isActive_page2_3` int(1) DEFAULT NULL,
  `isActive_page2_4` int(1) DEFAULT NULL,
  `presentation_1_image_1` varchar(255) DEFAULT NULL,
  `presentation_1_image_2` varchar(255) DEFAULT NULL,
  `presentation_1_image_3` varchar(255) DEFAULT NULL,
  `presentation_1_image_4` varchar(255) DEFAULT NULL,
  `presentation_2_image_1` varchar(255) DEFAULT NULL,
  `presentation_2_image_2` varchar(255) DEFAULT NULL,
  `presentation_2_image_3` varchar(255) DEFAULT NULL,
  `presentation_2_image_4` varchar(255) DEFAULT NULL,
  `page_1_image_1` varchar(255) DEFAULT NULL,
  `page_1_image_2` varchar(255) DEFAULT NULL,
  `page_1_image_3` varchar(255) DEFAULT NULL,
  `page_1_image_4` varchar(255) DEFAULT NULL,
  `page_2_image_1` varchar(255) DEFAULT NULL,
  `page_2_image_2` varchar(255) DEFAULT NULL,
  `page_2_image_3` varchar(255) DEFAULT NULL,
  `page_2_image_4` varchar(255) DEFAULT NULL,
  `page2_1_image_1` varchar(255) DEFAULT NULL,
  `page2_1_image_2` varchar(255) DEFAULT NULL,
  `page2_1_image_3` varchar(255) DEFAULT NULL,
  `page2_1_image_4` varchar(255) DEFAULT NULL,
  `page2_2_image_1` varchar(255) DEFAULT NULL,
  `page2_2_image_2` varchar(255) DEFAULT NULL,
  `page2_2_image_3` varchar(255) DEFAULT NULL,
  `page2_2_image_4` varchar(255) DEFAULT NULL,
  `page2_3_image_1` varchar(255) DEFAULT NULL,
  `page2_3_image_2` varchar(255) DEFAULT NULL,
  `page2_3_image_3` varchar(255) DEFAULT NULL,
  `page2_3_image_4` varchar(255) DEFAULT NULL,
  `page2_4_image_1` varchar(255) DEFAULT NULL,
  `page2_4_image_2` varchar(255) DEFAULT NULL,
  `page2_4_image_3` varchar(255) DEFAULT NULL,
  `page2_4_image_4` varchar(255) DEFAULT NULL,
  `presentation_1_contenu2` text,
  `presentation_1_contenu3` text,
  `presentation_1_contenu4` text,
  `presentation_2_contenu2` text,
  `presentation_2_contenu3` text,
  `presentation_2_contenu4` text,
  `page1_1_contenu2` text,
  `page1_1_contenu3` text,
  `page1_1_contenu4` text,
  `page1_2_contenu2` text,
  `page1_2_contenu3` text,
  `page1_2_contenu4` text,
  `page1_3_contenu2` text,
  `page1_3_contenu3` text,
  `page1_3_contenu4` text,
  `page1_4_contenu2` text,
  `page1_4_contenu3` text,
  `page1_4_contenu4` text,
  `page2_1_contenu2` text,
  `page2_1_contenu3` text,
  `page2_1_contenu4` text,
  `page2_2_contenu2` text,
  `page2_2_contenu3` text,
  `page2_2_contenu4` text,
  `is_activ_btn_glissiere1_champ1` decimal(1,0) DEFAULT NULL,
  `is_activ_btn_glissiere1_champ2` decimal(1,0) DEFAULT NULL,
  `presentation_3_titre` varchar(255) DEFAULT NULL,
  `presentation_3_contenu1` text,
  `presentation_3_contenu2` text,
  `presentation_4_contenu1` text,
  `presentation_4_contenu2` text,
  `presentation_4_titre` varchar(255) DEFAULT NULL,
  `page1_3_titre` varchar(255) DEFAULT NULL,
  `page2_3_titre` varchar(255) DEFAULT NULL,
  `page1_3_contenu1` text,
  `page1_4_titre` varchar(255) DEFAULT NULL,
  `page1_4_contenu1` text,
  `page2_3_contenu1` text,
  `page2_3_contenu2` text,
  `page2_4_titre` varchar(255) DEFAULT NULL,
  `page2_4_contenu1` text,
  `page2_4_contenu2` text,
  `btn_gli1_content1` varchar(255) DEFAULT NULL,
  `gl1_custom_link1` varchar(255) DEFAULT NULL,
  `gl1_existed_link1` varchar(255) DEFAULT NULL,
  `gl1_existed_link2` varchar(255) DEFAULT NULL,
  `gl1_custom_link2` varchar(255) DEFAULT NULL,
  `btn_gli1_content2` varchar(255) DEFAULT NULL,
  `bloc_1_image1_content` varchar(255) DEFAULT NULL,
  `bloc1_content` text,
  `bloc1_custom_link1` varchar(255) DEFAULT NULL,
  `bloc1_existed_link1` varchar(255) DEFAULT NULL,
  `btn_bloc1_content1` varchar(255) DEFAULT NULL,
  `is_activ_btn_bloc1` varchar(255) DEFAULT NULL,
  `bloc_1_image2_content` varchar(255) DEFAULT NULL,
  `bloc1_content2` text,
  `bloc1_custom_link2` varchar(255) DEFAULT NULL,
  `bloc1_existed_link2` varchar(255) DEFAULT NULL,
  `btn_bloc1_content2` varchar(255) DEFAULT NULL,
  `is_activ_btn_bloc2` varchar(255) DEFAULT NULL,
  `type_bloc` varchar(255) DEFAULT NULL,
  `is_activ_bloc` varchar(255) DEFAULT NULL,
  `bonplan_id` varchar(11) DEFAULT '',
  `bonplan_content` text,
  `fidelity_id` int(11) DEFAULT NULL,
  `fidelity_content` text,
  `is_activ_btn_fd` varchar(255) DEFAULT NULL,
  `FD_existed_link` varchar(255) DEFAULT NULL,
  `fd_custom_link` varchar(255) DEFAULT NULL,
  `btn_fd_content` varchar(255) DEFAULT NULL,
  `is_activ_btn_bp` varchar(255) DEFAULT NULL,
  `bp_existed_link` varchar(255) DEFAULT NULL,
  `bp_custom_link` varchar(255) DEFAULT NULL,
  `btn_bp_content` varchar(255) DEFAULT NULL,
  `place_logo` varchar(1) DEFAULT NULL,
  `is_activ_btn_glissiere1_page1_champ1` decimal(2,0) DEFAULT NULL,
  `btn_gli1_p1_content1` varchar(255) DEFAULT NULL,
  `gl1_p1_custom_link1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere1_page1_champ2` decimal(2,0) DEFAULT NULL,
  `gl1_existed_p1_link1` varchar(255) DEFAULT NULL,
  `gl1_existed_p1_link2` varchar(255) DEFAULT NULL,
  `gl1_p1_custom_link2` varchar(255) DEFAULT NULL,
  `btn_gli1_p1_content2` varchar(255) DEFAULT NULL,
  `nombre_blick_gli_p1` varchar(10) DEFAULT NULL,
  `nombre_blick_gli_presentation` decimal(10,0) DEFAULT NULL,
  `nombre_blick_gli_p2` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere1_page2_champ1` varchar(255) DEFAULT NULL,
  `btn_gli1_p2_content1` varchar(255) DEFAULT NULL,
  `gl1_existed_p2_link1` varchar(255) DEFAULT NULL,
  `gl1_p2_custom_link1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere1_page2_champ2` varchar(255) DEFAULT NULL,
  `btn_gli1_p2_content2` varchar(255) DEFAULT NULL,
  `gl1_existed_p2_link2` varchar(100) DEFAULT NULL,
  `gl1_p2_custom_link2` varchar(100) DEFAULT NULL,
  `nombre_blick_gli_presentation2` varchar(2) DEFAULT NULL,
  `gl2_existed_link2` varchar(100) DEFAULT NULL,
  `gl2_custom_link2` varchar(100) DEFAULT NULL,
  `btn_gli2_content1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere2_champ1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere2_champ2` varchar(255) DEFAULT NULL,
  `btn_gli2_content2` varchar(255) DEFAULT NULL,
  `gl2_existed_link1` varchar(255) DEFAULT NULL,
  `nombre_blick_gli_presentation3` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere3_champ1` varchar(255) DEFAULT NULL,
  `btn_gli3_content1` varchar(255) DEFAULT NULL,
  `gl3_existed_link1` varchar(255) DEFAULT NULL,
  `gl3_custom_link1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere3_champ2` varchar(255) DEFAULT NULL,
  `btn_gli3_content2` varchar(255) DEFAULT NULL,
  `gl3_existed_link2` varchar(255) DEFAULT NULL,
  `gl3_custom_link2` varchar(255) DEFAULT NULL,
  `nombre_blick_gli_presentation4` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere4_champ1` varchar(255) DEFAULT NULL,
  `btn_gli4_content1` varchar(255) DEFAULT NULL,
  `gl4_existed_link1` varchar(255) DEFAULT NULL,
  `gl4_custom_link1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere4_champ2` varchar(255) DEFAULT NULL,
  `btn_gli4_content2` varchar(255) DEFAULT NULL,
  `gl4_existed_link2` varchar(255) DEFAULT NULL,
  `gl4_custom_link2` varchar(255) DEFAULT NULL,
  `nombre_blick_gli2_p1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere2_page1_champ1` varchar(255) DEFAULT NULL,
  `btn_gli2_p1_content1` varchar(255) DEFAULT NULL,
  `gl2_existed_p1_link1` varchar(255) DEFAULT NULL,
  `gl2_p1_custom_link1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere2_page1_champ2` varchar(255) DEFAULT NULL,
  `btn_gli2_p1_content2` varchar(255) DEFAULT NULL,
  `gl2_existed_p1_link2` varchar(255) DEFAULT NULL,
  `gl2_p1_custom_link2` varchar(255) DEFAULT NULL,
  `nombre_blick_gli_page1_gli3` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere3_champ1_p1` varchar(255) DEFAULT NULL,
  `btn_gli3_content1_p1` varchar(255) DEFAULT NULL,
  `gl3_existed_link1_p1` varchar(255) DEFAULT NULL,
  `gl3_custom_link1_p1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere3_champ2_p1` varchar(255) DEFAULT NULL,
  `btn_gli3_content2_p1` varchar(255) DEFAULT NULL,
  `gl3_existed_link2_p1` varchar(255) DEFAULT NULL,
  `gl3_custom_link2_p1` varchar(255) DEFAULT NULL,
  `nombre_blick_gli_page1_gli4` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere4_champ1_p1` varchar(255) DEFAULT NULL,
  `btn_gli4_content1_p1` varchar(255) DEFAULT NULL,
  `gl4_existed_link1_p1` varchar(255) DEFAULT NULL,
  `gl4_custom_link1_p1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere4_champ2_p1` varchar(255) DEFAULT NULL,
  `btn_gli4_content2_p1` varchar(255) DEFAULT NULL,
  `gl4_existed_link2_p1` varchar(255) DEFAULT NULL,
  `gl4_custom_link2_p1` varchar(255) DEFAULT NULL,
  `nombre_blick_gli2_p2` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere2_page2_champ2` varchar(255) DEFAULT NULL,
  `btn_gli2_p2_content1` varchar(255) DEFAULT NULL,
  `gl2_existed_p2_link1` varchar(255) DEFAULT NULL,
  `gl2_p2_custom_link1` varchar(255) DEFAULT NULL,
  `btn_gli2_p2_content2` varchar(255) DEFAULT NULL,
  `gl2_existed_p2_link2` varchar(255) DEFAULT NULL,
  `gl2_p2_custom_link2` varchar(255) DEFAULT NULL,
  `nombre_blick_gli_page2_gli3` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere3_champ2_p2` varchar(255) DEFAULT NULL,
  `btn_gli3_content1_p2` varchar(255) DEFAULT NULL,
  `gl3_existed_link1_p2` varchar(255) DEFAULT NULL,
  `gl3_custom_link1_p2` varchar(255) DEFAULT NULL,
  `btn_gli3_content2_p2` varchar(255) DEFAULT NULL,
  `gl3_existed_link2_p2` varchar(255) DEFAULT NULL,
  `gl3_custom_link2_p2` varchar(255) DEFAULT NULL,
  `nombre_blick_gli_page2_gli4` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere4_champ2_p2` varchar(255) DEFAULT NULL,
  `btn_gli4_content1_p2` varchar(255) DEFAULT NULL,
  `gl4_existed_link1_p2` varchar(255) DEFAULT NULL,
  `gl4_custom_link1_p2` varchar(255) DEFAULT NULL,
  `btn_gli4_content2_p2` varchar(255) DEFAULT NULL,
  `gl4_existed_link2_p2` varchar(255) DEFAULT NULL,
  `gl4_custom_link2_p2` varchar(255) DEFAULT NULL,
  `gl2_custom_link1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere2_page2_champ1` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere3_champ1_p2` varchar(255) DEFAULT NULL,
  `is_activ_btn_glissiere4_champ1_p2` varchar(255) DEFAULT NULL,
  `is_activ_bloc2` varchar(255) DEFAULT NULL,
  `type_bloc2` varchar(255) DEFAULT NULL,
  `bloc1_2_content` text,
  `is_activ_btn_bloc1_2` varchar(255) DEFAULT NULL,
  `btn_bloc1_2_content1` varchar(255) DEFAULT NULL,
  `bloc1_2_existed_link1` varchar(255) DEFAULT NULL,
  `bloc1_2_custom_link1` varchar(255) DEFAULT NULL,
  `bloc1_2_content2` text,
  `is_activ_btn_bloc2_2` varchar(255) DEFAULT NULL,
  `btn_bloc1_2_content2` varchar(255) DEFAULT NULL,
  `bloc1_2_existed_link2` varchar(255) DEFAULT NULL,
  `bloc1_2_custom_link2` varchar(255) DEFAULT NULL,
  `bonplan_id_2` varchar(255) DEFAULT NULL,
  `bonplan_content_2` text,
  `is_activ_btn_bp_2` varchar(255) DEFAULT NULL,
  `btn_bp_content_2` varchar(255) DEFAULT NULL,
  `bp_existed_link_2` varchar(255) DEFAULT NULL,
  `bp_custom_link_2` varchar(255) DEFAULT NULL,
  `fidelity_2_id` int(11) DEFAULT NULL,
  `fidelity_2_content` text,
  `is_activ_btn_fd_2` varchar(255) DEFAULT NULL,
  `btn_fd_2_content` varchar(255) DEFAULT NULL,
  `FD_2_existed_link` varchar(255) DEFAULT NULL,
  `fd_custom_2_link` varchar(255) DEFAULT NULL,
  `is_activ_bloc3` varchar(255) DEFAULT NULL,
  `type_bloc3` varchar(255) DEFAULT NULL,
  `bloc1_3_content` text,
  `is_activ_btn_bloc1_3` varchar(255) DEFAULT NULL,
  `btn_bloc1_3_content1` varchar(255) DEFAULT NULL,
  `bloc1_3_existed_link1` varchar(255) DEFAULT NULL,
  `bloc1_3_custom_link1` varchar(255) DEFAULT NULL,
  `bloc1_3_content2` text,
  `is_activ_btn_bloc2_3` varchar(255) DEFAULT NULL,
  `btn_bloc1_3_content2` varchar(255) DEFAULT NULL,
  `bloc1_3_existed_link2` varchar(255) DEFAULT NULL,
  `bloc1_3_custom_link2` varchar(255) DEFAULT NULL,
  `bonplan_id_3` varchar(255) DEFAULT NULL,
  `bonplan_content_3` text,
  `is_activ_btn_bp_3` int(10) DEFAULT NULL,
  `btn_bp_content_3` varchar(255) DEFAULT NULL,
  `bp_existed_link_3` varchar(10) DEFAULT NULL,
  `bp_custom_link_3` varchar(255) DEFAULT NULL,
  `fidelity_3_id` int(11) DEFAULT NULL,
  `bloc1_4_custom_link1` varchar(100) DEFAULT NULL,
  `is_activ_btn_fd_3` int(10) DEFAULT NULL,
  `btn_fd_3_content` varchar(255) DEFAULT NULL,
  `FD_3_existed_link` varchar(255) DEFAULT NULL,
  `fd_custom_3_link` varchar(255) DEFAULT NULL,
  `is_activ_bloc4` int(10) DEFAULT NULL,
  `type_bloc4` varchar(50) DEFAULT NULL,
  `bloc1_4_content` text,
  `btn_bloc1_4_content1` varchar(50) DEFAULT NULL,
  `is_activ_btn_bloc1_4` varchar(10) DEFAULT NULL,
  `bloc1_4_existed_link1` varchar(10) CHARACTER SET koi8u DEFAULT NULL,
  `fidelity_3_content` text,
  `bloc1_4_content2` text,
  `is_activ_btn_bloc2_4` int(10) DEFAULT NULL,
  `btn_bloc1_4_content2` varchar(100) DEFAULT NULL,
  `bloc1_4_existed_link2` varchar(10) DEFAULT NULL,
  `bloc1_4_custom_link2` varchar(100) DEFAULT NULL,
  `bonplan_id_4` varchar(10) DEFAULT NULL,
  `bonplan_content_4` text,
  `is_activ_btn_bp_4` int(3) DEFAULT NULL,
  `btn_bp_content_4` varchar(100) DEFAULT NULL,
  `bp_existed_link_4` varchar(10) DEFAULT NULL,
  `bp_custom_link_4` varchar(100) DEFAULT NULL,
  `fidelity_4_id` int(3) DEFAULT NULL,
  `fidelity_4_content` text,
  `is_activ_btn_fd_4` int(10) DEFAULT NULL,
  `btn_fd_4_content` varchar(50) DEFAULT NULL,
  `FD_4_existed_link` varchar(10) DEFAULT NULL,
  `fd_custom_4_link` varchar(100) DEFAULT NULL,
  `presentation_3_image_1` varchar(100) DEFAULT NULL,
  `presentation_3_image_2` varchar(100) DEFAULT NULL,
  `presentation_4_image_1` varchar(50) DEFAULT NULL,
  `presentation_4_image_2` varchar(50) DEFAULT NULL,
  `bloc_1_2_image1_content` varchar(50) DEFAULT NULL,
  `bloc_1_4_image1_content` varchar(50) DEFAULT NULL,
  `bloc_1_4_image2_content` varchar(50) DEFAULT NULL,
  `bloc_1_3_image2_content` varchar(50) DEFAULT NULL,
  `bloc_1_3_image1_content` varchar(50) DEFAULT NULL,
  `bloc_1_2_image2_content` varchar(50) DEFAULT NULL,
  `couleurLogo` varchar(2) DEFAULT NULL,
  `page1_3_image_1` varchar(100) DEFAULT NULL,
  `page1_3_image_2` varchar(100) DEFAULT NULL,
  `page1_4_image_1` varchar(100) DEFAULT NULL,
  `page1_4_image_2` varchar(100) DEFAULT NULL,
  `couleurTexteMenu` varchar(1) DEFAULT NULL,
  `affichageNomCommercant` varchar(1) DEFAULT NULL,
  `Transparent_du_banniere` float DEFAULT NULL,
  `bloc1_islightbox` varchar(1) DEFAULT NULL,
  `bloc1_islightbox2` int(2) DEFAULT NULL,
  `bloc1_islightboxbp1` int(2) DEFAULT NULL,
  `bloc1_islightboxfd1` int(2) DEFAULT NULL,
  `bloc2_islightbox` int(2) DEFAULT NULL,
  `bloc2_islightbox2` int(2) DEFAULT NULL,
  `bloc2_islightboxbp` int(2) DEFAULT NULL,
  `bloc2_islightboxfd` int(2) DEFAULT NULL,
  `bloc3_islightbox` int(2) DEFAULT NULL,
  `bloc3_islightbox2` int(2) DEFAULT NULL,
  `bloc3_islightboxbp` int(2) DEFAULT NULL,
  `bloc3_islightboxfd` int(2) DEFAULT NULL,
  `bloc4_islightbox` int(2) DEFAULT NULL,
  `bloc4_islightbox2` int(2) DEFAULT NULL,
  `bloc4_islightboxbp` int(2) DEFAULT NULL,
  `bloc4_islightboxfd` int(2) DEFAULT NULL,
  `gli1_islightbox` int(2) DEFAULT NULL,
  `gli1_islightbox2` int(2) DEFAULT NULL,
  `gli2_islightbox` int(2) DEFAULT NULL,
  `gli2_islightbox2` int(2) DEFAULT NULL,
  `gli3_islightbox` int(2) DEFAULT NULL,
  `gli3_islightbox2` int(2) DEFAULT NULL,
  `gli4_islightbox` int(2) DEFAULT NULL,
  `gli4_islightbox2` int(2) DEFAULT NULL,
  `is_actif_livre_or` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_glissiere`)
) ENGINE=MyISAM AUTO_INCREMENT=383 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Déchargement des données de la table `glissieres`
--

INSERT INTO `glissieres` (`id_glissiere`, `IdCommercant`, `id_ionauth`, `presentation_1_titre`, `presentation_1_contenu1`, `presentation_2_titre`, `presentation_2_contenu1`, `page1_1_titre`, `page1_1_contenu1`, `page1_2_titre`, `page1_2_contenu1`, `page2_1_titre`, `page2_1_contenu1`, `page2_2_titre`, `page2_2_contenu1`, `isActive_presentation_1`, `isActive_presentation_2`, `isActive_presentation_3`, `isActive_presentation_4`, `isActive_page1_1`, `isActive_page1_2`, `isActive_page1_3`, `isActive_page1_4`, `isActive_page2_1`, `isActive_page2_2`, `isActive_page2_3`, `isActive_page2_4`, `presentation_1_image_1`, `presentation_1_image_2`, `presentation_1_image_3`, `presentation_1_image_4`, `presentation_2_image_1`, `presentation_2_image_2`, `presentation_2_image_3`, `presentation_2_image_4`, `page_1_image_1`, `page_1_image_2`, `page_1_image_3`, `page_1_image_4`, `page_2_image_1`, `page_2_image_2`, `page_2_image_3`, `page_2_image_4`, `page2_1_image_1`, `page2_1_image_2`, `page2_1_image_3`, `page2_1_image_4`, `page2_2_image_1`, `page2_2_image_2`, `page2_2_image_3`, `page2_2_image_4`, `page2_3_image_1`, `page2_3_image_2`, `page2_3_image_3`, `page2_3_image_4`, `page2_4_image_1`, `page2_4_image_2`, `page2_4_image_3`, `page2_4_image_4`, `presentation_1_contenu2`, `presentation_1_contenu3`, `presentation_1_contenu4`, `presentation_2_contenu2`, `presentation_2_contenu3`, `presentation_2_contenu4`, `page1_1_contenu2`, `page1_1_contenu3`, `page1_1_contenu4`, `page1_2_contenu2`, `page1_2_contenu3`, `page1_2_contenu4`, `page1_3_contenu2`, `page1_3_contenu3`, `page1_3_contenu4`, `page1_4_contenu2`, `page1_4_contenu3`, `page1_4_contenu4`, `page2_1_contenu2`, `page2_1_contenu3`, `page2_1_contenu4`, `page2_2_contenu2`, `page2_2_contenu3`, `page2_2_contenu4`, `is_activ_btn_glissiere1_champ1`, `is_activ_btn_glissiere1_champ2`, `presentation_3_titre`, `presentation_3_contenu1`, `presentation_3_contenu2`, `presentation_4_contenu1`, `presentation_4_contenu2`, `presentation_4_titre`, `page1_3_titre`, `page2_3_titre`, `page1_3_contenu1`, `page1_4_titre`, `page1_4_contenu1`, `page2_3_contenu1`, `page2_3_contenu2`, `page2_4_titre`, `page2_4_contenu1`, `page2_4_contenu2`, `btn_gli1_content1`, `gl1_custom_link1`, `gl1_existed_link1`, `gl1_existed_link2`, `gl1_custom_link2`, `btn_gli1_content2`, `bloc_1_image1_content`, `bloc1_content`, `bloc1_custom_link1`, `bloc1_existed_link1`, `btn_bloc1_content1`, `is_activ_btn_bloc1`, `bloc_1_image2_content`, `bloc1_content2`, `bloc1_custom_link2`, `bloc1_existed_link2`, `btn_bloc1_content2`, `is_activ_btn_bloc2`, `type_bloc`, `is_activ_bloc`, `bonplan_id`, `bonplan_content`, `fidelity_id`, `fidelity_content`, `is_activ_btn_fd`, `FD_existed_link`, `fd_custom_link`, `btn_fd_content`, `is_activ_btn_bp`, `bp_existed_link`, `bp_custom_link`, `btn_bp_content`, `place_logo`, `is_activ_btn_glissiere1_page1_champ1`, `btn_gli1_p1_content1`, `gl1_p1_custom_link1`, `is_activ_btn_glissiere1_page1_champ2`, `gl1_existed_p1_link1`, `gl1_existed_p1_link2`, `gl1_p1_custom_link2`, `btn_gli1_p1_content2`, `nombre_blick_gli_p1`, `nombre_blick_gli_presentation`, `nombre_blick_gli_p2`, `is_activ_btn_glissiere1_page2_champ1`, `btn_gli1_p2_content1`, `gl1_existed_p2_link1`, `gl1_p2_custom_link1`, `is_activ_btn_glissiere1_page2_champ2`, `btn_gli1_p2_content2`, `gl1_existed_p2_link2`, `gl1_p2_custom_link2`, `nombre_blick_gli_presentation2`, `gl2_existed_link2`, `gl2_custom_link2`, `btn_gli2_content1`, `is_activ_btn_glissiere2_champ1`, `is_activ_btn_glissiere2_champ2`, `btn_gli2_content2`, `gl2_existed_link1`, `nombre_blick_gli_presentation3`, `is_activ_btn_glissiere3_champ1`, `btn_gli3_content1`, `gl3_existed_link1`, `gl3_custom_link1`, `is_activ_btn_glissiere3_champ2`, `btn_gli3_content2`, `gl3_existed_link2`, `gl3_custom_link2`, `nombre_blick_gli_presentation4`, `is_activ_btn_glissiere4_champ1`, `btn_gli4_content1`, `gl4_existed_link1`, `gl4_custom_link1`, `is_activ_btn_glissiere4_champ2`, `btn_gli4_content2`, `gl4_existed_link2`, `gl4_custom_link2`, `nombre_blick_gli2_p1`, `is_activ_btn_glissiere2_page1_champ1`, `btn_gli2_p1_content1`, `gl2_existed_p1_link1`, `gl2_p1_custom_link1`, `is_activ_btn_glissiere2_page1_champ2`, `btn_gli2_p1_content2`, `gl2_existed_p1_link2`, `gl2_p1_custom_link2`, `nombre_blick_gli_page1_gli3`, `is_activ_btn_glissiere3_champ1_p1`, `btn_gli3_content1_p1`, `gl3_existed_link1_p1`, `gl3_custom_link1_p1`, `is_activ_btn_glissiere3_champ2_p1`, `btn_gli3_content2_p1`, `gl3_existed_link2_p1`, `gl3_custom_link2_p1`, `nombre_blick_gli_page1_gli4`, `is_activ_btn_glissiere4_champ1_p1`, `btn_gli4_content1_p1`, `gl4_existed_link1_p1`, `gl4_custom_link1_p1`, `is_activ_btn_glissiere4_champ2_p1`, `btn_gli4_content2_p1`, `gl4_existed_link2_p1`, `gl4_custom_link2_p1`, `nombre_blick_gli2_p2`, `is_activ_btn_glissiere2_page2_champ2`, `btn_gli2_p2_content1`, `gl2_existed_p2_link1`, `gl2_p2_custom_link1`, `btn_gli2_p2_content2`, `gl2_existed_p2_link2`, `gl2_p2_custom_link2`, `nombre_blick_gli_page2_gli3`, `is_activ_btn_glissiere3_champ2_p2`, `btn_gli3_content1_p2`, `gl3_existed_link1_p2`, `gl3_custom_link1_p2`, `btn_gli3_content2_p2`, `gl3_existed_link2_p2`, `gl3_custom_link2_p2`, `nombre_blick_gli_page2_gli4`, `is_activ_btn_glissiere4_champ2_p2`, `btn_gli4_content1_p2`, `gl4_existed_link1_p2`, `gl4_custom_link1_p2`, `btn_gli4_content2_p2`, `gl4_existed_link2_p2`, `gl4_custom_link2_p2`, `gl2_custom_link1`, `is_activ_btn_glissiere2_page2_champ1`, `is_activ_btn_glissiere3_champ1_p2`, `is_activ_btn_glissiere4_champ1_p2`, `is_activ_bloc2`, `type_bloc2`, `bloc1_2_content`, `is_activ_btn_bloc1_2`, `btn_bloc1_2_content1`, `bloc1_2_existed_link1`, `bloc1_2_custom_link1`, `bloc1_2_content2`, `is_activ_btn_bloc2_2`, `btn_bloc1_2_content2`, `bloc1_2_existed_link2`, `bloc1_2_custom_link2`, `bonplan_id_2`, `bonplan_content_2`, `is_activ_btn_bp_2`, `btn_bp_content_2`, `bp_existed_link_2`, `bp_custom_link_2`, `fidelity_2_id`, `fidelity_2_content`, `is_activ_btn_fd_2`, `btn_fd_2_content`, `FD_2_existed_link`, `fd_custom_2_link`, `is_activ_bloc3`, `type_bloc3`, `bloc1_3_content`, `is_activ_btn_bloc1_3`, `btn_bloc1_3_content1`, `bloc1_3_existed_link1`, `bloc1_3_custom_link1`, `bloc1_3_content2`, `is_activ_btn_bloc2_3`, `btn_bloc1_3_content2`, `bloc1_3_existed_link2`, `bloc1_3_custom_link2`, `bonplan_id_3`, `bonplan_content_3`, `is_activ_btn_bp_3`, `btn_bp_content_3`, `bp_existed_link_3`, `bp_custom_link_3`, `fidelity_3_id`, `bloc1_4_custom_link1`, `is_activ_btn_fd_3`, `btn_fd_3_content`, `FD_3_existed_link`, `fd_custom_3_link`, `is_activ_bloc4`, `type_bloc4`, `bloc1_4_content`, `btn_bloc1_4_content1`, `is_activ_btn_bloc1_4`, `bloc1_4_existed_link1`, `fidelity_3_content`, `bloc1_4_content2`, `is_activ_btn_bloc2_4`, `btn_bloc1_4_content2`, `bloc1_4_existed_link2`, `bloc1_4_custom_link2`, `bonplan_id_4`, `bonplan_content_4`, `is_activ_btn_bp_4`, `btn_bp_content_4`, `bp_existed_link_4`, `bp_custom_link_4`, `fidelity_4_id`, `fidelity_4_content`, `is_activ_btn_fd_4`, `btn_fd_4_content`, `FD_4_existed_link`, `fd_custom_4_link`, `presentation_3_image_1`, `presentation_3_image_2`, `presentation_4_image_1`, `presentation_4_image_2`, `bloc_1_2_image1_content`, `bloc_1_4_image1_content`, `bloc_1_4_image2_content`, `bloc_1_3_image2_content`, `bloc_1_3_image1_content`, `bloc_1_2_image2_content`, `couleurLogo`, `page1_3_image_1`, `page1_3_image_2`, `page1_4_image_1`, `page1_4_image_2`, `couleurTexteMenu`, `affichageNomCommercant`, `Transparent_du_banniere`, `bloc1_islightbox`, `bloc1_islightbox2`, `bloc1_islightboxbp1`, `bloc1_islightboxfd1`, `bloc2_islightbox`, `bloc2_islightbox2`, `bloc2_islightboxbp`, `bloc2_islightboxfd`, `bloc3_islightbox`, `bloc3_islightbox2`, `bloc3_islightboxbp`, `bloc3_islightboxfd`, `bloc4_islightbox`, `bloc4_islightbox2`, `bloc4_islightboxbp`, `bloc4_islightboxfd`, `gli1_islightbox`, `gli1_islightbox2`, `gli2_islightbox`, `gli2_islightbox2`, `gli3_islightbox`, `gli3_islightbox2`, `gli4_islightbox`, `gli4_islightbox2`, `is_actif_livre_or`) VALUES
(382, 301299, 1992, 'dfsvsd', '<p>dfvfdsv</p>\r\n', 'eyr', '<p>sdfsf</p>\r\n', '', '', '', '', '', '', '', '', 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '<p>dfvsdfvsdvs</p>\r\n', NULL, NULL, '<p>sdfsdfs</p>\r\n', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '', NULL, NULL, '1', '1', 'fdvsdfv', '<p>dsvsdfv</p>\r\n', '<p>dfvsdfvs</p>\r\n', '<p>dfgsdg</p>\r\n', '<p>dfgfdsg</p>\r\n', 'rtyeyersdfg', '', '', '', '', '', '', '', '', '', '', 'gli11', '', 'p1', 'ag', '', 'gli12', NULL, '<p>tryryteryryeryr</p>\r\n', 'testpriviconcept.ovh', 'p2', 'bloc11', '1', NULL, '<p>rtyttryy</p>\r\n', '', 'p2', 'bloc12', '1', '2', '1', '154', '<p>ert\"dtersdvsss</p>\r\n', 8, '<p>edeezedzedd</p>\r\n', '1', 'p2', '', 'dedzddzeded', '1', 'ag', '', 'retre', 'g', '0', '', '', '0', '0', '0', '', 'gli12', '', '2', '', '0', '', '0', '', '0', '', '0', '', '2', 'p1', '', 'g21', '1', '1', 'g22', 'art', '2', '1', 'g31', 'ag', '', '1', 'g32', 'p1', '', '2', '1', 'g41', 'art', '', '1', 'g42', 'art', '', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '2', '0', '', '0', '', '0', '', '0', '', '', '0', '', '0', '', '', '0', '', '', '0', '', '0', '', '', '0', '', '', '0', '', '0', '', '', '0', '', '', '0', '0', '0', '1', '1fd', '<p>tr&#39;grgrg</p>\r\n', '1', 'bloc21', 'art', '', '<p>rfgfdg</p>\r\n', '1', 'bloc22', 'ag', '', '154', '<p>ertzrtd</p>\r\n', '1', 'detrzte', 'ag', '', 8, '<p>fgdddd</p>\r\n', '1', 'dedzddzeded', '0', '', '1', '1fd', '<p>dsfsg</p>\r\n', '1', 'bloc31', 'bp', '', '<p>sdgsgsd</p>\r\n', '1', 'bloc32', 'p2', '', '154', '<p>edtrztzrt</p>\r\n', 1, 'zerdtr', 'p2', '', 8, '', 1, 'dedzddzeded', 'art', '', 1, '1bp', '<p>bloc41</p>\r\n', 'bloc41', '1', 'bp', '<p>trgtgrgrg</p>\r\n', '<p>bloc42</p>\r\n', 1, 'bloc42', 'bt', '', '154', '<p>dtrrtetrty</p>\r\n', 1, 'erdtertzt', 'art', '', 8, '<p>ergergerzg</p>\r\n', 1, 'dedzddzeded', 'art', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'n', NULL, NULL, NULL, NULL, 'n', NULL, 0.7, '1', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
