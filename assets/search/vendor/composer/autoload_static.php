<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit92a770c7875b78133a3ac7ccc5ff7a5c
{
    public static $files = array (
        '97be8d00d4e1b8596dda683609f3dce2' => __DIR__ . '/..' . '/tcdent/php-restclient/restclient.php',
        '36b2687a172a0246ec8d56f5c8b8580c' => __DIR__ . '/..' . '/serpwow/google-search-results/google-search-results.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {

        }, null, ClassLoader::class);
    }
}
