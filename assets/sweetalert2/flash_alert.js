/**
 * cacher le flash_alert onload
 */
$('.flashalert').hide();

/**
 * afficher le flash_alert onload
 */
function alert() {
    $(document).ready(function() {
        checkFlashAlert();
    });
}

alert()

/**
 * chercher une cookie dans le navigateur
 * if not exists => on affiche le flash_alert
 */
function checkFlashAlert() {
    const cookieA = getCookie_ow('flash_alert');
    if (cookieA === "") {
        showSwal();
    } else {
        const flashAlert = JSON.parse(cookieA);
        if (!flashAlert.statut) {
            showSwal();
        }
    }
}

/**
 * ajouter une cookie dans le navigateur
 * @param {*} cname // nom de l'utilisateur
 * @param {*} cvalue // valeur de la cookie
 * @param {*} exdays // nombre de jour d'expiration de la cookie
 */
function setCookie_ow(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * 
 * @param {*} cname 
 */
function getCookie_ow(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/**
 *   conversion boolean value
 * from the input checkbox
 */
function test(bool) {
    if (bool == true) {
        return 1
    } else {
        return 0
    }
}

/**
 * 
 */
var baseUrl = 'http://localhost/sortez/';

function showSwal() {
    setTimeout(() => {
        const html = `
        <form action="" method="post" id="azform" class="mb-4">
            <div class="form-group">
                <input id="username" name="username" class="form-control az-input-flash-alert" type="text" placeholder="Username" required>
            </div>
            <div class="form-group">
                <input id="email" name="email" class="form-control az-input-flash-alert" type="email" placeholder="email" required>
            </div>
            <div class="container-fluid mb-2">
                <div class="row">
                    <div class="form-check form-inline m-auto d-flex text-align-center">
                        <input type="checkbox" name="annuaire" class="form-check-input" id="annuaire">
                        <label class="form-check-label mr-4 az-check-label" for="annuare"> Annuaire</label>
                        <input type="checkbox" name="actualite" class="form-check-input" id="actualite">
                        <label class="form-check-label mr-4 az-check-label" for="actualite"> Actulité</label>
                        <input type="checkbox" name="agenda" class="form-check-input" id="agenda">
                        <label class="form-check-label mr-4 az-check-label" for="agenda"> Agenda</label>
                    </div>
                    <div class="form-check form-inline m-auto d-flex text-align-center">
                        <input type="checkbox" name="deals" class="form-check-input" id="deals_fidelite">
                        <label class="form-check-label mr-4 az-check-label" for="deals"> Deals & Fidélité</label>
                        <input type="checkbox" name="boutique" class="form-check-input" id="boutique_en_ligne">
                        <label class="form-check-label mr-4 az-check-label" for="boutique"> Boutiques en ligne</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <select id="commune" class="form-control az-input-flash-alert">
                    <option value="null">choisissez la commune</option>
                    <option value="antananarivo">Antananarivo</option>
                    <option value="itaosy">Itaosy</option>
                </select>
            </div>
        </form>
        `;
        Swal.fire({
            title: '<h3 classe="alert-title" style="color:#e80eae">Alerte flash</h3>',
            html,
            confirmButtonText: 'Envoyer',
            confirmButtonColor: '#e80eae',
            showCloseButton: true,
            allowOutsideClick: false,
            focusConfirm: false,
            background: '#ffffff',
            onOpen: () => {
                $('.swal2-confirm').attr('disabled', true);
                $('#username,#email').on('keyup', function() {
                    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    var name_value = $("#username").val();
                    var email_value = $('#email').val();
                    var isEmail = regex.test(email_value);
                    var commune = $('#commune').val();

                    if (name_value != '' && email_value != '' && isEmail == true) {
                        $('.swal2-confirm').attr('disabled', false);
                    } else {
                        $('.swal2-confirm').attr('disabled', true);
                    }
                });
            },

        }).then((result) => {
            if (result.value) {
                const data = {
                    visiteur_nom: $('body #username').val(),
                    visiteur_email: $('body #email').val(),
                    visiteur_annuaire: test($("body #annuaire").is(":checked")),
                    visiteur_actualite: test($('body #actualite').is(":checked")),
                    visiteur_agenda: test($('body #agenda').is(":checked")),
                    visiteur_deals_fidelite: test($('body #deals_fidelite').is(":checked")),
                    visiteur_boutique_en_ligne: test($('body #boutique_en_ligne').is(":checked")),
                    visiteur_commune: $('body #commune').val()
                }
                $.ajax({
                    type: "POST",
                    url: baseUrl + "front/flash_alert",
                    data,
                    cache: false,
                    success: function(response) {
                        const value = {
                            statut: true,
                            username: data.visiteur_nom
                        }
                        setCookie_ow('flash_alert', JSON.stringify(value), 1);
                        Swal.fire('Enregistré', 'Élement enregistré avec succès.', 'success')
                        return true
                    },
                    failure: function(response) {
                        return false;
                    }
                });
            } else if (result.dismiss == 'close') {
                Swal.fire('Think you !').then(() => {
                    const value = {
                        statut: true
                    }
                    setCookie_ow('flash_alert', JSON.stringify(value), 1);
                })
            }
        })
    }, 2000);
}