$(function(){

    var jour_show = $('#jour_show'),
        heure_show = $('#heure_show'),
        minute_show = $('#minute_show'),
        ts = new Date(<?php echo intval($date_annee_fin); ?>,<?php echo intval($date_mois_fin)-1?>,<?php echo intval($date_jour_fin)?>),
        newYear = true;

    if((new Date()) > ts){
        ts = (new Date()).getTime() + 12*24*60*60*1000;
        newYear = false;
    }

    $('#countdown').countdown({
        timestamp	: ts,
        callback	: function(days, hours, minutes, seconds){

            var message = "";

            message += days + " day" + ( days==1 ? '':'s' ) + ", ";
            message += hours + " hour" + ( hours==1 ? '':'s' ) + ", ";
            message += minutes + " minute" + ( minutes==1 ? '':'s' ) + " and ";
            message += seconds + " second" + ( seconds==1 ? '':'s' ) + " <br />";

            if(newYear){
                message += "left until the new year!";
            }
            else {
                message += "left to 10 days from now!";
            }

            jour_show.html(days);
            heure_show.html(hours);
            minute_show.html(minutes);
        }
    });

});