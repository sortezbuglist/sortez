$(document).ready(function(){
//    Menu responive
    $('.menu-respons').click(function(){
        $('.navigation').slideToggle(300);
   });

   //$( "#forms-tabs" ).tabs();
    $( "#fonctionement-tabs" ).tabs();

    // init plugin (with callback)
    $('.clearable').clearSearch({ callback: function(){ console.log("cleared");}});
    $('.clearable').width('').change();
    function checkWidth() {
        var windowsize = $(window).width(),
            windowheight = $(window).height(),
            removalheight = $('.head').height(),
            contentheight = $('.page-black').height(),
            pageheight =  removalheight + contentheight,
            contentheight2 = windowheight - removalheight;
        if ( windowheight > pageheight ) {
            $('.page-black').css('min-height', contentheight2);
            $('.page-black').css('padding-bottom', '0');

        }
        else if (windowheight <= contentheight) {
            $('.page-black').css('min-height', 'auto');
            $('.page-black').css('padding-bottom', '70px');

        }
        else {
            $('.page-black').css('min-height', contentheight2);
            $('.page-black').css('padding-bottom', '60px');

        }

    }

    $(window).on('load', function() {
        checkWidth();
    });
    $(window).on('resize', function() {
        checkWidth();
    });

});