/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	config.toolbar = [
        { name: 'editing', items: [ 'Source','Undo', 'Redo','Find', 'Replace', '-', 'SelectAll', '-', 'Scayt','Bold','Strike','NumberedList','BulletedList','TextColor','BGColor', 'Link', 'Unlink','-', '-','Outdent','-','Indent','-', '-','JustifyCenter','JustifyLeft','JustifyRight','JustifyBlock','BidiLtr','BidiRtl', '-','Image','wenzgmap','Iframe','VideoDetector','Table','HorizontalRule','Styles', 'Format', 'Font', 'FontSize','About'  ] },
        '/',



    ];
    config . extraPlugins = 'videodetector,wenzgmap' ;
	config.filebrowserBrowseUrl = 'http://testpriviconcept.ovh/assets/kcfinder/browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = 'http://testpriviconcept.ovh/assets/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = 'http://testpriviconcept.ovh/assets/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = 'http://testpriviconcept.ovh/assets/kcfinder/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = 'http://testpriviconcept.ovh/assets/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = 'http://testpriviconcept.ovh/assets/kcfinder/upload.php?opener=ckeditor&type=flash';
};
