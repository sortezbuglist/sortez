<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Famille extends  MY_Controller {	
	const RECHECHE_STRUCTURE = 'recherche structure';
	const RDV_RECHECHE_STRUCTURE = 'rdv recherche structure';
	const RECHECHE_DISPO = 'recherche disponibilité';
	const RDV_ADAPTATION = 'rendez-vous adaptation';
	const RDV_ADAPTATION_REGULIER = 'rendez-vous adaptation régulier';
	const RDV_ADAPTATION_IRREGULIER = 'rendez-vous adaptation irrégulier';

	function __construct()
	{
		parent::__construct();
		$this->load->model('structure_model');
		$this->load->model('temps_accueil_model');
		$this->load->model('type_temps_accueil_model');
		$this->load->model('temps_accueil_has_section_model');
		$this->load->model('section_model');
		$this->load->model('ville_model');
		$this->load->model('parent_model','parent');
		$this->load->model('enfant_model','enfant');
		$this->load->model('disponibilites_model');
		$this->load->model('param_familiplace_model');
		$this->load->model('ouverture_model');
		$this->load->model('fermeture_model');
		$this->load->model('reservation_model');
		$this->load->model('alerte_model');
		$this->load->model('jour_model');
		$this->load->model('recursion_creneau_model');
		$this->load->model('enfant_has_structure_model');
		$this->load->model('alerte_has_structure_model');
		$this->load->model('alerte_has_type_accueil_model');
		$this->load->model('alerte_has_jour_model');
		$this->load->model('alerte_has_recursion_creneau_model');
		$this->load->model('textes_model','textes');
		$this->load->model('users_structure_model');
		$this->load->model('journee_pedag_model','journee_pedag');
		$this->load->model('mail_alerte_disponibilite_model');
		
		$this->load->css('assets/css/famille.css');
		$this->load->css('assets/css/famille_compte.css');
		$this->load->css('assets/css/fm.selectator.jquery.css');
		$this->load->css('assets/css/etape_famille.css');
		$this->load->css('assets/css/bootstrap-timepicker.min.css');
		$this->load->css('assets/css/actualite.css');
		$this->load->css('assets/css/dispo.css');
		$this->load->css('assets/css/jqueryui-editable.css');
		
		$this->load->js('assets/js/jquery.maskedinput.min.js');
		$this->load->js('assets/js/fm.selectator.jquery.js');
		$this->load->js('assets/js/famille_validation_form.js');
		$this->load->js('assets/js/famille_compte.js');
		$this->load->js('assets/js/custom.js');
		$this->load->js('assets/js/locales/bootstrap-datepicker.fr.js');	
		$this->load->js('assets/js/jquery.mCustomScrollbar.concat.min.js');
		$this->load->js('assets/js/jquery.mCustomScrollbar.concat.min.js');
		$this->load->js('assets/js/jqueryui-editable.min.js');
		$this->load->js('assets/js/jqueryui-editable.js');
		$this->load->js('assets/js/blockUI.js');
		
		$this->load->library('image_moo');
	}
	
	public function get_info_homepage_coordonnee($idUserParent) {
		$data = new stdClass();
		//Pour coordonnées
		$criteres = array('users_id' => $idUserParent);
		$infoParentFamille = $this->parent->get_parent_list($criteres);
		$infoParentFamille = current($infoParentFamille);			
		$lstSituation = get_situation_maritale(false);
		$lstSituationFamiliale = "";
		foreach ($lstSituation as $key => $value) {
			if (!empty($key)) {
				$val = "{value: ". $key . ", text: '". $value ."'}";					
				if (empty($lstSituationFamiliale)) {
					$lstSituationFamiliale .= $val;
				} else {
					$lstSituationFamiliale .= "," . $val;
				}
			}
		}
		$data->parent_situation_maritale = $lstSituationFamiliale ;			
		$data->id = $infoParentFamille->id;
		$data->nom =  $infoParentFamille->nom;
		$data->prenom =  $infoParentFamille->prenom;
		$data->adresse = $infoParentFamille->adresse;
		$data->ville_id = $infoParentFamille->ville_id;
		$data->ville ="";
		if(isset($data->ville_id)){
				$ville = $this->ville_model->get_ville_by_id($data->ville_id);
				if(isset($ville->nom))$data->ville =$ville->nom;
		}
		$data->code_postal = $infoParentFamille->code_postal;
		$data->email = $infoParentFamille->email;
		$data->tel1 = $infoParentFamille->tel1;
		$data->situation_maritale  = $infoParentFamille->situation_maritale;
		return $data;			
	}
	
	public function get_info_homepage_enfants($idUserParent, &$lstMenu = array()) {
		//Pour famille
		//Chercher la liste des enfants
		$data = new stdClass();
		$criteres = $lstEnfants = array();	
		$premier_enfant = false;	
		if (!empty($idUserParent)) {
			$criteres['users_id'] = $idUserParent;
			$lstEnfants  = $this->enfant->get_enfant_list($criteres);		
			if (!empty($lstEnfants)) {
				$premier_enfant = current($lstEnfants);
				foreach($lstEnfants as $key => $rowEnfant) {
					$rowEnfantArray = (array) $rowEnfant;
					$idUserParent = $rowEnfant->parent_id;
					$rowEnfantArray['enfant_photo'] = $this->enfant->get_photo_enfant($idUserParent, $rowEnfant->enfant_id) . "?" . rand();
					//$nomUser = empty($rowEnfant->enfant_prenom) ?  $rowEnfant->enfant_nom : $rowEnfant->enfant_prenom;
					//$rowEnfantArray['date_naissance'] = convertToDateMonthYearFrenchFromMysql($rowEnfant->enfant_date_naissance_mysql);
					$rowEnfantArray['date_naissance'] = $rowEnfant->enfant_date_naissance;
					//img: '". site_url() . $rowEnfantArray['enfant_photo']."',
					//goto_creche('#structure_id#', $rowEnfant->enfant_id);
					$nomUser = $rowEnfant->enfant_prenom. " " . $rowEnfant->enfant_nom;
					$menu = "{
						        name: '".$nomUser."',							        					        
						        title: '".$nomUser."',
						        fun: function () {
						            goto_creche('#structure_id#', ". $rowEnfant->enfant_id .", '". $rowEnfant->enfant_date_naissance ."', '".$rowEnfant->enfant_code_postal ."');
						        }
					   		 }";
					$lstMenu .= empty($lstMenu) ? $menu : "," . $menu;
					
					$criteres = array("enfant_id" => $rowEnfant->enfant_id);
					$listStructureResa = $this->reservation_model->get_list_reservation_enfant_structure($criteres);
					$rowEnfantArray['listStructureResa'] =$listStructureResa;
					$lstEnfants[$key] = (object) $rowEnfantArray;
				}
			} 	
		}
		$data->premier_enfant = $premier_enfant;	
		$data->lstEnfants = $lstEnfants;
		return $data;				
	}
	
	public function get_enfant_list_for_resa($idUserParent, $enfant_id = false){
		$critere['users_id'] = $idUserParent;
		if($enfant_id){
			$critere['enfant_id'] = $enfant_id;
		}
		return $this->enfant->get_enfant_list($critere);
	}


	public function get_info_etablissement_favoris($idUserParent) {		
		$criteres = array('users_id' => $idUserParent);
		$infoParentFamille = $this->parent->get_parent_list($criteres);
		$infoParentFamille = current($infoParentFamille);
		$idParent = $infoParentFamille->id;
		
		$criteresFavoris = array();
		$criteresFavoris['parent_id'] = $idParent;

		$disponibilite_list = array();		
		$disponibilite_list = $this->reservation_model->get_etablissement_favoris($criteresFavoris, "", true);	
		$structure_list_Dispo = $this->get_structures_by_dispo_list	($disponibilite_list);						 
		return $structure_list_Dispo;
	}

	public function homepage_famille_reconnue(){			
		if (!$this->ion_auth->logged_in() || (($this->ion_auth->logged_in() && get_cookie('action_login') != 'login_parent'))){
			redirect('auth/do_logout/login_parent/compte_inaccessible');
		}
		
		$this->load->js('assets/js/jquery.nailthumb.1.1.min.js');
		$this->load->js('assets/js/contextMenu.min.js');				
		$this->load->css('assets/css/jquery.nailthumb.1.1.min.css');
		$this->load->css('assets/css/contextMenu.css');
		
		$user = $this->ion_auth->user()->row();
		$idUserParent = $user->id;
		if (!empty($idUserParent)) {			
			$this->load->model('parent_model','parent');
			$data['coordonnee']= $this->get_info_homepage_coordonnee($idUserParent);
			$lstMenu = "";
			$data['enfants']= $this->get_info_homepage_enfants($idUserParent, $lstMenu);
			$data['lstMenu'] = $lstMenu;
			$data['etablissement_favoris']= $this->get_info_etablissement_favoris($idUserParent);		
			$data['mes_alertes'] = $this->get_mes_alertes();
			$data['lst_enfant'] = $this->get_enfant_list_for_resa($idUserParent);
			//$data['mes_reservations'] = $this->get_mes_reservations($idUserParent);
			$data['users_id'] = $idUserParent;
		}		
	
		$this->load->view('familles/homepage_famille_reconnue', $data);
	}
	
	public function get_mes_reservations($idUserParent){
		$data_enfant_reservation_list = array();
		$data_enfant_reservation_list = $this->enfant->get_enfant_list(array('users_id' => $idUserParent));
		if(is_array($data_enfant_reservation_list)){
			foreach ($data_enfant_reservation_list as $enfant){
				$enfant->enfant_photo = $this->enfant->get_photo_enfant($enfant->parent_id, $enfant->enfant_id);
				$enfant->reservation_list = $this->get_reservations_enfant($enfant->enfant_id, $idUserParent);
			}
		}
		return $data_enfant_reservation_list;
		//return $this->enfant->get_enfant_list(array('users_id' => $idUserParent));
	}

	public function get_reservations_enfant($enfant_id, $users_id_parent, $passe_futur='futur'){
		$criteres_enfant = array('users_id' => $users_id_parent);
		$criteres_enfant['users_id'] = $users_id_parent;
		$criteres_enfant['enfant_id'] = $enfant_id;
		
		$reservation_list = array();
		$enfant_list = $this->enfant->get_enfant_list($criteres_enfant);
		if($enfant_list && count($enfant_list) > 0){
			if($passe_futur=='futur'){
				$criteres['is_venir']=1;
			}
			if($passe_futur=='passe'){
				$criteres['is_realiser']=1;
			}
			$reservation_list = $this->reservation_model->get_reservation_list($criteres, "jour_reservation ASC", false, false);
			/*$data['enfant_id'] = $this->input->post('enfant_id');
			$data['passe_ou_futur'] = $this->input->post('passe_ou_futur'); 
			$data['users_id'] = $users_id_parent;*/
		}
		return $reservation_list;
	}
	
	public function load_famille_reconue_mes_alertes(){
		$data['mes_alertes'] = $this->get_mes_alertes();
		$result = array( 
			'html' => $this->load->view('familles/alertes/famille_reconnue_mes_alertes', $data, TRUE)
		);
		echo json_encode($result);
	}
	
	public function get_reservations_par_enfant(){
		// les reservations
		$users_id_parent = $this->input->post('parent_id');
		$enfant_id = $this->input->post('enfant_id');
		$structure_id = $this->input->post('structure_id');
		$criteres_enfant = array('users_id' => $users_id_parent);
		$enfants = $this->enfant->get_enfant_list($criteres_enfant);
		if($this->input->post('passe_ou_futur')=='futur')$criteres['is_venir']=1;
		if($this->input->post('passe_ou_futur')=='passe')$criteres['is_realiser']=1;
		$parcours_enfant = array();
		$infos_enfant = array();
		foreach($enfants as $e){
			$criteres['enfant_id'] = $e->enfant_id;
			
			// informations sur l'enfant
			$photo = $this->enfant->get_photo_enfant($e->parent_id, $e->enfant_id);
			$prenom = $e->enfant_prenom;
			$nom = $e->enfant_nom;
			$cp = $e->enfant_code_postal;
			$date_nais = $e->enfant_date_naissance;
			$infos_enfant[''.$e->enfant_id] = array('enfant_nom' => $nom, 'enfant_photo' => $photo, 'enfant_prenom' => $prenom, 'code_postal' => $cp, 'date_nais' => $date_nais);
			if($structure_id){
				$criteres['structure_id'] = $structure_id;
			}
			$parcours_enfant[''.$e->enfant_id] = $this->reservation_model->get_reservation_list($criteres, "jour_reservation ASC", false, false);
		}
			
		$data['lst_reservation'] = $parcours_enfant;
		$data['lst_enfant'] = $infos_enfant;
		
		$data['enfant_id'] = $this->input->post('enfant_id');
		$data['passe_ou_futur'] = $this->input->post('passe_ou_futur'); 
		$data['users_id'] = $users_id_parent;
		$this->output->unset_template();
		echo $this->load->view('familles/reservations_par_enfant', $data);
	}
	
	public function upload_photo_enfant_in_temp($idEnfant = 0) {
		$dataRequest = $_REQUEST;
		$result = array();
		if (!empty($dataRequest['enfant_id'])) {
			
			$key = array_search($idEnfant, $dataRequest['enfant_id']);
			//Chercher 
			$_FILES['photo']['name']= $_FILES['enfant_photo']['name'][$key];
			$_FILES['photo']['type']= $_FILES['enfant_photo']['type'][$key];
			$_FILES['photo']['tmp_name']= $_FILES['enfant_photo']['tmp_name'][$key];
			$_FILES['photo']['error']= $_FILES['enfant_photo']['error'][$key];
			$_FILES['photo']['size']= $_FILES['enfant_photo']['size'][$key];	
			$extTab = explode(".", $_FILES['enfant_photo']['name'][$key]);
			$extension = "";
			if (count($extTab)>0) {
				$extension = "." . $extTab[1];
			}
			$nomImage = "photo_".rand(1,1000) . "_" . $idEnfant . $extension;
			$dirPhotoEnfant = $this->config->item('dir_temp') ;	
			if(!is_dir($dirPhotoEnfant)){
					mkdir($dirPhotoEnfant,0777,true);
					chmod($dirPhotoEnfant,0777);
			}						
			$config_photo_enfant = array(
									'upload_path' 	=>  $dirPhotoEnfant,
									'allowed_types' => 'gif|jpeg|jpg|png',
									'file_name'		=>	$nomImage,
									'overwrite'		=> 	true
			);
			$this->load->library('upload', $config_photo_enfant);
			$this->upload->initialize($config_photo_enfant);
			$photoName = 'photo';
			if ((isset($_FILES[$photoName])) && (!empty($_FILES[$photoName]['name']))) {
				//$this->upload->do_upload($photoName);
				if ( ! $this->upload->do_upload($photoName))
				{
						$error = array('error' => $this->upload->display_errors());
							// print_r($error);
				}
				else
					{
						$data_photo = array('upload_data' => $this->upload->data());
						$image_uploaded = $data_photo['upload_data']['full_path'];
						$this->image_moo->load($image_uploaded)
						->resize_crop(64,64)
						->border(3, "#ffffff")
						->border(1, "#000000")
						->save($image_uploaded,true);
				}
				
			}
			$result = array("status" => "success" , "photo_name" => site_url() . $dirPhotoEnfant. "/" . $nomImage . "?" . rand());		
		}
				
		echo json_encode($result);	
	}
	
	/**
	 * 
	 * Méthode a appelé lors du click sur le bouton "Rendez vous d'adaptation" dans la page recherche picto
	 */
	public function rechercher_rdv_adaptation($idTypeRecherche = 0){
		// Chargement des divers fichiers css et js utile
		$this->load->js($this->config->item('url_api_google_map'));
		$this->load->js('assets/js/custom_api_google_map.js');
		$this->load->js("assets/js/infobox.js");//infoBox pour googleMap
		$this->output->set_title('Familiplace - Disponibilit&eacute;');
		/*
		switch ($idTypeRecherche) {
			case 1:
				$typeRecherche = self::RDV_ADAPTATION_REGULIER;
				break;
			case 2:
				$typeRecherche = self::RDV_ADAPTATION_IRREGULIER;
				break;
			default:
				$typeRecherche = self::RDV_ADAPTATION;
				break;
		}		
		$dataRdvAdaptation = $this->get_data_search_dispo($typeRecherche);
		*/	
		//Pas de notion de régulier - irrégulier pour RDV Adaptation
		/*if ($idTypeRecherche != 0) {
			$idTypeRecherche = 1; //PICTO
		}
				
		$dataRdvAdaptation = $this->get_data_search_picto($idTypeRecherche);*/
		
		//Hasina : desormais on utilise get_data_search_dispo
		
		$dataRdvAdaptation = $this->get_data_search_dispo(self::RDV_RECHECHE_STRUCTURE);
		$this->load->view('familles/resultat', $dataRdvAdaptation);
	}

	public function renvoi_adaptation() {
		$idStructure = $this->input->post('structure_id');
		$enfantId = $this->input->post('enfant_id');		
		$criteres = array('structure_id' => $idStructure);
		$userLst = $this->structure_model->get_structure_list($criteres);		
		$userStructure = current($userLst);
		
		$data = array();
		$data['mailDirectrice'] = "";
				
		$userStructureDirecteur = $this->users_structure_model->get_by_structure_id($idStructure);	
		if (!empty($userStructureDirecteur)) {	
			$idDirecteur = $userStructureDirecteur->users_id;
			$user = $this->ion_auth->where('id', $idDirecteur)->users()->row();		
			$data['mailDirectrice'] = $user->email;
		}
		$data['nomStructure'] = $userStructure->nom;
		

		$criteres_enfant = array();
		if (empty($enfantId)) {
			$parent = $this->get_parent_by_user_connected();
			if ($parent) {
				$criteres_enfant['parent_id'] = $parent->id;
			}
		} else {		
			$criteres_enfant["enfant_id"] = $enfantId;
		}		
		
		$enfant_list = array();
		if (!empty($criteres_enfant)) {
			$enfant_list = $this->enfant->get_enfant_list($criteres_enfant);
			$result = array();
			if(count($enfant_list) > 0){
				$enfant_list = current($enfant_list);
				$enfantId = $enfant_list->enfant_id;
				$enfant_list->directrice_name = 
				$enfant_list->directrice_email =  $data['mailDirectrice'];
        	
				//c'est ville qui prime 
				$data['enfant_code_postal_or_ville'] =  empty($enfant_list->enfant_ville) ? $enfant_list->enfant_code_postal : $enfant_list->enfant_ville;
			}
		}		
		
		$mail_content = $this->send_mail_creer_rdv_directrice(false, $enfant_list);
		/*
		$subject = $mail_content->subject ;
		$message = $mail_content->message ;
		$message = str_replace("<br>", "\t\n". chr(13) , $message);
		$data['mailTo'] = $data['mailDirectrice'] . "?subject=" . $subject . "&body=". $message;
		*/
		// récupération de l'objet parent ou gestionnaire connect?
		$connected = get_parent_or_gestionnaire_by_user_connected();
		$data['connected']= $connected;
		if($connected!=null){
			$user = $connected['user'];
			$data['user']= $user;
		}
		$data['mail_content'] = $mail_content;
		$dataMessageView = $this->load->view('familles/mail_rdv_adaptation', $data, TRUE);
		$dataMessageView = str_replace(array("\r\n","\n"),' ',str_replace("'","&apos;", $dataMessageView));
		$data['dataMessageView'] =$dataMessageView;
			
		$dataView = $this->load->view('familles/no_resultat', $data, TRUE);
		$result = array("dataView" => $dataView);
		echo json_encode($result);	
	}

	public function adapter_enfant_by_id() {
		$idEnfant = $this->input->post('enfant_id');
		$idStructure = $this->input->post('structure_id');
		
		$enfant_has_structure = array("enfant_id" => $idEnfant, "structure_id" => $idStructure);		
		$this->enfant_has_structure_model->insert_or_update_enfant_has_structure($enfant_has_structure);		
		$this->reservation_model->update_reservation_adaptation_by_enfant($idEnfant, $idStructure);
		
		$result = array("message" => "Adaptation de l'enfant effectuée avec succès");
		echo json_encode($result);			
	}
		
	public function adapter_enfant() {
		$idReservation = $this->input->post('idReservation');
		$idStructure = $this->input->post('structure_id');
		
		$this->reservation_model->update_reservation_adaptation($idReservation, $idStructure);
		$result = array("message" => "Adaptation de l'enfant effectuée avec succès");
		echo json_encode($result);			
	}
	
	public function do_get_fil_ariane() {
		$controller = $this->input->post('controller');
		$method = $this->input->post('method');
		$filArianeText = get_fil_ariane ($controller , $method);
		$result = array("fil_ariane" => $filArianeText);
		echo json_encode($result);	
	}
	
	public function update_presence_reservation () {
		$idReservation = $this->input->post('id_reservation');
		$present = $this->input->post('present');		
		
		$reservation = array('present' => $present);
		
		$this->reservation_model->update_reservation_where_id($reservation, $idReservation);
		if(!$present){
			$reservation = $this->reservation_model->get_reservation_by_id($idReservation);
			$this->send_mail_famille_enfant_absent($reservation->enfant_id, $reservation->disponibilites_id, $idReservation);
		}
							
		$result = array("message" => "Modification de présence effectuée avec succès");
		echo json_encode($result);
	}
		
	public function update_horaire_reservation () {
		$idReservation = $this->input->post('id_reservation');
		$horaire_debut = $this->input->post('horaire_debut');
		$horaire_fin = $this->input->post('horaire_fin');
		$reservation = array('horaire_debut' => $horaire_debut, 'horaire_fin' => $horaire_fin);
		$this->reservation_model->update_reservation_where_id($reservation, $idReservation);
		
		$heure_deb=explode(":",$horaire_debut);
		$heure_fin=explode(":",$horaire_fin);
		$hd=(int)$heure_deb[0];							
		$hd =  str_repeat('0', 2-strlen($hd)). $hd;
		$hf=(int)$heure_fin[0];
		$hf =  str_repeat('0', 2-strlen($hf)). $hf;
		$horaire = $hd.'H'.$heure_deb[1].'&nbsp;-&nbsp;'.$hf.'H'.$heure_fin[1];
							
		$result = array("message" => "Modification de l'horaire effectuée avec succès", "horaire" => $horaire);
		echo json_encode($result);
	}
	
	public function cancel_reservation_a_venir() {
		$idEnfant = $this->input->post('idEnfant');
		$idDisponibilite = $this->input->post('idDisponibilite');
		$idReservation = $this->input->post('idReservation');
		$date=$this->input->post('date_annule');
		$date_en=date_fr_to_en($date,'/','-');	
		$comment=$this->input->post('comment');		
		$data=array(			 	
				'annulation_date' => $date_en,
				'annulation_comment'=> $comment,				
				'annule'=> 1,
				'valide'=> 0,
				'id' => $idReservation,
			);		
	    $this->reservation_model->update_reservation_by_id($idReservation, $data);	
	    //$this->send_mail_famille_enfant_absent($idEnfant, $idDisponibilite, $idReservation, $date);
		$result = array("message" => "Annulation effectuée avec succès");
		echo json_encode($result);			
	}	
	
	public function load_html_annulation_reservation(){
		$result = array( 
			'html' => $this->load->view('familles/reservations_par_enfant_annuler_modal', array(), TRUE)
		);
		echo json_encode($result);
	}
	
	public function control_data_header() {
		$this->load->controller('gestionnaire');
		$data = $this->gestionnaire->index_login_common($action);
	}		
	
	public function menu_famille_compte() {
		$dataView = "";
		$action = $this->input->post('action');
		$idEnfant = $this->input->post('enfant_id');
		
		//Actions possibles :menu_famille_mon_compte, menu_famille_mes_enfants, menu_famille_mon_enfant, 
		//menu_famille_mes_reservations, menu_famille_mes_etablissements_favoris, menu_famille_mes_alertes
		$action = substr($action, 5);		
		//Ajouter les autres informations nécessaires ici
		$data = $this->session->userdata("data_login");	
		if (empty($data)) {
			$this->load->controller('gestionnaire');
			$data = $this->gestionnaire->index_login_common($action);
		}
		
		$idParent = 0;
		$infoParent = isset($data['infoParent'])  ? $data['infoParent'] : array();
		if (!empty($infoParent)) {
			$lstParent = current($infoParent);
			$nomUser = empty($lstParent->prenom) ?  $lstParent->nom : $lstParent->prenom;
			$idParent = empty($lstParent->id) ?  $lstParent->nom : $lstParent->id;
		}
		switch ($action) {
			case "famille_add_new_enfant": 	//Ajouter un nouveau enfant dans menu Mon compte
			case "famille_mon_compte": //Affiche étape 1 seulement
			case "famille_mon_enfant": //Affichage etape 2 uniquement
			case "famille_compte_parent": //Affichage etape 1 et 2 pour consultation uniquement								
				$step = "";
				$idStructure = 0;
				$typeActionEnfant = $this->input->post('typeActionEnfant') ;	
				$typeActionEnfant = (!empty($typeActionEnfant)) ? $typeActionEnfant : 0;
				$typeMenu = $this->input->post('typeMenu') ;
				$typeMenu = (!empty($typeMenu)) ? $typeMenu : 0;
				if ($action == "famille_mon_compte") {
					$step = "etape1";
				} else if ($action == "famille_mon_enfant") {																
					if (!empty($typeActionEnfant)) {
						$idParent = $this->input->post('parent_id');	
						$idStructure = $this->input->post('strucure_id');
					}
					$step = "etape2";
				} else if ($action == "famille_compte_parent") {
					$idParent = $this->input->post('parent_id');
					$idStructure = $this->input->post('strucure_id');
				}	else if ($action =="famille_add_new_enfant") {
					$idParent = $this->input->post('parent_id');
					$step = "etape2";
					$idEnfant = '0-new';
				}		
				$passe_ou_futur = 0;	
				if($this->input->post('passe_ou_futur')=='passe') $passe_ou_futur = 1;
				if($this->input->post('passe_ou_futur')=='futur') $passe_ou_futur = 2;		
				$dataView = $this->do_inscription_famille($idParent, TRUE, $step, $idEnfant, $idStructure, $typeActionEnfant, $typeMenu, $passe_ou_futur);
				break;
			case 'famille_mes_reservations' :
				$criteres['parent_id'] = $idParent;
				$criteres['valide'] = 1;
				$criteres['annule_not_show'] = 1;				
				//Pas de section
				if($this->input->post('passe_ou_futur')=='passe') $criteres['is_realiser']=1;
				if($this->input->post('passe_ou_futur')=='futur') $criteres['is_venir']=1;
				$reservation_list = $this->reservation_model->get_reservation_list($criteres, "jour_reservation ASC", false, false);
				
				$data['reservation_panier_list'] = get_reservation_info_list($reservation_list);
				
				$dataView = $this->load->view('familles/'. $action, $data, TRUE);				
				break;
			case 'famille_mes_etablissements_favoris' :
				//Chercher la liste des établissements favoris
				$criteresFavoris = array();
				$criteresFavoris['show_rdv_adaptation'] = 0;
				$criteresFavoris['date_sup_equals_date_du_jour'] = 1;
				$criteresFavoris['parent_id'] = $idParent;
				$criteresFavoris['structure_active'] = 1;				
				$data['lstEtablissementFavoris'] = $structure_list = $this->get_etablissement_favoris($criteresFavoris);
											
				$structure_list_Dispo = $this->get_structure_list_info($structure_list, $criteresFavoris, false);
				
				$structure_list_Rdv = array();
				//CRA: Fix : 4340: Crèches préférées 
				/*
					$criteresFavoris['show_rdv_adaptation'] = 1;
					$structure_list = $this->get_etablissement_favoris($criteresFavoris);								
					$structure_list_Rdv = $this->get_structure_list_info($structure_list, $criteresFavoris, false);
				*/			
				
				$data['structure_list'] =  array_merge($structure_list_Dispo, $structure_list_Rdv);
				
				
				$dataView = $this->load->view('familles/'. $action, $data, TRUE);
				break;
			case 'famille_mes_alertes':
				//Chercher mes alertes
				$dataView = $this->menu_famille_resultats_alerte($action);
				break;
			case 'famille_mes_alertes_parametrage':
				$dataView = $this->menu_famille_mes_alertes_parametrage($action);
				break;
			default :
				$dataView = $this->load->view('familles/'. $action, $data, TRUE);
				break;								
		}
		
		$result = array("dataView" => $dataView);
		echo json_encode($result);		
	}
	
	function menu_famille_mes_alertes_parametrage($action){
		$parent = $this->get_parent_by_user_connected();
		if($parent != null && isset($parent)){
			$enfant_list = $this->enfant->get_enfant_list(array('parent_id' => $parent->id));
			$i = 1;
			foreach ($enfant_list as $enfant){
				$enfant->enfant_photo = base_url().$this->enfant->get_photo_enfant($enfant->parent_id, $enfant->enfant_id);
				$alerte = $this->alerte_model->get_alerte_by_enfant_id($enfant->enfant_id);
				if($alerte){
					$enfant->alerte = $this->alerte_model->get_alerte_by_enfant_id($enfant->enfant_id);
				}
				$i++;
			}
			$data['enfant_list'] = $enfant_list;
			return $this->load->view('familles/'. $action, $data, TRUE);
		}
		return null;
	}
	
	public function get_mes_alertes(){
		$parent = $this->get_parent_by_user_connected();
		if ($parent){
			return $this->data_enfant_alerte($parent->id);
		}
		return false;
	}
	
	public function load_alertes_parametre(){
		$enfant_id = $this->input->post('enfant_id');
		$enfant = $this->enfant->get_enfant_by_id($enfant_id);
		$data['enfant'] = $enfant;
		$data['alerte_parametre_list'] = get_alerte_parametre_list_by_enfant_id($enfant_id);
		$result = array( 
			'enfant' => $enfant,
			'html_list_alerte_parametre' => $this->load->view('familles/alertes/list_alerte_parametre', $data, TRUE)
		);
		echo json_encode($result);
	}
	
	public function load_form_gerer_alerte(){
		$alerte_id = $this->input->post('alerte_id');
		$enfant_id = $this->input->post('enfant_id');
		
		$enfant = $this->enfant->get_enfant_by_id($enfant_id);
		$alerte = get_parametre_alerte_by_id($alerte_id);
		if(!$alerte){
			$alerte = new stdClass();
			$alerte->id = 0;
		}
		$enfant->alerte = $alerte;
		
		$data['enfant'] = $enfant;
		$data = array_merge($data, $this->data_form_creer_alerte($alerte, $enfant));
		$result = array( 
			'enfant' => $enfant,
			'html_form_gerer_alerte' => $this->load->view('familles/alertes/form_gerer_alerte', $data, TRUE)
		);
		echo json_encode($result);
	}
	
	public function load_form_gerer_mail_alerte(){
		$parent_id = $this->input->post('parent_id');
		$this->load->model('type_envoie_alerte_model');
		
		$data['field_checkbox_par_semaine'] =  array(
			'type'  => 'checkbox',
            'name' 	=> 'type_envoie',
            'value' => 1,
			'id' => "semaine"
     	);
     	$data['field_checkbox_des_dispo_existe'] =  array(
			'type'  => 'checkbox',
            'name' 	=> 'type_envoie',
            'value' => 2
     	);
     	$data['field_checkbox_pas_d_envoi'] =  array(
			'type'  => 'checkbox',
            'name' 	=> 'type_envoie',
            'value' => 3
     	);
     	
     	$type_envoie = $this->type_envoie_alerte_model->get_type_envoie_mail_alerte($parent_id);
     	
     	if($type_envoie){
     		if($type_envoie->id == 1){ // Une fois par semaine
	     		$data['field_checkbox_par_semaine']['checked'] = true;
	     	}else if($type_envoie->id == 2){ // Dès que la dispo existe
	     		$data['field_checkbox_des_dispo_existe']['checked'] = true;
	     	}else if($type_envoie->id == 3){ // Pas d'envoie
	     		$data['field_checkbox_pas_d_envoi']['checked'] = true;
	     	}else { // par défaut une fois par semaine
	     		$data['field_checkbox_par_semaine']['checked'] = true;
	     	}
     	}else { // par défaut une fois par semaine
     		$data['field_checkbox_par_semaine']['checked'] = true;
     	}
     	
     	$data['parent_id'] = $parent_id;
		$result = array( 
			'html' => $this->load->view('familles/alertes/form_gerer_mail_alerte', $data, TRUE)
		);
		echo json_encode($result);
	}
	
	public function save_type_envoie_alerte(){
		$parent_id = $this->input->post('parent_id');
		$type_envoie = $this->input->post('type_envoie');
		
		$type_envoie_mail_alerte = array(
			'type_envoie_alerte_id' => $type_envoie
		);
		
		$this->parent->update($type_envoie_mail_alerte, $parent_id);
		$this->send_mail_alerte_parent($parent_id);
	}
	
	public function delete_alerte(){
		$alerte_id = $this->input->post('alerte_id');
		$alerte = $this->alerte_model->get_alerte_by_id($alerte_id);
		
		// Suppression alerte
		$this->alerte_model->delete_alerte($alerte_id);
		
		//Maj de la liste des alertes parametrées restantes
		if($alerte){
			$enfant = $this->enfant->get_enfant_by_id($alerte->enfant_id);
			$data['alerte_parametre_list'] = get_alerte_parametre_list_by_enfant_id($alerte->enfant_id);
			$result = array( 
				'enfant' => $enfant,
				'html_tableau_alertes_parametre' => $this->load->view('familles/alertes/tableau_alertes_parametre', $data, TRUE)
			);
			echo json_encode($result);
		}
	}
	
	public function data_form_creer_alerte($alerte, $enfant){
		$ville = false;
		$struture_selected = array();
		$type_accueils = array();
     	$jours_particulier = array();
     	$crenau_regulier = array();
     	$deb_periode = '';
     	$fin_periode = '';
     	$structure_ville_list = array();
     	
		$critere_type_accueil['not_show_temps_adaptation'] = 1;
		$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list_by_more_criteria($critere_type_accueil);
		if(isset($alerte) && isset($alerte->id) && !empty($alerte->id)){
			$struture_selected = isset($alerte->structures) ? get_object_id_list($alerte->structures) : array();
			$type_accueils = isset($alerte->type_accueils) ? get_object_id_list($alerte->type_accueils) : array();
	     	$jours_particulier = isset($alerte->jour_particuliers) ? get_object_id_list($alerte->jour_particuliers) : array();
	     	$crenau_regulier = isset($alerte->creneau_reguliers) ? get_object_id_list($alerte->creneau_reguliers) : array();
	     	$deb_periode = isset($alerte->deb_periode) ? date_en_to_fr($alerte->deb_periode, '-', '/') : '';
	     	$fin_periode = isset($alerte->fin_periode) ? date_en_to_fr($alerte->fin_periode, '-', '/') : '';
	     	$ville_id = isset($alerte->ville_id) ? $alerte->ville_id : false;
	     	
			$critere_stucture['structure_active'] = 1;
	     	if($ville_id){
				$ville = $this->ville_model->get_ville_by_id($ville_id);
				$critere_stucture['ville_id_list'] = array($ville_id);
			}
			$structure_ville_list = $this->structure_model->get_structure_list_by_more_criteria($critere_stucture);
		}else if($enfant && isset($enfant->ville_id) && $enfant->ville_id != 0){
			$ville = $this->ville_model->get_ville_by_id($enfant->ville_id);
			if($ville){
				$critere_stucture['structure_active'] = 1;
				$critere_stucture['ville_id_list'] = array($ville->id);
				$structure_ville_list = $this->structure_model->get_structure_list_by_more_criteria(array('ville_id_list' => array($ville->id)));
			}
		}
		
     	$data['ville'] = $ville;
		$data['field_nom_ville'] =  array(
			'type'  => 'text',
            'name' 	=> 'nom_ville',
            'class'	=> 'form-control',
			'value' => $ville ? $ville->nom : '',
			'placeholder' => 'Ville ou Code Postal'
     	);
     	$data['field_structure'] =  array(
     		'name' 	  	=> 'structure[]',
     		'options' 	=> get_dropdown_option_array($structure_ville_list, 'id', 'nom'),
     		'value_selected' => $struture_selected,
     		'extra' => 'id="structure" class="selectpicker form-control" data-none-results-text="Aucun résultat trouvé pour " data-selected-text-format="count > 3" data-live-search="true" multiple'
     	);
     	
     	$data = array_merge($data, $this->data_criteres_recherche('', $type_temps_accueil_list, $type_accueils, $jours_particulier, $crenau_regulier, $deb_periode, $fin_periode, true));
     	return $data;
	}
	
	function menu_famille_resultats_alerte($action){
		$parent = $this->get_parent_by_user_connected();
		$data['enfant_list'] = $this->data_enfant_alerte($parent->id);
		return $this->load->view('familles/'. $action, $data, TRUE);
	}
	
	function data_enfant_alerte($parent_id){
		if(isset($parent_id)){
			$enfant_list = $this->enfant->get_enfant_list(array('parent_id' => $parent_id));
			foreach ($enfant_list as $enfant){
				$enfant->enfant_photo = base_url().$this->enfant->get_photo_enfant($enfant->parent_id, $enfant->enfant_id);
				$enfant->disponibilite_list = $this->get_disponibilite_result_alerte($enfant->enfant_id, $parent_id);
			}
			return $enfant_list;
		}
		return null;
	}
	
	function get_disponibilite_result_alerte($enfant_id, $parent_id = null, $mail = false){
		$criteres_list = $this->get_criteres_dispo_by_alerte_list(get_alerte_parametre_list_by_enfant_id($enfant_id));
		$disponibilite_merge_list = array();
		if($criteres_list && count($criteres_list) > 0){
			foreach ($criteres_list as $criteres){
				$disponibilite_list = $this->get_disponibilite_list_info($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));
				if($disponibilite_list && count($disponibilite_list) > 0){
					foreach ($disponibilite_list as $disponibilite){
						if(!in_array_object($disponibilite_merge_list, "id", $disponibilite->id)){
							if($mail){
								$mail_alerte_disponibilite = array(
									'disponibilites_id' => $disponibilite->id,
									'enfant_id' => $enfant_id,
									'parent_id' => $parent_id
								);
								if(!$this->mail_alerte_disponibilite_model->get_mail_alerte_disponibilite($mail_alerte_disponibilite)){
									$this->mail_alerte_disponibilite_model->add($mail_alerte_disponibilite);
									array_push($disponibilite_merge_list, $disponibilite);
								}
							}else {
								array_push($disponibilite_merge_list, $disponibilite);
							}
						}
					}
				}
			}
		}
		return $disponibilite_merge_list;
	}
	
	public function load_mes_alertes_parametrage_dialog(){
		$enfant_id = $this->input->post('enfant_id');
		
		$enfant = $this->enfant->get_enfant_by_id($enfant_id);
		$alerte = get_parametre_alerte_by_enfant_id($enfant_id);
		if(!$alerte){
			$alerte = new stdClass();
			$alerte->id = 0;
		}
		
		$enfant->alerte = $alerte;
		$data = array(
			'enfant' => $enfant
		);
		$data = array_merge($data, $this->data_form_field_mes_alertes_parametrage_dialog($enfant));
		$result = array( 
			'enfant' => $enfant, 
			'html_mes_alertes_parametrage' => $this->load->view('familles/mes_alertes_parametrage_dialog', $data, TRUE)
		);
		echo json_encode($result);
	}
	
	public function save_alerte(){
		$alerte_id = $this->input->post('alerte_id');
		$parent_id = $this->input->post('parent_id');
		$enfant_id = $this->input->post('enfant_id');
		$ville_id = $this->input->post('ville_id');
		$emails = $this->input->post('emails');
		$structure_selected = $this->input->post('structure');
		$type_accueils = $this->input->post('type_accueils');
		$jours_particulier = $this->input->post('jour_particulier');
		$crenau_regulier = $this->input->post('crenau_regulier');
		$deb_periode = $this->input->post('periode_debut');
		$fin_periode = $this->input->post('periode_fin');
		$mode_envoie = $this->input->post('mode_envoie');
		
		$alerte_nom = $this->get_name_alerte($enfant_id);
		$alerte = array(
			'id' => $alerte_id,
			'parent_id' => $parent_id,
			'enfant_id' => $enfant_id,
			'emails' => $emails,
			'deb_periode' => $deb_periode ? date_fr_to_en($deb_periode, '/', '-') : null,
			'fin_periode' => $fin_periode ? date_fr_to_en($fin_periode, '/', '-') : null,
			'email_par_semaine' => $mode_envoie
		);
		if(!$alerte_id || $alerte_id == 0){
			$alerte['nom'] = $alerte_nom;
		}
		if($ville_id && !empty($ville_id)){
			$alerte['ville_id'] = $ville_id;
		}else {
			$alerte['ville_id'] = null;
		}
		$alerte_data = array(
			'alerte' => $alerte,
			'structures' => $structure_selected ? $structure_selected : array(),
			'type_accueils' => $type_accueils ? $type_accueils : array(),
			'jours_particulier' => $jours_particulier ? $jours_particulier : array(),
			'crenau_regulier' => $crenau_regulier ? $crenau_regulier : array()
		);
		
		$alerte_id = $this->alerte_model->save_alerte($alerte_data);
		$this->send_mail_alerte_parent($parent_id);
		
		if($alerte_id){
			$enfant = $this->enfant->get_enfant_by_id($enfant_id);
			$data['alerte_parametre_list'] = get_alerte_parametre_list_by_enfant_id($enfant_id);
			$result = array( 
				'enfant' => $enfant,
				'html_tableau_alertes_parametre' => $this->load->view('familles/alertes/tableau_alertes_parametre', $data, TRUE)
			);
			echo json_encode($result);
		}
	}
	
	public function get_name_alerte($enfant_id){
		$nb_alerte = $this->alerte_model->get_nombre_alerte_enfant($enfant_id);
		return "Alerte".((int)$nb_alerte + 1);
	}
	
	public function data_form_field_mes_alertes_parametrage_dialog($enfant){
		if($enfant){
			$alerte = $enfant->alerte;
			$recursion_creneau_list = $this->recursion_creneau_model->get_recursion_creneau_list();
			$jour_list = $this->jour_model->get_jour_list();
			
			$ville = false;
			$envoie_mail = 1;
			$structure = false;
			$emails = '';
			$struture_selected = isset($alerte) && isset($alerte->structures) ? get_object_id_list($alerte->structures) : array();
			$type_accueils = array();
	     	$jours_particulier = array();
	     	$crenau_regulier = array();
	     	$deb_periode = '';
	     	$fin_periode = '';
	     	$structure_ville_list = array();
		     	
			
			$critere_type_accueil['not_show_adaptation'] = 1;
			/*$critere_type_accueil['date_naissance'] = $enfant->date_naissance;
			if($struture_selected && count($struture_selected) > 0){
				$critere_type_accueil['struture_id_list'] = $struture_selected;
			}*/
			/*$tout_type_acceuil = new stdClass();
			$tout_type_acceuil->id = 0;
			$tout_type_acceuil->nom = "Tout type d'accueil";
			$type_accueils = array(0);
			
			$type_temps_accueil_list[0] = $tout_type_acceuil;
			$type_temps_accueil_list = array_merge($type_temps_accueil_list, $this->type_temps_accueil_model->get_type_temps_accueil_list_by_more_criteria($critere_type_accueil));*/
			
			$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list_by_more_criteria($critere_type_accueil);
			$emails = $enfant->parent_email;
			if(isset($alerte) && $alerte->id != 0){
				$envoie_mail = $alerte->email_par_semaine;
				$structure_list = array();
				if(count($struture_selected) > 0){
					$structure_list = $this->structure_model->get_all_structure(array('id' => $struture_selected[0]));
				} 
				if(count($structure_list) > 0) {
					$structure = $structure_list[0];
					$ville = new stdClass();
					$ville->id = $structure->ville_id;
					$ville->nom = $structure->ville_nom;
				}
				
				$emails = $alerte->emails;
				$type_accueils = $alerte->type_accueils ? get_object_id_list($alerte->type_accueils) : array();
		     	$jours_particulier = $alerte->jour_particuliers ? get_object_id_list($alerte->jour_particuliers) : array();
		     	$crenau_regulier = $alerte->creneau_reguliers ? get_object_id_list($alerte->creneau_reguliers) : array();
		     	$deb_periode = date_en_to_fr($alerte->deb_periode, '-', '/');
		     	$fin_periode = date_en_to_fr($alerte->fin_periode, '-', '/');
			}else if(isset($enfant->ville_id) && $enfant->ville_id != 0){
				$ville = $this->ville_model->get_ville_by_id($enfant->ville_id);
			}
			
			$structure_ville_list = array();
			if($ville){
				$structure_ville_list = $this->structure_model->get_structure_list_by_more_criteria(array('ville_id_list' => array($ville->id), 'date_naissance' => $enfant->date_naissance));
			}
			
	     	$data['ville'] = $ville;
			$data['field_email'] =  array(
				'type'  => 'text',
	            'name' 	=> 'emails',
	            'class'	=> 'form-control',
				'value' => $emails,
				'placeholder' => 'Liste de mail pour envoyer l\'alerte'
	     	);
	     	$data['field_envoie_mail'] =  array(
				'name' 	  	=> 'mode_envoie',
	     		'options' 	=> array(
	     			0 => 'D&egrave;s que la disponibilit&eacute; existe',
	     			1 => 'Une fois par semaine',
	     		),
	     		'value_selected' => array($envoie_mail),
	     		'extra' => 'class="selectpicker form-control"'
	     	);
			$data['field_nom_ville'] =  array(
				'type'  => 'text',
	            'name' 	=> 'nom_ville',
	            'class'	=> 'form-control',
				'value' => $ville ? $ville->nom : '',
				'placeholder' => 'Ville de l\'&eacute;tablissement'
	     	);
	     	$data['field_structure'] =  array(
	     		'name' 	  	=> 'structure[]',
	     		'options' 	=> get_dropdown_option_array($structure_ville_list, 'id', 'nom'),
	     		'value_selected' => $struture_selected,
	     		'extra' => 'id="structure" class="selectpicker form-control" multiple'
	     	);
	     	
	     	$data = array_merge($data, $this->data_criteres_recherche('', $type_temps_accueil_list, $type_accueils, $jours_particulier, $crenau_regulier, $deb_periode, $fin_periode, true));
	     	return $data;
		}
		return null;
	}
	
	public function get_criteres_dispo_by_alerte_list($alerte_list){
		$criteres_list = array();
		if($alerte_list){
			foreach ($alerte_list as $alerte){
				$criteres = array();
				$criteres['parent_id'] = $alerte->parent_id;
				$criteres['enfant_id'] = $alerte->enfant_id;
				if($alerte->structures){
					$criteres['structure_id_list'] = get_object_id_list($alerte->structures);
				}else if(isset($alerte->ville_id) && $alerte->ville_id != null){
					$critere_structure['ville_id_list'] = array($alerte->ville_id);
					$critere_structure['structure_active'] = 1;
					$criteres['structure_id_list'] = get_object_id_list($this->structure_model->get_structure_list_by_more_criteria($critere_structure));
				}else {
					$critere_structure['structure_active'] = 1;
					$criteres['structure_id_list'] = get_object_id_list($this->structure_model->get_structure_list_by_more_criteria($critere_structure));
				}
				
				if($alerte->type_accueils){
					$criteres['type_accueils'] = get_object_id_list($alerte->type_accueils);
				}
				if($alerte->jour_particuliers){
					$criteres['jours_particulier'] = get_object_id_list($alerte->jour_particuliers);
				}
				if($alerte->creneau_reguliers){
					$criteres['crenau_regulier'] = get_object_id_list($alerte->creneau_reguliers);
				}
				if(isset($alerte->deb_periode) && $alerte->deb_periode != null){
					$criteres['deb_periode'] = $alerte->deb_periode;
				}
				if(isset($alerte->fin_periode) && $alerte->fin_periode != null){
					$criteres['fin_periode'] = $alerte->fin_periode;
				}
				$enfant = $this->enfant->get_enfant_by_id($criteres['enfant_id']);
				if($enfant){
					$criteres['date_naissance'] = $enfant->date_naissance;
				}
				
				$criteres['date_sup_equals_date_du_jour'] = 1;
				$criteres['show_rdv_adaptation'] = 0;
				array_push($criteres_list, $criteres);
			}
		}
		return $criteres_list;
	}
	
	public function send_mail_inscription($dataParent) {		
		$data = array(
			'fullNameParent' => $dataParent['nom'] . " " . $dataParent['prenom'],
			'titleParent' => get_title_by_id($dataParent['titre']));  
		$template = $this->config->item('famille_email_templates').$this->config->item('famille_email_confirmation_inscription');
		$message = $this->load->view($template, $dataParent, true);
		$this->email->clear();
		$this->email->from($this->config->item('admin_email', 'ion_auth'), $this->config->item('site_title', 'ion_auth'));
		$this->email->to($dataParent['email']);
		$this->email->subject($this->config->item('site_title', 'ion_auth') . ' - ' . $this->config->item('famille_email_subject_inscription'));
		$this->email->message($message);
		$this->email->send();
	}
	
	public function add_new_enfant() {
		$nbEnfants = $this->input->post('nbEnfants');
		$idParent = $this->input->post('parent_id');
		
		$data['parent_role'] = get_role("Vous êtes"); 
		$data['parent_role_no_title'] = get_role();
		$data['nbEnfants'] = $nbEnfants;
		$data['cptEnfant'] = $nbEnfants;
		$data['parent_id'] = $idParent;	
		$data['photo_enfant_default'] = 	$this->config->item('dir_photo_enfant').'enfant_default.png';
		//CRA : fix
		//$data['lstVilleJson'] = $this->ville_model->get_ville_json(array(), true);
		
		$data['rowEnfant'] = (object)(array("enfant_id" => $nbEnfants . "-new", "enfant_is_same_addresse" => 1));		
		$dataView = $this->load->view('familles/enfant', $data, TRUE);
			
		$result = array("dataView" => $dataView);	
		echo json_encode($result);	
	}
	

	
	function voir_disponibilite_enfant($id = 0) {
		$data['id_enfant'] = $id;				
		$this->load->view('familles/voir_disponibilite_enfant', $data);	
	}
	
	function voir_adaptation_enfant($id = 0) {
		$data['id_enfant'] = $id;				
		$this->load->view('familles/voir_adaptation_enfant', $data);			
	}
	
	public function delete_enfant() {
		$idEnfant = $this->input->post('id_enfant');
		$idParent = $this->input->post('parent_id');
		if ($this->enfant->delete($idEnfant)) {		
			//Supprimer la photo de l'enfant
			$photo = $this->enfant->get_photo_enfant($idParent, $idEnfant);
			if (!empty($photo) && !strpos($photo, "enfant_default") ) {
				unlink($photo);
			}			
			$message = "Suppression effectuée avec succès";
			$result = array("message" => $message);
		} else {
			$message = "Erreur trouvée lors de la suppresion";
			$result = array("error" => $message);
		}			
		echo json_encode($result);
	}
	
	function check_info_parent_enfant() {
		$idEnfant = $this->input->post('idEnfant');
		$criteres = array('enfant_id' => $idEnfant);
		$infoParentFamille = $this->enfant->get_enfant_list($criteres);
		$response = current($infoParentFamille);
		echo json_encode($response);
	}
	
			
	public function check_exist_mail(){

		$email = trim($this->input->post('email'));
		$id_user = trim($this->input->post('id_user_parent'));
		
		$this->load->model('users_groups_model','users');
		
		// Vérifier si l'email existe déjà dans la base
		$criteres = array("email" => $email, "not_id_user" => $id_user);
		$isValid = count($this->users->get_users($criteres)) > 0 ? "exist" : "none";

		echo json_encode(array(
    		'valid' => $isValid,
		));
	}
	
	public function check_ville(){
		$ville = trim($this->input->post('ville'));

		$isValid = $this->ville_model->is_valid_name($ville);
		$isValid = $isValid ? "exist" : "none";

		echo json_encode(array(
    		'valid' => $isValid,
		));
	}
	
	public function check_ville_codePostal(){
		$ville = trim($this->input->post('ville'));
		$codePostal = $this->input->post('code_postal');
		
		$villeInDatabase = $this->ville_model->get_ville_by_code_postal($codePostal);
		$first_ville = new stdClass();
		if (empty($villeInDatabase)) {
			$first_ville->id = '';
			$first_ville->nom = '';
		} else {
			$first_ville = current($villeInDatabase);
		}
		
		$isValid = false;		
		if ($ville == $first_ville->nom) {
			$isValid = true;
		}		
		$isValid = $isValid ? "exist" : "none";

		echo json_encode(array(
    		'valid' => $isValid,
			'id' => $first_ville->id,
			'nom' => $first_ville->nom
		));
	}	
	
	//Mettre à jour les étapes
	public function valider_inscription() {
		$dataRequest = $this->input->post();
		$dataFile = $_FILES;
		
		$isViaMenuEnfant= isset($dataRequest['is_via_menu_enfant']) ? $dataRequest['is_via_menu_enfant'] : 0;	
		$idParent= isset($dataRequest['parent_id']) ? $dataRequest['parent_id'] : 0;
		$typeMenuRetour= isset($dataRequest['type_menu_retour']) ? $dataRequest['type_menu_retour'] : 0;
		$typeActionEnfant = isset($dataRequest['type_action_enfant_value']) ? $dataRequest['type_action_enfant_value'] : 0;
		
		$isConnection = false;
		
		$dataParent = array();
		if (!empty($idParent)) {
			$dataParent['id'] = $idParent;
		}
		$dataParent['titre'] = isset($dataRequest['titre']) ? $dataRequest['titre'] : '';
		$dataParent['situation_maritale'] = isset($dataRequest['situation_maritale']) ? $dataRequest['situation_maritale'] : '';
		$dataParent['nom'] = isset($dataRequest['nom']) ? $dataRequest['nom'] : '';
		$dataParent['prenom'] = isset($dataRequest['prenom']) ? $dataRequest['prenom'] : '';
		$dataParent['role'] = isset($dataRequest['role']) ? $dataRequest['role'] : '';
		$dataParent['email'] = isset($dataRequest['email']) ? $dataRequest['email'] : '';
		$dataParent['complement'] = isset($dataRequest['complement']) ? $dataRequest['complement'] : '';
		$dataParent['tel1'] = isset($dataRequest['tel1']) ? $dataRequest['tel1'] : '';
		$dataParent['adresse'] = isset($dataRequest['adresse']) ? $dataRequest['adresse'] : '';
		$dataParent['ville_id'] = isset($dataRequest['parent_id_ville']) ? $dataRequest['parent_id_ville'] : '';
		
		if (empty($dataParent['ville_id'])) {			
			$ville = isset($dataRequest['ville']) ? $dataRequest['ville'] : '';
			if (!empty($ville)) {
				$dataParent['ville_id'] = $this->ville_model->get_id_by_name($ville);
			}
			if (empty($dataParent['ville_id'])) {
				unset($dataParent['ville_id']);
			}
		}
		$dataParent['code_postal'] = isset($dataRequest['code_postal']) ? $dataRequest['code_postal'] : '';
		$password = isset($dataRequest['password']) ? $dataRequest['password'] : '';
		//begin transaction
		$this->db->trans_start();
						
		//Ajouter un utilisateur 
		$idUser = isset($dataRequest['id_user_parent']) ? $dataRequest['id_user_parent'] : 0;	
		
		//Information pour PARENT		
		if (empty($isViaMenuEnfant)) {	
			if (empty($idUser)) {
				$additional_data = array(
						'first_name' => $dataParent['prenom'],
						'last_name'  => $dataParent['nom'],
						'active' => 1 // 0 : pour Directeur , 1 : pour Parents
					);
				$username =  $dataParent['nom'] . " " . $dataParent['prenom'];			
				$email = $dataParent['email'];		
				$idUser = $this->ion_auth_model->register($username, $password, $email, $additional_data);
				if ($idUser) {					
					//Connecter l'utilisateur
					$action = 'login_parent';
					if ($this->ion_auth->login($email, $password)) {
						if($this->config->item('user_expire', 'ion_auth') === 0)
						{
							$expire = (60*60*24*365*2);
						}
						else
						{
							$expire = $this->config->item('user_expire', 'ion_auth');
						}
						set_cookie(array(
								'name'   => 'action_login',
								'value'  => $action,
								'expire' => $expire
						));
					}	
					$this->load->model('groups_model','groups');
					$this->ion_auth->add_to_group($this->groups->get_famille(), $idUser); 
					
					$isConnection = true;
					
					//Envoi email de confirmation
					$dataParent_mail = $dataParent;
					$dataParent_mail['password'] = $password;
					$res = $this->send_mail_inscription($dataParent_mail);											
					//Fin envoi email										
				} 	
			} else {
				$additional_data = array(
					'username' => $dataParent['nom']. " ". $dataParent['prenom'],
					'first_name' => $dataParent['prenom'],
					'last_name'  => $dataParent['nom'],
					'email' => $dataParent['email'],				
				);
				//Mettre à jour les informations concernants l'utilisateur connecté
				$res = $this->ion_auth->update($idUser, $additional_data);
			}
			
			$dataParent['users_id'] = $idUser;

			if (empty($idParent)) {
				//Nouvelle insertion
				$idParent = $this->parent->insert($dataParent);
			} else {
				//Modification
				$this->parent->update($dataParent,  $idParent);
			}
		}
		
		$voirDisponibilite = $voirAdaptation = "";
	
		//Pour ENFANTS		
		if (!empty($idParent)) {
			$dataParent['users_id'] = $idUser;
			$tabId = isset($dataRequest['enfant_id']) ? $dataRequest['enfant_id'] : array(); 
			
			$tabNom = isset($dataRequest['enfant_nom']) ? $dataRequest['enfant_nom'] : array();
			$tabPrenom = isset($dataRequest['enfant_prenom']) ? $dataRequest['enfant_prenom'] : array();
			$tabAdresse = isset($dataRequest['enfant_adresse']) ? $dataRequest['enfant_adresse'] : array();			
			
			$tabIsSameAdresse = isset($dataRequest['enfant_is_same_adresse']) ? $dataRequest['enfant_is_same_adresse'] : array();
			$tabComplement = isset($dataRequest['enfant_complement']) ? $dataRequest['enfant_complement'] : array();
			$tabCodePostal = isset($dataRequest['enfant_code_postal']) ? $dataRequest['enfant_code_postal'] : array();
			$tabVilleId = isset($dataRequest['enfant_ville_id']) ? $dataRequest['enfant_ville_id'] : array();
						
			$tabDateNaissance = isset($dataRequest['enfant_date_naissance']) ? $dataRequest['enfant_date_naissance'] : array();
			$tabAllergie = isset($dataRequest['enfant_handicap_allergie']) ? $dataRequest['enfant_handicap_allergie'] : array();
			$tabHabitude = isset($dataRequest['enfant_habitude']) ? $dataRequest['enfant_habitude'] : array();
			$tabNomAccompagnateur= isset($dataRequest['enfant_nom_accompagnateur']) ? $dataRequest['enfant_nom_accompagnateur'] : array();
						
			$tabSituationParent = isset($dataRequest['enfant_situation_parent']) ? $dataRequest['enfant_situation_parent'] : array();		
			$tabNomParent = isset($dataRequest['enfant_nom_parent']) ? $dataRequest['enfant_nom_parent'] : array();
			$tabPrenomParent = isset($dataRequest['enfant_prenom_parent']) ? $dataRequest['enfant_prenom_parent'] : array();
			$tabEmailParent = isset($dataRequest['enfant_email_parent']) ? $dataRequest['enfant_email_parent'] : array();
			$tabEnfantPhoto = isset($dataFile['enfant_photo']) ? $dataFile['enfant_photo'] : array();
			
			$tabIsSameAdresse = isset($dataRequest['enfant_is_same_adresse_array']) ? $dataRequest['enfant_is_same_adresse_array'] : array();			
			if ($tabIsSameAdresse) {
				$tabIsSameAdresse = explode("," , $tabIsSameAdresse);
			}
			
			if (!empty($tabId)) {
				$enfant = 0;
				foreach ($tabId as $key => $enfantId) {		
					$dataEnfant = array();		
					$enfantIdArray = explode('-new', $enfantId);
					$isNewEnfant = false;
					$enfantIdOriginal = $enfantId;
					
					$dataEnfant['nom'] = $tabNom[$key];
					$dataEnfant['prenom'] = $tabPrenom[$key];
					
					//$dataEnfant['is_same_adresse'] 	= isset($tabIsSameAdresse[$key]) ? (($tabIsSameAdresse[$key]==true || $tabIsSameAdresse[$key]=='true' || $tabIsSameAdresse[$key]=='on' || $tabIsSameAdresse[$key]==1) ? 1 : 0) : 0;
					$dataEnfant['is_same_adresse'] = 0;				
					if ($tabIsSameAdresse[$key] == 'true' ) {
						$dataEnfant['is_same_adresse'] = 1;
					}		

					if (empty($dataParent['nom']) && (!empty($idParent))) {
						$resParent = $this->parent->get_parent_list(array("parent_id" => $idParent));
						$dataParent = current($resParent);
						$dataParent = (array) $dataParent;						
					}
					if ($dataEnfant['is_same_adresse'] == 1) {
						$dataEnfant['adresse'] = $dataParent['adresse'];
						$dataEnfant['complement'] = $dataParent['complement'];	
						$dataEnfant['code_postal'] = $dataParent['code_postal'];
						$dataEnfant['ville_id'] = $dataParent['ville_id'];
					} else {
						$dataEnfant['adresse'] = $tabAdresse[$key];
						$dataEnfant['complement'] = $tabComplement[$key];	
						$dataEnfant['code_postal'] = $tabCodePostal[$key];	
						$dataEnfant['ville_id'] = isset($tabVilleId[$key]) ? $tabVilleId[$key] : null;	
					}			
					
					$dataEnfant['date_naissance'] =date_fr_to_en($tabDateNaissance[$key], "/", "-");
					$dataEnfant['allergie'] = $tabAllergie[$key];
					$dataEnfant['habitude'] = $tabHabitude[$key];
					$dataEnfant['nom_accompagnateur'] = $tabNomAccompagnateur[$key];
					
					$dataEnfant['situation_parent'] = $tabSituationParent[$key];
					$dataEnfant['nom_parent'] = $tabNomParent[$key];
					$dataEnfant['prenom_parent'] = $tabPrenomParent[$key];
					$dataEnfant['email_parent'] = $tabEmailParent[$key];
					
					if (count($enfantIdArray) > 1) {
						$isNewEnfant = true;
						$dirPhotoEnfant = $this->config->item('dir_photo_enfant');
						//Nouvelle insertion
						$enfantId = $this->enfant->insert($dataEnfant, $idParent);
					} else {
						//Modification
						$this->enfant->update($dataEnfant, $enfantId);
					}
					
					
								
					if (!empty($enfantId)) {
						//UPLOAD PHOTO
						$dirPhotoEnfant = $this->config->item('dir_photo_enfant') . "/" . $idParent;
						if(!is_dir($dirPhotoEnfant)){
							mkdir($dirPhotoEnfant,0777,true);
						}
						if (!empty($tabEnfantPhoto) && (isset($tabEnfantPhoto['name'][$enfant]))) {
							$_FILES['photo']['name']= $tabEnfantPhoto['name'][$enfant];
					        $_FILES['photo']['type']= $tabEnfantPhoto['type'][$enfant];
					        $_FILES['photo']['tmp_name']= $tabEnfantPhoto['tmp_name'][$enfant];
					        $_FILES['photo']['error']= $tabEnfantPhoto['error'][$enfant];
					        $_FILES['photo']['size']= $tabEnfantPhoto['size'][$enfant]; 
				        
							$config_photo_enfant = array(
								'upload_path' 	=>  $dirPhotoEnfant,
								'allowed_types' => 'gif|jpeg|jpg|png',
								'file_name'		=>	$enfantId,
								'overwrite'		=> 	true
							);
							$this->load->library('upload', $config_photo_enfant);
							$this->upload->initialize($config_photo_enfant);
						
							//$photoName = 'enfant_photo_'.$enfantIdOriginal;
							$photoName = 'photo';						
							if ((isset($_FILES[$photoName])) && (!empty($_FILES[$photoName]['name']))) {
								//$this->upload->do_upload($photoName);
								if ( ! $this->upload->do_upload($photoName))
								{
										$error = array('error' => $this->upload->display_errors());
											// print_r($error);
								}
								else
									{
										$data_photo = array('upload_data' => $this->upload->data());
										$image_uploaded = $data_photo['upload_data']['full_path'];
										$this->image_moo->load($image_uploaded)
										->resize_crop(64,64)
										->border(3, "#ffffff")
										->border(1, "#000000")
										->save($image_uploaded,true);
								}
							}
						}		
					} //end if (!empty($enfantId)) 
					
					$enfant++;
					if ($enfant == 1) {
						$firstEnfantPrenom = empty($dataEnfant['prenom']) ? $dataEnfant['nom'] : $dataEnfant['prenom'];
						/*
						$voirDisponibilite = "<a href='". site_url("famille/voir_disponibilite_enfant/".$enfantId) ."'>Voir les accueils disponibles pour $firstEnfantPrenom</a>";
						$voirAdaptation = "<a href='". site_url("famille/voir_adaptation_enfant/".$enfantId) ."'>Prendre un RDV d’adaptation pour $firstEnfantPrenom</a>";
						*/
						$jsVoirDisponibilite = 'rechercher_dispo_enfant(1, '. $enfantId. ');';
						$jsVoirAdaptation = 'rechercher_dispo_enfant(2, '. $enfantId. ');';
						$voirDisponibilite = "<a onclick='" . $jsVoirDisponibilite . "'>Voir les accueils disponibles pour $firstEnfantPrenom</a>";
						$voirAdaptation = "<a onclick='" . $jsVoirAdaptation . "'>Prendre un RDV d’adaptation pour $firstEnfantPrenom</a>";						
					}
												
				} //end foreach ($tabId as $key => $enfantId) {	
			} //end if (!empty($tabId)) {
		}//end if (!empty($idParent)) {
		
		//end transaction
		$this->db->trans_complete();
		
		//A faire après COMMIT
		if ($isConnection) {
			//Sauvegarder les informations dans login si nouveau parent
			//CHERCHER les information concernant l'utilisateur
			$dataUserLogin = array();
		 	$dataUserLogin['isLoginParent'] = 1;
			$user = $this->ion_auth->user()->row();
			$idUserParent = $user->id;
			$this->load->model('parent_model','parent');
			$criteres = array('users_id' => $idUserParent);
			$infoParent = $this->parent->get_parent_list($criteres);	 
			$dataUserLogin['infoParent'] = $infoParent;	
			$this->session->unset_userdata("data_login");
	 		$this->session->set_userdata("data_login", $dataUserLogin);
		}
		//var_dump($dataRequest);	
		if ($isNewEnfant == true) {
			$message = "L'ajout a bien été pris en compte";
		} else {
			$message = "Vos modifications ont bien été prises en compte";
		}
		$result = array("message" => $message,
						"voirDisponibilite" => $voirDisponibilite,
						"voirAdaptation" => $voirAdaptation,
						'status' =>	'success',
						'is_via_menu_enfant' => $isViaMenuEnfant,
						'typeActionEnfant' => $typeActionEnfant,
						'type_menu_retour' => $typeMenuRetour
				);	
		echo json_encode($result);			
	}
	
	public function inscription_famille($id = 0) {	
		$this->do_inscription_famille($id);
	}
	
	public function do_inscription_famille($id = 0, $isViewOnly = false, $step = 0, $idEnfant = 0, $idStructure = 0, $typeActionEnfant =0, $typeMenu = 0, $passe_ou_futur = 0) {		
		$this->data = array();
		//Si l'utilisateur connaisse l'id , il peut modifier n'importe qui
		if (!$this->ion_auth->logged_in() && (!empty($id))) {
			$id = 0;
		}
		$validationChecked = "";
		//Traitement de l'étape n°1 - PARENT
		if (!empty($id)) {		
			$criteres = array('parent_id' => $id);
			$infoParentFamille = $this->parent->get_parent_list($criteres);	 
			$this->data['infoParent'] = $infoParentFamille;	
			
			if (!empty($infoParentFamille)) {
				$infoParentFamille = current($infoParentFamille);
				$id = $infoParentFamille->id;
				$title = $infoParentFamille->titre;
				$nom = $infoParentFamille->nom;
				$prenom = $infoParentFamille->prenom;
				$role = $infoParentFamille->role;
				$email = $infoParentFamille->email;
				$adresse = $infoParentFamille->adresse;
				$complement= $infoParentFamille->complement;				
				$tel1= $infoParentFamille->tel1;
				$tel2= $infoParentFamille->tel2;
				$code_postal= $infoParentFamille->code_postal;
				$ville_id= $infoParentFamille->ville_id;
				$ville ="";
				if (!empty($ville_id)) {
					$ville = $this->ville_model->get_name_by_id($ville_id);
				}
				//Sauvegarder les information des parents quelque soit l'étape
				$this->data['parent_enfant_adresse'] = $adresse;
				$this->data['parent_enfant_code_postal'] = $code_postal;
				$this->data['parent_enfant_complement'] = $complement;
				$this->data['parent_enfant_ville_id'] = $ville_id;
				$this->data['parent_enfant_ville'] = $ville;
				
				$situation_maritale = $infoParentFamille->situation_maritale;
				$users_id = $infoParentFamille->users_id;
				$validationChecked = "checked";
							
			}
		}
		
		$this->data['id_user_parent'] = isset($users_id) ? $users_id : 0;
		$this->data['validationChecked'] = $validationChecked;
		
		$this->data['id_parent'] = isset($id) ? $id : 0;
		$this->data['ville_id'] = isset($ville_id) ? $ville_id : "";
		
		$this->data['title'] = isset($title) ? $title : 0;
		$this->data['parent_title'] = get_title("Titre");
		
		$this->data['role'] = isset($role) ? $role : 0;
		$this->data['parent_role'] = get_role("Vous êtes");
		$this->data['parent_role_no_title'] = get_role();
		
		$this->data['situation_maritale'] = isset($situation_maritale) ? $situation_maritale : 0;
		$this->data['parent_situation_maritale'] = get_situation_maritale("Situation maritale");
		

		$this->data['nom'] = array(
              'name'        => 'nom',
              'placeholder' =>  "Nom du parent",
              'class' => "form-control"	,
			  'value' => isset($nom) ? $nom : '',
		);
		$this->data['prenom'] = array(
              'name'        => 'prenom',
              'placeholder' =>  "Prénom du parent",
              'class' => "form-control",
			  'value' => isset($prenom) ? $prenom : '',
		);
		$this->data['email'] = array(
              'name'        => 'email',
			  'id'        => 'email',
              'placeholder' =>  "Email du parent",
              'class' => "form-control",
			  'value' => isset($email) ? $email : '',
		);
		$this->data['confirmation_email'] = array(
              'name'        => 'confirmation_email',
              'placeholder' =>  "Confirmation du parent",
              'class' => "form-control",
			  'value' => isset($email) ? $email : '',
		);
		$this->data['adresse'] = array(
              'name'        => 'adresse',
			  'id'        => 'adresse',
              'placeholder' =>  "n° Rue + Adresse",
              'class' => "form-control",
			  'value' => isset($adresse) ? $adresse : '',
		);
		$this->data['complement'] = array(
              'name'        => 'complement',
			  'id'        => 'complement',
              'placeholder' =>  "Complément d'adresse",
              'class' => "form-control",
			  'value' => isset($complement) ? $complement : '',
		);
		$this->data['tel1'] = array(
              'name'        => 'tel1',
			  'id'        => 'tel1',
              'placeholder' =>  "Téléphone",
              'class' => "form-control",
			  'value' => isset($tel1) ? $tel1 : '',
		);		
		
		$this->data['code_postal'] = array(
              'name'        => 'code_postal',
			  'id'        => 'code_postal',
              'placeholder' =>  "Code postal",
              'class' => "form-control",
			  'value' => empty($code_postal) ? '' : $code_postal ,
		);
		$this->data['ville'] = array(
              'name'        => 'ville',
			  'id'        => 'ville',
              'placeholder' =>  "Ville",
              'class' => "form-control",
			  'value' => isset($ville) ? $ville : '',
		);	
		//CRA : fix
		//$this->data['lstVilleJson'] = $this->ville_model->get_ville_json(array(), true);
		//$this->data['lstVilleJson'] = $this->ville_model->get_ville(array(), true);
		
		$this->data['password'] = array(
              'name'        => 'password',
              'placeholder' =>  "Mot de passe",
              'class' => "form-control"	
		);
		$this->data['confirm_password'] = array(
              'name'        => 'confirm_password',
              'placeholder' =>  "Confirmation de mot de passe",
              'class' => "form-control"	
		);
		//Etape 2 : Chercher ENFANT
		$criteres = $lstEnfants = array();
		
		if ($isViewOnly && $step=="etape2" && (!empty($idEnfant))) {
			//Modifier UN ENFANT	
			if(is_numeric($idEnfant)){	
				$criteres['enfant_id'] = $idEnfant;
			} else {
				$criteres['enfant_id'] = -1;
			}
		} else {				
			if (!empty($id)) {
				$criteres['parent_id'] = $id;								
			}
		} 		
		if (!empty($criteres)) {			
			$lstEnfants  = $this->enfant->get_enfant_list($criteres);
		}
		$this->data['section_list'] = false;

		if (!empty($lstEnfants)) {
			foreach($lstEnfants as $key => $rowEnfant) {
				$rowEnfantArray = (array) $rowEnfant;
				$rowEnfantArray['enfant_photo'] = $this->enfant->get_photo_enfant($id, $rowEnfant->enfant_id) . "?" . rand();
				/*
				//Chercher : les réservations à réaliser pour l'enfant 
				$criteres = array('enfant_id' => $rowEnfant->enfant_id);
				$criteres['valide'] = 1;
				$criteres['is_realiser'] = 1;
				if (!empty($idStructure)) {
					$criteres['structure_id'] = $idStructure;
				}				
				$rowEnfantArray['lstReservationARealiser'] = $this->reservation_model->get_reservation_list($criteres, '', false, false);	
												
				//Chercher : les adaptations pour l'enfant 
				$criteres = array();
				$criteres = array('enfant_id' => $rowEnfant->enfant_id);
				//$criteres['adaptation'] = 1; //Pas de notion d'adaptation mais temps d'accueil
				//$criteres['show_rdv_adaptation'] = 1; ////Pas de notion de temps d'accueil non plus				
				//Pas de notion de VALIDE ici
				if (!empty($idStructure)) {
					$criteres['structure_id'] = $idStructure;
					//liste des réservations et savoir si l’enfant est adapté ou non		
					$criteres['is_adapted_in_structure'] = 1;
					$criteres['is_lst_adaptation'] = 1;
				}
				$rowEnfantArray['lstReservationAdaptation'] = $this->reservation_model->get_reservation_list($criteres, '', false, false);
				
				
				//Chercher : les réservations à venir pour l'enfant
				$criteres = array();
				$criteres = array('enfant_id' => $rowEnfant->enfant_id);
				if (!empty($idStructure)) {
					$criteres['structure_id'] = $idStructure;
				}
				$criteres['valide'] = 1;
				$criteres['is_venir'] = 1;				
				//$rowEnfantArray['lstReservationVenir'] = $this->reservation_model->get_reservation_panier_list($criteres);	
				$rowEnfantArray['lstReservationVenir'] = $this->reservation_model->get_reservation_list($criteres, '', false, false);
				*/
				//Chercher parcours de l'enfant
				$criteres = array('enfant_id' => $rowEnfant->enfant_id);	
				$criteres['valide'] = 1; // ne pas afficher les réservations déjà annulées
				$rowEnfantArray['is_adapted_in_structure'] = 0; //Par défaut	
				if($typeActionEnfant==2){ // si parcours enfant
					if($passe_ou_futur==1) $criteres['is_realiser'] = 1;
					if($passe_ou_futur==2) $criteres['is_venir'] = 1;
					$criteres_section['structure_id'] =  $idStructure;
					$this->data['section_list'] = $this->section_model->get_section_by_more_criteria_list($criteres_section);
					$this->data['section_enfant'] = array_shift(get_section_enfant($idStructure, $rowEnfant->enfant_id));
				}	
				if (!empty($idStructure)) {
					$criteres['structure_id'] = $idStructure;
					$rowEnfantArray['structure_id'] = $idStructure;
					$rowEnfantArray['is_adapted_in_structure'] = $this->enfant_has_structure_model->check_enfant_is_adapte_in_structure($rowEnfant->enfant_id, $idStructure);
				}					
				$rowEnfantArray['lstReservationParcours'] =  get_reservation_by_criteria ($criteres);
				 
				$lstEnfants[$key] = (object) $rowEnfantArray;
			}
		}
	
		$this->data['lstEnfants'] = $lstEnfants;
		$this->data['passe_ou_futur'] = $passe_ou_futur;
		
		//Fin
		
		//Etape 3 : Chercher les textes à afficher
		$this->data['synthese_rgu_familiplace'] = $this->param_familiplace_model->get_value_by_cle("synthese_rgu_familiplace");
		$this->data['famille_validation_condition_generale'] = $this->param_familiplace_model->get_value_by_cle("famille_validation_condition_generale");
		
		$this->data['photo_enfant_default'] = 	$this->config->item('dir_photo_enfant').'enfant_default.png';
		
		if (!$isViewOnly) {
			$this->load->view('familles/etapes_famille',$this->data);
		} else {
			$this->data['typeMenu'] = $typeMenu; //1 : Liste réservation ; 2 : famille; 3 : Famille Gestionnaire; 4 : famille reconnue
			if (empty($step) || (!empty($typeActionEnfant))) {
				$this->data['typeActionEnfant'] = $typeActionEnfant; //1 : Fiche identité, 2 : Parcours , 3: Habitude ; 4 : famille reconnue								
				$this->data['is_readonly'] = true;
			} else  {
				$this->data['is_via_compte'] = true;
			}
			$this->data['step'] = $step;
			if ($step == "etape2" || empty($step)) {
				$this->data['parent_role'] = get_role("Vous êtes");		
				$this->data['parent_role_no_title'] = get_role();			
			}
			return $this->load->view('familles/etapes_famille',$this->data, TRUE);
		}
	}	
	
	public function back_to_index() {
		$this->load->controller('gestionnaire');
		$data = $this->gestionnaire->index_login_common();	
		$this->do_index_famille($data);
	}

	/**
	 * Fonction affichant la page d'accueil de l'appli
	 */
	public function do_index_famille($data= array()) {
			//Famille
			$this->output->set_title('Familiplace - Accueil');

			// Json utile pour le combo ville
			//$data['lstVilleJson'] = $this->ville_model->get_ville(array(), true);
			
			// Champs
			$data['field_age_enfant'] =  array(
				'name'			=> 'date_ne_le',
	            'placeholder'	=> 'Age de l\'enfant',
	            'class'   		=> '',
				'maxlength'     => 10,
				'onKeyUp'    => "return on_input_date_naissance(this);"
			);
			$data['field_date_naissance_jour'] =  array(
				'name'			=> 'date_naissance_jour',
	            'placeholder'	=> 'jj',
	            'class'   		=> 'jour',
				'maxlength'     => 2
			);
			$data['field_date_naissance_mois'] =  array(
	            'name'        	=> 'date_naissance_mois',
	            'placeholder'	=> 'mm',
	            'class'   		=> 'mois',
				'maxlength'     => 2
			);
			$data['field_date_naissance_annee'] =  array(
	            'name'        	=> 'date_naissance_annee',
	            'placeholder'	=> 'aaaa',
	            'class'   		=> 'annee',
				'maxlength'     => 4
			);
			$data['field_date_naissance_hidden'] =  array(
	            'name'        	=> 'date_naissance_hidden',
				'style'         => 'visibility: hidden'
			);
			$data['field_code_postal'] =  array(
				'id'			=> 'ville',
	            'name'        	=> 'code_postal_or_ville',
	            'placeholder'	=> 'Ville ou code postal',
	            'class'   		=> 'code_p'
			);
			$data['button_rechercher'] =  array(
	            'value'        	=> 'Rechercher',
	            'id'			=> 'enf',
				'name' 			=> 'enf',
	            'class'   		=> 'valider_enf'
			);
			$data['form_search_attribute'] = array('class' => 'formulaire_enf', 'id' => 'formulaire_enf');
			$data['textes'] = $this->textes->get_all_textes();
			$data["slides"] = array();
				$slides = $this->textes->get_all_slides();
				$tab_file = array();
				foreach($slides as $s){
					$photos = array();
					$photos["img"]= $this->get_pictures_name_info($s["id"],"slides");
					$nbphoto = 0;
					if(isset($photos["img"]["images_name"])) $nbphotos = count($photos["img"]["images_name"]);
					$photos["nb"] = $nbphotos;
					$photos["id"] = $s["id"];
					$photos["content"] = $s["contenu"];
					$tab_file[$s["id"]] = $photos;
				}
			$data["tab_file"] = $tab_file;
			$data["slides"] = $slides;
			$slide = $this->textes->get_all_slide();
			$t_slide = array();
			foreach($slide as $v ){
				$t_slide[$v["nom"]][$v["conteneur"]] = explode("/?/?//",$v["content"]);;
			}
			$data["t_slide"] = $t_slide;
			$this->load->view('familles/homepage', $data);		

	}
	//picture pour slide
	public function  get_pictures_name_info($info_id,$structure_id){
		$images_name = array();
		$images_file_name = array();
		$actu_directory = $this->config->item('dir_slides_fichiers')."/".$structure_id."/".$info_id;
		$upload = $this->config->item('dir_slides_fichiers');
		// $upload=substr($upload,2);
		$base_url = base_url().$upload;
		if (is_dir($actu_directory)){
			$files_name_list = scandir($actu_directory);
			if(count($files_name_list) >= 1){
				foreach ($files_name_list as $files_name){
					// if(in_array(get_file_extension($files_name), $this->config->item('files_image_accepted'))){
					if(get_file_extension($files_name)!=""){
						// array_push($images_name, actualite_url($structure_id."/".$info_id.'/'.$files_name));
						array_push($images_name, $base_url."/".$structure_id."/".$info_id.'/'.$files_name);
						array_push($images_file_name, $files_name);
					}
				}
			}
		}
		if(count($images_name) < 1){
			// $images_name = array(structures_url('default.jpg'));
			$images_name = array();
			$images_file_name = array();
		}
		$data["images_name"] = $images_name;
		$data["images_file_name"] = $images_file_name;
		return $data;
	}
	
	public function home_page_famille(){
		$this->do_homepage_famille_compte(false);	
	}
	public function home_page_famille_via_menu($menuAction = ""){		
		$menuAction = empty($menuAction) ? "menu_famille_mon_compte" : $menuAction;
		$menuSommaire = '#familleMonCompte';
		switch ($menuAction) {
			case 'menu_famille_mes_reservations':
				$menuSommaire = '#familleMesReservations';
				break;
		}	
		$data['default_action'] = $menuAction;
		$data['default_sommaire'] = $menuSommaire;
		$this->do_homepage_famille_compte(true, $data);	
		
	}
	public function homepage_famille_compte_old() {
		if (!$this->ion_auth->logged_in()){
			redirect('auth/do_logout/login_parent/compte_inaccessible');
		}
		 $this->do_homepage_famille_compte_old();			
	}
	
	public function homepage_famille_compte() {
		if (!$this->ion_auth->logged_in()){
			redirect('auth/do_logout/login_parent/compte_inaccessible');
		}
		$this->do_homepage_famille_compte();			 			
	}	
	
	public function do_homepage_famille_compte() {
		$this->homepage_famille_reconnue();
	}
	
	public function do_homepage_famille_compte_old($isCompte = true , $data = array()) {
		if (empty($data)) {
			$data = $this->session->userdata("data_login");	
			if (empty($data)) {
				$this->load->controller('gestionnaire');
				$data = $this->gestionnaire->index_login_common($action);
			}
		}
		
		$this->data = $data; 
		
		$infoParent = isset($data['infoParent'])  ? $data['infoParent'] : array();
		if (!empty($infoParent)) {
			$lstParent = current($infoParent);
			$this->data['parent_id'] = $lstParent->id;
			$this->data['photo_enfant_default'] = 	$this->config->item('dir_photo_enfant').'enfant_default.png';
			
			$criteres_enfant = array("parent_id" => $lstParent->id);
			$enfant_list = $this->enfant->get_enfant_list($criteres_enfant);
			//Prendre premier enfant			
			if(count($enfant_list) > 0){
				$enfant_list = current($enfant_list);
				//c'est la ville qui prime
				$enfant_code_postal_or_ville =  empty($enfant_list->enfant_ville) ? $enfant_list->enfant_code_postal : $enfant_list->enfant_ville;
				$this->data['enfant_date_naissance'] = $enfant_list->enfant_date_naissance;
				$this->data['enfant_code_postal_or_ville'] = $enfant_code_postal_or_ville;
			}
		}
		
		//Chercher la liste des enfants
		$user = $this->ion_auth->user()->row();
		$idUserParent = $user->id;
		$criteres = $lstEnfants = array();		
		if (!empty($idUserParent)) {
			$criteres['users_id'] = $idUserParent;
			$lstEnfants  = $this->enfant->get_enfant_list($criteres);		
			if (!empty($lstEnfants)) {
				foreach($lstEnfants as $key => $rowEnfant) {
					$rowEnfantArray = (array) $rowEnfant;
					$idUserParent = $rowEnfant->parent_id;
					$rowEnfantArray['enfant_photo'] = $this->enfant->get_photo_enfant($idUserParent, $rowEnfant->enfant_id) . "?" . rand();
					//infoTitreEnfant
					$nomUser = empty($rowEnfant->enfant_prenom) ?  $rowEnfant->enfant_nom : $rowEnfant->enfant_prenom;
					$rowEnfantArray['infoTitreEnfant'] = $nomUser . " , né(e) le ". convertToDateMonthYearFrenchFromMysql($rowEnfant->enfant_date_naissance_mysql);
					$lstEnfants[$key] = (object) $rowEnfantArray;
				}
			} 	
		}		
		$this->data['lstEnfantsMenu'] = $lstEnfants;
		
		//Chercher la liste des établissements favoris
		$criteresFavoris = array();
		$criteresFavoris['show_rdv_adaptation'] = 0;
		$criteresFavoris['date_sup_equals_date_du_jour'] = 1;
		$criteresFavoris['parent_id'] = $idUserParent;
		$criteresFavoris['structure_active'] = 1;
		$data['lstEtablissementFavoris'] = $structure_list = $this->get_etablissement_favoris($criteresFavoris);								
		$structure_list_Dispo = $this->get_structure_list_info($structure_list, $criteresFavoris, false);		
		$structure_list_Rdv = array();
		//CRA: Fix : 4340: Crèches préférées
		/* 
		$criteresFavoris['show_rdv_adaptation'] = 1;
		$structure_list = $this->get_etablissement_favoris($criteresFavoris);								
		$structure_list_Rdv = $this->get_structure_list_info($structure_list, $criteresFavoris, false);
		*/
		$this->data['structure_list'] =  array_merge($structure_list_Dispo, $structure_list_Rdv);
		
		
		//Chercher mes alertes
		$parent = $this->get_parent_by_user_connected();
		$this->data['enfant_list'] = $this->data_enfant_alerte($parent->id);
		
		if ($isCompte == true) {
			
			$this->data['default_action'] = isset($data['default_action']) ? $data['default_action'] : "menu_famille_mon_compte";
			$this->data['default_sommaire'] = isset($data['default_sommaire']) ? $data['default_sommaire'] : "#familleMonCompte";
			$this->load->view('familles/homepage_famille_compte', $this->data);
		} else {
			
			$this->load->view('familles/homepage_famille', $this->data);
		}
	}
	
	public function get_etablissement_favoris ($criteres) {		
		$lstEtablissementFavoris = array();		
		$lstEtablissementFavoris = $this->reservation_model->get_etablissement_favoris($criteres);
		if (!empty($lstEtablissementFavoris)) {
			foreach ($lstEtablissementFavoris as $creche) {
				$images_name = $this->get_pictures_name_structure($creche->id);
				if (!empty($images_name)) {
					$creche->first_image = current($images_name) . "?" . rand();
				}
			}
		}
		
		return $lstEtablissementFavoris;
	}
	
	/**
	 * Fonction affichant la page d'accueil de l'appli
	 */
	public function index() {	
		$data = array();
		//CRA : C'est ici qu'on va gérer toutes les actions pour HOMEPAGE
		//Récupérer les informations du gestionnaire ou directeur
		$this->load->controller('gestionnaire');
		$data = $this->gestionnaire->index_login_common();	

		//Sauver dans session pour éviter erreur LOG
	 	$this->session->unset_userdata("data_login");
	 	$this->session->set_userdata("data_login", $data);
		
		if ($this->ion_auth->is_admin())
		{
			//Administrateur
			redirect('auth');
		} else if (isset($data['isLoginPro']) && $data['isLoginPro'] == 1){
			if (isset($data['isPortail']) && $data['isPortail'] == 1) {
				//Portail	
				$this->load->view('gestionnaires/welcome_message', $data);			
			} else {				
				//$this->session->unset_userdata('data_dir');
				$this->session->set_userdata(array('data_dir'=>$data));
				redirect('directeurs/directeur_connecte');

				//Directeur ou Gestionnaire 
				//CRA:$this->load->view('directeurs/welcome_message', $data);
			}			
		} else if (isset($data['isLoginParent']) && $data['isLoginParent'] == 1 && (!empty($data['infoParent']))){
			$action = $this->session->userdata($this->config->item('key_session_last_action'));
			if($action){
				$this->session->unset_userdata($this->config->item('key_session_last_action'));
				redirect($action);
			}else {				
				//$this->load->view('familles/homepage_famille', $data);
				$this->do_homepage_famille_compte(false, $data);
			}
		} else { 		
			$this->do_index_famille();
		}		
	}
	
	/**
	 * 
	 * Recherche par picto : rechercher les crèches avec le nombre de disponibilité correspondant validant les critères renseignés
	 */
	public function rechercher_creche($idTypeRecherche = 0) {
		// Chargement des divers fichiers css et js utile
		$this->load->js($this->config->item('url_api_google_map'));
		$this->load->js("assets/js/infobox.js");//infoBox pour googleMap
		$this->load->js('assets/js/custom_api_google_map.js');
		$this->output->set_title('Familiplace - Cr&egrave;che disponible');
		
		$type_recherche = self::RECHECHE_STRUCTURE;
		if($idTypeRecherche == 1){
			$type_recherche = self::RDV_RECHECHE_STRUCTURE;
		}
		$this->load->view('familles/resultat', $this->get_data_search_dispo($type_recherche,$idTypeRecherche));
	}
	
	/**
	 * 
	 * Méthode a appelé lors du click sur le bouton "Voir la liste des disponibilités" dans la page recherche picto
	 */
	public function rechercher_dispo(){
		// Chargement des divers fichiers css et js utile
		$this->load->js($this->config->item('url_api_google_map'));
		$this->load->js("assets/js/infobox.js");//infoBox pour googleMap
		$this->load->js('assets/js/custom_api_google_map.js');
		$this->output->set_title('Familiplace - Disponibilit&eacute;');
		$this->load->view('familles/resultat', $this->get_data_search_dispo());
	}
	
	/**
	 * 
	 * Afficher les disponibilités selectionnés dans la vue des recherches de disponibilité
	 */
	public function panier_resa(){
		if($this->ion_auth->logged_in() && get_cookie('action_login') == 'login_parent'){
			$this->output->set_title('Familiplace - Panier r&eacute;servaton');
			$this->load->view('familles/reservation_dispo', $this->get_data_search_reservation_panier());
		}else {
			$this->session->set_userdata($this->config->item('key_session_last_action'), 'famille/panier_resa');
			redirect('auth/login_parent');
		}
	}
	
	public function delete_reservation(){
		$reservation_id = $this->input->post('reservation_id');
		if($reservation_id){
			$this->reservation_model->delete_reservation_by_id($reservation_id);
		}
	}
	
	public function supprime_resa_necessite_adaptation(){
		$reservation_adapation_list = json_decode($this->input->post('reservation_id_json'));
		$this->supprime_resa_necessite_adaptation_by_resa_adaptation_list($reservation_adapation_list);
	}
	
	public function supprime_resa_necessite_adaptation_by_resa_adaptation_list($reservation_adapation_list){
		if(is_array($reservation_adapation_list)){
			foreach ($reservation_adapation_list as $reservation_id){
				$this->reservation_model->delete_reservation_by_id($reservation_id);
			}
		}
	}
	
	public function check_resa_deja_reserve(){
		$reservation_list_selected = json_decode($this->input->post('json_reservation_selected'));
		$dispo_deja_reserve_list = array();
		$dispo_pas_reserve_list = array();
		if(is_array($reservation_list_selected)){
			foreach ($reservation_list_selected as $reservation_selected){
				if($this->reservation_model->get_dispo_reserver_enfant_in_resa_valide($reservation_selected->disponibilites_id, $reservation_selected->enfant_id, true)){
					$disponibilite = $this->disponibilites_model->get_disponibilites_by_id($reservation_selected->disponibilites_id);
					if(isset($disponibilite->regulier) && $disponibilite->regulier){
						$reservation_selected->jour = get_jour_detail_dispo_regulier($disponibilite->id, $disponibilite, ", ");
					}else{
						$jour = isset($disponibilite->jour) ? substr($disponibilite->jour, 0, 10)  : '';
						if (!empty($jour)) {
							$reservation_selected->jour = strtoupper(get_day_name_by_date($jour)).' '.date_en_to_fr($jour,'-','/');
						}
					}
					array_push($dispo_deja_reserve_list, $reservation_selected);
				}else {
					array_push($dispo_pas_reserve_list, $reservation_selected);
				}
			}
		}
		$result = array(
			'dispo_deja_reserve_list' => $dispo_deja_reserve_list,
			'dispo_pas_reserve_list' => $dispo_pas_reserve_list
		);
		echo json_encode($result);
	}
	
	/**
	 * Validation des reservations sélectionner dans le panier
	 */
	public function valider_reservation(){
		if($this->ion_auth->logged_in() && get_cookie('action_login') == 'login_parent'){
			$reservation_list_selected = json_decode($this->input->post('json_reservation'));
			$reservaton_list = array();
			if(is_array($reservation_list_selected)){
				$enfant_reserve_list = array();
				$data_enfant_mail = array();
				$k_e = array();
				$rdv_adaptation_list = $this->get_rdv_adaptation_list_in_reservation_list($reservation_list_selected);
				foreach ($reservation_list_selected as $reservation_selected){
					$selected = array();
					$selected['parent_id'] = $reservation_selected->parent_id;
					$selected['enfant_id'] = $reservation_selected->enfant_id;
					$selected['disponibilites_id'] = $reservation_selected->disponibilites_id;
					$selected['horaire_debut'] = $reservation_selected->arrivee;
					$selected['horaire_fin'] = $reservation_selected->depart;
					$selected['panier'] = 0;
					$selected['annule'] = 0;
					$selected['annulation_date'] = '0000-00-00';
					$selected['annulation_comment'] = '';
					
					//if($structure_id != null && !$this->enfant_has_structure_model->check_enfant_is_adapte_in_structure($reservation_selected->enfant_id, $reservation_selected->structure_id)
					
					$enfant_structure_list = false;
					$structure_id = $reservation_selected->structure_id;
					if(isset($structure_id)){
						$enfant_structure_list = $this->enfant_has_structure_model->get_enfant_has_structure_list(array('enfant_id' => $reservation_selected->enfant_id, 'structure_id' => $structure_id));
						if(!$enfant_structure_list){
							$this->enfant_has_structure_model->add_enfant_has_structure(array('enfant_id' => $selected['enfant_id'], 'structure_id' => $structure_id));
						}
					}
					if(((int) $reservation_selected->adaptation)){
						$selected['valide'] = 0;
						if($enfant_structure_list){
							$selected['valide'] = $enfant_structure_list[0]->adapte;
						}
					}else {
						$selected['valide'] = 1;
					}
					
					//sauvegarde des réservations
					if(!$reservation_selected->adaptation
					  || ($reservation_selected->adaptation && $selected['valide'])
					  || ($reservation_selected->adaptation && !$selected['valide'] && $this->resa_adaptation_to_validate_have_rdv_adaptation($rdv_adaptation_list, $reservation_selected))){
						if((int) $reservation_selected->regulier){
							$this->creer_reservation_regulier_par_jour($selected, $reservation_selected, $rdv_adaptation_list);
						}else {
							$selected['jour_reservation'] = $reservation_selected->jour;
							$this->reservation_model->update_reservation_where_id($selected, $reservation_selected->reservation_id);
						}	  	
					}
					
					//data_mail
					$a_prevoir = $this->disponibilites_model->get_dispo_list_by_simple_criteria(array('id' => $selected['disponibilites_id']));
					$selected1 = $selected;
					$selected1["a_prevoir"] = "";
					$selected1["jour"] = "".$reservation_selected->jour;
					$selected1["enfant_prenom"] = "".$reservation_selected->enfant_prenom;
					$selected1['structure_id'] = $reservation_selected->structure_id;
					$data_struct = $this->structure_model->get_structure_by_simple_criteria(array('id' => $reservation_selected->structure_id));
					if(count($a_prevoir)>0){
						$selected1["a_prevoir"] = $a_prevoir[0]->prevoir;
					}
					if(count($data_struct)>0){
						$selected1["creche"] = $data_struct[0]->nom;
						$selected1["lieu"] = $data_struct[0]->num_rue." ".$data_struct[0]->nom_rue." ".$data_struct[0]->code_postal." ";
						$ville = $this->ville_model->get_name_by_id($data_struct[0]->ville_id);
						$selected1["lieu"] = $selected1["lieu"].$ville;
					}
					if(!isset($data_enfant_mail[$selected['enfant_id']])){
						$k_e[$selected['enfant_id']] = 0;
						$data_enfant_mail[$selected['enfant_id']][$k_e[$selected['enfant_id']]] = $selected1 ;
						$k_e[$selected['enfant_id']]++;
					}
					else{
						$data_enfant_mail[$selected['enfant_id']][$k_e[$selected['enfant_id']]] = $selected1 ;
						$k_e[$selected['enfant_id']]++;
					}
					
					if(!in_array($reservation_selected->enfant_id, $enfant_reserve_list)){
						array_push($enfant_reserve_list, $reservation_selected->enfant_id);
					}
				}
				if(is_array($enfant_reserve_list)){
					$parent = $this->get_parent_by_user_connected();
					foreach ($enfant_reserve_list as $enfant_id){
						$enfant = $this->enfant->get_enfant_by_id($enfant_id);
						//parent membre mail
						$this->send_mail_reservation_famille($parent, $enfant,$data_enfant_mail[$enfant_id]);
						
						//parent 2 non-membre
						$membre = get_title_by_id($parent->id)." ".$parent->prenom." ".$parent->nom;
						$data_mail= array("data"=>$data_enfant_mail[$enfant_id] ,"membre"=> $membre);
						$parent->email = $enfant->parent2_email;
						$this->send_mail_reservation_famille($parent, $enfant,$data_mail);
					}
				}
			}
			redirect('famille/panier_resa?after_resa=1');
		}else {
			$this->session->set_userdata($this->config->item('key_session_last_action'), 'famille/panier_resa');
			redirect('auth/login_parent');
		}
	}
	
	public function get_rdv_adaptation_list_in_reservation_list($reservation_list){
		$rdv_adaptation_list = array();
		if(is_array($reservation_list)){
			foreach ($reservation_list as $reservation){
				if(in_array($reservation->type_accueil_id, array(4, 7)) && !in_array_object($rdv_adaptation_list, "reservation_id", $reservation->reservation_id)){
					array_push($rdv_adaptation_list, $reservation);
				}
			}
		}
		return $rdv_adaptation_list;
	}
	
	public function resa_adaptation_to_validate_have_rdv_adaptation($rdv_adaptation_list, $resa){
		if(is_array($rdv_adaptation_list)){
			foreach ($rdv_adaptation_list as $rdv_adaptation){
				if($rdv_adaptation->structure_id == $resa->structure_id){
					return $rdv_adaptation;
				}
			}
		}
		return false;
	}
	
	public function creer_reservation_regulier_par_jour($reservation_dispo_regulier, $reservation_source, $rdv_adaptation_list = array()){
		//Suppresion de la réservation dans le panier d'abord
		$this->reservation_model->delete_reservation_by_id($reservation_source->reservation_id, false);
		if($reservation_source->adaptation && !$reservation_dispo_regulier['valide'] ){
			$rdv = $this->resa_adaptation_to_validate_have_rdv_adaptation($rdv_adaptation_list, $reservation_source);
			if($rdv){
				if(date_diff_day($reservation_source->date_debut,$rdv->jour) < $this->config->item('nb_jour_adaptation')){
					$date_debut = add_day_to_date($rdv->jour, $this->config->item('nb_jour_adaptation'));
				}else {
					$date_debut = $reservation_source->date_debut;
				}
				$jour_list = get_jour_disponibilite_reguliere_list($reservation_dispo_regulier['disponibilites_id'], $date_debut);
				$this->creer_ligne_resa_regulier($reservation_dispo_regulier, $jour_list);
			}
		}else {
			$jour_list = get_jour_disponibilite_reguliere_list($reservation_dispo_regulier['disponibilites_id']);
			$this->creer_ligne_resa_regulier($reservation_dispo_regulier, $jour_list);
		}
	}
	
	public function creer_ligne_resa_regulier($reservation_dispo_regulier, $jour_list){
		//création de ligne de réservation par jour
		if(is_array($jour_list)){
			foreach ($jour_list as $jour){
				$reservation_dispo_regulier['jour_reservation'] = $jour;
				$this->reservation_model->add_reservation($reservation_dispo_regulier);
			}
		}
	}
	
	public function filter_dispo_final_to_save(){
		$enfant_id = $this->input->post('enfant_id');
		$dispo_selected = $this->input->post('dispo_selected');
		
		$dispo_a_sauvegarde_list = array();
		$dispo_a_rejete_list = array();
		if($dispo_selected){
			foreach ($dispo_selected as $dispo_id){
				if($this->reservation_model->get_dispo_reserver_enfant($dispo_id, $enfant_id)){
					$disponibilites = $this->get_disponibilite_info($this->disponibilites_model->get_disponibilites_by_id($dispo_id), false);
					array_push($dispo_a_rejete_list, $disponibilites);
				}else{
					array_push($dispo_a_sauvegarde_list, $dispo_id);
				}
			}
			$result = array(
				'dispo_a_sauvegarder_list' => $dispo_a_sauvegarde_list,
				'dispo_a_rejete_list' => $dispo_a_rejete_list
			);
			echo json_encode($result);
		}
	}
	
	/**
	 * Validation des disponibilité des accueils régulier ou irrégulier selectionner dans la page resultat des disponibilités
	 */
	public function valider_disponibilite(){
		$json_dispo_final_selected = $this->input->post('json_dispo_final_selected');
		$dispo_list_selected = $this->input->post('dispo_selected');
		$date_naissance = $this->input->post('date_naissance');
		$enfant_id = $this->input->post('enfant_id');
		
		if($json_dispo_final_selected){
			$dispo_list_selected = json_decode($json_dispo_final_selected);
		}
		
		$this->session->set_userdata('last_dispos_selected', $dispo_list_selected);
		if($this->ion_auth->logged_in() && get_cookie('action_login') == 'login_parent'){
			$this->session->unset_userdata('last_dispos_selected');
			$panier = array();
			$user = $this->ion_auth->user()->row();
			$parent = $this->get_parent_by_user_connected();
			$is_rdv_adaptation = false;
			if($enfant_id && $parent != null && $dispo_list_selected){
				$is_rdv_adaptation = $this->disponibilites_model->check_dispo_rdv_adaptation($dispo_list_selected[0]);
				$this->creer_panier_resa($dispo_list_selected, $parent->id, $enfant_id);
			}
			$accueil_adapation_moins_5_list = $this->get_accueil_adapation_moins_5_jour_rdv_adaptation();
			$this->supprime_resa_necessite_adaptation_by_resa_adaptation_list(get_object_id_list($accueil_adapation_moins_5_list,"reservation_id"));
			
			if($is_rdv_adaptation){
				redirect('famille/panier_resa?rdvadaptation=1');
			}else {
				redirect('famille/panier_resa?enfant_id='.$enfant_id);
			}
		}else {
			$action_dispo = $this->session->userdata('action_dispo');
			if($action_dispo){
				$this->session->set_userdata($this->config->item('key_session_last_action'), $action_dispo);
			}else {
				$this->session->set_userdata($this->config->item('key_session_last_action'), $this->config->item('url_rechercher_dispo_apres_authentification'));
			}
			$this->session->set_userdata('via_authentification', true);
			redirect('auth/login_parent');
		}
	}
	
	public function creer_panier_resa($dispo_list_selected, $parent_id,  $enfant_id){
		if($dispo_list_selected){
			$paner_reservation_list = array();
			foreach ($dispo_list_selected as $disponibilite_id){
				$reservation = array();
				$reservation['enfant_id'] = $enfant_id;
				$reservation['parent_id'] = $parent_id;
				$reservation['disponibilites_id'] = $disponibilite_id;
				$reservation['valide'] = 0;
				$reservation['panier'] = 1;
				$reservation_id = $this->reservation_model->add_reservation($reservation);
			}
			$this->session->set_userdata('enfant_panier_id', $enfant_id);
		}
	}
	
	/**
	 * 
	 * Affiche la fiche de disponibilité d'une structure
	 */
	public function show_fiche_dispo_structure(){
		// Récupération des types de temps d'accueil à afficher dans le champ multiple select "type d'accueil"
		//$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list(array());
		$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list_by_date_naissance('',1);
		
		$data = $this->data_criteres_recherche('', $type_temps_accueil_list);
		$this->load->view('familles/fiche_dispo', $data);
	}
	
	/**
	 * Retourne les messages envoyés si picto non trouvé 
	 */
	function renvoi_message_picto() {
		$type_recherche = $this->input->post('type_recherche_hidden');
		$isRDVAdaptationPicto = false;
		if($type_recherche == Famille::RDV_RECHECHE_STRUCTURE ) {
			$isRDVAdaptationPicto = true;
		}
		$msg="";
		if (!$isRDVAdaptationPicto) {
			//Dispo
			$msg = '<div class="mess_no_result">';
			$msg .=	"Aucune disponibilité ne correspond à votre sélection. Vous pouvez élargir votre recherche avec la carte, modifier vos critères ci-dessus ou créer une alerte depuis votre compte famille.";
			$msg .=	"</div>";	
		} else {
			//RDV
			//Cela ne doit pas exister car on affiche TOUS avec 0
			$msg = "";			
		}
		$result = array("dataView" => $msg);
		echo json_encode($result);
	}
	/**
	 * 
	 * Retourne les informations des crèches trouvés en json
	 */
	public function rechercher_creche_json(){
		$typeRecherche = $this->input->post('type_recherche');
		$idTypeRecherche = $this->input->post('critere_type_recherche');
		echo json_encode($this->get_data_search_dispo($typeRecherche, $idTypeRecherche));
		//echo json_encode($this->get_data_search_picto());
	}
	
	/**
	 * 
	 * Retourne les disponibilités trouvés en json
	 */
	public function rechercher_dispo_json(){
		$data = $this->get_data_search_dispo();
		
		$result = array(
			'data' => $data,
			'page_resultat_dispo' => $this->load->view('familles/resultat_dispo', $data, true)
		);
		echo json_encode($result);
	}
	
	public function get_structure_json(){
		$ville_id = $this->input->post('ville_id');
		$date_naissance = $this->input->post('date_naissance');
		$criteres = array();
		if($ville_id){
			$criteres['ville_id_list'] = array($ville_id);
		}
		if($date_naissance){
			$criteres['date_naissance'] = $date_naissance;
		}
		$criteres['structure_active'] = 1;
		
		$structure_list = $this->structure_model->get_structure_list_by_more_criteria($criteres);
		echo json_encode($structure_list);
	}
	
	public function get_structure_json_by_id(){
		$structure_id = $this->input->post('structure_id');
		if($structure_id){
			echo json_encode(get_structure_by_id($structure_id));
		}
	}
	
	public function get_dispo_json(){
		echo json_encode($this->get_data_search_dispo());
	}
	
	public function rechercher_resa_panier_enfant_ajax(){
		echo $this->load->view('familles/tableau_reservation',$this->get_data_search_reservation_panier(), true);
	}
	/**
	 * 
	 * Récupération des données à envoyer au view pour la recherche par picto
	 */
	public function get_data_search_picto($idTypeRecherche = 0){
		if($_REQUEST){
			// Date de naissance page d'accueil (jj-mm-aaa)
			$jj = $this->input->post('date_naissance_jour');
			$mm = $this->input->post('date_naissance_mois');
			$aaaa = $this->input->post('date_naissance_annee');
			// Date de naissance recherche picto
			$date_ne_le = $this->input->post('date_ne_le');
			$code_postal_or_ville = $this->input->post('code_postal_or_ville');
			$type_accueils = $this->input->post('type_accueils');
			$bounds = $this->input->post('bounds');
			
			//critere Periode
			$jours_particulier = $this->input->post('jour_particulier');
			$crenau_regulier = $this->input->post('crenau_regulier');
			$deb_periode = $this->input->post('periode_debut');
			$fin_periode = $this->input->post('periode_fin');
			
			$date_naissance = '';
			if($jj && $mm && $aaaa) {
				$date_naissance = $aaaa.'-'.$mm.'-'.$jj;
			}elseif ($date_ne_le){
				if(strpos($date_ne_le, '/')){
					$date_naissance = date_fr_to_en($date_ne_le, '/', '-');
				}elseif(strpos($date_ne_le, ' ')){
					$date_naissance = date_fr_to_en($date_ne_le, ' ', '-');
				}
			}
			
			$param_criteres_search['date_naissance'] = $date_naissance;
			$param_criteres_search['code_postal_or_ville'] = $code_postal_or_ville;
			$param_criteres_search['type_accueils'] = $type_accueils;
			$param_criteres_search['bounds'] = $bounds;
			$param_criteres_search['jours_particulier'] = $jours_particulier;
			$param_criteres_search['crenau_regulier'] = $crenau_regulier;
			$param_criteres_search['deb_periode'] = $deb_periode;
			$param_criteres_search['fin_periode'] = $fin_periode;
			$this->session->set_userdata('param_criteres_search',$param_criteres_search);
		}else{
			$param_criteres_search = $this->session->userdata('param_criteres_search');
			$date_naissance = $param_criteres_search['date_naissance'];
			$code_postal_or_ville = $param_criteres_search['code_postal_or_ville'];
			$type_accueils = $param_criteres_search['type_accueils'];
			$bounds = $param_criteres_search['bounds'];
			$jours_particulier = $param_criteres_search['jours_particulier'];
			$crenau_regulier = $param_criteres_search['crenau_regulier'];
			$deb_periode = $param_criteres_search['deb_periode'];
			$fin_periode = $param_criteres_search['fin_periode'];		
		}
		
		//Gestion des critères récus
		$ville_criteres = array();
		if(is_numeric($code_postal_or_ville)){
			$ville_criteres['code_postal'] = $code_postal_or_ville; 
		}else {
			$ville_criteres['nom'] = $code_postal_or_ville;
		}
		
		// Récupération des résultats
		$criteres = array();
		
		if($date_naissance != '') {
			$criteres['date_naissance'] = $date_naissance;
		}
		if(count($type_accueils > 0)){
			$criteres['type_accueils'] = $type_accueils;
		}
		
		if($bounds){
			$criteres['bounds'] = $bounds;
		}else if ($ville_criteres){
			$ville_list = $this->ville_model->get_ville($ville_criteres);
			if(count($ville_list) > 0){
				$ville =$ville_list[0];
				$data['ville_creche'] = $ville;
				$criteres['ville_nom'] = $code_postal_or_ville = $ville->nom;
			}
		}
		$criteres['structure_active'] = true;
		
		//Recherche des crèches
		$structure_list = $this->structure_model->get_structure_list_by_more_criteria($criteres);
		
		//Récuperation du parent à partir de l'id user
		$parent = $this->get_parent_by_user_connected();
		if($parent != null) {
			$criteres['parent_id'] = $parent->id;
		}
		// param periode
		if($jours_particulier){
			$criteres['jours_particulier'] = $jours_particulier;
		}
		if($crenau_regulier){
			$criteres['crenau_regulier'] = $crenau_regulier;
		}
		if($deb_periode || $fin_periode){
			$criteres['deb_periode'] = date_fr_to_en($deb_periode, '/', '-');
			$criteres['fin_periode'] = date_fr_to_en($fin_periode, '/', '-');
		}
		//CRA : 20/09/2014
		$data['field_code_postal_or_ville'] = $code_postal_or_ville;
		
		//CRA: Attention ici est juste un test pour RDV ADAPTATION 
      	$not_show_adaptation = 1;
 		
		switch ($idTypeRecherche) {
			case 1:
				$typeRecherche = self::RDV_RECHECHE_STRUCTURE;
				//$not_show_adaptation = 0;
				break;						
		}			
			
		//Cherche si cela passe par POST
		if (empty($typeRecherche)) {			
			$typeRecherche = $this->input->post('type_recherche');
		}		
      	$typeRecherche = empty($typeRecherche) ? self::RECHECHE_STRUCTURE : $typeRecherche;
      	
		if (in_array($typeRecherche , array(self::RDV_RECHECHE_STRUCTURE))) {			
			$criteres['show_rdv_adaptation'] = 1;
			$not_show_adaptation = 0;
		}else{
			$criteres['show_rdv_adaptation'] = 0;
			$not_show_adaptation = 1;
		}
		
		$criteres['date_sup_equals_date_du_jour'] = 1;
		// Recherche des structures
		$data['structure_list'] = $this->get_structure_list_info($structure_list, $criteres, true);
		$data['nb_etablissement_trouve'] = count($data['structure_list']);
		$data['not_show_header'] = true;
		// Json utile pour le combo ville
		//$data['lstVilleJson'] = $this->ville_model->get_ville(array(), true); 
		$data['code_postal_or_ville'] = $code_postal_or_ville;
     	$data['form_search_dispo_attribute'] = array('id' => 'form_search_dispo');
		//CRA: $data['type_recherche'] = self::RECHECHE_STRUCTURE;	
		$data['type_recherche'] = $typeRecherche;
		
     	
     	// Récupération des types de temps d'accueil à afficher dans le champ multiple select "type d'accueil"
     	$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list_by_date_naissance($date_naissance, $not_show_adaptation);
     	//CRA: A changer si on va prendre en compte RDV ou non
		//$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list_by_date_naissance($date_naissance, $not_show_adaptation);
     	$data = array_merge($data, $this->data_criteres_recherche($date_naissance, $type_temps_accueil_list, $type_accueils, $jours_particulier, $crenau_regulier, $deb_periode, $fin_periode));
     	$data['idTypeRecherche'] = $idTypeRecherche;
   	
		return $data;
	}
	
	public function data_criteres_recherche($date_naissance = '', $type_temps_accueil_list = array(), $type_accueils_selected = array(), $jours_particulier = array(), $crenau_regulier = array(), $deb_periode = '', $fin_periode = ''){
		$data['field_enfant_ne_le'] =  array(
			'type'  => 'text',
            'name' 	=> 'date_ne_le',
            'class'	=> 'form-control',
			'value' => date_en_to_fr($date_naissance, '-', '/')
     	);
     	$data['field_type_accueil'] =  array(
     		'name' 	  	=> 'type_accueils[]',
     		'options' 	=> get_dropdown_option_array($type_temps_accueil_list, 'id', 'nom'),
			'value_selected' => $type_accueils_selected,
     		'extra' => 'id="select2" multiple'
     	);
     	$data = array_merge($data, data_critere_recherche_periode($jours_particulier,$crenau_regulier,$deb_periode,$fin_periode));
     	return $data;
	}
	
	/**
	 * 
	 * Récupération des données à envoyer au view pour la recherche des disponibilités
	 */
	public function get_data_search_dispo($typeRecherche = "", $idTypeRecherche = 0){
		//Cherche si cela passe par POST
		if (empty($typeRecherche)) {			
			$typeRecherche = $this->input->post('type_recherche');
		}
		$structure_id = '';
		// structure_id pour un structure selectionné
		$structure_id = $this->input->post('structure_id');		
		if (empty($structure_id)) {
			$structure_id = $this->input->post('structure_id_selected');						
		}
		$structure_id_list = $this->input->post('structure_id_list');		
		$date_naissance = false;
		$enfant_id = false;				
		$bounds = false;
		if($_REQUEST){
			if($this->input->get('poursuivre_resa')){
				$param_criteres_search = $this->session->userdata('param_criteres_search');
				$code_postal_or_ville = $param_criteres_search['code_postal_or_ville'];
				$type_accueils = $param_criteres_search['type_accueils'];
				$bounds = $param_criteres_search['bounds'];			
				$jours_particulier = $param_criteres_search['jours_particulier'];
				$crenau_regulier = $param_criteres_search['crenau_regulier'];
				$deb_periode = $param_criteres_search['deb_periode'];
				$fin_periode = $param_criteres_search['fin_periode'];
				$date_naissance = date_fr_to_en($this->input->get('date_naissance'), '/', '-');
				$enfant_id = $this->input->get('enfant_id');
				$this->session->unset_userdata('via_authentification');
			}else {
				// Date de naissance page d'accueil (jj-mm-aaa)
				$jj = $this->input->post('date_naissance_jour');
				$mm = $this->input->post('date_naissance_mois');
				$aaaa = $this->input->post('date_naissance_annee');
				// Date de naissance recherche picto
				$date_ne_le = $this->input->post('date_ne_le');
				$code_postal_or_ville = $this->input->post('code_postal_or_ville');
				$type_accueils = $this->input->post('type_accueils');
				$bounds = $this->input->post('bounds');
				if(is_string($bounds)){
					$bounds_obj = json_decode($bounds);
					if(is_object($bounds_obj)){
						$bounds = (array) $bounds_obj;
					}else $bounds = array();
				}
				$enfant_id = $this->input->post('enfant_id');
				
				//critere Periode
				 $jours_particulier = $this->input->post('jour_particulier');
				 $crenau_regulier = $this->input->post('crenau_regulier');
				 $deb_periode = $this->input->post('periode_debut');
				 $fin_periode = $this->input->post('periode_fin');
									
				//Gestion des critères récus
				$date_naissance = false;
				if($jj && $mm && $aaaa) {
					$date_naissance = $aaaa.'-'.$mm.'-'.$jj;
				}elseif ($date_ne_le){
					if(strpos($date_ne_le, '/')){
						$date_naissance = date_fr_to_en($date_ne_le, '/', '-');
					}elseif (strpos($date_ne_le, ' ')){
						$date_naissance = date_fr_to_en($date_ne_le, ' ', '-');
					}
				}

				$param_criteres_search['date_naissance'] = $date_naissance;
				$param_criteres_search['code_postal_or_ville'] = $code_postal_or_ville;
				$param_criteres_search['type_accueils'] = $type_accueils;
				$param_criteres_search['bounds'] = $bounds;			
				$param_criteres_search['jours_particulier'] = $jours_particulier;
				$param_criteres_search['crenau_regulier'] = $crenau_regulier;
				$param_criteres_search['deb_periode'] = $deb_periode;
				$param_criteres_search['fin_periode'] = $fin_periode;
				//$param_criteres_search['enfant_id'] = $enfant_id;
				$this->session->set_userdata('param_criteres_search',$param_criteres_search);
			}
		}else {
			$param_criteres_search = $this->session->userdata('param_criteres_search');
			$date_naissance = $param_criteres_search['date_naissance'];
			$code_postal_or_ville = $param_criteres_search['code_postal_or_ville'];
			$type_accueils = $param_criteres_search['type_accueils'];
			$bounds = $param_criteres_search['bounds'];			
			$jours_particulier = $param_criteres_search['jours_particulier'];
			$crenau_regulier = $param_criteres_search['crenau_regulier'];
			$deb_periode = $param_criteres_search['deb_periode'];
			$fin_periode = $param_criteres_search['fin_periode'];	
			//$enfant_id = $param_criteres_search['enfant_id'];	
		}
		
		$ville_criteres = array();
		if(is_numeric($code_postal_or_ville)){
			$ville_criteres['code_postal'] = $code_postal_or_ville; 
		}else if($code_postal_or_ville && !empty($code_postal_or_ville)){
			$ville_criteres['nom'] = $code_postal_or_ville;					
		}
		
		// Récupération des résultats
		$criteres = array();
		if($date_naissance) {
			$criteres['date_naissance'] = $date_naissance;
		}
		if(count($type_accueils > 0)){
			$criteres['type_accueils'] = $type_accueils;
		}
		
		if($bounds){
			$criteres['bounds'] = $bounds;
		}else if ($ville_criteres){
			$ville_list = $this->ville_model->get_ville($ville_criteres);
			if(count($ville_list) > 0){
				$ville =$ville_list[0];
				$data['ville_creche'] = $ville;
				$criteres['ville_nom'] = $code_postal_or_ville = $ville->nom;
			}
		}
		
		// param periode
		if($jours_particulier){
			$criteres['jours_particulier'] = $jours_particulier;
		}
		if($crenau_regulier){
			$criteres['crenau_regulier'] = $crenau_regulier;
		}
		if($deb_periode || $fin_periode){
			$criteres['deb_periode'] = date_fr_to_en($deb_periode, '/', '-');
			$criteres['fin_periode'] = date_fr_to_en($fin_periode, '/', '-');
		}
		
		//ajout critère pour $structure_id
		if(isset($structure_id)&&is_numeric($structure_id)){
			$criteres['structure_id'] = $structure_id; 
		}
		$is_semaine=$this->input->post("is_semaine");
		//if(isset($is_semaine) && ($is_semaine!=1)){ Clément code à revoir
		if($is_semaine){
			$criteres['is_semaine'] = $is_semaine; 
			//Supprimer ville car pour une structure bien déterminée
			unset($criteres['ville_nom']);
		}
		//Récuperation du parent à partir de l'id user
		$parent = $this->get_parent_by_user_connected();
		if($parent != null) {
			$criteres['parent_id'] = $parent->id;
			if($enfant_id){ //Dans le cas d'une enfant connue
				$criteres['enfant_id'] = $enfant_id;
			}elseif ($date_naissance){ // Dans le cas ou l'enfant_id n'est pas renseigné mais son date est renseigné
				$criteres_enfant['parent_id'] = $parent->id;
				$criteres_enfant['date_naissance'] = $date_naissance;
				$enfant_list = $this->enfant->get_enfant_list($criteres_enfant);
				if(count($enfant_list) == 1){
					$enfant = $enfant_list[0];
					$enfant_id = $enfant->enfant_id;
					$criteres['enfant_id'] = $enfant->enfant_id;
				}
			}
		}
		
		//Recherche des crèches et de dispo
		//CRA: Initialisation pour éviter des erreurs
		$data['structure_list'] = array();
		$data['nb_etablissement_trouve'] = 0;
		$criteres['date_sup_equals_date_du_jour'] = 1;
		$data['idTypeRecherche'] = $this->input->post('critere_type_recherche') ? $this->input->post('critere_type_recherche') : $idTypeRecherche;
		
		$idTypeRecherche = $data['idTypeRecherche'];
		$isRDVAdaptation = false;	
		if ((in_array($typeRecherche , array(Famille::RDV_RECHECHE_STRUCTURE, Famille::RDV_ADAPTATION, Famille::RDV_ADAPTATION_REGULIER, Famille::RDV_ADAPTATION_IRREGULIER)))  || 
			( isset($idTypeRecherche) && $idTypeRecherche != 0) ){
			$isRDVAdaptation = true;
		}
		if ($isRDVAdaptation) {			
			//$criteres['adaptation'] = 1; //CRA : il n'y a plus de notion de A pour RDV
			$criteres['show_rdv_adaptation'] = 1;
			$not_show_adaptation = 0;
		}else{
			$criteres['show_rdv_adaptation'] = 0;
			$not_show_adaptation = 1;
		}
		$criteres['structure_active'] = 1;

		//CRA: 31/10/2014 : Add $typeRecherche == self::RDV_RECHECHE_STRUCTURE
		if (($data['idTypeRecherche'] && $data['idTypeRecherche']!=0) || ($typeRecherche == self::RDV_RECHECHE_STRUCTURE)) {
			//RDV Adaptation - sans tenir compte regulier et irregulier
			if ($typeRecherche == self::RDV_RECHECHE_STRUCTURE && (!empty($structure_id_list)))	{
				$tabList = explode(",", $structure_id_list);				
				$criteres['structure_id_list'] = $tabList;
				$criteres['show_zero_picto'] = 1;
				$criteres['show_all_structure'] = 1;		
				$criteres['lstStructureNotShow'] = array();			
			} 				
			$data['dispo_regulier_list'] = $this->get_disponibilite_list_info($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));

			//CRA: 31/10/2014 : Chercher les dipos 0
			if ($typeRecherche == self::RDV_RECHECHE_STRUCTURE && empty($structure_id_list)) {							
				if (!empty($data['dispo_regulier_list'])) {					
					$arrayStructureList = array();
			    	foreach($data['dispo_regulier_list'] as $value) {
			    		$arrayStructureList[] = $value->structure_id;
			    	}
			    	$arrayStructureList = array_unique($arrayStructureList);
			    	$criteres['lstStructureNotShow'] = $arrayStructureList;			    			    
				} else {
					//Mettre en haut les crèches où il y a de RDV d'adaptation
					//Chercher PICTO pour cette ville tout d'abord		
					//Hasina : Dans le cas où une ville ont des crèches on envoie le $criteres['show_zero_picto'] = 1
					if($this->structure_model->get_structure_list_by_more_criteria(array('ville_nom' => $criteres['ville_nom']))){
						$criteres['show_zero_picto'] = 1;	
						$data['dispo_regulier_list'] = $this->get_disponibilite_list_info($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));
						if (!empty($data['dispo_regulier_list'])) {					
							$arrayStructureList = array();
					    	foreach($data['dispo_regulier_list'] as $value) {
					    		$arrayStructureList[] = $value->structure_id;
					    	}
					    	$arrayStructureList = array_unique($arrayStructureList);
					    	$criteres['lstStructureNotShow'] = $arrayStructureList;	
						}
					}
					/*else {
						unset($criteres['ville_nom']);
						unset($criteres['code_postal']);
					}
					$data['dispo_regulier_list'] = $this->get_disponibilite_list_info($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));
					if (!empty($data['dispo_regulier_list'])) {					
						$arrayStructureList = array();
				    	foreach($data['dispo_regulier_list'] as $value) {
				    		$arrayStructureList[] = $value->structure_id;
				    	}
				    	$arrayStructureList = array_unique($arrayStructureList);
				    	$criteres['lstStructureNotShow'] = $arrayStructureList;	
					}*/
				}	
				$data['dispo_regulier_list_zero'] = array();
				if($bounds){
					$criteres['show_zero_picto'] = 1;	
					$criteres['show_all_structure'] = 1;			 											
			    	$data['dispo_regulier_list_zero'] = $this->get_disponibilite_list_info($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));
				} else {									
					$criteres['show_zero_picto'] = 1;				 											
			    	$data['dispo_regulier_list_zero'] = $this->get_disponibilite_list_info($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));		    		    	
			    	/*if ( empty($data['dispo_regulier_list']) && empty($data['dispo_regulier_list_zero'])) {		    		
			    		$criteres['show_all_structure'] = 1;
						$data['dispo_regulier_list_zero'] = $this->get_disponibilite_list_info($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));		    		
			    	} */
				}
		    			    	
		    	//Si null on prend tous
		    	$data['dispo_regulier_list'] = array_merge($data['dispo_regulier_list'], $data['dispo_regulier_list_zero']);				    	
			}
			//Fin CRA
			
			$dispo_list = 	$data['dispo_regulier_list'];		
		} else {
			$criteres['regulier'] = 1;
			$data['dispo_regulier_list'] = $this->get_disponibilite_list_info($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));
			$criteres['regulier'] = 0;
			$data['dispo_irregulier_list'] = $this->get_disponibilite_list_info($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));
			$dispo_list = array_merge($data['dispo_regulier_list'], $data['dispo_irregulier_list']);
		}
				
		$data['last_dispos_selected'] = array();
		if ($this->session->userdata('last_dispos_selected')){
			$data['last_dispos_selected'] = $this->session->userdata('last_dispos_selected');
		}
		// Recherche de structure		
		$data['structure_list'] = $this->get_structures_by_dispo_list($dispo_list, $isRDVAdaptation);
		$data['nb_etablissement_trouve'] = count($data['structure_list']);
		$data['not_show_header'] = true;
		// Récupération des types de temps d'accueil à afficher dans le champ multiple select "type d'accueil"
		//$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list(array());
		$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list_by_date_naissance($date_naissance, $not_show_adaptation);
	
		// Json utile pour le combo ville
		//$data['lstVilleJson'] = $this->ville_model->get_ville(array(), true); 
		// Champs
		$data['code_postal_or_ville'] = $code_postal_or_ville;
		//CRA : 03/10/2014
		$data['field_code_postal_or_ville'] = $code_postal_or_ville;
		$data['structure_id'] = $structure_id;
		$data['form_search_dispo_attribute'] = array('id' => 'form_search_dispo');
     	$data['type_recherche'] = empty($typeRecherche) ? self::RECHECHE_DISPO : $typeRecherche;
     	if($enfant_id){
     		$data['enfant_id'] = $enfant_id;
     		if($structure_id && !empty($structure_id)){
     			$enfant_structure_list = $this->enfant_has_structure_model->get_enfant_has_structure_list(array('enfant_id' => $enfant_id, 'structure_id' => $structure_id));
     			if($enfant_structure_list){
					$data['enfant_adapte_structure'] = $enfant_structure_list[0]->adapte;
				}
     		}
     	}
     	if($this->session->userdata('via_authentification')){
     		$data['via_authentification'] = true;
     	}
     	$this->session->unset_userdata('via_authentification');
     	$data = array_merge($data, $this->data_criteres_recherche($date_naissance, $type_temps_accueil_list, $type_accueils, $jours_particulier, $crenau_regulier, $deb_periode, $fin_periode));
		return $data;
	}
	
	function choisir_enfant_reserver_json(){
		$parent_id = $this->input->post('parent_id');
		$enfant_id = $this->input->post('enfant_id');
		$date_naissance = $this->input->post('date_naissance');
		$criteres = array();
		if ($parent_id){
			$criteres['parent_id'] = $parent_id;
		}
		if($enfant_id){
			$criteres['enfant_id'] = $enfant_id;
		}
		if($date_naissance){
			$criteres['date_naissance'] = date_fr_to_en($date_naissance, '/', '-');
		}
		
		$enfant_list = $this->enfant->get_enfant_list($criteres);
		$titre_dialog = "Choix de l'enfant &agrave; reserver";
		if($date_naissance && count($enfant_list) < 1){
			$titre_dialog = "Date de naissance erron&eacutee";
		}
		$data['list_enfant'] = $enfant_list;
		$result = array(
			'enfant_list' => $enfant_list,
			'titre_dialog' => $titre_dialog,
			'html_resultat_dispo_choix_enfant_reserve' => $this->load->view('familles/resultat_dispo_choix_enfant_reserve', $data, true)
		);
		echo json_encode($result);
	}
	
	/**
	 * 
	 * Récupération des données à envoyer au view pour la recherche des paniers (reservation)
	 */
	public function get_data_search_reservation_panier(){
		$date_naissance = '';
		$enfant_id = false;
		$rdvadaptation = false;
		$after_resa = false;
		if($_REQUEST){
			$date_ne_le = $this->input->post('date_ne_le') ? $this->input->post('date_ne_le') : $this->input->get('date_ne_le');
			$enfant_id = $this->input->get('enfant_id');
			$after_resa = $this->input->get('after_resa');
			$rdvadaptation = $this->input->get('rdvadaptation');
			$date_naissance = date_fr_to_en($date_ne_le, '/', '-'); 
			$param_reservation_panier['date_naissance'] = $date_naissance;
			$this->session->set_userdata('param_reservation_panier',$param_reservation_panier);
		}else {
			$param_reservation_panier = $this->session->userdata('param_reservation_panier');
			$date_naissance = $param_reservation_panier['date_naissance'];
		}
		//Récuperation de l'enfant à reserver
		$parent = $this->get_parent_by_user_connected();
		$criteres_enfant = array();
		$criteres['parent_id'] = $parent->id;
		if($date_naissance != '') {
			$criteres['date_naissance'] = $date_naissance;
		}
		/*if($rdvadaptation){
			$criteres['show_rdv_adaptation'] = 1;
		}*/
		//$orderby = $enfant_id ? "ORDER BY reservation.date_creation DESC" : "ORDER BY enfant.id ASC";
		$reservation_list = $this->reservation_model->get_reservation_panier_list($criteres, "ORDER BY reservation.date_creation DESC");
		
		if($after_resa){
			$data['after_resa'] = true;
		}
		$data['enfant_concerne_resa_id'] = $enfant_id;
		$data['parent_id'] = $parent->id;
		$data['reservation_panier_list'] = get_reservation_info_list($reservation_list,true);
		$data['structure_reserve_list'] = $this->get_stucture_reserve_list($reservation_list);
		$data['all_enfant_list'] = $this->enfant->get_enfant_list(array('parent_id' => $parent->id));
		
		//Champs
		$data['field_enfant_ne_le'] =  array(
			'type'  => 'text',
            'name' 	=> 'date_ne_le',
            'class'	=> 'form-control',
			'value' => date_en_to_fr($date_naissance, '-', '/')
     	);
		return $data;
	}
	
	public function get_reservation_panier_parent_json_list(){
		$parent = $this->get_parent_by_user_connected();
		$not_rdv_adaptation = $this->input->post('not_rdv_adaptation');
		$rdv_adaptation = $this->input->post('rdv_adaptation');
		if ($parent != null){
			$criteres['parent_id'] = $parent->id;
			if($not_rdv_adaptation){
				// On ne recupere pas les rdv d'aptation
				$criteres['show_rdv_adaptation'] = 0;
			}
			if($rdv_adaptation){
				$criteres['show_rdv_adaptation'] = 1;
			}
			$reservation_list = $this->reservation_model->get_reservation_panier_list($criteres, "ORDER BY reservation.date_creation DESC");
			echo json_encode($reservation_list);
		}
	}
	
	public function get_accueil_adapation_moins_5_jour_rdv_adaptation(){
		$parent = $this->get_parent_by_user_connected();
		$accueil_adapation_moins_5_jour_list = array();
		if ($parent != null){
			$rdv_adaptation_list = $this->get_rdv_adaptation_panier_list($parent->id);
			if(is_array($rdv_adaptation_list)){
				foreach ($rdv_adaptation_list as $rdv_adaptation){
					$resa_panier_adaptation_list = $this->get_resa_panier_adaptation_list_by_structure_id($rdv_adaptation->structure_id, $parent->id);
					if(is_array($resa_panier_adaptation_list)){
						foreach ($resa_panier_adaptation_list as $resa_panier_adaptation){
							$date_accueil = $resa_panier_adaptation->regulier ? $resa_panier_adaptation->date_fin : $resa_panier_adaptation->jour;
							$jour_diff = date_diff_day($date_accueil, $rdv_adaptation->jour);
							if($jour_diff < $this->config->item('nb_jour_adaptation') && !in_array_object($accueil_adapation_moins_5_jour_list, "reservation_id", $resa_panier_adaptation->reservation_id)){
								array_push($accueil_adapation_moins_5_jour_list, $resa_panier_adaptation);
							}
						}
					}
				}
			}
		}
		return $accueil_adapation_moins_5_jour_list;
	}
	
	public function get_accueil_adapation_moins_5_jour_rdv_adaptation_json(){
		echo json_encode($this->get_accueil_adapation_moins_5_jour_rdv_adaptation());
	}
	
	public function get_resa_panier_adaptation_list_by_structure_id($structure, $parent_id){
		$criteres['structure_id'] = $structure;
		$criteres['parent_id'] = $parent_id;
		$criteres['adaptation'] = 1;
		return $this->reservation_model->get_reservation_panier_list($criteres, "ORDER BY reservation.date_creation DESC");
	}
	
	public function get_rdv_adaptation_panier_list($parent_id, $struture_id = false, $date_naissance = false){
		if(isset($parent_id)){
			$criteres['parent_id'] = $parent_id;
			$criteres['show_rdv_adaptation'] = 1;
			if($struture_id){
				$criteres['structure_id'] = $struture_id;
			}
			if($date_naissance){
				$criteres['date_naissance'] = $date_naissance;
			}
			return $this->reservation_model->get_reservation_panier_list($criteres);
		}
		return false;
	}
	
	public function get_rdv_adaptation_panier_json(){
		$date_naissance = $this->input->post('date_naissance');
		$structure_id = $this->input->post('structure_id');
		$enfant_id = $this->input->post('enfant_id');
		$parent = $this->get_parent_by_user_connected();
		if ($parent != null && $structure_id && $date_naissance){
			$rdv_adaptation_list = $this->get_rdv_adaptation_panier_list($parent->id, $structure_id, date_fr_to_en($date_naissance, '/', '-'));
			if($rdv_adaptation_list && count($rdv_adaptation_list) > 0){
				echo json_encode($rdv_adaptation_list);
			}
		}
	} 
	
	public function get_structure_have_dispo_adaptation_list(){
		$enfant_id = $this->input->post('enfant_id');
		$disponibilite_list_selected = json_decode($this->input->post('json_disponibilite'));
		$structure_adaption_id_list = array();
		if($disponibilite_list_selected && $enfant_id){
			foreach ($disponibilite_list_selected as $disponibilite_id){
				$disponibilite = $this->disponibilites_model->get_disponibilites_by_id($disponibilite_id);
				if($disponibilite->adaptation == 1){
					if(!in_array($disponibilite->structure_id, $structure_adaption_id_list)
						&& !$this->enfant_has_structure_model->check_enfant_is_adapte_in_structure($enfant_id, $disponibilite->structure_id)){
							array_push($structure_adaption_id_list, $disponibilite->structure_id);
					}
				}
			}
			$panier_adaptation_list = $this->get_resa_panier_enfant_necessite_adaptation($enfant_id);
			if($panier_adaptation_list){
				foreach ($panier_adaptation_list as $panier_adaptation){
					if(!in_array($panier_adaptation->structure_id, $structure_adaption_id_list)
						&& !$this->enfant_has_structure_model->check_enfant_is_adapte_in_structure($enfant_id, $panier_adaptation->structure_id)){
							array_push($structure_adaption_id_list, $panier_adaptation->structure_id);
					}
				}
			}
		}
		echo json_encode($structure_adaption_id_list);
	}
	
	public function get_creneaux_adaptations_structure_json(){
		$structure_id = $this->input->post("structure_id");
		$date_naissance = $this->input->post("date_naissance");
		$dispo_adaptation_list = $this->get_creneaux_adaptations_structure($structure_id, $date_naissance);
		if($dispo_adaptation_list){
			echo json_encode($dispo_adaptation_list);
		}
	}
	
	/**
	 * Vérifie si une structure possède un ou des créneaux d'adaptation
	 */
	public function get_creneaux_adaptations_structure($structure_id, $date_naissance = false){
		$criteres['structure_id'] = $structure_id;
		$criteres['show_rdv_adaptation'] = 1;
		$criteres['date_sup_equals_date_du_jour'] = 1;
		if($date_naissance){
			$criteres['date_naissance'] = $date_naissance;
		}
		return $this->disponibilites_model->get_dispo_list_by_more_criteria($criteres);
	}
	
	public function check_structure_has_creneaux_adaptations(){
		$structure_id = $this->input->post("structure_id");
		$date_naissance = $this->input->post("date_naissance");
		$dispo_adaptation_list = $this->get_creneaux_adaptations_structure($structure_id, $date_naissance);
		$has_creneau = "0";
		if($dispo_adaptation_list && count($dispo_adaptation_list) > 0){
			$has_creneau = "1";
		}else if($structure_id){
			$parent = $this->get_parent_by_user_connected();
			if($parent){
				$rdv_adaptation_list = $this->get_rdv_adaptation_panier_list($parent->id, $structure_id, $date_naissance);
				$has_creneau = ($rdv_adaptation_list && count($rdv_adaptation_list) > 0) ? "1" :"0";
			}
		}
		echo $has_creneau;
	}
	
	public function get_reservation_by_id_json(){
		$reservation_id = $this->input->post("reservation_id");
		$reservation = $this->reservation_model->get_reservation_by_id($reservation_id);
		if($reservation){
			echo json_encode($reservation);
		}
	}
	
	public function get_resa_panier_enfant_necessite_adaptation($enfant_id){
		$criteres['enfant_id'] = $enfant_id;
		$criteres['adaptation'] = 1;
		$panier_list = $this->reservation_model->get_reservation_panier_list($criteres);
		return ($panier_list && count($panier_list) > 0) ? $panier_list : false;
	}
	
	public function disponibilite_necessite_adaptation(){
		$enfant_id = $this->input->post('enfant_id');
		$disponibilite_list_selected = json_decode($this->input->post('json_disponibilite'));
		$dispo_enfant_adaptation_list = array();
		if($disponibilite_list_selected && $enfant_id){
			foreach ($disponibilite_list_selected as $disponibilite_id){
				$disponibilite = $this->disponibilites_model->get_disponibilites_by_id($disponibilite_id);
				if($disponibilite->adaptation == 1){
					if($structure_id != null && !$this->enfant_has_structure_model->check_enfant_is_adapte_in_structure($enfant_id, $disponibilite->structure_id)){
						array_push($dispo_enfant_adaptation_list, $disponibilite);
					}
				}
			}
		}
		echo json_encode($dispo_enfant_adaptation_list);
	}
	
	public function reservation_necessite_adaptation(){
		$reservation_list_selected = json_decode($this->input->post('json_reservation'));
		$resa_par_enfant_adaptation_list = array();
		if($reservation_list_selected){
			$reservation_par_enfant_list= group_array_object_by_key($reservation_list_selected, "enfant_id");
			foreach ($reservation_par_enfant_list as $key => $reservation_enfant_list){
				$resa_enfant_adaptation_list = array();
				foreach ($reservation_enfant_list as $reservation_enfant){
					if($reservation_enfant->adaptation == 1){
						$structure_id = $this->get_structure_id_by_dispo_id($reservation_enfant->disponibilites_id);
						if($structure_id != null && !$this->enfant_has_structure_model->check_enfant_is_adapte_in_structure($reservation_enfant->enfant_id, $structure_id)){
							array_push($resa_enfant_adaptation_list, $reservation_enfant);
						}
					}
				}
				if(count($resa_enfant_adaptation_list) > 0){
					$resa_par_enfant_adaptation_list[$key] = $resa_enfant_adaptation_list;
				}
			}
		}
		echo json_encode($resa_par_enfant_adaptation_list);
	}
	
	public function reservation_incompatible_age_enfant(){
		$reservation_list_selected = json_decode($this->input->post('json_reservation'));
		$resa_par_age_enfant_incompatible_list = array();
		if($reservation_list_selected){
			$reservation_par_enfant_list= group_array_object_by_key($reservation_list_selected, "enfant_id");
			foreach ($reservation_par_enfant_list as $key => $reservation_enfant_list){
				$resa_age_enfant_incompatible_list = array();
				foreach ($reservation_enfant_list as $reservation_enfant){
					if(!$this->disponibilites_model->enfant_dans_age_dispo($reservation_enfant->disponibilites_id, $reservation_enfant->date_naissance)){
						array_push($resa_age_enfant_incompatible_list, $reservation_enfant);
					}
				}
				if(count($resa_age_enfant_incompatible_list) > 0){
					$resa_par_age_enfant_incompatible_list[$key] = $resa_age_enfant_incompatible_list;
				}
			}
		}
		echo json_encode($resa_par_age_enfant_incompatible_list);
	}
	
	public function get_disponibilites_compatible_age_enfant(){
		$date_naissance = date_fr_to_en($this->input->post('date_naissance'), '/', '-');
		$disponibilite_list_selected = json_decode($this->input->post('json_disponibilite'));
		$dispo_age_compatible_list = array();
		$dispo_age_incompatible_list = array();
		$this->session->set_userdata('last_dispos_selected', $disponibilite_list_selected);
		if($date_naissance && $disponibilite_list_selected){
			foreach ($disponibilite_list_selected as $disponibilite_id){
				$disponibilite = $this->disponibilites_model->get_disponibilites_by_id($disponibilite_id);
				if($this->disponibilites_model->enfant_dans_age_dispo($disponibilite_id, $date_naissance)){
					array_push($dispo_age_compatible_list, $disponibilite);
				}else {
					array_push($dispo_age_incompatible_list, $disponibilite);
				}
			}
		}
		$result = array(
			'dispo_age_compatible_list' => $dispo_age_compatible_list,
			'dispo_age_incompatible_list' => $dispo_age_incompatible_list
		);
		echo json_encode($result);
	}
	
	public function get_structure_id_by_dispo_id($disponibilite_id){
		$disponiblite_list = $this->disponibilites_model->get_dispo_list_by_simple_criteria(array('id' => $disponibilite_id));
		if(count($disponiblite_list) > 0){
			return $disponiblite_list[0]->structure_id;
		}
		return null;
	}
	
	public function get_enfant_by_json() {
		$enfant_id = $this->input->post('enfant_id');
		$criteres_enfant = array("enfant_id" => $enfant_id);
		$enfant_list = $this->enfant->get_enfant_list($criteres_enfant);
		$result = array();
		if(count($enfant_list) > 0){
			$enfant_list = current($enfant_list);
			//c'est ville qui prime 
			$enfant_code_postal_or_ville =  empty($enfant_list->enfant_ville) ? $enfant_list->enfant_code_postal : $enfant_list->enfant_ville;

			$result = array(
				"enfant_id" => $enfant_list->enfant_id,
				"enfant_nom" => $enfant_list->enfant_nom,
				"enfant_prenom" => $enfant_list->enfant_prenom,
				"enfant_date_naissance" => $enfant_list->enfant_date_naissance,
				"enfant_code_postal_or_ville" => $enfant_code_postal_or_ville
			);
		} 
		echo json_encode($result);		
	}
	
	public function get_enfant_list_json() {
		$parent_id = $this->input->post('parent_id');
		$enfant_id = $this->input->post('enfant_id');
		$date_naissance = $this->input->post('date_naissance');
		$criteres = array();
		if ($parent_id){
			$criteres['parent_id'] = $parent_id;
		}
		if($enfant_id){
			$criteres['enfant_id'] = $enfant_id;
		}
		if($date_naissance){
			$criteres['date_naissance'] = date_fr_to_en($date_naissance, '/', '-');
		}
		
		$enfant_list = $this->enfant->get_enfant_list($criteres);
		if(count($enfant_list) > 0){
			echo json_encode($enfant_list);
		} 
	}
	
	public function get_enfant($criteres_enfant){
		$enfant_list = $this->enfant->get_enfant_list($criteres_enfant);
		$enfant_reservee = null;
		if(count($enfant_list) > 0){
			$count_enfant_reservee = $this->session->userdata('count_enfant_reservee');
			if(empty($count_enfant_reservee)){
				$count_enfant_reservee = 0;
			}
			$count_enfant_reservee ++;
			if($count_enfant_reservee > count($enfant_list)) $count_enfant_reservee = 1;
			$this->session->set_userdata('count_enfant_reservee', $count_enfant_reservee);
			return $enfant_list[$count_enfant_reservee - 1];
		}
		return null;
	}
	
	public function get_stucture_reserve_list($reservation_list) {
		$structure_id_list = array();
		$structure_list = array();
		foreach ($reservation_list as $reservation){
			if(!in_array($reservation->structure_id, $structure_id_list)){
				array_push($structure_id_list, $reservation->structure_id);
				$structure = new stdClass();
				$structure->id = $reservation->structure_id;
				$structure->nom = $reservation->structure_nom;
				$structure->image_names = $this->get_pictures_name_structure($structure->id);
				array_push($structure_list, $structure);
			}
		}
		return $structure_list;
	}
	
	public function get_structures_by_dispo_list($disponibilite_list, $isRDVAdaptation = false){
		$structure_id_list = array();
		foreach ($disponibilite_list as $disponibilite) {
			if(!in_array($disponibilite->structure_id, $structure_id_list)){
				array_push($structure_id_list, $disponibilite->structure_id);
			}
		}
		
		$structure_list = array();
		foreach ($structure_id_list as $structure_id){
			$i = 0;
			foreach ($disponibilite_list as $disponibilite){
				//CRA: $disponibilite->id != null
				if($structure_id == $disponibilite->structure_id && ($disponibilite->id != null)){
					$i++;
				}
			}

			$structure = $this->structure_model->get_structure_by_id($structure_id, true);
	
			$structure->image_names = $this->get_pictures_name_structure($structure->structure_id);
			$structure->nb_dispo = $i;
			$structure->isRDVAdaptationPicto = $isRDVAdaptation;
			array_push($structure_list, $structure);
		}
		return $structure_list;
	}
	
	public function get_stucture_id_by_dispo_list($disponibilite_list) {
		$structure_id_list = array();
		foreach ($disponibilite_list as $disponibilite){
			if(!in_array($disponibilite->structure_id, $structure_id_list)){
				array_push($structure_id_list, $disponibilite->structure_id);
			}
		}
		return $structure_id_list;
	}
	
	public function  get_pictures_name_structure($structure_id){
		$images_name = array();
		$structure_directory = $this->config->item('dir_pictures_creche').$structure_id;
		if (is_dir($structure_directory)){
			$files_name_list = scandir($structure_directory);
			if(count($files_name_list) >= 1){
				foreach ($files_name_list as $files_name){
					if(in_array(get_file_extension($files_name), $this->config->item('files_image_accepted'))){
						array_push($images_name, structures_url($structure_id.'/'.$files_name));
					}
				}
			}
		}
		if(count($images_name) < 1){
			$images_name = array(structures_url('default.jpg'));
		}
		return $images_name;
	}
	
	public function  get_logo_structure($structure_id){
		$structure_directory_log = $this->config->item('dir_logo_creche');
		$files_name_list = scandir($structure_directory_log);
			if(count($files_name_list) >= 1){
				foreach ($files_name_list as $files_name){
					if(in_array(get_file_extension($files_name), $this->config->item('files_image_accepted'))){
						 if($structure_id.'.'.get_file_extension($files_name)==$files_name)						
							return logos_structures_url($files_name);
					}
				}
		}		
		return logos_structures_url($files_name);
	}
	
	public function get_logo_ville($nom_ville){
		$uri = $this->config->item('dir_logo_ville');
		if(is_file($uri.'/'.$nom_ville.'png')){
			return ville_url($nom_ville.'png');
		}
		else{
			return ville_url('Madrid.png');
		}
	}
	
	/**
	 * Récupération des informations des crèches trouvés
	 * @param $structure_list
	 */
	public function get_structure_list_info($structure_list, $criteres = null, $check_dispo = false) {
		$structure_array = array();
		foreach ($structure_list as $key => $structure){
			// Récupération des images de la structure pour la slide
			$structure->nom = str_replace('Crèche', '', $structure->nom);
			$structure->image_names = $this->get_pictures_name_structure($structure->id);
			if ($criteres != null){
				$criteres['structure_id'] = $structure->id;
				//Voir nb dispo : ne pas prendre en compte que les places libres				
				$nbDispo = 0;
				$disponibilite_list = $this->disponibilites_model->get_dispo_list_by_more_criteria($criteres);
				foreach ($disponibilite_list as $disponibilite){
					//N'afficher que les disponibilites ayant des places libres (nb_place libre du disponibilite > total reservation sur le disponilites)
					if($this->disponibilites_model->get_disponibilites_libre($disponibilite->id)){
						$nbDispo++;
					}
				}
				$structure->nb_dispo = $nbDispo;
				
				//$structure->nb_dispo = (int)count($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));
				if (isset($criteres['show_rdv_adaptation']) && $criteres['show_rdv_adaptation']==1) {
					$structure->isRDVAdaptationPicto = true;
				} else {
					$structure->isRDVAdaptationPicto = false;
				}
				if($structure->nb_dispo != 0){
					array_push($structure_array, $structure);
				}
			}
		}
		
		$res = $check_dispo ? $structure_array : $structure_list;
		return $res;
	}
	
	/**
	 * Récupération des informations des disponibilités trouvés
	 * @param $disponibilite_list
	 */
	public function get_disponibilite_list_info($disponibilite_list) {
		//$disponibilite_libre_list = array();
		foreach ($disponibilite_list as $disponibilite){
			$disponibilite = $this->get_disponibilite_info($disponibilite);
			//N'afficher que les disponibilites ayant des places libres (nb_place libre du disponibilite > total reservation sur le disponilites)
			//if($this->disponibilites_model->get_disponibilites_libre($disponibilite->id)){
				//array_push($disponibilite_libre_list, $disponibilite);
			//}
		}
		return $disponibilite_list;
	}
	
	public function get_disponibilite_info($disponibilite, $jour_a_la_ligne = true){
		if($disponibilite->regulier){
			$disponibilite->jour = get_jour_detail_dispo_regulier($disponibilite->id, $disponibilite);
		}else{
			$disponibilite->jour = strtoupper(get_day_name_by_date($disponibilite->jour)).' '.date_en_to_fr($disponibilite->jour,'-','/');
		}
		$horaireDispo = date('H:i',strtotime($disponibilite->horaire_arrivee_tot))." à ".date('H:i',strtotime($disponibilite->horaire_depart_tard));		
		$disponibilite->horaire_dispo = $horaireDispo;
		if (isset($disponibilite->plafondPSU)){
			$disponibilite->plafond_psu = calcul_plafond_tarif_psu($disponibilite->plafondPSU);
		}
		return $disponibilite;
	}
	
	/**
	 * JSON : Récuperer un ville à partir de son code postale
	 */
	function get_ville_json_by_code_postal(){
		$code_postal = $this->input->post('code_postal');
		$ville = $this->ville_model->get_ville_by_code_postal($code_postal);
		if($ville){
			echo json_encode($ville[0]);
		}
	}
	
	/**
	 * JSON : Récuperer un ville à partir de son nom
	 */
	
	function get_ville_json_by_name(){
		$name = $this->input->post('nom_ville');
		if($name){
			$criteres = array();
			$criteres['nom'] = $name;
			$ville = $this->ville_model->get_ville($criteres);
			if($ville){
				echo json_encode($ville[0]);
			}
		}
	}
	
	function get_ville_json_like_name(){
		$nom_ville = $this->input->get('nom_ville');
		$ville_contains_structure = $this->input->get('ville_contains_structure');
		$ville_list = $this->ville_model->get_ville_list_like_name($nom_ville, $ville_contains_structure);
		header('Content-type: application/json');
		echo json_encode($ville_list);
	}
	
	function get_type_accueil_json(){
		$date_naissance = $this->input->post('date_ne_le');
		$not_show_adaptation = $this->input->post('not_show_adaptation');
		$struture_id_list = $this->input->post('structure_array_id');
		
		$criteres = array();
		if($date_naissance)
			$criteres['date_naissance'] = $date_naissance;
		if($not_show_adaptation)
			$criteres['not_show_adaptation'] = $not_show_adaptation;
		if($struture_id_list)
			$criteres['struture_id_list'] = $struture_id_list;
			
		$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list_by_more_criteria($criteres);
		echo json_encode($type_temps_accueil_list);
	}
	
	function get_temps_accueil_json(){
		$structure_id = $this->input->post('structure_id');
		$type_accueil_id = $this->input->post('type_accueil_id');
		
		if($structure_id && $type_accueil_id){
			$temps_accueil_list = $this->temps_accueil_model->get_temps_accueil_by_type_id_structure_id($type_accueil_id, $structure_id);
			if($temps_accueil_list && count($temps_accueil_list) > 0){
				echo json_encode($temps_accueil_list[0]);
			}
		}
	}
	
	/**
	 * JSON : Récupérer un ville à partir d'un bounds
	 */
		
	function get_ville_list_json_by_bounds(){
		$lat_min = $this->input->post('lat_min');
		$lat_max = $this->input->post('lat_max');
		$long_min = $this->input->post('long_min');
		$long_max = $this->input->post('long_max');
		
		if($south_lat && $south_long && $north_lat && $north_long){
			$criteres = array();
			$criteres['lat_min'] = (float) $lat_min;
			$criteres['lat_max'] = (float) $lat_max;
			$criteres['long_min'] = (float) $long_min;
			$criteres['long_max'] = (float) $long_max;
			$ville = $this->ville_model->get_ville($criteres);
			echo json_encode($ville);
		}
	}
	
	public function get_parent_by_user_connected(){
		$parent = null;
		$user = $this->ion_auth->user()->row();
		if(isset($user) && isset($user->id)){
			$criteres['users_id'] = $user->id;
			$parent_list = $this->parent->get_parent_list($criteres);
			$parent = null;
			if(count($parent_list) >= 1){
				$parent = $parent_list[0];
			}
		}
		return $parent;
	}
	
	public function get_parent_by_user_connected_json(){
		$parent = $this->get_parent_by_user_connected();
		if($parent != null){
			echo json_encode($this->get_parent_by_user_connected());
		}else{
			echo '';
		}
	}
		
	public function get_creche($str_id=0, $idTypeRecherche=''){	
        $this->load->js($this->config->item('url_api_google_map'));
		$this->load->js('assets/js/custom_api_google_map.js');
		$this->load->js("assets/js/infobox.js");//infoBox pour googleMap
		$this->load->js('assets/js/jquery.nailthumb.1.1.min.js');
		$this->load->css('assets/css/jquery.nailthumb.1.1.min.css');
		$enfant_id = false;
		$via_mon_compte = false;
		$tab_active = $this->input->post('tab_active');
		$menu_actualite_selected = $this->input->post('menu_actualite_selected');

		if($_REQUEST)  {
			$id = $this->input->post('structure_id_hidden');
			$idTypeRecherche = $this->input->post('critere_type_recherche_hidden');
			$enfant_id = $this->input->post('enfant_id');
			$via_mon_compte = $this->input->post('via_mon_compte');
			if (empty($idTypeRecherche)) {
				$idTypeRecherche =  $this->input->post('critere_type_recherche');
			}
			$date_naissance = $this->input->post('date_ne_le');
		}
		else {			
			$id = $str_id;					
		}	
		//Dans le cas ou l'utilisateur n'a pa sélectionné de crèche depuis la page recherche picto
		//on la redirige dans la page de recheche picto;
		if(!$id || empty($id)){
			redirect('famille/rechercher_creche');
		}
		//Si via établissement, il nous faut garder les filtres sinon c'est écrasé par dispo de la semaine dans actualité
		$is_via_etablissement = $this->input->post('is_via_etablissement');
		if ($is_via_etablissement) {			
			$param_criteres_search = $this->session->userdata('param_criteres_search_creche');
			if (empty($param_criteres_search)) {
				$param_criteres_search = $_REQUEST;
			}
		} else {
			$param_criteres_search = $this->session->userdata('param_criteres_search');
			$this->session->set_userdata('param_criteres_search_creche', $param_criteres_search);
		}
		
		$code_postal_or_ville = '';
		$type_accueils = '';
		$jours_particulier = '';
		$crenau_regulier = '';
		$deb_periode = '';
		$fin_periode = '';
		
		if(is_array($param_criteres_search)){
			if (empty($date_naissance)) {
				$date_naissance = array_key_exists('date_naissance', $param_criteres_search)? $param_criteres_search['date_naissance'] : '';
			}	
			$code_postal_or_ville = array_key_exists('code_postal_or_ville', $param_criteres_search)? $param_criteres_search['code_postal_or_ville'] : '';
			$type_accueils = array_key_exists('type_accueils', $param_criteres_search)? $param_criteres_search['type_accueils'] : '';
			$jours_particulier = array_key_exists('jours_particulier', $param_criteres_search)? $param_criteres_search['jours_particulier'] : '';
			$crenau_regulier = array_key_exists('crenau_regulier', $param_criteres_search)? $param_criteres_search['crenau_regulier'] : '';
			$deb_periode = array_key_exists('deb_periode', $param_criteres_search)? $param_criteres_search['deb_periode'] : '';
			$fin_periode = array_key_exists('fin_periode', $param_criteres_search)? $param_criteres_search['fin_periode'] : '';	
		}
		
		$structure = $this->structure_model->get_structure_by_id($id);
		$structure->nom_ville=$this->ville_model->get_name_by_id($structure->ville_id);
		
		//Recuperation ouverture
		$structure->ouverture_list = $this->ouverture_model->get_ouverture_list_by_structure_id_and_annee_scolaire_id($structure->structure_id,get_current_annee_scolaire()->id);
		// Recuperation fermeture
		$structure->fermeture_list = $this->fermeture_model->get_fermeture_list_by_structure_id_and_by_annee_scolaire_id($structure->structure_id,get_current_annee_scolaire()->id);
		// REcuperation des journees pedagogiques
		$structure->jpedag_list = $this->journee_pedag->get_by_struct_id_and_annee_scolaire_id($structure->structure_id, get_current_annee_scolaire()->id);
		/*Recuperation de l'images creche*/  
    	$structure->id = $structure->structure_id;
     	$structure->image_names = $this->get_pictures_name_structure($structure->id);
     	
     	$criteres['structure_id'] = $structure->structure_id;	
		$structure->nb_dispo = count($this->disponibilites_model->get_dispo_list_by_more_criteria($criteres));		
		
      	$typeRecherche = empty($typeRecherche) ? self::RECHECHE_DISPO : $typeRecherche;
		$type_temps_accueil_list = $this->type_temps_accueil_model->get_type_temps_accueil_list_by_date_naissance(date_fr_to_en($date_naissance, '/', '-'), 1);
		
		$structure_list[0] = (array) $structure;
		
		$data = array();
		//recuperation donne ville
		$ville= $this->ville_model->get_ville_by_id($structure->ville_id);
		if($ville){
			$data['ville_creche'] = $ville;
			$structure_list[0]['ville_creche'] = $ville->nom;
		}
		
		$data['structure_list'] = $structure_list;
		$data = array_merge($data, $this->data_criteres_recherche('', $type_temps_accueil_list));
		
		//CRA : 04/10/2014 : Prendre en compte DISPO mais non pas TEMPS D'ACCUEIl			
		 $data['type_temps_accueil_list'] =  $this->temps_accueil_model->get_by_struct_id($structure->structure_id);       	
       	 
     	$parent = $this->get_parent_by_user_connected();
		if ($parent != null) {
			$criteres['parent_id'] = $parent->id;
			if($via_mon_compte){
				$data['enfant_parent_list'] = $this->enfant->get_enfant_list(array('parent_id' => $parent->id));
				if($enfant_id){
		       		$enfant_selected_list = $this->enfant->get_enfant_list(array('parent_id' => $parent->id, 'enfant_id' => $enfant_id));
		       		$data['enfant_parent_selected'] = ($enfant_selected_list && count($enfant_selected_list) > 0) ? current($enfant_selected_list) : false;
		       	}
			}
		}
     	
       	$data['structure'] = $structure;
       	if($enfant_id){
       		$data['enfant_id'] = $enfant_id;
       	}
		
       	/* attribut pour la formulaire de critere*/
       	$data['form_search_dispo_attribute'] = array('id' => 'form_search_dispo'); //rechercher_dispo_creche
      	
      	if ($idTypeRecherche == 0 || $idTypeRecherche == '') {
      		$this->session->set_userdata('action_dispo', 'famille/get_creche/'.$structure->id);
      	} else {
      		$this->session->set_userdata('action_dispo', 'famille/get_creche/'.$structure->id . "/" . $idTypeRecherche);
      	}
      	
      	$data = array_merge($data, $this->data_criteres_recherche($date_naissance, $type_temps_accueil_list, $type_accueils, $jours_particulier, $crenau_regulier, $deb_periode, $fin_periode));
      	
      	//CRA :
      	$data['tab_active'] = $tab_active;
      	$data['menu_actualite_selected'] = $menu_actualite_selected;
      	$data['critere_type_recherche'] = $data['idTypeRecherche'] = $idTypeRecherche; 
      	$data['type_recherche'] = $typeRecherche;      	
      	$data['structure_id'] = $id;
      	$data['nb_etablissement_trouve'] = 1;
      	$data['code_postal_or_ville'] = $code_postal_or_ville;
      	$data['field_code_postal_or_ville'] = $code_postal_or_ville;
      	
      	$this->load->view('familles/fiche_dispo', $data);
	}
	
	public function load_famille_reconnue_mes_reservation(){
		$user = $this->ion_auth->user()->row();
		$structure_id = $this->input->post('structure_id');
		$enfant_id = $this->input->post('enfant_id');
		if($user){
			$idUserParent = $user->id;
			if (!empty($idUserParent)) {			
				$data['lst_enfant'] = $this->get_enfant_list_for_resa($idUserParent, $enfant_id);
				$data['users_id'] = $idUserParent;
				if($structure_id){
					$data['structure_id'] = $structure_id;
				}
				$result = array( 
					'html' => $this->load->view('familles/famille_reconnue_mes_reservation', $data, TRUE)
				);
				echo json_encode($result);
			}
		}
	}
		
	public function get_fiche_activite($temps_accueil_id, $structure_id =0){
		$data = $this->get_data_fiche_activite($temps_accueil_id, $structure_id);
		$data['name_modal'] =  "myModal";
        echo $this->load->view('familles/fiche_activite',$data,TRUE);
	}
	
	public function get_fiche_activite_by_dispo($dispo_id){
		$dispo = $this->disponibilites_model->get_dispo_resa_by_id($dispo_id);		
		$dispo = current($dispo);
	
		/*
		$type_temps_accueil_id = $dispo->type_temps_accueil_id ; //CRA : ??? $dispo->temps_accueil_id;
		$structure_id = $dispo->structure_id;		
		$temps_accueil = current($this->temps_accueil_model->get_temps_accueil_by_type_id_structure_id($type_temps_accueil_id,$structure_id));		
		$data = $this->get_data_fiche_activite($temps_accueil->id);
		*/		

		$temps_accueil = current($this->temps_accueil_model->get_by_struct_id_in_dispo($dispo->structure_id, $dispo->type_temps_accueil_id));
		$data = $this->do_get_data_fiche_activite($dispo->structure_id);
		$data['temps_accueil'] = $temps_accueil;			
		$data['dispo'] =  $dispo;
		$data['name_modal'] =  "myModalDispo";
      	echo $this->load->view('familles/fiche_activite',$data,TRUE);
	}
	
	public function do_get_data_fiche_activite($structure_id){
		//Recup ville creche
		$structure = $this->structure_model->get_structure_by_id($structure_id);
		$ville_creche = $this->ville_model->get_ville_by_id($structure->ville_id);
		$data['ville_creche'] = $ville_creche;
		
		/*Recuperation de l'images creche*/  
     	$structure->id = $structure_id;
      	$structure->logo = $this->get_logo_structure($structure->id);
		$image_creche = current($this->get_pictures_name_structure($structure->id));
		$structure->image_creche = $image_creche;
		$data['structure']=$structure;
		
		return $data;
	}
	
	public function get_data_fiche_activite($temps_accueil_id, $structure_id=0){
		//Recup temps accueil en se référent aux dispos
		$temps_accueil = current($this->temps_accueil_model->get_temps_accueil_by_temps_accueil_id($temps_accueil_id));		
		
		$data = $this->do_get_data_fiche_activite($temps_accueil->structure_id);
		$data['temps_accueil'] = $temps_accueil;

		return $data;      	
	}
	
    public function get_actualite_by_id(){      
	  echo $this->load->view('familles/fiche_etablissement',null,TRUE);  
	}
	
	public function check_user_is_parent_connected(){
		echo is_parent_connected() ? '1' : '0';
	}
	
	public function send_mail_directeur() {
		$from_name = $this->input->post('rdv_mail_name');
		$to_email =  $this->input->post('rdv_to_email');
		$from_email = $this->input->post('rdv_mail_adr');
		$subject =  $this->input->post('rdv_mail_subject');
		$message =  $this->input->post('rdv_mail_content');
		$sendstatus = send_email_without_template($from_email, $to_email, $from_name, $subject, $message);
 		$status = $sendstatus ? 'success' : 'error' ;
		$message = $sendstatus ? 'Votre email a bien &eacute;t&eacute; envoy&eacute;' : 'Erreur lors de l&rsquo;envoi';
		echo json_encode(array('status' => $status, 'message' => $message));
	}
	
	public function send_mail_contact(){
		$name = $this->input->post('mail_name');
		$content = $this->input->post('mail_content');
		$adr = $this->input->post('mail_adr');
		$subject = $this->input->post('mail_subject');
		$data = array('expediteur' => $name,'contenu' => $content);
		$template = $this->config->item('famille_email_templates').$this->config->item('famille_email_contact');
		$sendstatus = send_email_familiplace($adr, $this->config->item('adresse_email_smartcite'), $name, $template, $subject, $data);
		//send_email_familiplace($from_email, $to_email, $from_name, $template, $subject, $data);
		$status = $sendstatus ? 'success' : 'error' ;
		$message = $sendstatus ? 'Votre email a bien &eacute;t&eacute; envoy&eacute;' : 'Erreur lors de l&rsquo;envoi';
		echo json_encode(array('status' => $status, 'message' => $message));
	}
	
	public function check_mail_alerte(){
		$all_parent_list = $this->parent->get_parent_list();
		if(count($all_parent_list) > 0){
			foreach ($all_parent_list as $parent){
				$parent_id = $parent->id;
				$this->send_mail_alerte_parent($parent_id);
			}
		}
	}
	
	public function check_mail_alerte_parent(){
		$this->send_mail_alerte_parent(1);
	}
	
	public function send_mail_alerte_parent($parent_id){
		$parent = $this->parent->get_parent_by_id($parent_id);
		if ($parent){
			$type_envoie_alerte_id = $parent && isset($parent->type_envoie_alerte_id) ? $parent->type_envoie_alerte_id : 1;
			switch ($type_envoie_alerte_id){
				case 1 : // Une fois par semaine
					if(strtolower($this->config->item('jour_envoie_mail_par_semaine') == convert_jour_fr_to_en(strtolower(date('l'))))){
						$this->send_mail_alerte_parent_par_enfant($enfant->enfant_id, $parent_id, $parent->email);
					}
					break;
				case 2 : //Dès que la disponibilité existe
					$this->send_mail_alerte_parent_par_enfant($parent_id, $parent->email);
					break;
				case 3 : //Pas d'envoie par mail
					break;
				default: //Par défaut une fois par semaine
					if(strtolower($this->config->item('jour_envoie_mail_par_semaine') == convert_jour_fr_to_en(strtolower(date('l'))))){
						$this->send_mail_alerte_parent_par_enfant($parent_id, $parent->email);
					}
					break;
			}
		}
	}
	
	public function send_mail_alerte_parent_par_enfant($parent_id, $email_parent){
		$criteres['parent_id'] = $parent_id;
		$enfant_list = $this->enfant->get_enfant_list($criteres);
		if(is_array($enfant_list)){
			foreach ($enfant_list as $enfant){
				$this->send_mail_alerte($enfant->enfant_id, $parent_id,$email_parent);
			}
		}
	}
	
	public function send_mail_alerte($enfant_id, $parent_id, $email_parent){
		$disponibilite_list = $this->get_disponibilite_result_alerte($enfant_id, $parent_id, true);
		if(count($disponibilite_list)){
			$parent = $this->parent->get_parent_by_id($parent_id);
			$enfant = $this->enfant->get_enfant_by_id($enfant_id);
			$from_email = $this->config->item('no_reply_mail');
			$to_email = $email_parent;
			$from_name = $this->config->item('no_reply_name');
			$template = $this->config->item('famille_email_templates').$this->config->item('alerte_famille_email_template');
			$subject = 'Créneau disponible pour '.$enfant->prenom;
			$data = array(
				'fullNameParent' => $parent->nom . " " . $parent->prenom,
				'first_name_enfant' => $enfant->prenom,
				'titleParent' => get_title_by_id($parent->titre),
				'disponibilite_list' => $disponibilite_list,
				'from_name' => $from_name
			);  
			send_email_familiplace($from_email, $to_email, $from_name, $template, $subject, $data);
		}
	}
	
	public function send_mail_famille_enfant_absent($enfant_id, $disponibilite_id, $reservation_id, $date_annulation = ''){
		if($enfant_id){
			$parent = $this->parent->get_parent_by_enfant_id($enfant_id);
			$enfant = $this->enfant->get_enfant_by_id($enfant_id);
			$disponibilite = $this->disponibilites_model->get_disponibilites_by_id($disponibilite_id);
			$reservation = $this->reservation_model->get_reservation_by_id($reservation_id);
			$user = $this->ion_auth->user()->row();
			$from_email = $user->email;
			$to_email = $parent->email;
			$from_name = $this->config->item('no_reply_name');
			$template = $this->config->item('famille_email_templates').$this->config->item('mail_famille_enfant_absent_template');
			$subject = "Signalisation d'absence de ".$enfant->prenom;
			$data = array(
				'titleParent' => get_title_by_id($parent->titre),
				'fullNameParent' => $parent->nom . " " . $parent->prenom,
				'first_name_enfant' => $enfant->prenom,
				'structure_nom' => $disponibilite->structure_nom,
				'jour_reservation' => date_en_to_fr($reservation->jour_reservation, '-', '/'),
				//'date_annulation' => $date_annulation,
				'from_name' => $from_name
			);  
			send_email_familiplace($from_email, $to_email, $from_name, $template, $subject, $data);
		}
	}
	
	public function send_mail_reservation_famille($parent ,$enfant, $data_enfant){
		if($parent && $enfant){
			$from_email = $this->config->item('no_reply_mail');
			$to_email = $parent->email;
			$from_name = $this->config->item('no_reply_name');
			$template = $this->config->item('famille_email_templates').$this->config->item('famille_email_reservation');
			$subject = 'Réservation de créneau pour '.$enfant->prenom;
			$data = array(
				'fullNameParent' => $parent->nom . " " . $parent->prenom,
				'titleParent' => get_title_by_id($parent->titre),
				'enfant_prenom' => $enfant->prenom,
				'from_name' => $from_name,
				'data_enfant' => $data_enfant
				
			);  
			
			send_email_familiplace($from_email, $to_email, $from_name, $template, $subject, $data);
		}
	}
	
	public function update_coordonnee() {
		$dataRequest = $this->input->post();
		$idParent = $dataRequest['id'];
		$dataParent['nom'] = $dataRequest['nom'];
		$dataParent['prenom'] = $dataRequest['prenom'];
		$dataParent['adresse'] = $dataRequest['adresse'];
		$dataParent['code_postal'] = $dataRequest['code_postal'];
		$dataParent['tel1'] = $dataRequest['tel1'];
		$dataParent['email'] = $dataRequest['email'];
		$dataParent['ville_id'] = $dataRequest['ville_parent_id'];
		$situationMaritale = $dataRequest['situation_maritale'];
		//Recherche nouvelle ville	
		/*$ville_criteres = array("code_postal" => $dataParent['code_postal']);
		$ville_list = $this->ville_model->get_ville($ville_criteres);
		if(count($ville_list) > 0){
			$ville = current($ville_list);
			$dataParent['ville_id'] = $ville->id;
		}*/
		  		
		$lstSituation = get_situation_maritale(false);
		if (!empty($situationMaritale)) {
			$dataParent['situation_maritale'] = array_search($situationMaritale, $lstSituation);
		}
		
		$result = array();
		if ($this->parent->update($dataParent,  $idParent)) {	
			//Voir les enfants
			$criteresEnfant = array("parent_id" => $idParent, "is_same_adresse" => 1 );
			$lstEnfantToUpdated = $this->enfant->get_enfant_list($criteresEnfant);
			if (!empty($lstEnfantToUpdated)) {
				$dataEnfant['adresse'] = $dataParent['adresse'];
				if (!empty($dataParent['ville_id'])) {
					$dataEnfant['ville_id'] = $dataParent['ville_id'];
				}
				$dataEnfant['code_postal'] = $dataParent['code_postal'];				
				foreach($lstEnfantToUpdated as $rowEnfant) {
					$enfantId = $rowEnfant->enfant_id;
					$this->enfant->update($dataEnfant, $enfantId);
				}				
			}
			$result = array("message" => "Les modifications ont bien été prises en compte");
		}
		
		echo json_encode($result);	
	}
	
	public function send_mail_creer_rdv_directrice($send_mail = true, $enfantInfo = array()){
		$from_email = $this->input->post('parent_email'); 
		$to_email = $this->input->post('directrice_email');
		$from_name = $this->input->post('parent_name');
		$to_name = $this->input->post('directrice_name');
		$parent_tire = $this->input->post('parent_titre');
		$directice_titre = $this->input->post('directrice_titre');
		if (!empty($enfantInfo)) {
        	$to_name = $this->input->post('directrice_name');
        	$to_email = $this->input->post('directrice_email');
			
			$date_naissance = $enfantInfo->enfant_date_naissance;
			$nom_enfant = $enfantInfo->enfant_nom;
			$prenom_enfant = $enfantInfo->enfant_prenom;
			//Chercher parent			
			$criteresParent = array("parent_id" => $enfantInfo->parent_id);
			$lstParent = $this->parent->get_parent_list($criteresParent);
			$parent = current($lstParent);
			$from_email = $parent->email;
        	$from_name =  $parent->prenom. ' ' .$parent->nom;
		} else {
			$date_naissance = $this->input->post('date_naissance');
			$nom_enfant = $this->input->post('nom_enfant');
			$prenom_enfant = $this->input->post('prenom_enfant');
		}		
		$template = $this->config->item('famille_email_templates').$this->config->item('famille_email_demande_rdv_adaptation');
		//$subject = "Demande de création de RDV d'adaptation";
		$subject = "Demande de RDV d'adaptation";
		$nom_prenom = "";		
		if($date_naissance && ($nom_enfant || $prenom_enfant)){
			$prenom_enfant = $prenom_enfant ? $prenom_enfant : '';
			$nom_enfant = $nom_enfant ? $nom_enfant : '';
			$nom_prenom = trim($prenom_enfant." ".$nom_enfant);
			$subject .= " pour ".$nom_prenom." né(e) le ".$date_naissance;
		}
		$data = array(
			'fullNameParent' => $from_name,
			'titleParent' => get_title_by_id($parent_tire),
			'titleDirectrice' => get_title_by_id($directice_titre),
			'from_name' => $from_name,
			'date_naissance' => $date_naissance,
			'enfant_nom_prenom' => $nom_prenom
		);  
		
		$mail_content = new stdClass();
		$this->email->clear();
		$this->email->from($from_email, $from_name);
		$this->email->to($to_email);
		$this->email->subject($subject);
		$message = $this->load->view($template, $data, true);
		$this->email->message($message);
		if($send_mail){
			$this->email->send();
		}
		
		$mail_content->to_email = $to_email;
		$mail_content->from_email = $from_email;
		$mail_content->from_name = $from_name;
		$mail_content->subject = $subject;	
		//$mail_content->subject = str_replace("é", "e", $subject);		
		$mail_content->message = strip_tags($message);
		$mail_content->message = strip_tags($message, "<br>");
		
		return $mail_content;
	}
	
	public function mentions_legales(){
		$this->load->css('assets/css/style_ml.css');
		$data["textes"] = get_footer_text();
		$this->load->view('familles/mention_legale', $data);
	}
	public function cgu(){
		$this->load->model('groups_model');
		$this->load->css('assets/css/style_ml.css');
		$data["textes"] = get_footer_text();
		$isInGroupDirecteur = $this->ion_auth->in_group( $this->groups_model->get_directeur());
		$isInGroupGestionnaire = $this->ion_auth->in_group( $this->groups_model->get_gestionnaire());
		$isInGroupSuperAdmin = $this->ion_auth->in_group( $this->groups_model->get_superAdmin());
		$data["pro"] = false;
		if ( $isInGroupDirecteur  || $isInGroupGestionnaire || $isInGroupSuperAdmin  ) {	$data["pro"] = true;	}
		$this->load->view('familles/cgu', $data);
	}
	
	public function json_verif_ville_cp(){
		$ville = $this->input->post('ville');
		$cp =  $this->input->post('cp');
		if(!empty($ville) && !empty($cp) && verif_ville_cp($ville, $cp)){
			$reponse = array('reponse'=>1);
		}else{
			$reponse = array('reponse'=>0);
		}
		echo json_encode($reponse);
	}
}