-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : priviconfwdb2.mysql.db
-- Généré le :  ven. 25 jan. 2019 à 07:50
-- Version du serveur :  5.6.39-log
-- Version de PHP :  7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `priviconfwdb2`
--

-- --------------------------------------------------------

--
-- Structure de la table `livreor_commentaires`
--

CREATE TABLE `livreor_commentaires` (
  `id` int(10) UNSIGNED NOT NULL,
  `nom` varchar(52) CHARACTER SET utf8 NOT NULL,
  `mail` varchar(56) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `vote` int(12) NOT NULL,
  `date` datetime DEFAULT NULL,
  `idcommercant` int(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `livreor_commentaires`
--
ALTER TABLE `livreor_commentaires`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `livreor_commentaires`
--
ALTER TABLE `livreor_commentaires`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
