-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 21 jan. 2019 à 14:42
-- Version du serveur :  5.7.21
-- Version de PHP :  7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `sortez`
--

-- --------------------------------------------------------

--
-- Structure de la table `reservation_type_sejour`
--

DROP TABLE IF EXISTS `reservation_type_sejour`;
CREATE TABLE IF NOT EXISTS `reservation_type_sejour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Id_commercant` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `Pays` varchar(255) DEFAULT NULL,
  `Nom` varchar(255) NOT NULL,
  `Adresse` varchar(255) DEFAULT NULL,
  `code_postal` varchar(10) DEFAULT NULL,
  `mail` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `id_ville` varchar(255) DEFAULT NULL,
  `tel` varchar(255) NOT NULL,
  `date_debut_res` date DEFAULT NULL,
  `date_fin_res` date DEFAULT NULL,
  `nbre_adulte` int(11) DEFAULT NULL,
  `nbre_enfant` varchar(255) DEFAULT NULL,
  `etat` int(1) DEFAULT NULL,
  `message_client` text,
  `date_de_reservation` date DEFAULT NULL,
  `num_card` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
